//
//  JYPublishMomentViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPublishMomentViewController.h"

#import "JYPublishImgCell.h"
#import "JYPublishMomentHeaderView.h"
#import "JYPublishMomentFooterView.h"
#import "JYImgSelectModel.h"    //图片选择模型
#import "ZLPhotoActionSheet.h"
#import "JYPublishMomentViewModel.h"
#import "JYCommunityViewModel.h"
#import "JYUploadImgsTool.h"

static NSInteger maxImgCount = 9;   //暂定最多可选9张图片

static NSString *const cellId = @"JYPublishImgCell";
static NSString *const footerId = @"JYPublishMomentFooterView";
static NSString *const headerId = @"JYPublishMomentHeaderView";

@interface JYPublishMomentViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) UICollectionViewFlowLayout *layout;

@property (nonatomic ,strong) NSMutableArray *imgsArr;
@property (nonatomic ,assign) NSInteger selectedImgCount;   //已选图片张数

@property (nonatomic ,strong) JYPublishMomentViewModel *rootVM;
@property (nonatomic,strong) JYCommunityViewModel *communityVM;
@property (nonatomic,strong) JYUploadImgsTool *uploadViewModel;

@property (nonatomic,copy) NSString *content ;//内容
@property (nonatomic,copy) NSString *imgStr;//图片地址;
//@property (nonatomic,copy) NSString *imageStr; //图片
@property (nonatomic,copy) NSString *classifyId;//分类id
@property (nonatomic,strong) NSArray *categoryArr;//分类数组
@property (nonatomic,strong) NSMutableArray *imgUrlArray; //图片链接数组
//@property (nonatomic,strong) JYPublishMomentFooterView *footer;
@property (nonatomic,strong) NSMutableArray<UIImage *> *picArr; //图片数组

@end

@implementation JYPublishMomentViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
    [self requestCommunityCategory];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    self.selectedImgCount = 0;
    
    JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
    model.isAddBtn = YES;
    model.img = nil;
    [self.imgsArr addObject:model];
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"发布";
    
    self.layout = [[UICollectionViewFlowLayout alloc] init];
    self.layout.minimumLineSpacing = 10;
    self.layout.minimumInteritemSpacing = 7;
    self.layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    
    [self.collectionView registerNib:[UINib nibWithNibName:cellId bundle:nil] forCellWithReuseIdentifier:cellId];
    CGFloat itemWidth = (SCREEN_WIDTH-20-(20-7)*3)/4.0;
    CGFloat itemHeight = itemWidth;
    self.layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    
    [self.collectionView registerClass:[JYPublishMomentHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId];
    self.layout.headerReferenceSize = CGSizeMake(SCREEN_WIDTH, 190);
    
    [self.collectionView registerNib:[UINib nibWithNibName:footerId bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerId];
    self.layout.footerReferenceSize = CGSizeMake(SCREEN_WIDTH, 175);
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UICollectionViewDataSource,UICollectionViewDelegateFlowLayout *********

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //暂定最多显示九张
    if (self.imgsArr.count == maxImgCount) {
        return maxImgCount;
    }
    return self.imgsArr.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    JYPublishImgCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
//    [cell updateCellModel:self.imgsArr[indexPath.row] atIndexPath:indexPath];
    [cell updateCelWithImgsModel:self.imgsArr[indexPath.row] atIndexPath:indexPath];
    WEAK(ws);
    cell.delImgBlock = ^(NSIndexPath *tpIndexP){
        [ws updataViewAfterDeleteImageAtIndex:tpIndexP.row];
    };
    
    cell.addImgBlock = ^(){
        ZLPhotoActionSheet *picker = [[ZLPhotoActionSheet alloc] init];
        picker.maxSelectCount = maxImgCount-self.selectedImgCount;
        picker.maxPreviewCount = maxImgCount;
        
        [picker showPhotoLibraryWithSender:ws
                     lastSelectPhotoModels:nil
                                completion:^(NSArray<UIImage *> * _Nonnull selectPhotos, NSArray<ZLSelectPhotoModel *> * _Nonnull selectPhotoModels) {
                                    
//                                    [ws.picArr removeAllObjects];
                                    [ws.picArr addObjectsFromArray:selectPhotos];
                                    [ws updateViewAfterAddImgs:selectPhotos];
                                    
//                                    [ws requestUpLoadImageWithArr:[selectPhotos mutableCopy]];
//                                    [ws updateViewAfterAddImgs:selectPhotos];
                                }];
    };
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        JYPublishMomentHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId forIndexPath:indexPath];
        header.returnContentBlock = ^(NSString *content) {
            weakSelf.content = content;
        };
        return header;
    }
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        WEAK(ws);
        JYPublishMomentFooterView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerId forIndexPath:indexPath];
        [footer updataCategoryWithArr:ws.categoryArr];
        
        footer.selectChannelBlock = ^(NSString *channelId) {
            ws.classifyId = channelId;
        };
        
        footer.publishBlock = ^{
            [ws startPublish];
        };
        return footer;
    }
    return nil;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
//{
//    return CGSizeMake(SCREEN_WIDTH, 190);
//}
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
//{
//    return CGSizeMake(SCREEN_WIDTH, 175);
//}

#pragma mark - ======================== Net Request ========================

- (void)requestPublishBBS:(NSDictionary *)params
{
    WEAKSELF
    [self showHUD];
    [self.rootVM requestPublishParams:params success:^(NSString *msg, id responseData) {
        [self hideHUD];
        [weakSelf showSuccessTip:@"发布成功"];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//获取社区分类
- (void)requestCommunityCategory{
    WEAKSELF
    [self.communityVM requestColumnDataSuccess:^(NSString *msg, id responseData) {
        weakSelf.categoryArr = responseData;
        [weakSelf.collectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)startPublish{
    WEAKSELF
    if (!self.content.length) {
        [self showSuccessTip:@"请输入内容"];
        return ;
    }
    if (!self.picArr.count) {
        [self showSuccessTip:@"请选择图片"];
        return;
    }
    if (!self.classifyId.length) {
        [self showSuccessTip:@"请选择分类"];
        return;
    }
    
    [self requestUpLoadImageWithArr:self.picArr finishBlock:^(NSString *images) {
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"content"] = self.content;
        dic[@"classifyId"] = self.classifyId;
        dic[@"images"] = images;
        [weakSelf requestPublishBBS:dic];
    }];
}


//imageurl 类型
- (void)requestUpLoadImageWithArr:(NSMutableArray *)imageArr finishBlock:(void (^)(NSString * images))finishBlock{
    WEAKSELF
    
    [self.uploadViewModel UploadImgs:imageArr];
    self.uploadViewModel.CompliteBlock = ^(NSMutableArray *imgStrArr ,NSMutableArray *imageHttpArr, NSMutableArray *failArrIndex) {
        if (failArrIndex.count ) {
            [weakSelf showSuccessTip:@"上传失败"];
        }else{
            weakSelf.imgUrlArray =  [imgStrArr mutableCopy];
             NSMutableString * imgStr = [NSMutableString string];
            [weakSelf.imgUrlArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if (idx == 0) {
                    [imgStr appendString:obj];
                }else{
                    [imgStr appendFormat:@",%@",obj];
                }
            }];
            weakSelf.imgStr = [imgStr copy];
            if (finishBlock) {
                finishBlock(weakSelf.imgStr);
            }
        }
//        [weakSelf.collectionView reloadData];
//        [weakSelf updateViewAfterAddImgs:weakSelf.imgUrlArray];
    };
}

//更新数据images 类型
- (void)updateViewAfterAddImgs:(NSArray<UIImage *>*)imgs{

    for (UIImage *image in imgs) {
        if (self.selectedImgCount == maxImgCount-1) {
            JYImgSelectModel *model = self.imgsArr.lastObject;
            model.isAddBtn = NO;
            model.img = image;
            //            model.imageUrl = imageUrl;
            self.selectedImgCount++;
        }
        else
        {
            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
            model.isAddBtn = NO;
            model.img = image;
            //            model.imageUrl = imageUrl;
            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
            self.selectedImgCount++;
        }
    }
    [self.collectionView reloadData];
}


- (void)updataViewAfterDeleteImageAtIndex:(NSInteger)index{
    //    [self.picArr removeObjectAtIndex:index];
    if (self.selectedImgCount == maxImgCount - 1) {
        JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
        model.isAddBtn = YES;
        model.img = nil;
        [self.imgsArr addObject:model];
    }
    if (self.selectedImgCount < maxImgCount) {
        [self.imgsArr removeObjectAtIndex:index];
        [self.picArr removeObjectAtIndex:index];
    }
    
    self.selectedImgCount -- ;
    
    [self.collectionView reloadData];
}

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    
}

#pragma mark - ======================== Update View ========================

//添加图片更新界面 (暂时注释 方法计划没有变，只是数组中放着nsstring)
//- (void)updateViewAfterAddImgs:(NSArray <UIImage *>*)imgs
//{
//    for (UIImage *tpImg in imgs) {
//        if (self.selectedImgCount == maxImgCount-1) {
//            JYImgSelectModel *model = self.imgsArr.lastObject;
//            model.isAddBtn = NO;
//            model.img = tpImg;
//            self.selectedImgCount++;
//        }
//        else
//        {
//            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
//            model.isAddBtn = NO;
//            model.img = tpImg;
//            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
//            self.selectedImgCount++;
//        }
//    }
//    [self.collectionView reloadData];
//}

////imgs 内部是逗号拼接的
//- (void)updateViewAfterAddImgs:(NSArray *)imgs{
////    NSArray * imageArr = [imgs componentsSeparatedByString:@","];
//    NSMutableArray * endImageArr = [NSMutableArray array];
////
//    for (NSString * url in imgs) {
//        if ([url hasPrefix:@"http"]) {
//            [endImageArr addObject:url];
//        }else{
//            NSString * httpUrl ;
//            httpUrl = SF(@"%@%@",JY_HTTP_PREFIX,url);
//            [endImageArr addObject:httpUrl];
//        }
//    }
//
//    for (NSString *imageUrl in endImageArr) {
//        if (self.selectedImgCount == maxImgCount-1) {
//            JYImgSelectModel *model = self.imgsArr.lastObject;
//            model.isAddBtn = NO;
//            //            model.img = tpImg;
//            model.imageUrl = imageUrl;
//            self.selectedImgCount++;
//        }
//        else
//        {
//            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
//            model.isAddBtn = NO;
//            //            model.img = tpImg;
//            model.imageUrl = imageUrl;
//            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
//            self.selectedImgCount++;
//        }
//    }
//    [self.collectionView reloadData];
//}



////删除图片更新界面
//- (void)updateViewAfterDelImgAtIndexPath:(NSIndexPath *)indexP
//{
//    //如果在选满图片之后删除
//    if (self.selectedImgCount == maxImgCount) {
//        [self.imgsArr removeObjectAtIndex:indexP.row];
//
//        JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
//        model.isAddBtn = YES;
//        [self.imgsArr addObject:model];
//    }
//
//    if (self.selectedImgCount < maxImgCount) {
//        [self.imgsArr removeObjectAtIndex:indexP.row];
//    }
//
//    self.selectedImgCount--;
//    [self.collectionView reloadData];
//}

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (JYPublishMomentViewModel *)rootVM
{
    if (!_rootVM) {
        _rootVM = [[JYPublishMomentViewModel alloc] init];
    }
    return _rootVM;
}

- (NSMutableArray *)imgsArr
{
    if (!_imgsArr) {
        _imgsArr = [[NSMutableArray alloc] init];
    }
    return _imgsArr;
}

- (JYCommunityViewModel *)communityVM
{
    if (!_communityVM) {
        _communityVM = [[JYCommunityViewModel alloc] init];
    }
    return _communityVM;
}

- (NSArray *)categoryArr
{
    if (!_categoryArr) {
        _categoryArr = [NSArray array];
    }
    return _categoryArr;
}

- (JYUploadImgsTool *)uploadViewModel
{
    if (!_uploadViewModel) {
        _uploadViewModel = [[JYUploadImgsTool alloc] init];
    }
    return _uploadViewModel;
}

- (NSMutableArray *)imgUrlArray
{
    if (!_imgUrlArray) {
        _imgUrlArray = [NSMutableArray array];
    }
    return _imgUrlArray;
}

- (NSMutableArray<UIImage *> *)picArr
{
    if (!_picArr) {
        _picArr = [NSMutableArray array];
    }
    return _picArr;
}

@end
