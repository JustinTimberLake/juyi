//
//  JYOilCardDetailViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOilCardDetailViewController.h"
#import "JYOilCardViewModel.h"
#import "JYCommonWebViewModel.h"
#import "JYCommonWebViewController.h"

static CGFloat  const BtnMargin = 15;
static CGFloat  const BtnWidth = 80;
static CGFloat  const BtnHeight = 34;

@interface JYOilCardDetailViewController ()
@property (nonatomic,strong) JYOilCardViewModel *oilCardViewModel;
@property (weak, nonatomic) IBOutlet UILabel *oilCardTypeLab;
@property (weak, nonatomic) IBOutlet UILabel *oilCardNumberLab;
@property (weak, nonatomic) IBOutlet UILabel *oilCardNameLab;
@property (weak, nonatomic) IBOutlet UILabel *oilCardPhoneLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewH;
@property (weak, nonatomic) IBOutlet UIImageView *wechatMarkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *alipayMarkImageView;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (nonatomic,strong) JYCommonWebViewModel *webViewModel;
@property (weak, nonatomic) IBOutlet UIView *moneyView;
@property (nonatomic,strong) NSArray *moneyArr;
@property (nonatomic,strong) NSMutableArray *moneyBtnArr;
@property (nonatomic,copy) NSString *selectBtnStr;
@property (nonatomic,assign) NSInteger payType; //支付方式





@end

@implementation JYOilCardDetailViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
    [self requestOilCardInfo];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"油卡详情";
//    创建按钮
    for (int i = 0; i < self.moneyArr.count; i++) {
        
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:SF(@"%@元",self.moneyArr[i]) forState:UIControlStateNormal];
        [btn setTitleColor:RGB0X(0x666666) forState:UIControlStateNormal];
        btn.titleLabel.font = FONT(14);
        btn.layer.cornerRadius = 3;
        btn.layer.borderColor = RGB0X(0xdbdbdb).CGColor;
        btn.layer.borderWidth = 0.5;
        btn.width = BtnWidth;
        btn.height =BtnHeight;
        btn.clipsToBounds = YES;
        btn.tag = 1000 + i;
        [btn addTarget:self action:@selector(moneyBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.moneyBtnArr addObject:btn];
        
        UIButton * newBtn = self.moneyBtnArr[i];
        if (i == 0) {
            newBtn.x = 0;
            newBtn.y = 0;
        }else{
            UIButton * previewBtn = self.moneyBtnArr[i - 1];
            CGFloat leftWidth = CGRectGetMaxX(previewBtn.frame) + BtnMargin;
            CGFloat rightWidth = self.moneyView.width - leftWidth;
            if (rightWidth >= BtnWidth) {
                newBtn.x = leftWidth;
                newBtn.y = previewBtn.y;
            }else{
                newBtn.x = 0;
                newBtn.y = BtnMargin + CGRectGetMaxY(previewBtn.frame);
            }
            
        }
        [self.moneyView addSubview:newBtn];

    }
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

- (void)moneyBtnAction:(UIButton *)btn{
    WEAKSELF
    [self.moneyBtnArr enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.tag != btn.tag) {
            [obj setTitleColor:RGB0X(0x666666) forState:UIControlStateNormal];
            [obj setBackgroundColor:[UIColor whiteColor]];
        }else{
            [obj setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [obj setBackgroundColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN]];
//            weakSelf.selectTag = obj.tag;
            weakSelf.selectBtnStr = self.moneyArr[obj.tag - 1000];
        }
    }];
}

//支付宝 1 。微信2
- (IBAction)payBtnAction:(UIButton *)sender {
    
    if (sender.tag == 1000) {
        self.wechatMarkImageView.image = [UIImage imageNamed:@"选中"];
        self.alipayMarkImageView.image = [UIImage imageNamed:@"未选中"];
        self.payType = 2;
    }else{
        self.wechatMarkImageView.image = [UIImage imageNamed:@"未选中"];
        self.alipayMarkImageView.image = [UIImage imageNamed:@"选中"];
        self.payType = 1;
    }
}

- (IBAction)rechargeBtnAction:(UIButton *)sender {
    if (!self.selectBtnStr.length) {
        [self showSuccessTip:@"请选择充值金额"];
        return;
    }else if (self.payType == 0){
        [self showSuccessTip:@"请选择支付方式"];
        return;
    }else if (!self.agreeBtn.selected){
        [self showSuccessTip:@"请勾选是否同意"];
        return;
    }else{
        [self showSuccessTip:@"开始充值"];
    }
}

- (IBAction)agreeBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
}

- (IBAction)userDelegateBtnAction:(UIButton *)sender {
    WEAKSELF
    [self.webViewModel requestGetContentUrlWithType:JYWebViewType_RechargeOilDelegate success:^(NSString *msg, id responseData) {
        NSString * contentUrl = responseData;
        JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
        webVC.url = contentUrl;
        webVC.navTitle = @"充值油库协议";
        [weakSelf.navigationController pushViewController:webVC animated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    self.moneyArr = @[@"50",@"100",@"200",@"500",@"1000"];
    self.moneyBtnArr = [NSMutableArray array];
}
- (void)requestOilCardInfo{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"oilCardId"] = self.oilCardId;
    [self.oilCardViewModel requestGetOilCardInfoWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf updataUI];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

- (void)updataUI{

    self.oilCardTypeLab.text = SF(@"油卡类型：%@", self.oilCardViewModel.oilCardInfoModel.oilCardTitle);
    self.oilCardNumberLab.text = SF(@"油卡卡号：%@", self.oilCardViewModel.oilCardInfoModel.oilCardNum);
    self.oilCardNameLab.text = SF(@"持卡人姓名：%@", self.oilCardViewModel.oilCardInfoModel.oilCardName);
    self.oilCardPhoneLab.text = SF(@"持卡人手机号：%@",self.oilCardViewModel.oilCardInfoModel.oilCardPhone);
    self.scrollViewH.constant = CGRectGetMaxY(self.agreeBtn.frame) + 30;
}

#pragma mark - ======================== Getter ========================

- (JYOilCardViewModel *)oilCardViewModel
{
    if (!_oilCardViewModel) {
        _oilCardViewModel = [[JYOilCardViewModel alloc] init];
    }
    return _oilCardViewModel;
}

- (JYCommonWebViewModel *)webViewModel
{
    if (!_webViewModel) {
        _webViewModel = [[JYCommonWebViewModel alloc] init];
    }
    return _webViewModel;
}


@end
