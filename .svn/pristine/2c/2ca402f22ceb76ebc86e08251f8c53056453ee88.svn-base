//
//  JYRegistViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRegistViewController.h"
#import "JYLoginViewController.h"
#import "JYRegistViewModel.h"
#import "JYGetCodeViewModel.h"
#import <libkern/OSAtomic.h>
#import "JYCommonWebViewController.h"
#import "JYAlertPicViewController.h"

@interface JYRegistViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;

@property (nonatomic,strong) JYRegistViewModel *viewModel;
@property (nonatomic,strong) JYGetCodeViewModel *getCodeViewModel;
@end

@implementation JYRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    
}

#pragma mark ==================设置UI==================

- (void)setUpUI{
    WEAKSELF
    self.naviTitle = @"注册";
    self.view.backgroundColor = [UIColor whiteColor];
    [self rightItemTitle:@"登录" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:FONT(15) action:^{
        [weakSelf turnLoginVC];
    }];
//    [self hideBackNavItem];
}

- (void)turnLoginVC{
    JYLoginViewController * loginVC = [[JYLoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

//注册请求
- (void)requestWithRegist{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"phone"] = self.phoneTextField.text;
    dic[@"password"] = self.confirmPasswordTextField.text;
    dic[@"code"] = self.codeTextField.text;
    [self.viewModel requestRegistWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"注册成功,请登录"];
        [weakSelf turnLoginVC];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ==================Action==================
- (IBAction)sendCodeBtnAction:(UIButton *)sender {
    if (!self.phoneTextField.text.length) {
        [self showSuccessTip:@"请输入手机号"];
        return;
    }
    if (![self.phoneTextField.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    
    sender.enabled = NO;
    __block int32_t timeOutCount = 60;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1ull * NSEC_PER_SEC, 0);
    dispatch_source_set_cancel_handler(timer, ^{
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
    });
    dispatch_resume(timer);
    
    WEAKSELF
    [self.getCodeViewModel requestGetCodeWithPhoneNumber:self.phoneTextField.text andType:1 success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"已发送您的手机，请查收"];
        dispatch_source_set_event_handler(timer, ^{
            OSAtomicDecrement32(&timeOutCount);
            [sender setTitle:SF(@"(%ds)后重试", timeOutCount)  forState:UIControlStateDisabled];
            [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_LINE_HEAD] forState:UIControlStateDisabled];
            if (timeOutCount == 0) {
                sender.enabled = YES;
                NSLog(@"timersource cancel");
                dispatch_source_cancel(timer);
            }
        });
        
    } failure:^(NSString *errorMsg) {
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
        [weakSelf showSuccessTip:errorMsg];
    }];

}
- (IBAction)agreeBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

//用户协议
- (IBAction)userProtrolBtnAction:(UIButton *)sender {
    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
    webVC.navTitle = @"用户协议";
    webVC.webViewType = JYWebViewType_UserDelegate;
    [self.navigationController pushViewController:webVC animated:YES];
}

- (IBAction)registBtnAction:(UIButton *)sender {
    if (!self.phoneTextField.text.length) {
        [self showSuccessTip:@"请输入手机号"];
        return;
    }
    if (![self.phoneTextField.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    if (!self.codeTextField.text.length) {
        [self showSuccessTip:@"请输入验证码"];
        return;
    }
    if (![self.codeTextField.text checkJustNumber]) {
        [self showSuccessTip:@"请输入正确的验证码"];
        return;
    }
    
    if (self.passwordTextField.text.length < 6) {
        [self showSuccessTip:@"密码不应小于6位"];
        return;
    }
    if (![self.passwordTextField.text checkEnglishAndNum]) {
        [self showSuccessTip:@"密码为字母和数字组合"];
        return;
    }
    if (![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text] ) {
        [self showSuccessTip:@"确认密码输入有误"];
        return;
    }
    if (!self.agreeBtn.isSelected) {
        [self showAlert];
        return;
    }
    
    [self requestWithRegist];
}

//弹窗显示
- (void)showAlert{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    alertVC.contentText = @"请先同意\n用户使用协议";
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^{
        weakSelf.agreeBtn.selected = YES;
    };
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (JYRegistViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYRegistViewModel alloc] init];
    }
    return _viewModel;
}

- (JYGetCodeViewModel *)getCodeViewModel
{
    if (!_getCodeViewModel) {
        _getCodeViewModel = [[JYGetCodeViewModel alloc] init];
    }
    return _getCodeViewModel;
}


@end
