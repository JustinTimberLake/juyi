//
//  JYServiceOrderDetailViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYServiceOrderDetailViewController.h"
#import "JYServiceHeaderTableViewCell.h"
#import "JYServiceDetailTableViewCell.h"
#import "JYQRCodeServiceDetailTableViewCell.h"
#import "JYServiceOrderStatusTableViewCell.h"
#import "JYMyOrderViewModel.h"
#import "JYPayViewModel.h"
#import "JYAlipayClient.h"
#import "JYPaySuccessViewController.h"
#import "JYBaseTabBarController.h"
#import "JYApplyRefundViewModel.h"
#import "JYAlertPicViewController.h"

typedef NS_ENUM(NSInteger,OrderState) {
    OrderState_ReadyPay,//待支付
    OrderState_ReadyUse,//待使用
    OrderState_Other//其他状态，评价，关闭，完成，
};

@interface JYServiceOrderDetailViewController ()<
    UITableViewDataSource,
    UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) JYMyOrderViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;
@property (nonatomic,assign) OrderState orderState; //订单状态
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftBtn;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightBtn;

@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (nonatomic,strong) JYApplyRefundViewModel *applyRefundViewModel;

//// 删除、取消、退款 服务订单类型
//@property (nonatomic,assign) DelServiceOrderType delorderType;

@end

@implementation JYServiceOrderDetailViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JYMyOrderViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyOrderViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self networkRequest];
}

//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)backAction{
    if (self.checkMode) {
        JYBaseTabBarController * tabBarVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        tabBarVC.selectedIndex = 3;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"订单详情";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceHeaderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceHeaderTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceDetailTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYQRCodeServiceDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYQRCodeServiceDetailTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceOrderStatusTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceOrderStatusTableViewCell"];
    
    self.bottomLeftBtn.hidden = YES;
    
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    dict[@"orderId"] = self.orderId;
    [self.viewModel requesMyServiceOrderDetailWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf updataOrderState];
        [weakSelf updataBottomUI];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
    }];

}

// 删除、取消 服务订单
- (void)requestDeleteOrderWithOrderId:(NSString *)orderId andType:(DelOrderType)type{
    WEAKSELF
    [self.viewModel requestMyServiceDelServiceOrderWithOrderId:orderId andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//未发货，退款
- (void)requestApplyRefundWithOrderId:(NSString *)orderId andType:(JYApplyRefundType)type{
    WEAKSELF
    [self.applyRefundViewModel requestApplyRefundWithNotSendProductWithOrderId:orderId andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf networkRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//更新订单状态
- (void)updataOrderState{
    if ([self.viewModel.serviceDetail.orderState intValue] == 1) {
        self.orderState = OrderState_ReadyPay;
    }else if ([self.viewModel.serviceDetail.orderState intValue] == 2){
        self.orderState = OrderState_ReadyUse;
    }else{
        self.orderState = OrderState_Other;
    }
}

//更新底部UI和按钮状态
- (void)updataBottomUI{
    self.totalPriceLab.text = SF(@"%@",self.viewModel.serviceDetail.totalPrice);
    self.bottomLeftBtn.hidden = self.orderState == OrderState_ReadyUse ? NO:YES;
    switch ([self.viewModel.serviceDetail.orderState intValue]) {
        case 1:
            [self.bottomRightBtn setTitle:@"去支付" forState:UIControlStateNormal];
            break;
        case 2:
            [self.bottomRightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
            break;
        case 3:
            [self.bottomRightBtn setTitle:@"去评价" forState:UIControlStateNormal];
            break;
        case 5:{
            [self.bottomRightBtn setTitle:@"已关闭" forState:UIControlStateNormal];
            [self.bottomRightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
}

- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId andTotalPrice:(NSString *)totalPrice{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalPrice floatValue]);
    
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalPrice,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}


#pragma mark actions （点击事件）
- (IBAction)bottomBtnAction:(UIButton *)sender {
    WEAKSELF
    if (sender.tag == 1000) {
        [self requestDeleteOrderWithOrderId:self.viewModel.serviceDetail.orderId andType:DelOrderType_Del];
    }else{
        sender.enabled = [sender.currentTitle isEqualToString:@"已关闭"] ?NO :YES;
        if ([sender.currentTitle isEqualToString:@"去支付"]) {
            [self showAlertWithPickerViewType:PickerViewTypePay];
            self.returnPayTypeBlock = ^(NSString *str) {
                [weakSelf payTypeWithStr:str andOrderId:weakSelf.orderId];
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                if (weakSelf.orderId.length) {
                    dic[@"orderId"] = weakSelf.orderId;
                    dic[@"orderType"] = @"服务订单";
                }
                JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
                

            };

        }else if ([sender.currentTitle isEqualToString:@"去评价"]){
            
        }else if ([sender.currentTitle isEqualToString:@"申请退款"]){
            
            [self requestApplyRefundWithOrderId:self.viewModel.serviceDetail.orderId andType:JYApplyRefundType_Service];
//            废弃
//            [self requestDeleteOrderWithOrderId:self.viewModel.serviceDetail.orderId andType:DelOrderType_Refund];
        }
    }
}


#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
       switch (indexPath.section) {
        case 0:{
            JYServiceHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceHeaderTableViewCell"];
            cell.model = self.viewModel.serviceDetail;
            return cell;
            
        }
            break;
        case 1:{
            JYServiceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceDetailTableViewCell"];
            cell.model = self.viewModel.serviceDetail;
            return cell;
        }
            break;
            
        case 2:{
            if (self.orderState == OrderState_ReadyUse) {
                JYQRCodeServiceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYQRCodeServiceDetailTableViewCell"];
                cell.model = self.viewModel.serviceDetail;
                return cell;
            }else{
                JYServiceOrderStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceOrderStatusTableViewCell"];
                cell.model = self.viewModel.serviceDetail;
                return cell;

            }
        }
            
        case 3:{
            if (self.orderState == OrderState_ReadyUse) {
                JYServiceOrderStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceOrderStatusTableViewCell"];
                cell.model = self.viewModel.serviceDetail;
                return cell;
            }else{
                return nil;
            }
        }

            break;
        default:
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    这种写法勿删
//    if (self.orderState == OrderState_ReadyUse) {
//        switch (indexPath.section) {
//            case 0:
//                return 190;
//                break;
//            case 1:
//                return 203;
//                break;
//            case 2:
//                return 316;
//                break;
//            case 3:
//                return 125;
//            default:
//                break;
//        }
//
//    }else if (self.orderState == OrderState_ReadyPay){
//        switch (indexPath.section) {
//            case 0:
//                return 190;
//                break;
//            case 1:
//                return 203;
//                break;
//            case 2:
//                return 90;
//                break;
//            default:
//                break;
//        }
//    }else{
//        switch (indexPath.section) {
//            case 0:
//                return 190;
//                break;
//            case 1:
//                return 203;
//                break;
//            case 2:
//                return 90;
//                break;
//            default:
//                break;
//        }
//    }
    switch (indexPath.section) {
        case 0:
            return 190;
            break;
        case 1:
            return 203;
            break;
        case 2:{
            if (self.orderState == OrderState_ReadyUse) {
                return 316;
            }else if(self.orderState == OrderState_ReadyPay){
                return 90;
            }else{
                return 125;
            }
        }
            break;
        case 3:{
            if (self.orderState == OrderState_ReadyUse) {
                return 90;
            }else{
                return 0;
            }
        }
        default:
            break;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.orderState == OrderState_ReadyUse) {
        return 4;
    }else{
        return 3;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_GRAY_BG];
    return view;
}

#pragma mark ==================支付代码==================

//支付方式选择
- (void)payTypeWithStr:(NSString *)str andOrderId:(NSString *)orderId{
    if ([str isEqualToString:@"账户支付"]) {
        NSLog(@"账户支付");
        if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [self.totalPriceLab.text floatValue]) {
            [self showAlertPicViewWithAccountTypeWithOrderId:orderId andTotalPrice:self.viewModel.serviceDetail.totalPrice];;
            
        }else{
            [self showAlertPicViewWithNotEnoughMoney];
        }
    }else if([str isEqualToString:@"微信支付"]){
        NSLog(@"微信支付");
             [self requestWXSignWithOrderNum:orderId];
    }else{
        NSLog(@"支付宝支付");
        [self requestAlipaySignWithOrderNum:orderId];
    }
}


//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        //        [weakSelf showAlertPicViewWithNotEnoughMoney];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
}



//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            //            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
//                switch ([code intValue]) {
//                    case 9000:
//                        //                        [self popToRootViewControllerAnimated:NO];
//                    {
//                        [weakSelf turnPaySuccessPage];
//                    }
//                        break;
//                    case 8000:
//                        //                        [self showSuccessTip:@"正在处理中"];
//                        break;
//                    case 4000:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                    case 6001:
//                        //                        [self showSuccessTip:@"支付已取消"];
//                        break;
//                    case 6002:
//                        //                        [self showSuccessTip:@"网络连接错误"];
//                        break;
//                    default:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                }
                
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//跳转到支付成功页面
- (void)turnPaySuccessPage{
    //    UIApplication *app = [UIApplication sharedApplication];
    //    JYBaseTabBarController *tabbar = (JYBaseTabBarController*)app.delegate.window.rootViewController;
    //    UINavigationController *nav= tabbar.viewControllers[tabbar.selectedIndex];
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    [self.navigationController pushViewController:successVC animated:YES];
}


- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}

- (JYApplyRefundViewModel *)applyRefundViewModel
{
    if (!_applyRefundViewModel) {
        _applyRefundViewModel = [[JYApplyRefundViewModel alloc] init];
    }
    return _applyRefundViewModel;
}


@end
