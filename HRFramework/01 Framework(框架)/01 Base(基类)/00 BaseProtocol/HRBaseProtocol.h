
/*~!
 | @FUNC  框架基协议
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  所有类遵守此协议
 */

#import <Foundation/Foundation.h>

@protocol HRBaseProtocol <NSObject>

@optional
#pragma mark 初始化数据
//1.0 初始化数据
- (void)initializeData;

#pragma mark 布局UI
//2.1 布局控件
- (void)configUI;
//2.2 刷新控件
- (void)refreshUI;

@end
