
/*~!
 | @FUNC  标签基类控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HRTabBarItemStatus) {
    HRTabBarItemStatusSelected, //选中状态
    HRTabBarItemStatusDefault, //默认状态
};

@interface HRBaseTabBarController : UITabBarController

//1.0 初始化（控制器名称数组）
- (instancetype)initWithChildControllerNames:(NSArray<NSString *> *)names;

//2.1.0 标签标题
- (void)itemTitles:(NSArray <NSString *>*)titles;
- (HRBaseTabBarController *(^)(NSArray <NSString *>*titles))setupItemTitles;

//2.1.1 标签标题颜色 [统一]
- (void)itemsTitleColor:(UIColor *)color status:(HRTabBarItemStatus)status;
- (HRBaseTabBarController *(^)(UIColor *))setupItemsTitleDefaultColor;
- (HRBaseTabBarController *(^)(UIColor *))setupItemsTitleSelectedColor;

//2.1.2 标签标题颜色 [不统一]
- (void)itemTitleColor:(UIColor *)color atIndex:(NSInteger)idx status:(HRTabBarItemStatus)status;



@end
