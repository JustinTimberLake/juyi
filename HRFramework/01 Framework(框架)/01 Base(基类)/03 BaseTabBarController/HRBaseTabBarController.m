

#import "HRBaseTabBarController.h"

@interface HRBaseTabBarController ()

@end

@implementation HRBaseTabBarController

#pragma mark - ---------- Lifecycle ----------

//1.0 初始化（控制器名称数组）
- (instancetype)initWithChildControllerNames:(NSArray<NSString *> *)names{
    if (self = [super init]) {
        [self configChildControllerWithNames:names];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma mark - ---------- Private Methods ----------

//- (void)configItems:(NSArray <HRTabBarItem *>*)items {
//    NSMutableArray <UIViewController *>*childControllers = [NSMutableArray array];
//    [items enumerateObjectsUsingBlock:^(HRTabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        // 视图控制器
//        Class c = NSClassFromString(obj.childControllerName);
//        UIViewController *vc = [[c alloc] init];
//        [childControllers addObject:vc];
//        // 图片
//        vc.tabBarItem.image = [[UIImage imageNamed:obj.defaultImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        vc.tabBarItem.selectedImage = [[UIImage imageNamed:obj.selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//        // 标题
//        vc.title = obj.title;
//        // 标题颜色
//        [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:obj.defaultTitleColor,NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
//        [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:obj.selectedImageName,NSForegroundColorAttributeName, nil] forState:UIControlStateDisabled];
//        // 图片边距
//        vc.tabBarItem.imageInsets = obj.imageInsets;
//    }];
//    [self setViewControllers:childControllers animated:YES];
//}
//根据名字初始化子视图控制器
- (void)configChildControllerWithNames:(NSArray <NSString *>*)names {
    NSMutableArray *childControllers = [NSMutableArray array];
    [names enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Class c = NSClassFromString(obj);
        UIViewController *vc = [[c alloc] init];
        [childControllers addObject:vc];
    }];
    [self setViewControllers:childControllers];
}


#pragma mark - ---------- Public Methods ----------
//2.1.0 标签标题
- (void)itemTitles:(NSArray <NSString *>*)titles {
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.tabBarItem.title = titles[idx];
    }];
}
//2.1.0 标签标题(链式)
- (HRBaseTabBarController *(^)(NSArray <NSString *>*titles))setupItemTitles {
    return ^(NSArray <NSString *>*titles) {
        [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.tabBarItem.title = titles[idx];
        }];
        return self;
    };
}

//2.1.1 标签标题未选中状态颜色
- (void)itemsTitleColor:(UIColor *)color status:(HRTabBarItemStatus)status {
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIControlState state = (status == HRTabBarItemStatusSelected) ? UIControlStateSelected : UIControlStateNormal;
        [obj.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName, nil] forState:state];
    }];
}
- (HRBaseTabBarController *(^)(UIColor *))setupItemsTitleDefaultColor {
    return ^(UIColor *color) {
        [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
        }];
        return self;
    };
}
- (HRBaseTabBarController *(^)(UIColor *))setupItemsTitleSelectedColor {
    return ^(UIColor *color) {
        [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
        }];
        return self;
    };
}

//2.1.2 标签标题颜色 [不统一]
- (void)itemTitleColor:(UIColor *)color atIndex:(NSInteger)idx status:(HRTabBarItemStatus)status {
    UIViewController *vc = self.childViewControllers[idx];
    UIControlState state = (status == HRTabBarItemStatusDefault) ? UIControlStateNormal : UIControlStateSelected;
    [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName, nil] forState:state];
}

@end
