

#import "HRRequestManager.h"
#import "SNStarsAlertView.h"
@implementation HRRequestManager

#pragma mark - ---------- Lifecycle ----------

#pragma mark - ---------- Public Methods ----------
#pragma mark GET请求
- (void)GET_URL:(NSString *)url params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    [self requestWay:HRHttpRequestMethodGet url:url params:params progress:nil success:success failure:failure];
}

#pragma mark POST请求
- (void)POST_URL:(NSString *)url params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    [self requestWay:HRHttpRequestMethodPost url:url params:params progress:nil success:success failure:failure];
}

#pragma mark 表单请求
- (void)FORM_URL:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    [self requestWay:HRHttpRequestMethodForm url:url params:params progress:progress success:success failure:failure];
}

#pragma mark - ---------- Private Methods ----------
//处理请求
- (void)requestWay:(HRHttpRequestMethod)method url:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    self.requestUrl = url;
    self.requestParams = params;
    [self requestProgress:^(NSProgress *p) {
        //进度回调
        if (progress) {
            progress(p);
        }
    } success:^(id responseObject) {
        if (success) {
            NSDictionary * dic = responseObject;
            NSString * msg = dic[@"errorMsg"];
            if ([msg isEqualToString:@"登录异常"]) {
                [[User_InfoShared shareUserInfo] clearData];
                JY_POST_NOTIFICATION(JY_NOTI_LOGOUT, nil);
                SNStarsAlertView *alert = [[SNStarsAlertView alloc]initWithTitle:@"提醒" message:@"登录异常请重新登录" cancelButtonTitle:@"取消" otherButtonTitle:@"确定" cancelButtonClick:nil otherButtonClick:^{
                    JY_POST_NOTIFICATION(JY_NOTI_LOGIN_ERROE, nil);
                }];
                [alert show];
                
            }

            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error.userInfo);
        }
    }];
}

@end
