

#import "HRBaseRequest.h"
#import "AFNetworking.h"
#import "NSDictionary+UnicodeToChinese.h"

@interface HRBaseRequest ()

//网络请求
@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;
//成功block
@property (copy, nonatomic) void(^successBlock)(id responseObject);
//失败block
@property (copy, nonatomic) void(^failureBlock)(NSError *error);

@end

@implementation HRBaseRequest

#pragma mark - ---------- Lazy Loading ----------

- (AFHTTPSessionManager *)sessionManager {
    if (!_sessionManager) {
        _sessionManager = [AFHTTPSessionManager manager];
        _sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    }
    return _sessionManager;
}

#pragma mark - ---------- Lifecycle ----------

#pragma mark - ---------- Lifecycle ----------

- (instancetype)init {
    self = [super init];
    if (self) {
        //初始化各属性默认参数
        _requestType = HRRequestModeHttp;
        _requestWay = HRHttpRequestWayAsync;
        _requestMethod = HRHttpRequestMethodPost;
        _requestUrl = @"";
        _requestParams = @{};
        _requestTimeOut = 10.f;
        _requestCryptType = HRRequestCryptTypeNone;
        
        //清空缓存信息
        _error = nil;
        //默认未开始监测
        _netStatus = HRNetStatusNotMoniter;
        //默认打印日志
        _showLog = YES;
    }
    return self;
}

#pragma mark - ---------- Public Methods ----------
//发送请求
- (void)requestProgress:(void(^)(NSProgress *progress))progress success:(void(^)(id responseObject))success failure:(void(^)(NSError *error))failure {
    
    self.progressBlock = progress;
    self.successBlock = success;
    self.failureBlock = failure;
    //刷新请求超时时间
    self.sessionManager.requestSerializer.timeoutInterval = self.requestTimeOut;
    //请求类型（http\socket）
    if (self.requestType == HRRequestModeHttp) {
        [self http];
    } else {
        [self socket];
    }
}

#pragma mark - ---------- Private Methods ----------
//短链接
- (void)http {
    //请求方式（同步\异步）
    if (self.requestWay == HRHttpRequestWayAsync) {
        [self async];
    } else {
        [self sync];
    }
}
//长链接
- (void)socket {
    //长链接 预留功能
}
//异步
- (void)async {
    //请求方法(GET\POST\FORM)
    switch (self.requestMethod) {
        case HRHttpRequestMethodGet: {
            [self GET];
        }
            break;
        case HRHttpRequestMethodPost: {
            [self POST];
        }
            break;
        case HRHttpRequestMethodForm: {
            [self FORM];
        }
            break;
    }
}
//同步
- (void)sync {
    //同步。预留功能
}
//GET
- (void)GET {
    [self.sessionManager GET:self.requestUrl parameters:self.requestParams progress:^(NSProgress * _Nonnull downloadProgress) {
        if (self.progressBlock) {
            self.progressBlock(downloadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.successBlock(responseObject);
    
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull   error) {
        self.failureBlock(error);
    }];
}
//POST
- (void)POST {
    [self.sessionManager POST:self.requestUrl parameters:self.requestParams progress:^(NSProgress * _Nonnull uploadProgress) {
        if (self.progressBlock) {
            self.progressBlock(uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        id result = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (self.successBlock) {
            self.successBlock(result);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        _error = error;
        if (self.failureBlock) {
            self.failureBlock(error);
        }
    }];
}
//FORM
- (void)FORM {
    //表单 预留功能
}


@end
