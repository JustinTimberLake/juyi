
/*~!
 | @FUNC  打电话
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <Foundation/Foundation.h>
#import "HRURLScheme.h"

@interface HRCall : HRURLScheme

//1.0 拨打电话
+ (void)callPhoneNumber:(NSString *)phoneNumber alert:(BOOL)alert;

@end
