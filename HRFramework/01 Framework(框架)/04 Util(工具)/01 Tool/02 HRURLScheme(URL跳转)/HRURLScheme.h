
/*~!
 | @FUNC  URL跳转
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  iOS 10.0.2 港版设备出现url可能出现无法跳转问题，系统bug
 */

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, HRSettingItem) {
    HRSettingsHome,             //设置
    HRSettingsAirPlane,         //飞行模式
    HRSettingsWifi,             //Wifi
    HRSettingsBluetooth,        //蓝牙
    HRSettingsHotSpot,          //个人热点
    HRSettingsNotification,     //通知
    HRSettingsGeneral,          //通用
    HRSettingsGeneralAbout,     //通用／关于本机
    HRSettingsLocationService,  //隐私／定位服务
    HRSettingsPhone,            //电话
};

@interface HRURLScheme : NSObject

//0.1 初始化
+ (instancetype)manager;

//1.0 访问本app设置界面
+ (void)openSelfSetting;
//1.1 访问设置页面或子页面
+ (void)openSettingItem:(HRSettingItem)item;
//1.2 访问系统app（自定义urlScheme，含设置及其子界面）
+ (void)openSystemApp:(NSString *)urlScheme;
//1.3 访问其他app (系统原因，暂时限制使用)
+ (void)openOtherApp:(NSString *)urlScheme;


/*
 
 自定义 urlScheme 选项
 
 关于本机	prefs:root=General&path=About
 辅助功能	prefs:root=General&path=ACCESSIBILITY
 飞行模式	prefs:root=AIRPLANE_MODE
 自动锁定	prefs:root=General&path=AUTOLOCK
 蓝牙	prefs:root=Bluetooth
 日期与时间	prefs:root=General&path=DATE_AND_TIME
 FaceTime	prefs:root=FACETIME
 通用	prefs:root=General
 键盘	prefs:root=General&path=Keyboard
 iCloud	prefs:root=CASTLE
 iCloud存储空间	prefs:root=CASTLE&path=STORAGE_AND_BACKUP
 语言与地区	prefs:root=General&path=INTERNATIONAL
 定位服务	prefs:root=LOCATION_SERVICES
 邮件、通讯录、日历	prefs:root=ACCOUNT_SETTINGS
 音乐	prefs:root=MUSIC
 音乐	prefs:root=MUSIC&path=EQ
 音乐	prefs:root=MUSIC&path=VolumeLimit
 备忘录	prefs:root=NOTES
 通知	prefs:root=NOTIFICATIONS_ID
 电话	prefs:root=Phone
 照片与相机	prefs:root=Photos
 描述文件	prefs:root=General&path=ManagedConfigurationList
 还原	prefs:root=General&path=Reset
 电话铃声	prefs:root=Sounds&path=Ringtone
 Safari	prefs:root=Safari
 声音	prefs:root=Sounds
 软件更新	prefs:root=General&path=SOFTWARE_UPDATE_LINK
 App Store	prefs:root=STORE
 Twitter	prefs:root=TWITTER
 视频	prefs:root=VIDEO
 VPN	prefs:root=General&path=VPN
 墙纸	prefs:root=Wallpaper
 WiFi	prefs:root=WIFI
 个人热点	prefs:root=INTERNET_TETHERING
 */


@end
