

#import "HRURLScheme.h"
#import <UIKit/UIKit.h>

@implementation HRURLScheme

//0.1 初始化
+ (instancetype)manager {
    return [[HRURLScheme alloc] init];
}

//1.0 访问本app设置界面
+ (void)openSelfSetting {
    OPEN_URL(UIApplicationOpenSettingsURLString);
}

//访问设置页面或子页面
+ (void)openSettingItem:(HRSettingItem)item {
    NSString *url = @"";
    switch (item) {
        case HRSettingsHome:
            url = @"Prefs:root=SETTING";
            break;
        case HRSettingsAirPlane:
            url = @"Prefs:root=AIRPLANE_MODE";
            break;
        case HRSettingsWifi:
            url = @"Prefs:root=WIFI";
            break;
        case HRSettingsBluetooth:
            url = @"Prefs:root=Bluetooth";
            break;
        case HRSettingsHotSpot:
            url = @"Prefs:root=INTERNET_TETHERING";
            break;
        case HRSettingsNotification:
            url = @"Prefs:root=NOTIFICATIONS_ID";
            break;
        case HRSettingsGeneral:
            url = @"Prefs:root=General";
            break;
        case HRSettingsGeneralAbout:
            url = @"Prefs:root=General&path=About";
            break;
        case HRSettingsPhone:
            url = @"Prefs:root=Phone";
            break;
        case HRSettingsLocationService:
            url = @"Prefs:root=LOCATION_SERVICES";
            break;
    }
    
    if (!OPEN_URL(url)) {
        NSLog(@"\"%@\"-URL无法访问", url);
    }
}
//访问系统app（含系统设置界面等）
+ (void)openSystemApp:(NSString *)urlScheme {
    if (!OPEN_URL(urlScheme)) {
        NSLog(@"\"%@\"-URL无法访问", urlScheme);
    }
}
//访问其他app
+ (void)openOtherApp:(NSString *)urlScheme {
    NSString *urlStr = [NSString stringWithFormat:@"%@://", urlScheme];
    if (!OPEN_URL(urlStr)) {
        NSLog(@"\"%@\"-URL无法访问", urlStr);
    }
}

@end








