
/*~!
 | @FUNC  发邮件
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <Foundation/Foundation.h>
#import "HRURLScheme.h"

typedef NS_ENUM(NSUInteger, HREamilStatus) {
    HREamilCancelStatus, // 取消发送
    HREamilSaveStatus, // 已保存
    HREamilSendedStatus, // 已发送
    HREamilFailStatus // 发送失败
};

@interface HREmail : HRURLScheme

//1.0 发送邮件（在邮件界面自己编辑信息）
+ (void)receiver:(NSString *)emailAddress;

/*!
 *  @author muyingbo, 16-07-29 17:07:37
 *
 *  @brief 发送至多人（也可以一人、发送完返回app）
 *
 *  @param receivers 收件人
 *  @param copiers   抄送
 *  @param secreters 密送
 *  @param subject   主题
 *  @param message   内容
 *  @param completed 完成回调
 */
- (void)receivers:(NSArray <NSString *>*)receivers copy:(NSArray <NSString *>*)copiers secret:(NSArray <NSString *>*)secreters subject:(NSString *)subject message:(NSString *)message completed:(void(^)(HREamilStatus status))completed;


@end
