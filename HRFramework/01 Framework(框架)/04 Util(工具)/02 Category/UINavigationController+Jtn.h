//
//  UINavigationController+Jtn.h
//  Grain
//
//  Created by Justin on 2017/12/27.
//  Copyright © 2017年 东方银谷. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Jtn)
//右滑动返回是否跳转到跟试图
@property (nonatomic, assign) BOOL zj_PopToRoot;

@end
