//
//  SNStarsAlertView.h
//  allstars
//
//  Created by 紫色ぃ年糕 on 2016/10/21.
//  Copyright © 2016年 全民歌星(北京)科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AlertBtnClick)();

@interface SNStarsAlertView : UIView

/// 左侧按钮
@property (nonatomic, strong) UIButton *cancelBtn;

/// 右侧按钮
@property (nonatomic, strong) UIButton *otherBtn;

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
             otherButtonTitle:(NSString *)otherButtonTitle
            cancelButtonClick:(AlertBtnClick)cancelButtonClick
             otherButtonClick:(AlertBtnClick)otherButtonClick;

- (void)show;

-(void)showWithFire;

- (void)hidden;

@end
