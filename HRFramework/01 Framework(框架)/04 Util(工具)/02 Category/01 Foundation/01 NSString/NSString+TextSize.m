//
//  NSString+TextSize.m
//  ZK
//
//  Created by Stronger_WM on 2017/3/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "NSString+TextSize.h"

@implementation NSString (TextSize)

- (CGFloat)stringWidthAtHeight:(CGFloat)height font:(UIFont *)aFont
{
    return [NSString widthOfText:self textHeight:height font:aFont];
}

- (CGFloat)stringHeightAtWidth:(CGFloat)width font:(UIFont *)aFont
{
    return [NSString heightOfText:self textWidth:width font:aFont];
}

+ (CGFloat)heightOfText:(NSString *)aString textWidth:(CGFloat)width font:(UIFont *)aFont
{
    CGRect result_rect;
    
    result_rect = [aString boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:aFont} context:nil];
    
    CGFloat height;
    height = result_rect.size.height;
    
    return height;
}

+ (CGFloat)widthOfText:(NSString *)aString textHeight:(CGFloat)height font:(UIFont *)aFont
{
    CGRect result_rect;
    
    result_rect = [aString boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:aFont} context:nil];
    
    CGFloat width;
    width = result_rect.size.width;
    
    return width;
}

@end
