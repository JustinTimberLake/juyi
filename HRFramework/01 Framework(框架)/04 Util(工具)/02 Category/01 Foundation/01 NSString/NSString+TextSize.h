//
//  NSString+TextSize.h
//  ZK
//
//  Created by Stronger_WM on 2017/3/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//  计算文本尺寸

#import <Foundation/Foundation.h>

@interface NSString (TextSize)

+ (CGFloat)widthOfText:(NSString *)aString textHeight:(CGFloat)height font:(UIFont *)aFont;
+ (CGFloat)heightOfText:(NSString *)aString textWidth:(CGFloat)width font:(UIFont *)aFont;

- (CGFloat)stringWidthAtHeight:(CGFloat)height font:(UIFont *)aFont;
- (CGFloat)stringHeightAtWidth:(CGFloat)width font:(UIFont *)aFont;

@end
