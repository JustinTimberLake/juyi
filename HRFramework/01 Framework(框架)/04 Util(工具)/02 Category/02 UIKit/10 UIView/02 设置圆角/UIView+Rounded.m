
#import "UIView+Rounded.h"

@implementation UIView (Rounded)

//1.0 设置圆角
- (void)roundedCorners:(CGFloat)radius {
    UIBezierPath *berzierPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:radius];
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.frame = self.bounds;
    shapeLayer.path = berzierPath.CGPath;
    self.layer.mask = shapeLayer;
}

@end
