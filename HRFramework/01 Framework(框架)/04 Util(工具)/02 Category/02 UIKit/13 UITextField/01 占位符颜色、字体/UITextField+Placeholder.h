
/*~!
 | @FUNC  占位符
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <UIKit/UIKit.h>

@interface UITextField (Placeholder)

//1.0 修改placeholder文本颜色
- (void)placeholerColor:(UIColor *)color;

//1.1 修改placeholder字体
- (void)placeholerFont:(UIFont *)font;

@end
