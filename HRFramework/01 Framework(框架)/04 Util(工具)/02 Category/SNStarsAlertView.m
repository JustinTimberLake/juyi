//
//  SNStarsAlertView.m
//  allstars
//
//  Created by 紫色ぃ年糕 on 2016/10/21.
//  Copyright © 2016年 全民歌星(北京)科技有限公司. All rights reserved.
//

#import "SNStarsAlertView.h"
#import "Masonry.h"
@interface SNStarsAlertView ()

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *messageLbl;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *message;

@property (nonatomic, copy) NSString *cancelButtonTitle;

@property (nonatomic, copy) NSString *otherButtonTitle;

@property (nonatomic, copy) AlertBtnClick cancelButtonClick;

@property (nonatomic, copy) AlertBtnClick otherButtonClick;

@property (nonatomic, weak) UIView *lastView;

@property (nonatomic, strong) CAEmitterLayer *caELayer;

//@property (nonatomic, strong)
@end

@implementation SNStarsAlertView
#pragma mark - Override Function
- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
             otherButtonTitle:(NSString *)otherButtonTitle
            cancelButtonClick:(AlertBtnClick)cancelButtonClick
             otherButtonClick:(AlertBtnClick)otherButtonClick {
    if (self = [super initWithFrame:CGRectZero]) {
        self.title = title;
        self.message = message;
        self.cancelButtonTitle = cancelButtonTitle;
        self.otherButtonTitle = otherButtonTitle;
        self.cancelButtonClick = cancelButtonClick;
        self.otherButtonClick = otherButtonClick;
        
        [self initUI];
    }
    return self;
}

- (void)show {
    UIView *lastView = [UIApplication sharedApplication].keyWindow.subviews.lastObject;
    if ([lastView isKindOfClass:[SNStarsAlertView class]]) {
        self.lastView = lastView;
        [self.lastView setHidden:YES];
    }
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.offset(0);
    }];
}

-(void)showWithFire{
    
    UIView *lastView = [UIApplication sharedApplication].keyWindow.subviews.lastObject;
    if ([lastView isKindOfClass:[SNStarsAlertView class]]) {
        self.lastView = lastView;
        [self.lastView setHidden:YES];
    }
    
    self.caELayer                   = [CAEmitterLayer layer];
    // 发射源
    self.caELayer.emitterPosition   = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT - 50);
    // 发射源尺寸大小
    self.caELayer.emitterSize       = CGSizeMake(50, 0);
    // 发射源模式
    self.caELayer.emitterMode       = kCAEmitterLayerOutline;
    // 发射源的形状
    self.caELayer.emitterShape      = kCAEmitterLayerLine;
    // 渲染模式
    self.caELayer.renderMode        = kCAEmitterLayerAdditive;
    // 发射方向
    self.caELayer.velocity          = 1;
    // 随机产生粒子
    self.caELayer.seed              = (arc4random() % 100) + 1;
    
    // cell
    CAEmitterCell *cell             = [CAEmitterCell emitterCell];
    // 速率
    cell.birthRate                  = 1.0;
    // 发射的角度
    cell.emissionRange              = 0.11 * M_PI;
    // 速度
    cell.velocity                   = 300;
    // 范围
    cell.velocityRange              = 150;
    // Y轴 加速度分量
    cell.yAcceleration              = 75;
    // 声明周期
    cell.lifetime                   = 2.04;
    //是个CGImageRef的对象,既粒子要展现的图片
    cell.contents                   = (id)
    [[UIImage imageNamed:@"FFRing"] CGImage];
    // 缩放比例
    cell.scale                      = 0.2;
    // 粒子的颜色
    cell.color                      = [[UIColor colorWithRed:0.6
                                                       green:0.6
                                                        blue:0.6
                                                       alpha:1.0] CGColor];
    // 一个粒子的颜色green 能改变的范围
    cell.greenRange                 = 1.0;
    // 一个粒子的颜色red 能改变的范围
    cell.redRange                   = 1.0;
    // 一个粒子的颜色blue 能改变的范围
    cell.blueRange                  = 1.0;
    // 子旋转角度范围
    cell.spinRange                  = M_PI;
    
    // 爆炸
    CAEmitterCell *burst            = [CAEmitterCell emitterCell];
    // 粒子产生系数
    burst.birthRate                 = 1.0;
    // 速度
    burst.velocity                  = 0;
    // 缩放比例
    burst.scale                     = 2.5;
    // shifting粒子red在生命周期内的改变速度
    burst.redSpeed                  = -1.5;
    // shifting粒子blue在生命周期内的改变速度
    burst.blueSpeed                 = +1.5;
    // shifting粒子green在生命周期内的改变速度
    burst.greenSpeed                = +1.0;
    //生命周期
    burst.lifetime                  = 0.35;
    
    // 火花 and finally, the sparks
    CAEmitterCell *spark            = [CAEmitterCell emitterCell];
    //粒子产生系数，默认为1.0
    spark.birthRate                 = 400;
    //速度
    spark.velocity                  = 125;
    // 360 deg//周围发射角度
    spark.emissionRange             = 2 * M_PI;
    // gravity//y方向上的加速度分量
    spark.yAcceleration             = 75;
    //粒子生命周期
    spark.lifetime                  = 3;
    //是个CGImageRef的对象,既粒子要展现的图片
    spark.contents                  = (id)
    [[UIImage imageNamed:@"FFTspark"] CGImage];
    //缩放比例速度
    spark.scaleSpeed                = -0.2;
    //粒子green在生命周期内的改变速度
    spark.greenSpeed                = -0.1;
    //粒子red在生命周期内的改变速度
    spark.redSpeed                  = 0.4;
    //粒子blue在生命周期内的改变速度
    spark.blueSpeed                 = -0.1;
    //粒子透明度在生命周期内的改变速度
    spark.alphaSpeed                = -0.25;
    //子旋转角度
    spark.spin                      = 2* M_PI;
    //子旋转角度范围
    spark.spinRange                 = 2* M_PI;
    
    
    self.caELayer.emitterCells = [NSArray arrayWithObject:cell];
    cell.emitterCells = [NSArray arrayWithObjects:burst, nil];
    burst.emitterCells = [NSArray arrayWithObject:spark];
    [self.layer addSublayer:self.caELayer];
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.offset(0);
    }];

}

- (void)hidden {
    [self.caELayer removeAllAnimations];
    self.caELayer = nil;
    self.cancelButtonClick = nil;
    self.otherButtonClick = nil;
    [self removeFromSuperview];
    
    if (self.lastView) {
        [self.lastView setAlpha:0];
        [self.lastView setHidden:NO];
        [UIView animateWithDuration:0.25 animations:^{
            [self.lastView setAlpha:1];
        } completion:nil];
    }
}

#pragma mark - Private Function
- (void)cancelBtnClick {
    if (self.cancelButtonClick) {
        self.cancelButtonClick();
    }
    [self hidden];
}

- (void)otherBtnClick {
    if (self.otherButtonClick) {
        self.otherButtonClick();
    }
    [self hidden];
}

- (void)initUI {
    [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.titleLbl];
    [self.bgView addSubview:self.messageLbl];
    
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.offset(270);
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.left.right.offset(0);
        make.top.offset(25);
    }];
    [self.messageLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40);
        make.right.offset(-40);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(25);
    }];
    
    if (self.otherButtonTitle) {
        [self.bgView addSubview:self.cancelBtn];
        [self.bgView addSubview:self.otherBtn];
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(16);
            make.top.equalTo(self.messageLbl.mas_bottom).offset(26);
            make.bottom.offset(-22);
            make.width.offset(108);
            make.height.offset(39);
        }];
        [self.otherBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-16);
            make.top.equalTo(self.messageLbl.mas_bottom).offset(26);
            make.bottom.offset(-22);
            make.width.offset(108);
            make.height.offset(39);
        }];
    } else {
        [self.bgView addSubview:self.cancelBtn];
        [self.cancelBtn setBackgroundColor:RGB0X(0xFFAE00)];
        [self.cancelBtn setTitleColor:RGB0X(0xFFFFFF) forState:UIControlStateNormal];
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.messageLbl.mas_bottom).offset(28);
            make.bottom.offset(-22);
            make.width.offset(108);
            make.height.offset(39);
            make.centerX.offset(0);
        }];
    }
    
    
}

#pragma mark - Lazyload Function

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        [_bgView setBackgroundColor:[UIColor whiteColor]];
        _bgView.layer.cornerRadius = 10;
        _bgView.layer.masksToBounds = YES;
    }
    return _bgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.text = self.title;
        _titleLbl.numberOfLines = 0;
        _titleLbl.font = [UIFont boldSystemFontOfSize:15];
        _titleLbl.textColor = RGB0X(0x333333);
        _titleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLbl;
}

- (UILabel *)messageLbl {
    if (!_messageLbl) {
        _messageLbl = [[UILabel alloc] init];
        _messageLbl.text = self.message;
        _messageLbl.numberOfLines = 0;
        _messageLbl.font = [UIFont systemFontOfSize:13];
        _messageLbl.textColor = RGB0X(0x333333);
        _messageLbl.textAlignment = NSTextAlignmentCenter;
        _messageLbl.numberOfLines = 0;
    }
    return _messageLbl;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc] init];
        [_cancelBtn setTitle:self.cancelButtonTitle forState:UIControlStateNormal];
        [_cancelBtn setBackgroundColor:RGB0X(0xEEEEEE)];
        _cancelBtn.layer.masksToBounds = YES;
        _cancelBtn.layer.cornerRadius = 5;
        [_cancelBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_cancelBtn setTitleColor:RGB0X(0x999999) forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}

- (UIButton *)otherBtn {
    if (!_otherBtn) {
        _otherBtn = [[UIButton alloc] init];
        [_otherBtn setTitle:self.otherButtonTitle forState:UIControlStateNormal];
        [_otherBtn setBackgroundColor:RGB0X(0xFFAE00)];
        [_otherBtn setTitleColor:RGB0X(0xFFFFFF) forState:UIControlStateNormal];
        _otherBtn.layer.masksToBounds = YES;
        _otherBtn.layer.cornerRadius = 5;
        [_otherBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_otherBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_otherBtn addTarget:self action:@selector(otherBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _otherBtn;
}

@end
