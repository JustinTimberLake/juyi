//
//  UINavigationController+Jtn.m
//  Grain
//
//  Created by Justin on 2017/12/27.
//  Copyright © 2017年 东方银谷. All rights reserved.
//

#import "UINavigationController+Jtn.h"

@implementation UINavigationController (Jtn)

+ (void)load
{
    // Inject "-pushViewController:animated:"
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        Method originalMethod = class_getInstanceMethod(self, @selector(pushViewController:animated:));
        Method swizzledMethod = class_getInstanceMethod(self, @selector(zj_pushViewController:animated:));
        method_exchangeImplementations(originalMethod, swizzledMethod);
        
        
        //重写 pop 适当时间调用 popTo
        Method originalMethodPop = class_getInstanceMethod(self, @selector(popViewControllerAnimated:));
        Method swizzledMethodPop = class_getInstanceMethod(self, @selector(zj_popViewControllerAnimated:));
        method_exchangeImplementations(originalMethodPop, swizzledMethodPop);
        
        
    });
}
- (UIViewController *)zj_popViewControllerAnimated:(BOOL)animated
{
    if (self.zj_PopToRoot) {
        [self popToRootViewControllerAnimated:YES];
        return nil;
    }
    return [self zj_popViewControllerAnimated:YES];
}
- (void)zj_pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    self.zj_PopToRoot = NO;
    // Forward to primary implementation.
    if (![self.viewControllers containsObject:viewController]) {
        [self zj_pushViewController:viewController animated:animated];
    }
}

- (BOOL)zj_PopToRoot
{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setZj_PopToRoot:(BOOL)hidden
{
    objc_setAssociatedObject(self, @selector(zj_PopToRoot), @(hidden), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
