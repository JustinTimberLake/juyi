
/*~!
 | @FUNC  框架常量
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef HRConstant_h
#define HRConstant_h

#pragma mark - ---------- 偏好设置（Userdefault） ----------


#pragma mark - ---------- 解归档（Archive） ----------


#pragma mark - ---------- 通知（Notification） ----------


#pragma mark - ---------- 其他（Others） ----------
//cell复用（UITabelView & UICollectionView）
//static NSString *const HRCellIdentifier = @"HRCellIdentifier";



#endif /* HRConstant_h */
