//
//  JYActivityDetailViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYActivityDetailViewController.h"

@interface JYActivityDetailViewController ()

@end

@implementation JYActivityDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"活动详情";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
