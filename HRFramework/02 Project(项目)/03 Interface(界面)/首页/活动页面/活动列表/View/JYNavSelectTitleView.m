//
//  JYNavSelectTitleView.m
//  JY
//
//  Created by duanhuifen on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYNavSelectTitleView.h"

@implementation JYNavSelectTitleView

- (void)awakeFromNib{
    [super awakeFromNib];
    for (UIButton * btn in self.titleView.subviews) {
        if (btn.tag == 1000) {
            btn.selected = YES;
        }else{
            btn.selected = NO;
        }
    }
      
}

//- (void)creatTitleWithArr:(NSArray *)arr{
//    float Margin = 25;
//    for (int i = 0; i < arr.count; i++) {
//        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [btn setTitle:arr[i] forState:UIControlStateNormal];
//        [btn setTitleColor:RGB0X(0x333333) forState:UIControlStateNormal];
//        [btn setTitleColor:RGB0X(0xFCC71E) forState:UIControlStateSelected];
//        [btn sizeToFit];
//        btn.frame = CGRectMake(btn.width + Margin, <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
//    }
//
//}

- (IBAction)navBtnClick:(UIButton *)sender {
    if (_navBtnClickBlock) {
        _navBtnClickBlock(sender);
    }
    
}

//- (NSArray *)titleArr
//{
//    if (!_titleArr) {
//        _titleArr = @[@"全部",@"线上活动",@"线下活动"];
//    }
//    return _titleArr;
//}



@end
