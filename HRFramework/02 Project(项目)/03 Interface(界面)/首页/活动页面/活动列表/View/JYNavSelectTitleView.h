//
//  JYNavSelectTitleView.h
//  JY
//
//  Created by duanhuifen on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYNavSelectTitleView : UIView

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (nonatomic,strong) NSArray * titleArr;
@property (nonatomic,copy) void(^navBtnClickBlock)(UIButton * btn);
@end
