//
//  JYActivityTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYActivityTableViewCell.h"
#import "JYActivityModel.h"

@interface JYActivityTableViewCell ()
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *currentTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UIImageView *stateBgImageView;
@property (weak, nonatomic) IBOutlet UILabel *stateLab;


@end
@implementation JYActivityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bottomLine.hidden = YES;
}

- (void)showBottomLine:(BOOL)isShow{
    self.bottomLine.hidden = isShow ? NO : YES;
}

- (void)setModel:(JYActivityModel *)model{
    _model = model;
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:model.actImage] placeholderImage:nil];
    self.currentTitleLab.text = model.actTitle;
//    self.timeLab.text = SF(@"%@/%@",model.actStartTime,model.actEndTime);
    self.timeLab.text = SF(@"%@/%@",[NSString YYYYMMDDWithDataStr:model.actStartTime],[NSString YYYYMMDDWithDataStr:model.actEndTime] );
    // 2即将开始3正在进行4已结束
    switch ([model.actState intValue]) {
        case 2:{
            self.stateBgImageView.image = [UIImage imageNamed:@"活动-黄色按钮"];
            self.stateLab.text = @"即将开始";
        }
            break;
        case 3:{
            self.stateBgImageView.image = [UIImage imageNamed:@"活动-黄色按钮"];
            self.stateLab.text = @"正在进行";
        }
            break;
        case 4:{
            self.stateBgImageView.image = [UIImage imageNamed:@"活动-已结束灰色按钮"];
            self.stateLab.text = @"已结束";
        }
            
            break;
    
        default:
            break;
    }
}

- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.currentTitleLab setLabelTitleHighlight:self.model.actTitle andSearchStr:keywords];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
