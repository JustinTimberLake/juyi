//
//  JYActivityTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYActivityModel;
@interface JYActivityTableViewCell : UITableViewCell

@property (nonatomic,strong) JYActivityModel *model;


- (void)showBottomLine:(BOOL)isShow;

- (void)setHighLightWithKeywords:(NSString *)keywords;
@end
