//
//  JYActivityListViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYActivityListViewController.h"
#import "SDCycleScrollView.h"
#import "JYActivityTableViewCell.h"
#import "JYNavSelectTitleView.h"
#import "JYChooseCommonTableViewCell.h"
#import "JYActivityDetailViewController.h"
#import "JYActivityViewModel.h"
#import "JYBaseWebViewController.h"
#import "JYActivityModel.h"
#import "JYHomeViewModel.h"
#import "JYHomeBannerModel.h"
#import "JYCommonWebViewController.h"
#import "JYAutoSizeTitleView.h"
#import "JYDetailViewController.h"
#import "JYVideoListViewController.h"


static NSString * ActivityCellId = @"JYActivityTableViewCell";
static NSString * ChooseCellId = @"JYChooseCommonTableViewCell";

@interface JYActivityListViewController ()<UITableViewDelegate,
    UITableViewDataSource,
    SDCycleScrollViewDelegate
    >
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet UITableView *activityTableView;
@property (nonatomic,strong) SDCycleScrollView * scrollView;
@property (nonatomic,strong) JYNavSelectTitleView * titleView;

@property (weak, nonatomic) IBOutlet UIView *blackBgView;
@property (weak, nonatomic) IBOutlet UITableView *chooseTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseTableViewH;

@property (nonatomic,strong) NSArray * titleChooseDataArr;
@property (nonatomic,strong) UIButton * selectTitleBtn;

@property (nonatomic,strong) JYActivityViewModel *viewModel;
//1全部2线下活动3线上活动
@property (nonatomic,assign) NSInteger activityType;
//1全部2即将开始3正在进行4已结束
@property (nonatomic,assign) NSInteger state;
//请求banner用
@property (nonatomic,strong) JYHomeViewModel *homeViewModel;
@property (nonatomic,strong) NSArray *navTitleArr;
@property (nonatomic,strong) JYAutoSizeTitleView *autoSizeTitleView;


@end

@implementation JYActivityListViewController

#pragma mark -------------生命周期-------------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self creatTitle];
    [self initData];
    [self setUpRefreshData];
    [self requestBanner];

    [self requestGetActivityWithIsMore:NO];
}

#pragma mark -------------初始化UI-------------

- (void)configUI{
    WEAKSELF
//    [self creatBannerView];
    [self.activityTableView registerNib:[UINib nibWithNibName:@"JYActivityTableViewCell" bundle:nil] forCellReuseIdentifier:ActivityCellId];
    [self rightImageItem:@"筛选图标" action:^{
        self.blackBgView.hidden = NO;
    }];
//    暂时弃用，勿删
//    self.titleView.frame = CGRectMake(0, 0, SCREEN_WIDTH - 60 * 2, 44);
//    self.navigationItem.titleView = self.titleView;
//
//    self.selectTitleBtn = [self.titleView viewWithTag:1000];
//
//    self.titleView.navBtnClickBlock = ^(UIButton * btn ){
//        weakSelf.selectTitleBtn.selected = NO;
//        weakSelf.selectTitleBtn = btn;
//        weakSelf.selectTitleBtn.selected = YES;
//        weakSelf.titleView.lineView.centerX = btn.centerX;
//        weakSelf.activityType = weakSelf.selectTitleBtn.tag - 1000 + 1;
//        [weakSelf requestGetActivityWithIsMore:NO];
//    };
    
    self.blackBgView.hidden = YES;
    self.chooseTableViewH.constant = 44 * self.titleChooseDataArr.count;
//    初始化chooseTableView
    [self.chooseTableView registerNib:[UINib nibWithNibName:@"JYChooseCommonTableViewCell" bundle:nil] forCellReuseIdentifier:ChooseCellId];
//    self.bannerView.mj_h = HB_IPONE6_ADAPTOR_HEIGHT(190);
//    self.activityTableView.tableHeaderView = self.bannerView;
}
- (void)creatTitle{
    self.navTitleArr = @[@"全部",@"线上活动",@"线下活动"];
    WEAKSELF
    JYAutoSizeTitleView * autoSizeTitleView = [[JYAutoSizeTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 60 * 2, 44)];
    self.autoSizeTitleView = autoSizeTitleView;
//    [self.titleView addSubview:autoSizeTitleView];
    self.navigationItem.titleView = autoSizeTitleView;
    
    //    需要设置的属性
    autoSizeTitleView.norColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    autoSizeTitleView.selColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isShowUnderLine = YES;
    autoSizeTitleView.underLineH = 3;
    autoSizeTitleView.underLineColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isUnderLineEqualTitleWidth = YES;
    autoSizeTitleView.fontSize = FONT(15);    autoSizeTitleView.didSelectItemsWithIndexBlock = ^(NSInteger index) {
//        if (index == 0) {
//            weakSelf.home = YES;
//            weakSelf.secondaryId = @"";
//            [weakSelf requestHomeTop];
//        }else{
//            JYShopSecondaryModel * model = self.classifyViewModel.secondClassifyArr[index - 1];
//            weakSelf.secondaryId = model.secondaryId;
//            [weakSelf setUpRefreshData];
//            weakSelf.home = NO;
//        }
        if (index == 0) {
            weakSelf.activityType = 1;
        }else if (index == 1 ){
            weakSelf.activityType = 3;
        }else{
             weakSelf.activityType = 2;
        }
//          weakSelf.activityType = index + 1;
         [weakSelf requestGetActivityWithIsMore:NO];
    };
//    [self.titleArr addObject:@"首页"];
    [autoSizeTitleView updataTitleData:self.navTitleArr];

}

- (void)initData{
    self.activityType = 1;
    self.state = 1;
}

- (void)creatBannerView{
    //轮播图也写到layoutSubview里了
    self.scrollView = [SDCycleScrollView cycleScrollViewWithFrame:(CGRectMake(0, 0, SCREEN_WIDTH, HB_IPONE6_ADAPTOR_HEIGHT(190))) delegate:self  placeholderImage:JY_PLACEHOLDER_IMAGE];
    self.scrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    self.scrollView.delegate = self; // 如需监听图片点击，请设置代理，实现代理方法
    self.scrollView.autoScrollTimeInterval = 3;// 自定义轮播时间间隔
    self.scrollView.titleLabelBackgroundColor = [UIColor clearColor];
    self.scrollView.showPageControl = YES;
    self.scrollView.currentPageDotImage = [UIImage imageNamed:@"轮播-选中"];
    self.scrollView.pageDotImage = [UIImage imageNamed:@"轮播-未选中"];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    [self.bannerView addSubview:self.scrollView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -------------action-------------

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    self.blackBgView.hidden = YES;
}

#pragma mark -------------网络请求-------------

- (void)setUpRefreshData{
    WEAKSELF
    self.activityTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestGetActivityWithIsMore:NO];
    }];
    self.activityTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestGetActivityWithIsMore:YES];
    }];
//    [self.activityTableView.mj_header beginRefreshing];
}

//请求首页banner
- (void)requestBanner{
    WEAKSELF
    [self.homeViewModel requestBannerWithBannerType:BannerType_Activity success:^(NSString *msg, id responseData) {
        [weakSelf updataBanner];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


- (void)endRefresh{
    [self.activityTableView.mj_header endRefreshing];
    [self.activityTableView.mj_footer endRefreshing];
}

- (void)requestGetActivityWithIsMore:(BOOL)isMore{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(self.activityType);
    dic[@"state"] = @(self.state);
    [self.viewModel requestGetActivitysWithParams:dic isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.activityTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)updataBanner{
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYHomeBannerModel * model in self.homeViewModel.bannerArr) {
        
        [imageArr addObject:model.bannerImage];
    }
    self.scrollView.imageURLStringsGroup = imageArr;
}

#pragma mark -------------UItableView 数据源和代理-------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section{
    if (tableView == self.chooseTableView) {
        return self.titleChooseDataArr.count ;
    }
    return self.viewModel.listArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JYActivityTableViewCell * activityCell = [tableView dequeueReusableCellWithIdentifier:ActivityCellId];
    
    JYChooseCommonTableViewCell * chooseCell = [tableView dequeueReusableCellWithIdentifier:ChooseCellId];
    if (tableView == self.chooseTableView) {
        chooseCell.contentLab.text = self.titleChooseDataArr[indexPath.row];
        return chooseCell;
    }
    activityCell.model = self.viewModel.listArr[indexPath.row];
    return activityCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.chooseTableView) {
        return 44;
    }

    return 220;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //1全部2即将开始3正在进行4已结束
    JYChooseCommonTableViewCell * chooseCell = [tableView cellForRowAtIndexPath:indexPath];
    if (tableView == self.chooseTableView) {
        chooseCell.bgView.backgroundColor = RGB0X(0xf5f5f5);
        self.blackBgView.hidden = YES;
        self.state = indexPath.row + 1;
        [self requestGetActivityWithIsMore:NO];
    }else{
          JYActivityModel * model = self.viewModel.listArr[indexPath.row];
        if(model.actType == 2 ){
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.actUrl;
            webVC.navTitle = @"活动详情";
            [self.navigationController pushViewController:webVC animated:YES];

        }else{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            detailVC.goodsId = model.actUrl;
            [self.navigationController pushViewController:detailVC animated:YES];
        }

//        暂时注释  是网页
//        JYActivityDetailViewController * detailVC = [[JYActivityDetailViewController alloc] init];
//        [self.navigationController pushViewController:detailVC animated:YES];
    }
}
#pragma mark ------------SDCycleScrollView 代理--------------

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    JYHomeBannerModel * model = self.homeViewModel.bannerArr[index];
    
    //(1活动详情，2帖子详情，3油站列表，4店铺列表，5油站详情，6店铺详情，7商品列表，8商品详情，9 url)
    
    //   最新的： (1商品 2是油站3 是视频4是资讯5URL)
    switch ([model.bannerType intValue]) {
        case 1:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            detailVC.goodsId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
            
        case 2:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            detailVC.shopId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 3:{
            JYVideoListViewController * videoListVC = [[JYVideoListViewController alloc] init];
            [self.navigationController pushViewController:videoListVC animated:YES];
        }
            break;
        case 4:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            webVC.navTitle = @"资讯详情";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 5:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
            
            
        default:
            break;
    }
}


#pragma mark -----------------lazy------------------

- (JYNavSelectTitleView *)titleView
{
    if (!_titleView) {
        _titleView = LOADXIB(@"JYNavSelectTitleView");
    }
    return _titleView;
}

- (NSArray *)titleChooseDataArr
{
    if (!_titleChooseDataArr) {
        _titleChooseDataArr = @[@"全部",@"即将开始",@"正在进行",@"已经结束"];
    }
    return _titleChooseDataArr;
}

- (JYActivityViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYActivityViewModel alloc] init];
    }
    return _viewModel;
}

- (JYHomeViewModel *)homeViewModel
{
    if (!_homeViewModel) {
        _homeViewModel = [[JYHomeViewModel alloc] init];
    }
    return _homeViewModel;
}

@end
