//
//  JYActivityModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYActivityModel : JYBaseModel
//活动id
@property (nonatomic,copy) NSString *actId;
//标题
@property (nonatomic,copy) NSString *actTitle;
// 2即将开始3正在进行4已结束
@property (nonatomic,copy) NSString *actState;
//开始时间
@property (nonatomic,copy) NSString *actStartTime;
//结束时间
@property (nonatomic,copy) NSString *actEndTime;
//图片
@property (nonatomic,copy) NSString *actImage;
//内容
@property (nonatomic,copy) NSString *actContent;
//内容url
@property (nonatomic,copy) NSString *actUrl;
//1线上 2线下
@property (nonatomic,assign) NSInteger actType;

@end
