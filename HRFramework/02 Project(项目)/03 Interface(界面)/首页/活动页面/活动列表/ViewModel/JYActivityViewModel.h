//
//  JYActivityViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYActivityViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;

//3.8.1.	JY-008-001 获取活动
- (void)requestGetActivitysWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
