//
//  JYLocationViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYLocationViewModel.h"
#import "JYCityModel.h"
#import "JYProvinceModel.h"
#import "JYProCityCountyModel.h"

@implementation JYLocationViewModel

//获取热门城市
- (void)requestGetHotCitySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOCATION_GetHotCity) params:nil success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            NSArray * arr = [JYCityModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.hotTagArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取省
- (void)requestGetProvinceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOCATION_GetProvince) params:nil success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            NSArray * arr = [JYProvinceModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.provinceArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取市
- (void)requestGetCityWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOCATION_GetCity) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [weakSelf.cityArr removeAllObjects];
            NSArray * arr = [JYCityModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.cityArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//搜索市
- (void)requestSearchCityWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOCATION_SearchCity) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            NSArray * arr = [JYCityModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.searchCityArr removeAllObjects];
            [weakSelf.searchCityArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取所有省市区
- (void)requestGetProCityCountySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOCATION_GetProCityCounty) params:nil success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [weakSelf.allProCityCountArr removeAllObjects];
            NSArray * arr = [JYProCityCountyModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.allProCityCountArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (NSMutableArray *)cityArr
{
    if (!_cityArr) {
        _cityArr = [NSMutableArray array];
    }
    return _cityArr;
}

- (NSMutableArray *)provinceArr
{
    if (!_provinceArr) {
        _provinceArr = [NSMutableArray array];
    }
    return _provinceArr;
}

- (NSMutableArray *)hotTagArr
{
    if (!_hotTagArr) {
        _hotTagArr = [NSMutableArray array];
    }
    return _hotTagArr;
}

- (NSMutableArray *)searchCityArr
{
    if (!_searchCityArr) {
        _searchCityArr = [NSMutableArray array];
    }
    return _searchCityArr;
}

- (NSMutableArray *)allProCityCountArr
{
    if (!_allProCityCountArr) {
        _allProCityCountArr = [NSMutableArray array];
    }
    return _allProCityCountArr;
}

@end
