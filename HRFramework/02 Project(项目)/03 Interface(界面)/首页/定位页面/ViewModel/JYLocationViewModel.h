//
//  JYLocationViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYLocationViewModel : JYBaseViewModel
//根据省获取市数组
@property (nonatomic,strong) NSMutableArray *cityArr;
//省数组
@property (nonatomic,strong) NSMutableArray *provinceArr;
//热门城市数组
@property (nonatomic,strong) NSMutableArray *hotTagArr;
//搜索市数组
@property (nonatomic,strong) NSMutableArray *searchCityArr;
//所有省市区数组
@property (nonatomic,strong) NSMutableArray *allProCityCountArr;

//获取热门城市
- (void)requestGetHotCitySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取省
- (void)requestGetProvinceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取市
- (void)requestGetCityWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//搜索市
- (void)requestSearchCityWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取所有省市区
- (void)requestGetProCityCountySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;



@end
