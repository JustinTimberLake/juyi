//
//  JYLocationViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYLocationViewController.h"
#import "JYSearchHistoryCell.h"
#import "JYCityTitleView.h"
#import "JYSearchView.h"
#import "JYLocationViewModel.h"
#import "JYCityModel.h"
#import "LZSortTool.h"
#import "JYProvinceModel.h"

@interface JYLocationViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *provinceTableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *searchBgView;
@property (weak, nonatomic) IBOutlet UILabel *currentLocationLab;
@property (weak, nonatomic) IBOutlet UIView *hotCityView;
@property (weak, nonatomic) IBOutlet UIView *hotView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hotCityViewH;

@property (weak, nonatomic) IBOutlet UIButton *locationBtn;

//@property (nonatomic,strong) UIView * searchView ;
//@property (nonatomic,strong) UIView * searchSquareView ;
//@property (nonatomic,strong) UIImageView * searchIconImageView ;
//@property (nonatomic,strong) UITextField * searchTextField ;
//@property (nonatomic,strong) UIButton * cancleBtn;

@property (nonatomic,strong) NSMutableArray * hotTagArr;
@property (nonatomic,strong) NSMutableArray * rightIndexArr;
@property (nonatomic,strong) NSArray * provinceArr;

@property (nonatomic,strong) JYSearchView * searchBarView;
@property (weak, nonatomic) IBOutlet UIView *shadeView;
@property (weak, nonatomic) IBOutlet UITableView *shadeCityTableView;
@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
//没有结果模式
@property (nonatomic, assign , getter=isNoResultMode) BOOL noResultMode;

@property (nonatomic,strong) JYLocationViewModel *viewModel;

@property (nonatomic,strong) NSMutableArray *proDataSource;
@property (weak, nonatomic) IBOutlet UIView *tapView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationBtnW;


@end

@implementation JYLocationViewController

static CGFloat const SearchViewHeight = 32;
static CGFloat const SearchIconW = 20;
static int const tagCount = 3 ;
static int const tagMargin = 5 ;
static int const tagTopMargin = 10 ;
static int const tagBorderMargin = 20 ;
#define HotTagW (SCREEN_WIDTH - tagBorderMargin * 2 - (tagCount - 1) * tagMargin) / 3
#define HotTagH 35

static NSString * const historyCellId = @"JYSearchHistoryCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configProcinceTableView];
//    [self creatSearchView];
    [self configSearchView];
    [self requestGetHotCity];
    [self requestGetProvince];
    [self configShadeCityTableView];
    [self configResultTableView];
    [self configInitDateAndUI];
//    获取当前城市
    [self loadCurrentCity];
    self.navHeight.constant = kDevice_Is_iPhoneX ?96 :64;
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}
- (void)configInitDateAndUI{
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.tapView addGestureRecognizer:tap];
    self.shadeView.hidden = YES;
    
//    [self.locationBtn sizeToFit];
    self.locationBtn .layer.borderColor = RGB0X(0xEE3F3A).CGColor;
    self.locationBtn .layer.borderWidth = 0.5;
    self.locationBtn .layer.cornerRadius = 2;
    self.locationBtn .clipsToBounds = YES;
//    self.locationBtn.width = [NSString widthOfText:self.locationBtn.currentTitle textHeight:35 font:FONT(15)] + 20;
    
    self.resultTableView.hidden = YES;
//    self.noResultMode = YES;
}

- (void)configSearchView{
    WEAKSELF
    [self.searchBgView addSubview:self.searchBarView];
    self.searchBarView.searchTextFieldEditBlock = ^(NSString *txt) {
        weakSelf.resultTableView.hidden = NO;
        if (!txt.length) {
            [weakSelf showSuccessTip:@"请输入搜索内容"];
            return;
        }
        [weakSelf requestSearchCityWithKeywords:txt];
    };
    self.searchBarView.cancleBtnActionBlock = ^{
        weakSelf.resultTableView.hidden = YES;
    };
}

- (void)loadCurrentCity{
   NSString * cityName = [[NSUserDefaults standardUserDefaults] objectForKey:JY_CURRENT_CITYNAME];
//    self.currentLocationLab.text = cityName;
    float width =  [NSString widthOfText:cityName textHeight:35 font:FONT(15)] + 20;
    self.locationBtnW.constant = width;
    [self.locationBtn setTitle:cityName forState:UIControlStateNormal];
}

////    创建搜索框 (暂时弃用)
//- (void)creatSearchView{
//    UIView * searchView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH - 10, self.searchBgView.height)];
//    searchView.backgroundColor = [UIColor clearColor];
//    self.searchView = searchView;
//    [self.searchBgView addSubview:searchView];
//    
//    UIView * searchSquareView = [[UIView alloc]initWithFrame:CGRectMake(0, 7,SCREEN_WIDTH - 10, SearchViewHeight)];
//    self.searchSquareView = searchSquareView;
//    [self.searchView addSubview:searchSquareView];
//    searchSquareView.layer.cornerRadius = SearchViewHeight / 2;
//    searchSquareView.clipsToBounds = YES;
//    searchSquareView.backgroundColor = RGB0X(0xf0f0f0);
//    
//    UIImageView * searchIconImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"放大镜"]];
//    searchIconImageView.frame = CGRectMake(15, 7, SearchIconW, SearchIconW);
//    self.searchIconImageView = searchIconImageView;
//    [self.searchSquareView addSubview:searchIconImageView];
//    
//    UITextField * searchTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.searchIconImageView.frame), 0, self.searchSquareView.width - self.searchIconImageView.width, SearchViewHeight)];
//    self.searchTextField = searchTextField;
//    self.searchTextField.placeholder = @"搜索您要查看的内容";
//    [self.searchSquareView addSubview:searchTextField];
//    [searchTextField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
//    searchTextField.delegate = self;
//    searchTextField.returnKeyType = UIReturnKeySearch;
//    searchTextField.keyboardType = UIKeyboardTypeDefault;
//    searchTextField.enablesReturnKeyAutomatically = YES;
//    searchTextField.font = FONT(14);
//    searchTextField.textColor = [UIColor darkGrayColor];
//    searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
//    
////    UIButton * cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
////    cancleBtn.frame = CGRectMake(CGRectGetMaxX(self.searchSquareView.frame), 7, SCREEN_WIDTH - CGRectGetMaxX(self.searchSquareView.frame), SearchViewHeight);
////    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
////    [cancleBtn setTitleColor:RGB0X(0x333333) forState:UIControlStateNormal];
////    cancleBtn.titleLabel.font = FONT(15);
////    cancleBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
////    [cancleBtn addTarget:self action:@selector(cancleAction:) forControlEvents:UIControlEventTouchUpInside];
////    self.cancleBtn = cancleBtn;
////    [self.searchView addSubview:cancleBtn];
//    
//}
//省tableview
- (void)configProcinceTableView{
    self.provinceTableView.delegate = self;
    self.provinceTableView.dataSource = self;
    self.provinceTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.provinceTableView registerNib:[UINib nibWithNibName:@"JYSearchHistoryCell" bundle:nil] forCellReuseIdentifier:historyCellId];
    self.provinceTableView.sectionIndexColor = RGB0X(0x666666);
    self.provinceTableView.tableHeaderView = self.headerView;
}
//市tableview 创建
- (void)configShadeCityTableView{
    self.shadeCityTableView.delegate = self;
    self.shadeCityTableView.dataSource = self;
    self.shadeCityTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.shadeCityTableView registerNib:[UINib nibWithNibName:@"JYSearchHistoryCell" bundle:nil] forCellReuseIdentifier:historyCellId];
}
//搜索结果的tableView
- (void)configResultTableView{
    self.resultTableView.delegate = self;
    self.resultTableView.dataSource = self;
    self.resultTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.resultTableView registerNib:[UINib nibWithNibName:@"JYSearchHistoryCell" bundle:nil] forCellReuseIdentifier:historyCellId];
}

#pragma mark --------action --------

- (void)textChange:(UITextField *)textField{
    
}

- (void)hotBtnAction:(UIButton *)btn{
    JYCityModel * model = self.hotTagArr[btn.tag - 1000];
    [JY_USERDEFAULTS setObject:model.cityTitle forKey:JY_CURRENT_SELECTCITYNAME];
    [JY_USERDEFAULTS synchronize];
    JY_POST_NOTIFICATION(JY_SELECT_CITY,nil);
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)locationBtnAction:(UIButton *)sender {
    
    [JY_USERDEFAULTS setObject:self.locationBtn.currentTitle forKey:JY_CURRENT_SELECTCITYNAME];
    [JY_USERDEFAULTS synchronize];
    JY_POST_NOTIFICATION(JY_SELECT_CITY,nil);
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)cancleAction:(UIButton *)btn{
//    self.searchView.hidden = YES;
//    [self.searchTextField resignFirstResponder];
//    [self.navigationController popViewControllerAnimated:YES];
//}

#pragma mark -------手势 -----------

- (void)tap:(UITapGestureRecognizer *)tap{
    [self shadeViewHide];
}
#pragma mark --------自定义方法-------

- (void)creatTagBtn{
    if (self.hotTagArr.count) {
        int row = 0;
        int line = 0;
        for (int i = 0; i < self.hotTagArr.count; i ++) {
            JYCityModel * cityModel = self.hotTagArr[i];
            line = i / tagCount;
            row = i % tagCount;
            UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(row * (tagMargin + HotTagW), line *(tagTopMargin + HotTagH) , HotTagW, HotTagH);
            btn.titleLabel.font = FONT(14);
            [btn setTitle:cityModel.cityTitle forState:UIControlStateNormal];
            [btn setTitleColor:RGB0X(0x333333) forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor whiteColor]];
            btn.layer.borderColor = RGB0X(0xdbdbdb).CGColor;
            btn.layer.borderWidth = 0.5;
            btn.layer.cornerRadius = 2;
            btn.clipsToBounds = YES;
            btn.tag = 1000 + i;
            [btn addTarget:self action:@selector(hotBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.hotCityView addSubview:btn];
//            [self.rightIndexArr addObject:[self firstCharactor:cityModel.cityTitle]];
        }
        self.hotCityViewH.constant = CGRectGetMaxY([self.hotCityView viewWithTag:(self.hotTagArr.count - 1 + 1000)].frame);
    }else{
        self.hotCityViewH.constant = 0;
    }
    self.hotView.height = self.hotCityViewH.constant + 15 *2 + 30;
    self.headerView.height = 40 + self.hotView.height;
    self.provinceTableView.tableHeaderView = self.headerView;
}

//获取新的省数组
- (void)getSortNewArrWithDataArr:(NSArray *)dataArr{
    [self.rightIndexArr removeAllObjects];
//    NSArray * arr = @[@"鲁迅",@"###",@"刘一",@"赵四",@"钱",@"李三",@"孙五",@"王二",@"黄蓉",@"孙悟空",@"哪吒",@"李天王",@"范冰冰",@"赵丽颖",@"霍建华",@"黄晓明",@"成龙",@"李连杰",@"李小龙",@"曾小贤",@"LiShan"];
//    NSArray *arr1 = [LZSortTool sortStrings:arr withSortType:LZSortResultTypeDoubleValues];
//    对模型排序
    NSArray *pArr = [LZSortTool sortObjcs:dataArr byKey:@"firstLetter" withSortType:LZSortResultTypeDoubleValues];
    self.proDataSource = [NSMutableArray arrayWithArray:pArr];
    NSLog(@"🌺🌺🌺🌺🌺%@",self.proDataSource);
    
    for (NSDictionary * dic  in self.proDataSource) {
        NSString * str = [dic objectForKey:LZSortToolKey];
        [self.rightIndexArr addObject:str];
    }
    [self.provinceTableView reloadData];
}


//- (NSString *)firstCharactor:(NSString *)aString
//{
//    //转成了可变字符串
//    NSMutableString *str = [NSMutableString stringWithString:aString];
//    //先转换为带声调的拼音
//    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
//    //再转换为不带声调的拼音
//    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
//    //转化为大写拼音
//    NSString *pinYin = [str capitalizedString];
//    //获取并返回首字母
//    return [pinYin substringToIndex:1];
//}

- (void)shadeViewShow{
    [UIView animateWithDuration:0.2 animations:^{
        self.shadeView.hidden = NO;
    }];
}

- (void)shadeViewHide{
    [UIView animateWithDuration:0.2 animations:^{
        self.shadeView.hidden = YES;
    }];
}

#pragma mark ---- 网络请求-----------
//获取热门城市请求
- (void)requestGetHotCity{
    WEAKSELF
    [self.viewModel requestGetHotCitySuccess:^(NSString *msg, id responseData) {
        [weakSelf updataHotCityUI];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//获取省请求
- (void)requestGetProvince{
    WEAKSELF
    [self.viewModel requestGetProvinceSuccess:^(NSString *msg, id responseData) {
//        [weakSelf.provinceTableView reloadData];
        [weakSelf getSortNewArrWithDataArr:weakSelf.viewModel.provinceArr];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//获取市请求
- (void)requestCityWithpProvinceCode:(NSString *)provinceCode{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"provinceCode"] = provinceCode;
    [self.viewModel requestGetCityWithParams:dic Success:^(NSString *msg, id responseData) {
        [weakSelf.shadeCityTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//搜索城市请求
- (void)requestSearchCityWithKeywords:(NSString *)keywords{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"key"] = keywords;
    [self.viewModel requestSearchCityWithParams:dic Success:^(NSString *msg, id responseData) {
        if (!weakSelf.viewModel.searchCityArr.count) {
            self.noResultMode = YES;
        }else{
            self.noResultMode = NO;
        }
        [weakSelf.resultTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



#pragma mark ---- 更新数据 -----------

- (void)updataHotCityUI{
    [self.hotTagArr removeAllObjects];
    [self.hotTagArr addObjectsFromArray:self.viewModel.hotTagArr];
    [self creatTagBtn];
}


#pragma mark ---- tableview 的数据源和代理-----------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.provinceTableView) {
        return self.proDataSource.count;
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.provinceTableView) {
        NSDictionary *dic = self.proDataSource[section];
        return [[dic objectForKey:LZSortToolValueKey] count];

    }else if (tableView == self.resultTableView){
        if (self.isNoResultMode) {
            return 1;
        }
        return self.viewModel.searchCityArr.count;
    }else{
        return self.viewModel.cityArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JYSearchHistoryCell * historyCell = [tableView dequeueReusableCellWithIdentifier:historyCellId];
    historyCell.selectionStyle = 0;
    if (tableView == self.resultTableView ) {
        if (self.isNoResultMode) {
            historyCell.contentLab.text = @"抱歉，未找到相关位置，可修改后重试";
        }else{
            historyCell.SearchCityModel = self.viewModel.searchCityArr[indexPath.row];
        }
    }else if (tableView == self.provinceTableView){
    
        NSDictionary *dic = self.proDataSource[indexPath.section];
        NSArray * items = [dic objectForKey:LZSortToolValueKey] ;
        JYProvinceModel * provinceModel = items[indexPath.row];
        historyCell.contentLab.text = provinceModel.provinceTitle;
    }else if (tableView == self.shadeCityTableView){
        historyCell.SearchCityModel = self.viewModel.cityArr[indexPath.row];
    }
    return historyCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.provinceTableView) {
        JYCityTitleView * titleView = LOADXIB(@"JYCityTitleView");
        NSDictionary *dic = [self.proDataSource objectAtIndex:section];
        NSString * str = [dic objectForKey:LZSortToolKey];
        titleView.titleLab.text = str;
        return titleView;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.provinceTableView) {
        return 30;
    }else{
        return 0.1;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.provinceTableView) {
        NSDictionary *dic = self.proDataSource[indexPath.section];
        NSArray * items = [dic objectForKey:LZSortToolValueKey] ;
        JYProvinceModel * provinceModel = items[indexPath.row];
        [self requestCityWithpProvinceCode:provinceModel.provinceCode];
        [self shadeViewShow];
    }else if (tableView == self.shadeCityTableView){
       JYCityModel *cityModel = self.viewModel.cityArr[indexPath.row];
        [JY_USERDEFAULTS setObject:cityModel.cityTitle forKey:JY_CURRENT_SELECTCITYNAME];
        [JY_USERDEFAULTS synchronize];
        JY_POST_NOTIFICATION(JY_SELECT_CITY,nil);
        [self dismissViewControllerAnimated:YES completion:nil];
    }else if (tableView == self.resultTableView ) {
        if (!self.isNoResultMode) {
            JYCityModel *cityModel = self.viewModel.searchCityArr[indexPath.row];
            [JY_USERDEFAULTS setObject:cityModel.cityTitle forKey:JY_CURRENT_SELECTCITYNAME];
            [JY_USERDEFAULTS synchronize];
            JY_POST_NOTIFICATION(JY_SELECT_CITY,nil);
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }else{
        [self shadeViewHide];
    }
}

//显示每组标题索引

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (tableView == self.provinceTableView) {
        return self.rightIndexArr;
    }
    return nil;
}

//返回每个索引的内容

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView == self.provinceTableView) {
        return self.rightIndexArr[section];
    }
    return nil;
}

//响应点击索引时的委托方法
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index{
    NSLog(@"===%@  ===%ld",title,(long)index);
    
    [tableView
     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]
     atScrollPosition:UITableViewScrollPositionTop animated:YES];
    return index;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.searchBarView.searchTextField resignFirstResponder];
}

- (IBAction)closeBtnAction:(id)sender {
    [self.searchBarView.searchTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark ---------------lazy----------------

- (NSMutableArray *)hotTagArr
{
    if (!_hotTagArr) {
        _hotTagArr = [NSMutableArray array];
    }
    return _hotTagArr;
}

- (NSMutableArray *)rightIndexArr
{
    if (!_rightIndexArr) {
        _rightIndexArr = [NSMutableArray array];
    }
    return _rightIndexArr;
}
- (JYSearchView *)searchBarView
{
    if (!_searchBarView) {
        _searchBarView = LOADXIB(@"JYSearchView");
    }
    return _searchBarView;
}

- (JYLocationViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYLocationViewModel alloc] init];
    }
    return _viewModel;
}

@end
