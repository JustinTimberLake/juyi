//
//  JYCityTitleView.h
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYCityTitleView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
