//
//  JYProCityCountyModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYCityModel.h"

@interface JYProCityCountyModel : JYBaseModel
//省编码
@property (nonatomic,copy) NSString *id;
//省名称
@property (nonatomic,copy) NSString *name;
//该省下所有城市
@property (nonatomic,strong) NSArray<JYCityModel *> *cityList;

@end
