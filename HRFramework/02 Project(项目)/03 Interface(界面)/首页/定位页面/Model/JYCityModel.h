//
//  JYCityModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYCountyModel.h"

@interface JYCityModel : JYBaseModel
//城市编码
@property (nonatomic,copy) NSString *id;
//城市名称
@property (nonatomic,copy) NSString *name;

@property (nonatomic,strong) NSArray<JYCountyModel *> *cityList;

@property (nonatomic,copy) NSString *cityCode;

@property (nonatomic,copy) NSString *cityTitle;

@end
