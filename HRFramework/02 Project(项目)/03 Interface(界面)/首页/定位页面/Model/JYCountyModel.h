//
//  JYCountyModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYCountyModel : JYBaseModel
// 区县编码
@property (nonatomic,copy) NSString *id;
// 区县名称
@property (nonatomic,copy) NSString *name;
@end
