//
//  JYProvinceModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYProvinceModel : JYBaseModel
//省编码
@property (nonatomic,copy) NSString *provinceCode;
//省名称
@property (nonatomic,copy) NSString *provinceTitle;

@property (nonatomic,copy) NSString *firstLetter;
@end
