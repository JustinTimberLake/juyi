//
//  JYProCityCountyModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProCityCountyModel.h"

@implementation JYProCityCountyModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"cityList":@"JYCityModel"
             };
}

@end
