//
//  JYCategoryViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCategoryViewController.h"
#import "JYTurnSearchView.h"
#import "JYSearchViewController.h"
#import "JYCategoryTableViewCell.h"
#import "JYCategoryDetailCollectionViewCell.h"
#import "JYProductListViewController.h"
#import "JYShopClassifyViewModel.h"
#import "JYShopClassifyModel.h"
#import "JYShopSecondaryModel.h"

static NSString * const CategoryCellId = @"JYCategoryTableViewCell";
static NSString * const CategoryDetailCellId = @"JYCategoryDetailCollectionViewCell";

@interface JYCategoryViewController ()<

    UITableViewDelegate,
    UITableViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDataSource

    >
@property (nonatomic,strong) JYTurnSearchView *searchView;
@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *detailCollectionView;
@property (nonatomic,assign) NSInteger currentRow;

@property (nonatomic,strong) JYShopClassifyViewModel *classifyViewModel;
//当前选中的一级分类id
@property (nonatomic,copy) NSString *classifyId;
//二级分类id
@property (nonatomic,copy) NSString *secondClassifyId;

@end

@implementation JYCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self requestShopClassify];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.searchView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.searchView.hidden = YES;
}

- (void)configUI{
    WEAKSELF
//    [self.navigationController.view addSubview:self.searchView];
    [self rightItemTitle:@"" color:[UIColor whiteColor] font:FONT(14) action:^{
        
    }];
    
    self.navigationItem.titleView = self.searchView;
    self.searchView.placeHolder = @"请输入关键字";
    
    self.searchView.startTurnSearchActionBlock = ^{
        JYSearchViewController * searchVC = [[JYSearchViewController alloc]init];
        searchVC.searchType = SearchTypeProduct;
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
    };
    
    self.categoryTableView.backgroundColor = RGB0X(0xf5f5f5);
    self.detailCollectionView.backgroundColor = RGB0X(0xf5f5f5);
    
    [self.categoryTableView registerNib:[UINib nibWithNibName:@"JYCategoryTableViewCell" bundle:nil] forCellReuseIdentifier:CategoryCellId];
    [self.detailCollectionView registerNib:[UINib nibWithNibName:@"JYCategoryDetailCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CategoryDetailCellId];

//    [self.categoryTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
    self.currentRow = 0;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark ------------网络请求-----------
//一级分类
- (void)requestShopClassify{
    WEAKSELF
    [self.classifyViewModel requestGetShopClassifySuccess:^(NSString *msg, id responseData) {
        [self.categoryTableView reloadData];
        [weakSelf.categoryTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        [self requestSecondClassifyWithIsFirst:YES];
//        [weakSelf.detailCollectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//二级分类

- (void)requestSecondClassifyWithIsFirst:(BOOL)isFirst{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(1);
    if (isFirst && self.classifyViewModel.classifyArr.count) {
        JYShopClassifyModel *classifyModel = self.classifyViewModel.classifyArr[0];
        self.classifyId = classifyModel.classifyId;
    }
    dic[@"classifyId"] = self.classifyId;
    
    [self.classifyViewModel requestGetShopSecondaryWithParams:dic    Success:^(NSString *msg, id responseData) {
        [weakSelf.detailCollectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ------------tableView 数据源和代理-----------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.classifyViewModel.classifyArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    JYCategoryTableViewCell * categoryCell = [tableView dequeueReusableCellWithIdentifier:CategoryCellId];
    if (self.currentRow == indexPath.row) {
        [categoryCell tansformMethodSelect];
    }else{
        [categoryCell tansformMethodUnSelect];
    }
    
    categoryCell.classifyModel = self.classifyViewModel.classifyArr[indexPath.row];

    return categoryCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JYCategoryTableViewCell *lastCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow inSection:0]];
    [lastCell tansformMethodUnSelect];
    
    JYCategoryTableViewCell * categoryCell = [tableView cellForRowAtIndexPath:indexPath];
    self.currentRow = indexPath.row;
    [categoryCell tansformMethodSelect];
    
   JYShopClassifyModel *classifyModel = self.classifyViewModel.classifyArr[indexPath.row];
    self.classifyId = classifyModel.classifyId;
    [self requestSecondClassifyWithIsFirst:NO];
}




#pragma mark ------------collectionView 数据源和代理-----------


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.classifyViewModel.secondClassifyArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYCategoryDetailCollectionViewCell * detailCell = [collectionView dequeueReusableCellWithReuseIdentifier:CategoryDetailCellId   forIndexPath:indexPath];
    detailCell.model = self.classifyViewModel.secondClassifyArr[indexPath.item];
    return detailCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    JYShopSecondaryModel * secondModel = self.classifyViewModel.secondClassifyArr[indexPath.item];
    self.secondClassifyId = secondModel.secondaryId;
    
    JYProductListViewController * productVC = [[JYProductListViewController alloc] init];
    productVC.queryType = JYQueryType_Classity;
    productVC.classifyId = self.classifyId;
    productVC.secondaryId = self.secondClassifyId;
    [self.navigationController pushViewController:productVC animated:YES];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - self.categoryTableView.width - 20 - 5 * 2 )/3, 120);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(20, 10, 10, 10);
}


#pragma mark-------------lazy-----------------

//- (JYTurnSearchView *)searchView
//{
//    if (!_searchView) {
//        _searchView = [[JYTurnSearchView alloc] init];
//    }
//    return _searchView;
//}

- (JYTurnSearchView *)searchView
{
    if (!_searchView) {
        _searchView = [[JYTurnSearchView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH - 50-50, 44)];
        //        _searchView.frame = CGRectMake(0, 20, SCREEN_WIDTH - 50-50, 44);
    }
    return _searchView;
}

- (JYShopClassifyViewModel *)classifyViewModel
{
    if (!_classifyViewModel) {
        _classifyViewModel = [[JYShopClassifyViewModel alloc] init];
    }
    return _classifyViewModel;
}

@end
