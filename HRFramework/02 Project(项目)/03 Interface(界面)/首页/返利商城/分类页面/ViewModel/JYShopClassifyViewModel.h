//
//  JYShopClassifyViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@class JYShopClassifyModel;

@interface JYShopClassifyViewModel : JYBaseViewModel
//一级分类
@property (nonatomic,strong) NSMutableArray<JYShopClassifyModel *> *classifyArr;
//二级分类
@property (nonatomic,strong) NSMutableArray *secondClassifyArr;

//获取商品分类
- (void)requestGetShopClassifySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//二级分类
- (void)requestGetShopSecondaryWithParams:(NSDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
