//
//  JYShopClassifyViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYShopClassifyViewModel.h"
#import "JYShopClassifyModel.h"
#import "JYShopSecondaryModel.h"

@implementation JYShopClassifyViewModel
//获取商品分类
- (void)requestGetShopClassifySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_SHOP_GetShopClassify) params:nil success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            NSArray * arr = [JYShopClassifyModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.classifyArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//二级分类
- (void)requestGetShopSecondaryWithParams:(NSDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@======%@",JY_PATH(JY_SHOP_GetShopSecondary),params);
    [manager POST_URL:JY_PATH(JY_SHOP_GetShopSecondary) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [weakSelf.secondClassifyArr removeAllObjects];
            NSArray * arr = [JYShopSecondaryModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.secondClassifyArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray<JYShopClassifyModel *> *)classifyArr
{
    if (!_classifyArr) {
        _classifyArr = [NSMutableArray array];
    }
    return _classifyArr;
}

- (NSMutableArray *)secondClassifyArr
{
    if (!_secondClassifyArr) {
        _secondClassifyArr = [NSMutableArray array];
    }
    return _secondClassifyArr;
}
@end
