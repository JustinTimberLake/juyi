//
//  JYCategoryDetailCollectionViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYShopSecondaryModel;

@interface JYCategoryDetailCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) JYShopSecondaryModel *model;
@end
