//
//  JYCategoryTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,JYCategoryType) {
    JYCategoryType_shop,//返利商城分类
    JYCategoryType_proCityCouny//省市区分类
};

@class JYShopClassifyModel;

@interface JYCategoryTableViewCell : UITableViewCell

@property (nonatomic,strong) JYShopClassifyModel *classifyModel;

@property (nonatomic,assign) JYCategoryType categoryType;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIView *yellowLineView;

- (void)tansformMethodSelect;
- (void)tansformMethodUnSelect;

@end
