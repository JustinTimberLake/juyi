//
//  JYCategoryDetailCollectionViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCategoryDetailCollectionViewCell.h"
#import "JYShopSecondaryModel.h"

@interface JYCategoryDetailCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;


@end
@implementation JYCategoryDetailCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setModel:(JYShopSecondaryModel *)model{
    _model = model;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.secondaryImage] placeholderImage:nil];
    self.titleLab.text = model.secondaryTitle;
}

@end
