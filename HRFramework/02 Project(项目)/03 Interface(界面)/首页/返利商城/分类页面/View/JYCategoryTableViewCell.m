//
//  JYCategoryTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCategoryTableViewCell.h"
#import "JYShopClassifyModel.h"

@interface JYCategoryTableViewCell ()


@end
@implementation JYCategoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if (self.categoryType == JYCategoryType_proCityCouny) {
        self.yellowLineView.hidden = YES;
    }
}

- (void)setClassifyModel:(JYShopClassifyModel *)classifyModel{
    _classifyModel = classifyModel;
    self.titleLab.text = classifyModel.classifyTitle;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)tansformMethodSelect{
    if (self.categoryType == JYCategoryType_proCityCouny) {
        self.yellowLineView.hidden = YES;
      self.bgView.backgroundColor = [UIColor whiteColor];
    }else{
        self.yellowLineView.hidden = NO;
        self.bgView.backgroundColor = RGB0X(0xf5f5f5);
    }
}

-(void)tansformMethodUnSelect{
    if (self.categoryType == JYCategoryType_proCityCouny) {
        self.yellowLineView.hidden = YES;
        self.bgView.backgroundColor = RGB0X(0xf5f5f5);
    }else{
        self.yellowLineView.hidden = YES;
        self.bgView.backgroundColor = [UIColor whiteColor];
    }
}

@end
