//
//  JYShopSecondaryModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYShopSecondaryModel : JYBaseModel
// 二级分类ID
@property (nonatomic,copy) NSString *secondaryId;
// 二级分类名称
@property (nonatomic,copy) NSString *secondaryTitle;
// 二级分类图片
@property (nonatomic,copy) NSString *secondaryImage;

@end
