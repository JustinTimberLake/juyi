//
//  JYShopClassifyModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYShopClassifyModel : JYBaseModel
//分类ID
@property (nonatomic,copy) NSString *classifyId;
//分类名称
@property (nonatomic,copy) NSString *classifyTitle;

@end
