//
//  JYStoreViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYStoreViewModel.h"


@implementation JYStoreViewModel
//获取商店头部信息
- (void)requestShopInfoWithShopId:(NSString *)shopId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"ShopId"] = shopId;
    NSLog(@"%@ --%@",JY_PATH(JY_SHOP_GetShopInformation),dic);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_SHOP_GetShopInformation) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
            weakSelf.shopModel = [JYShopListModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
    
}
@end
