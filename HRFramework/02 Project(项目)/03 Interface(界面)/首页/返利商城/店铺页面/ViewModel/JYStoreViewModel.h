//
//  JYStoreViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYShopListModel.h"

@interface JYStoreViewModel : JYBaseViewModel

@property (nonatomic,strong) JYShopListModel *shopModel;
//获取商店头部信息
- (void)requestShopInfoWithShopId:(NSString *)shopId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
