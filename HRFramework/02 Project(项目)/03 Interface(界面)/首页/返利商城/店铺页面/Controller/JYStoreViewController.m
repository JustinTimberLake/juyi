//
//  JYStoreViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYStoreViewController.h"
#import "JYSearchViewController.h"
#import "JYProductCell.h"
#import "JYStoreCollectionHeaderView.h"
#import "JYDetailViewController.h"
#import "JYAutoSizeTitleView.h"
#import "JYShopListModel.h"
#import "JYGetHomeTopViewModel.h"
#import "JYShopClassifyViewModel.h"
#import "JYShopSecondaryModel.h"
#import "JYProductListViewModel.h"
#import "JYCollectionViewModel.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件
#import "JYStoreViewModel.h"
#import "JYAlertPicViewController.h"
#import "HRCall.h"
//// 默认标题间距
//static CGFloat const margin = 20;
//// 下划线默认高度
//static CGFloat const YZUnderLineH = 2;



static NSString *const ProductCellId = @"JYProductCell";
static NSString *const StoreHeaderViewId = @"JYStoreCollectionHeaderView";

@interface JYStoreViewController ()<
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout,
    UIScrollViewDelegate
    >
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (nonatomic,strong) JYAutoSizeTitleView * autoSizeTitleView;

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (weak, nonatomic) IBOutlet UILabel *BusinessHoursLab;//营业时间
@property (weak, nonatomic) IBOutlet UIView *starView;
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn;
@property (nonatomic,strong) JYGetHomeTopViewModel *viewModel;
@property (nonatomic,strong) JYShopClassifyViewModel *classifyViewModel;
@property (nonatomic,strong) JYProductListViewModel *productViewModel;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;
@property (nonatomic,strong) JYStoreViewModel *storeViewModel;
@property (nonatomic,strong) NSMutableArray *titleArr;
@property (weak, nonatomic) IBOutlet UIView *topView;

//是否是首页
@property (nonatomic, assign , getter=isHome) BOOL home;
//标题id
@property (nonatomic,copy) NSString *secondaryId;
//赞取消赞
@property (nonatomic,copy) NSString *operatStr;

@property (nonatomic,assign) JY_CollectionType collectionType;


@end

@implementation JYStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self creatTitle];
    [self updataUIWithShopType];
    [self initData];
    [self requestSecondTitle];
    [self setUpRefreshData];
    [self requestShopInfo];
    
}

- (void)configUI{
    WEAKSELF
    
    self.naviTitle = @"店铺";
    self.topView.layer.shadowColor = [[UIColor colorWithHex:0x000000 alpha:0.2] CGColor];
    self.topView.layer.shadowOffset = CGSizeMake(0, 2);
    
    [self rightImageItem:@"搜索黑" action:^{
        JYSearchViewController * searchVC = [[JYSearchViewController alloc]init];
        searchVC.searchType = SearchTypeProduct;
        searchVC.isStoreProduct = YES;
        searchVC.shopProductId = weakSelf.shopId;
        [weakSelf.navigationController pushViewController:searchVC animated:YES];

    }];
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYProductCell" bundle:nil] forCellWithReuseIdentifier:ProductCellId];
    
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYStoreCollectionHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:StoreHeaderViewId];
}

- (void)updataUIWithShopType{
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:self.storeViewModel.shopModel.shopImage] placeholderImage:DEFAULT_COURSE];
    self.titleLab.text = self.storeViewModel.shopModel.shopTitle.length?self.storeViewModel.shopModel.shopTitle:@" ";
    self.telLab.text = self.storeViewModel.shopModel.shopTel;
    [self configStar:self.storeViewModel.shopModel];
    //    缺少具体的距离值
    self.collectionBtn.selected = [self.storeViewModel.shopModel.isCollect intValue] == 1 ? YES : NO;
    NSString *str= SCREEN_WIDTH>320?@"营业时间:":@"";
    self.BusinessHoursLab.text = self.storeViewModel.shopModel.business_hours.length?[NSString stringWithFormat:@"%@ %@",str,self.storeViewModel.shopModel.business_hours]:@"营业时间:";
}

- (void)initData{
    self.home = YES;
}

- (void)configStar:(JYShopListModel *)model{
    NSInteger num = [model.shopEvaluate intValue];
    for (UIImageView * view in self.starView.subviews) {
        if ((view.tag - 1000) < num) {
            view.image = [UIImage imageNamed:@"评价黄星"];
        }else{
            view.image = [UIImage imageNamed:@"评价灰星"];
        }
    }
}


- (void)creatTitle{
    WEAKSELF
    JYAutoSizeTitleView * autoSizeTitleView = [[JYAutoSizeTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    self.autoSizeTitleView = autoSizeTitleView;
    [self.titleView addSubview:autoSizeTitleView];
    
//    需要设置的属性
    autoSizeTitleView.norColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    autoSizeTitleView.selColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isShowUnderLine = YES;
    autoSizeTitleView.underLineH = 1;
    autoSizeTitleView.underLineColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isUnderLineEqualTitleWidth = YES;
    autoSizeTitleView.fontSize = FONT(15);
    autoSizeTitleView.didSelectItemsWithIndexBlock = ^(NSInteger index) {
        if (index == 0) {
            weakSelf.home = YES;
            weakSelf.secondaryId = @"";
            [weakSelf requestHomeTop];
        }else{
            weakSelf.home = NO;
            JYShopSecondaryModel * model = self.classifyViewModel.secondClassifyArr[index - 1];
            weakSelf.secondaryId = model.secondaryId;
            [weakSelf setUpRefreshData];
            
        }
    };
    [self.titleArr addObject:@"首页"];
    [autoSizeTitleView updataTitleData:self.titleArr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}


#pragma mark --------------Action----------------
- (IBAction)collectionBtnAction:(UIButton *)sender {
    
    if (![self judgeLogin]) {
        return;
    }
    
    sender.selected = !sender.selected;
    self.operatStr = sender.selected ?@"1":@"2";
    [self requestCollectionOperation];
}



#pragma mark --------------网络请求----------------
- (void)requestHomeTop{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    if (self.shopType == JYShopType_Store) {
        dic[@"type"] = @"2";
        dic[@"shopId"] = self.shopId;
    }else if (self.shopType == JYShopType_keepCar){
        dic[@"type"] = @"3";
        dic[@"shopId"] = self.shopId;
    }

    [self.viewModel requestGetHomeTopWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf.myCollectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求商店信息
- (void)requestShopInfo{
    WEAKSELF
//    if (self.shopType == JYShopType_Store) {
        [self.storeViewModel requestShopInfoWithShopId:self.shopId success:^(NSString *msg, id responseData) {
            [weakSelf updataUIWithShopType];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
//    }else{
//
//    }
}

#pragma mark ------请求子标题
- (void)requestSecondTitle{
    
    WEAKSELF
//    if (self.shopType == JYShopType_keepCar) {
//
//    }else{
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"type"] = @"2";
        dic[@"shopId"] = self.shopId;
        [self.classifyViewModel requestGetShopSecondaryWithParams:dic Success:^(NSString *msg, id responseData) {
            [weakSelf updataTitleView];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
//    }
}

//请求数据
- (void)setUpRefreshData{
    WEAKSELF
    self.myCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (weakSelf.home == YES) {
            [weakSelf requestHomeTopGoodsList:NO];
        }else{
            [weakSelf requestGoodsListIsMore:NO];
        }
    }];
    self.myCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (weakSelf.home == YES) {
            [weakSelf requestHomeTopGoodsList:YES];
        }else{
            [weakSelf requestGoodsListIsMore:YES];
        }
    }];
    [self.myCollectionView.mj_header beginRefreshing];
}

- (void)endRefresh{
    [self.myCollectionView.mj_header endRefreshing];
    [self.myCollectionView.mj_footer endRefreshing];
}

//JY-007-010 获取商城首页/店铺首页商品列表
- (void)requestHomeTopGoodsList:(BOOL)isMore{
    WEAKSELF;
    [self.viewModel requestGetHomeGoodsListWithType:HomeGoodsListType_Shop andShopId:self.shopId isMore:isMore sucess:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myCollectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}



- (void)requestGoodsListIsMore:(BOOL)isMore{
    
    WEAKSELF
    
    if (self.shopType == JYShopType_Store) {
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"type"] = @"2";
        //    dic[@"classifyId"] = self.classifyId;
        dic[@"secondaryId"] = self.secondaryId;
        dic[@"shopId"] = self.shopId;//少此参数
        [self.productViewModel requestGoodsListWithParams:dic isMore:isMore success:^(NSString *msg, id responseData) {
            [weakSelf endRefresh];
            [weakSelf.myCollectionView reloadData];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
            [weakSelf endRefresh];
        }];
    }else{
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"shopId"] = self.shopId;
        dic[@"shopTypeId"] =  self.secondaryId;

        [self.productViewModel requestKeepGoodsListWithParams:dic isMore:isMore success:^(NSString *msg, id responseData) {
            [weakSelf endRefresh];
            [weakSelf.myCollectionView reloadData];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
            [weakSelf endRefresh];
        }];
    }
}
//点赞请求
- (void)requestCollectionOperation{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
//    if (self.shopType == JYShopType_keepCar) {
        dic[@"shopId"] = self.shopId;
//    }else{
//        dic[@"shopId"] = self.shopId;
//    }
    dic[@"type"] = self.shopType == JYShopType_keepCar ? @"1" : @"2";
    dic[@"operat"] = self.operatStr;
    self.collectionType = JY_CollectionType_Seller;
    [self.collectionViewModel requestCollectionWithParams:dic andType:self.collectionType success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"操作成功"];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



- (void)updataTitleView{
//    NSMutableArray * titles = [NSMutableArray array];
    [self.titleArr removeAllObjects];
    
    for (JYShopSecondaryModel * model in self.classifyViewModel.secondClassifyArr) {
        [self.titleArr addObject:model.secondaryTitle];
    }
//    [self.titleArr addObjectsFromArray:titles];
    [self.titleArr insertObject:@"首页" atIndex:0];
    [self.autoSizeTitleView updataTitleData:self.titleArr];
}



#pragma mark --------------collectionView 数据源和代理----------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.isHome) {
//        if (self.viewModel.goodsArr.count >= 6) {
//            return 2;
//        }
//        return 0;
        return self.viewModel.goodsListArr.count;
    }else{
        if (self.shopType == JYShopType_Store) {
            
            return self.productViewModel.goodsArr.count;
        }else{
            return self.productViewModel.keepCarGoodsArr.count;
        }
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYProductCell * productCell = [collectionView dequeueReusableCellWithReuseIdentifier:ProductCellId forIndexPath:indexPath];
    if (self.productViewModel.goodsArr.count && self.shopType == JYShopType_Store) {
        productCell.model = self.productViewModel.goodsArr[indexPath.item];
    }else if (self.productViewModel.keepCarGoodsArr.count && self.shopType == JYShopType_keepCar){
        productCell.model = self.productViewModel.keepCarGoodsArr[indexPath.item];
    }else{
//        productCell.model = self.viewModel.goodsArr[indexPath.item + 4];
        productCell.model = self.viewModel.goodsListArr[indexPath.item];
    }
    return productCell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    if (self.isHome) {
        JYStoreCollectionHeaderView * headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:StoreHeaderViewId forIndexPath:indexPath];
        headerView.goodsArr = self.viewModel.goodsArr;
        headerView.homeProductBtnActionBlock = ^(NSInteger tag) {
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            JYMyProductModel * productModel;
            NSString * tagStr = SF(@"%ld",(long)tag);
            
            if ([tagStr hasPrefix:@"1"]) {
                productModel = weakSelf.viewModel.goodsArr[tag - 1000];
            }else if ([tagStr hasPrefix:@"2"]){
                productModel = weakSelf.viewModel.goodsArr[tag - 2000];
            }else if ([tagStr hasPrefix:@"3"]){
                productModel = weakSelf.viewModel.goodsArr[tag - 3000];
            }
            detailVC.goodsId = productModel.goodsId;
            [weakSelf.navigationController pushViewController:detailVC animated:YES];
        };
        [headerView hideTopHeader];
        return headerView;
    }else{
        return nil;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (self.isHome) {
        return CGSizeMake(SCREEN_WIDTH, [JYStoreCollectionHeaderView heightWithContainBannerAndTitle:NO andGoods:self.viewModel.goodsArr]);
    }else{
        return CGSizeZero;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
    JYMyProductModel *model;
    
    if (self.isHome) {
//       model = self.viewModel.goodsArr[indexPath.item + 4];
        detailVC.detailType = (self.shopType == JYShopType_Store) ? DetailTypeStore : DetailTypeKeepCar;
        model = self.viewModel.goodsListArr[indexPath.item];
    }else{
        if (self.shopType == JYShopType_Store) {
            detailVC.detailType = DetailTypeStore;
            model = self.productViewModel.goodsArr[indexPath.item];
        }else if (self.shopType == JYShopType_keepCar){
            detailVC.detailType = DetailTypeKeepCar;
            model = self.productViewModel.keepCarGoodsArr[indexPath.item];

        }
    }
    
    detailVC.shopId = self.shopId;
    detailVC.goodsId = model.goodsId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - 20 -5 )/2, [JYProductCell cellH]);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
#pragma mark -------------telephone-----------------

- (IBAction)telephoneClick:(UIButton *)sender {
    if (![self judgeLogin]) {
        return ;
    }
    WEAKSELF;
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.contentText= SF(@"呼叫%@",weakSelf.storeViewModel.shopModel.shopTel );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [HRCall callPhoneNumber:weakSelf.storeViewModel.shopModel.shopTel alert:NO];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

#pragma mark -------------lazy-----------------

- (JYGetHomeTopViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYGetHomeTopViewModel alloc] init];
    }
    return _viewModel;
}

- (JYShopClassifyViewModel *)classifyViewModel
{
    if (!_classifyViewModel) {
        _classifyViewModel = [[JYShopClassifyViewModel alloc] init];
    }
    return _classifyViewModel;
}

- (NSMutableArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = [NSMutableArray array];
    }
    return _titleArr;
}

- (JYProductListViewModel *)productViewModel
{
    if (!_productViewModel) {
        _productViewModel = [[JYProductListViewModel alloc] init];
    }
    return _productViewModel;
}

- (JYCollectionViewModel *)collectionViewModel
{
    if (!_collectionViewModel) {
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}

- (JYStoreViewModel *)storeViewModel
{
    if (!_storeViewModel) {
        _storeViewModel = [[JYStoreViewModel alloc] init];
    }
    return _storeViewModel;
}


@end
