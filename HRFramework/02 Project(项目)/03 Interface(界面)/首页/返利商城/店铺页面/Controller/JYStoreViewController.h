//
//  JYStoreViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@class JYShopListModel;

typedef NS_ENUM(NSInteger,JYShopType) {
    JYShopType_Store ,//商城模块跳入
    JYShopType_keepCar ,//养车周边模块跳入
};

@interface JYStoreViewController : JYBaseViewController

//@property (nonatomic,strong)  JYShopListModel * shopModel;

@property (nonatomic,assign) JYShopType shopType;

//少商城id 从商品跳转店铺
@property (nonatomic,copy) NSString *shopId;
@end
