//
//  JYShopOrderViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

#import "JYCreatOrderModel.h"

@interface JYShopOrderViewModel : JYBaseViewModel

@property (nonatomic,strong) JYCreatOrderModel *creatOrderModel;

//3.7.1.	JY-007-006 创建订单
- (void)requestCreateShopOrderWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


//3.7.1.	JY-007-007 提交订单
- (void)requestSubmitShopOrderWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
