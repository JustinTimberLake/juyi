//
//  JYShopOrderViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYShopOrderViewModel.h"

@implementation JYShopOrderViewModel
//3.7.1.	JY-007-006 创建订单
- (void)requestCreateShopOrderWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_SHOP_CreateShopOrder) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            weakSelf.creatOrderModel = [JYCreatOrderModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


//3.7.1.	JY-007-007 提交订单
- (void)requestSubmitShopOrderWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@--%@",JY_PATH(JY_SHOP_SubmitShopOrder),params);
    [manager POST_URL:JY_PATH(JY_SHOP_SubmitShopOrder) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
//            NSString * orderId = RESULT_DATA[@"orderId"];
            successBlock(RESULT_MESSAGE,RESULT_DATA);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
