//
//  JYProductNoticeTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
//暂时废弃
@interface JYProductNoticeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *freightLab;
@property (weak, nonatomic) IBOutlet UITextField *messageTextField;

@end
