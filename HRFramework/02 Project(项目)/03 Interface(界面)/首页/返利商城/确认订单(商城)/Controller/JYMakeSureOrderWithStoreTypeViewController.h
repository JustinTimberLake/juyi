//
//  JYMakeSureOrderWithStoreTypeViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"
#import "JYGetAttributeModel.h"
#import "JYGoodsInfoModel.h"

@interface JYMakeSureOrderWithStoreTypeViewController : JYBaseViewController
//1实体服务2商品
@property (nonatomic,copy) NSString *typeStr;
//实体服务ID/商品ID
//@property (nonatomic,copy) NSString *goodsId;

@property (nonatomic,strong) JYGetAttributeModel * attributeModel;

@property (nonatomic,copy) NSString *item1Name;

@property (nonatomic,copy) NSString *item2Name;
//商品数量
@property (nonatomic,copy) NSString *goodsNum;

@property (nonatomic,strong) JYGoodsInfoModel *goodsModel;



@end
