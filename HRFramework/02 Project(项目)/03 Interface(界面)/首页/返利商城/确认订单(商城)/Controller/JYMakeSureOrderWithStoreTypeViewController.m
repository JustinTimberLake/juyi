//
//  JYMakeSureOrderWithStoreTypeViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMakeSureOrderWithStoreTypeViewController.h"
#import "JYSwitchTableViewCell.h"
#import "JYProductNoticeTableViewCell.h"
#import "PickerViewType.h"
#import "JYPickerViewAlertController.h"
#import "JYPaySuccessViewController.h"
#import "JYAlertPicViewController.h"
#import "JYShopOrderViewModel.h"
#import "JYUsableCouponCell.h"
#import "JYCouponModel.h"
#import "JYMyAdressMianViewController.h"
#import "JYPayViewModel.h"
#import "JYPaySuccessViewController.h"
#import "JYRechargeViewController.h"
#import "JYGetIntegralRatioViewModel.h"

static NSString * const SwitchCellId = @"JYSwitchTableViewCell";
static NSString * const ProductNoticeCellId = @"JYProductNoticeTableViewCell";
static NSString * UsableCouponCellId = @"JYUsableCouponCell";

@interface JYMakeSureOrderWithStoreTypeViewController ()<UITableViewDelegate,
UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic,strong) NSArray * itemArr;
@property (weak, nonatomic) IBOutlet UIButton *storeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
//@property (weak, nonatomic) IBOutlet UILabel *colorLab;
//@property (weak, nonatomic) IBOutlet UILabel *sizeLab;
//@property (weak, nonatomic) IBOutlet UILabel *countLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (nonatomic,strong)  JYProductNoticeTableViewCell * noticeHeader;
@property (nonatomic,copy) NSString *addressId;

//弹窗的类型 （普通/支付）
@property (nonatomic, assign ) PickerViewType pickerViewType;
@property (nonatomic,strong) JYShopOrderViewModel *viewModel;
@property (nonatomic,copy) NSString *useScore; //是否使用积分
@property (nonatomic,copy) NSString *useCouponId;//可用优惠券选中id
@property (nonatomic,copy) NSString *message;//留言
@property (weak, nonatomic) IBOutlet UILabel *freightLab;
@property (weak, nonatomic) IBOutlet UITextField *messageTextField;
@property (weak, nonatomic) IBOutlet UILabel *addressNameLa;
@property (weak, nonatomic) IBOutlet UILabel *addressPhoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressDetailLab;
@property (weak, nonatomic) IBOutlet UIView *addAddressView;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addAddressViewH;
@property (weak, nonatomic) IBOutlet UIView *addressNormalView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressNormalViewH;

@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (nonatomic,strong) JYGetIntegralRatioViewModel *integralViewModel;
@property (nonatomic,copy) NSString *IntegralRatio; //积分兑换比率
@property (nonatomic,copy) NSString *needIntegral; //订单需要多少积分

//@property (nonatomic,copy) NSString *originalTotalPrice;//原始价格
@property (nonatomic,copy) NSString *goodsTotalPrice; //商品总价
@property (nonatomic,copy) NSString *endTotalPrice;// 最终价格

@property (nonatomic,copy) NSString *couponPrice; //优惠券价钱
@property (weak, nonatomic) IBOutlet UILabel *item1TitleLab;
@property (weak, nonatomic) IBOutlet UILabel *item2TitleLab;
@property (weak, nonatomic) IBOutlet UILabel *countTitleLab;



@end

@implementation JYMakeSureOrderWithStoreTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self updataUI];
    [self requestCreatOrder];
    [self addNotification];
    [self updataNeedIntegral];
    //    暂时不需要兑换比率
    //    [self requestIntegralRadio];
}

- (void)configUI{
    self.naviTitle = @"确认订单";
    
    [self.orderTableView registerNib:[UINib nibWithNibName:@"JYSwitchTableViewCell" bundle:nil] forCellReuseIdentifier:SwitchCellId];
    [self.orderTableView registerNib:[UINib nibWithNibName:@"JYProductNoticeTableViewCell" bundle:nil] forCellReuseIdentifier:ProductNoticeCellId];
    [self.orderTableView registerNib:[UINib nibWithNibName:@"JYUsableCouponCell" bundle:nil] forCellReuseIdentifier:UsableCouponCellId];
    
    self.orderTableView.tableHeaderView = self.headerView;
    self.orderTableView.backgroundColor = RGB0X(0xf5f5f5);
    self.itemArr = @[@"我的积分",@"我的优惠券"];
    
    
    self.addAddressView.hidden = [self.typeStr intValue] == 1 ? YES : NO;
    self.addressNormalView.hidden = [self.typeStr intValue] == 1 ? YES : NO;
    self.addressNormalViewH.constant = [self.typeStr intValue] == 1 ? 0 : 130;
    self.addAddressViewH.constant = [self.typeStr intValue] == 1 ? 0 : 130;
    
    if ([self.typeStr intValue] == 1) {
        self.headerView.height = 124 + 144;
        
    }else{
        self.headerView.height = 124 + 144 + 130;
    }
    self.orderTableView.tableHeaderView = self.headerView;
}

- (IBAction)commitBtnAction:(UIButton *)sender {
    [self requestCommitShopOrder];
}

#pragma mark ==================通知==================

- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
        [self showSuccessTip:noti.object];
    }
}

//账户页面支付弹出
- (void)showAlertPicViewWithAccountType{
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * totalStr = [self.totalPriceLab.text floatValue] > 0 ? self.totalPriceLab.text :@"";
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalStr floatValue]);
    
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalStr,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ----------------网络请求---------------------------
//创建订单
- (void)requestCreatOrder{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = self.typeStr; //1实体服务2商品
    dic[@"goodsId"] = self.goodsModel.goodsId;
    //    self.goodsId;
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    
    [self.viewModel requestCreateShopOrderWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf updataAddressUIWithAfterCreatOrder];
        
        if (weakSelf.viewModel.creatOrderModel.coupons.count) {
            weakSelf.viewModel.creatOrderModel.isExpand = YES;
        }
        
        [weakSelf.orderTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

////请求积分兑换比率
//- (void)requestIntegralRadio{
//    WEAKSELF
//    [self.integralViewModel requestGetIntegralRatioSuccess:^(NSString *msg, id responseData) {
//        weakSelf.IntegralRatio = responseData;
//        [weakSelf updataNeedIntegral];
//        [weakSelf.orderTableView reloadData];
//    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
//    }];
//}


//更新订单需要积分
- (void)updataNeedIntegral{
    //    self.needIntegral = SF(@"%.f",[self.goodsTotalPrice floatValue] * [self.IntegralRatio intValue]);
    self.needIntegral = SF(@"%.f",[self.goodsModel.goodsScore floatValue] * [self.goodsNum intValue]);
}


- (void)requestCommitShopOrder{
    WEAKSELF
    if ([self.typeStr intValue] == 2 && !self.addressId.length) {
        [self showSuccessTip:@"请选择地址"];
        return;
    }
    
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = self.typeStr; //1实体服务2商品
    dic[@"goodsId"] = self.goodsModel.goodsId;
    //    self.goodsId;
    dic[@"itemId"] = self.attributeModel.itemId;
    dic[@"goodsNum"] = self.goodsNum ;
    //    dic[@"expressage"] = self.attributeModel.price ? self.attributeModel.price :self.goodsModel.goodsPrice;
    if ([self.typeStr intValue] == 2) {
        dic[@"expressage"] = self.goodsModel.freight;
    }
    dic[@"addressId"] = self.addressId;
    dic[@"leaveMsg"] = self.messageTextField.text;
    dic[@"couponId"] = self.useCouponId;
    dic[@"score"] = self.useScore.length ? self.useScore : @"2";
    //    dic[@"totalPrice"] = self.totalPriceLab.text;
    dic[@"totalPrice"] = self.goodsTotalPrice;
    
    [self.viewModel requestSubmitShopOrderWithParams:dic success:^(NSString *msg, id responseData) {
        NSDictionary * dic = responseData;
        
        NSString * orderStr = dic[@"orderId"];
        NSString * orderType = dic[@"type"];
        if (!orderStr.length) {
            [weakSelf showSuccessTip:@"提交订单失败"];
            return ;
        }
        
        if ([orderType intValue] == 1) {
            //            走支付
//            [weakSelf showAlertWithPickerViewType:PickerViewTypePay];
            [self showAlertPayWays];
        }else{
            //            走数据库 (包含油库支付，积分支付，优惠券支付)
            //            [weakSelf requestPayUseOilDepotWithOrderSn:orderStr];
            [weakSelf turnPaySuccessPageWithOrderSn:orderStr];
        }
        self.returnPayTypeBlock = ^(NSString *str) {
            [weakSelf payTypeWithStr:str andOrderId:orderStr];
            NSMutableDictionary * dic = [NSMutableDictionary dictionary];
            if (orderStr.length) {
                dic[@"orderId"] = orderStr;
                dic[@"orderType"] = [weakSelf.typeStr intValue] == 1 ? @"实体订单" :@"商品订单";
            }
            JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
        };
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        //        [weakSelf showAlertPicViewWithNotEnoughMoney];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
    
}

#pragma mark ==================action==================

//切换地址
- (IBAction)turnAddressSelectBtnAction:(UIButton *)sender {
    WEAKSELF
    JYMyAdressMianViewController * addressVC = [[JYMyAdressMianViewController alloc] init];
    addressVC.selectMode = YES;
    addressVC.currentSelectAddressBlock = ^(JYMyAdressModel *model) {
        [weakSelf updataAddressUIWithModel:model];
    };
    [self.navigationController pushViewController:addressVC animated:YES];
}

#pragma mark ----------------更新UI---------------------------
//选完地址后更新地址
- (void)updataAddressUIWithModel:(JYMyAdressModel *)model{
    self.addAddressView.hidden = YES;
    self.addressNameLa.text = model.addressName;
    self.addressPhoneLab.text = model.addressPhone;
    self.addressDetailLab.text = model.addressInfo;
    self.addressId = model.addressId;
}

//创建订单后更改默认地址
- (void)updataAddressUIWithAfterCreatOrder{
    self.addAddressView.hidden = YES;
    self.addressNameLa.text = self.viewModel.creatOrderModel.contacter;
    self.addressPhoneLab.text = self.viewModel.creatOrderModel.phoneNumber;
    self.addressDetailLab.text = self.viewModel.creatOrderModel.ReceivingAddress;
    self.addressId = self.viewModel.creatOrderModel.addressId;
}

- (void)updataUI{
    
    [self.storeBtn setTitle:self.goodsModel.shopTitle forState:UIControlStateNormal];
    self.titleLab.text = self.goodsModel.goodsTitle;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:self.goodsModel.goodsImage] placeholderImage:JY_PLACEHOLDER_IMAGE];
    
    if (self.item1Name.length) {
        self.item1TitleLab.hidden = NO;
        self.item1TitleLab.text = SF(@"%@:%@",self.item1Name,self.attributeModel.item1);
    }else{
    }
    
    if (self.item2Name.length) {
        self.item2TitleLab.hidden = NO;
        self.item2TitleLab.text = SF(@"%@:%@",self.item2Name,self.attributeModel.item2);
    }else{
        self.item2TitleLab.hidden = YES;
    }
    NSString * count = self.goodsNum.length ? self.goodsNum : @"1";
    self.countTitleLab.text = SF(@"数量:x%@",count);
    
    self.priceLab.text = self.attributeModel.price.length ? SF(@"￥%@",self.attributeModel.price) : SF(@"￥%@",self.goodsModel.goodsPrice);
    self.freightLab.text =  [self.goodsModel.freight floatValue] > 0 ? SF(@"快递 %@元",self.goodsModel.freight):@"快递";
    
    //    商品单价
    float goodsPrice = self.attributeModel.price.length ? [self.attributeModel.price floatValue] : [self.goodsModel.goodsPrice floatValue];
    self.goodsTotalPrice = SF(@"%.2f",goodsPrice * [self.goodsNum floatValue]);
    
    self.endTotalPrice =  SF(@"%.2f",  [self.goodsTotalPrice floatValue]  +  [self.goodsModel.freight floatValue] );
    self.totalPriceLab.text = self.endTotalPrice;
}

- (void)updataCouponUI{
    
}
- (void)updataTotalPrice{
    //    自己当前积分需要多少钱
    CGFloat  integralPrice = [self.viewModel.creatOrderModel.score floatValue] / [self.IntegralRatio floatValue];
    
    
    //    使用积分 并且使用优惠券
    if ([self.useScore intValue] == 1 && self.useCouponId.length) {
        //        都使用
        self.totalPriceLab.text = SF(@"%.2f",[self.endTotalPrice floatValue] - integralPrice - [self.couponPrice floatValue]);
        
    }else if([self.useScore intValue] == 1 && !self.useCouponId.length  ){
        //        使用积分 不使用优惠券
        //        if (integralPrice >=[self.endTotalPrice floatValue]) {
        //
        //            self.totalPriceLab.text = ([self.goodsModel.freight floatValue] > 0) ? self.goodsModel.freight : @"0.00";
        //
        //        }else{
        //            self.totalPriceLab.text = SF(@"%.2f",[self.endTotalPrice floatValue] - integralPrice);
        //        }
        //        使用积分 不使用优惠券
        if (integralPrice >=[self.endTotalPrice floatValue]) {
            self.totalPriceLab.text = SF(@"%.2f",[self.goodsModel.freight floatValue]);
        }else{
            self.totalPriceLab.text = SF(@"%.2f",[self.endTotalPrice floatValue] - integralPrice);
        }
        
        
        
    }else if([self.useScore intValue] == 2 && self.useCouponId.length){
        //        不使用积分，使用优惠券
        //        if ([self.couponPrice floatValue] >=[self.endTotalPrice floatValue]) {
        //
        //            self.totalPriceLab.text = ([self.goodsModel.freight floatValue] > 0) ? self.goodsModel.freight : @"0.00";
        //
        //        }else{
        //            self.totalPriceLab.text = SF(@"%.2f",[self.endTotalPrice floatValue] - [self.couponPrice floatValue]);
        //        }
        //        不使用积分，使用优惠券
        if ([self.couponPrice floatValue] >=[self.goodsTotalPrice floatValue]) {
            self.totalPriceLab.text = SF(@"%.2f",[self.goodsModel.freight floatValue]) ;
        }else{
            self.totalPriceLab.text = SF(@"%.2f",[self.endTotalPrice floatValue] - [self.couponPrice floatValue]);
        }
        
    }else if([self.useScore intValue] == 2 && !self.useCouponId.length){
        //        不使用积分，不使用优惠券
        self.totalPriceLab.text = SF(@"%.2f",[self.endTotalPrice floatValue]);
        
    }
    
}


//跳转到支付成功页面
- (void)turnPaySuccessPageWithOrderSn:(NSString *)orderSn{
    //    UIApplication *app = [UIApplication sharedApplication];
    //    JYBaseTabBarController *tabbar = (JYBaseTabBarController*)app.delegate.window.rootViewController;
    //    UINavigationController *nav= tabbar.viewControllers[tabbar.selectedIndex];
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    successVC.orderId = orderSn;
    successVC.orderType = [self.typeStr intValue] == 1 ? @"实体订单" : @"商品订单";
    [self.navigationController pushViewController:successVC animated:YES];
}



#pragma mark ------------tableView 数据源和代理-----------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //    if (section ==1) {
    //        if (self.viewModel.creatOrderModel.coupons.count) {
    //            self.viewModel.creatOrderModel.isExpand = 1;
    //        }else{
    //            self.viewModel.creatOrderModel.isExpand = 0;
    //        }
    //        return  self.viewModel.creatOrderModel.isExpand ? 1 : 0;
    //    }
    if (section ==1) {
        //        if (self.orderViewModel.creatOrderModel.coupons.count) {
        //            self.orderViewModel.creatOrderModel.isExpand = YES;
        //        }
        return  self.viewModel.creatOrderModel.isExpand ? 1 : 0;
    }
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    JYUsableCouponCell * couponCell = [tableView dequeueReusableCellWithIdentifier:UsableCouponCellId];
    couponCell.couponId = weakSelf.useCouponId;
    [couponCell creatCouponBtnWithData:self.viewModel.creatOrderModel.coupons];
    couponCell.couponBtnActionBlock = ^(UIButton *btn) {
        //        JYCouponModel * model = self.viewModel.creatOrderModel.coupons[tag];
        //        weakSelf.useCouponId = model.couponId;
        //        weakSelf.couponPrice = model.couponPrice;
        //        [weakSelf updataTotalPrice];
        if (btn.selected) {
            JYCouponModel * model = self.viewModel.creatOrderModel.coupons[btn.tag - 1000];
            weakSelf.useCouponId = model.couponId;
            weakSelf.couponPrice = model.couponPrice;
            
            //        提前设置好积分状态
            if ([self.couponPrice floatValue] > [self.goodsTotalPrice floatValue]) {
                self.useScore = @"2";
            }
            [weakSelf.orderTableView reloadData];
        }else{
            self.useCouponId = nil;
            self.couponPrice = nil;
        }
        
        [weakSelf updataTotalPrice];
    };
    return couponCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return  [JYUsableCouponCell cellHeightWithCount:self.viewModel.creatOrderModel.coupons.count];
    }
    return 0;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    WEAKSELF
    JYSwitchTableViewCell * switchHeader = LOADXIB(@"JYSwitchTableViewCell");
    switchHeader.switchType = JYSwitchType_Shop;
    switchHeader.titleLab.text = self.itemArr[section];
    switchHeader.switchBtn.tag = section;
    
    if (section == 0){
        switchHeader.detailLab.text = [self.viewModel.creatOrderModel.score intValue] > 0 ? SF(@"共有%@积分 当前订单使用%@",self.viewModel.creatOrderModel.score,self.needIntegral):@"";
        switchHeader.switchBtn.enabled = ([self.viewModel.creatOrderModel.score intValue ] > [self.needIntegral intValue] && [self.viewModel.creatOrderModel.isExchange intValue] == 1) ? YES : NO;
        
        
        //        没有合适的优惠券
        BOOL noCoupon = NO;
        for (JYCouponModel * couponModel in self.viewModel.creatOrderModel.coupons) {
            if ([couponModel.couponPrice intValue] >= [self.goodsTotalPrice intValue]) {
                noCoupon = YES;
            }
        }
        switchHeader.switchBtn.selected = [self.useScore intValue] == 1 ? YES : NO;
        if ((noCoupon == YES && self.useCouponId.length) ||[self.viewModel.creatOrderModel.isExchange intValue] != 1 ) {
            switchHeader.switchBtn.enabled = NO;
            switchHeader.switchBtn.selected = NO;
            self.useScore = @"2";
        }
        //        else{
        //            switchHeader.switchBtn.enabled = YES;
        //        }
        
    }else if (section == 1){
        //        if ( [ self.viewModel.creatOrderModel.score intValue] > [self.needIntegral intValue] ) {
        //            switchHeader.switchBtn.enabled = NO;
        //            switchHeader.switchBtn.selected = NO;
        //        }else{
        //            switchHeader.switchBtn.enabled = YES;
        //        }
        
        if (self.viewModel.creatOrderModel.isExpand) {
            switchHeader.switchBtn.selected = YES;
        }else{
            switchHeader.switchBtn.selected = NO;
        }
        
        
        if (([self.useScore intValue] == 1 && [self.viewModel.creatOrderModel.score intValue] > [self.needIntegral intValue]) || !weakSelf.viewModel.creatOrderModel.coupons.count) {
            switchHeader.switchBtn.enabled = NO;
            switchHeader.switchBtn.selected = NO;
        }else{
            switchHeader.switchBtn.enabled = YES;
        }
        
        switchHeader.detailLab.text = self.viewModel.creatOrderModel.coupons.count ? @"可用":@"不可用";
    }
    
    //    [switchHeader updataSwitchUIWithData:self.viewModel.creatOrderModel section:section];
    switchHeader.rightSwitchBtnActionBlock = ^(UIButton * sender){
        switch (section) {
            case 0:{
                weakSelf.useScore = sender.selected ? @"1" :@"2";
                if (sender.selected &&  [self.viewModel.creatOrderModel.score intValue] > [self.needIntegral intValue]) {
                    weakSelf.viewModel.creatOrderModel.isExpand = NO;
                }else{
                    if (weakSelf.viewModel.creatOrderModel.coupons.count) {
                        weakSelf.viewModel.creatOrderModel.isExpand = YES;
                    }
                }
                
                [weakSelf updataTotalPrice];
                [weakSelf.orderTableView reloadData];
                
            }
                break;
            case 1:{
                weakSelf.viewModel.creatOrderModel.isExpand = sender.selected ? YES : NO;
                if (sender.selected == NO) {
                    weakSelf.useCouponId = @"";
                }
                [weakSelf updataTotalPrice];
                [weakSelf.orderTableView reloadData];
                
                //                [weakSelf.orderTableView reloadData];
            }
                break;
            default:
                break;
        }
    };
    return switchHeader;
}


#pragma mark ==================支付代码==================

//支付方式选择
- (void)payTypeWithStr:(NSString *)str andOrderId:(NSString *)orderId{
    if ([str isEqualToString:@"账户支付"]) {
        NSLog(@"账户支付");
        if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [self.totalPriceLab.text floatValue]) {
            [self showAlertPicViewWithAccountTypeWithOrderId:orderId];
            
        }else{
            [self showAlertPicViewWithNotEnoughMoney];
        }
    }else if([str isEqualToString:@"微信支付"]){
        NSLog(@"微信支付");
        
        if (![WXApi isWXAppInstalled]) {
            [self showSuccessTip:@"请安装微信客户端"];
            return;
        }else{
            [self requestWXSignWithOrderNum:orderId];
        }
    }else{
        NSLog(@"支付宝支付");
        [self requestAlipaySignWithOrderNum:orderId];
    }
}

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            //            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
                //                switch ([code intValue]) {
                //                    case 9000:
                //                        //                        [self popToRootViewControllerAnimated:NO];
                //                    {
                //                        [weakSelf turnPaySuccessPage];
                //                    }
                //                        break;
                //                    case 8000:
                //                        //                        [self showSuccessTip:@"正在处理中"];
                //                        break;
                //                    case 4000:
                //                        //                        [self showSuccessTip:@"支付失败"];
                //                        break;
                //                    case 6001:
                //                        //                        [self showSuccessTip:@"支付已取消"];
                //                        break;
                //                    case 6002:
                //                        //                        [self showSuccessTip:@"网络连接错误"];
                //                        break;
                //                    default:
                //                        //                        [self showSuccessTip:@"支付失败"];
                //                        break;
                //                }
                
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//跳转到支付成功页面
- (void)turnPaySuccessPage{
    //    UIApplication *app = [UIApplication sharedApplication];
    //    JYBaseTabBarController *tabbar = (JYBaseTabBarController*)app.delegate.window.rootViewController;
    //    UINavigationController *nav= tabbar.viewControllers[tabbar.selectedIndex];
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    [self.navigationController pushViewController:successVC animated:YES];
}

- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}

- (JYShopOrderViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYShopOrderViewModel alloc] init];
    }
    return _viewModel;
}

- (JYGetIntegralRatioViewModel *)integralViewModel{
    if(!_integralViewModel){
        _integralViewModel = [[JYGetIntegralRatioViewModel alloc] init];
    }
    return _integralViewModel;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
