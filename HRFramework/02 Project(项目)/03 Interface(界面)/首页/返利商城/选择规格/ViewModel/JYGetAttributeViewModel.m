//
//  JYGetAttributeViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetAttributeViewModel.h"
#import "JYGetAttributeModel.h"

@implementation JYGetAttributeViewModel

//3.7.1.	JY-007-005 获取商品属性
- (void)requestGetAttributeWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_SHOP_GetAttribute) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [weakSelf.attributeArr removeAllObjects];
            NSArray * arr = [JYGetAttributeModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.attributeArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//3.7.1.	JY-007-009 获取商品属性标题
- (void)requestGetAttributeTitleWithParams:(NSMutableDictionary * )params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{

    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"+++%@",JY_PATH(JY_SHOP_GetAttributeTitle));
    [manager POST_URL:JY_PATH(JY_SHOP_GetAttributeTitle) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,RESULT_DATA);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)attributeArr
{
    if (!_attributeArr) {
        _attributeArr = [NSMutableArray array];
    }
    return _attributeArr;
}
@end
