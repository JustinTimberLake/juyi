//
//  JYGetAttributeViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYGetAttributeViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *attributeArr;

//3.7.1.	JY-007-005 获取商品属性
- (void)requestGetAttributeWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.7.1.	JY-007-009 获取商品属性标题
- (void)requestGetAttributeTitleWithParams:(NSMutableDictionary * )params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
