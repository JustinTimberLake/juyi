//
//  JYGetAttributeModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYGetAttributeModel : JYBaseModel
//商品属性ID
@property (nonatomic,copy) NSString *itemId;
//属性值1
@property (nonatomic,copy) NSString *item1;

//属性值2
@property (nonatomic,copy) NSString *item2;

//数量
@property (nonatomic,copy) NSString *num;

//价格
@property (nonatomic,copy) NSString *price;


@end
