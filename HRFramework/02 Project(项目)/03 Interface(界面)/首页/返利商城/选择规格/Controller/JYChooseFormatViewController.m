//
//  JYChooseFormatViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYChooseFormatViewController.h"
#import "JYGetAttributeViewModel.h"
#import "JYGetAttributeModel.h"

@interface JYChooseFormatViewController ()

@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UIView *sizeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colorViewH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizeViewH;
@property (nonatomic,strong) NSArray * colorArr;
@property (nonatomic,strong) NSArray * sizeArr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chooseViewH;
@property (weak, nonatomic) IBOutlet UIView *countView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITextField *countTextField;

@property (nonatomic,strong) JYGetAttributeViewModel *viewModel;

@property (nonatomic,strong) NSMutableArray *item1Arr;
@property (nonatomic,strong) NSMutableArray *item1NewArr;
@property (nonatomic,strong) NSMutableArray *item2Arr;
@property (nonatomic,strong) NSMutableArray *item2NewArr;
@property (nonatomic,strong) NSMutableArray *item1BtnArr;
@property (nonatomic,strong) NSMutableArray *item2BtnArr;

//@property (nonatomic,copy) NSString *itemId;
//@property (nonatomic,assign) NSInteger selectTag;
@property (nonatomic,copy) NSString *selectStr;
@property (nonatomic,copy) NSString *selectItem1Str;
@property (nonatomic,copy) NSString *selectItem2Str;
@property (nonatomic,strong) JYGetAttributeModel * currentModel;
@property (weak, nonatomic) IBOutlet UILabel *titleOneLab;
@property (weak, nonatomic) IBOutlet UILabel *titleTwoLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *countViewH;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itme2TopH;
@property (weak, nonatomic) IBOutlet UIView *blackView;


@end

@implementation JYChooseFormatViewController

static int const count = 5;
static int const margin = 17;
static int const padding = 10;

//static CGFloat  const BtnMargin = 15;
static CGFloat  const BtnWidth = 80;
static CGFloat  const BtnHeight = 34;
static CGFloat  const BtnLeftDistance = 20;
static CGFloat  const FormatTitleH = 45;

#define BtnW (SCREEN_WIDTH - margin * 2 - (count - 1)* padding ) / count
#define BtnH 30



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
//    [self configData];
    self.chooseViewH.constant = 0;
    [self configUI];
//    [self requestAttributeTitle];
    [self requestAttribute];
    
    
}


//- (void)updataTitleWithItem1Name:(NSString *)itme1Name andItem2Name:(NSString *)item2Name{
//     self.titleOneLab.text = itme1Name;
//     self.titleTwoLab.text = item2Name;
//     self.titleOneStr = itme1Name;
//     self.titleTwoStr = item2Name;
//}

//- (void)configData{
//    self.colorArr = @[@"红色",@"橙色",@"黄色",@"绿色"];
//    self.sizeArr = @[@"XL",@"XXL",@"XXXL"];
//}
//
//
//
- (void)configUI{
     self.titleOneLab.text = self.titleOneStr;
     self.titleTwoLab.text = self.titleTwoStr;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self.blackView addGestureRecognizer:tap];
    
//     self.titleOneStr = itme1Name;
//     self.titleTwoStr = item2Name;
//  self.colorViewH.constant = [self creatBtnandGetHeightWithView:self.colorView andDataArr:self.colorArr];
//    self.sizeViewH.constant = [self creatBtnandGetHeightWithView:self.sizeView andDataArr:self.sizeArr];
//    self.chooseViewH.constant = 45 * 2 + self.colorViewH.constant + self.sizeViewH .constant + 60;
}

#pragma mark ----------网络请求------------

//- (void)requestAttributeTitle{
//    WEAKSELF
//    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
//    dic[@"type"] = self.typeStr;// 1实体服务2商品
//    dic[@"goodsId"] = self.goodsId;
//
//     [self.viewModel requestGetAttributeTitleWithParams:dic success:^(NSString *msg, id responseData) {
//         NSString * titleOneStr = responseData[@"item1name"];
//         NSString * titleTwoStr = responseData[@"item2name"];
//         weakSelf.titleOneLab.text = titleOneStr;
//         weakSelf.titleTwoLab.text = titleTwoStr;
//     } failure:^(NSString *errorMsg) {
//         [weakSelf showSuccessTip:errorMsg];
//     }];
//}
- (void)requestAttribute{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = self.typeStr;
    dic[@"goodsId"] = self.goodsId;
     [self.viewModel requestGetAttributeWithParams:dic success:^(NSString *msg, id responseData) {
         [weakSelf updataFormatUIWithTypeStr:weakSelf.typeStr];
     } failure:^(NSString *errorMsg) {
         [weakSelf showSuccessTip:errorMsg];
     }];
}


- (void)deleteRepeatItemWithArr:(NSMutableArray *)arr andNewArr:(NSMutableArray *)newArr{
    for (unsigned i = 0; i < [arr count]; i++){
        if ([newArr containsObject:[arr objectAtIndex:i]] == NO){
            [newArr addObject:[arr objectAtIndex:i]];
        }
    }
}





- (CGFloat)creatBtnandGetHeightWithView:(UIView *)view andDataArr:(NSArray *)dataArr andIndex:(NSInteger)index andBtnArr:(NSMutableArray *)btnArr {
    int row;
    int line ;
    int h;
    if (!dataArr.count) {
        return 0;
    }
    for (int i = 0; i < dataArr.count; i++) {
        row = i % count;
        line = i / count;
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:dataArr[i] forState:UIControlStateNormal];
        btn.layer.cornerRadius = 3;
        btn.clipsToBounds = YES;
        btn.tag = index +i;
//        btn.frame = CGRectMake(margin + (BtnW + padding) * row,(BtnH + padding / 2) * line, BtnW, BtnH);
        CGFloat textWidth =  [NSString widthOfText:dataArr[i] textHeight:MAXFLOAT font:FONT(13)];
        btn.width = textWidth + 2 * BtnLeftDistance;
        btn.height = BtnHeight;
        btn.backgroundColor = RGB(242, 242, 242);
        btn.titleLabel.font = FONT(13);
        [btn setTitleColor:RGB0X(0x333333) forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
//        [view addSubview:btn];
        [btnArr addObject:btn];
        
        UIButton * newBtn = btnArr[i];
        if (i == 0) {
            newBtn.x = 0;
            newBtn.y = 0;
        }else{
            UIButton * previewBtn = btnArr[i - 1];
            CGFloat leftWidth = CGRectGetMaxX(previewBtn.frame) + margin;
            CGFloat rightWidth = view.width - leftWidth;
            if (rightWidth >= BtnWidth) {
                newBtn.x = leftWidth;
                newBtn.y = previewBtn.y;
            }else{
                newBtn.x = 0;
                newBtn.y = margin + CGRectGetMaxY(previewBtn.frame);
            }
            
        }
        [view addSubview:newBtn];
    }
    UIButton * lastBtn = view.subviews.lastObject;
    h = CGRectGetMaxY(lastBtn.frame) + 15;
    return h;
}

#pragma mark ==================更新UI==================

//根据实体还是商品更新UI
- (void)updataFormatUIWithTypeStr:(NSString *)typeStr{
//    if ([typeStr intValue] == 1) {
//
//    }else{
        for (JYGetAttributeModel * model in self.viewModel.attributeArr) {
            [self.item1Arr addObject:model.item1];
            [self.item2Arr addObject:model.item2];
        }
        
        [self deleteRepeatItemWithArr:self.item1Arr andNewArr:self.item1NewArr];
        [self deleteRepeatItemWithArr:self.item2Arr andNewArr:self.item2NewArr];
        
    self.colorViewH.constant = self.titleOneStr.length ? [self creatBtnandGetHeightWithView:self.colorView andDataArr:self.item1NewArr andIndex:1000 andBtnArr:self.item1BtnArr] : 0;
    self.itme2TopH.constant = self.titleTwoStr.length ? 47 : 0;
    self.sizeViewH.constant =  self.titleTwoStr.length ? [self creatBtnandGetHeightWithView:self.sizeView andDataArr:self.item2NewArr andIndex:2000 andBtnArr:self.item2BtnArr] : 0;
        
//        self.chooseViewH.constant = 45 * 2 + self.colorViewH.constant + self.sizeViewH .constant + 60;
    
//    }
    self.countView.hidden = ([typeStr intValue] == 1) ? YES : NO;
    self.countViewH.constant = ([typeStr intValue] == 1) ? 0 : 60;
    
    CGFloat height = 0.0;
    self.titleOneLab.hidden = self.titleOneStr.length ? NO : YES;
    self.titleTwoLab.hidden = self.titleTwoStr.length ? NO : YES;
  
    
    
//    if ([typeStr intValue] == 2)
        //                商品
        if (!self.titleOneStr.length && !self.titleTwoStr.length) {
            height = 0;
        }else if (self.titleOneStr.length && !self.titleTwoStr.length){
            //                只有一个属性， 和数量
            height = FormatTitleH;
        }else {
            //                俩属性都有 和数量
            height = FormatTitleH * 2;
        }
    
     self.chooseViewH.constant = height+ self.colorViewH.constant + self.sizeViewH .constant + self.countViewH.constant;

//    }else if([typeStr intValue] == 1){
        //                都不显示数量

        //                实体
//        if (!self.titleOneStr.length && !self.titleTwoStr.length) {

            //                没有属性，都不显示弹窗
//        }else if (self.titleOneStr.length && !self.titleTwoStr.length){
//            //                只有一个属性
//        }
//    }

}

#pragma mark ----------Action------------
- (void)btnAction:(UIButton *)btn{
    if (btn.tag >= 2000) {
      self.selectItem2Str =   [self returnTitleSelectOnlyOneItem:btn WithArr:self.item2BtnArr];
    }else{
       self.selectItem1Str =  [self returnTitleSelectOnlyOneItem:btn WithArr:self.item1BtnArr];
    }
    
    if (self.titleOneStr.length && self.titleTwoStr.length ) {
        for (JYGetAttributeModel * model in self.viewModel.attributeArr) {
            if ([model.item1 isEqualToString:self.selectItem1Str] && [model.item2 isEqualToString:self.selectItem2Str]) {
                self.currentModel = model;
            }
        }
    }else if(self.titleOneStr.length && !self.titleTwoStr.length){
        for (JYGetAttributeModel * model in self.viewModel.attributeArr) {
            if ([model.item1 isEqualToString:self.selectItem1Str]) {
                self.currentModel = model;
            }
        }

    }else{
        JYGetAttributeModel * model  =  [[JYGetAttributeModel alloc] init];
        model.item1 = @"";
        model.item2 = @"";
        model.num = @"1";
        model.price = @"";
        self.currentModel = model;
    }
    self.countTextField.text = @"1";

}

- (NSString *)returnTitleSelectOnlyOneItem:(UIButton *)btn WithArr:(NSArray *)arr {
    WEAKSELF
    [arr enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.tag != btn.tag) {
            [obj setTitleColor:RGB0X(0x666666) forState:UIControlStateNormal];
            [obj setBackgroundColor:RGB(242, 242, 242)];
        }else{
            [obj setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [obj setBackgroundColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN]];
            weakSelf.selectStr = obj.currentTitle;
        }
    }];
    return self.selectStr;
}

//立即购买
- (IBAction)buyBtnAction:(UIButton *)sender {
    
#pragma mark -----------暂时注释  duanhuifen----------------
//    if (!self.selectItem1Str.length) {
//        [self showSuccessTip:SF(@"请选择%@",self.titleOneLab.text)];
//        return;
//    }
//    if (!self.selectItem2Str.length) {
//        [self showSuccessTip:SF(@"请选择%@",self.titleTwoLab.text)];
//        return;
//    }
    //        先判断是否选择了属性
    if (self.titleOneStr.length && !self.selectItem1Str.length) {
        [self showSuccessTip:SF(@"请选择%@",self.titleOneStr)];
        return;
    }
    
    if (self.titleTwoStr.length && !self.selectItem2Str.length) {
        [self showSuccessTip:SF(@"请选择%@",self.titleTwoStr)];
        return;
    }
    
    
    if (_commitBtnActionBlock) {
        self.commitBtnActionBlock(self.currentModel,self.countTextField.text);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//数量加减
- (IBAction)minusOrPlusBtnAction:(UIButton *)sender {
    
    int count = [self.countTextField.text floatValue];
    
    if (sender.tag == 1000) {
        //        减
        if (count == 1) {
            [self showSuccessTip:@"不能再减了"];
            return;
        }
        count--;
        
        
    }else{
//        先判断是否选择了属性
        if (self.titleOneStr.length && !self.selectItem1Str.length) {
            [self showSuccessTip:SF(@"请选择%@",self.titleOneStr)];
            return;
        }
        
        if (self.titleTwoStr.length && !self.selectItem2Str.length) {
            [self showSuccessTip:SF(@"请选择%@",self.titleTwoStr)];
            return;
        }
        
        if (count == [self.currentModel.num intValue]) {
            [self showSuccessTip:@"库存不够了"];
            return;
        }
        count++;
        //        加
        
    }
    self.countTextField.text = SF(@"%d",count);
}

- (void)tap:(UITapGestureRecognizer *)tap{
    [self dismissViewControllerAnimated:YES completion:nil];
}

///*
// * detailType ==  1实体 2商品
// */
//- (void)updataFormatUIWithDetailType:(NSString *)detailType andItem1:(NSString *)item1 andItem2:(NSString *)item2{
//
//}




//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark ----------lazy------------
- (JYGetAttributeViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYGetAttributeViewModel alloc] init];
    }
    return _viewModel;
}

- (NSMutableArray *)item1Arr
{
    if (!_item1Arr) {
        _item1Arr = [NSMutableArray array];
    }
    return _item1Arr;
}

- (NSMutableArray *)item1NewArr
{
    if (!_item1NewArr) {
        _item1NewArr = [NSMutableArray array];
    }
    return _item1NewArr;
}

- (NSMutableArray *)item2Arr
{
    if (!_item2Arr) {
        _item2Arr = [NSMutableArray array];
    }
    return _item2Arr;
}

- (NSMutableArray *)item2NewArr
{
    if (!_item2NewArr) {
        _item2NewArr = [NSMutableArray array];
    }
    return _item2NewArr;
}

- (NSMutableArray *)item1BtnArr
{
    if (!_item1BtnArr) {
        _item1BtnArr = [NSMutableArray array];
    }
    return _item1BtnArr;
}

- (NSMutableArray *)item2BtnArr
{
    if (!_item2BtnArr) {
        _item2BtnArr = [NSMutableArray array];
    }
    return _item2BtnArr;
}


@end
