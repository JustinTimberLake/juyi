//
//  JYProductListViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductListViewController.h"
#import "JYTurnSearchView.h"
#import "JYSearchViewController.h"
#import "JYProductCell.h"
#import "JYProductListViewModel.h"
#import "JYDetailViewController.h"

static NSString *const ProductCellId = @"JYProductCell";

@interface JYProductListViewController ()<
    UICollectionViewDataSource,
    UICollectionViewDelegate
>

@property (nonatomic,strong) JYTurnSearchView * searchView;
@property (weak, nonatomic) IBOutlet UICollectionView *productCollectionView;
@property (nonatomic,strong) JYProductListViewModel *viewModel;

@end

@implementation JYProductListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setUpRefreshData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.searchView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.searchView.hidden = YES;
}

- (void)configUI{
    WEAKSELF
//    [self.navigationController.view addSubview:self.searchView];
//    self.searchView.placeHolder = @"请输入关键字";
//
//    self.searchView.startTurnSearchActionBlock = ^{
//        JYSearchViewController * searchVC = [[JYSearchViewController alloc]init];
//        [weakSelf.navigationController pushViewController:searchVC animated:YES];
//    };
    
    self.navigationItem.titleView = self.searchView;
    self.searchView.placeHolder = @"请输入关键字";
    
    self.searchView.startTurnSearchActionBlock = ^{
        JYSearchViewController * searchVC = [[JYSearchViewController alloc]init];
        searchVC.searchType = SearchTypeProduct;
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
    };
    
    [self rightItemTitle:@"" color:[UIColor whiteColor] font:FONT(15) action:^{
        
    }];
    
    [self.productCollectionView registerNib:[UINib nibWithNibName:@"JYProductCell" bundle:nil] forCellWithReuseIdentifier:ProductCellId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ------------网络请求-----------

- (void)setUpRefreshData{
    WEAKSELF
    self.productCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestGoodsListIsMore:NO];
    }];
    self.productCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestGoodsListIsMore:YES];
    }];
    [self.productCollectionView.mj_header beginRefreshing];
}

- (void)endRefresh{
    [self.productCollectionView.mj_header endRefreshing];
    [self.productCollectionView.mj_footer endRefreshing];
}


- (void)requestGoodsListIsMore:(BOOL)isMore{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(self.queryType);
    dic[@"classifyId"] = self.classifyId;
    dic[@"secondaryId"] = self.secondaryId;
    dic[@"shopId"] = self.shopId;
    [self.viewModel requestGoodsListWithParams:dic isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.productCollectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf endRefresh];
    }];
}

#pragma mark ------------collectionView 数据源和代理-----------


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.goodsArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYProductCell * productCell = [collectionView dequeueReusableCellWithReuseIdentifier:ProductCellId  forIndexPath:indexPath];
    productCell.model = self.viewModel.goodsArr[indexPath.item];
    return productCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   JYMyProductModel * model = self.viewModel.goodsArr[indexPath.item];
    JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
    detailVC.detailType = DetailTypeStore;
    detailVC.goodsId = model.goodsId;
    [self.navigationController pushViewController:detailVC animated:YES];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - 20 - 5  )/2,  [JYProductCell cellH]);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

#pragma mark-------------lazy-----------------

- (JYTurnSearchView *)searchView
{
    if (!_searchView) {
        _searchView = [[JYTurnSearchView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH - 50-50, 44)];
    }
    return _searchView;
}

- (JYProductListViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYProductListViewModel alloc] init];
    }
    return _viewModel;
}
@end
