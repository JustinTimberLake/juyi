//
//  JYProductListViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,JYQueryType) {
    JYQueryType_Classity = 1,//分类查询
    JYQueryType_Store//商城查询
};

@interface JYProductListViewController : JYBaseViewController

@property (nonatomic,assign) JYQueryType queryType;

#pragma mark -----------根据分类查询的参数---------------
//一级分类
@property (nonatomic,copy) NSString *classifyId;
//二级分类
@property (nonatomic,copy) NSString *secondaryId;

#pragma mark -----------根据商城查询参数---------------
//商城id
@property (nonatomic,copy) NSString *shopId;


@end
