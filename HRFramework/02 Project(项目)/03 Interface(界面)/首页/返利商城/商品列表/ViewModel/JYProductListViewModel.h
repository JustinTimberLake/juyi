//
//  JYProductListViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYProductListViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *goodsArr;

@property (nonatomic,strong) NSMutableArray *keepCarGoodsArr;
//请求商品列表
- (void)requestGoodsListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//请求养车店商品列表
- (void)requestKeepGoodsListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
