//
//  JYProductListViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductListViewModel.h"
#import "JYMyProductModel.h"

@implementation JYProductListViewModel

//请求商品列表
- (void)requestGoodsListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    
    NSString * pageCount;
    if (self.goodsArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.goodsArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.goodsArr.count/10+2 ] : @"1";
    }
    params[@"pageNum"] = pageCount;
    params[@"pageSize"] = @(10);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_SHOP_GetGoodsList));
    [manager POST_URL:JY_PATH(JY_SHOP_GetGoodsList) params:params success:^(id result) {
        
        NSLog(@"%@",result);
        
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [self.goodsArr removeAllObjects];
            }
            NSArray * arr = [JYMyProductModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            
            [weakSelf.goodsArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//请求养车店商品列表
- (void)requestKeepGoodsListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSString * pageCount;
    if (self.goodsArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.goodsArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.goodsArr.count/10+2 ] : @"1";
    }
    params[@"pageNum"] = pageCount;
    params[@"pageSize"] = @(10);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_KEEPCAR_GetKeepGoodsList) params:params success:^(id result) {
        
        NSLog(@"%@",result);
        
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [self.keepCarGoodsArr removeAllObjects];
            }
            NSArray * arr = [JYMyProductModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            
            [weakSelf.keepCarGoodsArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (NSMutableArray *)keepCarGoodsArr
{
    if (!_keepCarGoodsArr) {
        _keepCarGoodsArr = [NSMutableArray array];
    }
    return _keepCarGoodsArr;
}

- (NSMutableArray *)goodsArr
{
    if (!_goodsArr) {
        _goodsArr = [NSMutableArray array];
    }
    return _goodsArr;
}
@end
