//
//  JYGetHomeTopViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetHomeTopViewModel.h"
#import "JYMyProductModel.h"

@implementation JYGetHomeTopViewModel
//获取首页推广接口
- (void)requestGetHomeTopWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_SHOP_GetHomeTop));
    [manager POST_URL:JY_PATH(JY_SHOP_GetHomeTop) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [weakSelf.goodsArr removeAllObjects];
            NSArray * arr = [JYMyProductModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [self.goodsArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//JY-007-010 获取商城首页/店铺首页商品列表
- (void)requestGetHomeGoodsListWithType:(HomeGoodsListType)homeGoodsListType andShopId:(NSString *)shopId isMore:(BOOL)isMore sucess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(homeGoodsListType);
    dic[@"ShopId"] = shopId;
    dic[@"pageSize"] = @(10);
    NSString * pageCount;
    if (weakSelf.goodsListArr.count % 10 == 0) {
        pageCount = isMore ? SF(@"%lu",weakSelf.goodsListArr.count / 10 + 1):@"1";
    }else{
         pageCount = isMore ? SF(@"%lu",weakSelf.goodsListArr.count / 10 + 2):@"1";
    }
    dic[@"pageNum"] = pageCount;
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@--%@",JY_PATH(JY_SHOP_GetHomeGoodsList),dic);
    [manager POST_URL:JY_PATH(JY_SHOP_GetHomeGoodsList) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.goodsListArr removeAllObjects];
            }
            NSArray * arr = [JYMyProductModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.goodsListArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);

        }else{
            failureBlock(RESULT_MESSAGE);
        }
        
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray *)goodsListArr
{
    if (!_goodsListArr) {
        _goodsListArr = [NSMutableArray array];
    }
    return _goodsListArr;
}

- (NSMutableArray *)goodsArr
{
    if (!_goodsArr) {
        _goodsArr = [NSMutableArray array];
    }
    return _goodsArr;
}
@end
