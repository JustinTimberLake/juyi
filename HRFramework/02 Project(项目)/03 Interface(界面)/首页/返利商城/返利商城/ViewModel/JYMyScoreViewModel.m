//
//  JYMyScoreViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/12/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyScoreViewModel.h"

@implementation JYMyScoreViewModel

//获取我的积分
- (void)requestGetMyPointsSuccessBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_USER_GetMyPoints) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSString * myPoints = [result[@"data"][@"userScore"] stringValue];
            successBlock(RESULT_MESSAGE,myPoints);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
