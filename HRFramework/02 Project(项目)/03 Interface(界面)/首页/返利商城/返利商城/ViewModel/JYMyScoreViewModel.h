//
//  JYMyScoreViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYMyScoreViewModel : JYBaseViewModel

//获取我的积分
- (void)requestGetMyPointsSuccessBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock;

@end
