//
//  JYGetHomeTopViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

typedef NS_ENUM(NSInteger,HomeGoodsListType) {
    HomeGoodsListType_Store = 1, //商城首页
    HomeGoodsListType_Shop       //店铺首页
};

@interface JYGetHomeTopViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *goodsArr;
@property (nonatomic,strong) NSMutableArray *goodsListArr; //列表数组
//获取首页推广接口
- (void)requestGetHomeTopWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//JY-007-010 获取商城首页/店铺首页商品列表
- (void)requestGetHomeGoodsListWithType:(HomeGoodsListType)homeGoodsListType andShopId:(NSString *)shopId isMore:(BOOL)isMore sucess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
