//
//  JYStoreCollectionHeaderView.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYStoreCollectionHeaderView.h"
#import "SDCycleScrollView.h"
#import "JYMyProductModel.h"
#import "JYHomeBannerModel.h"

@interface JYStoreCollectionHeaderView ()<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerViewH;
@property (weak, nonatomic) IBOutlet UIView *myIntegrateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myIntegrateViewH;

@property (nonatomic,strong) SDCycleScrollView * scrollView;
@property (weak, nonatomic) IBOutlet UIView *integrateView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *integrateViewH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *integrateTopH;

//第一个相关产品
@property (weak, nonatomic) IBOutlet UIImageView *oneImageView;
@property (weak, nonatomic) IBOutlet UILabel *oneTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *oneSubTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *onePriceLab;
@property (weak, nonatomic) IBOutlet UILabel *oneIntLab;
@property (weak, nonatomic) IBOutlet UILabel *oneMainTitleLab;

//第二个相关产品
@property (weak, nonatomic) IBOutlet UIImageView *twoLeftImageView;
@property (weak, nonatomic) IBOutlet UILabel *twoLeftTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *twoLeftPriceLab;
@property (weak, nonatomic) IBOutlet UILabel *twoLeftIntLab;

@property (weak, nonatomic) IBOutlet UIImageView *twoRightImageView;
@property (weak, nonatomic) IBOutlet UILabel *twoRightTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *twoPriceLab;
@property (weak, nonatomic) IBOutlet UILabel *twoRightIntLab;

//第三个商品
@property (weak, nonatomic) IBOutlet UILabel *threeLeftTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *threeLeftSubTitleLab;
@property (weak, nonatomic) IBOutlet UIImageView *threeLeftImageView;

@property (weak, nonatomic) IBOutlet UILabel *threeRightTopTitleLab;
@property (weak, nonatomic) IBOutlet UIImageView *threeRightTopImageView;
@property (weak, nonatomic) IBOutlet UILabel *threeRightTopSubTitleLab;

@property (weak, nonatomic) IBOutlet UILabel *threeRightBottomTitleLab;
@property (weak, nonatomic) IBOutlet UIImageView *threeRightBottomImageView;
@property (weak, nonatomic) IBOutlet UILabel *threeRightBottomSubTitleLab;


//主页面的商品展示区
@property (weak, nonatomic) IBOutlet UIView *mainProductView;
//一个商品的图
@property (weak, nonatomic) IBOutlet UIView *oneProductView;
//两个商品的图
@property (weak, nonatomic) IBOutlet UIView *twoProductView;
//三个商品的图
@property (weak, nonatomic) IBOutlet UIView *threeProductView;





@end

@implementation JYStoreCollectionHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.myJiFenLab.text = [User_InfoShared shareUserInfo].personalModel.userScore;
    [self creatBannerView];
}

- (void)hideHomeTopTitle{
    self.oneTitleLab.hidden = YES;
    self.oneSubTitleLab.hidden = YES;
    self.onePriceLab.hidden = YES;
    self.oneIntLab.hidden = YES;
    self.oneMainTitleLab.hidden = YES;
    
    self.twoLeftTitleLab.hidden = YES;
    self.twoLeftPriceLab.hidden = YES;
    self.twoLeftIntLab.hidden = YES;
    self.twoRightTitleLab.hidden = YES;
    self.twoPriceLab.hidden = YES;
    self.twoRightIntLab.hidden = YES;
    
    self.threeLeftTitleLab.hidden = YES;
    self.threeLeftSubTitleLab.hidden = YES;
    self.threeRightTopTitleLab.hidden = YES;
    self.threeRightTopSubTitleLab.hidden = YES;
    self.threeRightBottomTitleLab.hidden = YES;
    self.threeRightBottomSubTitleLab.hidden = YES;

}

- (void)hideTopHeader{
    self.bannerView.hidden = YES;
    self.bannerViewH.constant = 0;
    
    self.myIntegrateView.hidden = YES;
    self.myIntegrateViewH.constant = 0;
    
    self.integrateView.hidden = YES;
    self.integrateViewH.constant = 0;
    self.integrateTopH.constant = 0;
}


- (void)creatBannerView{
    self.scrollView = [SDCycleScrollView cycleScrollViewWithFrame:(CGRectMake(0, 0, SCREEN_WIDTH, HB_IPONE6_ADAPTOR_WIDTH(190))) delegate:self  placeholderImage:[UIImage imageNamed:@"商城活动推荐位3"]];
    self.scrollView.delegate = self; // 如需监听图片点击，请设置代理，实现代理方法
    self.scrollView.autoScrollTimeInterval = 3;// 自定义轮播时间间隔
    self.scrollView.titleLabelBackgroundColor = [UIColor clearColor];
    self.scrollView.showPageControl = YES;
    self.scrollView.currentPageDotImage = [UIImage imageNamed:@"轮播-选中"];
    self.scrollView.pageDotImage = [UIImage imageNamed:@"轮播-未选中"];
    [self.bannerView addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor whiteColor];
}

- (void)updataImageWithArr:(NSArray *)arr{
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYHomeBannerModel * model in arr) {
        
        [imageArr addObject:model.bannerImage];
    }
    self.scrollView.imageURLStringsGroup = imageArr;
}

- (void)setGoodsArr:(NSArray *)goodsArr{
    self.oneProductView.hidden = goodsArr.count == 1 ? NO : YES;
    self.twoProductView.hidden = goodsArr.count == 2 ? NO : YES;
    self.threeProductView.hidden = goodsArr.count == 3? NO : YES;

    [self hideHomeTopTitle];
    
//    现在返回只有3条数据
    if (goodsArr.count == 1 ) {
        JYMyProductModel * model = goodsArr[0];
        self.oneTitleLab.text = model.goodsTitle;
        self.oneSubTitleLab.text = model.goodsSubtitle;
        [self.oneImageView sd_setImageWithURL:[NSURL URLWithString:model.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位3"]];
        self.onePriceLab.text = SF(@"￥%@",model.goodsPrice);
        self.oneIntLab.text = SF(@"%@积分",model.goodsScore);
        self.oneMainTitleLab.text = model.goodsTitle;
    }
    
    if (goodsArr.count == 2) {
        JYMyProductModel * model = goodsArr[0];
        [self.twoLeftImageView sd_setImageWithURL:[NSURL URLWithString:model.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
        self.twoLeftTitleLab.text = model.goodsTitle;
        self.twoLeftPriceLab.text = SF(@"￥%@",model.goodsPrice);
        self.twoLeftIntLab.text =SF(@"%@积分",model.goodsScore);
        
        JYMyProductModel * rightModel = goodsArr[1];
        [self.twoRightImageView sd_setImageWithURL:[NSURL URLWithString:rightModel.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
        self.twoRightTitleLab.text = rightModel.goodsTitle;
        self.twoPriceLab.text = SF(@"￥%@",rightModel.goodsPrice);
        self.twoRightIntLab.text =SF(@"%@积分",rightModel.goodsScore);

    }
    
    if (goodsArr.count == 3) {
        JYMyProductModel * model = goodsArr[0];
        self.threeLeftTitleLab.text = model.goodsTitle;
        self.threeLeftSubTitleLab.text = model.goodsSubtitle;
        [self.threeLeftImageView sd_setImageWithURL:[NSURL URLWithString:model.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
        
        JYMyProductModel * model1 = goodsArr[1];
        self.threeRightTopTitleLab.text = model1.goodsTitle;
        self.threeRightTopSubTitleLab.text = model1.goodsSubtitle;
        [self.threeRightTopImageView sd_setImageWithURL:[NSURL URLWithString:model1.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位1"]];
        
        JYMyProductModel * model2 = goodsArr[2];
        self.threeRightBottomTitleLab.text = model2.goodsTitle;
        self.threeRightBottomSubTitleLab.text = model2.goodsSubtitle;
        [self.threeRightBottomImageView sd_setImageWithURL:[NSURL URLWithString:model2.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位1"]];
    }
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (_didselectImageWithIndexBlock) {
        self.didselectImageWithIndexBlock(index);
    }
}

- (IBAction)lookMyOrderBtnAction:(UIButton *)sender {
    if (_lookMyOrderBlock) {
        self.lookMyOrderBlock();
    }
}

- (IBAction)homeProductBtnAction:(UIButton *)sender {
    if (_homeProductBtnActionBlock) {
        self.homeProductBtnActionBlock(sender.tag);
    }
}



+ (CGFloat)heightWithContainBannerAndTitle:(BOOL)isContain andGoods:(NSArray *)goods{
    CGFloat picH = 0.0;
    
    if (goods.count == 1) {
         picH =  (SCREEN_WIDTH - 20) * 350/710 + 10*2 ;
    }else if (goods.count == 2){
          picH = (SCREEN_WIDTH - 20 - 5 )/2 + 10*2 ;
    }else if(goods.count == 3){
        picH =  (SCREEN_WIDTH - 25)/2 +10*2;
    }
    
    if (isContain) {
        CGFloat bannerH = HB_IPONE6_ADAPTOR_WIDTH(190);
        CGFloat integrateH = 50 + 10;
        CGFloat integrateTitleH = 55;
        return bannerH +integrateH +integrateTitleH + picH;
    }else{
         return picH;
    }
}

@end
