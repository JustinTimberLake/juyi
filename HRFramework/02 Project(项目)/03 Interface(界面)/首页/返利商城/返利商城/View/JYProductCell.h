//
//  JYProductCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYMyProductModel.h"
@interface JYProductCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *productNameLB;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLB;
@property (weak, nonatomic) IBOutlet UILabel *piontsLB;//积分
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
//@property (nonatomic,assign) CGFloat cellH;

@property (strong, nonatomic) JYMyProductModel * model;

+ (CGFloat)cellH;

- (void)setHighLightWithKeywords:(NSString *)keywords;

@end
