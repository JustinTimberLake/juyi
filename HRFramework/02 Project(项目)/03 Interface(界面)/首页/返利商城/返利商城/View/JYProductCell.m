//
//  JYProductCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductCell.h"

static CGFloat const topMargin = 15;


@implementation JYProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(JYMyProductModel *)model{
    _model = model;
    [_productImageView sd_setImageWithURL:[NSURL URLWithString:_model.goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
    _productNameLB.text = [_model.goodsTitle placeholder:@"暂无"];
    _productPriceLB.text = [[NSString stringWithFormat:@"¥ %@",_model.goodsPrice] placeholder:@"暂无"];
    _piontsLB.text = [[NSString stringWithFormat:@"%@积分",_model.goodsScore] placeholder:@"暂无"];
}


+ (CGFloat)cellH{
   return (SCREEN_WIDTH - 20 - 5 )/2 + 36 +21 + topMargin * 3;
}

- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.productNameLB setLabelTitleHighlight:_model.goodsTitle andSearchStr:keywords];
}
@end
