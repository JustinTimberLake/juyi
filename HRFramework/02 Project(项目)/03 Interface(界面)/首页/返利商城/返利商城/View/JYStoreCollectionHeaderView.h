//
//  JYStoreCollectionHeaderView.h
//  JY
//
//  Created by duanhuifen on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYStoreCollectionHeaderView : UICollectionReusableView

@property (nonatomic, assign , getter=isHideTopHeader) BOOL hideTopHeader;

@property (nonatomic,strong) NSArray *goodsArr;
//图片点击index
@property (nonatomic,copy) void(^didselectImageWithIndexBlock)(NSInteger index);

- (void)hideTopHeader;
//更新图片数据
- (void)updataImageWithArr:(NSArray *)arr;

+ (CGFloat)heightWithContainBannerAndTitle:(BOOL)isContain andGoods:(NSArray *)goods;

//查看我的订单
@property (nonatomic,copy) void(^lookMyOrderBlock)();
//首页商品按钮点击block
@property (nonatomic,copy) void(^homeProductBtnActionBlock)(NSInteger tag);
@property (weak, nonatomic) IBOutlet UILabel *myJiFenLab;
@end
