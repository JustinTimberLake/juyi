//
//  JYMapViewController.h
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,JYCurrentMapMode) {
    JYCurrentMapModeMaintainCar,//养车周边地图页面
    JYCurrentMapModeAddOil,//加油地图页面
    JYCurrentMapModeWeiZhangGaoFa//违章高发地
};

@interface JYMapViewController : JYBaseViewController
@property (nonatomic,assign) JYCurrentMapMode currentMapMode;

@property (nonatomic,assign) NSInteger storeType;

@end
