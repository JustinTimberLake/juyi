//
//  JYMapViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.

#import "JYMapViewController.h"
#import "JYTurnSearchView.h"
#import "JYSearchViewController.h"
#import "JYMapPaopaoView.h"
#import "JYSearchViewModel.h"
#import "JYHomeViewModel.h"
#import "JYShopListModel.h"
#import "JYKeepCarViewModel.h"
#import "JYAddOilViewModel.h"
#import "JYDetailViewController.h"
#import "JYStoreViewController.h"
#import "JYAnnotationView.h"
#import "JYLocationAnnotation.h"
#import "JYWeiZhangGaoFaViewModel.h"
#import "JYJUHEWeiZhangGaoFaModel.h"
//#import "JYPointAnnotation.h"

#import <MapKit/MapKit.h>

#import <BaiduMapAPI_Base/BMKBaseComponent.h>//引入base相关所有的头文件
#import <BaiduMapAPI_Map/BMKMapComponent.h>//引入地图功能所有的头文件
#import <BaiduMapAPI_Search/BMKSearchComponent.h>//引入检索功能所有的头文件
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Radar/BMKRadarComponent.h>//引入周边雷达功能所有的头文件
#import <BaiduMapAPI_Map/BMKMapView.h>//只引入所需的单个头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件

@interface JYMapViewController ()<
    BMKMapViewDelegate,
    BMKLocationServiceDelegate,
    BMKGeoCodeSearchDelegate,
    BMKPoiSearchDelegate
    >

@property (nonatomic,strong) JYTurnSearchView * searchView;

@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (weak, nonatomic) IBOutlet UILabel *distanceLab;
@property (weak, nonatomic) IBOutlet UIView *starView;

@property (weak, nonatomic) IBOutlet UIView *underView;
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (nonatomic, strong) BMKMapView *mapview;
@property (nonatomic, strong) BMKLocationService *locationService;
@property (nonatomic, assign) CLLocationCoordinate2D locationPt;//坐标(经纬度)
@property (nonatomic, assign) CLLocationCoordinate2D moveLocationPt;//移动时坐标(经纬度)
@property (nonatomic, assign) CGFloat currentAngle;//当前手机角度(定位图标跟随使用)
@property (nonatomic,strong) JYAnnotationView *selectAnnotation;
@property (nonatomic,strong) BMKPoiSearch *poiSearch;
@property (nonatomic,copy) NSString *keywords;
@property (nonatomic,strong) NSMutableArray *annotationViews;
@property (nonatomic,strong) BMKGeoCodeSearch *searcher;

@property (nonatomic,strong) JYSearchViewModel *searchViewModel;
@property (nonatomic,assign) JYSearchType searchType;
@property (nonatomic,strong) JYHomeViewModel *homeViewModel;
@property (nonatomic,strong) JYKeepCarViewModel *keepCarViewModel;
@property (nonatomic,strong) JYAddOilViewModel *addOilViewModel;
@property (nonatomic,copy) NSString *localCityName;
@property (nonatomic,strong) NSMutableArray *dataArr; //页面大头针模型数组
@property (nonatomic,strong) JYShopListModel * detailModel; //详情model
@property (nonatomic,strong) JYJUHEWeiZhangGaoFaModel *gaofaModel; //违章高发model
//@property (nonatomic,copy) NSString *cityCode;

//@property (nonatomic,copy) NSString *lat;
//@property (nonatomic,copy) NSString *lng;

@property (nonatomic,strong) JYLocationAnnotation *locAnnotation;
@property (nonatomic,strong) BMKAnnotationView *locAnnotationView;
@property (nonatomic,strong) UIImage *locImage;

@property (nonatomic,strong) JYWeiZhangGaoFaViewModel *gaofaViewModel;


@end

@implementation JYMapViewController

#pragma mark ||============ 生命周期 ============||
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configMapview];
    [self configLocationService];
    [self configGeoCodeSearch];
    [self requestMapDataWithType:self.currentMapMode];
    
//    [self configPoiSearch];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.searchView.hidden = NO;
    self.mapview.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.searchView endEditing:YES];
    self.searchView.hidden = YES;
    self.mapview.delegate = nil;
//    self.poiSearch.delegate = nil;
    _searcher.delegate = nil;
}

- (void)configUI{
    if (self.currentMapMode == JYCurrentMapModeWeiZhangGaoFa) {
        self.naviTitle = @"违章高发地";
    }else{
        WEAKSELF
        //    [self.navigationController.view addSubview:self.searchView];
        self.navigationItem.titleView = self.searchView;
        self.searchView.placeHolder = @"输入关键字";
        self.searchView.currentSearch = YES;
        self.searchView.startSearchWithTextFieldBlock = ^(NSString *text) {
            weakSelf.keywords = text;
            [weakSelf requestSearchWithKeyWord:text];
            //        [weakSelf sendPoiSearchWithkeyWords];
        };
        [self rightItemTitle:@"取消" color:RGB0X(0x333333) font:FONT(15) action:^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }
}

- (void)configMapview{
    self.mapview = [[BMKMapView alloc]initWithFrame:SCREEN_BOUNDS];
    self.mapview.zoomLevel = 14;
    self.mapview.showsUserLocation = YES;//打开定位图层
    self.mapview.userTrackingMode = BMKUserTrackingModeNone;//罗盘跟随模式
    //在地图上自定义控件需要放在中间图层
//    [self.view addSubview:self.underView];
    [self.underView addSubview:self.mapview];
//    [self.view addSubview:self.detailView];
//    [self.view addSubview:self.locationBtn];
    
    BMKLocationViewDisplayParam* testParam = [[BMKLocationViewDisplayParam alloc] init];
    testParam.isRotateAngleValid = true;// 跟随态旋转角度是否生效
    testParam.isAccuracyCircleShow = NO;// 精度圈是否显示
    testParam.locationViewImgName = @"定位方向图标-";// 定位图标名称
    testParam.locationViewOffsetX = 0;//定位图标偏移量(经度)
    testParam.locationViewOffsetY = 0;// 定位图标偏移量(纬度)
    [self.mapview updateLocationViewWithParam:testParam]; //调用此方法后自定义定位图层生效 [testParam release];
}

- (void)configMyLocationPoint{
    self.mapview.showsUserLocation = NO;
    if (_locAnnotation == nil) {
        _locAnnotation = [[JYLocationAnnotation alloc] init];
        _locAnnotation.coordinate = self.locationPt;
        [self.mapview addAnnotation:_locAnnotation];
    }else{
        _locAnnotation.coordinate = self.locationPt;
    }
}

//- (void)configPoiSearch{
//    self.poiSearch =[[BMKPoiSearch alloc]init];
//    self.poiSearch.delegate = self;
//    self.keywords = @"商场";
//    [self sendPoiSearchWithkeyWords];
//}

//发起检索
//- (void)sendPoiSearchWithkeyWords{
//    
//    BMKNearbySearchOption *option = [[BMKNearbySearchOption alloc]init];
//    option.pageIndex = 1;
//    option.pageCapacity = 10;
//    option.location = self.moveLocationPt;
//    option.keyword = self.keywords;
//    BOOL flag = [self.poiSearch poiSearchNearBy:option];
//    if(flag){
//        NSLog(@"周边检索发送成功");
//    }else{
//        NSLog(@"周边检索发送失败");
//    }
//}

- (void)configLocationService{
    self.locationService = [[BMKLocationService alloc]init];
    self.locationService.delegate = self;
    self.locationService.distanceFilter = 10;
    [self.locationService startUserLocationService];
}

- (void)configGeoCodeSearch{
    _searcher =[[BMKGeoCodeSearch alloc]init];
    _searcher.delegate = self;
    BMKReverseGeoCodeOption *reverseGeoCodeSearchOption = [[BMKReverseGeoCodeOption alloc] init];
    reverseGeoCodeSearchOption.reverseGeoPoint = self.locationPt;
    BOOL flag = [_searcher reverseGeoCode:reverseGeoCodeSearchOption];
//    BMKGeoCodeSearchOption *geoCodeSearchOption = [[BMKGeoCodeSearchOption alloc]init];
//    geoCodeSearchOption.city= @"北京市";
//    geoCodeSearchOption.address = @"海淀区上地10街10号";
//    BOOL flag = [_searcher geoCode:geoCodeSearchOption];
    if(flag)
    {
        NSLog(@"geo检索发送成功");
    }
    else
    {
        NSLog(@"geo检索发送失败");
    }
}

//-(BMKPointAnnotation *)latitude:(CGFloat)latitude longitude:(CGFloat)longitude title:(NSString *)title subTitle:(NSString *)subTitle {
//    BMKPointAnnotation *point = [[BMKPointAnnotation alloc]init];
//    
//    point.coordinate = CLLocationCoordinate2DMake(latitude,longitude);
//    
//    point.title = title;
//    point.subtitle = subTitle;
//    
//    
//    [self.mapview addAnnotation:point];
//    
//    return point;
//}

////创建POI检索结果的大头针
//- (void)reloadPoiSearchAnnotationsWithArr:(NSArray *)arr{
//    [self.annotationViews removeAllObjects];
//    [self.mapview removeAnnotations:self.mapview.annotations];
//    for (BMKPoiInfo * pointInfo  in arr) {
//        BMKPointAnnotation * newAnnotations = [[BMKPointAnnotation alloc]init];
//        newAnnotations.coordinate = pointInfo.pt;
////        newAnnotations.title = pointInfo.name;
////        newAnnotations.subtitle = pointInfo.address;
//        [self.annotationViews addObject:newAnnotations];
//    }
//    [self.mapview addAnnotations:self.annotationViews];
//}

////添加大头针
//- (void)addAnnotationsWithArr:(NSArray *)arr{
//    [self.dataArr removeAllObjects];
//    [self.annotationViews removeAllObjects];
//    [self.mapview removeAnnotations:self.mapview.annotations];
//    [self.dataArr addObjectsFromArray:arr];
//    for (int i = 0; i < arr.count; i++) {
//        JYShopListModel * model = arr[i];
//        BMKPointAnnotation * newAnnotations = [[BMKPointAnnotation alloc]init];
////        目前返回的模型中，经纬度相反 暂时写死
////        newAnnotations.coordinate = CLLocationCoordinate2DMake([model.shopLat floatValue], [model.shopLong floatValue]);
//        
//        if (i == 0) {
//            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.911725, 116.450909);
//        }else if (i == 1){
//            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.908847, 116.469306);
//        }else if (i == 2){
//            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.92567, 116.461257);
//        }else{
//            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.932116, 116.40636);
//        }
//
//        [self.annotationViews addObject:newAnnotations];
//    }
//    [self.mapview addAnnotations:self.annotationViews];
//}

//添加大头针
- (void)addAnnotationsWithArr:(NSArray *)arr{
    
    [self.dataArr removeAllObjects];
    [self.annotationViews removeAllObjects];
    [self.mapview removeAnnotations:self.mapview.annotations];
    
    [self.mapview addAnnotation:self.locAnnotation];
    
    [self.dataArr addObjectsFromArray:arr];
    
    if (self.currentMapMode == JYCurrentMapModeWeiZhangGaoFa) {
        for (int i = 0; i < arr.count; i++) {
            JYJUHEWeiZhangGaoFaModel * gaofaModel = arr[i];
            BMKPointAnnotation * newAnnotations = [[BMKPointAnnotation alloc]init];
            //        目前返回的模型中，经纬度相反 暂时写死
        
            newAnnotations.coordinate = CLLocationCoordinate2DMake([gaofaModel.location[1] floatValue], [gaofaModel.location[0] floatValue]);
            newAnnotations.title = SF(@"%d",i + 1);
            
            //        if (i == 0) {
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.911725, 116.450909);
            //        }else if (i == 1){
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.908847, 116.469306);
            //        }else if (i == 2){
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.92567, 116.461257);
            //        }else{
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.932116, 116.40636);
            //        }
            
            [self.annotationViews addObject:newAnnotations];
            
        }

    }else{
        for (int i = 0; i < arr.count; i++) {
            JYShopListModel * model = arr[i];
            BMKPointAnnotation * newAnnotations = [[BMKPointAnnotation alloc]init];
            //        目前返回的模型中，经纬度相反 暂时写死
            newAnnotations.coordinate = CLLocationCoordinate2DMake([model.shopLat floatValue], [model.shopLong floatValue]);
            newAnnotations.title = SF(@"%d",i + 1);
            
            //        if (i == 0) {
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.911725, 116.450909);
            //        }else if (i == 1){
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.908847, 116.469306);
            //        }else if (i == 2){
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.92567, 116.461257);
            //        }else{
            //            newAnnotations.coordinate = CLLocationCoordinate2DMake(39.932116, 116.40636);
            //        }
            
            [self.annotationViews addObject:newAnnotations];
            _mapview.centerCoordinate = newAnnotations.coordinate;//移动到中心点
        }
        
    }
    [self.mapview addAnnotations:self.annotationViews];
}

#pragma mark ==================更新UI==================

//更新大头针数据UI
- (void)updataAnnotaionUIWithModel:(id)dataModel{
    if ([dataModel isKindOfClass:[JYShopListModel class]]) {
        JYShopListModel * model = (JYShopListModel *)dataModel;
        [self.picImageView sd_setImageWithURL:[NSURL URLWithString:model.shopImage] placeholderImage:DEFAULT_COURSE];
        self.titleLab.text = model.shopTitle;
        self.addressLab.text = model.shopAddress;
        self.telLab.text = model.shopTel;
        [self configStar:model];
        
        CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
        CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
        self.distanceLab.text = [self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[model.shopLat floatValue] endLng:[model.shopLong floatValue]];
    }else if([dataModel isKindOfClass:[JYJUHEWeiZhangGaoFaModel class]]){
//        JYJUHEWeiZhangGaoFaModel * model = (JYJUHEWeiZhangGaoFaModel *)dataModel;
//        [self showSuccessTip:model.address];
//        [self.picImageView sd_setImageWithURL:[NSURL URLWithString:model.shopImage] placeholderImage:DEFAULT_COURSE];
//        self.titleLab.text = model.shopTitle;
//        self.addressLab.text = model.shopAddress;
//        self.telLab.text = model.shopTel;
//        [self configStar:model];
//
//        CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
//        CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
//        self.distanceLab.text = [self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[model.shopLat floatValue] endLng:[model.shopLong floatValue]];

    }
}

- (void)configStar:(JYShopListModel *)model{
    NSInteger num = [model.shopEvaluate intValue];
    for (UIImageView * view in self.starView.subviews) {
        if ((view.tag - 1000) < num) {
            view.image = [UIImage imageNamed:@"评价黄星"];
        }else{
            view.image = [UIImage imageNamed:@"评价灰星"];
        }
    }
}


//获得两点之间的距离
- (NSString *)getTheDistanceWithStartLat:(CGFloat)startLat startLng:(CGFloat)startLng andEndLat:(CGFloat)endLat endLng:(CGFloat )endLng{
    BMKMapPoint start = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(startLat,startLng));
    BMKMapPoint end = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(endLat,endLng));
    CLLocationDistance distance = BMKMetersBetweenMapPoints(start, end);
    NSString *distanceStr;
//    if (distance > 1000) {
        distanceStr = [NSString stringWithFormat:@"%.2fkm",distance/1000];
//    }else{
//        distanceStr = [NSString stringWithFormat:@"%.2fm",distance];
//    }
    return distanceStr;
}

//旋转
- (UIImage *)image:(UIImage *)image imageRotatedByDegrees:(CGFloat)degrees
{
    
    CGFloat width = CGImageGetWidth([image CGImage]);
    CGFloat height = CGImageGetHeight([image CGImage]);
    
    CGSize rotatedSize;
    
    rotatedSize.width = width;
    rotatedSize.height = height;
//    rotatedSize.width = height *sin(degrees * M_PI / 180);
//    rotatedSize.height = height *cos(degrees * M_PI / 180);
    
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    CGContextRotateCTM(bitmap, degrees * M_PI / 180);
    CGContextRotateCTM(bitmap, M_PI);
    CGContextScaleCTM(bitmap, -1.0, 1.0);
    CGContextDrawImage(bitmap, CGRectMake(-rotatedSize.width/2, -rotatedSize.height/2, rotatedSize.width, rotatedSize.height), [image CGImage]);
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark ||============Action============||

- (IBAction)locationBtnClick:(id)sender {
    [self.locationService startUserLocationService];
    self.mapview.zoomLevel = 17;
    [self.mapview setCenterCoordinate:self.locationPt animated:YES];
}

- (IBAction)detailViewBtnAction:(UIButton *)sender {
    if (sender.tag == 1000) {
        NSLog(@"去这里");
        [self gotoTheDestination];
        
//        BMKPoiSearch *search = [[BMKPoiSearch alloc]init];
        
        
    }else{
        NSLog(@"详情页面");
        JYShopListModel * shopModel = self.detailModel;
        if (self.currentMapMode == JYCurrentMapModeAddOil) {
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            detailVC.shopId = shopModel.shopId;
            [self.navigationController pushViewController:detailVC animated:YES];
            
        }else{
            JYStoreViewController * storeVC = [[JYStoreViewController alloc] init];
            storeVC.shopType = JYShopType_keepCar;
            storeVC.shopId = shopModel.shopId;
            [self.navigationController pushViewController:storeVC animated:YES];
        }
    }
}

- (void)gotoTheDestination{
    
//    116.373798,39.968484
    CLLocationCoordinate2D destination = CLLocationCoordinate2DMake([self.detailModel.shopLat floatValue], [self.detailModel.shopLong floatValue]);
//    判断是否安装了百度地图，如果安装了百度地图，则使用百度地图导航
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        NSLog(@"-- 百度地图");

        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%f,%f|name=目的地&mode=driving&coord_type=bd09ll",destination.latitude, destination.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];

    }else
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {

        CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
        CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
            // 高德地图
            // 起点为“我的位置”，终点为后台返回的address
            NSString *urlString = [[NSString stringWithFormat:@"iosamap://path?sourceApplication=applicationName&sid=BGVIS1&slat=%lf&slon=%lf&sname=我的位置&did=BGVIS2&dlat=%lf&dlon=%lf&dname=%@&dev=0&m=0&t=0",startLat, startLng,[self.detailModel.shopLat floatValue],[self.detailModel.shopLong floatValue],self.detailModel.shopTitle] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        } else{
        //使用自带地图导航
        MKMapItem *currentLocation =[MKMapItem mapItemForCurrentLocation];
    
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:destination addressDictionary:nil]];
        toLocation.name = self.detailModel.shopTitle;
    
        [MKMapItem openMapsWithItems:@[currentLocation,toLocation] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
    
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ==================网络请求==================

////请求城市编码
//- (void)requestCityCode{
//    WEAKSELF
//    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
//    dic[@"cityTitle"] = self.localCityName;
//    [self.homeViewModel requestGetCityCodeWithParams:dic success:^(NSString *msg, id responseData) {
//        NSString * cityCode = responseData;
//        [JY_USERDEFAULTS setObject:cityCode forKey:JY_LOCATION_CITYCODE];
//        [JY_USERDEFAULTS synchronize];
//    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
//    }];
//}

//根据关键字搜索
- (void)requestSearchWithKeyWord:(NSString *)keyWord{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"key"] = keyWord;
    dic[@"cityCode"] = [JY_USERDEFAULTS objectForKey:JY_LOCATION_CITYCODE];
    if (self.currentMapMode == JYCurrentMapModeAddOil) {
        self.searchType = JYSearchType_OilStation;
    }else if (self.currentMapMode ==  JYCurrentMapModeMaintainCar){
        self.searchType = JYSearchType_Store;
    }
    
    [self.searchViewModel requestWithParams:dic AndSearchType:self.searchType andIsMore:NO success:^(NSString *msg, id responseData) {
//        添加大头针
        [weakSelf addAnnotationsWithArr:self.searchViewModel.listArr];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求相应的数据  养车周边的店铺 或者加油站
- (void)requestMapDataWithType:(JYCurrentMapMode)mapMode{
    WEAKSELF

    if (mapMode == JYCurrentMapModeAddOil) {
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"shopTypeId"] = @"0";
        dic[@"cityCode"] = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYCODE];
        dic[@"orderby"] = @"3"; //默认排序
        [self.addOilViewModel requestGetStationListWithParams:dic isMore:NO success:^(NSString *msg, id responseData) {
            [weakSelf addAnnotationsWithArr:self.addOilViewModel.shopListArr];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];

    }else if(mapMode == JYCurrentMapModeMaintainCar){
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"type"] = @(self.storeType);
        dic[@"shopTypeId"] = @"0";
        dic[@"cityCode"] = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYCODE];
        dic[@"orderby"] = @"3";
        [self.keepCarViewModel requestGetKeepListWithParams:dic isMore:NO success:^(NSString *msg, id responseData) {
            [weakSelf addAnnotationsWithArr:self.keepCarViewModel.shopListArr];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }else{
//        违章高发地
        [self.gaofaViewModel requestIllegalHighHairSuccess:^(NSString *msg, id responseData) {
            [weakSelf addAnnotationsWithArr:self.gaofaViewModel.gaofaArr];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }
}



#pragma mark设置定位图标的旋转角度
- (void)setLocationViewAngle:(CGFloat)angle{
    BMKAnnotationView*locationView = [self.mapview valueForKey:@"_locationView"];
    if(locationView){
        locationView.transform=CGAffineTransformMakeRotation(_currentAngle);

    }
}




#pragma mark ||============MAPAPIdelegate============||
// 这个方法是当地图加载完成后会走的一个方法，一般把在mapView上添加其他控件的方法，写在这里
- (void)mapViewDidFinishLoading:(BMKMapView *)mapView{
    
    [self.mapview setCenterCoordinate:self.locationPt animated:YES];

//    NSMutableArray *arr = [[NSMutableArray alloc]init];
//    for (int i = 0; i < 3; i++) {
//         BMKPointAnnotation *point = [[BMKPointAnnotation alloc]init];
//        point.coordinate = CLLocationCoordinate2DMake(39.9129900 ,116.4677600 + i);
//        [arr addObject:point];
//        [self.mapview addAnnotations:arr];
//    }
    
}

//接收反向地理编码结果
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        //      在此处理正常结果
        [JY_USERDEFAULTS setObject:result.cityCode forKey:JY_LOCATION_CITYCODE];
        [JY_USERDEFAULTS synchronize];
        
//        [self requestMapDataWithType:self.currentMapMode];

    }
    else {
        NSLog(@"抱歉，未找到结果");
    }
}



//地图渲染每一帧画面过程中，以及每次需要重绘地图时（例如添加覆盖物）都会调用此接口
- (void)mapView:(BMKMapView*)mapView onDrawMapFrame:(BMKMapStatus*)status{
    [self setLocationViewAngle:_currentAngle];
//    self.detailView.hidden = YES;
}

//处理方向变更信息
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    
//    [self.mapview updateLocationData:userLocation];
//    CGFloat angle = userLocation.heading.magneticHeading*M_PI/180;
//    _currentAngle= angle;
//    [self setLocationViewAngle:_currentAngle];
    
    if(nil == userLocation.location || nil == userLocation.heading
       || userLocation.heading.headingAccuracy < 0) {
        return;
    }
    
    CLLocationDirection  theHeading = userLocation.heading.magneticHeading;
    NSLog(@"🌺🌺🌺🌺🌺🌺🌺🌺%f",theHeading);
    
    double d = 360 - theHeading;
    
    float direction = theHeading;
    
    if(nil != self.locAnnotationView) {
        if (direction > 180)
        {
            direction = 360 - direction;
        }
        else
        {
            direction = 0 - direction;
        }
        self.locAnnotationView.image = [self image:self.locImage imageRotatedByDegrees: - direction];
    }
}

//处理位置坐标更新
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    //实时经纬度
    self.locationPt = CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    self.moveLocationPt = self.locationPt;
    
    [self configMyLocationPoint];
    //没有这句不显示小圆点
//    [self.mapview updateLocationData:userLocation];
//    [self sendPoiSearchWithkeyWords];
    
    NSLog(@"didUpdateUserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
}

/**
 *在地图View停止定位后，会调用此函数
 *@param mapView 地图View
 */
- (void)didStopLocatingUser
{
    [self.locationService stopUserLocationService];
}

//- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
//{
//    // 1、****定位成功之后获取当前位置经纬度****
//    self.moveLocationPt = mapView.centerCoordinate;
//    // 2、****添加地图标记****
//    [self sendPoiSearchWithkeyWords];
//}


//大头针
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation{

//    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
//        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
//        JYMapPaopaoView *paopao = [[JYMapPaopaoView alloc]init];
//        newAnnotationView.paopaoView = [[BMKActionPaopaoView alloc]initWithCustomView:paopao];
////        [newAnnotationView setImage:[UIImage imageNamed:@"红色位置图标"]];
//        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
//        newAnnotationView.animatesDrop = NO;// 设置该标注点动画显示
//        newAnnotationView.image = [UIImage imageNamed:@"红色位置图标"];
//        newAnnotationView.canShowCallout = NO;
//        newAnnotationView.draggable = NO;
//
//        return newAnnotationView;
//    }
//    return nil;
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        JYAnnotationView * annotationView = (JYAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"JYAnnotation"];
        if (annotationView == nil) {
            annotationView = [[JYAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"JYAnnotation"];
        }
        
        BMKPointAnnotation * newAnnotation = (BMKPointAnnotation *)annotation;
        annotationView.countText = newAnnotation.title;
        if (self.currentMapMode == JYCurrentMapModeAddOil) {
            annotationView.bottomImage = @"地图加油站";
        }else if(self.currentMapMode == JYCurrentMapModeMaintainCar){
            annotationView.bottomImage = @"地图房子";
        }else{
             annotationView.bottomImage = @"";
        }
        annotationView.canShowCallout = NO;
        annotationView.draggable = NO;
        return annotationView;
    }else if ([annotation isKindOfClass:[JYLocationAnnotation class]]){
        BMKPinAnnotationView *locAnnotationView = (BMKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"JYMyLocation"];
        if (locAnnotationView == nil) {
            locAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"JYMyLocation"];
        }
        locAnnotationView.annotation = annotation;
        self.locAnnotationView = locAnnotationView;
        self.locImage = locAnnotationView.image = [UIImage imageNamed:@"地图蓝色我的位置"];//带箭头方向的图片
        locAnnotationView.centerOffset = CGPointMake(0, locAnnotationView.frame.size.height/2);
        return locAnnotationView;
    }
    return nil;
}


////当选中一个annotation views时，调用此接口
//- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view
//{
//    //坐标
//    NSLog(@"选中一个annotation views:%f,%f",view.annotation.coordinate.latitude,view.annotation.coordinate.longitude);
//    if ([view.annotation isKindOfClass:[BMKPointAnnotation class]]) {
//        //取出piAnnoarray中的每个标注
//        for (int i = 0; i< [self.annotationViews count]; i++) {
//            
////            BMKPinAnnotationView *pinAnnotation = [[BMKPinAnnotationView alloc]init];
//            BMKPinAnnotationView *pinAnnotation = [self.annotationViews objectAtIndex:i];
//            //判断他的selected状态
//            if(pinAnnotation.isSelected ){
//                
//                pinAnnotation.image = view.image = [UIImage imageNamed:@"蓝色位置图标"];
//                //重新计算Frame，如果你用的图片大小一样，则不需要重新计算 设置图片一定要放到计算<span style="font-family: Arial, Helvetica, sans-serif;">Frame的前面</span>
//                
////                [self SetAonnotionLaFrame:pinAnnotation labelTag:i];
//                
//                /// 设置当前地图的中心点 把选中的标注作为地图中心点
//                [self.mapview setCenterCoordinate:pinAnnotation.annotation.coordinate animated:YES] ;
//            }
//            else{
//                
//                pinAnnotation.image = [UIImage imageNamed:@"红色位置图标"];
//                
//                //重新计算Frame
////                [self SetAonnotionLaFrame:pinAnnotation labelTag:i];
//            }
//            
//        }
//        
//        
//    }
//    
//}

- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view{
    
//    [self.mapview setCenterCoordinate:view.annotation.coordinate animated:YES];
//    默认的情况 勿删
//    if ([view.annotation isKindOfClass:[BMKPointAnnotation class]]) {
//        
//        view.image = [UIImage imageNamed:@"蓝色位置图标"];
//        self.detailView.hidden = NO;
//        if (self.selectAnnotation) {
//            self.selectAnnotation.image = [UIImage imageNamed:@"红色位置图标"];
//        }
//        self.selectAnnotation = view;
//        
//    }
    
//    自定义的情况
    if ([view isKindOfClass:[JYAnnotationView class]]) {
        JYAnnotationView * annotation = (JYAnnotationView *)view;
//        annotation.enabled = self.currentMapMode == JYCurrentMapModeWeiZhangGaoFa ? NO : YES;
        if (self.currentMapMode == JYCurrentMapModeWeiZhangGaoFa) {
             annotation.topImage = @"红色位置图标";
        }else{
            annotation.topImage = @"蓝色位置图标";
        }
        self.detailView.hidden = self.currentMapMode == JYCurrentMapModeWeiZhangGaoFa ? YES : NO;
        if (self.selectAnnotation) {
            self.selectAnnotation.topImage = @"红色位置图标";
        }
        self.selectAnnotation = annotation;
    }
    if (self.currentMapMode == JYCurrentMapModeWeiZhangGaoFa) {
//        for (int i = 0; i < self.annotationViews.count; i++) {
//            if ([view.annotation isEqual:self.annotationViews[i]]) {
//                self.gaofaViewModel = self.dataArr[i];
//                [self updataAnnotaionUIWithModel:self.gaofaViewModel];
//            }
//        }

    }else{
        for (int i = 0; i < self.annotationViews.count; i++) {
            if ([view.annotation isEqual:self.annotationViews[i]]) {
                self.detailModel = self.dataArr[i];
                [self updataAnnotaionUIWithModel:self.detailModel];
            }
        }
    }
    
    
    //    [view setSelected:NO animated:YES];
}

//- (void)mapView:(BMKMapView *)mapView didDeselectAnnotationView:(BMKAnnotationView *)view{
//    [view setImage:[UIImage imageNamed:@"红色位置图标"]];
//}

////实现PoiSearchDeleage处理回调结果
//- (void)onGetPoiResult:(BMKPoiSearch*)searcher result:(BMKPoiResult*)poiResultList errorCode:(BMKSearchErrorCode)error
//{
//    if (error == BMK_SEARCH_NO_ERROR) {
//        //在此处理正常结果
//        NSLog(@"%@",poiResultList);
//        [self reloadPoiSearchAnnotationsWithArr:poiResultList.poiInfoList];
//    }
//    else if (error == BMK_SEARCH_AMBIGUOUS_KEYWORD){
//        //当在设置城市未找到结果，但在其他城市找到结果时，回调建议检索城市列表
//        // result.cityList;
//        NSLog(@"起始点有歧义");
//    } else {
//        NSLog(@"抱歉，未找到结果");
//    }
//}

#pragma mark ||============懒加载============||
- (JYTurnSearchView *)searchView
{
    if (!_searchView) {
        _searchView = [[JYTurnSearchView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH - 50-70, 44)];
    }
    return _searchView;
}

- (NSMutableArray *)annotationViews
{
    if (!_annotationViews) {
        _annotationViews = [NSMutableArray array];
    }
    return _annotationViews;
}

- (JYSearchViewModel *)searchViewModel
{
    if (!_searchViewModel) {
        _searchViewModel = [[JYSearchViewModel alloc] init];
    }
    return _searchViewModel;
}

- (JYHomeViewModel *)homeViewModel
{
    if (!_homeViewModel) {
        _homeViewModel = [[JYHomeViewModel alloc] init];
    }
    return _homeViewModel;
}

- (JYKeepCarViewModel *)keepCarViewModel
{
    if (!_keepCarViewModel) {
        _keepCarViewModel = [[JYKeepCarViewModel alloc] init];
    }
    return _keepCarViewModel;
}

- (JYAddOilViewModel *)addOilViewModel
{
    if (!_addOilViewModel) {
        _addOilViewModel = [[JYAddOilViewModel alloc] init];
    }
    return _addOilViewModel;
}

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (JYWeiZhangGaoFaViewModel *)gaofaViewModel{
    if(!_gaofaViewModel){
        _gaofaViewModel = [[JYWeiZhangGaoFaViewModel alloc] init];
    }
    return _gaofaViewModel;
}


@end
