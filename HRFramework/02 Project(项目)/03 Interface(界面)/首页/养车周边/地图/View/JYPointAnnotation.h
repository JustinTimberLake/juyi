//
//  JYPointAnnotation.h
//  JY
//
//  Created by risenb on 2017/8/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>

@interface JYPointAnnotation : NSObject<BMKAnnotation>
@property (nonatomic,copy) NSString *countText; //个数
@property (nonatomic,assign) CLLocationCoordinate2D coordinate; //经纬度

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
