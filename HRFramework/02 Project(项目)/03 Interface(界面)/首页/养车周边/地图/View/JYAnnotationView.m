//
//  JYAnnotationView.m
//  JY
//
//  Created by risenb on 2017/8/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAnnotationView.h"


@interface JYAnnotationView ()

@property (nonatomic,strong) UIView *contentView;
@property (nonatomic,strong) UIImageView *topImageView;
@property (nonatomic,strong) UIImageView *bottomImageView;
@property (nonatomic,strong) UILabel *countLab;

@end

@implementation JYAnnotationView


- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        [self setBounds:CGRectMake(0, 0, 24, 44)];
        [self configUI];
    }
    return self;
}

- (void)configUI{
    UIView * contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 44)];
    contentView.backgroundColor = [UIColor clearColor];
    self.contentView = contentView;
    [self addSubview:contentView];
    
    UIImageView * topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 27)];
    topImageView.image = [UIImage imageNamed:@"红色位置图标"];
    self.topImageView = topImageView;
    [self.contentView addSubview:topImageView];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 24, 22)];
    self.countLab = label;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:label];
    
    UIImageView * bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(topImageView.frame) + 3,14 , 14)];
    bottomImageView.image = [UIImage imageNamed:@"拍照图标"];
    self.bottomImageView = bottomImageView;
    [self.contentView addSubview:bottomImageView];
}

- (void)setCountText:(NSString *)countText{
    self.countLab.text = countText;
}

- (void)setTopImage:(NSString *)topImage{
    self.topImageView.image = [UIImage imageNamed:topImage];
}

- (void)setBottomImage:(NSString *)bottomImage{
    self.bottomImageView.image = [UIImage imageNamed:bottomImage];
}

@end
