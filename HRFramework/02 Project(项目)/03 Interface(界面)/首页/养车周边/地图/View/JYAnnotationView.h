//
//  JYAnnotationView.h
//  JY
//
//  Created by risenb on 2017/8/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>

@interface JYAnnotationView : BMKAnnotationView
@property (nonatomic,copy) NSString *countText; //数字
@property (nonatomic,copy) NSString *bottomImage; //底部小图标
@property (nonatomic,copy) NSString *topImage;//上面图标

@end
