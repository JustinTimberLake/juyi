//
//  JYPointAnnotation.m
//  JY
//
//  Created by risenb on 2017/8/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPointAnnotation.h"

@implementation JYPointAnnotation

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate{
    if (self = [super init]) {
        self.coordinate = coordinate;
    }
    return self;
}

@end
