//
//  JYStoreListViewController.h
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,JYCurrentListMode) {
    JYCurrentListModeMaintainCar,//养车周边店铺列表页面
    JYCurrentListModeAddOil,//加油列表页面
};

//养车店铺类型
typedef NS_ENUM(NSInteger,JYStoreType) {
    JYStoreType_CarWashBeauty = 1,//洗车美容
    JYStoreType_CarAutoRepairAdaptations//汽修改装
};

@interface JYStoreListViewController : JYBaseViewController
//共用页面的分类模式
@property (nonatomic,assign) JYCurrentListMode currentListMode;

@property (nonatomic,assign) JYStoreType storeType;

@end
