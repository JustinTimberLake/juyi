//
//  JYStoreListViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYStoreListViewController.h"
#import "JYStoreListTableViewCell.h"
#import "JYSearchHistoryCell.h"
#import "HFButton.h"
#import "JYTurnSearchView.h"
#import "JYSearchViewController.h"
#import "JYMapViewController.h"
#import "JYDetailViewController.h"
#import "JYKeepCarViewModel.h"
#import "JYLocationViewModel.h"
#import "JYAddOilViewModel.h"
#import "JYProCityCountyModel.h"
#import "JYCityModel.h"
#import "JYShopTypeModel.h"
#import "JYShopListModel.h"
#import "JYStoreViewController.h"
#import "JYCategoryTableViewCell.h"
#import "SDCycleScrollView.h"
#import "JYHomeViewModel.h"
#import "MMScanViewController.h"
//#import "JYActivityDetailViewController.h"
//#import "JYMomentDetailViewController.h"
//#import "JYStoreListViewController.h"
//#import "JYProductListViewController.h"
//#import "JYStoreViewController.h"
#import "JYDetailViewController.h"
#import "JYCommonWebViewController.h"
#import "JYHomeBannerModel.h"
#import "JYScanQRViewController.h"
#import "JYVideoListViewController.h"


//常量首字母大写
static NSString * const storeCellId = @"JYStoreListTableViewCell";
static NSString * const choseCellId = @"JYSearchHistoryCell";
static NSString * const categoryCellId = @"JYCategoryTableViewCell";


@interface JYStoreListViewController ()<
    UITableViewDelegate,
    UITableViewDataSource,
    SDCycleScrollViewDelegate
>
// tableview
@property (weak, nonatomic) IBOutlet UITableView *myTableView; // 主tableview
@property (weak, nonatomic) IBOutlet UIView *blackBgView;
@property (weak, nonatomic) IBOutlet UIView *currentCityView;

//
@property (weak, nonatomic) IBOutlet UITableView *leftTableView;
@property (weak, nonatomic) IBOutlet UITableView *rightTableView;
@property (weak, nonatomic) IBOutlet UITableView *storeOrSortTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *storeOrSortViewH;

// 顶部筛选btn
@property (weak, nonatomic) IBOutlet HFButton *topFirstBtn;
@property (weak, nonatomic) IBOutlet HFButton *topSecondBtn;
@property (weak, nonatomic) IBOutlet HFButton *topThirdBtn;
@property (nonatomic,strong) UIButton *selectedTitleButton;

@property (weak, nonatomic) IBOutlet UIView *blackTapView;
@property (nonatomic,strong) JYTurnSearchView *searchView;

@property (weak, nonatomic) IBOutlet UIButton *scanBtn;

//排序数组
@property (nonatomic,strong) NSArray *orderByArr;

@property (nonatomic,strong) JYKeepCarViewModel *keepCarViewModel;
@property (nonatomic,strong) JYLocationViewModel *locationViewModel;
@property (nonatomic,strong) JYAddOilViewModel *addOilViewModel;

//需要请求时用的参数
@property (nonatomic,copy) NSString *cityCode;
@property (nonatomic,copy) NSString *cityTitle;
@property (nonatomic,copy) NSString *shopTypeId;
@property (nonatomic,copy) NSString *shopTypeTitle;
@property (nonatomic,copy) NSString *orderByCode;
@property (nonatomic,copy) NSString *orderByTitle;
@property (nonatomic,assign) NSInteger currentRow;
@property (nonatomic,copy) NSString *localCityCode;

@property (nonatomic,strong) SDCycleScrollView * scrollView;
@property (nonatomic,strong) JYHomeViewModel *homeViewModel;
@property (nonatomic,strong) NSArray *cityArray;

@end

@implementation JYStoreListViewController

#pragma mark ------生命周期-----------

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configResultTableView];
    [self configChoseTableView];
    [self configNavUI];
    [self configUI];
    [self configTopSelectBtn];
    [self initData];
    [self setUpRefreshData];
    [self requestADBanner];
//    [self requestProvinceCity];
    [self refreshDataWithIsMore:NO];
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.searchView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.searchView.hidden = YES;
}

#pragma mark ------初始化UI界面-----------

- (void)configNavUI{
    WEAKSELF
    [self rightImageItem:@"地图图标" action:^{
        JYMapViewController * mapVC = [[JYMapViewController alloc] init];
        if (self.currentListMode == JYCurrentListModeAddOil) {
            mapVC.currentMapMode = JYCurrentMapModeAddOil;
        }else{
            mapVC.currentMapMode = JYCurrentMapModeMaintainCar;
        }
        mapVC.storeType = self.storeType;
        [weakSelf.navigationController pushViewController:mapVC animated:YES];
    }];
    
}
//初始化数据
- (void)initData{
    self.localCityCode = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYCODE];
    NSString *selectName = [JY_USERDEFAULTS objectForKey:JY_CURRENT_SELECTCITYNAME];
    NSString *cityName = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYNAME];
    if (selectName.length) {
        [self.topFirstBtn setTitle:selectName forState:UIControlStateNormal];
    }else if (cityName.length){
        [self.topFirstBtn setTitle:cityName forState:UIControlStateNormal];
    }
    self.currentRow = 0;
}

- (void)configUI{
    WEAKSELF
    self.topFirstBtn.selected = NO; //注释
    self.topSecondBtn.selected = NO;
    self.topThirdBtn.selected = NO;
    self.blackBgView.hidden = YES;
    self.storeOrSortTableView.hidden = YES;
//    tap
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.blackTapView addGestureRecognizer:tap];
    self.navigationItem.titleView = self.searchView;
//    [self.navigationController.view addSubview:self.searchView];
    self.searchView.placeHolder = @"请输入关键字";
    
    self.searchView.startTurnSearchActionBlock = ^{
        JYSearchViewController * searchVC = [[JYSearchViewController alloc]init];
        searchVC.cityCode = weakSelf.cityCode;
        searchVC.searchType = weakSelf.currentListMode == JYCurrentListModeMaintainCar ? SearchTypeKeepCarStore : SearchTypeOilStation;
        [weakSelf.navigationController pushViewController:searchVC animated:YES];
    };
}

//加油页面和周边养车共用的页面
- (void)configTopSelectBtn{
    if (self.currentListMode == JYCurrentListModeAddOil) {
        [self.topSecondBtn setTitle:@"油站类型" forState:UIControlStateNormal];
        self.scanBtn.hidden = NO;
    }else{
        [self.topSecondBtn removeFromSuperview];
        [self.topSecondBtn setTitle:@"店铺类型" forState:UIControlStateNormal];
        self.scanBtn.hidden = YES;
    }
}

- (void)configResultTableView{
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYStoreListTableViewCell" bundle:nil] forCellReuseIdentifier:storeCellId];
    self.myTableView.showsVerticalScrollIndicator = NO;
    self.myTableView.estimatedRowHeight=150;
}

- (void)configChoseTableView{
    self.leftTableView.delegate = self;
    self.leftTableView.dataSource = self;
    self.leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.leftTableView registerNib:[UINib nibWithNibName:@"JYSearchHistoryCell" bundle:nil] forCellReuseIdentifier:choseCellId];
    [self.leftTableView registerNib:[UINib nibWithNibName:@"JYCategoryTableViewCell" bundle:nil] forCellReuseIdentifier:categoryCellId];
    self.myTableView.showsVerticalScrollIndicator = NO;
    
    self.rightTableView.delegate = self;
    self.rightTableView.dataSource = self;
    self.rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.rightTableView registerNib:[UINib nibWithNibName:@"JYSearchHistoryCell" bundle:nil] forCellReuseIdentifier:choseCellId];
    self.rightTableView.showsVerticalScrollIndicator = NO;
    
    self.storeOrSortTableView.delegate = self;
    self.storeOrSortTableView.dataSource = self;
    self.storeOrSortTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.storeOrSortTableView registerNib:[UINib nibWithNibName:@"JYSearchHistoryCell" bundle:nil] forCellReuseIdentifier:choseCellId];
    self.storeOrSortTableView.showsVerticalScrollIndicator = NO;

}

#pragma mark -------action--------
- (IBAction)topBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    UIButton *btn =(UIButton *)sender;
        if ([self.selectedTitleButton isEqual:btn]) {
            [self.blackBgView setHidden:!self.blackBgView.isHidden];
        }else {
            [self.blackBgView setHidden:NO];
            [self.selectedTitleButton setSelected:NO];
            self.selectedTitleButton = btn;
            [self.selectedTitleButton setSelected:YES];
        }
    if (!self.blackBgView.hidden) {
        self.currentRow = 0;
        BOOL hiddenCurrentCityView = self.selectedTitleButton.tag - 1000;
        self.currentCityView.hidden = hiddenCurrentCityView;
        self.storeOrSortTableView.hidden = !hiddenCurrentCityView;
        
        
//     勿删   
//        switch (self.selectedTitleButton.tag) {
//            case 1000:
//            {
//                self.currentCityView.hidden = NO;
//                self.storeOrSortTableView.hidden = YES;
//            }
//                break;
//            case 1001:case 1002:
//            {
//                self.currentCityView.hidden = YES;
//                self.storeOrSortTableView.hidden = NO;
//            }
//                
//                break;
//                
//            default:
//                break;
//        }
    }
    
    
    if  (self.selectedTitleButton.tag == 1000){
//        [self requestProvinceCity];
        self.blackTapView.top = CGRectGetMaxY(self.currentCityView.frame);

    }else if  (self.selectedTitleButton.tag == 1001) {
        if (self.currentListMode == JYCurrentListModeAddOil) {
            [self requestAddOilType];
        }else{
            [self requestKeepType];
        }
        self.blackTapView.top = CGRectGetMaxY(self.storeOrSortTableView.frame);
//        self.storeOrSortViewH.constant = 0 * 44;
    } else if (self.selectedTitleButton.tag == 1002){
        self.blackTapView.top = CGRectGetMaxY(self.storeOrSortTableView.frame);
        self.storeOrSortViewH.constant = self.orderByArr.count * 44;
    }
    [self.storeOrSortTableView reloadData];
    
}

- (void)tap:(UITapGestureRecognizer *)tap{
    [self hideBlackBgView];
}

//隐藏黑色背景view
- (void)hideBlackBgView{
    self.topFirstBtn.selected = NO; //注释
    self.topSecondBtn.selected = NO;
    self.topThirdBtn.selected = NO;
    self.blackBgView.hidden = YES;
    [self setUpRefreshData];
}

- (void)updataBanner{
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYHomeBannerModel * model in self.homeViewModel.ADBannerArr) {
        [imageArr addObject:model.bannerImage];
    }
    self.scrollView.imageURLStringsGroup = imageArr;
}

//扫一扫
- (IBAction)scanBtnAction:(UIButton *)sender {
    
    MMScanViewController *scanVc = [[MMScanViewController alloc] initWithQrType:MMScanTypeQrCode onFinish:^(NSString *result, NSError *error) {
        if (error) {
            NSLog(@"error: %@",error);
        } else {
            NSLog(@"扫描结果：%@",result);
            //            [self showInfo:result];
        }
    }];
    
    
//    JYScanQRViewController * scanVC = [[JYScanQRViewController alloc]init];
//    scanVC.isPush = YES;
    [self.navigationController pushViewController:scanVc animated:YES];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:sellVC];
//    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark -------网络请求-----------
//请求首页banner
- (void)requestADBanner{
    WEAKSELF
    
    [self.homeViewModel requestGetADPositionWithType:@"1" success:^(NSString *msg, id responseData) {
        [weakSelf updataBanner];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求养车周边店铺类型
- (void)requestKeepType{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(self.storeType);
    dic[@"cityCode"] = self.cityCode.length ? self.cityCode : self.localCityCode;
    
    [self.keepCarViewModel requestGetKeepTypeWithParams:dic success:^(NSString *msg, id responseData) {
         self.storeOrSortViewH.constant = self.keepCarViewModel.typelistArr.count * 44;
        [weakSelf.storeOrSortTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求加油站类型
- (void)requestAddOilType{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"cityCode"] = self.cityCode.length ? self.cityCode : self.localCityCode;
    [self.addOilViewModel requestGetStationTypeWithParams:dic success:^(NSString *msg, id responseData) {
         self.storeOrSortViewH.constant = self.addOilViewModel.typelistArr.count * 44;
         [weakSelf.storeOrSortTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求养车周边店铺列表
- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
//    [self.myTableView.mj_header beginRefreshing];
}


- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}

//根据模式请求不同数据
- (void)refreshDataWithIsMore:(BOOL)isMore{
    if (self.currentListMode == JYCurrentListModeMaintainCar) {
        [self requestKeepCarListDataWithIsMore:isMore];
    }else{
        [self requestAddOilListDataWithIsMore:isMore];
    }
}

//请求养车周边店铺列表
- (void)requestKeepCarListDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] =[User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(self.storeType);
    dic[@"shopTypeId"] = self.shopTypeId;
    dic[@"cityCode"] = self.cityCode.length ? self.cityCode : self.localCityCode;
    dic[@"orderby"] = self.orderByCode.length ? self.orderByCode : @"3";//1距离优先2好评优先3默认排序
    [self.keepCarViewModel requestGetKeepListWithParams:dic isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];

}

//请求加油站店铺列表
- (void)requestAddOilListDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
#pragma mark ----此参数不应该是必传参数，后台要调整
    dic[@"shopTypeId"] = self.shopTypeId;
//    dic[@"shopTypeId"] = self.shopTypeId.length ? self.shopTypeId : @"1";
    dic[@"cityCode"] = self.cityCode.length ? self.cityCode : self.localCityCode;
    dic[@"orderby"] = self.orderByCode.length ? self.orderByCode : @"3";
    
    [self.addOilViewModel requestGetStationListWithParams:dic isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];

    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求省市区接口
- (void)requestProvinceCity{
    WEAKSELF

    [self.locationViewModel requestGetProCityCountySuccess:^(NSString *msg, id responseData) {
        
        [weakSelf.leftTableView reloadData];
        [weakSelf.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        [weakSelf.rightTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark -------更新UI-----------

//更新顶部按钮的状态
- (void)updataTopSelectBtnUIWithData:(id)data andIndex:(NSIndexPath *)index{
    if ([data isKindOfClass:[JYCityModel class]]) {
        JYCityModel *cityModel = data;
        self.cityCode = cityModel.id;
        self.cityTitle = cityModel.name;
        [self.topFirstBtn setTitle:self.cityTitle forState:UIControlStateNormal];
    }else if ([data isKindOfClass:[JYShopTypeModel class]]){
        JYShopTypeModel * typeModel = data;
        self.shopTypeId = typeModel.shopTypeId;
        self.shopTypeTitle = typeModel.shopTypeTitle;
        [self.topSecondBtn setTitle:self.shopTypeTitle forState:UIControlStateNormal];
    }else{
        NSString * str = data;
        self.orderByTitle = str;
        self.orderByCode = SF(@"%ld",index.row + 1);
        [self.topThirdBtn setTitle:str forState:UIControlStateNormal];
    }
}

#pragma mark -------tableview 的代理和数据源-----------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.leftTableView) {
        return self.cityArray.count;
    }else if (tableView == self.rightTableView){
            JYProCityCountyModel * proCityCountyModel = self.cityArray[self.leftTableView.indexPathForSelectedRow.row];
            return proCityCountyModel.cityList.count;
    }else if (tableView == self.storeOrSortTableView){
        if (self.selectedTitleButton.tag == 1001 ) {
            if (self.currentListMode == JYCurrentListModeAddOil) {
                return self.addOilViewModel.typelistArr.count;
            }else{
                return self.keepCarViewModel.typelistArr.count;
            }
        }else{
            return 2;
        }
    }else{
        if (self.currentListMode == JYCurrentListModeAddOil) {
            return self.addOilViewModel.shopListArr.count;
        }else{
            return self.keepCarViewModel.shopListArr.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYStoreListTableViewCell * storeCell = [tableView dequeueReusableCellWithIdentifier:storeCellId ];
    storeCell.gasStation = self.currentListMode?YES:NO;
    JYSearchHistoryCell * choseCell = [tableView dequeueReusableCellWithIdentifier:choseCellId ];
    JYCategoryTableViewCell * categoryCell = [tableView dequeueReusableCellWithIdentifier:categoryCellId];
    categoryCell.categoryType = JYCategoryType_proCityCouny;
    
    if (tableView == self.leftTableView) {
        JYProCityCountyModel * provinceModel = self.cityArray[indexPath.row];
        categoryCell.titleLab.text = provinceModel.name;
        
        if (self.currentRow == indexPath.row) {
            [categoryCell tansformMethodSelect];
        }else{
            [categoryCell tansformMethodUnSelect];
        }
        return categoryCell;
        
    }else if (tableView == self.rightTableView){
        [choseCell hideLine];
            JYProCityCountyModel * provinceModel = self.cityArray[self.leftTableView.indexPathForSelectedRow.row];
            JYCityModel * cityModel = provinceModel.cityList[indexPath.row];
            choseCell.contentLab.text = cityModel.name;
        return choseCell;
    }else if (tableView == self.storeOrSortTableView){
        if (self.selectedTitleButton.tag == 1001) {
            if (self.currentListMode == JYCurrentListModeAddOil) {
               choseCell.shopTypeModel = self.addOilViewModel.typelistArr[indexPath.row];
            }else{
                choseCell.shopTypeModel = self.keepCarViewModel.typelistArr[indexPath.row];
            }
        }else{
            choseCell.contentLab.text = self.orderByArr[indexPath.row];
        }
        return choseCell;
        
    }
    JYShopListModel *listModel;
    if (self.keepCarViewModel.shopListArr.count && self.currentListMode == JYCurrentListModeMaintainCar) {
        listModel = self.keepCarViewModel.shopListArr[indexPath.row];
    }else if (self.addOilViewModel.shopListArr.count && self.currentListMode == JYCurrentListModeAddOil){
        listModel = self.addOilViewModel.shopListArr[indexPath.row];
    }
    storeCell.listModel = listModel;
    WEAKSELF;
    storeCell.navBlock = ^(void){
        if (![weakSelf judgeLogin]) {
            return ;
        }
            //导航
            [JYStoreListTableViewCell gotoMap:listModel andViewController:weakSelf];
        
    };
    return storeCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.leftTableView || tableView == self.rightTableView || tableView == self.storeOrSortTableView) {
        return 44;
    }
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JYSearchHistoryCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if (tableView == self.leftTableView ) {
        
//        [cell setBackgroundViewColor:[UIColor whiteColor]];
        JYCategoryTableViewCell *lastCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow inSection:0]];
        [lastCell tansformMethodUnSelect];
        
        JYCategoryTableViewCell * categoryCell = [tableView cellForRowAtIndexPath:indexPath];
        self.currentRow = indexPath.row;
        [categoryCell tansformMethodSelect];
        
        [self.leftTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
        [self.rightTableView reloadData];
        
    }else if ( tableView == self.rightTableView){
        
        [cell setBackgroundViewColor:[UIColor whiteColor]];

         JYProCityCountyModel * provinceModel = self.cityArray[self.leftTableView.indexPathForSelectedRow.row];
         JYCityModel * cityModel = provinceModel.cityList[indexPath.row];
         [self updataTopSelectBtnUIWithData:cityModel andIndex:indexPath];
         [self refreshDataWithIsMore:NO];
         self.cityCode = cityModel.id;
         [self hideBlackBgView];
    }else if (tableView == self.storeOrSortTableView){
        [cell setBackgroundViewColor:RGB(239, 239, 239)];
        
        if (self.selectedTitleButton.tag == 1001) {
            JYShopTypeModel * typeModel;
            if (self.currentListMode == JYCurrentListModeMaintainCar) {
                typeModel = self.keepCarViewModel.typelistArr[indexPath.row];
            }else{
                typeModel = self.addOilViewModel.typelistArr[indexPath.row];
            }
            [self updataTopSelectBtnUIWithData:typeModel andIndex:indexPath];
//            self.orderByCode = @"2";//好评优先
            [self refreshDataWithIsMore:NO];
          
        }else{
            NSString * titleStr = self.orderByArr[indexPath.row];
            [self updataTopSelectBtnUIWithData:titleStr andIndex:indexPath];
            [self refreshDataWithIsMore:NO];
        }
        [self hideBlackBgView];
    }else{
        JYShopListModel * shopModel;
        if (self.currentListMode == JYCurrentListModeAddOil) {
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            shopModel = self.addOilViewModel.shopListArr[indexPath.row];
            detailVC.shopId = shopModel.shopId;
            [self.navigationController pushViewController:detailVC animated:YES];

        }else{
            JYStoreViewController * storeVC = [[JYStoreViewController alloc] init];
            storeVC.shopType = JYShopType_keepCar;
            shopModel = self.keepCarViewModel.shopListArr[indexPath.row];
//            storeVC.shopModel = shopModel;
            storeVC.shopId = shopModel.shopId;
            [self.navigationController pushViewController:storeVC animated:YES];
        }
    }
}




#pragma mark ------------SDCycleScrollView 代理--------------

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    JYHomeBannerModel * model = self.homeViewModel.ADBannerArr[index];
    
    //(1活动详情，2帖子详情，3油站列表，4店铺列表，5油站详情，6店铺详情，7商品列表，8商品详情，9 url)
    
//   最新的： (1商品 2是油站3 是视频4是资讯5URL)
    switch ([model.bannerType intValue]) {
        case 1:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            detailVC.goodsId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;

        case 2:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            detailVC.shopId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 3:{
            JYVideoListViewController * videoListVC = [[JYVideoListViewController alloc] init];
            [self.navigationController pushViewController:videoListVC animated:YES];
        }
            break;
        case 4:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            webVC.navTitle = @"资讯详情";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 5:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
            
            
        default:
            break;
    }
}

//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

#pragma mark ---------------lazy-------------

- (JYTurnSearchView *)searchView
{
    if (!_searchView) {
        _searchView = [[JYTurnSearchView alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH - 50-70, 44)];
    }
   return _searchView;
}

- (JYKeepCarViewModel *)keepCarViewModel
{
    if (!_keepCarViewModel) {
        _keepCarViewModel = [[JYKeepCarViewModel alloc] init];
    }
    return _keepCarViewModel;
}

- (JYLocationViewModel *)locationViewModel
{
    if (!_locationViewModel) {
        _locationViewModel = [[JYLocationViewModel alloc] init];
    }
    return _locationViewModel;
}

- (JYAddOilViewModel *)addOilViewModel
{
    if (!_addOilViewModel) {
        _addOilViewModel = [[JYAddOilViewModel alloc] init];
    }
    return _addOilViewModel;
}

- (NSArray *)orderByArr
{
    if (!_orderByArr) {
        _orderByArr = @[@"距离优先",@"好评优先"];
    }
    return _orderByArr;
}

- (JYHomeViewModel *)homeViewModel{
    if(!_homeViewModel){
        _homeViewModel = [[JYHomeViewModel alloc] init];
    }
    return _homeViewModel;
}

-(NSArray *)cityArray{
    if (!_cityArray) {
        //JSON文件的路径
        NSString *path = [[NSBundle mainBundle] pathForResource:@"china_city_data" ofType:@"json"];
        
        //加载JSON文件
        NSData *data = [NSData dataWithContentsOfFile:path];
        
        //将JSON数据转为NSArray或NSDictionary
        NSDictionary *dictArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        //        mj_objectArrayWithKeyValuesArray
        NSArray * lianxArray = [JYProCityCountyModel mj_objectArrayWithKeyValuesArray:dictArray];
        
        //赋值
        _cityArray = lianxArray;
    }
    return _cityArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
