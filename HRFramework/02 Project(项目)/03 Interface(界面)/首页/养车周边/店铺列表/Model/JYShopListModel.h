//
//  JYShopListModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYShopListModel : JYBaseModel
// 养车店id
@property (nonatomic,copy) NSString *shopId;
//养车店标题
@property (nonatomic,copy) NSString *shopTitle;
//养车店图片
@property (nonatomic,copy) NSString *shopImage;
//养车店评价
@property (nonatomic,copy) NSString *shopEvaluate;
//养车店地址
@property (nonatomic,copy) NSString *shopAddress;
//养车店电话
@property (nonatomic,copy) NSString *shopTel;
//养车店经度
@property (nonatomic,copy) NSString *shopLong;
//养车店纬度
@property (nonatomic,copy) NSString *shopLat;
//是否收藏（1收藏2未收藏）
@property (nonatomic,copy) NSString *isCollect;
//环信ID
@property (nonatomic,copy) NSString *hxUserId;
//区分实体和商铺 1 实体 2商铺
@property (nonatomic,copy) NSString *shopType;
//营业时间
@property (nonatomic,copy) NSString *business_hours;

//  type 区分线上和线下店铺（1线上2线下） 1商品  2 服务
@property (nonatomic,copy) NSString *type;



@end
