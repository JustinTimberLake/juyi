//
//  JYShopTypeModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYShopTypeModel : JYBaseModel
//id
@property (nonatomic,copy) NSString *shopTypeId;
// 标题
@property (nonatomic,copy) NSString *shopTypeTitle;

@end
