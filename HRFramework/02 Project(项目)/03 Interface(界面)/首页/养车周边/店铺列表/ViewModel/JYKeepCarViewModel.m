//
//  JYKeepCarViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYKeepCarViewModel.h"
#import "JYShopTypeModel.h"
#import "JYShopListModel.h"

@implementation JYKeepCarViewModel


//3.3.1	JY-005-001 获取养车店类型
- (void)requestGetKeepTypeWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_KEEPCAR_GetKeepType) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        
        if (RESULT_SUCCESS) {
            [weakSelf.typelistArr removeAllObjects];
            
            NSArray * arr = [JYShopTypeModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            
            [weakSelf.typelistArr addObjectsFromArray:arr];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//3.3.1	JY-005-002 获取养车店列表
- (void)requestGetKeepListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    
    WEAKSELF
    
    NSString * pageCount;
    if (self.shopListArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.shopListArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.shopListArr.count/10+2 ] : @"1";
    }
    params[@"pageNum"] = pageCount;
    params[@"pageSize"] = @(10);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@%@",JY_PATH(JY_KEEPCAR_GetKeepList),params);
    [manager POST_URL:JY_PATH(JY_KEEPCAR_GetKeepList) params:params success:^(id result) {
        
        NSLog(@"%@",result);
        
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [self.shopListArr removeAllObjects];
            }
            NSArray * arr = [JYShopListModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            
            [weakSelf.shopListArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)typelistArr
{
    if (!_typelistArr) {
        _typelistArr = [NSMutableArray array];
    }
    return _typelistArr;
}
- (NSMutableArray *)shopListArr
{
    if (!_shopListArr) {
        _shopListArr = [NSMutableArray array];
    }
    return _shopListArr;
}
@end
