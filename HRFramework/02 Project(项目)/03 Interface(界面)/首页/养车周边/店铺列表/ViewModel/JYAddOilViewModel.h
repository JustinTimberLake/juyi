//
//  JYAddOilViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYAddOilViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *typelistArr;

@property (nonatomic,strong) NSMutableArray *shopListArr;

//3.6.1.	JY-006-001获取加油站类型
- (void)requestGetStationTypeWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.6.2.	JY-006-002获取加油站列表
- (void)requestGetStationListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
