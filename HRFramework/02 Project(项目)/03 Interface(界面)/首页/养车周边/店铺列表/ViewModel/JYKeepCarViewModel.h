//
//  JYKeepCarViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYKeepCarViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *typelistArr;

@property (nonatomic,strong) NSMutableArray *shopListArr;

//3.3.1	JY-005-001 获取养车店类型
- (void)requestGetKeepTypeWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.3.1	JY-005-002 获取养车店列表
- (void)requestGetKeepListWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
