//
//  JYStoreListTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYShopListModel;

@interface JYStoreListTableViewCell : UITableViewCell
@property (nonatomic,assign,getter=isGasStation) BOOL gasStation;
@property (nonatomic,strong) JYShopListModel *listModel;
//是否是高亮
//@property (nonatomic,assign,getter=isHighLight) BOOL highLight;

- (void)setHighLightWithKeywords:(NSString *)keywords;

@property (nonatomic,copy) void (^navBlock) (void);

+ (void)gotoMap:(JYShopListModel*)model andViewController:(UIViewController *)viewController;

+ (void)showAlertPicViewWith:(NSString *)telNum and:(UIViewController *)viewController;

@end
