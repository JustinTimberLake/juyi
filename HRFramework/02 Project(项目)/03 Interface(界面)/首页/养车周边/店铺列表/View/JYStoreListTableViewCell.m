//
//  JYStoreListTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYStoreListTableViewCell.h"
#import "JYShopListModel.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件
//#import <MapKit/MapKit.h>
//#import <CoreLocation/CoreLocation.h>
#import "JYAlertPicViewController.h"
#import "HRCall.h"
#import <MapKit/MapKit.h>
@interface JYStoreListTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (weak, nonatomic) IBOutlet UIView *starView;
@property (weak, nonatomic) IBOutlet UIButton *navButton;
@property (weak, nonatomic) IBOutlet UIImageView *TelImageView;
@property (weak, nonatomic) IBOutlet UIButton *telBtn;
@property (weak, nonatomic) IBOutlet UIImageView *addressImageView;


@end
@implementation JYStoreListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.navButton.layer setMasksToBounds:YES];
    [self.navButton.layer setCornerRadius:4.0]; //设置矩形四个圆角半径
}

- (void)setListModel:(JYShopListModel *)listModel{
    _listModel = listModel;
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:listModel.shopImage] placeholderImage:[UIImage imageNamed:@"店铺列表"]];
    self.titleLab.text = listModel.shopTitle;

    self.addressLab.text = listModel.shopAddress;
    if(!self.isGasStation){
        self.telLab.text = listModel.shopTel;
    }else{
        self.telLab.text = [NSString stringWithFormat:@"营业时间:%@",listModel.business_hours];
        self.TelImageView.image = [UIImage imageNamed:@"营业时间"];
        self.addressImageView.image = [UIImage imageNamed:@"address"];
    }
    
    [self configStar:listModel];
    
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
//    if (SCREEN_WIDTH<=320) {
//        [self.navButton setTitle:[NSString stringWithFormat:@"%@KM",[self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[listModel.shopLat floatValue] endLng:[listModel.shopLong floatValue]]] forState:UIControlStateNormal];
//    }else{
        [self.navButton setTitle:[NSString stringWithFormat:@"%@KM",[self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[listModel.shopLat floatValue] endLng:[listModel.shopLong floatValue]]] forState:UIControlStateNormal];
//    }
    
}
- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.titleLab setLabelTitleHighlight:self.listModel.shopTitle andSearchStr:keywords];
}


- (void)configStar:(JYShopListModel *)model{
    NSInteger num = [model.shopEvaluate intValue];
    for (UIImageView * view in self.starView.subviews) {
        if ((view.tag - 1000) < num) {
            view.image = [UIImage imageNamed:@"评价黄星"];
        }else{
            view.image = [UIImage imageNamed:@"评价灰星"];
        }
    }
}

//获得两点之间的距离
- (NSString *)getTheDistanceWithStartLat:(CGFloat)startLat startLng:(CGFloat)startLng andEndLat:(CGFloat)endLat endLng:(CGFloat )endLng{
    BMKMapPoint start = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(startLat,startLng));
    BMKMapPoint end = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(endLat,endLng));
    CLLocationDistance distance = BMKMetersBetweenMapPoints(start, end);
    NSString *distanceStr;
//    if (distance > 1000) {
        distanceStr = [NSString stringWithFormat:@"%.2f",distance/1000];
//    }else{
//        distanceStr = [NSString stringWithFormat:@"%.2fm",distance];
//    }
    return distanceStr;
}

- (IBAction)navBtnClick:(UIButton *)sender {
    if (self.navBlock) {
        self.navBlock();
    }
}

#pragma mark - 去地图展示路线
/** 去地图展示路线 */
+ (void)gotoMap:(JYShopListModel*)model andViewController:(UIViewController *)viewController{
    // 后台返回的目的地坐标是百度地图的
    // 百度地图与高德地图、苹果地图采用的坐标系不一样，故高德和苹果只能用地名不能用后台返回的坐标
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
    
    CGFloat latitude  = model.shopLat.floatValue;  // 纬度
    CGFloat longitude = model.shopLong.floatValue; // 经度
    NSString *address = model.shopAddress; // 地址
    
    // 打开地图的优先级顺序：百度地图->高德地图->苹果地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        // 百度地图
//    baidumap://map/direction?origin=中关村&destination=五道口&mode=driving&region=北京
//        //本示例是通过该URL启动地图app并进入北京市从中关村到五道口的驾车导航路线图
        // 起点为“我的位置”，终点为后台返回的坐标
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=%f,%f&mode=driving&src=%@", latitude, longitude,model.shopTitle] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        NSURL *url = [NSURL URLWithString:urlString];
        [[UIApplication sharedApplication] openURL:url];
    }else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
    // 高德地图
    // 起点为“我的位置”，终点为后台返回的address
        NSString *urlString = [[NSString stringWithFormat:@"iosamap://path?sourceApplication=applicationName&sid=BGVIS1&slat=%lf&slon=%lf&sname=我的位置&did=BGVIS2&dlat=%lf&dlon=%lf&dname=%@&dev=0&m=0&t=0",startLat, startLng,latitude,longitude,model.shopTitle] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com"]]){
        // 苹果地图
        CLLocationCoordinate2D destination = CLLocationCoordinate2DMake([model.shopLat floatValue], [model.shopLong floatValue]);
        
        MKMapItem *currentLocation =[MKMapItem mapItemForCurrentLocation];
        
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:destination addressDictionary:nil]];
        toLocation.name = model.shopTitle;
        
        [MKMapItem openMapsWithItems:@[currentLocation,toLocation] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
        }else{
            // 快递员没有安装上面三种地图APP，弹窗提示安装地图APP
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请安装地图APP" message:@"建议安装百度地图APP" preferredStyle:UIAlertControllerStyleAlert];
            [viewController presentViewController:alertVC animated:NO completion:nil];
        }
}
+ (void)showAlertPicViewWith:(NSString *)telNum and:(UIViewController *)viewController{
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.contentText= SF(@"呼叫%@",telNum );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [HRCall callPhoneNumber:telNum alert:NO];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [viewController presentViewController:alertVC animated:YES completion:nil];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
