//
//  JYMaintainCarAroundCell.m
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMaintainCarAroundCell.h"

@implementation JYMaintainCarAroundCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.picImageView.clipsToBounds = YES;
}

@end
