//
//  JYMaintainCarAroundCell.h
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYMaintainCarAroundCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
