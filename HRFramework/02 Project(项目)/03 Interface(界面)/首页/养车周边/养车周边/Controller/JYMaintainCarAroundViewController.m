//
//  JYMaintainCarAroundViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMaintainCarAroundViewController.h"
#import "JYMaintainCarAroundCell.h"
#import "JYStoreListViewController.h"
#import "JYBaseWebViewController.h"
#import "JYCommonWebViewController.h"

@interface JYMaintainCarAroundViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic,strong) NSArray *typeTitleArr;
@property (nonatomic,strong) NSArray *imageArr;
@property (nonatomic,strong) NSArray *contentArray;

@end

@implementation JYMaintainCarAroundViewController{
    NSArray *_contentArray;
}
static NSString * const cellId = @"JYMaintainCarAroundCell";
static CGFloat const margin = 10;



- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"养车周边";
    [self congfigCollectionView];
}
- (void)congfigCollectionView{
    
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYMaintainCarAroundCell" bundle:nil] forCellWithReuseIdentifier:cellId];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.typeTitleArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYMaintainCarAroundCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    cell.contentLabel.text = self.contentArray[indexPath.item];
    cell.titleLab.text = self.typeTitleArr[indexPath.item];
    cell.picImageView.image = [UIImage imageNamed:self.imageArr[indexPath.item]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0://洗车美容
        {
            JYStoreListViewController * vc = [[JYStoreListViewController alloc]init];
            vc.storeType = JYStoreType_CarWashBeauty;
            vc.currentListMode = JYCurrentListModeMaintainCar;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1://汽车改装
        {
            JYStoreListViewController * vc = [[JYStoreListViewController alloc]init];
            vc.storeType = JYStoreType_CarAutoRepairAdaptations;
            vc.currentListMode = JYCurrentListModeMaintainCar;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        case 2://4s店保养
        {
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = @"https://m.lechebang.com/duijie/h5/index?lcb_act_id=1467&lcb_al_id=1260&lcb_site_id=1261&lcb_show_bar=0";
            webVC.navTitle = @"4S店保养";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 3://车险
        {
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = @"https://product.haibaobaoxian.com/index?pcode=weix-h5-all";
            webVC.navTitle = @"车险";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
            
        default:
            break;
    }
 }
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - 20),HB_IPONE6_ADAPTOR_WIDTH(133));
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//这个是两行cell之间的间距（上下行cell的间距）
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return margin;
}

//两个cell之间的间距（同一行的cell的间距）
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return margin/2;
}

- (NSArray *)typeTitleArr
{
    if (!_typeTitleArr) {
        _typeTitleArr = @[@"洗车美容",@"汽修改装",@"4S店保养",@"车险"];
    }
    return _typeTitleArr;
}

- (NSArray *)imageArr
{
    if (!_imageArr) {
        _imageArr = @[@"洗车美容",@"汽修改装",@"4s店保养",@"车险"];
    }
    return _imageArr;
}
-(NSArray *)contentArray{
    if (!_contentArray) {
        _contentArray = @[@"更细心的养护",@"更用心的技术",@"更专心的服务",@"更安心的护航"];
    }
    return _contentArray;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
