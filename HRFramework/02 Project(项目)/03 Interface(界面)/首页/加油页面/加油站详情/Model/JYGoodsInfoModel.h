//
//  JYGoodsInfoModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYImageModel.h"

@interface JYGoodsInfoModel : JYBaseViewModel
//商品ID
@property (nonatomic,copy) NSString *goodsId;
//标题
@property (nonatomic,copy) NSString *goodsTitle;
//副标题(绿色字部分)
@property (nonatomic,copy) NSString *goodsSubtitle;
//图片
@property (nonatomic,copy) NSString *goodsImage;
//价格
@property (nonatomic,copy) NSString *goodsPrice;
//积分
@property (nonatomic,copy) NSString *goodsScore;
//环信id
@property (nonatomic,copy) NSString *hxUserId;
//是否收藏（1已收藏2未收藏）
@property (nonatomic,copy) NSString *isCollect;
// H5内容(详细介绍Url)
@property (nonatomic,copy) NSString *goodsContent;
//头部图片
@property (nonatomic,strong) NSArray<JYImageModel *> *goodsImages;
//分享链接
@property (nonatomic,copy) NSString *shareUrl;
//分享标题
@property (nonatomic,copy) NSString *shareTitle;
//运费
@property (nonatomic,copy) NSString *freight;
//是否包邮（1包邮2不包邮）
@property (nonatomic,copy) NSString *isFreeShip;
//满多少包邮
@property (nonatomic,copy) NSString *freeShipping;
//是否支持七天无条件退货(1支持2不支持)
@property (nonatomic,copy) NSString *support;
//店铺id
@property (nonatomic,copy) NSString *shopId;
//店铺名字
@property (nonatomic,copy) NSString *shopTitle;
//商品是否下架   1上架2下架
@property (nonatomic,copy) NSString *productStatus;

@end
