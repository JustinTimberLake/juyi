//
//  JYGoodsInfoModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGoodsInfoModel.h"

@implementation JYGoodsInfoModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"goodsImages":@"JYImageModel"
             };
}
@end
