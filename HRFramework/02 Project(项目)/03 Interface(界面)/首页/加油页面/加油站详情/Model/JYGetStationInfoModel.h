//
//  JYGetStationInfoModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
@class JYImageModel;

@interface JYGetStationInfoModel : JYBaseModel
// 加油站id
@property (nonatomic,copy) NSString *shopId;
//加油站标题
@property (nonatomic,copy) NSString *shopTitle;
//加油站图片
@property (nonatomic,copy) NSString *shopImage;
// 加油站评价
@property (nonatomic,copy) NSString *shopEvaluate;
// 加油站地址
@property (nonatomic,copy) NSString *shopAddress;
// 加油站电话
@property (nonatomic,copy) NSString *shopTel;
// 加油站经度
@property (nonatomic,copy) NSString *shopLong;
// 加油站纬度
@property (nonatomic,copy) NSString *shopLat;
// 加油站环信id
@property (nonatomic,copy) NSString *hxUserId;
//是否收藏（1已收藏2未收藏）
@property (nonatomic,copy) NSString *isCollect;
//优惠券
@property (nonatomic,copy) NSString *shopCoupon;
//积分
@property (nonatomic,copy) NSString *shopScore;
// H5内容Url
@property (nonatomic,copy) NSString *shopContent;
//头部图片
@property (nonatomic,strong) NSArray<JYImageModel *> *shopImages;
//分享链接
@property (nonatomic,copy) NSString *shareUrl;
//分享标题
@property (nonatomic,copy) NSString *shareTitle;

@end
