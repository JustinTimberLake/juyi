//
//  JYCommentModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommentModel.h"

@implementation JYCommentModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"commentImages":@"JYImageModel",
             @"replyImages":@"JYImageModel"
             };
}
@end
