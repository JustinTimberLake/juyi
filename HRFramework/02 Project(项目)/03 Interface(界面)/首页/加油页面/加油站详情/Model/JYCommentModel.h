//
//  JYCommentModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYImageModel.h"

@interface JYCommentModel : JYBaseModel
//评论ID
@property (nonatomic,copy) NSString *commentId;
//昵称
@property (nonatomic,copy) NSString *commentTitle;
//用户头像
@property (nonatomic,copy) NSString *commentHead;
//时间
@property (nonatomic,copy) NSString *commentTime;
//评价星级
@property (nonatomic,copy) NSString *commentStar;
// 评价内容
@property (nonatomic,copy) NSString *commentContent;
// 评价图片
@property (nonatomic,copy) NSArray<JYImageModel *> *commentImages;
// 回复内容
@property (nonatomic,copy) NSString *replyContent;
// 回复图片
@property (nonatomic,copy) NSArray<JYImageModel *> *replyImages;
@end
