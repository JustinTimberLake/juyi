//
//  JYImageModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYImageModel : JYBaseModel
//图片url
@property (nonatomic,copy) NSString *imageUrl;
@property (nonatomic,copy) NSString *licenseId;
@end
