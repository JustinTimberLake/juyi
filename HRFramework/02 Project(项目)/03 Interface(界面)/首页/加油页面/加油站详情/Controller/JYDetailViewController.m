//
//  JYDetailViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYDetailViewController.h"
#import "JYCommonCommentTableViewCell.h"
#import "SDCycleScrollView.h"
#import "JYChooseOilGunViewController.h"
#import "JYShareViewController.h"
#import "JYLookAllCommentViewController.h"
#import "JYMakeSureOrderWithAddOilTypeViewController.h"
#import "JYChooseFormatViewController.h"
#import "JYMakeSureOrderWithStoreTypeViewController.h"
#import "JYMakeSureOrderWithStoreTypeViewController.h"
#import "JYStoreViewController.h"
#import "HR_ZoomViewVC.h"
//#import "ZKshareView.h"
#import "UShareCustomModel.h"
#import "JYGetStationInfoViewModel.h"
#import "JYImageModel.h"
#import "JYDetailWebCell.h"
#import "JYGetGoodsInfoViewModel.h"
#import "JYCollectionViewModel.h"
#import "UShareCustomModel.h"
#import "JYChatViewController.h"
#import "JYGetAttributeViewModel.h"
#import "JYContactUsViewController.h"
#import "IQKeyboardManager.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件
#import "JYAlertPicViewController.h"
#import "HRCall.h"
static NSString * const CommentCellId = @"JYCommonCommentTableViewCell";
static NSString * const WebCellCellId = @"JYDetailWebCell";

@interface JYDetailViewController ()<
    UITableViewDelegate,
    UITableViewDataSource,
    SDCycleScrollViewDelegate,
    UIWebViewDelegate
>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeight;
@property (strong, nonatomic) IBOutlet UIView *topHeaderView;
@property (weak, nonatomic) IBOutlet UIView *bannerView;

@property (weak, nonatomic) IBOutlet UITableView *detailTableView;
@property (weak, nonatomic) IBOutlet UIView *bottomBtnView;
@property (strong, nonatomic) IBOutlet UIView *storeBottomView;
@property (strong, nonatomic) IBOutlet UIView *addOilBottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerViewH;

@property (nonatomic,strong) SDCycleScrollView * scrollView;
@property (weak, nonatomic) IBOutlet UIView *imageCountView;
@property (weak, nonatomic) IBOutlet UILabel *imageCountLab;
@property (weak, nonatomic) IBOutlet UIButton *detailTitleBtn;

@property (weak, nonatomic) IBOutlet UIView *yellowLineView;
@property (nonatomic,strong) UIButton * selectBtn;

@property (strong, nonatomic) IBOutlet UIView *LookallCommentView;
@property (weak, nonatomic) IBOutlet UIButton *lookAllCommentBtn;

@property (weak, nonatomic) IBOutlet UIView *productTypePriceView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailTitleViewH;
@property (weak, nonatomic) IBOutlet UILabel *chooseItemLab;
@property (weak, nonatomic) IBOutlet UIView *categoryView;
//@property (nonatomic,strong) ZKshareView *shareView ;
//加油站详情viewmodel
@property (nonatomic,strong) JYGetStationInfoViewModel *getStationInfoViewModel;
@property (nonatomic,strong) JYGetGoodsInfoViewModel *goodsInfoViewModel;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;

@property (weak, nonatomic) IBOutlet UILabel *detailTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *integralLab;
@property (weak, nonatomic) IBOutlet UILabel *freightLab;
@property (weak, nonatomic) IBOutlet UILabel *noticeTopLab;
@property (weak, nonatomic) IBOutlet UILabel *noticeBottomLab;
//@property (weak, nonatomic) IBOutlet UIWebView *detailWebView;
//是否是评论模式
@property (nonatomic, assign , getter=isCommentMode) BOOL commentMode;

@property (weak, nonatomic) IBOutlet UIButton *addOiLCollectionBtn;
@property (weak, nonatomic) IBOutlet UIButton *storeCollectionBtn;
@property (weak, nonatomic) IBOutlet UILabel *notiTopTitleLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notiTopTitleWidth;

@property (weak, nonatomic) IBOutlet UILabel *notiBottomTitleLab;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;


@property (nonatomic,assign) JY_CollectionType collectionType;
//1收藏2取消收藏
@property (nonatomic,copy) NSString *operatStr;
@property (nonatomic,strong) UShareCustomModel *shareModel;

//属性VM
@property (nonatomic,strong) JYGetAttributeViewModel *attributeViewModel;
@property (nonatomic,strong) JYChooseFormatViewController * formatVC ;
@property (nonatomic,strong) NSArray *bannerImageArr; //轮播图片数组

//修改新增
@property (strong, nonatomic) IBOutlet UIView *GasStationDetailView;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailViewHeight;
@property (strong, nonatomic) IBOutlet UIView *productDetailView;
//油站详情
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;





@end

@implementation JYDetailViewController{
    CGFloat _webHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatBannerView];
    [self configUI];
    
    if (self.detailType == DetailTypeAddOil) {
       [self requestGetStationInfo];
    }else{
        [self requestGoodsInfoWithDetailType:self.detailType];
    }
    
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 80.0f;

}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.topHeaderView.height = CGRectGetMaxY(self.categoryView.frame);
    
    if (kDevice_Is_iPhoneX) {
        _bottomHeight.constant = 83;
    }
    
}

-  (void)configUI{

    if (self.detailType == DetailTypeAddOil) {
        self.naviTitle = @"油站详情";
        self.addOilBottomView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
        [self.bottomBtnView addSubview:self.addOilBottomView];
        self.productTypePriceView.hidden = YES;
        self.detailTitleViewH.constant = 50;
        self.chooseItemLab.text = @"选择油枪/油量";
        self.notiTopTitleLab.text = @"优惠券";
        self.notiBottomTitleLab.text = @"积分";
        self.detailViewHeight.constant = 130;
        [self.detailView addSubview: self.GasStationDetailView];
        [self.GasStationDetailView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];
    }else{
        self.naviTitle = @"商品详情";
        self.storeBottomView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
        [self.bottomBtnView addSubview:self.storeBottomView];
        self.productTypePriceView.hidden = NO;
        self.detailTitleViewH.constant = 100;
        self.chooseItemLab.text = @"选择规格";
        self.notiTopTitleLab.text = @"活动";
        self.notiBottomTitleLab.text = @"保证";
        [self.detailTitleBtn setTitle:@"商品详情" forState:UIControlStateNormal];
        self.detailViewHeight.constant = 100;
        [self.detailView addSubview: self.productDetailView];
        [self.productDetailView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.offset(0);
        }];

    }
    
    self.notiTopTitleWidth.constant = self.notiTopTitleLab.text.length == 2 ? 25 : 35;
    self.topHeaderView.height = CGRectGetMaxY(self.categoryView.frame);
    self.detailTableView.tableHeaderView = self.topHeaderView;
    self.topHeaderView.backgroundColor = RGB0X(0xf5f5f5);
    [self.detailTableView registerNib:[UINib nibWithNibName:@"JYCommonCommentTableViewCell" bundle:nil] forCellReuseIdentifier:CommentCellId];
    [self.detailTableView registerNib:[UINib nibWithNibName:@"JYDetailWebCell" bundle:nil] forCellReuseIdentifier:WebCellCellId];
    
    [self.bannerView bringSubviewToFront:self.imageCountView];
    
    [self addBorderAndCornerWithView:self.notiTopTitleLab borderColor:RGB0X(0xde2529) borderWidth:0.5 cornerRadius:3];
     [self addBorderAndCornerWithView:self.notiBottomTitleLab borderColor:RGB0X(0xde2529) borderWidth:0.5 cornerRadius:3];
    
    self.detailTitleBtn.selected = YES;
    self.selectBtn = self.detailTitleBtn;
    self.yellowLineView.width = 65;
    self.yellowLineView.height = 2;
    self.yellowLineView.centerX = self.detailTitleBtn.centerX;
    
//    WEAK(weakSelf)
//    [self rightImageItem:@"分享" action:^{
//        JYShareViewController * shareVC = [[JYShareViewController alloc] init];
//        shareVC.model = self.shareModel;
//        shareVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        [weakSelf presentViewController:shareVC animated:YES completion:nil];
//
//    }];
}

//增加边框和圆角 ，颜色
- (void)addBorderAndCornerWithView:(UIView *)view borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius{
    view.layer.cornerRadius = cornerRadius;
    view.layer.borderColor = borderColor.CGColor;
    view.layer.borderWidth = borderWidth;
    view.clipsToBounds = YES;
}

- (void)creatBannerView{
    WEAKSELF
    self.bannerViewH.constant = HB_IPONE6_ADAPTOR_HEIGHT(240);
    //轮播图也写到layoutSubview里了
    self.scrollView = [SDCycleScrollView cycleScrollViewWithFrame:(CGRectMake(0, 0, SCREEN_WIDTH, self.bannerViewH.constant)) delegate:self  placeholderImage:[UIImage imageNamed:@"商品详情头图"]];
    self.scrollView.delegate = self; // 如需监听图片点击，请设置代理，实现代理方法
    self.scrollView.autoScrollTimeInterval = 3;// 自定义轮播时间间隔
    self.scrollView.titleLabelBackgroundColor = [UIColor clearColor];
    self.scrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    self.scrollView.showPageControl = NO;
    self.scrollView.backgroundColor = [UIColor redColor];
    [self.bannerView addSubview:self.scrollView];
    
    
    
    self.scrollView.clickItemOperationBlock = ^(NSInteger currentIndex) {
        HR_ZoomViewVC *imgBorwserVC = [[HR_ZoomViewVC alloc] init];
        imgBorwserVC.idx = currentIndex+1;
//        imgBorwserVC.dataSourceArrForURLsGroup = [WS.Model.GoodsModel.productImgs copy];
        imgBorwserVC.dataSourceArrForURLsGroup = weakSelf.bannerImageArr;
        
        imgBorwserVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:imgBorwserVC animated:YES completion:nil];
    };
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark ==================自定义方法==================
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

#pragma mark -------------Action--------------
- (IBAction)titleBtnAction:(UIButton *)sender {
    self.selectBtn.selected = NO;
    self.selectBtn = sender;
    self.selectBtn.selected = YES;
    self.yellowLineView.centerX = sender.centerX;
    if (sender.tag == 2001) {
        self.detailTableView.tableFooterView = self.LookallCommentView;
        self.commentMode = YES;
    }else{
         self.detailTableView.tableFooterView = nil;
        self.commentMode = NO;
    }
    [self.detailTableView reloadData];
}

- (IBAction)shareBtnAction:(UIButton *)sender {
    JYShareViewController * shareVC = [[JYShareViewController alloc] init];
    shareVC.model = self.shareModel;
//    shareVC.ShareBtnActionBlock = ^(NSInteger tag) {
//        if (tag == 1000) {
//            NSLog(@"微信好友");
//        }else if (tag == 1001){
//            NSLog(@"朋友圈");
//        }else if (tag == 1002){
//            NSLog(@"qq好友");
//        }else{
//            NSLog(@"微博");
//        }
//    };
    shareVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:shareVC animated:YES completion:nil];
//    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    //分享界面
    //            ZKshareView *shareView = [[ZKshareView alloc] init];
    //            shareView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    //
    //            shareView.Model = shareModel;
    //            shareView.Success = ^(NSString *str) {
    //                [self showSuccessTip:str];
    //            };
    //            shareView.Faile = ^(NSString *file) {
    //                [self showSuccessTip:file];
    //            };
    //
    //            [window addSubview:shareView];
//    WEAKSELF
//    UShareCustomModel * model = [[UShareCustomModel alloc] init];
//    weakSelf.shareView.Model = model;
//    weakSelf.shareView.Success = ^(NSString *str) {
//        [weakSelf showSuccessTip:str];
//    };
//    weakSelf.shareView.Faile = ^(NSString *file) {
//        [weakSelf showSuccessTip:file];
//    };
//    [window addSubview:weakSelf.shareView];
    

}

//右侧三个点的点击
- (IBAction)showChooseViewBtnAction:(UIButton *)sender {
    WEAKSELF
    if (![self judgeLogin]) {
        return;
    }
    if (self.detailType == DetailTypeAddOil){
        JYChooseOilGunViewController * choseOilGunVC = [[JYChooseOilGunViewController alloc]init];
        choseOilGunVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        choseOilGunVC.shopId = self.shopId;
        
        choseOilGunVC.addOilBtnActionBlock = ^(NSString *gunId, NSString *shopId, NSString *oilNum,JYOilGunModel *gunModel,NSString *totalPrice) {
            
            //判断登录
            BOOL isLogin = [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
                
            }];
            if (isLogin) {
                JYMakeSureOrderWithAddOilTypeViewController * makeSureOrderVC = [[JYMakeSureOrderWithAddOilTypeViewController alloc] init];
                makeSureOrderVC.gunId = gunId;
                makeSureOrderVC.shopId = shopId;
                makeSureOrderVC.oilNum = oilNum;
                makeSureOrderVC.infoModel = weakSelf.getStationInfoViewModel.infoModel;
                makeSureOrderVC.totalPrice = totalPrice;
                makeSureOrderVC.gunModel = gunModel;
                [weakSelf.navigationController pushViewController:makeSureOrderVC animated:YES];
            }
        };
        [self presentViewController:choseOilGunVC animated:YES completion:nil];

    }else {
        [self requestAttributeTitleWithCompleteBlock:^(NSString *msg, id responseData) {
            NSString * titleOneStr = responseData[@"item1name"];
            NSString * titleTwoStr = responseData[@"item2name"];
            
//              weakSelf.formatVC.goodsId = self.goodsInfoViewModel.goodsModel.goodsId;
            
            if (weakSelf.detailType == DetailTypeStore) {
                [weakSelf creatFormatVCWithDetailType:self.detailType andItem1Name:titleOneStr andItem2Name:titleTwoStr];


            }else if(weakSelf.detailType == DetailTypeKeepCar){
                 weakSelf.formatVC.typeStr = @"1";
//                实体
                if (!titleOneStr.length && !titleTwoStr.length) {
                    //                没有属性，都不显示弹窗，直接跳转确认订单页面
                    
                    JYMakeSureOrderWithStoreTypeViewController * orderVC = [[JYMakeSureOrderWithStoreTypeViewController alloc] init];
                        JYGetAttributeModel * attributeModel = [[JYGetAttributeModel alloc] init];
                        orderVC.attributeModel = attributeModel;
                        orderVC.goodsNum = @"1";
                        orderVC.goodsModel = weakSelf.goodsInfoViewModel.goodsModel;
                        orderVC.typeStr = @"1";
                        orderVC.item1Name = @"";
                        orderVC.item2Name = @"";
                    
                    [weakSelf.navigationController pushViewController:orderVC animated:YES];

                }else{
                    [weakSelf creatFormatVCWithDetailType:self.detailType andItem1Name:titleOneStr andItem2Name:titleTwoStr];

                }
            }
        }];
        
//        JYChooseFormatViewController * formatVC = [[JYChooseFormatViewController alloc] init];
//        formatVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        formatVC.commitBtnActionBlock = ^(JYGetAttributeModel * currentModel,NSString *goodsNum){
//            JYMakeSureOrderWithStoreTypeViewController * orderVC = [[JYMakeSureOrderWithStoreTypeViewController alloc] init];
//            orderVC.attributeModel = currentModel;
//            orderVC.goodsNum = goodsNum;
//            orderVC.goodsModel = weakSelf.goodsInfoViewModel.goodsModel;
//            if (weakSelf.detailType == DetailTypeStore) {
//                orderVC.typeStr = @"2";
//            }else if (weakSelf.detailType == DetailTypeKeepCar){
//                orderVC.typeStr = @"1";
//            }
//            [weakSelf.navigationController pushViewController:orderVC animated:YES];
//        };
//        formatVC.goodsId = self.goodsInfoViewModel.goodsModel.goodsId;
//        if (self.detailType == DetailTypeStore) {
//            formatVC.typeStr = @"2";
//        }else if (self.detailType == DetailTypeKeepCar){
//            formatVC.typeStr = @"1";
//        }
//        [self presentViewController:formatVC animated:YES completion:nil];
    }
}

//创建一个规格页面
- (void)creatFormatVCWithDetailType:(DetailType)detailType andItem1Name:(NSString *)item1Name andItem2Name:(NSString *)item2Name{
    WEAKSELF
    JYChooseFormatViewController * formatVC = [[JYChooseFormatViewController alloc] init];
    formatVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    formatVC.titleOneStr = item1Name;
    formatVC.titleTwoStr = item2Name;
//    [formatVC updataTitleWithItem1Name:item1Name andItem2Name:item2Name];
    formatVC.goodsId = self.goodsInfoViewModel.goodsModel.goodsId;
    if (detailType == DetailTypeStore) {
        formatVC.typeStr = @"2";
    }else if (detailType == DetailTypeKeepCar){
        formatVC.typeStr = @"1";
    }
    formatVC.commitBtnActionBlock = ^(JYGetAttributeModel * currentModel,NSString *goodsNum){
        
        if (![self judgeLogin]) {
            return;
        }

//        if (item1Name.length && !currentModel.item1.length) {
//            [weakSelf showSuccessTip:SF(@"请选择%@",item1Name)];
//            return ;
//        }
//        if (item2Name.length && !currentModel.item2.length) {
//            [weakSelf showSuccessTip:SF(@"请选择%@",item2Name)];
//            return ;
//        }
        if (!item1Name.length && !item2Name.length) {
            currentModel.price = weakSelf.goodsInfoViewModel.goodsModel.goodsPrice;
        }
 
        JYMakeSureOrderWithStoreTypeViewController * orderVC = [[JYMakeSureOrderWithStoreTypeViewController alloc] init];
        orderVC.attributeModel = currentModel;
        orderVC.goodsNum = goodsNum;
        orderVC.goodsModel = weakSelf.goodsInfoViewModel.goodsModel;
        orderVC.item1Name = item1Name;
        orderVC.item2Name = item2Name;
        if (detailType == DetailTypeStore) {
            orderVC.typeStr = @"2";
        }else if (detailType == DetailTypeKeepCar){
            orderVC.typeStr = @"1";
        }
        [weakSelf.navigationController pushViewController:orderVC animated:YES];
    };
    
    [self.navigationController presentViewController:formatVC animated:YES completion:nil];
}

- (IBAction)lookAllCommentBtnAction:(UIButton *)sender {
    JYLookAllCommentViewController * allCommentVC = [[JYLookAllCommentViewController alloc] init];
    allCommentVC.detailType = self.detailType;
    allCommentVC.shopId = self.shopId;
    allCommentVC.goodsId = self.goodsId;
    [self.navigationController pushViewController:allCommentVC animated:YES];
}

//加油详情模式下底部按钮点击
- (IBAction)bottomBtnWithAddOilTypeAction:(UIButton *)sender {
    if (![self judgeLogin]) {
        return;
    }
    switch (sender.tag) {
        case 1000:{
            NSLog(@"收藏");
            sender.selected = !sender.selected;
            self.operatStr = sender.selected ?@"1":@"2";
            [self requestCollectionOperation];
        }
            break;
        case 1001:{
            //单聊界面
            NSString * userId = self.getStationInfoViewModel.infoModel.hxUserId;
            JYChatViewController *chatController = [[JYChatViewController alloc] initWithConversationChatter:userId conversationType:EMConversationTypeChat];
            chatController.chatTitle = @"客服";
            [self.navigationController pushViewController:chatController animated:YES];
        }
            break;
        case 1003:{
            NSLog(@"提交订单");
            [self showChooseViewBtnAction:sender];
        }
            break;
        case 1004:{
            NSLog(@"客服");
            JYContactUsViewController *vc = [[JYContactUsViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;

        default:
            break;
    }
}

//店铺详情底部按钮
- (IBAction)bottomBtnWithStoreTypeAction:(UIButton *)sender {
    if (sender.tag != 1002) {
        if (![self judgeLogin]) {
            return;
        }
    }
    switch (sender.tag) {
        case 1000:{
            NSLog(@"收藏");
            sender.selected = !sender.selected;
            self.operatStr = sender.selected ?@"1":@"2";
            [self requestCollectionOperation];
        }
            break;
        case 1001:{
            //单聊界面
            NSString * userId = self.goodsInfoViewModel.goodsModel.hxUserId;
            JYChatViewController *chatController = [[JYChatViewController alloc] initWithConversationChatter:userId conversationType:EMConversationTypeChat];
            chatController.chatTitle = @"客服";
            [self.navigationController pushViewController:chatController animated:YES];

        }
            break;
        case 1002:{
            JYStoreViewController * storeVC = [[JYStoreViewController alloc] init];
            storeVC.shopId = self.goodsInfoViewModel.goodsModel.shopId;
            [self.navigationController pushViewController:storeVC animated:YES];
        }
            break;

        case 1003:{
            NSLog(@"提交订单");
            if ([self.goodsInfoViewModel.goodsModel.productStatus intValue] == 2) {
                [self showSuccessTip:@"商品已下架"];
                return;
            }
            
            [self showChooseViewBtnAction:sender];
//            JYMakeSureOrderWithStoreTypeViewController * makeSureOrderVC = [[JYMakeSureOrderWithStoreTypeViewController alloc] init];
//            makeSureOrderVC.goodsModel = self.goodsInfoViewModel.goodsModel;
//            [self.navigationController pushViewController:makeSureOrderVC animated:YES];
        }
            
            break;
            
        default:
            break;
    }
}


#pragma mark -------------网络请求--------------

//请求加油站详情
- (void)requestGetStationInfo{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = self.shopId;
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    [self.getStationInfoViewModel requestGetStationInfoWithParams:dic success:^(NSString *msg, id responseData) {
        self.shopId = self.getStationInfoViewModel.infoModel.shopId;
        [self requestCommentData];
        [weakSelf updataUIWithStation];
        
        [weakSelf.detailTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求商品详情
- (void)requestGoodsInfoWithDetailType:(DetailType)type{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    NSString * typeStr; //1实体服务2商品
    if (type == DetailTypeStore) {
        typeStr = @"2";
    }else if (type == DetailTypeKeepCar){
        typeStr = @"1";
    }
    dic[@"type"] = typeStr;
    dic[@"goodsId"] = self.goodsId;
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    
    [self.goodsInfoViewModel requestGetGoodsInfoWithParams:dic success:^(NSString *msg, id responseData) {
        self.shopId = self.goodsInfoViewModel.goodsModel.shopId;
        [weakSelf requestCommentData];
        [weakSelf updataUIWithGoods];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];

}


- (void)endRefresh{
    [self.detailTableView.mj_header endRefreshing];
    [self.detailTableView.mj_footer endRefreshing];
}

//请求加油站评论
- (void)requestCommentData{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(self.detailType);
    dic[@"shopId"] = self.shopId;
    dic[@"goodsId"] = self.goodsId;
    dic[@"state"] = @(1);  //1全部2晒图3低分4最新
    
    [self.getStationInfoViewModel requestGetCommentWithParams:dic isMore:NO count:3 success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.detailTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)requestCollectionOperation{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    if (self.detailType == DetailTypeAddOil) {
        dic[@"shopId"] = self.shopId;
    }else{
        dic[@"goodsId"] = self.goodsId;
    }
    dic[@"operat"] = self.operatStr;
    if (self.detailType == DetailTypeAddOil) {
        self.collectionType = JY_CollectionType_OilStation;
    }else{
        self.collectionType = JY_CollectionType_Goods;
    }
    [self.collectionViewModel requestCollectionWithParams:dic andType:self.collectionType success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"操作成功"];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
    
}

//请求属性标题
- (void)requestAttributeTitleWithCompleteBlock:(void (^)(NSString *msg, id responseData))completeBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    NSString *typeStr;
    if (self.detailType == DetailTypeStore) {
        typeStr = @"2";
    }else if (self.detailType == DetailTypeKeepCar){
        typeStr = @"1";
    }
    dic[@"type"] = typeStr;// 1实体服务2商品
    dic[@"goodsId"] = self.goodsId;
    
    [self.attributeViewModel requestGetAttributeTitleWithParams:dic success:^(NSString *msg, id responseData) {
//        NSString * titleOneStr = responseData[@"item1name"];
//        NSString * titleTwoStr = responseData[@"item2name"];
//        weakSelf.titleOneLab.text = titleOneStr;
//        weakSelf.titleTwoLab.text = titleTwoStr;
        completeBlock(msg,responseData);
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//更新页面UI (加油站详情)
- (void)updataUIWithStation{
//    self.detailTitleLab.text = self.getStationInfoViewModel.infoModel.shopTitle;
    self.noticeTopLab.text = self.getStationInfoViewModel.infoModel.shopCoupon;
    self.noticeBottomLab.text = self.getStationInfoViewModel.infoModel.shopScore;
    self.titleName.text = self.getStationInfoViewModel.infoModel.shopTitle;
    self.numberLabel.text = self.getStationInfoViewModel.infoModel.shopTel;
    
    
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
    
    NSString *distance = [NSString stringWithFormat:@"距离您%@KM",[self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[self.getStationInfoViewModel.infoModel.shopLat floatValue] endLng:[self.getStationInfoViewModel.infoModel.shopLong floatValue]]];
    
    self.addressLabel.text = [self.getStationInfoViewModel.infoModel.shopAddress stringByAppendingString:distance];
    NSMutableArray *imageArr = [NSMutableArray array];
    
    if (self.getStationInfoViewModel.infoModel.shopImages.count) {
        
        for (JYImageModel * model in self.getStationInfoViewModel.infoModel.shopImages) {
            [imageArr addObject:model.imageUrl];
        }
    }
    self.bannerImageArr = imageArr;
    self.scrollView.imageURLStringsGroup = imageArr;
    
    self.imageCountLab.hidden = imageArr.count ? NO :YES;
    self.imageCountLab.text = SF(@"%d/%lu",1,(unsigned long)imageArr.count);
    
    if ([self.getStationInfoViewModel.infoModel.isCollect intValue] == 1) {

        self.addOiLCollectionBtn.selected = YES;
    }else{
        self.addOiLCollectionBtn.selected = NO;
    }
    self.shareModel = [[UShareCustomModel alloc] init];
    
    self.shareModel.shareTitle = self.getStationInfoViewModel.infoModel.shareTitle;
    self.shareModel.shareUrl = self.getStationInfoViewModel.infoModel.shareUrl;
    self.shareBtn.enabled = self.getStationInfoViewModel.infoModel.shareUrl ? YES :NO;
    
    
}


//获得两点之间的距离
- (NSString *)getTheDistanceWithStartLat:(CGFloat)startLat startLng:(CGFloat)startLng andEndLat:(CGFloat)endLat endLng:(CGFloat )endLng{
    BMKMapPoint start = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(startLat,startLng));
    BMKMapPoint end = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(endLat,endLng));
    CLLocationDistance distance = BMKMetersBetweenMapPoints(start, end);
    NSString *distanceStr;
    //    if (distance > 1000) {
    distanceStr = [NSString stringWithFormat:@"%.2f",distance/1000];
    //    }else{
    //        distanceStr = [NSString stringWithFormat:@"%.2fm",distance];
    //    }
    return distanceStr;
}

//更新页面UI (商品详情)
- (void)updataUIWithGoods{
    self.detailTitleLab.text = self.goodsInfoViewModel.goodsModel.goodsTitle;
    self.priceLab.text = SF(@"￥%@",self.goodsInfoViewModel.goodsModel.goodsPrice);
    self.integralLab.text = SF(@"兑换价：%@积分",self.goodsInfoViewModel.goodsModel.goodsScore);
    NSString *  freightStr = self.goodsInfoViewModel.goodsModel.freight.length ? self.goodsInfoViewModel.goodsModel.freight : @"0";

    self.freightLab.text = SF(@"运费：%@",freightStr);
//    商品模式的运费显示   实体的和加油的不显示
    if (self.detailType == DetailTypeStore) {
        
        self.freightLab.hidden = self.goodsInfoViewModel.goodsModel.freight.length ? NO : YES;
    }else{
        self.freightLab.hidden = YES;
    }
    if ([self.goodsInfoViewModel.goodsModel.isFreeShip intValue] == 1) {
        self.noticeTopLab.text = @"包邮";
    }else{
        self.noticeTopLab.text = self.goodsInfoViewModel.goodsModel.freeShipping;
    }
    if ([self.goodsInfoViewModel.goodsModel.support intValue] == 1 ) {
        self.noticeBottomLab.text = @"本商品支持七天无理由退货" ;
    }else{
        self.noticeBottomLab.text = @"本商品不支持七天无理由退货" ;
    }
    
    NSMutableArray *imageArr = [NSMutableArray array];
    if (self.goodsInfoViewModel.goodsModel.goodsImages.count) {
        
        for (JYImageModel * model in self.goodsInfoViewModel.goodsModel.goodsImages) {
            [imageArr addObject:model.imageUrl];
        }
    }
    self.bannerImageArr = imageArr;
    self.scrollView.imageURLStringsGroup = imageArr;
    
    self.imageCountLab.hidden = imageArr.count ? NO :YES;
    self.imageCountLab.text = SF(@"%d/%lu",1,(unsigned long)imageArr.count);
    
    if ([self.goodsInfoViewModel.goodsModel.isCollect intValue] == 1) {

        self.storeCollectionBtn.selected = YES;
    }else{
        self.storeCollectionBtn.selected = NO;
    }
    
    self.shareModel = [[UShareCustomModel alloc] init];
    self.shareModel.shareTitle = self.goodsInfoViewModel.goodsModel.shareTitle;
    self.shareModel.shareUrl = self.goodsInfoViewModel.goodsModel.shareUrl;
    
    self.shareBtn.enabled = self.goodsInfoViewModel.goodsModel.shareUrl ? YES :NO;
}


#pragma mark -------------tableview 的数据源和代理--------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isCommentMode) {
        return self.getStationInfoViewModel.commentArr.count;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYCommonCommentTableViewCell * commentCell = [tableView dequeueReusableCellWithIdentifier:CommentCellId];
    [commentCell setUpCellMode:CellMode_Comment];
    
    JYDetailWebCell * webCell = [tableView dequeueReusableCellWithIdentifier:WebCellCellId];
    WEAKSELF;
    webCell.refreshBlock = ^(CGFloat height) {
        _webHeight = height;
        [weakSelf.detailTableView reloadData];
    };
    if (self.detailType == DetailTypeAddOil) {
        
        [webCell loadContentUrl:self.getStationInfoViewModel.infoModel.shopContent];
    }else{
        [webCell loadContentUrl:self.goodsInfoViewModel.goodsModel.goodsContent];
    }
    
    if (self.isCommentMode) {
        commentCell.commentModel = self.getStationInfoViewModel.commentArr[indexPath.row];
        return commentCell;
    }else{
        return webCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isCommentMode) {
        return [JYCommonCommentTableViewCell cellHeightWithModel:self.getStationInfoViewModel.commentArr[indexPath.row] ];
       
    }else{
        return  _webHeight ?_webHeight:SCREEN_HEIGHT - self.bottomBtnView.height - 64 - self.categoryView.height;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    tableView.estimatedRowHeight = tableView.rowHeight;
//    tableView.rowHeight = UITableViewAutomaticDimension;
//    return tableView.rowHeight;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

#pragma mark -- buttonClick
- (IBAction)phoneClick:(UIButton *)sender {
    [self showAlertPicViewWithTelNum:self.getStationInfoViewModel.infoModel.shopTel];
}

- (IBAction)navigation:(UIButton *)sender {
    if (![self judgeLogin]) {
        return ;
    }
    //导航
    if(self.getStationInfoViewModel.infoModel.shopLat.length&&self.getStationInfoViewModel.infoModel.shopLong.length) {
        [self gotoMap:self.getStationInfoViewModel.infoModel andViewController:self];
    }
}
#pragma mark - 去地图展示路线
/** 去地图展示路线 */
- (void)gotoMap:(JYGetStationInfoModel*)model andViewController:(UIViewController *)viewController{
    //当前
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
    // 后台返回的目的地坐标是百度地图的
    // 百度地图与高德地图、苹果地图采用的坐标系不一样，故高德和苹果只能用地名不能用后台返回的坐标
    CGFloat latitude  = model.shopLat.floatValue;  // 纬度
    CGFloat longitude = model.shopLong.floatValue; // 经度
    NSString *address = model.shopAddress; // 地址

    // 打开地图的优先级顺序：百度地图->高德地图->苹果地图
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        // 百度地图
        //    baidumap://map/direction?origin=中关村&destination=五道口&mode=driving&region=北京
        //        //本示例是通过该URL启动地图app并进入北京市从中关村到五道口的驾车导航路线图
        // 起点为“我的位置”，终点为后台返回的坐标
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=%f,%f&mode=driving&src=%@", latitude, longitude,model.shopTitle] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        [[UIApplication sharedApplication] openURL:url];
    }else
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
            // 高德地图
            // 起点为“我的位置”，终点为后台返回的address
            NSString *urlString = [[NSString stringWithFormat:@"iosamap://path?sourceApplication=applicationName&sid=BGVIS1&slat=%lf&slon=%lf&sname=我的位置&did=BGVIS2&dlat=%lf&dlon=%lf&dname=%@&dev=0&m=0&t=0",startLat, startLng,latitude,longitude,self.getStationInfoViewModel.infoModel.shopTitle] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

//            NSString *urlString = [[NSString stringWithFormat:@"atiiosamap://path?sourceApplication=appliconName&sid=BGVIS1&sname=%@&did=BGVIS2&dname=%@&dev=0&t=0",@"我的位置",address] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"http://maps.apple.com"]]){
            // 苹果地图
            CLLocationCoordinate2D destination = CLLocationCoordinate2DMake([model.shopLat floatValue], [model.shopLong floatValue]);
            
            MKMapItem *currentLocation =[MKMapItem mapItemForCurrentLocation];
            
            MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:destination addressDictionary:nil]];
            toLocation.name = model.shopTitle;
            
            [MKMapItem openMapsWithItems:@[currentLocation,toLocation] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
        }else{
            // 快递员没有安装上面三种地图APP，弹窗提示安装地图APP
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请安装地图APP" message:@"建议安装百度地图APP" preferredStyle:UIAlertControllerStyleAlert];
            [viewController presentViewController:alertVC animated:NO completion:nil];
        }
}

- (void)showAlertPicViewWithTelNum:(NSString *)telNum{
    if (![self judgeLogin]) {
        return ;
    }
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    
    alertVC.contentText= SF(@"呼叫%@",telNum );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [HRCall callPhoneNumber:telNum alert:NO];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark -------SDCycleScrollView代理----------

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{
    if (self.detailType == DetailTypeStore || self.detailType == DetailTypeKeepCar ) {
        self.imageCountLab.text = SF(@"%ld/%lu",index + 1,(unsigned long)self.goodsInfoViewModel.goodsModel.goodsImages.count);
    }else{
        self.imageCountLab.text = SF(@"%ld/%lu",index + 1,(unsigned long)self.getStationInfoViewModel.infoModel.shopImages.count);
    }
}

- (JYGetStationInfoViewModel *)getStationInfoViewModel
{
    if (!_getStationInfoViewModel) {
        _getStationInfoViewModel = [[JYGetStationInfoViewModel alloc] init];
    }
    return _getStationInfoViewModel;
}

- (JYGetGoodsInfoViewModel *)goodsInfoViewModel
{
    if (!_goodsInfoViewModel) {
        _goodsInfoViewModel = [[JYGetGoodsInfoViewModel alloc] init];
    }
    return _goodsInfoViewModel;
}

- (JYCollectionViewModel *)collectionViewModel
{
    if (!_collectionViewModel) {
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}

//- (ZKshareView *)shareView
//{
//    if (!_shareView) {
//        _shareView = [[ZKshareView alloc]init];
//        _shareView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
//    }
//    return _shareView;
//}

- (JYGetAttributeViewModel *)attributeViewModel{
    if(!_attributeViewModel){
        _attributeViewModel = [[JYGetAttributeViewModel alloc] init];
    }
    return _attributeViewModel;
}

- (JYChooseFormatViewController *)formatVC{
    if(!_formatVC){
        _formatVC = [[JYChooseFormatViewController alloc] init];
        _formatVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        [self.navigationController presentViewController:_formatVC animated:YES completion:nil];

    }
    return _formatVC;
}

@end
