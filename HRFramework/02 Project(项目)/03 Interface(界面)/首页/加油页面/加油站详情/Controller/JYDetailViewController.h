//
//  JYDetailViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,DetailType) {
    DetailTypeAddOil = 1, //加油详情页类型
    DetailTypeStore, //返利商城页类型
    DetailTypeKeepCar//养车店 
};

@interface JYDetailViewController : JYBaseViewController
@property (nonatomic,assign) DetailType detailType;

//加油站id/商城id/养车店id
@property (nonatomic,copy) NSString *shopId;

//商品id 或者是实体服务id
@property (nonatomic,copy) NSString *goodsId;

@end
