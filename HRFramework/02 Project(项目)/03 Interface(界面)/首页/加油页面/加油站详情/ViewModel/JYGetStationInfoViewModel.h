//
//  JYGetStationInfoViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYGetStationInfoModel.h"

@interface JYGetStationInfoViewModel : JYBaseViewModel

@property (nonatomic,strong) JYGetStationInfoModel *infoModel;
//评论列表
@property (nonatomic,strong) NSMutableArray *commentArr;

//3.6.2.	JY-006-003获取加油站详情
- (void)requestGetStationInfoWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.6.2.	JY-006-004获取评论
- (void)requestGetCommentWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore count:(NSInteger)count success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
