//
//  JYCollectionViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCollectionViewModel.h"

@implementation JYCollectionViewModel
//收藏请求
- (void)requestCollectionWithParams:(NSDictionary *)params andType:(JY_CollectionType)collectionType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{

    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSString * url;
    if (collectionType == JY_CollectionType_OilStation) {
        url = JY_PATH(JY_PERSONCENTER_CollectStation);
    }else if (collectionType == JY_CollectionType_Goods){
        url = JY_PATH(JY_PERSONCENTER_CollectGoods);
    }else if (collectionType == JY_CollectionType_Video){
        url = JY_PATH(JY_PERSONCENTER_CollectVideo);
    }else{
        url = JY_PATH(JY_PERSONCENTER_CollectSeller);
    }

    [manager POST_URL:url params:params success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
