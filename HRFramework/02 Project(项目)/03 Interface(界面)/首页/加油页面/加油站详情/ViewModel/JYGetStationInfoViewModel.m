//
//  JYGetStationInfoViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetStationInfoViewModel.h"
#import "JYCommentModel.h"

@implementation JYGetStationInfoViewModel
//3.6.2.	JY-006-003获取加油站详情
- (void)requestGetStationInfoWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_GASSTATION_GetStationInfo) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            self.infoModel = [JYGetStationInfoModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//3.6.2.	JY-006-004获取评论
- (void)requestGetCommentWithParams:(NSMutableDictionary *)params isMore:(BOOL)isMore count:(NSInteger)count success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    
    NSString * pageCount;
    if (self.commentArr.count % count == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.commentArr.count/count+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.commentArr.count/count+2 ] : @"1";
    }
    params[@"pageNum"] = pageCount;
    params[@"pageSize"] = @(count);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@--%@",JY_PATH(JY_GASSTATION_GetComment),params);
    [manager POST_URL:JY_PATH(JY_GASSTATION_GetComment) params:params success:^(id result) {
        
        NSLog(@"%@",result);
        
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [self.commentArr removeAllObjects];
            }
            NSArray * arr = [JYCommentModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            
            [weakSelf.commentArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)commentArr
{
    if (!_commentArr) {
        _commentArr = [NSMutableArray array];
    }
    return _commentArr;
}

@end
