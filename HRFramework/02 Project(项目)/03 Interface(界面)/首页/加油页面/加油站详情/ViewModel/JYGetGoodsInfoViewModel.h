//
//  JYGetGoodsInfoViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYGoodsInfoModel.h"

@interface JYGetGoodsInfoViewModel : JYBaseViewModel

@property (nonatomic,strong) JYGoodsInfoModel *goodsModel;

//3.7.1.	JY-007-004 商品详情
- (void)requestGetGoodsInfoWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
