//
//  JYGetGoodsInfoViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetGoodsInfoViewModel.h"


@implementation JYGetGoodsInfoViewModel
//3.7.1.	JY-007-004 商品详情
- (void)requestGetGoodsInfoWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_SHOP_GetGoodsInfo));
    [manager POST_URL:JY_PATH(JY_SHOP_GetGoodsInfo) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            self.goodsModel = [JYGoodsInfoModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
@end
