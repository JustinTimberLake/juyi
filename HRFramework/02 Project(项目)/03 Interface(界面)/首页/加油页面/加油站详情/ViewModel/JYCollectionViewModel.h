//
//  JYCollectionViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

typedef NS_ENUM(NSInteger,JY_CollectionType) {
    JY_CollectionType_OilStation,//油站
    JY_CollectionType_Goods,//商品
    JY_CollectionType_Seller,//商家
    JY_CollectionType_Video//商家
};

@interface JYCollectionViewModel : JYBaseViewModel
//收藏请求
- (void)requestCollectionWithParams:(NSDictionary *)params andType:(JY_CollectionType)collectionType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
