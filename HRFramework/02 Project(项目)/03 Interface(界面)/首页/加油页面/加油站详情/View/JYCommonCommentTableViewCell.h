//
//  JYCommonCommentTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>


@class JYCommentModel;
@class JYCommunityModel;

typedef NS_ENUM(NSInteger,CellMode) {
    CellMode_Comment,// 评价
    CellMode_Bbs //社区
};

//社区的和评论的类似用同一个cell
@interface JYCommonCommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewH;
@property (nonatomic,strong) JYCommentModel *commentModel;  //评论模型
@property (nonatomic,strong) JYCommunityModel *bbsModel; //社区模型
//@property (nonatomic,assign) CellMode cellMode;

//删除我的发布接口
@property (nonatomic,copy) void(^deleteBBSIdBlock)();


- (void)setUpCellMode:(CellMode)cellMode;
//计算高度
+ (float)cellHeightWithModel:(JYCommentModel *)model;

//计算高度
+ (float)cellHeightWithBBSModel:(JYCommunityModel *)model;

@end
