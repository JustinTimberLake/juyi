//
//  JYCommentImageViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommentImageViewCell.h"
#import "JYImageModel.h"

@implementation JYCommentImageViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.PicImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.PicImageView.clipsToBounds = YES;
}

- (void)setImageModel:(JYImageModel *)imageModel{
    _imageModel = imageModel;
    [self.PicImageView sd_setImageWithURL:[NSURL URLWithString:imageModel.imageUrl] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
}
@end
