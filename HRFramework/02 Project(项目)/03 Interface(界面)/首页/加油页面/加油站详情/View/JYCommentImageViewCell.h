//
//  JYCommentImageViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYImageModel;

@interface JYCommentImageViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *PicImageView;

@property (nonatomic,strong) JYImageModel *imageModel;

@end
