//
//  JYDetailWebCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYDetailWebCell : UITableViewCell
//内容链接
@property (nonatomic,copy) NSString *contentUrl;

- (void)loadContentUrl:(NSString *)url;

@property (nonatomic, copy) void (^refreshBlock) (CGFloat height);
@end
