//
//  JYDetailWebCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYDetailWebCell.h"

@interface JYDetailWebCell ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
@implementation JYDetailWebCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
}

- (void)loadContentUrl:(NSString *)url{
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGFloat webViewHeight = self.webView.scrollView.contentSize.height;
    // 更新表上控件的frame
    // _webView,时间控件,查看数,评论数...的frame,整个表头的frame,再来一句
    if (self.refreshBlock) {
        self.refreshBlock(webViewHeight);
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
