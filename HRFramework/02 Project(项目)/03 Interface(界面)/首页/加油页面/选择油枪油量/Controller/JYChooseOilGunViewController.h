//
//  SHSelectDateController.h
//  SanHeRealEstate
//
//  Created by duanhuifen on 17/2/7.
//  Copyright © 2017年 CAPF. All rights reserved.
//

#import "JYBaseViewController.h"

@class JYOilGunModel;

@interface JYChooseOilGunViewController : JYBaseViewController
//店铺id
@property (nonatomic,copy) NSString *shopId;

@property (nonatomic,copy)void(^addOilBtnActionBlock)(NSString *gunId,NSString *shopId,NSString *oilNum,JYOilGunModel *gunModel,NSString *totalPrice);

@end
