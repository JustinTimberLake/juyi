//
//  JYChooseOilGunViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYChooseOilGunViewModel.h"
#import "JYOilGunModel.h"

@implementation JYChooseOilGunViewModel

//3.6.2.	JY-006-005获取油枪列表
- (void)requestGetOilGunListWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSLog(@"%@---%@",JY_PATH(JY_GASSTATION_GetOilGunList),params);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_GASSTATION_GetOilGunList) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.oilGunArr removeAllObjects];
            NSArray * arr = [JYOilGunModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [self.oilGunArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
 
}

- (NSMutableArray<JYOilGunModel *> *)oilGunArr
{
    if (!_oilGunArr) {
        _oilGunArr = [NSMutableArray array];
    }
    return _oilGunArr;
}

@end
