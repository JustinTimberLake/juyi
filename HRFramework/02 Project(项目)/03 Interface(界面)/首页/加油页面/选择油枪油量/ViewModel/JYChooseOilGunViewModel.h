//
//  JYChooseOilGunViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@class JYOilGunModel;

@interface JYChooseOilGunViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray<JYOilGunModel *> *oilGunArr;

//3.6.2.	JY-006-005获取油枪列表
- (void)requestGetOilGunListWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
