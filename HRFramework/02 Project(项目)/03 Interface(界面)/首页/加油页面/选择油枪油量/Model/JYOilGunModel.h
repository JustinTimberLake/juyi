//
//  JYOilGunModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYOilGunModel : JYBaseModel
//枪ID
@property (nonatomic,copy) NSString *gunId;
//枪号
@property (nonatomic,copy) NSString *gunNum;

//型号
@property (nonatomic,copy) NSString *oilModel;

//单价
@property (nonatomic,copy) NSString *oilPrice;


@end
