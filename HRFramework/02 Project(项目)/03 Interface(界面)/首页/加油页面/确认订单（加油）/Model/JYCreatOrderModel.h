//
//  JYCreatOrderModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@class JYCouponModel;

@interface JYCreatOrderModel : JYBaseModel
//商城订单和加油订单共用一个

//我的油库  (仅加油订单使用)
@property (nonatomic,copy) NSString *oilDepot;
//我的积分
@property (nonatomic,copy) NSString *score;
//我的优惠券
@property (nonatomic,strong) NSArray<JYCouponModel *> *coupons;
//默认地址
@property (nonatomic,copy) NSString *addressId;
//收货人
@property (nonatomic,copy) NSString *contacter;
//收货人地址
@property (nonatomic,copy) NSString *ReceivingAddress;
//联系方式
@property (nonatomic,copy) NSString *phoneNumber;
//积分兑换。1支持，2不支持
@property (nonatomic,copy) NSString *isExchange;


//扩展属性
@property (nonatomic,assign) BOOL isExpand;
@property (nonatomic,strong) NSString *title;


@end
