//
//  JYUsableCouponModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
@class JYCouponModel;

@interface JYUsableCouponModel : JYBaseModel

@property (nonatomic,assign) BOOL isExpand;
@property (nonatomic,strong) NSString *title;

@property (nonatomic,strong) NSArray<JYCouponModel *> *couponArr;

@end
