//
//  JYCouponModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYCouponModel : JYBaseModel
//优惠券ID
@property (nonatomic,copy) NSString *couponId;
//优惠券标题
@property (nonatomic,copy) NSString *couponTitle;
//优惠券金额
@property (nonatomic,copy) NSString *couponPrice;

//扩展属性
@property (nonatomic,assign) BOOL isExpand;

@property (nonatomic,assign,getter=isSelect) BOOL select;

@end
