//
//  JYMakeSureOrderWithAddOilTypeViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@class JYGetStationInfoModel;
@class JYOilGunModel;

@interface JYMakeSureOrderWithAddOilTypeViewController : JYBaseViewController
//加油站
@property (nonatomic,copy) NSString *shopId;
//枪
@property (nonatomic,copy) NSString *gunId;
//油量
@property (nonatomic,copy) NSString *oilNum;

@property (nonatomic,strong) JYGetStationInfoModel *infoModel;

@property (nonatomic,strong) JYOilGunModel *gunModel;

@property (nonatomic,copy) NSString *totalPrice;//总额


@end
