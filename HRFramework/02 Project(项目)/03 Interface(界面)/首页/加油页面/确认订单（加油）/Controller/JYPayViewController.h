//
//  JYPayViewController.h
//  JY
//
//  Created by Justin on 2018/8/7.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYPayViewController : UIViewController
//点击确定按钮
@property (nonatomic,copy) void (^SureBtnActionBlock)( NSInteger selectIndex);

@end
