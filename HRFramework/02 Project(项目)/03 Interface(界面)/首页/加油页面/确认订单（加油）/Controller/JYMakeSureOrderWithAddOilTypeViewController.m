//
//  JYMakeSureOrderWithAddOilTypeViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMakeSureOrderWithAddOilTypeViewController.h"
#import "JYSwitchTableViewCell.h"
#import "JYCouponView.h"
#import "JYUsableCouponView.h"
#import "JYPickerViewAlertController.h"
#import "PickerViewType.h"
#import "JYAlertPicViewController.h"
#import "JYPaySuccessViewController.h"
#import "JYUsableCouponCell.h"
#import "JYOrderViewModel.h"
#import "JYOilGunModel.h"
#import "JYGetStationInfoModel.h"
#import "JYCouponModel.h"
#import "JYInvoiceViewModel.h"
#import "JYGetInvoiceModel.h"
#import "JYCreatInvoiceHeaderViewController.h"
#import "JYPayViewModel.h"
#import "JYPaySuccessViewController.h"
#import "JYRechargeViewController.h"
#import "JYGetIntegralRatioViewModel.h"
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件
#import "JYGetUserViewModel.h"

static NSString * switchCellId = @"JYSwitchTableViewCell";
static NSString * UsableCouponCellId = @"JYUsableCouponCell";



@interface JYMakeSureOrderWithAddOilTypeViewController ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic,strong) JYUsableCouponView * couponView;
//@property (strong, nonatomic) IBOutlet UIView *couponFooterView;
@property (nonatomic,assign) float couponH;
//临时高度
@property (nonatomic,assign) float tempCouponH;

@property (nonatomic, assign , getter=isShow) BOOL show;

@property (nonatomic,strong) NSArray * leftTitleArr;
//弹窗的类型 （普通/支付）
@property (nonatomic, assign ) PickerViewType pickerViewType;

//@property (nonatomic,strong) NSMutableArray * dataArr;

@property (nonatomic,strong) JYUsableCouponModel * usableCouponModel;

@property (nonatomic,strong) JYOrderViewModel *orderViewModel;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *shopTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (weak, nonatomic) IBOutlet UILabel *distanceLab;
@property (weak, nonatomic) IBOutlet UIView *starView;
@property (weak, nonatomic) IBOutlet UILabel *gunTypeLab;
@property (weak, nonatomic) IBOutlet UILabel *oilTypeLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *oilNumLab;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;

@property (nonatomic,copy) NSString *useOilDepot; //是否使用油库
@property (nonatomic,copy) NSString *useScore; //是否使用积分
@property (nonatomic,copy) NSString *useCouponId;//可用优惠券选中id

@property (nonatomic,strong) JYInvoiceViewModel *invoiceViewModel; //发票的vm
@property (nonatomic,strong) JYPickerViewAlertController * alertVC;
@property (nonatomic,strong) NSMutableArray *invoiceArr; //发票列表
@property (nonatomic,copy) NSString *invoiceId;//发票公司id
@property (nonatomic,copy) NSString *invoiceTitle;//发票公司title
@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (nonatomic,copy) NSString *changeTotalPrice;//改变的总价格
@property (nonatomic,strong) JYGetIntegralRatioViewModel *integralViewModel;
@property (nonatomic,copy) NSString *IntegralRatio; //积分兑换比率
@property (nonatomic,copy) NSString *needIntegral; //订单需要多少积分
@property (nonatomic,copy) NSString *couponPrice; //优惠券价钱




@end

@implementation JYMakeSureOrderWithAddOilTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configData];
    [self updataUI];
    [self requestCreatOrder];
    [self addNotification];
    [self requestIntegralRadio];
    
}

#pragma mark ----------------初始化UI---------------------------
- (void)configUI{
    
    self.naviTitle = @"确认订单";
    
    [self.orderTableView registerNib:[UINib nibWithNibName:@"JYSwitchTableViewCell" bundle:nil] forCellReuseIdentifier:switchCellId];
    
    [self.orderTableView registerNib:[UINib nibWithNibName:@"JYUsableCouponCell" bundle:nil] forCellReuseIdentifier:UsableCouponCellId];
    
    self.orderTableView.tableHeaderView = self.headerView;
    self.orderTableView.backgroundColor = RGB0X(0Xf5f5f5);
    
}

//初始化数据
- (void)configData{
    self.useScore = @"2";
}
#pragma mark ==================通知==================
- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
        [self showSuccessTip:noti.object];
    }
}

#pragma mark ----------------更新UI---------------------------

- (void)updataUI{
    
    self.gunTypeLab.text = [NSString stringWithFormat:@"%@油枪",self.gunModel.gunNum];
    self.oilTypeLab.text = SF(@"%@",self.gunModel.oilModel);
    self.priceLab.text = SF(@"%.2f元/升",[self.gunModel.oilPrice floatValue]);
    self.oilNumLab.text = SF(@"%.2f升",[self.oilNum floatValue]);
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:self.infoModel.shopImage] placeholderImage:JY_PLACEHOLDER_IMAGE];
    self.shopTitleLab.text = self.infoModel.shopTitle;
    self.addressLab.text = self.infoModel.shopAddress;
    self.telLab.text = self.infoModel.shopTel;
    [self configStar:self.infoModel.shopEvaluate];
    self.totalPriceLab.text = SF(@"%.2f",[self.totalPrice floatValue]);
    
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
    self.distanceLab.text = [self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[self.infoModel.shopLat floatValue] endLng:[self.infoModel.shopLong floatValue]];
    
    
}

- (void)configStar:(NSString *)evaluate{
    NSInteger num = [evaluate intValue];
    for (UIImageView * view in self.starView.subviews) {
        if ((view.tag - 1000) < num) {
            view.image = [UIImage imageNamed:@"评价黄星"];
        }else{
            view.image = [UIImage imageNamed:@"评价灰星"];
        }
    }
}
//获得两点之间的距离
- (NSString *)getTheDistanceWithStartLat:(CGFloat)startLat startLng:(CGFloat)startLng andEndLat:(CGFloat)endLat endLng:(CGFloat )endLng{
    BMKMapPoint start = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(startLat,startLng));
    BMKMapPoint end = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(endLat,endLng));
    CLLocationDistance distance = BMKMetersBetweenMapPoints(start, end);
    NSString *distanceStr;
    distanceStr = [NSString stringWithFormat:@"%.2fkm",distance/1000];
    return distanceStr;
}


//更新订单需要积分
- (void)updataNeedIntegral{
    self.needIntegral = SF(@"%.f",[self.totalPrice floatValue] * [self.IntegralRatio intValue]);
}

//更新总价
- (void)updataTotalPrice{
    if ([self.useOilDepot intValue] == 1) {
        self.changeTotalPrice = @"0.00";
        self.totalPriceLab.text = self.changeTotalPrice;
    }else{
        
        CGFloat  integralPrice = [self.orderViewModel.creatOrderModel.score floatValue] / [self.IntegralRatio floatValue];
        
        //    使用积分 并且使用优惠券
        if ([self.useScore intValue] == 1 && self.useCouponId.length) {
            //        都使用
            self.totalPriceLab.text = SF(@"%.2f",[self.totalPrice floatValue] - integralPrice - [self.couponPrice floatValue]);
            
        }else if([self.useScore intValue] == 1 && !self.useCouponId.length  ){
            //        使用积分 不使用优惠券
            if (integralPrice >=[self.totalPrice floatValue]) {
                self.totalPriceLab.text = @"0.00";
            }else{
                self.totalPriceLab.text = SF(@"%.2f",[self.totalPrice floatValue] - integralPrice);
            }
            
        }else if([self.useScore intValue] == 2 && self.useCouponId.length){
            //        不使用积分，使用优惠券
            if ([self.couponPrice floatValue] >=[self.totalPrice floatValue]) {
                self.totalPriceLab.text = @"0.00";
            }else{
                self.totalPriceLab.text = SF(@"%.2f",[self.totalPrice floatValue] - [self.couponPrice floatValue]);
            }
        }else if([self.useScore intValue] == 2 && !self.useCouponId.length){
            //        不使用积分，不使用优惠券
            self.totalPriceLab.text = SF(@"%.2f",[self.totalPrice floatValue]);
            
        }
    }
}


//跳转到添加发票抬头页面
- (void)turnCreatInvoiceHeader{
    WEAKSELF
    JYCreatInvoiceHeaderViewController * creatVC = [[JYCreatInvoiceHeaderViewController alloc] init];
    creatVC.addInvoiceHeaderSuccessBlock = ^(NSString *str) {
        self.invoiceTitle = str;
//        [weakSelf requestInvoiceList];
        [weakSelf.orderTableView reloadData];
    };
    [self.navigationController pushViewController:creatVC animated:YES];
}

#pragma mark ----------------网络请求---------------------------

- (void)requestCreatOrder{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"shopId"] = self.shopId ;
    dic[@"gunId"] = self.gunId;
    dic[@"oilNum"] = self.oilNum;
    [self.orderViewModel requestCreateOrderWithParams:dic success:^(NSString *msg, id responseData) {
        //        [weakSelf updataUI];
        if (weakSelf.orderViewModel.creatOrderModel.coupons.count) {
            weakSelf.orderViewModel.creatOrderModel.isExpand = YES;
        }
        [weakSelf.orderTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


//提交订单
- (IBAction)commitOrderBtnAction:(UIButton *)sender {
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"shopId"] = self.shopId;
    dic[@"gunId"] = self.gunId;
    dic[@"oilNum"] = self.oilNum;
    dic[@"couponId"] = self.useCouponId;
    dic[@"oilDepot"] = self.useOilDepot.length ? self.useOilDepot : @"2";
    dic[@"score"] = self.useScore.length ? self.useScore : @"2";
    dic[@"invoiceId"] = self.invoiceId;
    //    dic[@"totalPrice"] = self.totalPriceLab.text ;
    //    要求传未计算的总价 不是计算后的价格
    dic[@"totalPrice"] = self.totalPrice ;
    
    
    [self.orderViewModel requestSubmitOrderWithParams:dic success:^(NSString *msg, id responseData) {
        NSDictionary * dic = responseData;
        
        NSString * orderStr = dic[@"orderId"];
        NSString * orderType = dic[@"type"];
        if (!orderStr.length) {
            [weakSelf showSuccessTip:@"提交订单失败"];
            return ;
        }
        
        if ([orderType intValue] == 1) {
            //            走支付
//            [weakSelf showAlertWithPickerViewType:PickerViewTypePay];
            [self showAlertPayWays];
        }else{
            //            走数据库 (包含油库支付，积分支付，优惠券支付)
            [weakSelf turnPaySuccessPageWithOrderSn:orderStr];
        }
        weakSelf.returnPayTypeBlock = ^(NSString *str) {
            [weakSelf payTypeWithStr:str andOrderId:orderStr];
            NSMutableDictionary * dic = [NSMutableDictionary dictionary];
            if (orderStr.length) {
                dic[@"orderId"] = orderStr;
                dic[@"orderType"] = @"服务订单";
            }
            JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
            
        };
        weakSelf.returnNormalBlock = ^(NSInteger selectIndex) {
            if (selectIndex == 0) {
                [weakSelf turnCreatInvoiceHeader];
                NSLog(@"去添加");
            }else{
                if (weakSelf.invoiceViewModel.InvoiceListArr.count) {
                    JYGetInvoiceModel * model = weakSelf.invoiceViewModel.InvoiceListArr[selectIndex - 1];
                    weakSelf.invoiceId = model.invoiceId;
                    weakSelf.invoiceTitle = model.invoiceTitle;
                    [weakSelf.orderTableView reloadData];
                }
            }
        };
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求积分兑换比率
- (void)requestIntegralRadio{
    WEAKSELF
    [self.integralViewModel requestGetIntegralRatioSuccess:^(NSString *msg, id responseData) {
        weakSelf.IntegralRatio = responseData;
        [weakSelf updataNeedIntegral];
        [weakSelf.orderTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
//请求发票列表
- (void)requestInvoiceList{
    WEAKSELF
    [self.invoiceViewModel requestGetInvoiceListSuccess:^(NSString *msg, id responseData) {
        [weakSelf.invoiceArr removeAllObjects];
        for (JYGetInvoiceModel * model in weakSelf.invoiceViewModel.InvoiceListArr) {
            [weakSelf.invoiceArr addObject:model.invoiceTitle];
        }
        [weakSelf.invoiceArr insertObject:@"去添加+" atIndex:0];
        [weakSelf.alertVC updatePickerViewDataArr:weakSelf.invoiceArr type:PickerViewTypeNormal];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)chooseBtnAction:(UIButton *)btn{
    NSLog(@"%ld",(long)btn.tag);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * totalStr = [self.totalPriceLab.text floatValue] > 0 ? self.totalPriceLab.text :@"";
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalStr floatValue]);
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalStr,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
    
}
#pragma mark --------------UITableview的数据源和代理----------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.leftTitleArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==1) {
        return  self.orderViewModel.creatOrderModel.isExpand ? 1 : 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    JYUsableCouponCell * couponCell = [tableView dequeueReusableCellWithIdentifier:UsableCouponCellId];
    couponCell.couponId = weakSelf.useCouponId;
    [couponCell creatCouponBtnWithData:self.orderViewModel.creatOrderModel.coupons];
    couponCell.couponBtnActionBlock = ^(UIButton * btn) {
        if (btn.selected) {
            
            JYCouponModel * model = self.orderViewModel.creatOrderModel.coupons[btn.tag - 1000];
            weakSelf.useCouponId = model.couponId;
            weakSelf.couponPrice = model.couponPrice;
            
            //        提前设置好积分状态
            if ([self.couponPrice floatValue] > [self.totalPrice floatValue]) {
                self.useScore = @"2";
            }
            [weakSelf.orderTableView reloadData];
        }else{
            self.useCouponId = nil;
            self.couponPrice = nil;
        }
        
        [weakSelf updataTotalPrice];
    };
    return couponCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return  [JYUsableCouponCell cellHeightWithCount:self.orderViewModel.creatOrderModel.coupons.count];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    WEAKSELF
    JYSwitchTableViewCell * switchHeader = LOADXIB(@"JYSwitchTableViewCell");
    switchHeader.switchType = JYSwitchType_Oil;
    switchHeader.titleLab.text = self.leftTitleArr[section];
    switchHeader.switchBtn.tag = section;
//    if (section == 0) {
//        switchHeader.switchBtn.selected = [self.useOilDepot intValue] == 1? YES : NO;
//    }else
    if (section == 0){
        
        //        没有合适的优惠券
        BOOL noCoupon = NO;
        for (JYCouponModel * couponModel in self.orderViewModel.creatOrderModel.coupons) {
            if ([couponModel.couponPrice intValue] >= [self.totalPrice intValue]) {
                noCoupon = YES;
            }
        }
        switchHeader.switchBtn.selected = [self.useScore intValue] == 1 ? YES : NO;
        if ([self.useOilDepot intValue] == 1  || (noCoupon == YES && self.useCouponId.length) ) {
            switchHeader.switchBtn.enabled = NO;
            switchHeader.switchBtn.selected = NO;
            self.useScore = @"2";
        }
        else{
            switchHeader.switchBtn.enabled = YES;
        }
        
    }else if (section == 1){
        //
        if (self.orderViewModel.creatOrderModel.isExpand) {
            switchHeader.switchBtn.selected = YES;
        }else{
            switchHeader.switchBtn.selected = NO;
        }
        
        
        if ([self.useOilDepot intValue] == 1  || !self.orderViewModel.creatOrderModel.coupons.count) {
            switchHeader.switchBtn.enabled = NO;
            switchHeader.switchBtn.selected = NO;
        }else{
            switchHeader.switchBtn.enabled = YES;
        }
        
        
    }else{
        switchHeader.detailLab.text = self.invoiceTitle.length ? self.invoiceTitle : @"";
        switchHeader.switchBtn.selected = switchHeader.detailLab.text.length;
    }
    
    [switchHeader updataSwitchUIWithData:self.orderViewModel.creatOrderModel OilSwitchIsSelected:NO andNeedOilNum:self.oilNum andNeedPrice:self.totalPrice andOilType:self.gunModel.oilModel andNeedIntegral:self.needIntegral section:section];
    
    __weak typeof(switchHeader)weakSwitchHeader = switchHeader;
    switchHeader.rightSwitchBtnActionBlock = ^(UIButton * sender){
        switch (section) {
//            case 0:{
//                self.useOilDepot = sender.selected ? @"1" :@"2";
//                if (sender.selected) {
//                    weakSelf.orderViewModel.creatOrderModel.isExpand = NO;
//                }else{
//                    if (weakSelf.orderViewModel.creatOrderModel.coupons.count) {
//                        weakSelf.orderViewModel.creatOrderModel.isExpand = YES;
//                    }
//                }
//                [weakSelf updataTotalPrice];
//                [weakSelf.orderTableView reloadData];
//            }
//                break;
            case 0:{
                self.useScore = sender.selected ? @"1" :@"2";
                if (sender.selected && [self.orderViewModel.creatOrderModel.score intValue]) {
                    weakSelf.orderViewModel.creatOrderModel.isExpand = NO;
                    self.useCouponId = @"";
                }
                [weakSelf updataTotalPrice];
                [weakSelf.orderTableView reloadData];
            }
                break;
            case 1:{
                weakSelf.orderViewModel.creatOrderModel.isExpand = sender.selected ? YES : NO;
                if (sender.selected == NO) {
                    weakSelf.useCouponId = @"";
                }else{
                    self.useScore = @"2";
                }
                [weakSelf updataTotalPrice];
                [weakSelf.orderTableView reloadData];
            }
                break;
            case 2:{
                if (sender.selected) {
                    [weakSelf requestInvoiceList];
                    [weakSelf showAlertWithPickerViewType:PickerViewTypeNormal];
                    weakSelf.returnNormalBlock = ^(NSInteger selectIndex) {
                        if (selectIndex == 0) {
                            [weakSelf turnCreatInvoiceHeader];
                            NSLog(@"去添加");
                            weakSwitchHeader.switchBtn.selected = self.invoiceTitle.length;
                        }else{
                            if (weakSelf.invoiceViewModel.InvoiceListArr.count) {
                                JYGetInvoiceModel * model = weakSelf.invoiceViewModel.InvoiceListArr[selectIndex - 1];
                                weakSelf.invoiceId = model.invoiceId;
                                weakSelf.invoiceTitle = model.invoiceTitle;
                                [weakSelf.orderTableView reloadData];
                            }
                        }
                    };
                    weakSelf.dismissBlock = ^{
                        [weakSelf.orderTableView reloadData];
                    };
                }else{
                    
                }
            }
                break;
                
                
            default:
                break;
        }
    };
    return switchHeader;
}

//支付方式选择
- (void)payTypeWithStr:(NSString *)str andOrderId:(NSString *)orderId{
    if ([str isEqualToString:@"账户支付"]) {
        NSLog(@"账户支付");
        [[JYGetUserViewModel alloc]requestGetUserInfoSuccess:^(NSString *msg, id responseData) {
            if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [self.totalPriceLab.text floatValue]) {
                [self showAlertPicViewWithAccountTypeWithOrderId:orderId];
                
            }else{
                [self showAlertPicViewWithNotEnoughMoney];
            }
        } failure:^(NSString *errorMsg) {
       
        }]; 
        
    }else if([str isEqualToString:@"微信支付"]){
        NSLog(@"微信支付");
        if (![WXApi isWXAppInstalled]) {
            [self showSuccessTip:@"请安装微信客户端"];
            return;
        }else{
            [self requestWXSignWithOrderNum:orderId];
        }
    }else{
        NSLog(@"支付宝支付");
        [self requestAlipaySignWithOrderNum:orderId];
    }
}

//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
    
}

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//跳转到支付成功页面
- (void)turnPaySuccessPageWithOrderSn:(NSString *)orderSn{
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    successVC.orderId = orderSn;
    successVC.orderType = @"服务订单";
    [self.navigationController pushViewController:successVC animated:YES];
}

- (NSArray *)leftTitleArr
{
    if (!_leftTitleArr) {
        _leftTitleArr = @[@"我的积分",@"我的优惠券",@"是否需要发票"];
    }
    return _leftTitleArr;
}

- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}

- (JYOrderViewModel *)orderViewModel
{
    if (!_orderViewModel) {
        _orderViewModel = [[JYOrderViewModel alloc] init];
    }
    return _orderViewModel;
}

- (JYInvoiceViewModel *)invoiceViewModel
{
    if (!_invoiceViewModel) {
        _invoiceViewModel = [[JYInvoiceViewModel alloc] init];
    }
    return _invoiceViewModel ;
}

- (NSMutableArray *)invoiceArr
{
    if (!_invoiceArr) {
        _invoiceArr = [NSMutableArray array];
    }
    return _invoiceArr;
}

- (JYGetIntegralRatioViewModel *)integralViewModel{
    if(!_integralViewModel){
        _integralViewModel = [[JYGetIntegralRatioViewModel alloc] init];
    }
    return _integralViewModel;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
