//
//  JYPayViewController.m
//  JY
//
//  Created by Justin on 2018/8/7.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import "JYPayViewController.h"

@interface JYPayViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIImageView *firstImage;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnArr;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *selectImageArr;
@property (strong, nonatomic) UIButton *selectBtn;
@property (strong, nonatomic) UIImageView *selectImage;

@end

@implementation JYPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIView *bgView = [[UIView alloc]init];
    [self.view addSubview:bgView];
    bgView.backgroundColor = [UIColor colorWithHex:0x000000 alpha:0.6];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.offset(MC_NavHeight);
        make.left.right.bottom.top.offset(0);
    }];
    [self.view bringSubviewToFront:self.contentView];
    
    _selectBtn = _firstBtn;
    _selectImage = _firstImage;
}

- (IBAction)choseClick:(UIButton *)sender {
    if (_selectBtn == sender) {
        return;
    }
    _selectImage.hidden = YES;
    _selectBtn = sender;
    for (UIImageView *imageView in _selectImageArr) {
        if (imageView.tag == sender.tag) {
            imageView.hidden = NO;
            _selectImage = imageView;
        }
    }
    
}

//取消
- (IBAction)dismissClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//确定
- (IBAction)sureClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.SureBtnActionBlock) {
        self.SureBtnActionBlock(_selectBtn.tag);
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
