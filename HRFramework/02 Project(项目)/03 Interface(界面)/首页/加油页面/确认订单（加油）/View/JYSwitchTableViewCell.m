//
//  JYSwitchTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSwitchTableViewCell.h"
#import "JYUsableCouponModel.h"
#import "JYCreatOrderModel.h"

@implementation JYSwitchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.switchBtn.selected = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)switchBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;

//    if ((self.switchType == JYSwitchType_Shop && sender.tag == 1)|| (self.switchType == JYSwitchType_Oil && sender.tag == 2)) {
////        [self switchBtnSelectOrUselect];
//        self.orderModel.isExpand = !self.orderModel.isExpand;
//        if ( self.orderModel.isExpand) {
//            self.switchBtn.selected = YES;
//        }else{
//            self.switchBtn.selected = NO;
//        }
//    }
    
    if (_rightSwitchBtnActionBlock) {
        self.rightSwitchBtnActionBlock(sender);
    }
}


//
//- (void)setUsableCouponModel:(JYUsableCouponModel *)usableCouponModel{
//    _usableCouponModel = usableCouponModel;
//    if (usableCouponModel.isExpand) {
//        self.switchBtn.selected = YES;
//    }else{
//        self.switchBtn.selected = NO;
//    }
//}

- (void)setOrderModel:(JYCreatOrderModel *)orderModel{
    _orderModel = orderModel;
//    self.switchBtn.selected = orderModel.isExpand ? YES : NO;
//    self.switchBtn.enabled = orderModel.coupons.count ? YES : NO;


    
    if (orderModel.isExpand) {
        self.switchBtn.selected = YES;
    }else{
        self.switchBtn.selected = NO;
    }
    
//    if (orderModel.coupons.count) {
//        self.switchBtn.enabled = YES;
//    }else{
//        self.switchBtn.enabled = NO;
//    }
    
}

- (void)updataSwitchUIWithData:(JYCreatOrderModel *)orderModel OilSwitchIsSelected:(BOOL)isSelected andNeedOilNum:(NSString *)needOilNum andNeedPrice:(NSString *)needPrice andOilType:(NSString *)oilType andNeedIntegral:(NSString *)needIntegral section:(NSInteger)section{
//    if (section == 0) {
//        self.switchBtn.selected = [orderModel.score intValue] > 0 ? YES : NO;
//        self.switchBtn.enabled = [orderModel.score intValue] > 0 ? YES : NO;
//    }else{
//        self.switchBtn.selected = orderModel.isExpand ? YES : NO;
//        self.switchBtn.enabled = orderModel.coupons.count ? YES : NO;
//    }

//    if (section == 0) {
//        if (orderModel.oilDepot.length) {
//            self.detailLab.text = SF(@"%@:%@L",oilType,orderModel.oilDepot);
//            self.switchBtn.enabled = YES;
//        }else{
//            self.detailLab.text = @"";
//        }
//        self.switchBtn.enabled = [orderModel.oilDepot floatValue] > [needOilNum floatValue] ? YES : NO;
////        self.switchBtn.selected = [self.useOilDepot intValue] == 1? YES : NO;
//    }else
    if (section == 0){
        if ([orderModel.score intValue]) {
            
            self.detailLab.text = SF(@"共有%@积分,当前订单使用%@",orderModel.score,needIntegral);
        }else{
            self.detailLab.text = @"";
        }
        self.switchBtn.enabled = [orderModel.score floatValue] > 0 ? YES : NO;
//        switchHeader.switchBtn.selected = [self.useScore intValue] == 1 ? YES : NO;
    }else if (section == 1){
////
//        if (orderModel.isExpand) {
//            self.switchBtn.selected = YES;
//        }else{
//            self.switchBtn.selected = NO;
//        }
        
        
////
//        if (orderModel.coupons.count) {
//            self.detailLab.text = @"可用";
////            self.switchBtn.selected = YES;
////            self.switchBtn.enabled = YES;
//        }else{
//            self.detailLab.text = @"不可用";
////            self.switchBtn.selected = NO;
////            self.switchBtn.enabled = NO;
//        }
        
//        self.switchBtn.enabled = orderModel.coupons.count > 0 ? YES : NO;
        self.detailLab.text = orderModel.coupons.count > 0 ? @"可用":@"不可用";
    }else{
//        self.detailLab.text = @"";
    }
    
////    控制油库按钮的选择
//    if (isSelected &&(section == 1 || section == 2 )) {
//        self.switchBtn.enabled = NO;
//        self.switchBtn.selected = NO;
//    }

}

//(商城订单使用)
- (void)updataSwitchUIWithData:(JYCreatOrderModel *)orderModel section:(NSInteger)section{
    if (section == 0) {
        self.switchBtn.selected = NO;
//        self.switchBtn.selected = [orderModel.score intValue] > 0 ? YES : NO;
        self.switchBtn.enabled = ([orderModel.isExchange intValue] == 1) > 0 ? YES : NO;
    }else{
        self.switchBtn.selected = orderModel.isExpand ? YES : NO;
        self.switchBtn.enabled = orderModel.coupons.count ? YES : NO;
    }

}
@end
