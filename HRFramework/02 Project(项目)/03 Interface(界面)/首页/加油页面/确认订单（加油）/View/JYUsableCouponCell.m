//
//  JYUsableCouponCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUsableCouponCell.h"
#import "JYCouponView.h"
#import "JYUsableCouponView.h"
#import "JYCouponModel.h"


static const  int count = 2;
static const  int margin = 15;
static const  float BtnH = 15;


@interface JYUsableCouponCell ()
@property (nonatomic,assign) float couponH;
//@property (weak, nonatomic) IBOutlet UIView *tagBtnView;
@property (nonatomic,strong) UIButton *selectBtn;

@property (nonatomic,strong) NSArray *dataArr;




@end
@implementation JYUsableCouponCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = RGB(249, 248, 238);
}

//创建可用优惠券的按钮
- (void)creatCouponBtnWithData:(NSArray *)dataArr{
    WEAKSELF
    self.dataArr = dataArr;
    if (dataArr.count) {
        [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        float BtnW = SCREEN_WIDTH / 2;
        NSInteger allCount = dataArr.count;
        for (int i = 0; i < allCount; i++) {
            int row = i / count;
            int line = i % count;
            JYCouponModel * couponModel = dataArr[i];
            
            JYUsableCouponView * view = [[JYUsableCouponView alloc] initWithFrame: CGRectMake(BtnW * line, margin +(BtnH + margin) * row, BtnW, BtnH)];
            view.squareBtn.tag = 1000 + i;
            view.titleLab.text = couponModel.couponTitle;
            [view.squareBtn addTarget: self action:@selector(chooseBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:view];
            [self bringSubviewToFront:view];
        }
        JYCouponView * lastView = self.contentView.subviews.lastObject;
        self.couponH = CGRectGetMaxY(lastView.frame) + margin;
    }else{
        self.couponH = 0;
    }
    self.height = self.couponH;
    
    if (self.couponId.length) {
        [self.dataArr enumerateObjectsUsingBlock:^( JYCouponModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.couponId isEqualToString:weakSelf.couponId]) {
                JYUsableCouponView * view =  self.contentView.subviews[idx];
//                [weakSelf chooseBtnAction:view.squareBtn];
                  view.squareBtn.selected = YES;
                  [view.squareBtn setBackgroundImage:[UIImage imageNamed:@"已勾选选框"] forState:UIControlStateSelected];
            }
        }];
    }
}


- (void)chooseBtnAction:(UIButton *)btn{
//    btn.selected = !btn.selected;
//    if (btn.selected) {
//        [btn setBackgroundImage:[UIImage imageNamed:@"已勾选选框"] forState:UIControlStateSelected];
//    }else{
//        [btn setBackgroundImage:[UIImage imageNamed:@"未勾选选框"] forState:UIControlStateNormal];
//    }
//    self.selectBtn = btn;
    
    if (self.selectBtn.tag == btn.tag) {
        btn.selected = !btn.selected;
    }else{
        self.selectBtn.selected = NO;
        btn.selected = YES;
        self.selectBtn = btn;
    }
    if (btn.selected) {
        [btn setBackgroundImage:[UIImage imageNamed:@"已勾选选框"] forState:UIControlStateSelected];
    }else{
        [btn setBackgroundImage:[UIImage imageNamed:@"未勾选选框"] forState:UIControlStateNormal];
    }
    
    NSLog(@"%ld",(long)btn.tag);
    
    
    
//    self.selectBtn.selected = NO;
//    self.selectBtn = btn;
//    self.selectBtn.selected = YES;
    if (_couponBtnActionBlock) {
        self.couponBtnActionBlock(btn);
    }
}

+ (CGFloat)cellHeightWithCount:(CGFloat)totalCount{
    if (totalCount) {
        int maxRow = (totalCount + count - 1)/2;
        CGFloat h  = margin +(BtnH + margin) * maxRow;
        return h;
    }else{
        return 0;
    }
}

//根据couponId 更新UI
- (void)updataSelectCouponWithCouponId:(NSString *)couponId{
//    for (JYCouponModel * couponModel  in self.dataArr) {
//        if ([couponModel.couponId isEqualToString:couponId]) {
//
//        }
//    }
    WEAKSELF
    [self.dataArr enumerateObjectsUsingBlock:^( JYCouponModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.couponId isEqualToString:couponId]) {
           JYUsableCouponView * view =  self.contentView.subviews[idx];
            [weakSelf chooseBtnAction:view.squareBtn];
        }
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
