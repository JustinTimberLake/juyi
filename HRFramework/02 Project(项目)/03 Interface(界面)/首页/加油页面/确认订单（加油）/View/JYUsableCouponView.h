//
//  JYUsableCouponView.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYUsableCouponView : UIView
//勾选按钮
@property (nonatomic,strong) UIButton *squareBtn;
@property (nonatomic,strong)  UILabel *titleLab;

@end
