//
//  JYUsableCouponCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYUsableCouponCell : UITableViewCell

@property (nonatomic,copy) void(^couponBtnActionBlock)(UIButton *btn);

+ (CGFloat)cellHeightWithCount:(CGFloat)count;

- (void)creatCouponBtnWithData:(NSArray *)dataArr;

@property (nonatomic,copy) NSString *couponId;
////根据couponId 更新UI
//- (void)updataSelectCouponWithCouponId:(NSString *)couponId;




@end
