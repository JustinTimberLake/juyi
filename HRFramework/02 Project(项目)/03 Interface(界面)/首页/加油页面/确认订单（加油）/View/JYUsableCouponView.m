//
//  JYUsableCouponView.m
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUsableCouponView.h"
#import "JYCouponView.h"
#import "JTNButton.h"
@interface JYUsableCouponView ()

@property (nonatomic,assign) float couponH;

@end

@implementation JYUsableCouponView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        [self creatBtnItem];
    }
    return self;
}

- (void)creatBtnItem{
    JTNButton * squareBtn = [JTNButton buttonWithType:UIButtonTypeCustom];
    [squareBtn setBackgroundImage:[UIImage imageNamed:@"未勾选选框"] forState:UIControlStateNormal];
    squareBtn.frame = CGRectMake(10, 0, 17, 17);
    self.squareBtn = squareBtn;
    [self addSubview:squareBtn];
    
    UILabel * titleLab = [[UILabel alloc] init];
    self.titleLab = titleLab;
    titleLab.textColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    titleLab.textAlignment = NSTextAlignmentLeft;
    titleLab.font = FONT(13);
    titleLab.text = @"";
    titleLab.frame = CGRectMake(CGRectGetMaxX(squareBtn.frame) + 10, 0, self.width - (CGRectGetMaxX(squareBtn.frame) + 10), 17);
    [self addSubview:titleLab];
}

@end
