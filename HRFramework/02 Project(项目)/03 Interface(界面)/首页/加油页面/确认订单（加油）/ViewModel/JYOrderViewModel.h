//
//  JYOrderViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYCreatOrderModel.h"

@interface JYOrderViewModel : JYBaseViewModel

@property (nonatomic,strong) JYCreatOrderModel *creatOrderModel;

//3.6.2.	JY-006-006创建订单
- (void)requestCreateOrderWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//JY-006-007提交订单
- (void)requestSubmitOrderWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//Y-012-011 油库抵扣支付接口
- (void)requestOilDepotPayWithOrderSn:(NSString *)orderSn  success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
