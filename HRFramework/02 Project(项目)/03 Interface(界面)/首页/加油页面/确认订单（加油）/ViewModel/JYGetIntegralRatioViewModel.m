//
//  JYGetIntegralRatioViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/12/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetIntegralRatioViewModel.h"

@implementation JYGetIntegralRatioViewModel
//获取积分兑换比率
- (void)requestGetIntegralRatioSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_SHOP_GetIntegralRatio) params:nil success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
            NSString * IntegralRatio = RESULT_DATA[@"IntegralRatio"];
            successBlock(RESULT_MESSAGE,IntegralRatio);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
        
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
