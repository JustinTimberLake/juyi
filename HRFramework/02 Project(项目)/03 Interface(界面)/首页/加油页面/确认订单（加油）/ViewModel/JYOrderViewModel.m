//
//  JYOrderViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOrderViewModel.h"
#import "JYCreatOrderModel.h"

@implementation JYOrderViewModel

//3.6.2.	JY-006-006创建订单
- (void)requestCreateOrderWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_GASSTATION_CreateOrder));
    [manager POST_URL:JY_PATH(JY_GASSTATION_CreateOrder) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            self.creatOrderModel = [JYCreatOrderModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//JY-006-007提交订单
- (void)requestSubmitOrderWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@ --%@",JY_PATH(JY_GASSTATION_SubmitOrder),params);
    [manager POST_URL:JY_PATH(JY_GASSTATION_SubmitOrder) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
//            NSString * orderId = RESULT_DATA[@"orderId"];
            successBlock(RESULT_MESSAGE,RESULT_DATA);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//Y-012-011 油库抵扣支付接口
- (void)requestOilDepotPayWithOrderSn:(NSString *)orderSn  success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"]= [User_InfoShared shareUserInfo].c;
    dic[@"orderSn"] = orderSn;

    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_PAY_OilDepotPay));
    [manager POST_URL:JY_PATH(JY_PAY_OilDepotPay) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}
@end
