//
//  JYGetIntegralRatioViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYGetIntegralRatioViewModel : JYBaseViewModel

//获取积分兑换比率
- (void)requestGetIntegralRatioSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
