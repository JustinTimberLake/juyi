//
//  JYLookAllCommentViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYLookAllCommentViewController : JYBaseViewController
//1加油站2商城3养车店
@property (nonatomic,assign) NSInteger detailType;
//加油站id/商城id/养车店id
@property (nonatomic,copy) NSString *shopId;
//商品id
@property (nonatomic,copy) NSString *goodsId;

@end
