//
//  JYLookAllCommentViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYLookAllCommentViewController.h"
#import "JYCommonCommentTableViewCell.h"
#import "JYGetStationInfoViewModel.h"


//typedef NS_ENUM(NSInteger,JYCommentType) {
//    JYCommentType_All = 1,//全部
//    JYCommentType_Image,//晒图
//    JYCommentType_LowScore,//低分
//    JYCommentType_New//最新
//};
static NSString * const commentCellId = @"JYCommonCommentTableViewCell";

@interface JYLookAllCommentViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *selectBtnView;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (nonatomic,strong) UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UIView *yellowLineView;
@property (weak, nonatomic) IBOutlet UITableView *commentTableView;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (nonatomic,strong) JYGetStationInfoViewModel *getStationInfoViewModel;
 //1全部2晒图3低分4最新
@property (nonatomic,assign) NSInteger commentType;

@end

@implementation JYLookAllCommentViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setUpRefreshCommentData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark -------------------初始化UI----------------------------
- (void)configUI{
    self.naviTitle = @"全部评价";
    
    [self.commentTableView registerNib:[UINib nibWithNibName:@"JYCommonCommentTableViewCell" bundle:nil] forCellReuseIdentifier:commentCellId];
    
    self.titleView.hidden = YES;
    
    [self.selectBtnView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton * btn = (UIButton *)obj;
            if (btn.tag == 1000) {
                btn.selected = YES;
            }else{
                btn.selected = NO;
            }
        }
    }];
    
    [self.titleView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UILabel class]]){
            UILabel * lab = (UILabel *)obj;
                NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:lab.text];
                [str addAttribute:NSForegroundColorAttributeName value:RGB0X(0x333333) range:NSMakeRange(0, 2)];
                lab.attributedText = str;
            }
        
    }];
    
    
//    初始化数据
    self.commentType = 1;
}

- (void)viewWillLayoutSubviews{
    self.yellowLineView.width = (SCREEN_WIDTH - 20) / 4;
    [self topBtnAction:self.allBtn];
}

#pragma mark --------------------Action----------------------------

- (IBAction)topBtnAction:(UIButton *)sender {
    self.selectBtn.selected = NO;
    self.selectBtn = sender;
    self.selectBtn.selected = YES;
   
    NSInteger labTag = sender.tag + 1000;
    self.yellowLineView.centerX = sender.centerX;
    self.commentType = (sender.tag - 1000) + 1;
    [self requestCommentDataWithIsMore:NO];
    
//    [self.titleView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([obj isKindOfClass:[UILabel class]]){
//            if (obj.tag == labTag) {
//                UILabel * lab = (UILabel *)obj;
////                    lab.textColor = RGB0X(0xfbc638);
//                NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:lab.text];
//                [str addAttribute:NSForegroundColorAttributeName value:RGB0X(0xfbc638) range:NSMakeRange(0, lab.text.length)];
//                lab.attributedText = str;
//            }else{
//                UILabel * lab = (UILabel *)obj;
//                NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:lab.text];
//                [str addAttribute:NSForegroundColorAttributeName value:RGB0X(0x333333) range:NSMakeRange(0, 2)];
//                lab.attributedText = str;
//            }
//        }
//    }];
}

- (void)unselectLab:(UILabel *)lab{
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:lab.text];
    [str addAttribute:NSForegroundColorAttributeName value:RGB0X(0x333333) range:NSMakeRange(0, 2)];
    lab.attributedText = str;
}

- (void)selectLab:(UILabel *)lab{
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:lab.text];
    [str addAttribute:NSForegroundColorAttributeName value:RGB0X(0xfbc638) range:NSMakeRange(0, lab.text.length)];
    lab.attributedText = str;

}

#pragma mark --------------------网络请求----------------------------
//请求评论数据
- (void)setUpRefreshCommentData{
    WEAKSELF
    self.commentTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.commentTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
    [self.commentTableView.mj_header beginRefreshing];
}


- (void)endRefresh{
    [self.commentTableView.mj_header endRefreshing];
    [self.commentTableView.mj_footer endRefreshing];
}

- (void)refreshDataWithIsMore:(BOOL)isMore{
    [self requestCommentDataWithIsMore:isMore];
}

//请求加油站评论
- (void)requestCommentDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(self.detailType);
    dic[@"shopId"] = self.shopId;
    dic[@"goodsId"] = self.goodsId;
    dic[@"state"] = @(self.commentType);
    
    [self.getStationInfoViewModel requestGetCommentWithParams:dic isMore:isMore count:10 success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.commentTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark ------------------tableView数据源和代理------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.getStationInfoViewModel.commentArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYCommonCommentTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:commentCellId];
    [cell setUpCellMode:CellMode_Comment];
    cell.commentModel = self.getStationInfoViewModel.commentArr[indexPath.row];
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    tableView.estimatedRowHeight = tableView.rowHeight;
//    tableView.rowHeight = UITableViewAutomaticDimension;
//    return tableView.rowHeight;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return [JYCommonCommentTableViewCell cellHeightWithModel:self.getStationInfoViewModel.commentArr[indexPath.row]];
}

- (JYGetStationInfoViewModel *)getStationInfoViewModel
{
    if (!_getStationInfoViewModel) {
        _getStationInfoViewModel = [[JYGetStationInfoViewModel alloc] init];
    }
    return _getStationInfoViewModel;
}



@end
