//
//  JYChatViewController.h
//  JY
//
//  Created by Stronger_WM on 2017/8/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "EaseMessageViewController.h"

@interface JYChatViewController : EaseMessageViewController <EaseMessageViewControllerDelegate,EaseMessageViewControllerDataSource>

@property (nonatomic ,copy) NSString *chatTitle;        //标题

@end
