//
//  JYChatViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/8/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYChatViewController.h"
#import "UIImage+Resource.h"
#import "UIColor+Image.h"
#import "IQKeyboardManager.h"

@interface JYChatViewController ()<UIGestureRecognizerDelegate>

@end

@implementation JYChatViewController

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithConversationChatter:(NSString *)conversationChatter conversationType:(EMConversationType)conversationType
{
    self = [super initWithConversationChatter:conversationChatter conversationType:conversationType];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
    self.delegate = self;
    self.dataSource = self;
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    // 清除内存中的图片缓存
    // 1.停止当前下载
    [[SDWebImageManager sharedManager] cancelAll];
    
    // 2.清空内存缓存
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    NSLog(@"⚠️⚠️⚠️%@内存警告⚠️⚠️⚠️", NSStringFromClass([self class]));
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

- (void)configKeyBoard {
    IQKeyboardManager *manger = [IQKeyboardManager sharedManager];
    manger.enable = NO;
    //    manger.shouldResignOnTouchOutside = YES;
    //    manger.shouldToolbarUsesTextFieldTintColor = YES;
    manger.enableAutoToolbar = NO;
}



//用于初始化界面
- (void)configUI
{
    [self configKeyBoard];
    //统一色板颜色
    self.view.backgroundColor = RGB0X(0Xf5f5f5);
    self.tableView.backgroundColor = RGB0X(0Xf5f5f5);
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 9, 12, 22)];
    imgView.image = [UIImage imageNamed:@"返回箭头"];
    [view addSubview:imgView];
    
    UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backAction)];
    [view addGestureRecognizer:backTap];
    
    UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backBarItem;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationItem.title = self.chatTitle;
    
//    //聊天键盘，更多功能item设置
    self.chatBarMoreView.moreViewBackgroundColor  = [UIColor whiteColor];
    
    [self.chatBarMoreView removeItematIndex:1];
    [self.chatBarMoreView removeItematIndex:2];
    [self.chatBarMoreView removeItematIndex:2];
    [self.chatBarMoreView updateItemWithImage:[UIImage imageNamed:@"相册"] highlightedImage:nil title:@"相册" atIndex:0];
    [self.chatBarMoreView updateItemWithImage:[UIImage imageNamed:@"拍照"] highlightedImage:nil title:@"拍照" atIndex:1];
    
    //设置toolbar输入框placeholder
    EaseChatToolbar *tempBar = (EaseChatToolbar *)self.chatToolbar;
    tempBar.inputTextView.placeHolder = @"";
    
    //设置toolbar输入框的背景
    tempBar.inputTextView.backgroundColor = [UIColor colorWithHexString:@"F0F0F0"];
    
    //设置toolbar背景
    [self removeAllUIImageViewFromSuperView:tempBar];
    tempBar.backgroundColor = [UIColor whiteColor];
    
    //设置录音ui
    for (UIButton *btn in tempBar.subviews) {
        if ([btn isMemberOfClass:[UIView class]]) {
            for (UIButton *recordBtn in btn.subviews) {
                if ([recordBtn.accessibilityIdentifier isEqualToString:@"record"]) {
                    [recordBtn setTitle:@"按住   说话" forState:UIControlStateNormal];
                    [recordBtn setTitle:@"松开 发送语音" forState:UIControlStateHighlighted];
                }
            }
        }
    }
    
    //左边风格按钮
    EaseChatToolbarItem *styleItem = tempBar.inputViewLeftItems[0];
    [styleItem.button setImage:[UIImage imageNamed:@"chat_voice"] forState:UIControlStateNormal];
    [styleItem.button setImage:[UIImage imageNamed:@"chat_keyboard"] forState:UIControlStateSelected];
    
    //emoji
    EaseChatToolbarItem *faceItem = tempBar.inputViewRightItems[1];
    [faceItem.button setImage:[UIImage imageNamed:@"chat_face"] forState:UIControlStateNormal];
    [faceItem.button setImage:[UIImage imageNamed:@"chat_keyboard"] forState:UIControlStateSelected];
    
    //more
    EaseChatToolbarItem *moreItem = tempBar.inputViewRightItems[0];
    [moreItem.button setImage:[UIImage imageNamed:@"chat_more"] forState:UIControlStateNormal];
    [moreItem.button setImage:[UIImage imageNamed:@"chat_keyboard"] forState:UIControlStateSelected];
    
    //设置聊天气泡
    //[UIImage imageNamed:@"回复框"]
    UIEdgeInsets padding = UIEdgeInsetsMake(10, 10, 10, 10);
    UIImage *sendImg = [[UIImage imageNamed:@"bubble_right"] resizableImageWithCapInsets:padding resizingMode:UIImageResizingModeStretch];
    [[EaseBaseMessageCell appearance] setSendBubbleBackgroundImage:sendImg];
    
    UIImage *recvImg = [[UIImage imageNamed:@"bubble_left"] resizableImageWithCapInsets:padding resizingMode:UIImageResizingModeStretch];
    [[EaseBaseMessageCell appearance] setRecvBubbleBackgroundImage:recvImg];
    
    
}

//剔除一个view中所有的uiimageView
- (void)removeAllUIImageViewFromSuperView:(UIView *)view
{
    [view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isMemberOfClass:[UIImageView class]]) {
            [obj removeFromSuperview];
        }
    }];
}


#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

- (void)backAction
{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
//    self.showRefreshHeader = YES;
//    //首次进入加载数据
//    [self tableViewDidTriggerHeaderRefresh];

}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

//
////具体样例：
//- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController
//                           modelForMessage:(EMMessage *)message
//{
//    //用户可以根据自己的用户体系，根据message设置用户昵称和头像
//    id<IMessageModel> model = nil;
//    model = [[EaseMessageModel alloc] initWithMessage:message];
//    if (model.isSender) {
////        NSUserDefaults * def =[NSUserDefaults standardUserDefaults];
////        model.avatarImage = DEFAULT_HEADER_IMG;//默认头像
//        model.avatarURLPath = [JY_USERDEFAULTS objectForKey:USER_HEADEIMAGE];//头像网络地址
//        model.nickname = [JY_USERDEFAULTS objectForKey:USER_NICKNAME];//用户昵称
//
//    }else{
//        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];//默认头像
////        model.avatarURLPath = @"";//头像网络地址
//        model.nickname = @"昵称";//用户昵称
//
//    }
//    return model;
//}
//
/// 重写EaseMessageViewController.h中的方法.
- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController modelForMessage:(EMMessage *)message
{
    
    if (message.direction == EMMessageDirectionSend){ /// 用户发送
        //用户可以根据自己的用户体系，根据message设置用户昵称和头像
        id<IMessageModel> model = nil;
        model = [[EaseMessageModel alloc] initWithMessage:message];
        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];//默认头像
//        NSString *url = [NSString ddl_webImageRequestWithUrl:self.userInformation.url];
        if ([User_InfoShared shareUserInfo].personalModel.userHead.length) {
          model.avatarURLPath = [User_InfoShared shareUserInfo].personalModel.userHead;//头像网络地址
        }
        model.nickname = [User_InfoShared shareUserInfo].personalModel.userNick;//用户昵称
        return model;
    } else {
        //用户可以根据自己的用户体系，根据message设置用户昵称和头像
        id<IMessageModel> model = nil;
        model = [[EaseMessageModel alloc] initWithMessage:message];
        model.avatarImage = [UIImage imageNamed:@"logo"];//默认头像
//        NSString *url = [NSString ddl_webImageRequestWithUrl:self.friend_url];
//        model.avatarURLPath = url;//头像网络地址
        model.nickname = @"客服";//用户昵称
        return model;
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
