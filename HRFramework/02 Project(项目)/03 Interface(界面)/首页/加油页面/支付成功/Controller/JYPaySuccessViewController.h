//
//  JYPaySuccessViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYPaySuccessViewController : JYBaseViewController
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *orderType;
@end
