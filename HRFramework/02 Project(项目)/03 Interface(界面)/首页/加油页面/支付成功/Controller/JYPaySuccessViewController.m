//
//  JYPaySuccessViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPaySuccessViewController.h"
#import "JYBaseTabBarController.h"
#import "JYServiceOrderDetailViewController.h"
#import "JYProductOrderViewController.h"
#import "JYPaySuccessViewModel.h"
#import "SDCycleScrollView.h"
#import "JYHomeViewModel.h"
#import "JYHomeBannerModel.h"
#import "JYDetailViewController.h"
#import "JYCommonWebViewController.h"
#import "JYVideoListViewController.h"

#import "JYMyOrderMianViewController.h"
#import "JYMyRedPickViewModel.h"


@interface JYPaySuccessViewController ()<SDCycleScrollViewDelegate>
@property (nonatomic,strong) JYPaySuccessViewModel *paySuccessViewModel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLab;
@property (weak, nonatomic) IBOutlet UIImageView *couponImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *tipLab;
@property (nonatomic,strong) SDCycleScrollView * scrollView;
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (nonatomic,strong) JYHomeViewModel *homeViewModel;
@property (weak, nonatomic) IBOutlet UIView *couponView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponViewH;
@property (nonatomic,strong) JYMyRedPickViewModel *couponViewModel;
@end

@implementation JYPaySuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self creatBannerView];
    [self requestGetDisCountAndScoreWithOrderId:self.orderId];
//    [self requestADBanner];
    
}

- (void)configUI{
    self.naviTitle = @"支付成功";
//    [self hideBackNavItem];
    self.couponView.hidden = YES;
    self.couponViewH.constant = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)creatBannerView{
    //轮播图也写到layoutSubview里了
    self.scrollView = [SDCycleScrollView cycleScrollViewWithFrame:(CGRectMake(0, 0, SCREEN_WIDTH, self.bannerView.height)) delegate:self  placeholderImage:[UIImage imageNamed:@"defaultImg"]];
    self.scrollView.delegate = self; // 如需监听图片点击，请设置代理，实现代理方法
    self.scrollView.autoScrollTimeInterval = 3;// 自定义轮播时间间隔
    self.scrollView.titleLabelBackgroundColor = [UIColor clearColor];
    self.scrollView.showPageControl = YES;
//    self.scrollView.localizationImageNamesGroup = self.imageArr;
    self.scrollView.currentPageDotImage = [UIImage imageNamed:@"轮播-选中"];
    self.scrollView.pageDotImage = [UIImage imageNamed:@"轮播-未选中"];
    [self.bannerView addSubview:self.scrollView];
    
    //    self.scrollView.clickItemOperationBlock = ^(NSInteger currentIndex) {
    //        HR_ZoomViewVC *imgBorwserVC = [[HR_ZoomViewVC alloc] init];
    //        imgBorwserVC.idx = currentIndex+1;
    //        imgBorwserVC.dataSourceArrForNamesGroup = weakSelf.imageArr;
    //        imgBorwserVC.contentText = @"你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所你父母能打福克斯你少时诵诗书所所所所所所所所所所所wo";
    //        imgBorwserVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    //        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:imgBorwserVC animated:YES completion:nil];
    //    };
    
}

#pragma mark ==================网络请求==================
////请求首页banner
//- (void)requestADBanner{
//    WEAKSELF
//
//    [self.homeViewModel requestGetADPositionWithType:@"3" success:^(NSString *msg, id responseData) {
//        [weakSelf updataBanner];
//    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
//    }];
//}


//优惠券领取请求
- (void)requestReceiveCouponWithCouponId:(NSString *)couponId{
    WEAKSELF
    [self.couponViewModel requestReceiveCouponWithCouponId:couponId success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf requestGetDisCountAndScoreWithOrderId:self.orderId];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//获取支付成功后积分和优惠券接口
- (void)requestGetDisCountAndScoreWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.paySuccessViewModel requestPaySuccessGetDiscountWithOrderId:orderId success:^(NSString *msg, id responseData) {
        [weakSelf updataUI];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
#pragma mark ==================私有方法==================
- (void)updataUI{
    self.scoreLab.text = self.paySuccessViewModel.successModel.Score.length ? SF(@"恭喜您获得%@积分",self.paySuccessViewModel.successModel.Score):@"";
    
    if (self.paySuccessViewModel.successModel.couponId.length) {
        self.couponView.hidden = NO;
        self.couponViewH.constant = 140;
        self.titleLab.text = [self.paySuccessViewModel.successModel.couponTitle placeholder:@"暂无"];
        self.priceLab.text = self.paySuccessViewModel.successModel.couponPrice;
        [self.couponImageView sd_setImageWithURL:[NSURL URLWithString:self.paySuccessViewModel.successModel.couponImage] placeholderImage:DEFAULT_COURSE];
        //    self.startTimeLB.text = SF(@"%@-%@",model.couponStartTime,model.couponEndTime);
        self.timeLab.text = SF(@"%@-%@",[NSString YYYYMMDDWithDataStr:self.paySuccessViewModel.successModel.couponStartTime],[NSString YYYYMMDDWithDataStr:self.paySuccessViewModel.successModel.couponEndTime]);
        switch ([self.paySuccessViewModel.successModel.couponState intValue]) {
            case 1:
                self.tipLab.text = @"待\n使\n用";
                break;
            case 2:
                self.tipLab.text = @"已\n使\n用";
                break;
            case 3:
                self.tipLab.text = @"已\n过\n期";
                break;
            case 4:
                self.tipLab.text = @"立\n即\n领\n取";
                break;
            case 5:
                self.tipLab.text = @"已\n失\n效";
                break;

            default:
                break;
        }
        
        if ([self.paySuccessViewModel.successModel.couponState intValue] == 1 ||[self.paySuccessViewModel.successModel.couponState intValue] == 4||[self.paySuccessViewModel.successModel.couponState intValue] == 5) {
            self.tipLab.textColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
        }else{
            self.tipLab.textColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
        }
    }else{
        self.couponView.hidden = YES;
        self.couponViewH.constant = 0;
    }

}


- (void)updataBanner{
    self.bannerView.hidden = self.homeViewModel.ADBannerArr.count >0 ? NO : YES;
    if (!self.homeViewModel.ADBannerArr.count ) {
        return;
    }
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYHomeBannerModel * model in self.homeViewModel.ADBannerArr) {
        [imageArr addObject:model.bannerImage];
    }
    self.scrollView.imageURLStringsGroup = imageArr;
}



#pragma mark -------------action -------------
- (IBAction)lookOrderBtnAction:(UIButton *)sender {
//    if ([self.orderType isEqualToString:@"服务订单"]) {
//
//        JYServiceOrderDetailViewController * serviceDetailVC = [[JYServiceOrderDetailViewController alloc] init];
//        serviceDetailVC.orderId = self.orderId;
//        serviceDetailVC.checkMode = YES;
//        serviceDetailVC.detailType = JYDetailType_Service;
//        [self.navigationController pushViewController:serviceDetailVC animated:YES];
//    }else if ([self.orderType isEqualToString:@"实体订单"]){
//        JYServiceOrderDetailViewController * serviceDetailVC = [[JYServiceOrderDetailViewController alloc] init];
//        serviceDetailVC.orderId = self.orderId;
//        serviceDetailVC.checkMode = YES;
//        serviceDetailVC.detailType = JYDetailType_ShiTi;
//        [self.navigationController pushViewController:serviceDetailVC animated:YES];
//    } else{
//
//        JYProductOrderViewController * productVC = [[JYProductOrderViewController alloc] init];
//        productVC.orderId = self.orderId;
//        productVC.checkMode = YES;
//        [self.navigationController pushViewController:productVC animated:YES];
//    }
    
    JYMyOrderMianViewController *myOrderVC = [[JYMyOrderMianViewController alloc] init];
    myOrderVC.checkMode = YES;
    [self.navigationController pushViewController:myOrderVC animated:YES];
    
}
- (IBAction)backHomeBtnAction:(UIButton *)sender {
  JYBaseTabBarController * tabBarVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    tabBarVC.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)couponViewBtnAction:(UIButton *)sender {
    [self requestReceiveCouponWithCouponId:self.paySuccessViewModel.successModel.couponId];
}


#pragma mark ------------SDCycleScrollView 代理--------------

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    JYHomeBannerModel * model = self.homeViewModel.ADBannerArr[index];
    
    //(1活动详情，2帖子详情，3油站列表，4店铺列表，5油站详情，6店铺详情，7商品列表，8商品详情，9 url)
    
    //   最新的： (1商品 2是油站3 是视频4是资讯5URL)
    switch ([model.bannerType intValue]) {
        case 1:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            detailVC.goodsId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
            
        case 2:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            detailVC.shopId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 3:{
            JYVideoListViewController * videoListVC = [[JYVideoListViewController alloc] init];
            [self.navigationController pushViewController:videoListVC animated:YES];
        }
            break;
        case 4:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            webVC.navTitle = @"资讯详情";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 5:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
            
            
        default:
            break;
    }
}



- (JYPaySuccessViewModel *)paySuccessViewModel
{
    if (!_paySuccessViewModel) {
        _paySuccessViewModel = [[JYPaySuccessViewModel alloc] init];
    }
    return _paySuccessViewModel;
}

- (JYHomeViewModel *)homeViewModel{
    if(!_homeViewModel){
        _homeViewModel = [[JYHomeViewModel alloc] init];
    }
    return _homeViewModel;
}


- (JYMyRedPickViewModel *)couponViewModel
{
    if (!_couponViewModel) {
        _couponViewModel = [[JYMyRedPickViewModel alloc] init];
    }
    return _couponViewModel;
}


@end
