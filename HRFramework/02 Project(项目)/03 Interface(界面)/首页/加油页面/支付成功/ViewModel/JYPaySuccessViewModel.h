//
//  JYPaySuccessViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYPaySuccessModel.h"

@interface JYPaySuccessViewModel : JYBaseViewModel

@property (nonatomic,strong) JYPaySuccessModel *successModel;
//Y-012-012 支付成功后获取优惠券和积分接口
- (void)requestPaySuccessGetDiscountWithOrderId:(NSString *)orderId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
