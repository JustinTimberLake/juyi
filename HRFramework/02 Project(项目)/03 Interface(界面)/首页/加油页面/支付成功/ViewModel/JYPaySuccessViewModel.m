//
//  JYPaySuccessViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPaySuccessViewModel.h"

@implementation JYPaySuccessViewModel
//Y-012-012 支付成功后获取优惠券和积分接口
- (void)requestPaySuccessGetDiscountWithOrderId:(NSString *)orderId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderId"] = orderId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@---%@",JY_PATH(JY_PAY_GetDiscount),dic);
    [manager POST_URL:JY_PATH(JY_PAY_GetDiscount) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
            weakSelf.successModel = [JYPaySuccessModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
    
}
@end
