//
//  JYPaySuccessModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYPaySuccessModel : JYBaseModel
//获得的积分
@property (nonatomic,copy) NSString *Score;
// 优惠券ID
@property (nonatomic,copy) NSString *couponId;

//标题
@property (nonatomic,copy) NSString *couponTitle;

//副标题
@property (nonatomic,copy) NSString *couponSubheading;

//金额
@property (nonatomic,copy) NSString *couponPrice;

//图片
@property (nonatomic,copy) NSString *couponImage;

// 1待使用2已使用3已过期4领取 5领取
@property (nonatomic,copy) NSString *couponState;

//开始时间
@property (nonatomic,copy) NSString *couponStartTime;

//结束时间
@property (nonatomic,copy) NSString *couponEndTime;


@end
