//
//  JYSearchViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSearchViewModel.h"
//#import "JYSearchModel.h"
#import "JYMyProductModel.h"

@implementation JYSearchViewModel

- (void)requestWithParams:(NSMutableDictionary *)params AndSearchType:(JYSearchType)searchType andIsMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF;
    params[@"type"] = @(searchType);
    
    NSString * pageCount;
    if (self.listArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+2 ] : @"1";
    }
    params[@"pageNum"] = pageCount;
    params[@"pageSize"] = @(10);
    NSLog(@"%@ ----%@",JY_PATH(JY_HOME_Search),params);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_HOME_Search) params:params success:^(id result) {
        
        NSLog(@"%@",result);
        
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.listArr removeAllObjects];
            }
            weakSelf.searchModel = [JYSearchModel mj_objectWithKeyValues:RESULT_DATA];
            
            switch (searchType) {
                case JYSearchType_All:{
                }
                    break;
                case JYSearchType_OilStation:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchStations];
                }
                    break;
                case JYSearchType_Store:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchKeepcars];
                }
                    break;
                case JYSearchType_Product:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchGoods];
                }
                    break;
                case JYSearchType_Activity:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchActs];
                }
                    break;
                case JYSearchType_Community:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchBbs];
                }
                    break;
                case JYSearchType_Cooperation:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchUnions];
                }
                    break;
                case JYSearchType_Video:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchVideos];
                }
                    break;
                case JYSearchType_Consult:{
                    [weakSelf.listArr addObjectsFromArray:weakSelf.searchModel.searchNews];
                }
                    break;
            }
            successBlock(RESULT_MESSAGE,RESULT_MESSAGE);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


//店铺内商品
- (void)requestShopSearchWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    NSLog(@"%@---%@",JY_PATH(JY_SHOP_ShopSearch),params);
    [manager POST_URL:JY_PATH(JY_SHOP_ShopSearch) params:params success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
            [weakSelf.shopProductArr removeAllObjects];
            NSArray * arr = [JYMyProductModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.shopProductArr addObjectsFromArray:arr];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//热门搜索接口
- (void)requestHotSearchSuccess:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_HOME_HotSearch) params:nil success:^(id result) {
        if (RESULT_SUCCESS) {
            [weakSelf.hotSearchArr removeAllObjects];
            NSArray * arr = [JYHotSearchModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.hotSearchArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}

- (NSMutableArray *)shopProductArr{
    if(!_shopProductArr){
        _shopProductArr = [NSMutableArray array];
    }
    return _shopProductArr;
}

- (NSMutableArray<JYHotSearchModel *> *)hotSearchArr{
    if(!_hotSearchArr){
        _hotSearchArr = [NSMutableArray array];
    }
    return _hotSearchArr;
}


@end
