//
//  JYSearchViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

#import "JYSearchModel.h" //用@class 点不出来
#import "JYHotSearchModel.h"

//1.全部2.加油站3.店铺4.商品5.活动6.帖子7.合作联盟8.视频9.资讯
typedef NS_ENUM(NSInteger,JYSearchType) {
    JYSearchType_All = 1,//全部
    JYSearchType_OilStation ,//加油站
    JYSearchType_Store ,//店铺
    JYSearchType_Product ,//商品
    JYSearchType_Activity ,//活动
    JYSearchType_Community ,//帖子
    JYSearchType_Cooperation ,//合作联盟
    JYSearchType_Video ,//视频
    JYSearchType_Consult ,//资讯
};

@interface JYSearchViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;

@property (nonatomic,strong) JYSearchModel *searchModel;

@property (nonatomic,strong) NSMutableArray *shopProductArr;

@property (nonatomic,strong) NSMutableArray<JYHotSearchModel *> *hotSearchArr;

//JY-003-001 搜索
- (void)requestWithParams:(NSMutableDictionary *)params AndSearchType:(JYSearchType)searchType andIsMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//店铺内商品
- (void)requestShopSearchWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//热门搜索接口
- (void)requestHotSearchSuccess:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock;

@end
