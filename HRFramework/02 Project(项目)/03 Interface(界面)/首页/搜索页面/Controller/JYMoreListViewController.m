//
//  JYMoreListViewController.m
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMoreListViewController.h"
#import "JYStoreListTableViewCell.h"
#import "JYStoreTableViewCell.h"
#import "JYSearchResultTitleView.h"
#import "JYActivityTableViewCell.h"
#import "JYCommunityTableViewCell.h"
#import "JYCommunityModel.h"
#import "JYMoreListViewController.h"
#import "JYProductCell.h"
#import "JYDetailViewController.h"
#import "JYCooperationTableViewCell.h"
#import "JYMyFavoritesMvTableViewCell.h"
#import "JYCooperationModel.h"
#import "JYConsultTableViewCell.h"
#import "JYSearchViewModel.h"
#import "JYShopListModel.h"
#import "JYActivityModel.h"
#import "JYCommonWebViewController.h"
#import "JYMomentDetailViewController.h"
#import "JYHeZuoDetailViewController.h"
#import "CLTableViewCell.h"
#import "JYCollectionViewModel.h"
#import "CLPlayerView.h"
#import "JYStoreViewController.h"
#import "JYConsultModel.h"

static NSString *const ProductCellId = @"JYProductCell";
static NSString *const StoreListCellId = @"JYStoreListTableViewCell";
static NSString *const StoreCellId = @"JYStoreTableViewCell";
static NSString *const ActivityCellId = @"JYActivityTableViewCell";
static NSString *const CommunityCellId = @"JYCommunityTableViewCell";
static NSString *const ConsultCellId = @"JYConsultTableViewCell";
static NSString *const CooperationCellId = @"JYCooperationTableViewCell";
static NSString *const MyFavoritesMvCellId = @"JYMyFavoritesMvTableViewCell";
static NSString *const CLTableViewCellIdentifier = @"CLTableViewCellIdentifier";



@interface JYMoreListViewController ()<
    UITableViewDelegate,
    UITableViewDataSource,
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout,
    CLTableViewCellDelegate
>


@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) NSArray * dataArr;
@property (nonatomic,strong) NSArray * cooperationArr;
@property (nonatomic,strong) JYSearchViewModel *viewModel;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;
/**CLplayer*/
@property (nonatomic, weak) CLPlayerView *playerView;
/**记录Cell*/
@property (nonatomic, assign) UITableViewCell *cell;
@end

@implementation JYMoreListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setUpSearchRefreshData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)configUI{
    
//    JYCommunityModel *m1 = [[JYCommunityModel alloc] init];
//    self.dataArr = @[
//                     m1,m1,m1,m1,m1,m1,m1,m1,m1,m1,m1,m1,m1,m1,m1,m1
//                     ];
//    
//    JYCooperationModel * model = [[JYCooperationModel alloc] init];
//    self.cooperationArr = @[model,model];
    

    
    [self configTableView];
    
    if (self.moreType == MoreTypeProduct) {
        self.myCollectionView.hidden = NO;
    }else{
        self.myCollectionView.hidden = YES;
    }
    
}

- (void)configTableView{
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYProductCell" bundle:nil] forCellWithReuseIdentifier:ProductCellId];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYStoreListTableViewCell" bundle:nil] forCellReuseIdentifier:StoreListCellId];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYStoreTableViewCell" bundle:nil] forCellReuseIdentifier:StoreCellId];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYActivityTableViewCell" bundle:nil] forCellReuseIdentifier:ActivityCellId];
    [self.myTableView registerClass:[JYCommunityTableViewCell class] forCellReuseIdentifier:CommunityCellId];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYConsultTableViewCell" bundle:nil] forCellReuseIdentifier:ConsultCellId];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYCooperationTableViewCell" bundle:nil] forCellReuseIdentifier:CooperationCellId];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYMyFavoritesMvTableViewCell" bundle:nil] forCellReuseIdentifier:MyFavoritesMvCellId];
    [self.myTableView registerClass:[CLTableViewCell class] forCellReuseIdentifier:CLTableViewCellIdentifier];


}
#pragma mark ==================自定义方法==================
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

- (void)reloadDataWithIndexPath:(NSIndexPath *)indexPath{
    //    JYMyLoveMvModel * model = self.findViewModel.videoListArr[indexPath.row];
    CLModel *model = self.viewModel.searchModel.searchVideos[indexPath.row];;
    
    if ([model.isCollect intValue] == 1) {
        model.isCollect = @"2";
    }else{
        model.isCollect = @"1";
    }
    [self.myTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark ----------------网络请求----------------

- (void)setUpSearchRefreshData{
    WEAKSELF
    if (self.moreType == MoreTypeProduct) {
        self.myCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf requestSearchResultWithIsMore:NO];
        }];
        self.myCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf requestSearchResultWithIsMore:YES];
        }];
        [self.myCollectionView.mj_header beginRefreshing];
    }else{
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf requestSearchResultWithIsMore:NO];
        }];
        self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf requestSearchResultWithIsMore:YES];
        }];
        [self.myTableView.mj_header beginRefreshing];

    }
}

//结束刷新
- (void)endRefresh{
    if (self.moreType == MoreTypeProduct) {
        [self.myCollectionView.mj_header endRefreshing];
        [self.myCollectionView.mj_footer endRefreshing];
    }else{
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    }
    
}

- (void)requestSearchResultWithIsMore:(BOOL)isMore{
    WEAKSELF
//    if (self.searchType == SearchTypeAll) {
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"key"] = self.keyWord;
    dic[@"cityCode"] = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYCODE];
    [self.viewModel requestWithParams:dic AndSearchType:(self.moreType + 2) andIsMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
        if (self.moreType == MoreTypeProduct) {
            [weakSelf.myCollectionView reloadData];
        }
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//收藏
- (void)requestCollectVideoWithVideoId:(NSString *)videoId andOperat:(NSString *)operat andIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"videoId"] = videoId;
    dic[@"operat"] = operat;
    [self.collectionViewModel requestCollectionWithParams:dic andType:JY_CollectionType_Video success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf reloadDataWithIndexPath:indexPath];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark ----------------tableview 的数据源和代理---------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.moreType == MoreTypeProduct) {
        return 0;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.moreType == MoreTypeProduct) {
        return 0;
    }
    return self.viewModel.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    switch (self.moreType) {
        case MoreTypeAddOilStation:{
          JYStoreListTableViewCell * oilStationCell = [tableView dequeueReusableCellWithIdentifier:StoreListCellId];
            oilStationCell.gasStation = self.moreType == MoreTypeAddOilStation?YES:NO;
            JYShopListModel *listModel = self.viewModel.listArr[indexPath.row];
            oilStationCell.listModel = listModel;
            WEAKSELF;
            oilStationCell.navBlock = ^(void){
                if (![weakSelf judgeLogin]) {
                    return ;
                }
                    //导航
                    [JYStoreListTableViewCell gotoMap:listModel andViewController:weakSelf];
            };

            return oilStationCell;
        }
            break;
        case MoreTypeStore:{
            JYStoreTableViewCell * storeCell = [tableView dequeueReusableCellWithIdentifier:StoreCellId];
            storeCell.model = self.viewModel.listArr[indexPath.row];
            return storeCell;
        }
            break;
        case MoreTypeActivity:{
            JYActivityTableViewCell * activityCell = [tableView dequeueReusableCellWithIdentifier:ActivityCellId];
            activityCell.model = self.viewModel.listArr[indexPath.row];
            return activityCell;
        }
            break;
        case MoreTypeCommunity:{
            JYCommunityTableViewCell * CommunityCell = [tableView dequeueReusableCellWithIdentifier:CommunityCellId];
             JYCommunityModel *model = self.viewModel.listArr[indexPath.section];
            [CommunityCell updateCellModel:model];
            return CommunityCell;
        }
            break;
        case MoreTypeCooperation:{
            JYCooperationTableViewCell * cooperationCell = [tableView dequeueReusableCellWithIdentifier:CooperationCellId];
            cooperationCell.model = self.viewModel.listArr[indexPath.row];
            return cooperationCell;
        }
            break;
        case MoreTypeVideo:{
//            JYMyFavoritesMvTableViewCell * videoCell = [tableView dequeueReusableCellWithIdentifier:MyFavoritesMvCellId];
//            videoCell.model = self.viewModel.listArr[indexPath.row];
//            return videoCell;
            CLTableViewCell *videoCell = [tableView dequeueReusableCellWithIdentifier:CLTableViewCellIdentifier];
            videoCell.indexPath = indexPath;
            CLModel *model = self.viewModel.listArr[indexPath.row];
            videoCell.model = model;
            videoCell.collectionBtnClickBlock = ^(NSString *isCollect){
                if (![self judgeLogin]) {
                    return ;
                }
                [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
            };
            videoCell.delegate = self;
            return videoCell;

        }
            break;
        case MoreTypeConsult:{
            JYConsultTableViewCell * consultCell = [tableView dequeueReusableCellWithIdentifier:ConsultCellId];
            consultCell.model = self.viewModel.listArr[indexPath.row];
            return consultCell;
        }
            break;
        case MoreTypeProduct:{
            return nil;
        }
            break;

//        default:{
//            JYStoreListTableViewCell * oilStationCell = [tableView dequeueReusableCellWithIdentifier:StoreListCellId];
//            oilStationCell.listModel = self.viewModel.listArr[indexPath.row];
//            return oilStationCell;
//        }
            
            break;
    }
        
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (self.moreType) {
        case MoreTypeAddOilStation: case MoreTypeStore: case MoreTypeConsult:{
            return 115;
        }
            break;
        case MoreTypeActivity:{
            return 230;
        }
            break;
        case MoreTypeCommunity:{
            JYCommunityModel *model = self.viewModel.listArr[indexPath.section];
            CGFloat rowHeight = [JYCommunityTableViewCell cellHeightAccordingModel:model];
            return rowHeight;
        }
            break;
        case MoreTypeCooperation:{
            JYCooperationModel * model = self.viewModel.listArr[indexPath.row];
            CGFloat rowHeight = [JYCooperationTableViewCell cellHeightWithModel:model];
            return rowHeight;
        }
             break;
        case MoreTypeVideo:{
            return 200;
        }
            break;
        case MoreTypeProduct:{
            return 0;
        }
            break;

        default:{
            return 200;
        }
            
            break;
    }
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    WEAKSELF
//    if (tableView == self.historyTableView) {
//        JYSearchTitleView * titleView = LOADXIB(@"JYSearchTitleView");
//        return titleView;
//    }else if(tableView == self.resultTableView && self.searchType == SearchTypeAll){
//        JYSearchResultTitleView * resultSearchTitleView = LOADXIB(@"JYSearchResultTitleView");
//        resultSearchTitleView.titleLab.text = self.searchResultTitleArr[section];
//        resultSearchTitleView.moreBtnActionBlock = ^{
//            [weakSelf turnMoreVCWithIndex:section];
//        };
//        return resultSearchTitleView;
//    }else{
//        return nil;
//    }
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (tableView == self.historyTableView) {
//        return 45;
//    }else if (tableView == self.resultTableView && self.searchType == SearchTypeAll){
//        return 55;
//        
//    }else{
//        return 0;
//    }
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//    if (self.moreType == MoreTypeAddOilStation) {
//        JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
//        detailVC.detailType = DetailTypeAddOil;
//        JYShopListModel * shopModel = self.viewModel.listArr[indexPath.row];
//        detailVC.shopId = shopModel.shopId;
//        [self.navigationController pushViewController:detailVC animated:YES];
//    }else if (indexPath.section == 1){
//
//    }else if (indexPath.section == 3){
//        JYActivityModel * model = self.viewModel.listArr[indexPath.row];
//        JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
//        webVC.url = model.actUrl;
//        webVC.navTitle = @"活动详情";
//        [self.navigationController pushViewController:webVC animated:YES];
//    }else if (indexPath.section == 4){
//        JYMomentDetailViewController * momentDetailVC = [[JYMomentDetailViewController alloc] init];
//        [self.navigationController pushViewController:momentDetailVC animated:YES];
//    }else if (indexPath.section == 5){
//        JYHeZuoDetailViewController * cooperationVC = [[JYHeZuoDetailViewController alloc] init];
//        [self.navigationController pushViewController:cooperationVC animated:YES];
//    }else if (indexPath.section == 6){
//
//    }else if (indexPath.section == 7){
//
//    }
//    if (indexPath.section == 0) {
//        JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
//        detailVC.detailType = DetailTypeAddOil;
//        JYShopListModel * shopModel = self.searchViewModel.searchModel.searchStations[indexPath.row];
//        detailVC.shopId = shopModel.shopId;
//        [self.navigationController pushViewController:detailVC animated:YES];
//    }else if (indexPath.section == 1){
//        JYShopListModel *model = self.searchViewModel.searchModel.searchKeepcars[indexPath.row];
//        JYStoreViewController * storeVC = [[JYStoreViewController alloc ] init];
//        storeVC.shopId = model.shopId;
//        [self.navigationController pushViewController:storeVC animated:YES];
//    }else if (indexPath.section == 3){
//        JYActivityModel * model = self.searchViewModel.searchModel.searchActs[indexPath.row];
//        JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
//        webVC.url = model.actUrl;
//        webVC.navTitle = @"活动详情";
//        [self.navigationController pushViewController:webVC animated:YES];
//    }else if (indexPath.section == 4){
//        JYCommunityModel *model = self.searchViewModel.searchModel.searchBbs[indexPath.row];
//        JYMomentDetailViewController * momentDetailVC = [[JYMomentDetailViewController alloc] init];
//        momentDetailVC.bbsId = model.bbsId;
//        [self.navigationController pushViewController:momentDetailVC animated:YES];
//    }else if (indexPath.section == 5){
//        JYCooperationModel * model = self.searchViewModel.searchModel.searchUnions[indexPath.row];
//        JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
//        webVC.url = model.unionsContentUrl;
//        webVC.navTitle = @"合作加盟详情";
//
//        [self.navigationController pushViewController:webVC animated:YES];
//        //            JYHeZuoDetailViewController * cooperationVC = [[JYHeZuoDetailViewController alloc] init];
//        //            [self.navigationController pushViewController:cooperationVC animated:YES];
//    }else if (indexPath.section == 6){
//
//    }else if (indexPath.section == 7){
//        JYConsultModel *model = self.searchViewModel.searchModel.searchNews[indexPath.row];
//        JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
//        webVC.url = model.newsUrl;
//        webVC.navTitle = @"资讯详情";
//
//        [self.navigationController pushViewController:webVC animated:YES];
//
//    }


    switch (self.moreType) {
        case MoreTypeAddOilStation:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            JYShopListModel * shopModel = self.viewModel.searchModel.searchStations[indexPath.row];
            detailVC.shopId = shopModel.shopId;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case MoreTypeStore:{
            JYShopListModel *model = self.viewModel.searchModel.searchKeepcars[indexPath.row];
            JYStoreViewController * storeVC = [[JYStoreViewController alloc ] init];
            storeVC.shopId = model.shopId;
            storeVC.shopType = [model.type intValue] == 1 ? JYShopType_Store :JYShopType_keepCar;

            [self.navigationController pushViewController:storeVC animated:YES];
        }
            break;
        case MoreTypeProduct:{
            
        }
            break;
        case MoreTypeActivity:{
            JYActivityModel * model = self.viewModel.searchModel.searchActs[indexPath.row];
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.actUrl;
            webVC.navTitle = @"活动详情";
            [self.navigationController pushViewController:webVC animated:YES];

        }
            break;
        case MoreTypeCommunity:{
            JYCommunityModel *model = self.viewModel.searchModel.searchBbs[indexPath.row];
            JYMomentDetailViewController * momentDetailVC = [[JYMomentDetailViewController alloc] init];
            momentDetailVC.bbsId = model.bbsId;
            [self.navigationController pushViewController:momentDetailVC animated:YES];

        }
            break;
        case MoreTypeCooperation:{
            JYCooperationModel * model = self.viewModel.searchModel.searchUnions[indexPath.row];
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.unionsContentUrl;
            webVC.navTitle = @"合作加盟详情";
    
            [self.navigationController pushViewController:webVC animated:YES];

        }
            break;
        case MoreTypeVideo:{
            
        }
            break;
        case MoreTypeConsult:{
            JYConsultModel *model = self.viewModel.searchModel.searchNews[indexPath.row];
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.newsUrl;
            webVC.navTitle = @"资讯详情";
    
            [self.navigationController pushViewController:webVC animated:YES];

        }
            break;

        default:
            break;
    }
}


#pragma mark --------------商品模式collectionView 数据源和代理----------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYProductCell * productCell = [collectionView dequeueReusableCellWithReuseIdentifier:ProductCellId forIndexPath:indexPath];
    productCell.model = self.viewModel.listArr[indexPath.item];
    return productCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    JYMyProductModel * model = self.viewModel.listArr[indexPath.item];
    JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
    detailVC.detailType = DetailTypeStore;
    detailVC.goodsId = model.goodsId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - 20 -5 )/2, [JYProductCell cellH]);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//去掉UItableview headerview黏性(sticky) 去掉header的浮动效果

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    
//    if (scrollView == self.myTableView)
//        
//    {
//        CGFloat sectionHeaderHeight = 55; //sectionHeaderHeight
//        
//        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//            
//            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//            
//        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//            
//            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//        }
//    }
//}
#pragma mark ==================CLTableViewCellDelegate==================

//在willDisplayCell里面处理数据能优化tableview的滑动流畅性，cell将要出现的时候调用
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.moreType == MoreTypeVideo){
        CLModel *model = self.viewModel.listArr[indexPath.row];
        
        CLTableViewCell * myCell = (CLTableViewCell *)cell;
        myCell.model = model;
        //Cell开始出现的时候修正偏移量，让图片可以全部显示
        [myCell cellOffset];
        //第一次加载动画
        
        [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:myCell.model.pictureUrl] completion:^(BOOL isInCache) {
            if (!isInCache) {
                //主线程
                dispatch_async(dispatch_get_main_queue(), ^{
                    CATransform3D rotation;//3D旋转
                    rotation = CATransform3DMakeTranslation(0 ,50 ,20);
                    //逆时针旋转
                    rotation = CATransform3DScale(rotation, 0.8, 0.9, 1);
                    rotation.m34 = 1.0/ -600;
                    myCell.layer.shadowColor = [[UIColor blackColor]CGColor];
                    myCell.layer.shadowOffset = CGSizeMake(10, 10);
                    myCell.alpha = 0;
                    myCell.layer.transform = rotation;
                    [UIView beginAnimations:@"rotation" context:NULL];
                    //旋转时间
                    [UIView setAnimationDuration:0.6];
                    myCell.layer.transform = CATransform3DIdentity;
                    myCell.alpha = 1;
                    myCell.layer.shadowOffset = CGSizeMake(0, 0);
                    [UIView commitAnimations];
                });
            }
        }];
    }
}
//cell离开tableView时调用
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.moreType == MoreTypeVideo){
        
        //因为复用，同一个cell可能会走多次
        if ([_cell isEqual:cell]) {
            //区分是否是播放器所在cell,销毁时将指针置空
            [_playerView destroyPlayer];
            _cell = nil;
        }
    }
}
#pragma mark - 点击播放代理
- (void)cl_tableViewCellPlayVideoWithCell:(CLTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF;
    //记录被点击的Cell
    _cell = cell;
    //销毁播放器
    [_playerView destroyPlayer];
    CLPlayerView *playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, cell.width, cell.height)];
    _playerView = playerView;
    [cell.contentView addSubview:_playerView];
    //    //重复播放，默认不播放
    //    _playerView.repeatPlay = YES;
    //    //当前控制器是否支持旋转，当前页面支持旋转的时候需要设置，告知播放器
    //    _playerView.isLandscape = YES;
    //    //设置等比例全屏拉伸，多余部分会被剪切
    _playerView.fillMode = ResizeAspect;
    //    //设置进度条背景颜色
    //    _playerView.progressBackgroundColor = [UIColor purpleColor];
    //    //设置进度条缓冲颜色
    //    _playerView.progressBufferColor = [UIColor redColor];
    //    //设置进度条播放完成颜色
    //    _playerView.progressPlayFinishColor = [UIColor greenColor];
    //    //全屏是否隐藏状态栏
    //    _playerView.fullStatusBarHidden = NO;
    //    //转子颜色
    //    _playerView.strokeColor = [UIColor redColor];
    //视频地址
    _playerView.url = [NSURL URLWithString:cell.model.videoUrl];
    _playerView.title = cell.model.videoTitle;
    _playerView.isCollect = cell.model.isCollect;
    //播放
    [_playerView playVideo];
    //返回按钮点击事件回调
    [_playerView backButton:^(UIButton *button) {
        NSLog(@"返回按钮被点击");
    }];
    //播放完成回调
    [_playerView endPlay:^{
        //销毁播放器
        [_playerView destroyPlayer];
        _playerView = nil;
        _cell = nil;
        NSLog(@"播放完成");
    }];
    
    _playerView.collectionBtnClickBlock = ^(NSString *isCollect) {
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:cell.model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
}

- (JYSearchViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYSearchViewModel alloc] init];
    }
    return _viewModel;
}

- (JYCollectionViewModel *)collectionViewModel{
    if(!_collectionViewModel){
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}


@end
