//
//  JYSearchViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSearchViewController.h"
#import "JYProductCell.h"
#import "JYDetailViewController.h"
#import "JYProductOrderTableViewCell.h"
#import "JYServiceOrderTableViewCell.h"
#import "JYStoreListTableViewCell.h"
#import "JYStoreTableViewCell.h"
#import "JYSearchResultTitleView.h"
#import "JYActivityTableViewCell.h"
#import "JYCommunityTableViewCell.h"
#import "JYCommunityModel.h"
#import "JYMoreListViewController.h"
#import "JYSearchProductTableViewCell.h"
#import "JYConsultTableViewCell.h"
#import "JYCooperationTableViewCell.h"
#import "JYMyFavoritesMvTableViewCell.h"
#import "JYCooperationModel.h"
#import "JYSearchViewModel.h"
#import "JYLocationViewModel.h"
#import "JYCityModel.h"
#import "JYCommonWebViewController.h"
#import "JYMomentDetailViewController.h"
#import "JYHeZuoDetailViewController.h"
#import "JYStoreViewController.h"
#import "CLTableViewCell.h"
#import "JYCollectionViewModel.h"
#import "CLPlayerView.h"

#import "JYShopListModel.h"
#import "JYActivityModel.h"
#import "JYConsultModel.h"
#import "FMTagsView.h"

static NSString *const ProductCellId = @"JYProductCell";
static NSString *const ProductOrderCellId = @"JYProductOrderTableViewCell";
static NSString *const ServiceOrderCellId = @"JYServiceOrderTableViewCell";
static NSString *const StoreListCellId = @"JYStoreListTableViewCell";
static NSString *const StoreCellId = @"JYStoreTableViewCell";
static NSString *const ActivityCellId = @"JYActivityTableViewCell";
static NSString *const CommunityCellId = @"JYCommunityTableViewCell";
static NSString *const SearchProductCellId = @"JYSearchProductTableViewCell";
static NSString *const ConsultCellId = @"JYConsultTableViewCell";
static NSString *const CooperationCellId = @"JYCooperationTableViewCell";
static NSString *const MyFavoritesMvCellId = @"JYMyFavoritesMvTableViewCell";
static NSString *const CLTableViewCellIdentifier = @"CLTableViewCellIdentifier";

@interface JYSearchViewController ()<
    UITextFieldDelegate,
    UITableViewDelegate,
    UITableViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout,
    CLTableViewCellDelegate,
    FMTagsViewDelegate
>
@property (nonatomic,strong) UIView * searchView ;
@property (nonatomic,strong) UIView * searchSquareView ;
@property (nonatomic,strong) UIImageView * searchIconImageView ;
@property (nonatomic,strong) UITextField * searchTextField ;
@property (nonatomic,strong) UIButton * cancleBtn;

@property (weak, nonatomic) IBOutlet UIButton *clearHistoryBtn;

@property (nonatomic,strong) NSMutableArray * hotTagArr;
@property (weak, nonatomic) IBOutlet UIView *noResultView;

@property (weak, nonatomic) IBOutlet UICollectionView *productCollection;

@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
@property (nonatomic,strong) NSArray * dataArr;
@property (nonatomic,strong) NSArray * searchResultTitleArr;
@property (weak, nonatomic) IBOutlet UILabel *hotLabel;//热门标签

@property (nonatomic,strong) NSArray * cooperationArr;

@property (nonatomic,strong) JYSearchViewModel *searchViewModel;
@property (nonatomic,strong) JYLocationViewModel *locationViewModel;
//搜索关键字
@property (nonatomic,copy) NSString *keyWords;
@property (nonatomic,strong) NSMutableArray *historys;
//viewmodel 中搜索的类型，因为外部传入的和viewmodel 中的不一样
@property (nonatomic,assign) JYSearchType vmSearchType;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;
/**CLplayer*/
@property (nonatomic, weak) CLPlayerView *playerView;
/**记录Cell*/
@property (nonatomic, assign) UITableViewCell *cell;
@property (nonatomic,assign) CGFloat totalSectionCount;

@property (weak, nonatomic) IBOutlet FMTagsView *historySearchView;

@property (weak, nonatomic) IBOutlet FMTagsView *hotSearchView;
@property (weak, nonatomic) IBOutlet UIView *historyView;

@end

@implementation JYSearchViewController


static CGFloat const SearchViewHeight = 32;
static CGFloat const SearchIconW = 20;
#define HotTagW (SCREEN_WIDTH - tagBorderMargin * 2 - (tagCount - 1) * tagMargin) / 3
#define HotTagH 35


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self creatSearchView];
    [self configUI];
//    [self requestGetHotCity];
    [self requestHotSearch];
//    没有搜索结果页面
    self.noResultView.hidden = YES;
    [self getHistoryData];
    [self creatTagsView];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.searchView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.searchView.hidden = YES;
}

-(void)creatTagsView{
    
    //历史搜索
    _historySearchView.contentInsets = UIEdgeInsetsZero;
    _historySearchView.tagInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    _historySearchView.tagBorderWidth = 1;
    _historySearchView.tagcornerRadius = 3;
    _historySearchView.tagBorderColor = RGB0X(0xdbdbdb);
    _historySearchView.tagSelectedBorderColor = [UIColor clearColor];
    _historySearchView.tagBackgroundColor = [UIColor clearColor];
    _historySearchView.backgroundColor = [UIColor clearColor];
    _historySearchView.lineSpacing = 15;
    _historySearchView.interitemSpacing = 19;
    _historySearchView.tagFont = [UIFont systemFontOfSize:14];
    _historySearchView.tagTextColor = RGB0X(0x333333);
    _historySearchView.tagSelectedBackgroundColor = _historySearchView.tagBackgroundColor;
    _historySearchView.tagSelectedTextColor = _historySearchView.tagTextColor;
    _historySearchView.tagSelectedBorderColor = _historySearchView.tagBorderColor;
    _historySearchView.delegate = self;
    _historySearchView.tag = 100;
   
    //热门搜索
    _hotSearchView.contentInsets = UIEdgeInsetsZero;
    _hotSearchView.tagInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    _hotSearchView.tagBorderWidth = 1;
    _hotSearchView.tagcornerRadius = 3;
    _hotSearchView.tagBorderColor = RGB0X(0xdbdbdb);
    _hotSearchView.tagSelectedBorderColor = [UIColor clearColor];
    _hotSearchView.tagBackgroundColor = [UIColor clearColor];
    _hotSearchView.backgroundColor = [UIColor clearColor];
    _hotSearchView.lineSpacing = 15;
    _hotSearchView.interitemSpacing = 19;
    _hotSearchView.tagFont = [UIFont systemFontOfSize:14];
    _hotSearchView.tagTextColor = RGB0X(0x333333);
    _hotSearchView.tagSelectedBackgroundColor = _hotSearchView.tagBackgroundColor;
    _hotSearchView.tagSelectedTextColor = _hotSearchView.tagTextColor;
    _hotSearchView.tagSelectedBorderColor = _historySearchView.tagBorderColor;
    _hotSearchView.delegate = self;
    
    [self updataHistory];
}
- (void)tagsView:(FMTagsView *)tagsView didSelectTagAtIndex:(NSUInteger)index{
    if (tagsView.tag == 100) {
        self.keyWords  = self.historys[index];
    }else{
        self.keyWords  = self.hotTagArr[index];
    }
    if (self.searchType == SearchTypeProduct) {
        self.productCollection.hidden = NO;
    }else{
        self.resultTableView.hidden = NO;
    }
    [self setUpSearchRefreshData];
}

-(void)updataHistory{
    _historySearchView.tagsArray = self.historys;
    _hotSearchView.tagsArray = self.hotTagArr;
    if (!self.historys.count) {
        self.historyView.hidden = YES;
        [self.hotLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(20);
            make.top.equalTo(self.historyView).offset(20);
        }];
    }else{
        self.historyView.hidden = NO;
        [self.hotLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(20);
            make.top.equalTo(self.historyView.mas_bottom).offset(20);
        }];
    }
}
//    创建搜索框
- (void)creatSearchView{
//    iOS11以后调整成40
    UIView * searchView;
    if (kDevice_Is_iPhoneX){
         searchView = [[UIView alloc]initWithFrame:CGRectMake(10, 42, SCREEN_WIDTH - 10, 44)];
    }else{
          searchView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, SCREEN_WIDTH - 10, 44)];
    }
  
    searchView.backgroundColor = [UIColor clearColor];
    self.searchView = searchView;
    [self.navigationController.view addSubview:searchView];
    
    UIView * searchSquareView = [[UIView alloc]initWithFrame:CGRectMake(0, 7, SCREEN_WIDTH - 75, SearchViewHeight)];
    self.searchSquareView = searchSquareView;
    [self.searchView addSubview:searchSquareView];
    searchSquareView.layer.cornerRadius = SearchViewHeight / 2;
    searchSquareView.clipsToBounds = YES;
    searchSquareView.backgroundColor = RGB0X(0xf0f0f0);
    
    UIImageView * searchIconImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"放大镜"]];
    searchIconImageView.frame = CGRectMake(15, 7, SearchIconW, SearchIconW);
    self.searchIconImageView = searchIconImageView;
    [self.searchSquareView addSubview:searchIconImageView];
    
    UITextField * searchTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.searchIconImageView.frame) + 15, 0, self.searchSquareView.width - CGRectGetMaxX(self.searchIconImageView.frame) - 15, SearchViewHeight)];
    self.searchTextField = searchTextField;
    self.searchTextField.placeholder = @"搜索您要查看的内容";
    [self.searchSquareView addSubview:searchTextField];
    [searchTextField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    searchTextField.delegate = self;
    searchTextField.returnKeyType = UIReturnKeySearch;
    searchTextField.keyboardType = UIKeyboardTypeDefault;
    searchTextField.enablesReturnKeyAutomatically = YES;
    searchTextField.font = FONT(14);
    searchTextField.textColor = [UIColor darkGrayColor];
    searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    UIButton * cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancleBtn.frame = CGRectMake(CGRectGetMaxX(self.searchSquareView.frame), 7, SCREEN_WIDTH - CGRectGetMaxX(self.searchSquareView.frame), SearchViewHeight);
    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:RGB0X(0xfcc71e) forState:UIControlStateNormal];
    cancleBtn.titleLabel.font = FONT(16);
    cancleBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [cancleBtn addTarget:self action:@selector(cancleAction:) forControlEvents:UIControlEventTouchUpInside];
    self.cancleBtn = cancleBtn;
    [self.searchView addSubview:cancleBtn];

}

//根据搜索类型控制UI
- (void)configUI{
    self.searchResultTitleArr = @[@"加油站",@"店铺",@"商品",@"活动",@"帖子",@"合作加盟",@"视频",@"资讯"];
    
    if (self.searchType != SearchTypeProduct) {
        
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYProductOrderTableViewCell" bundle:nil] forCellReuseIdentifier:ProductOrderCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYServiceOrderTableViewCell" bundle:nil] forCellReuseIdentifier:ServiceOrderCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYStoreListTableViewCell" bundle:nil] forCellReuseIdentifier:StoreListCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYStoreTableViewCell" bundle:nil] forCellReuseIdentifier:StoreCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYActivityTableViewCell" bundle:nil] forCellReuseIdentifier:ActivityCellId];
        [self.resultTableView registerClass:[JYCommunityTableViewCell class] forCellReuseIdentifier:CommunityCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYSearchProductTableViewCell" bundle:nil] forCellReuseIdentifier:SearchProductCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYConsultTableViewCell" bundle:nil] forCellReuseIdentifier:ConsultCellId];
        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYCooperationTableViewCell" bundle:nil] forCellReuseIdentifier:CooperationCellId];
//        [self.resultTableView registerNib:[UINib nibWithNibName:@"JYMyFavoritesMvTableViewCell" bundle:nil] forCellReuseIdentifier:MyFavoritesMvCellId];
        [self.resultTableView registerClass:[CLTableViewCell class] forCellReuseIdentifier:CLTableViewCellIdentifier];

    }
    self.resultTableView.hidden = YES;
    self.productCollection.hidden = YES;
    
    [self.productCollection registerNib:[UINib nibWithNibName:@"JYProductCell" bundle:nil] forCellWithReuseIdentifier:ProductCellId];
    
    self.resultTableView.backgroundColor = [UIColor whiteColor];
    
}

//- (void)updateUIWithSearchType:(SearchType)searchType{
//    switch (searchType) {
//        case SearchTypeProduct:
//            
//            
//            break;
//        case SearchTypeAddOilOrder:
//            
//            break;
//        case SearchTypeProductOrder:
//            
//            break;
//        case SearchTypeAll:
//            
//            break;
//            
//        default:
//            break;
//    }
//}
#pragma mark ----------------网络请求----------------

- (void)setUpSearchRefreshData{
//    发送高亮关键字通知
//    JY_POST_NOTIFICATION(JY_NOTI_SEARCHKEYWORD, self.keyWords);
    WEAKSELF
    if (self.searchType == SearchTypeProduct) {
        if (self.isStoreProduct) {
            [weakSelf requestShopProduct];
        }else{
            self.productCollection.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                [weakSelf requestSearchResultWithIsMore:NO];
            }];
            self.productCollection.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                [weakSelf requestSearchResultWithIsMore:YES];
            }];
            [self.productCollection.mj_header beginRefreshing];
        }

    }else{
        
        self.resultTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf requestSearchResultWithIsMore:NO];
        }];
        self.resultTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf requestSearchResultWithIsMore:YES];
        }];
        [self.resultTableView.mj_header beginRefreshing];
    }
}



//结束刷新
- (void)endRefresh{
    if (self.searchType == SearchTypeProduct) {
        [self.productCollection.mj_header endRefreshing];
        [self.productCollection.mj_footer endRefreshing];
    }else{
        [self.resultTableView.mj_header endRefreshing];
        [self.resultTableView.mj_footer endRefreshing];
    }
}

- (void)requestSearchResultWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self judgeRequestSearchTypeWithSearchType:self.searchType];
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"key"] = self.keyWords;
        dic[@"cityCode"] = self.cityCode.length?self.cityCode:[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYCODE];
        [self.searchViewModel requestWithParams:dic AndSearchType:self.vmSearchType andIsMore:isMore success:^(NSString *msg, id responseData) {
            [weakSelf endRefresh];
//            if (!weakSelf.searchViewModel.listArr.count && self.searchType != SearchTypeAll ) {
//                weakSelf.noResultView.hidden = NO;
//            }else{
//                weakSelf.noResultView.hidden = YES;
//            }
            if (self.searchType == SearchTypeAll) {
                if ([self updataTotalSectionCount] == 0) {
                    weakSelf.noResultView.hidden = NO;
                }else{
                    weakSelf.noResultView.hidden = YES;
                }
                
            }else{
                if (!weakSelf.searchViewModel.listArr.count) {
                    weakSelf.noResultView.hidden = NO;
                }else{
                    weakSelf.noResultView.hidden = YES;
                }
            }
            
            
            
            if (self.searchType == SearchTypeProduct) {
                [weakSelf.productCollection reloadData];
            }else{
                [weakSelf.resultTableView reloadData];
            }
            
        } failure:^(NSString *errorMsg) {
            weakSelf.noResultView.hidden = NO;
            [weakSelf endRefresh];
            [weakSelf showSuccessTip:errorMsg];
        }];
}
- (void)requestHotSearch{
    WEAKSELF
    [self.searchViewModel requestHotSearchSuccess:^(NSString *msg, id responseData) {
        [weakSelf updataHotSearchUI];
    } failureBlock:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//店铺内商品数据
- (void)requestShopProduct{
    WEAKSELF
    [self showHUD];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"shopId"] = self.shopProductId;
    dic[@"key"] = self.keyWords;
    [self.searchViewModel requestShopSearchWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        if (!weakSelf.searchViewModel.shopProductArr.count) {
            weakSelf.noResultView.hidden = NO;
        }
        [weakSelf.productCollection reloadData];
    } failure:^(NSString *errorMsg) {
        weakSelf.noResultView.hidden = NO;
        [weakSelf showSuccessTip:errorMsg];
        
    }];
}

- (void)updataHotSearchUI{
    [self.hotTagArr removeAllObjects];
    for (JYHotSearchModel *model in self.searchViewModel.hotSearchArr) {
        [self.hotTagArr addObject:model.searchName];
    }
//    [self.hotTagArr addObjectsFromArray:(JYHotSearchModel*)self.searchViewModel.hotSearchArr];
    [self updataHistory];
}

//更新section的个数和标题
- (CGFloat)updataTotalSectionCount{

    for (int i = 0; i < 8; i++) {
        if (i == 0 && self.searchViewModel.searchModel.searchStations.count ) {
            self.totalSectionCount ++;
        }else if (i == 1 && self.searchViewModel.searchModel.searchKeepcars.count){
            self.totalSectionCount ++;
        }else if (i == 2 &&self.searchViewModel.searchModel.searchGoods.count){
            self.totalSectionCount ++;
        }else if (i == 3 &&self.searchViewModel.searchModel.searchActs.count){
            self.totalSectionCount ++;
        }else if (i == 4 &&self.searchViewModel.searchModel.searchBbs.count){
            self.totalSectionCount ++;
        }else if (i == 5 &&self.searchViewModel.searchModel.searchUnions.count){
            self.totalSectionCount ++;
        }else if (i == 6 &&self.searchViewModel.searchModel.searchVideos.count){
            self.totalSectionCount ++;
        }else if (i == 7 && self.searchViewModel.searchModel.searchNews.count){
            self.totalSectionCount ++;
        }
    }
    return self.totalSectionCount;
}

- (void)judgeRequestSearchTypeWithSearchType:(SearchType)searchType{
    switch (searchType) {
        case SearchTypeAll:
            self.vmSearchType = JYSearchType_All;
            break;
        case SearchTypeOilStation:
            self.vmSearchType = JYSearchType_OilStation;
            break;
        case SearchTypeKeepCarStore:
            self.vmSearchType = JYSearchType_Store;
            break;
        case SearchTypeProduct:
            self.vmSearchType = JYSearchType_Product;
            break;
        default:
            break;
    }
}

//收藏
- (void)requestCollectVideoWithVideoId:(NSString *)videoId andOperat:(NSString *)operat andIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"videoId"] = videoId;
    dic[@"operat"] = operat;
    [self.collectionViewModel requestCollectionWithParams:dic andType:JY_CollectionType_Video success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf reloadDataWithIndexPath:indexPath];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark --------action --------

- (void)textChange:(UITextField *)textField{

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length) {
        if (self.searchType == SearchTypeProduct) {
            self.productCollection.hidden = NO;
        }else{
            self.resultTableView.hidden = NO;
        }
        [self writeToPlistWithData:textField.text];
        [self.searchTextField resignFirstResponder];
        self.keyWords = textField.text;
//        增加搜索结果请求
        [self setUpSearchRefreshData];
        return YES;
    }else{
        return NO;
    }
}

- (void)cancleAction:(UIButton *)btn{
    self.searchView.hidden = YES;
    [self.searchTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clearHistoryBtnAction:(UIButton *)sender {
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *historyPath = [documentsDirectory stringByAppendingPathComponent:@"history.plist"];
    BOOL bRet = [fileMgr fileExistsAtPath:historyPath];
    if (bRet) {
        //
        NSError *err;
        [fileMgr removeItemAtPath:historyPath error:&err];
        self.historys = nil;
        [self showSuccessTip:@"清除历史记录成功"];
        [self updataHistory];
    }
}

//热门搜索按钮点击
- (void)hotTagAction:(UIButton *)btn{
    self.keyWords = btn.currentTitle;
    if (self.searchType == SearchTypeProduct) {
        self.productCollection.hidden = NO;
    }else{
        self.resultTableView.hidden = NO;
    }
    [self setUpSearchRefreshData];
}

#pragma mark --------自定义方法-------
//跳转更多页面
- (void)turnMoreVCWithIndex:(NSInteger)index{
    JYMoreListViewController * moreVC = [[JYMoreListViewController alloc] init];
    moreVC.naviTitle = self.searchResultTitleArr[index];
    moreVC.moreType = index;
    moreVC.keyWord = self.keyWords;
    [self.navigationController pushViewController:moreVC animated:YES];
}


//读取历史记录
- (void)getHistoryData{
    
    [self.historys removeAllObjects];
    NSString * docStr = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString * fileName = [docStr stringByAppendingPathComponent:@"history.plist"];
    NSArray * history = [[NSArray alloc] initWithContentsOfFile:fileName];
    if (history.count) {
        [self.historys addObjectsFromArray:history];
    }
    [self updataHistory];
}

//写入历史记录
- (void)writeToPlistWithData:(NSString *)searchText{
    NSString * docStr = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString * fileName = [docStr stringByAppendingPathComponent:@"history.plist"];
    NSMutableArray * historyArr = [[NSMutableArray alloc] initWithContentsOfFile:fileName];
    if (historyArr.count) {
        for (int i = 0; i < historyArr.count; i++) {
            if ([searchText isEqualToString:historyArr[i]]) {
                [historyArr removeObjectAtIndex:i];
            }
        }
        [historyArr insertObject:searchText atIndex:0];
        
        while (historyArr.count > 3) {
             [historyArr removeLastObject];
        }
        
    }else{
        historyArr = [NSMutableArray arrayWithObject:searchText];
    }
    
    [historyArr writeToFile:fileName atomically:YES];
}

//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

- (void)reloadDataWithIndexPath:(NSIndexPath *)indexPath{
    //    JYMyLoveMvModel * model = self.findViewModel.videoListArr[indexPath.row];
    CLModel *model = self.searchViewModel.searchModel.searchVideos[indexPath.row];;

    if ([model.isCollect intValue] == 1) {
        model.isCollect = @"2";
    }else{
        model.isCollect = @"1";
    }
    [self.resultTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark ----------------tableview 的数据源和代理---------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.resultTableView && self.searchType == SearchTypeAll) {
        return 8;
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeProduct){
        return self.searchViewModel.searchModel.searchGoods.count;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.resultTableView && self.searchType == SearchTypeAll) {
        switch (section) {
            case 0:
                return self.searchViewModel.searchModel.searchStations.count;
                break;
            case 1:
                return self.searchViewModel.searchModel.searchKeepcars.count;
                break;
            case 2:
                return 1;
                break;
            case 3:
                return self.searchViewModel.searchModel.searchActs.count;
                break;
            case 4:
                return self.searchViewModel.searchModel.searchBbs.count;
                break;
            case 5:
                return self.searchViewModel.searchModel.searchUnions.count;
                break;
            case 6:
                return self.searchViewModel.searchModel.searchVideos.count;
                break;
            case 7:
                return self.searchViewModel.searchModel.searchNews.count;
                break;
    
            default:
                break;
        }
       
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeProduct){
        return 0;
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeKeepCarStore ){
        return self.searchViewModel.searchModel.searchKeepcars.count;
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeOilStation){
        return  self.searchViewModel.searchModel.searchStations.count;
    }
    return self.historys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    if (self.searchType == SearchTypeAddOilOrder && tableView == self.resultTableView) {
        JYServiceOrderTableViewCell * addOilOrderCell = [tableView dequeueReusableCellWithIdentifier:ServiceOrderCellId];
        return addOilOrderCell;
    }else if(self.searchType == SearchTypeProductOrder && tableView == self.resultTableView){
        JYProductOrderTableViewCell * productOrderCell = [tableView dequeueReusableCellWithIdentifier:ProductOrderCellId];
        return productOrderCell;
    }else if (self.searchType == SearchTypeAll && tableView == self.resultTableView){
        
        JYStoreListTableViewCell * oilStationCell = [tableView dequeueReusableCellWithIdentifier:StoreListCellId];
        
        JYStoreTableViewCell * storeCell = [tableView dequeueReusableCellWithIdentifier:StoreCellId];
        JYActivityTableViewCell * activityCell = [tableView dequeueReusableCellWithIdentifier:ActivityCellId];
        JYCommunityTableViewCell * CommunityCell = [tableView dequeueReusableCellWithIdentifier:CommunityCellId];
        JYSearchProductTableViewCell * searchProductCell = [tableView dequeueReusableCellWithIdentifier:SearchProductCellId];
        JYConsultTableViewCell * consultCell = [tableView dequeueReusableCellWithIdentifier:ConsultCellId];
        JYCooperationTableViewCell * cooperationCell = [tableView dequeueReusableCellWithIdentifier:CooperationCellId];
//        JYMyFavoritesMvTableViewCell * videoCell = [tableView dequeueReusableCellWithIdentifier:MyFavoritesMvCellId];
        CLTableViewCell *videoCell = [tableView dequeueReusableCellWithIdentifier:CLTableViewCellIdentifier];

        
        if (indexPath.section == 0) {
            JYShopListModel *listModel = self.searchViewModel.searchModel.searchStations[indexPath.row];
            oilStationCell.gasStation = self.searchType == SearchTypeOilStation?YES:NO;
            oilStationCell.listModel = listModel;
            [oilStationCell setHighLightWithKeywords:self.keyWords];
            
            WEAKSELF;
            oilStationCell.navBlock = ^(void){
                if (![weakSelf judgeLogin]) {
                    return ;
                }
                    //导航
                    [JYStoreListTableViewCell gotoMap:listModel andViewController:weakSelf];
               
            };

            return oilStationCell;
        }else if (indexPath.section == 1){
            storeCell.model = self.searchViewModel.searchModel.searchKeepcars[indexPath.row];
            [storeCell setHighLightWithKeywords:self.keyWords];
            return storeCell;
        }else if (indexPath.section == 2){
            searchProductCell.dataArr = self.searchViewModel.searchModel.searchGoods;
            [searchProductCell setHighLightWithKeywords:self.keyWords];
            return searchProductCell;
        }else if (indexPath.section == 3){
            activityCell.model = self.searchViewModel.searchModel.searchActs[indexPath.row];
            [activityCell showBottomLine:YES];
            [activityCell setHighLightWithKeywords:self.keyWords];
            return activityCell;
        }else if (indexPath.section == 4){
             JYCommunityModel *model = self.searchViewModel.searchModel.searchBbs[indexPath.row];
            [CommunityCell updateCellModel:model];
            [CommunityCell setHighLightWithKeywords:self.keyWords];
            return CommunityCell;
        }else if (indexPath.section == 5){
            cooperationCell.model = self.searchViewModel.searchModel.searchUnions[indexPath.row];
            [cooperationCell setHighLightWithKeywords:self.keyWords];
            return cooperationCell;
        }else if (indexPath.section == 6){
//            videoCell.model = self.searchViewModel.searchModel.searchVideos[indexPath.row];
//            [videoCell showBottomLine:YES];
//            return videoCell;
//            CLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLTableViewCellIdentifier forIndexPath:indexPath];
            
            videoCell.indexPath = indexPath;
            CLModel *model = self.searchViewModel.searchModel.searchVideos[indexPath.row];
//            videoCell.model = model;
            videoCell.collectionBtnClickBlock = ^(NSString *isCollect){
                if (![self judgeLogin]) {
                    return ;
                }
                [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
            };
            videoCell.delegate = self;
            return videoCell;

        }else{
            consultCell.model = self.searchViewModel.searchModel.searchNews[indexPath.row];
            [consultCell setHighLightWithKeywords:self.keyWords];
            return consultCell;
        }
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeProduct){
        return nil;
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeKeepCarStore){
        JYStoreListTableViewCell * storeCell = [tableView dequeueReusableCellWithIdentifier:StoreListCellId ];
        storeCell.gasStation = self.searchType == SearchTypeOilStation?YES:NO;
        JYShopListModel *listModel = self.searchViewModel.searchModel.searchKeepcars[indexPath.row];
        storeCell.listModel = listModel;
        
        WEAKSELF;
        storeCell.navBlock  = ^(void){
            if (![weakSelf judgeLogin]) {
                return ;
            }
            //导航
            [JYStoreListTableViewCell gotoMap:listModel andViewController:weakSelf];
        };
        [storeCell setHighLightWithKeywords:self.keyWords];
        return storeCell;
    }else{// if (tableView == self.resultTableView && self.searchType == SearchTypeOilStation ){
        JYStoreListTableViewCell * storeCell = [tableView dequeueReusableCellWithIdentifier:StoreListCellId ];
        storeCell.gasStation = self.searchType == SearchTypeOilStation?YES:NO;
        JYShopListModel *listModel = self.searchViewModel.searchModel.searchStations[indexPath.row];
        storeCell.listModel = listModel;
        
        WEAKSELF;
        storeCell.navBlock  = ^(void){
            if (![weakSelf judgeLogin]) {
                return ;
            }
                //导航
                [JYStoreListTableViewCell gotoMap:listModel andViewController:weakSelf];
           
        };
        
        
        [storeCell setHighLightWithKeywords:self.keyWords];
        return storeCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == self.resultTableView && (self.searchType == SearchTypeAddOilOrder || self.searchType == SearchTypeProductOrder)) {
        return 200;
    }else if (tableView == self.resultTableView && self.searchType == SearchTypeAll){
        if (indexPath.section == 0 ||indexPath.section == 1 ||indexPath.section == 7) {
            return 115;
        }else if (indexPath.section == 2){

            NSInteger count = 0;
            if (self.searchViewModel.searchModel.searchGoods.count >= 4) {
                count = 4;
            }else{
                count = self.searchViewModel.searchModel.searchGoods.count;
            }
            CGFloat rowHeight = [JYSearchProductTableViewCell cellHeightWithCount:count];
            return  rowHeight;
        }else if (indexPath.section == 3){
            return 230;
        }else if (indexPath.section == 4){
            JYCommunityModel *model = self.searchViewModel.searchModel.searchBbs[indexPath.row];
            CGFloat rowHeight = [JYCommunityTableViewCell cellHeightAccordingModel:model];
            return rowHeight;
        }else if (indexPath.section == 5){
            JYCooperationModel * model = self.searchViewModel.searchModel.searchUnions[indexPath.row];
            CGFloat rowHeight = [JYCooperationTableViewCell cellHeightWithModel:model];
            return rowHeight;
        }else if (indexPath.section == 6){
            return 200;
        }else{
            return 200;
        }
    }else if(tableView == self.resultTableView && (self.searchType == SearchTypeOilStation || self.searchType == SearchTypeKeepCarStore)){
        return 110;
    }
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    WEAKSELF

    if(tableView == self.resultTableView && self.searchType == SearchTypeAll){
        BOOL isShow;
        
        switch (section) {
            case 0: {
                isShow = self.searchViewModel.searchModel.searchStations.count;
//                return (self.searchViewModel.searchModel.searchStations.count == 0) * 55 + 0.1;
                break;
            }
            case 1: {
                isShow = self.searchViewModel.searchModel.searchKeepcars.count;
                break;
            }
            case 2: {
                isShow = self.searchViewModel.searchModel.searchGoods.count;
                break;
            }
            case 3: {
                isShow = self.searchViewModel.searchModel.searchActs.count;
                break;
            }
            case 4: {
                isShow = self.searchViewModel.searchModel.searchBbs.count;
                break;
            }
            case 5: {
                isShow = self.searchViewModel.searchModel.searchUnions.count;
                break;
            }
            case 6: {
                isShow = self.searchViewModel.searchModel.searchVideos.count;
                break;
            }
            case 7: {
                isShow = self.searchViewModel.searchModel.searchNews.count;
                break;
            }
                
            default:{
                return nil;
            }
                break;
        }
        if (isShow) {
            JYSearchResultTitleView * resultSearchTitleView = LOADXIB(@"JYSearchResultTitleView");
            
            resultSearchTitleView.titleLab.text = self.searchResultTitleArr[section];
            //        resultSearchTitleView.titleLab.text = self.newTitleArr[section];
            resultSearchTitleView.moreBtnActionBlock = ^{
                [weakSelf turnMoreVCWithIndex:section];
            };
            return resultSearchTitleView;

        }else{
            return nil;
        }
    }else{
        return nil;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
     if (tableView == self.resultTableView && self.searchType == SearchTypeAll){
        
        switch (section) {
            case 0: {
                return (self.searchViewModel.searchModel.searchStations.count > 0) * 55 + 0.1;
                break;
            }
            case 1: {
                return (self.searchViewModel.searchModel.searchKeepcars.count > 0) * 55 + 0.1;
                break;
            }
            case 2: {
                return (self.searchViewModel.searchModel.searchGoods.count > 0) * 55 + 0.1;
                break;
            }
            case 3: {
                return (self.searchViewModel.searchModel.searchActs.count > 0) * 55 + 0.1;
                break;
            }
            case 4: {
                return (self.searchViewModel.searchModel.searchBbs.count > 0) * 55 + 0.1;
                break;
            }
            case 5: {
                return (self.searchViewModel.searchModel.searchUnions.count > 0) * 55 + 0.1;
                break;
            }
            case 6: {
                return (self.searchViewModel.searchModel.searchVideos.count > 0) * 55 + 0.1;
                break;
            }
            case 7: {
                return (self.searchViewModel.searchModel.searchNews.count > 0) * 55 + 0.1;
                break;
            }

            default:{
                return 0.1;
            }
                break;
        }
    } else{
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.searchType == SearchTypeAll){
        if (indexPath.section == 0) {
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            JYShopListModel * shopModel = self.searchViewModel.searchModel.searchStations[indexPath.row];
            detailVC.shopId = shopModel.shopId;
            [self.navigationController pushViewController:detailVC animated:YES];
        }else if (indexPath.section == 1){
             JYShopListModel *model = self.searchViewModel.searchModel.searchKeepcars[indexPath.row];
            JYStoreViewController * storeVC = [[JYStoreViewController alloc ] init];
            storeVC.shopId = model.shopId;
            storeVC.shopType = [model.type intValue] == 1 ? JYShopType_Store :JYShopType_keepCar;
            [self.navigationController pushViewController:storeVC animated:YES];
        }else if (indexPath.section == 3){
            JYActivityModel * model = self.searchViewModel.searchModel.searchActs[indexPath.row];
            if (model.actType == 1) {//1.线上商品详情 2 .线下跳连接
                JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
                detailVC.detailType = DetailTypeStore;
                detailVC.goodsId = model.actUrl;
                [self.navigationController pushViewController:detailVC animated:YES];
            }else{
                JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
                webVC.url = model.actUrl;
                webVC.navTitle = @"活动详情";
                [self.navigationController pushViewController:webVC animated:YES];
            }
            
        }else if (indexPath.section == 4){
            JYCommunityModel *model = self.searchViewModel.searchModel.searchBbs[indexPath.row];
            JYMomentDetailViewController * momentDetailVC = [[JYMomentDetailViewController alloc] init];
            momentDetailVC.bbsId = model.bbsId;
            [self.navigationController pushViewController:momentDetailVC animated:YES];
        }else if (indexPath.section == 5){
          JYCooperationModel * model = self.searchViewModel.searchModel.searchUnions[indexPath.row];
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.unionsContentUrl;
            webVC.navTitle = @"合作加盟详情";
            
            [self.navigationController pushViewController:webVC animated:YES];
//            JYHeZuoDetailViewController * cooperationVC = [[JYHeZuoDetailViewController alloc] init];
//            [self.navigationController pushViewController:cooperationVC animated:YES];
        }else if (indexPath.section == 6){
            
        }else if (indexPath.section == 7){
            JYConsultModel *model = self.searchViewModel.searchModel.searchNews[indexPath.row];
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.newsUrl;
            webVC.navTitle = @"资讯详情";
            
            [self.navigationController pushViewController:webVC animated:YES];

        }
    }else if (self.searchType == SearchTypeKeepCarStore){
//        JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
//        detailVC.detailType = DetailTypeKeepCar;
//        JYShopListModel *listModel = self.searchViewModel.searchModel.searchKeepcars[indexPath.row];
//        detailVC.shopId = listModel.shopId;
//        [self.navigationController pushViewController:detailVC animated:YES];
        
        JYShopListModel *model = self.searchViewModel.searchModel.searchKeepcars[indexPath.row];
        JYStoreViewController * storeVC = [[JYStoreViewController alloc ] init];
        storeVC.shopId = model.shopId;
        storeVC.shopType = [model.type intValue] == 1 ? JYShopType_Store :JYShopType_keepCar;
        [self.navigationController pushViewController:storeVC animated:YES];
 
    }else if ( self.searchType == SearchTypeOilStation ){
        
        JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
        detailVC.detailType = DetailTypeAddOil;
        JYShopListModel * shopModel = self.searchViewModel.searchModel.searchStations[indexPath.row];
        detailVC.shopId = shopModel.shopId;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
   }


#pragma mark --------------商品模式collectionView 数据源和代理----------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.isStoreProduct) {
        return self.searchViewModel.shopProductArr.count;
    }
    return self.searchViewModel.listArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYProductCell * productCell = [collectionView dequeueReusableCellWithReuseIdentifier:ProductCellId forIndexPath:indexPath];
    if (self.isStoreProduct) {
           productCell.model = self.searchViewModel.shopProductArr[indexPath.item];
    }else{
        productCell.model = self.searchViewModel.listArr[indexPath.item];
    }
    [productCell setHighLightWithKeywords:self.keyWords];
    return productCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    JYMyProductModel * model ;
    if (self.isStoreProduct) {
        model = self.searchViewModel.shopProductArr[indexPath.item];
    }else{
        
        model = self.searchViewModel.listArr[indexPath.item];
    }
    JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
    detailVC.detailType = DetailTypeStore;
    detailVC.goodsId = model.goodsId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - 20 -5 )/2, [JYProductCell cellH]);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//去掉UItableview headerview黏性(sticky) 去掉header的浮动效果

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == self.resultTableView)
        
    {
        CGFloat sectionHeaderHeight = 55; //sectionHeaderHeight
        
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
            
        } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

#pragma mark ==================CLTableViewCellDelegate==================

//在willDisplayCell里面处理数据能优化tableview的滑动流畅性，cell将要出现的时候调用
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.searchType == SearchTypeAll && tableView == self.resultTableView && indexPath.section == 6){
        CLModel *model = self.searchViewModel.searchModel.searchVideos[indexPath.row];

        CLTableViewCell * myCell = (CLTableViewCell *)cell;
        myCell.model = model;
        [myCell setHighLightWithKeywords:self.keyWords];
        //Cell开始出现的时候修正偏移量，让图片可以全部显示
        [myCell cellOffset];
        //第一次加载动画
        
        [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:myCell.model.pictureUrl] completion:^(BOOL isInCache) {
            if (!isInCache) {
                //主线程
                dispatch_async(dispatch_get_main_queue(), ^{
                    CATransform3D rotation;//3D旋转
                    rotation = CATransform3DMakeTranslation(0 ,50 ,20);
                    //逆时针旋转
                    rotation = CATransform3DScale(rotation, 0.8, 0.9, 1);
                    rotation.m34 = 1.0/ -600;
                    myCell.layer.shadowColor = [[UIColor blackColor]CGColor];
                    myCell.layer.shadowOffset = CGSizeMake(10, 10);
                    myCell.alpha = 0;
                    myCell.layer.transform = rotation;
                    [UIView beginAnimations:@"rotation" context:NULL];
                    //旋转时间
                    [UIView setAnimationDuration:0.6];
                    myCell.layer.transform = CATransform3DIdentity;
                    myCell.alpha = 1;
                    myCell.layer.shadowOffset = CGSizeMake(0, 0);
                    [UIView commitAnimations];
                });
            }
        }];
    }
}
//cell离开tableView时调用
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.searchType == SearchTypeAll && tableView == self.resultTableView && indexPath.section == 6){
        
        //因为复用，同一个cell可能会走多次
        if ([_cell isEqual:cell]) {
            //区分是否是播放器所在cell,销毁时将指针置空
            [_playerView destroyPlayer];
            _cell = nil;
        }
    }
}
#pragma mark - 点击播放代理
- (void)cl_tableViewCellPlayVideoWithCell:(CLTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF;
    //记录被点击的Cell
    _cell = cell;
    //销毁播放器
    [_playerView destroyPlayer];
    CLPlayerView *playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, cell.width, cell.height)];
    _playerView = playerView;
    [cell.contentView addSubview:_playerView];
    //    //重复播放，默认不播放
    //    _playerView.repeatPlay = YES;
    //    //当前控制器是否支持旋转，当前页面支持旋转的时候需要设置，告知播放器
    //    _playerView.isLandscape = YES;
    //    //设置等比例全屏拉伸，多余部分会被剪切
    _playerView.fillMode = ResizeAspect;
    //    //设置进度条背景颜色
    //    _playerView.progressBackgroundColor = [UIColor purpleColor];
    //    //设置进度条缓冲颜色
    //    _playerView.progressBufferColor = [UIColor redColor];
    //    //设置进度条播放完成颜色
    //    _playerView.progressPlayFinishColor = [UIColor greenColor];
    //    //全屏是否隐藏状态栏
    //    _playerView.fullStatusBarHidden = NO;
    //    //转子颜色
    //    _playerView.strokeColor = [UIColor redColor];
    //视频地址
    _playerView.url = [NSURL URLWithString:cell.model.videoUrl];
    _playerView.title = cell.model.videoTitle;
    _playerView.isCollect = cell.model.isCollect;
    //播放
    [_playerView playVideo];
    //返回按钮点击事件回调
    [_playerView backButton:^(UIButton *button) {
        NSLog(@"返回按钮被点击");
    }];
    //播放完成回调
    [_playerView endPlay:^{
        //销毁播放器
        [_playerView destroyPlayer];
        _playerView = nil;
        _cell = nil;
        NSLog(@"播放完成");
    }];
    
    _playerView.collectionBtnClickBlock = ^(NSString *isCollect) {
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:cell.model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
}

- (NSMutableArray *)hotTagArr
{
    if (!_hotTagArr) {
        _hotTagArr = [NSMutableArray array];
    }
    return _hotTagArr;
}

- (JYSearchViewModel *)searchViewModel
{
    if (!_searchViewModel) {
        _searchViewModel = [[JYSearchViewModel alloc] init];
    }
    return _searchViewModel;
}

- (JYLocationViewModel *)locationViewModel
{
    if (!_locationViewModel) {
        _locationViewModel = [[JYLocationViewModel alloc] init];
    }
    return _locationViewModel;
}

- (NSMutableArray *)historys
{
    if (!_historys) {
        _historys = [NSMutableArray array];
    }
    return _historys;
}

- (JYCollectionViewModel *)collectionViewModel{
    if(!_collectionViewModel){
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
