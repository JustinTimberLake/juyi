//
//  JYMoreListViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,MoreType) {
    MoreTypeAddOilStation, //加油站
    MoreTypeStore,//店铺
    MoreTypeProduct,//商品
    MoreTypeActivity,//活动
    MoreTypeCommunity,//帖子
    MoreTypeCooperation,//合作加盟
    MoreTypeVideo,//视频
    MoreTypeConsult//咨询
};

@interface JYMoreListViewController : JYBaseViewController

@property (nonatomic,assign) MoreType moreType;

@property (nonatomic,copy) NSString *keyWord;


@end
