//
//  JYSearchViewController.h
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

//1.全部2.加油站3.店铺4.商品5.活动6.帖子7.合作联盟8.视频9.资讯

//搜索页面类型
typedef NS_ENUM(NSInteger,SearchType) {
    SearchTypeAll = 1,//总页面
    SearchTypeOilStation,//加油站
    SearchTypeKeepCarStore,//店铺
    SearchTypeProduct,//商品
//    5.活动6.帖子7.合作联盟8.视频9.资讯   以后的暂时不写
    SearchTypeAddOilOrder,//加油订单
    SearchTypeProductOrder,//商品订单
};

@interface JYSearchViewController : JYBaseViewController

@property (nonatomic,assign) SearchType searchType;

//店铺内商品
@property (nonatomic,assign) BOOL isStoreProduct;
//店内商品的店铺id
@property (nonatomic,copy) NSString *shopProductId;

@property (nonatomic,copy) NSString *cityCode;
@end
