//
//  JYCooperationTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYCooperationModel;

@interface JYCooperationTableViewCell : UITableViewCell

@property (nonatomic,strong) JYCooperationModel * model;

+ (CGFloat)cellHeightWithModel:(JYCooperationModel *)model;
- (void)setHighLightWithKeywords:(NSString *)keywords;

@end
