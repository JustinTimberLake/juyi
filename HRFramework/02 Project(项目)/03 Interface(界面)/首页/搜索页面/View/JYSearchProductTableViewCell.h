//
//  JYSearchProductTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYMyProductModel;

@interface JYSearchProductTableViewCell : UITableViewCell

@property (nonatomic,strong) NSArray<JYMyProductModel *> *dataArr;

+ (CGFloat)cellHeightWithCount:(CGFloat)count;

- (void)setHighLightWithKeywords:(NSString *)keywords;

@end
