//
//  JYConsultTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYConsultTableViewCell.h"
#import "JYConsultModel.h"


@interface JYConsultTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end
@implementation JYConsultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(JYConsultModel *)model{
    _model = model;
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:model.newsImage] placeholderImage:nil];
    self.titleLab.text = model.newsTitle;
    self.timeLab.text = model.newsTime;
}

- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.titleLab setLabelTitleHighlight:self.model.newsTitle andSearchStr:keywords];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
