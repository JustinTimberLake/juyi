//
//  JYStoreTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYShopListModel;
@interface JYStoreTableViewCell : UITableViewCell

@property (nonatomic,strong) JYShopListModel *model;

- (void)setHighLightWithKeywords:(NSString *)keywords;
@end
