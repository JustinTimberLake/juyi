//
//  JYSearchResultTitleView.m
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSearchResultTitleView.h"

@implementation JYSearchResultTitleView

- (IBAction)moreBtnAction:(UIButton *)sender {
    if (_moreBtnActionBlock) {
        self.moreBtnActionBlock();
    }
}

@end
