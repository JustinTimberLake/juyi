//
//  JYSearchHistoryCell.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSearchHistoryCell.h"
#import "JYCityModel.h"
#import "JYShopTypeModel.h"
#import "JYGetInvoiceModel.h"

@implementation JYSearchHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)hideLine{
    self.lineView.hidden = YES;
}
- (void)setBackgroundViewColor:(UIColor *)color{
    self.bgView.backgroundColor = color;
}

- (void)setSearchCityModel:(JYCityModel *)SearchCityModel{
    _SearchCityModel = SearchCityModel;
    self.contentLab.text = SearchCityModel.cityTitle;
}

- (void)setShopTypeModel:(JYShopTypeModel *)shopTypeModel{
    _shopTypeModel = shopTypeModel;
    self.contentLab.text = shopTypeModel.shopTypeTitle;
}

- (void)setInvoiceModel:(JYGetInvoiceModel *)invoiceModel{
    _invoiceModel = invoiceModel;
    self.contentLab.text = invoiceModel.invoiceTitle;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
