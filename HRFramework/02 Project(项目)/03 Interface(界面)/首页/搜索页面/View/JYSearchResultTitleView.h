//
//  JYSearchResultTitleView.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSearchResultTitleView : UIView
//更多按钮的点击
@property (nonatomic,copy) void(^moreBtnActionBlock)();
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@end
