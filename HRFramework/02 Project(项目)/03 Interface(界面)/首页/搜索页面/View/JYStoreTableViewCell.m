//
//  JYStoreTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYStoreTableViewCell.h"
#import "JYShopListModel.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件

@interface JYStoreTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (weak, nonatomic) IBOutlet UILabel *distanceLab;
@property (weak, nonatomic) IBOutlet UIView *starView;

@end

@implementation JYStoreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(JYShopListModel *)model{
    _model = model;
    [self.picImageView sd_setImageWithURL:[NSURL URLWithString:model.shopImage] placeholderImage:JY_PLACEHOLDER_IMAGE];
    self.titleLab.text = model.shopTitle;
    self.telLab.text = model.shopTel;
    [self configStar:model];
    
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
    self.distanceLab.text = [self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[model.shopLat floatValue] endLng:[model.shopLong floatValue]];
}
- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.titleLab setLabelTitleHighlight:self.model.shopTitle andSearchStr:keywords];
}
- (void)configStar:(JYShopListModel *)model{
    NSInteger num = [model.shopEvaluate intValue];
    for (UIImageView * view in self.starView.subviews) {
        if ((view.tag - 1000) < num) {
            view.image = [UIImage imageNamed:@"评价黄星"];
        }else{
            view.image = [UIImage imageNamed:@"评价灰星"];
        }
    }
}

//获得两点之间的距离
- (NSString *)getTheDistanceWithStartLat:(CGFloat)startLat startLng:(CGFloat)startLng andEndLat:(CGFloat)endLat endLng:(CGFloat )endLng{
    BMKMapPoint start = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(startLat,startLng));
    BMKMapPoint end = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(endLat,endLng));
    CLLocationDistance distance = BMKMetersBetweenMapPoints(start, end);
    NSString *distanceStr;
//    if (distance > 1000) {
        distanceStr = [NSString stringWithFormat:@"%.2fkm",distance/1000];
//    }else{
//        distanceStr = [NSString stringWithFormat:@"%.2fm",distance];
//    }
    return distanceStr;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
