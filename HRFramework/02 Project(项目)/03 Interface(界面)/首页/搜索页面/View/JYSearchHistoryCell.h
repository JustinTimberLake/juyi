//
//  JYSearchHistoryCell.h
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYCityModel;
@class JYShopTypeModel;
@class JYGetInvoiceModel;

@interface JYSearchHistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *bgView;

//搜索城市模型
@property (nonatomic,strong) JYCityModel *SearchCityModel;
//店铺类型模型
@property (nonatomic,strong) JYShopTypeModel *shopTypeModel;
//获取发票抬头模型
@property (nonatomic,strong) JYGetInvoiceModel *invoiceModel;


- (void)hideLine;
- (void)setBackgroundViewColor:(UIColor *)color;
- (void)tansformMethodSelect;
- (void)tansformMethodUnSelect;

@end
