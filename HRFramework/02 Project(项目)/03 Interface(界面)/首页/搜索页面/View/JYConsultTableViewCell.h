//
//  JYConsultTableViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYConsultModel;
@interface JYConsultTableViewCell : UITableViewCell

@property (nonatomic,strong) JYConsultModel *model;
- (void)setHighLightWithKeywords:(NSString *)keywords;

@end
