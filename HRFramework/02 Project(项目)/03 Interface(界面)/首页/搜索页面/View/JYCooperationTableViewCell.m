//
//  JYCooperationTableViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCooperationTableViewCell.h"
#import "JYCommentImageViewCell.h"
#import "JYCooperationModel.h"

static NSString * const ImageCellId = @"JYCommentImageViewCell";
static int const Count = 3;
static int const ImageM = 5;
static int const contentM = 10;
static int const CollectionViewM = 10;

#define ItemW  (SCREEN_WIDTH - (Count - 1) * ImageM - CollectionViewM * 2) / Count

@interface JYCooperationTableViewCell ()<
    UICollectionViewDelegateFlowLayout,
    UICollectionViewDelegate,
    UICollectionViewDataSource
>

@property (weak, nonatomic) IBOutlet UICollectionView *picCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *picCollectionViewH;
@property (nonatomic,strong) NSMutableArray * imageArr;
@property (nonatomic,assign) float rowsCount;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;


@end
@implementation JYCooperationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configCollectionView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

- (void)configCollectionView{
    self.picCollectionView.delegate = self;
    self.picCollectionView.dataSource = self;
    self.picCollectionView.bounces = NO;
    self.picCollectionView.showsVerticalScrollIndicator = NO;
    [self.picCollectionView registerNib:[UINib nibWithNibName:@"JYCommentImageViewCell" bundle:nil] forCellWithReuseIdentifier:ImageCellId];
//    self.rowsCount = (self.imageArr.count + Count -1) / Count;
//    self.picCollectionViewH.constant = (ItemW + ImageM ) * self.rowsCount - ImageM;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imageArr.count <3 ?self.imageArr.count:3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYCommentImageViewCell * imageCell = [collectionView dequeueReusableCellWithReuseIdentifier:ImageCellId forIndexPath:indexPath];
    imageCell.imageModel = self.imageArr[indexPath.item];
    return imageCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(ItemW, ItemW);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

+ (CGFloat)cellHeightWithModel:(JYCooperationModel *)model{
    CGFloat contentH = [model.unionsContent stringHeightAtWidth:SCREEN_WIDTH - 20 font:FONT(15)];
    if(contentH >34){
        contentH = 34;
    }
//    float rowsCount = (model.unionsImages.count + Count -1) / Count;
//    CGFloat imageViewH = (ItemW + ImageM ) * rowsCount - ImageM;
    return contentH + contentM * 3 + ItemW + 70;
}

- (void)setModel:(JYCooperationModel *)model{
    _model = model;
    [self.imageArr removeAllObjects];
    self.detailLab.text = model.unionsContent;
    self.titleLab.text = model.unionsTitle;
    self.timeLab.text = [NSString YYYYMMDDHHMMWithTimevalue:model.unionsTime styleWithDot:NO];
    [self.imageArr addObjectsFromArray:model.unionsImages];
    [self.picCollectionView reloadData];
}

- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.titleLab setLabelTitleHighlight:self.model.unionsTitle andSearchStr:keywords];
    [self.detailLab setLabelTitleHighlight:self.model.unionsContent andSearchStr:keywords];
}

- (NSMutableArray *)imageArr
{
    if (!_imageArr) {
        _imageArr = [NSMutableArray array];
    }
    return _imageArr;
}

@end
