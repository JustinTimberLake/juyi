//
//  JYSearchModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@class JYShopListModel;
@class JYActivityModel;
@class JYMyProductModel;
@class JYCommunityModel;
@class JYCooperationModel;
@class JYMyLoveMvModel;
@class JYConsultModel;
@class CLModel;

@interface JYSearchModel : JYBaseModel


//// 加油站
//    [{
//        同3.5.2 JY-005-002 获取店铺列表
//    }]
@property (nonatomic,strong) NSArray<JYShopListModel *> *searchStations;
// 店铺
//    [{
//        同3.5.2 JY-005-002 获取店铺列表
//    }]
@property (nonatomic,strong) NSArray<JYShopListModel *> *searchKeepcars;
// 商品
//    [{
//        同3.7.3. JY-007-003 商品列表
//    }]
@property (nonatomic,strong) NSArray<JYMyProductModel *> *searchGoods;
// 活动
//    [{
//        同3.8.1 JY-008-001 获取活动
//    }]
@property (nonatomic,strong) NSArray<JYActivityModel *> *searchActs;
// 帖子
//    [{
//        同3.9.2. JY-009-002 获取社区帖子列表
//    }]
@property (nonatomic,strong) NSArray<JYCommunityModel *> *searchBbs;
// 合作联盟
//    [{
//        同3.10.4 JY-010-004 获取合作加盟列表
//    }]
@property (nonatomic,strong) NSArray<JYCooperationModel *> *searchUnions;
// 视频
//    [{
//        同3.10.2. JY-010-002 获取视频列表
//    }]
@property (nonatomic,strong) NSArray<CLModel *> *searchVideos;
// 资讯
//    [{
//        同3.10.6. JY-010-006 获取资讯列表
//    }]
@property (nonatomic,strong) NSArray<JYConsultModel *> *searchNews;

@end
