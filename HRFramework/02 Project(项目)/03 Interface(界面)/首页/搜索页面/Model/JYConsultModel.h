//
//  JYConsultModel.h
//  JY
//
//  Created by risenb on 2017/8/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYConsultModel : JYBaseModel
//资讯ID
@property (nonatomic,copy) NSString *newsId;

//标题
@property (nonatomic,copy) NSString *newsTitle;

//图片
@property (nonatomic,copy) NSString *newsImage;

//时间
@property (nonatomic,copy) NSString *newsTime;

//详情URL
@property (nonatomic,copy) NSString *newsUrl;


@end
