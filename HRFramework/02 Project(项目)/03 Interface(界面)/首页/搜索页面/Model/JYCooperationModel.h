//
//  JYCooperationModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@class JYImageModel;

@interface JYCooperationModel : JYBaseModel

//@property (nonatomic,copy) NSString * detail;
//@property (nonatomic,strong) NSMutableArray * imageArr;

// 加盟ID
@property (nonatomic,copy) NSString *unionsId;
//标题
@property (nonatomic,copy) NSString *unionsTitle;
//时间
@property (nonatomic,copy) NSString *unionsTime;
//内容
@property (nonatomic,copy) NSString *unionsContent;
//图片
@property (nonatomic,strong) NSArray<JYImageModel *> *unionsImages;
//详情URL
@property (nonatomic,copy) NSString *unionsContentUrl;


@end
