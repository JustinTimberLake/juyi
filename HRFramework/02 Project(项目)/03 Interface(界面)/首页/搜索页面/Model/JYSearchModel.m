//
//  JYSearchModel.m
//  JY
//
//  Created by duanhuifen on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSearchModel.h"

@implementation JYSearchModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"searchStations":@"JYShopListModel",
             @"searchKeepcars":@"JYShopListModel",
             @"searchGoods":@"JYMyProductModel",
             @"searchActs":@"JYActivityModel",
             @"searchBbs":@"JYCommunityModel",
             @"searchUnions":@"JYCooperationModel",
             @"searchVideos":@"CLModel",
             @"searchNews":@"JYConsultModel",
             };
}
@end
