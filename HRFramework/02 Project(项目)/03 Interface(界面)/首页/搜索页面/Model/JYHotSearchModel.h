//
//  JYHotSearchModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYHotSearchModel : JYBaseModel
@property (nonatomic,copy) NSString *searchId;
@property (nonatomic,copy) NSString *searchName;
@end
