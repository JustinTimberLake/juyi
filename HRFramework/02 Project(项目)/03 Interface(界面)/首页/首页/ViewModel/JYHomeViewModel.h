//
//  JYHomeViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@class JYHomeBannerModel;
@class JYHomeTypeModel;

typedef NS_ENUM(NSInteger,BannerType) {
    BannerType_Home = 1,//首页
    BannerType_Store,//返利商城
    BannerType_Activity,//活动
};

@interface JYHomeViewModel : JYBaseViewModel

@property (nonatomic,assign) BannerType bannerType;

//广告位数组
@property (nonatomic,strong) NSMutableArray *ADBannerArr;

//首页banner数组
@property (nonatomic,strong) NSMutableArray<JYHomeBannerModel *> *bannerArr;
//首页分类数组
@property (nonatomic,strong) NSMutableArray<JYHomeTypeModel *> *homeTypeArr;

//获取城市code
- (void)requestGetCityCodeWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取轮播图banner
- (void)requestBannerWithBannerType:(BannerType)bannerType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//首页分类
- (void)requestGetHomeTypeSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//广告位接口
- (void)requestGetADPositionWithType:(NSString *)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//是否弹出新人奖励/消费
- (void)requestShowCoupon:(NSString *)token success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
