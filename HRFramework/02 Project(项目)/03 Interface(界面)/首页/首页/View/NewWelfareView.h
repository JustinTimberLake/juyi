//
//  NewWelfareView.h
//  JY
//
//  Created by Justin on 2018/8/3.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^sureBlock)();
typedef NS_ENUM(NSInteger,NewcomerType) {
    NewcomerAward,//新人奖励
    NewcomerConsumption//新人消费
};@interface NewWelfareView : UIView

+ (void)showWith:(NewcomerType)type Block:(sureBlock)blcok;
@end
