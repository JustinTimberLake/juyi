//
//  NewWelfareView.m
//  JY
//
//  Created by Justin on 2018/8/3.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import "NewWelfareView.h"
#import "SELUpdateAlertConst.h"
#import "JTNButton.h"
#define DEFAULT_MAX_HEIGHT SCREEN_HEIGHT/4*2

@interface NewWelfareView()
@property (nonatomic,copy) sureBlock block;
@property (nonatomic,assign) NewcomerType type;
@end
@implementation NewWelfareView

+ (void)showWith:(NewcomerType)type Block:(sureBlock)blcok
{
    
    NewWelfareView *updateAlert = [[NewWelfareView alloc]initWith:type];
    updateAlert.block = blcok;
    [[UIApplication sharedApplication].delegate.window addSubview:updateAlert];
}

- (instancetype)initWith:(NewcomerType)type
{
    self = [super init];
    if (self) {
        self.type = type;
        [self _setupUI];
 
    }
    return self;
}

- (void)_setupUI
{
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithHex:0x000000 alpha:0.6];
    
    
    //backgroundView
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self addSubview:bgView];
//    bgView.backgroundColor = [UIColor colorWithHex:0x000000 alpha:0.6];
    //获取更新内容高度
    
    
    UIImageView *imageView = [[UIImageView alloc]init];
    [self addSubview:imageView];
    imageView.image = self.type== NewcomerAward?[UIImage imageNamed:@"新人奖励"]:[UIImage imageNamed:@"新人消费"];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgView);
        make.centerY.equalTo(bgView);
    }];
    
    JTNButton *sureBtn = [JTNButton buttonWithType:UIButtonTypeCustom];
//    sureBtn.backgroundColor = [UIColor redColor];
    [self addSubview:sureBtn];
    [sureBtn addTarget:self action:@selector(receive) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(imageView.mas_bottom).offset(Ratio(-23));
        make.width.offset(Ratio(166));
        make.height.offset(Ratio(32));
        make.centerX.equalTo(bgView);
    }];

    
    //取消按钮
    JTNButton *cancelButton = [JTNButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setImage:[UIImage imageNamed:@"取消"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(Ratio(32));
        make.centerX.equalTo(bgView);
    }];
    
    //显示更新
    [self showWithAlert:bgView];
}
/** 取消按钮点击事件 */
- (void)cancelAction
{
    [self dismissAlert];
}

//立即领取
-(void)receive{
    if (self.block) {
        self.block();
    }
    [self cancelAction];
}

/**
 添加Alert入场动画
 @param alert 添加动画的View
 */
- (void)showWithAlert:(UIView*)alert{
    
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = SELAnimationTimeInterval;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [alert.layer addAnimation:animation forKey:nil];
}


/** 添加Alert出场动画 */
- (void)dismissAlert{
    
    [UIView animateWithDuration:SELAnimationTimeInterval animations:^{
        self.transform = (CGAffineTransformMakeScale(1.5, 1.5));
        self.backgroundColor = [UIColor clearColor];
        self.alpha = 0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    } ];
    
}

/**
 计算字符串高度
 @param string 字符串
 @param font 字体大小
 @param maxSize 最大Size
 @return 计算得到的Size
 */
- (CGSize)_sizeofString:(NSString *)string font:(UIFont *)font maxSize:(CGSize)maxSize
{
    return [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;
}


-(void)setSureBlock:(void (^)(void))sureBlock{
    
    objc_setAssociatedObject(self, @selector(sureBlock), sureBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

- (void(^)(void))sureBlock

{
    
    return objc_getAssociatedObject(self, @selector(sureBlock));
    
}

@end
