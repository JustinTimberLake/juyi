//
//  JYHomeCollectionViewCell.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHomeCollectionViewCell.h"
#import "JYHomeTypeModel.h"

@implementation JYHomeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if (SCREEN_WIDTH == 320) {
        self.ImageTop.constant = 10;
    }else{
        self.ImageTop.constant = HB_IPONE6_ADAPTOR_WIDTH(30);
    }
}

- (void)setModel:(JYHomeTypeModel *)model{
    self.titleLab.text = model.homeTitle;
    self.detailLab.text = model.homeSubtitle;
}


@end
