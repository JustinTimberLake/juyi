//
//  JYHomeCollectionViewCell.h
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYHomeTypeModel;
@interface JYHomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *topIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (nonatomic,strong) JYHomeTypeModel *model;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageTop;

//- (void)cellWithData:(NSArray *)dataArr andIndexPath:(NSIndexPath *)indexPath;

@end
