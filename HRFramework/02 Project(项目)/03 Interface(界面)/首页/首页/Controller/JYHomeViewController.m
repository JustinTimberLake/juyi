//
//  JYHomeViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/6/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHomeViewController.h"
#import "JYHomeCollectionViewCell.h"
#import "HFButton.h"
#import "SDCycleScrollView.h"
#import "JYSearchViewController.h"
#import "JYLocationViewController.h"
#import "JYMaintainCarAroundViewController.h"
#import "JYStoreListViewController.h"
#import "JYActivityListViewController.h"
#import "JYRebateStoreController.h"
#import "JYTurnSearchView.h"
#import "HR_ZoomViewVC.h"
#import "JYHomeViewModel.h"
#import "JYHomeBannerModel.h"
#import "JYDetailViewController.h"
#import "JYCommonWebViewController.h"
#import "JYVideoListViewController.h"
#import "JYHomeTypeModel.h"
#import "JYSelectBtn.h"
#import "JYContactUsViewController.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import "SNStarsAlertView.h"
//版本更新提醒
#import "HSUpdateApp.h"
#import "SELUpdateAlert.h"
//新人福利弹窗
#import "NewWelfareView.h"

#import "JYMyRedPickViewModel.h"
#import "JYGetCouponViewController.h"

typedef NS_ENUM(NSInteger,JYCityType) {
    JYCityType_Location,//定位
    JYCityType_Select   //选择城市
};

@interface JYHomeViewController ()<
UICollectionViewDelegate,
UICollectionViewDataSource,
UITextFieldDelegate,
UIAlertViewDelegate,
SDCycleScrollViewDelegate,
BMKLocationServiceDelegate
>
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic,strong) UIView * searchView ;
@property (nonatomic,strong) UIView * searchSquareView ;
@property (nonatomic,strong) UIImageView * searchIconImageView ;
@property (nonatomic,strong) UITextField * searchTextField ;
@property (nonatomic,strong) JYSelectBtn * cityBtn ;
@property (nonatomic,strong) SDCycleScrollView * scrollView;
//轮播图本地图片数组
@property (nonatomic,strong) NSArray * imageArr;
@property (nonatomic,strong) NSArray *homeTypeImageArr;
@property (nonatomic,strong) JYHomeViewModel *homeViewModel;
@property (nonatomic,copy) NSString *currentCityName;
@property (nonatomic,strong) BMKLocationService *locService;
@property (nonatomic,strong) CLGeocoder *geoCoder;
@property (nonatomic,copy) NSString *lat;
@property (nonatomic,copy) NSString *lng;
@property (nonatomic,strong) UIButton * searchBtn;
@end

@implementation JYHomeViewController{
    BOOL _cancel;//异地使用时取消切换为YES
}

static NSString * const cellId = @"JYHomeCollectionViewCell";
static CGFloat const SearchViewHeight = 32;
static CGFloat const SearchIconW = 20;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //防止卡死
    self.firstLevel = YES;
    [self initData];
    [self configNavUI];
    [self congfigCollectionView];
    [self creatBannerView];
    [self addNotification];
    
    //    网络请求
    [self requestHomeBanner];
    [self requestHomeType];
    
    JY_POST_NOTIFICATION(JY_NOTI_START_LOCATIONCITY, nil);
    
    //版本更新
    [HSUpdateApp hs_updateWithAPPID:@"1290029956" block:^(NSString *currentVersion, NSString *storeVersion, NSString *openUrl, BOOL isUpdate) {
        if (isUpdate) {
            [SELUpdateAlert showUpdateAlertWithVersion:storeVersion Description:@"快去更新吧"];
        }else{
            
        }
    }];
    
    
    
//    JY_ADD_NOTIFICATION(JY_NOTI_NEWWELFARE);
}

- (void)newWelfare{
    if ([User_InfoShared shareUserInfo].c.length) {
        [[JYHomeViewModel alloc]requestShowCoupon:[User_InfoShared shareUserInfo].c success:^(NSString *msg, id responseData) {
            NSDictionary *dic = responseData;
            if ([dic[@"show1"] integerValue]) {
                //新人奖励
                [NewWelfareView showWith:NewcomerAward Block:^{
                    JYGetCouponViewController *vc= [[JYGetCouponViewController alloc]init];
                    vc.newUser = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }];
            }else if([dic[@"show2"] integerValue]){
                //新人消费
                [NewWelfareView showWith:NewcomerConsumption Block:^{
                    JYGetCouponViewController *vc= [[JYGetCouponViewController alloc]init];
                    [self.navigationController pushViewController:vc animated:YES];
                }];
            }
        } failure:^(NSString *errorMsg) {
            
        }];

    }
}

- (void)viewWillAppear:(BOOL)animated{
    self.searchView.hidden = NO;
    [self newWelfare];//查看是否有新人福利
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.searchView.hidden = YES;
}


- (void)configNavUI{
    
    [self creatCityBtn];
    
    [self creatSearchView];
    
    //客服调距离 - -!
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 22, 20);
    [rightBtn addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImage:[UIImage imageNamed:@"客服"] forState:UIControlStateNormal];
    // 让按钮的内容往左边偏移10
    rightBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -10);
    UIBarButtonItem *rigtItem =  [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rigtItem;
    
//    WEAKSELF
//    [self rightImageItem:@"客服" action:^{
//        if (![weakSelf judgeLogin]) {
//            return ;
//        }
//        JYContactUsViewController * contactVC = [[JYContactUsViewController alloc] init];
//        [weakSelf.navigationController pushViewController:contactVC animated:YES];
//    }];
}

- (void)creatCityBtn{
    
    JYSelectBtn * cityBtn = [JYSelectBtn buttonWithType:UIButtonTypeCustom];
    self.cityBtn = cityBtn;
    [cityBtn addTarget:self action:@selector(citySelectAction:) forControlEvents:UIControlEventTouchUpInside];
    [cityBtn setImage:[UIImage imageNamed:@"下拉三角"] forState:UIControlStateNormal];
    [cityBtn setTitle:@"定位中" forState:UIControlStateNormal];
    [cityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cityBtn.titleLabel.font = FONT(15);
    // 让按钮内部的所有内容左对齐
    cityBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    // 让按钮的内容往左边偏移10
    cityBtn.contentEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 0);
    
    UIBarButtonItem *leftItem =  [[UIBarButtonItem alloc]initWithCustomView:cityBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}

//    创建搜索框
- (void)creatSearchView{
    
    self.navigationItem.titleView = self.searchView;
    self.searchSquareView = [[UIView alloc]init];
    CGFloat btnW = [NSString widthOfText:self.cityBtn.titleLabel.text textHeight:MAXFLOAT font:FONT(15)] + 5 + 10;
    self.searchSquareView.frame = CGRectMake(0, 0, SCREEN_WIDTH- 10 - btnW - 15 - 45, SearchViewHeight);
    
    self.searchSquareView.layer.cornerRadius = SearchViewHeight / 2;
    self.searchSquareView.clipsToBounds = YES;
    self.searchSquareView.backgroundColor = RGB0X(0xf0f0f0);
    
    [self.searchView addSubview:self.searchSquareView];
    
    UIImageView * searchIconImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"放大镜"]];
    searchIconImageView.frame = CGRectMake(15, 7, SearchIconW, SearchIconW);
    self.searchIconImageView = searchIconImageView;
    [self.searchSquareView addSubview:searchIconImageView];
    
    UIButton * searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.searchBtn = searchBtn;
    searchBtn.frame = CGRectMake(CGRectGetMaxX(self.searchIconImageView.frame), 0, self.searchView.width - CGRectGetMaxX(self.searchIconImageView.frame), SearchViewHeight);
    [self.searchSquareView addSubview:searchBtn];
    [searchBtn addTarget:self action:@selector(searchBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)rightItemAction{
    if (![self judgeLogin]) {
        return ;
    }
    JYContactUsViewController * contactVC = [[JYContactUsViewController alloc] init];
    [self.navigationController pushViewController:contactVC animated:YES];
}
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

- (void)initData{
    self.homeTypeImageArr = @[@"养车周边",@"加油",@"商城",@"活动"];
    NSString * cityName = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYNAME];
    self.currentCityName = cityName.length ? cityName : @"北京";
}

- (void)updataSelectCityWithCityType:(JYCityType)cityType{
    NSString *selectName  = [JY_USERDEFAULTS objectForKey:JY_CURRENT_SELECTCITYNAME];
    if (selectName.length) {
        [self updateCityWith:selectName];
    }else{
        [self updateCityWith:[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYNAME]];
    }
    
    if (cityType== JYCityType_Location && ![[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYNAME] isEqualToString:selectName] && [[JY_USERDEFAULTS objectForKey:JY_CURRENT_SELECTCITYNAME] length]&&!_cancel) {
        
        static dispatch_once_t disOnce;
        
        dispatch_once(&disOnce,^ {
            
            //只执行一次的代码
            
            SNStarsAlertView *alert = [[SNStarsAlertView alloc] initWithTitle:@"提示" message:[NSString stringWithFormat:@"当前定位城市是%@,是否切换",[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYNAME] ] cancelButtonTitle:@"取消" otherButtonTitle:@"确定" cancelButtonClick:^{
                _cancel = YES;
            } otherButtonClick:^{
                _cancel = NO;
                [JY_USERDEFAULTS setObject:@"" forKey:JY_CURRENT_SELECTCITYNAME];
                NSString *name = [JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYNAME];
                [self updateCityWith:name];
            }];
            [alert show];
            
        });
        
    }
}

-(void)updateCityWith:(NSString *)name{
    self.currentCityName = name;
    [self.cityBtn setTitle:name forState:UIControlStateNormal];
    CGFloat btnW = [NSString widthOfText:name textHeight:MAXFLOAT font:FONT(15)] + 5 + 10;
    self.cityBtn.frame = CGRectMake(0, 0, btnW, 20);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.cityBtn];
    [self requestCityCode];
    
    //    self.searchView.left = CGRectGetMaxX(self.cityBtn.frame) + 30;
    //    self.searchView.width = SCREEN_WIDTH - CGRectGetMaxX(self.cityBtn.frame) - 15 - 45;
    
    self.searchView.frame = CGRectMake(CGRectGetMaxX(self.cityBtn.frame) + 15, 25, SCREEN_WIDTH - (CGRectGetMaxX(self.cityBtn.frame)-15-45) , SearchViewHeight);
    self.searchSquareView.frame = CGRectMake(0, 0, SCREEN_WIDTH - 10 - btnW -15-45 , SearchViewHeight);
    
    self.searchBtn.frame = CGRectMake(CGRectGetMaxX(self.searchIconImageView.frame), 0, self.searchView.width - CGRectGetMaxX(self.searchIconImageView.frame), SearchViewHeight);
}



- (void)congfigCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.headerReferenceSize =  CGSizeMake(SCREEN_WIDTH, HB_IPONE6_ADAPTOR_WIDTH(190));
    flowLayout.minimumLineSpacing = 0;
    flowLayout.minimumInteritemSpacing = 0;
    self.myCollectionView.collectionViewLayout = flowLayout;
    
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYHomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellId];
    
    self.myCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //    网络请求
        [self requestHomeBanner];
        [self requestHomeType];
    }];
}

- (void)creatBannerView{
    //轮播图也写到layoutSubview里了
    self.scrollView = [SDCycleScrollView cycleScrollViewWithFrame:(CGRectMake(0, 0, SCREEN_WIDTH, HB_IPONE6_ADAPTOR_WIDTH(190))) delegate:self  placeholderImage:[UIImage imageNamed:@"首页banner"]];
    self.scrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    self.scrollView.delegate = self; // 如需监听图片点击，请设置代理，实现代理方法
    self.scrollView.autoScrollTimeInterval = 3;// 自定义轮播时间间隔
    self.scrollView.titleLabelBackgroundColor = [UIColor clearColor];
    self.scrollView.showPageControl = YES;
    self.scrollView.localizationImageNamesGroup = self.imageArr;
    self.scrollView.currentPageDotImage = [UIImage imageNamed:@"轮播-选中"];
    self.scrollView.pageDotImage = [UIImage imageNamed:@"轮播-未选中"];
//    self.scrollView.backgroundColor = [UIColor whiteColor];
    [self.myCollectionView addSubview:self.scrollView];
    
}


- (void)citySelectAction:(UIButton *)btn{
    NSLog(@"城市筛选");
    JYLocationViewController * cityVC = [[JYLocationViewController alloc]init];
    [self presentViewController:cityVC animated:YES completion:nil];
    
}

- (void)searchBtnAction:(UIButton*)btn{
    JYSearchViewController * searchVC = [[JYSearchViewController alloc]init];
    searchVC.searchType = SearchTypeAll;
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark -------------通知-------------

- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_SELECT_CITY);
    JY_ADD_NOTIFICATION(JY_NOTI_LOCATIONCITY);
}

-(void)getNotification:(NSNotification *)noti{
    
    if ([noti.name isEqualToString:JY_SELECT_CITY]) {
        [self updataSelectCityWithCityType:JYCityType_Select];
    }else{
//        if ([noti.name isEqualToString:JY_NOTI_NEWWELFARE]){
//        [self newWelfare];
//    }
//    else{
        [self updataSelectCityWithCityType:JYCityType_Location];
    }
}

#pragma mark -------------网络请求-------------
//请求首页banner
- (void)requestHomeBanner{
    WEAKSELF
    
    [self.homeViewModel requestBannerWithBannerType:BannerType_Home success:^(NSString *msg, id responseData) {
        [weakSelf updataBanner];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//首页分类请求
- (void)requestHomeType{
    WEAKSELF
    [self.homeViewModel requestGetHomeTypeSuccess:^(NSString *msg, id responseData) {
        [weakSelf.myCollectionView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
    [self.myCollectionView.mj_header endRefreshing];
}

//请求城市编码
- (void)requestCityCode{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"cityTitle"] = self.currentCityName;
    [self.homeViewModel requestGetCityCodeWithParams:dic success:^(NSString *msg, id responseData) {
        NSString * cityCode = responseData;
        if ([cityCode isKindOfClass:[NSNull class]]) {
            return ;
        }else{
            [JY_USERDEFAULTS setObject:cityCode forKey:JY_CURRENT_CITYCODE];
            [JY_USERDEFAULTS synchronize];
        }
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark -------------更新数据-------------

- (void)updataBanner{
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYHomeBannerModel * model in self.homeViewModel.bannerArr) {
        [imageArr addObject:model.bannerImage];
    }
    self.scrollView.imageURLStringsGroup = imageArr;
}

#pragma mark -------------collectionView  数据源和代理-------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.homeViewModel.homeTypeArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYHomeCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    cell.topIconImageView.image = [UIImage imageNamed:self.homeTypeImageArr[indexPath.item]];
    cell.model = self.homeViewModel.homeTypeArr[indexPath.item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath: (NSIndexPath *)indexPath{
    if (indexPath.item == 0) {
        //        养车周边
        JYMaintainCarAroundViewController * VC = [[JYMaintainCarAroundViewController alloc] init];
        [self.navigationController pushViewController:VC animated:YES];
    }else if (indexPath.item == 1){
        //        加油页面
        [MobClick event:@"addOil"];
        JYStoreListViewController * VC = [[JYStoreListViewController alloc] init];
        VC.currentListMode = JYCurrentListModeAddOil;
        [self.navigationController pushViewController:VC animated:YES];
    }else if (indexPath.item == 2){
        //        返利商城
        JYRebateStoreController * storeVC = [[JYRebateStoreController alloc] init];
        [self.navigationController pushViewController:storeVC animated:YES];
    }else if (indexPath.item == 3){
        //        活动页面
        JYActivityListViewController * VC = [[JYActivityListViewController alloc] init];
        [self.navigationController pushViewController:VC animated:YES];
        
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = (SCREEN_WIDTH-26) / 2;
    return CGSizeMake(width, width / 175*160);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//这个是两行cell之间的间距（上下行cell的间距）
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}


//两个cell之间的间距（同一行的cell的间距）
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}


#pragma mark ------------SDCycleScrollView 代理--------------

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    JYHomeBannerModel * model = self.homeViewModel.bannerArr[index];
    
    //(1活动详情，2帖子详情，3油站列表，4店铺列表，5油站详情，6店铺详情，7商品列表，8商品详情，9 url)
    
    //   最新的： (1商品 2是油站3 是视频4是资讯5URL)
    switch ([model.bannerType intValue]) {
        case 1:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            detailVC.goodsId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
            
        case 2:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            detailVC.shopId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 3:{
            JYVideoListViewController * videoListVC = [[JYVideoListViewController alloc] init];
            [self.navigationController pushViewController:videoListVC animated:YES];
        }
            break;
        case 4:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            webVC.navTitle = @"资讯详情";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 5:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
            
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc{
    [JY_NOTIFICATION removeObserver:self];
}

#pragma mark -------------lazy-------------

- (JYHomeViewModel *)homeViewModel{
    if (!_homeViewModel) {
        _homeViewModel = [[JYHomeViewModel alloc] init];
    }
    return _homeViewModel;
}

-(UIView *)searchView{
    if (!_searchView) {
        _searchView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.cityBtn.frame) + 15, 25, SCREEN_WIDTH - (CGRectGetMaxX(self.cityBtn.frame)+15+45) , SearchViewHeight)];
    }
    return _searchView;
}




@end
