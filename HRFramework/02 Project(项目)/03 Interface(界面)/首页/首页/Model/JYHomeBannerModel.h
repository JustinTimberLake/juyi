//
//  JYHomeBannerModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYHomeBannerModel : JYBaseModel

@property (nonatomic,copy) NSString *bannerId;
//标题
@property (nonatomic,copy) NSString *bannerTitle;
//图片
@property (nonatomic,copy) NSString *bannerImage;
//(1活动详情，2帖子详情，3油站列表，4店铺列表，5油站详情，6店铺详情，7商品列表，8商品详情，9 url)
@property (nonatomic,copy) NSString *bannerType;
//内容（对应ID或url）
@property (nonatomic,copy) NSString *bannerContent;
@end
