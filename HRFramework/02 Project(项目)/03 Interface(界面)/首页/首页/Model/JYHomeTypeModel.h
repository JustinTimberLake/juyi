//
//  JYHomeTypeModel.h
//  JY
//
//  Created by duanhuifen on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYHomeTypeModel : JYBaseModel

//1养车周边2加油3返利商城4活动
@property (nonatomic,copy) NSString *homeTypeId;
//标题
@property (nonatomic,copy) NSString *homeTitle;
//副标题
@property (nonatomic,copy) NSString *homeSubtitle;

@end
