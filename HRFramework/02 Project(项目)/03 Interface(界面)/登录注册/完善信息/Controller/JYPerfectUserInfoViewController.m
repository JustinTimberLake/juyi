//
//  JYPerfectUserInfoViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPerfectUserInfoViewController.h"
#import "JYRegistViewModel.h"
#import "JYGetCodeViewModel.h"
#import <libkern/OSAtomic.h>
#import "JYBaseTabBarController.h"

@interface JYPerfectUserInfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *tipLab;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;

@property (nonatomic,strong) JYRegistViewModel *registViewModel;
@property (nonatomic,strong) JYGetCodeViewModel *getCodeViewModel;

@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confimViewTopH;
@property (nonatomic,assign) BOOL isAlreadyRegist;



@end

@implementation JYPerfectUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
}

#pragma mark ==================设置UI==================

- (void)setUpUI{
    self.naviTitle = @"完善信息";
    self.confimViewTopH.constant = 110;
    switch (self.thirdType) {
        case JYThirdLoginType_QQ:
            self.tipLab.text = @"QQ登陆成功，请完善信息~";
            break;
        case JYThirdLoginType_Wechat:
            self.tipLab.text = @"微信登陆成功，请完善信息~";
            break;
        case JYThirdLoginType_Sina:
            self.tipLab.text = @"新浪登陆成功，请完善信息~";
            break;
    
        default:
            break;
    }
    
}

//已经注册的账号修改UI界面
- (void)updataUIIfRegisted{
    
    self.codeView.hidden = YES;
    self.passwordView.hidden = YES;
    self.confimViewTopH.constant = 0;
    self.confirmPasswordTextField.placeholder = @"请输入密码";
}

#pragma mark ==================action==================
- (IBAction)enterHomeBtnAction:(UIButton *)sender {
#pragma mark ------测试-------
//    [self turnHome];
//    已经注册的
    if (self.isAlreadyRegist) {
        if (!self.phoneTextField.text.length) {
            [self showSuccessTip:@"请输入手机号"];
            return;
        }
        if (![self.phoneTextField.text checkTelephoneNumber]) {
            [self showSuccessTip:@"请输入正确的手机号"];
            return;
        }
        if (!self.confirmPasswordTextField.text.length) {
            [self showSuccessTip:@"请输入密码"];
            return;
        }
        
        [self requestCheckAccount];
    }else{
        
        if (!self.phoneTextField.text.length) {
            [self showSuccessTip:@"请输入手机号"];
            return;
        }
        if (![self.phoneTextField.text checkTelephoneNumber]) {
            [self showSuccessTip:@"请输入正确的手机号"];
            return;
        }
        if (!self.codeTextField.text.length) {
            [self showSuccessTip:@"请输入验证码"];
            return;
        }
        if (![self.codeTextField.text checkJustNumber]) {
            [self showSuccessTip:@"请输入正确的验证码"];
            return;
        }
        
        if (self.passwordTextField.text.length < 6) {
            [self showSuccessTip:@"密码不应小于6位"];
            return;
        }
        if (![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text] ) {
            [self showSuccessTip:@"确认密码输入有误"];
            return;
        }
        [self requstThirdRegist];
    }
}

- (IBAction)getCodeBtnAction:(UIButton *)sender {
    if (!self.phoneTextField.text.length) {
        [self showSuccessTip:@"请输入手机号"];
        return;
    }
    if (![self.phoneTextField.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    sender.enabled = NO;
    __block int32_t timeOutCount = 60;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1ull * NSEC_PER_SEC, 0);
    dispatch_source_set_cancel_handler(timer, ^{
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
    });
    dispatch_resume(timer);
    
    WEAKSELF
    [self.getCodeViewModel requestGetCodeWithPhoneNumber:self.phoneTextField.text andType:1 success:^(NSString *msg, id responseData) {
        if ([msg isEqualToString:@"手机号已注册"]) {
            [weakSelf showSuccessTip:msg];
            weakSelf.isAlreadyRegist = YES;
            [weakSelf updataUIIfRegisted];
        }else{
            [weakSelf showSuccessTip:@"已发送您的手机，请查收"];
            dispatch_source_set_event_handler(timer, ^{
                OSAtomicDecrement32(&timeOutCount);
                [sender setTitle:SF(@"(%ds)后重试", timeOutCount)  forState:UIControlStateDisabled];
                [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_LINE_HEAD] forState:UIControlStateDisabled];
                if (timeOutCount == 0) {
                    sender.enabled = YES;
                    NSLog(@"timersource cancel");
                    dispatch_source_cancel(timer);
                }
            });

        }
    } failure:^(NSString *errorMsg) {
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ==================网络请求==================

- (void)requstThirdRegist{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"phone"] = self.phoneTextField.text;
    dic[@"password"] = self.confirmPasswordTextField.text;
    dic[@"code"] = self.codeTextField.text;
    dic[@"type"] = @(self.thirdType);
    dic[@"token"] = self.thirdToken;
    [self.registViewModel requestThirdRegistWithParams:dic success:^(NSString *msg, id responseData) {
        NSDictionary * dic = responseData;
        [weakSelf requestWithLoginFinish:dic andIsHide:YES];
        [weakSelf turnHome];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)requestCheckAccount{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"phone"] = self.phoneTextField.text;
    dic[@"password"] = self.confirmPasswordTextField.text;
    dic[@"token"] = self.thirdToken;
    [self.registViewModel requestCheckAccountWithParams:dic success:^(NSString *msg, id responseData) {
        NSDictionary * dic = responseData;
        [weakSelf requestWithLoginFinish:dic andIsHide:YES];
        [weakSelf turnHome];
    } failure:^(NSString *errorMsg) {
         [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark ==================自定义方法==================
//跳转首页
- (void)turnHome{
    WEAKSELF
    [self dismissViewControllerAnimated:YES completion:^{
        JYBaseTabBarController * tabVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        tabVC.selectedIndex = 0;
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
    }];
}
//- (void)loginSuccess
//{
//    [self dismissViewControllerAnimated:YES completion:^{
//        if (self.loginSuccessBlock) {
//            self.loginSuccessBlock();
//        }
//        else
//        {
////            [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_LOGIN_SUCCESS object:nil];
////            ZKBaseTabBarController *tabVC = (ZKBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
////            tabVC.selectedIndex = 0;
//        }
//    }];
//}

-(void)requestWithLoginFinish:(NSDictionary *)dic andIsHide:(BOOL)isHide{
    [User_InfoShared shareUserInfo].c = [dic objectForKey:@"C"];
    [User_InfoShared shareUserInfo].personalModel = [JYPersonInfoModel mj_objectWithKeyValues:dic];
    
    JY_POST_NOTIFICATION(JY_NOTI_LOGIN_SUCCESS, nil);
    
    if (isHide) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (JYRegistViewModel *)registViewModel
{
    if (!_registViewModel) {
        _registViewModel = [[JYRegistViewModel alloc] init];
    }
    return _registViewModel;
}

- (JYGetCodeViewModel *)getCodeViewModel
{
    if (!_getCodeViewModel) {
        _getCodeViewModel = [[JYGetCodeViewModel alloc] init];
    }
    return _getCodeViewModel;
}

@end
