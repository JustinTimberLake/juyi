//
//  User_InfoShared.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "User_InfoShared.h"
#import "JYBaseNatigationViewController.h"
#import "JYRegistViewController.h"
static User_InfoShared *instance;

@implementation User_InfoShared

+(instancetype)shareUserInfo{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[super allocWithZone:NULL]init];
    });
    return instance;
}
+(instancetype)allocWithZone:(struct _NSZone *)zone{
    return [[self class] shareUserInfo];
}
-(instancetype)copyWithZone:(struct _NSZone *)zone{
    return [[self class] shareUserInfo];
}

//清理数据
-(void)clearData{
    self.c = @"";
    self.personalModel = nil;
}
//判断登录
-(BOOL)isLoginWithViewController:(JYBaseViewController *)baseVC LoginC:(void (^)(NSString *))loginC{
    if (self.c && self.c.length) {
        if (loginC) {
            loginC(self.c);
        }
        return YES;
    }else{
#pragma mark --------------应该是跳转注册页面手动点击登录页--------
        if (baseVC) {
//            JYRegistViewController * registVC = [[JYRegistViewController alloc] init];
            JYLoginViewController * loginVC = [[JYLoginViewController alloc] init];
            JYBaseNatigationViewController * navVC  = [[JYBaseNatigationViewController alloc] initWithRootViewController:loginVC];
            [baseVC presentViewController:navVC animated:YES completion:nil];
            
////            下面的暂时注释
//            JYLoginViewController *loginVC = [[JYLoginViewController alloc]init];
//            [loginVC loginSuccessWithBlock:^(NSString *c) {
//                loginC(c);
//            }];
//            JYBaseNatigationViewController *navC = [[JYBaseNatigationViewController alloc]initWithRootViewController:loginVC];
//            [baseVC presentViewController:navC animated:YES completion:nil];
            
        }
        return NO;
    }
}

#pragma mark ----------------------归档和解档---------------------------
-(void)setC:(NSString *)c{
    [CZArchiverOrUnArchiver saveArchiverData:c fileName:LOGIN_C];
}
-(NSString *)c{
    return [CZArchiverOrUnArchiver loadArchiverDataFileName:LOGIN_C];
}

-(void)setCustomerTel:(NSString *)customerTel{
    [CZArchiverOrUnArchiver saveArchiverData:customerTel fileName:CUSTOMER_TEL];
}
-(NSString *)customerTel{
    return [CZArchiverOrUnArchiver loadArchiverDataFileName:CUSTOMER_TEL];
}

-(void)setPersonalModel:(JYPersonInfoModel *)personalModel{
    [CZArchiverOrUnArchiver saveArchiverData:personalModel fileName:LOGIN_PERSONAL];
}
-(JYPersonInfoModel *)personalModel{
    return [CZArchiverOrUnArchiver loadArchiverDataFileName:LOGIN_PERSONAL];
}

@end
