//
//  JYPersonInfoModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYPersonInfoModel : JYBaseModel

//用户id
@property (nonatomic,copy) NSString *userId;
//昵称
@property (nonatomic,copy) NSString *userNick;
//头像
@property (nonatomic,copy) NSString *userHead;
//手机号
@property (nonatomic,copy) NSString *userPhone;
//性别 1男 2女
@property (nonatomic,copy) NSString *userSex;
//生日
@property (nonatomic,copy) NSString *userBirthday;
//城市
@property (nonatomic,copy) NSString *userCity;
//城市编号
@property (nonatomic,copy) NSString *cityCode;
//环信账号
@property (nonatomic,copy) NSString *hxUser;
//环信密码
@property (nonatomic,copy) NSString *hxPwd;
//账户余额
@property (nonatomic,copy) NSString *userRemainder;
//我的积分
@property (nonatomic,copy) NSString *userScore;
//积分和人民币兑换比例(返回int  多少积分等于一元)
@property (nonatomic,copy) NSString *userRatio;

#pragma mark ------------下面的是哪儿来的--------------------
///** c:登录标识*/
//@property (nonatomic, copy) NSString *c;
///** Description:头像 */
//@property (copy, nonatomic) NSString *thumb;
///** Description:昵称 */
//@property (copy, nonatomic) NSString *nickName;
///** Description:性别” 1.男 2.女,默认 */
//@property (copy, nonatomic) NSString *gender;
///** Description:所在省名称 */
//@property (copy, nonatomic) NSString *provinceName;
///** Description:所在市名称 */
//@property (copy, nonatomic) NSString *cityName;
///** Description:生日 */
//@property (copy, nonatomic) NSString *birthday;
///** Description:电话号码 */
//@property (copy, nonatomic) NSString *mobile;
///** Description:推送开关" 0.关闭 1.开启（默认） */
//@property (copy, nonatomic) NSString *isPush;
///** Description:未读消息个数 */
//@property (copy, nonatomic) NSString *messageNo;
///** areaId 所在区id*/
//@property (copy, nonatomic) NSString *areaId;
///** areaName 所在区名称*/
//@property (copy, nonatomic) NSString *areaName;
///** address 详情地址*/
//@property (copy, nonatomic) NSString *address;
///** address 三方授权信息 - QQ */
//@property (copy, nonatomic) NSString *qqAuthorize;
///** address 三方授权信息 - 微信 */
//@property (copy, nonatomic) NSString *wxAuthorize;
///** address 三方授权信息 - 微博 */
//@property (copy, nonatomic) NSString *wbAuthorize;
///** address 三方授权信息 - 支付宝 */
//@property (copy, nonatomic) NSString *alipayAuthorize;
///** Description:用户ID */
//@property (copy, nonatomic) NSString *userId;

@end
