//
//  User_InfoShared.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JYPersonInfoModel.h"
#import "JYBaseViewController.h"
#import "JYLoginViewController.h"
@interface User_InfoShared : NSObject
//登录标识
@property (nonatomic, strong) NSString *c;
/** Description:网络状态 */

/** Description:个人信息 */
@property (strong, nonatomic) JYPersonInfoModel *personalModel;
/** customerTel:电话号码*/
@property (nonatomic, copy) NSString *customerTel;
//单例
+(instancetype)shareUserInfo;
//判断登录
-(BOOL)isLoginWithViewController:(JYBaseViewController *)baseVC LoginC:(void(^)(NSString *c))loginC;
//清理数据
-(void)clearData;

@end
