//
//  JYLoginViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYLoginViewController.h"
#import "JYBaseTabBarController.h"
#import "JYLogionViewModel.h"
#import "JYForgetPasswordViewController.h"
#import "JYPerfectUserInfoViewController.h"
#import "JYRegistViewController.h"
#import <WXApi.h>
#import <WeiboSDK.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "IQKeyboardManager.h"


@interface JYLoginViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *thirdLoginview;

@property (weak, nonatomic) IBOutlet UITextField *phoneNumTF;
@property (weak, nonatomic) IBOutlet UITextField *passWordTF;
@property (strong, nonatomic) JYLogionViewModel * viewModel;
@property (nonatomic,assign)UMSocialPlatformType platformType;
@property (nonatomic,assign) JYThirdLoginType thirdLoginType;
@property (nonatomic,copy) NSString *thirdToken;
@property (weak, nonatomic) IBOutlet UIButton *wxBtn;
@property (weak, nonatomic) IBOutlet UIButton *qqBtn;
@property (weak, nonatomic) IBOutlet UIButton *sinaBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qqCenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wxRightW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sinaLeftW;
@property (nonatomic,copy) void(^loginSuccessBlock)();


//@property (nonatomic,strong) NSDictionary *loginDic; //测试用
@end

@implementation JYLoginViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self networkRequest];
//    [self configThirdLoginUI];
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //修改navigation的背景色
//
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
//    //返回按钮
//    [self leftImageItem:@"返回箭头" action:^{
//        NSString *isBackStr = [CZArchiverOrUnArchiver loadArchiverDataFileName:LOGIN_BACK];
//        if ([isBackStr boolValue]) {
//            JYBaseTabBarController *baseTabVC = (JYBaseTabBarController*)APPLICATION.keyWindow.rootViewController;
//            baseTabVC.selectedIndex = 0;
//        }
//        [self dismissViewControllerAnimated:NO completion:^{
//
//        }];
//    }];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

- (void)backAction{
    
    [self dismissViewControllerAnimated:YES completion:nil];
//    JYBaseTabBarController * tabBarVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//    tabBarVC.selectedIndex = 0;
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)configThirdLoginUI{
    
    if (![QQApiInterface isQQInstalled]) {
        NSLog(@"没安装qq或qq空间");
        self.qqBtn.hidden = YES;
    }
    
    if (![WeiboSDK isWeiboAppInstalled]) {
          NSLog(@"没安装微博");
        self.sinaBtn.hidden = YES;
    }
    
    if (![WXApi isWXAppInstalled]) {
        NSLog(@"没安装微信");
        self.wxBtn.hidden = YES;
    }
    if (![WXApi isWXAppInstalled]&&![WeiboSDK isWeiboAppInstalled]&&![QQApiInterface isQQInstalled]) {
        self.thirdLoginview.hidden = YES;
    }
    
    if (![WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled] && [WeiboSDK isWeiboAppInstalled]) {
        self.qqCenterX.constant = -30;
    }
    if ([WXApi isWXAppInstalled] && [QQApiInterface isQQInstalled] && ![WeiboSDK isWeiboAppInstalled]) {
         self.qqCenterX.constant = 30;
    }
    if ([WXApi isWXAppInstalled] && ![QQApiInterface isQQInstalled] && [WeiboSDK isWeiboAppInstalled]) {
        self.wxRightW.constant = 0;
        self.sinaLeftW.constant = 0;
    }
    
    
}


#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)
#pragma mark config control（布局控件）
-(void)configUI{
    WEAKSELF
    self.naviTitle = @"登录";
    [self rightItemTitle:@"注册" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:FONT(15) action:^{
        [weakSelf turnRegistVC];
    }];
    
    self.phoneNumTF.delegate = self;
    [self.phoneNumTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.view.backgroundColor = [UIColor colorWithHex:0xffffff];
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginBack:) name:LOGIN_BACK object:nil];
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
}
- (void)textFieldChange:(UITextField *)textField{
    if (textField.text.length > 11) {
        [self showSuccessTip:@"手机号不能超过11位"];
        textField.text = [textField.text substringToIndex:11];
    }
}

- (void)turnRegistVC{
    JYRegistViewController * registVC = [[JYRegistViewController alloc] init];
    [self.navigationController pushViewController:registVC animated:YES];
}

#pragma mark networkRequest (网络请求)
-(void)networkRequest{
    
}

- (void)requestThirdLoginWithType:(UMSocialPlatformType)type Token:(NSString *)token{

    WEAKSELF
    switch (type) {
        case UMSocialPlatformType_WechatSession:
            self.thirdLoginType = JYThirdLoginType_Wechat;
            break;
        case UMSocialPlatformType_QQ:
            self.thirdLoginType = JYThirdLoginType_QQ;
            break;
        case UMSocialPlatformType_Sina:
            self.thirdLoginType = JYThirdLoginType_Sina;
            break;
        default:
            break;
    }
//    [weakSelf showHUD];
     [self.viewModel requestThirdLoginWithType:self.thirdLoginType andToken:token success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
//        if (![responseData isKindOfClass:]) {
            NSString * status = responseData[@"status"];
            if ([status intValue] == 2) {
                [weakSelf turnPerfectInfoPageWithType:self.thirdLoginType];
            }else{
                [weakSelf requestWithLoginFinish:responseData andIsHide:YES];
            }
//        }
    } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
    }];
}


//-(void)loginBack:(NSNotification *)notification{
//    NSLog(@"接受到的消息");
//}

//登录
-(void)requestWithLoginSuccess{
    WEAK(weakSelf);
    [self showHUD];
    [self.viewModel requestLogionWithParams:loginParam(self.phoneNumTF.text, self.passWordTF.text) success:^(NSString *msg, id responseData) {
        [weakSelf requestWithLoginFinish:responseData andIsHide:YES];
        if (weakSelf.successLoginBlock) {
            weakSelf.successLoginBlock([responseData objectForKey:@"C"]);
        }
        [self hideHUD];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
   
}

-(void)requestWithThreeLogin{
//    WEAK(weakSelf);
//    [[HRRequest manager]POST:URL_PATH_THIRDLOGIN para:self.threeParam success:^(id data) {
//        if ([data[@"isRegistered"] integerValue] == 1) {
//            //注册
//            self.threeLoginView = [[HQYM_ThreeLoginView alloc]init];
//            self.threeLoginView.registerBlock = ^{
//                HQYMRegisterViewController *registerVC = SB_ID(@"HQYM_LoginAndRegister", @"registerVC");
//                [registerVC registerSuccessWithBlock:^(NSDictionary *dic) {
//                    [weakSelf requestWithLoginFinish:dic];
//                }];
//                [weakSelf.navigationController pushViewController:registerVC animated:YES];
//            };
//            self.threeLoginView.backgroundColor = RGBColor(0, 0, 0, .7);
//            self.threeLoginView.frame = SCREEN_BOUNDS;
//            [APP_DELEGATE.window addSubview:self.threeLoginView];
//        }else{
//            [weakSelf requestWithLoginFinish:data];
//        }
//    } faiulre:^(NSString *errMsg) {
//        [APP_DELEGATE disPlayHint:errMsg];
//    }];
}

-(void)requestWithLoginFinish:(NSDictionary *)dic andIsHide:(BOOL)isHide{
    [User_InfoShared shareUserInfo].c = [dic objectForKey:@"C"];
    [User_InfoShared shareUserInfo].personalModel = [JYPersonInfoModel mj_objectWithKeyValues:dic];
    [JY_USERDEFAULTS setObject:[User_InfoShared shareUserInfo].personalModel.userHead forKey:USER_HEADEIMAGE];
     [JY_USERDEFAULTS setObject:[User_InfoShared shareUserInfo].personalModel.userNick forKey:USER_NICKNAME];
    [JY_USERDEFAULTS synchronize];
    
    
    if (self.successLoginBlock) {
        self.successLoginBlock([dic objectForKey:@"C"]);
    }
    
    JY_POST_NOTIFICATION(JY_NOTI_LOGIN_SUCCESS, nil);

//    JY_POST_NOTIFICATION(JY_NOTI_NEWWELFARE, nil);
    //添加个推
//    [UMessage setAlias:[HQYM_UserInfo shareUserInfo].personalModel.userId type:@"IOS" response:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
//        
//    }];
//
    if (isHide) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}


//-(NSString *)statistical{
//    return @"45";
//}
#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

- (IBAction)loginBtn:(id)sender {
    if (!self.phoneNumTF.text.length) {
        [self showSuccessTip:@"手机号不能为空"];
        return;
    }
    
    if (![self.phoneNumTF.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    
    if (!self.passWordTF.text.length) {
        [self showSuccessTip:@"密码不能为空"];
        return;
    }
   
    [self requestWithLoginSuccess];
    
}

- (IBAction)forgetPwdBtn:(id)sender {
    JYForgetPasswordViewController *forgetVC = [[JYForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgetVC animated:YES];
}




//第三方登录
- (IBAction)thirdPartyWithLoginAction:(UIButton *)sender {
    switch (sender.tag) {
        case 10001:{
            [MobClick event:@"wechatLogin"];
//            [MobClick event:@"wechatLogin" attributes:nil counter:2];
            self.platformType = UMSocialPlatformType_WechatSession;
//            [self sendAuthRequest];
        }
            break;
        case 10002:
            self.platformType = UMSocialPlatformType_QQ;
         [MobClick event:@"qqLogin"];
            break;
        case 10003:
        [MobClick event:@"sianLogin"];
            self.platformType = UMSocialPlatformType_Sina;
            break;
   
        default:
            break;
    }
#pragma mark -----测试------
//    [self turnPerfectInfoPageWithType:self.thirdLoginType];
    
    [self getUserInfoForPlatform:self.platformType];
}
#pragma mark ----这个是高云鹏的
//    switch (sender.tag) {
//        case 10001:
//        {
//            [[MDUmClient sharedInstance] umSSOGetAuthWithUserInfoForPlatformType:HRUmSSOPlatformTypeForWeChat handler:^(UMSocialUserInfoResponse *resp) {
//                self.threeParam = threeLoginParam(resp.openid,@"2");
//                [self requestWithThreeLogin];
//            } fail:^(id error) {
//                NSLog(@"123:%@",[error userInfo][@"message"]);
//                [APP_DELEGATE disPlayHint:[error userInfo][@"message"]];
//            }];
//            
//        }
//            break;
//        case 10002:
//        {
//            [[MDUmClient sharedInstance] umSSOGetAuthWithUserInfoForPlatformType:HRUmSSOPlatformTypeForQQ handler:^(UMSocialUserInfoResponse *resp) {
//                self.threeParam = threeLoginParam(resp.openid,@"1");
//                [self requestWithThreeLogin];
//            } fail:^(id error) {
//                [APP_DELEGATE disPlayHint:[error userInfo][@"message"]];
//            }];
//        }
//            break;
//        case 10003:
//        {
//            [[MDUmClient sharedInstance] umSSOGetAuthWithUserInfoForPlatformType:HRUmSSOPlatformTypeForSina handler:^(UMSocialUserInfoResponse *resp) {
//                self.threeParam = threeLoginParam(resp.openid,@"3");
//                [self requestWithThreeLogin];
//            } fail:^(id error) {
//                [APP_DELEGATE disPlayHint:[error userInfo][@"message"]];
//            }];
//        }
//            break;
//        case 10004:
//        {
//            [[HRRequest manager]POST:URL_LOGIN_ALIPAY para:nil success:^(id data) {
//                [[MDUmClient sharedInstance]alipayAuth:data fromScheme:USHARE_ALIPAY_SCHEMES callBack:^(NSDictionary *resultDic) {
//                    NSLog(@"%@",resultDic);
//                    NSString *resultStr = [NSString stringWithFormat:@"%@",resultDic[@"result"]];
//                    NSArray *reslutArr = [resultStr componentsSeparatedByString:@"&"];
//                    for (int i=0; i<reslutArr.count; i++) {
//                        NSString *contentStr = reslutArr[i];
//                        if ([contentStr hasPrefix:@"alipay_open_id"]) {
//                            NSArray *openIDArr = [contentStr componentsSeparatedByString:@"="];
//                            self.threeParam = threeLoginParam(openIDArr.lastObject,@"4");
//                            [self requestWithThreeLogin];
//                        }
//                    }
//                }];
//            } faiulre:^(NSString *errMsg) {
//                [APP_DELEGATE disPlayHint:errMsg];
//            }];
//        }
//            break;
//    }


//获取第三方登陆信息
- (void)getUserInfoForPlatform:(UMSocialPlatformType)platformType
{
  
    
    WEAKSELF
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:platformType currentViewController:self completion:^(id result, NSError *error) {
        if (error) {
            [weakSelf hideHUD];
             NSLog(@"%@",result);
        }else{
            UMSocialUserInfoResponse *resp = result;
            
            // 第三方登录数据(为空表示平台未提供)
            // 授权数据
            NSLog(@" uid: %@", resp.uid);
            NSLog(@" openid: %@", resp.openid);
            NSLog(@" accessToken: %@", resp.accessToken);
            NSLog(@" refreshToken: %@", resp.refreshToken);
            NSLog(@" expiration: %@", resp.expiration);
            
            // 用户数据
            NSLog(@" name: %@", resp.name);
            NSLog(@" iconurl: %@", resp.iconurl);
            NSLog(@" gender: %@", resp.gender);
            
            // 第三方平台SDK原始数据
            NSLog(@" originalResponse: %@", resp.originalResponse);
            
            weakSelf.thirdToken = resp.usid;
            

            
            
            [weakSelf requestThirdLoginWithType:platformType Token:weakSelf.thirdToken];
        }
    }];
    [weakSelf showHUD];
}


#pragma mark - ---------- Public Methods（公有方法） ----------
- (void)turnPerfectInfoPageWithType:(JYThirdLoginType)type{
    
    JYPerfectUserInfoViewController * perfectVC = [[JYPerfectUserInfoViewController alloc] init];
    perfectVC.thirdType = type;
    perfectVC.thirdToken = self.thirdToken;
    [self.navigationController pushViewController:perfectVC animated:NO];
}
#pragma mark self declare （本类声明）

#pragma mark ==================微信网页登录==================

-(void)sendAuthRequest
{
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo,snsapi_base";
    req.state = @"0744" ;
    [WXApi sendReq:req];
}

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
-(void)loginSuccessWithBlock:(SuccessLoginBlock)block{
    self.successLoginBlock = block;
}
#pragma mark ---------------懒加载--------------
- (JYLogionViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYLogionViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - 构建参数

NSDictionary *loginParam(NSString *account, NSString *pwd) {
    return @{@"phone":account?:@"", @"password":pwd?:@""};
}

NSDictionary *threeLoginParam(NSString *authorizeStr,NSString *typeStr){
    return @{@"authorize":authorizeStr,@"type":typeStr,@"isBind":@"1",@"c":@""};
}


@end
