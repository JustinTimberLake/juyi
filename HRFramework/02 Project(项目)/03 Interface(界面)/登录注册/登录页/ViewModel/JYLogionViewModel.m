//
//  JYLogionViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYLogionViewModel.h"

@implementation JYLogionViewModel

- (void)requestLogionWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_LOGION));
    [manager POST_URL:JY_PATH(JY_LOGION) params:params success:^(id result) {
        
        NSLog(@"%@",RESULT_DATA);
        
        if (RESULT_SUCCESS) {
            NSString * data = result[@"data"];
            successBlock(RESULT_MESSAGE,data);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//第三方登录
- (void)requestThirdLoginWithType:(JYThirdLoginType)thirdType andToken:(NSString *)token success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{

    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"type"] = @(thirdType);
    dic[@"token"] = token;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_LOGION_ThreeLogin));
    [manager POST_URL:JY_PATH(JY_LOGION_ThreeLogin) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
//            NSString * status = RESULT_DATA[@"status"];
            successBlock(RESULT_MESSAGE,RESULT_DATA);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
@end
