//
//  JYLogionViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYPersonInfoModel.h"

typedef NS_ENUM(NSInteger,JYThirdLoginType) {
    JYThirdLoginType_QQ = 1,
    JYThirdLoginType_Wechat,
    JYThirdLoginType_Sina
};



@interface JYLogionViewModel : JYBaseViewModel

//正常登录
- (void)requestLogionWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//第三方登录
- (void)requestThirdLoginWithType:(JYThirdLoginType)thirdType andToken:(NSString *)token success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
