//
//  JYForgetPasswordViewModel.m
//  JY
//
//  Created by risenb on 2017/9/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYForgetPasswordViewModel.h"

@implementation JYForgetPasswordViewModel
//忘记密码接口 修改密码接口
- (void)requestForgetPasswordWithParams:(NSMutableDictionary *)params andType:(NSInteger)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSString * url;
    if (type == 0) {
        url = JY_PATH(JY_LOGION_ForgetPwd);
    }else{
        url = JY_PATH(JY_LOGION_ChangePwd);
    }
    [manager POST_URL:url params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
@end
