//
//  JYForgetPasswordViewModel.h
//  JY
//
//  Created by risenb on 2017/9/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYForgetPasswordViewModel : JYBaseViewModel

//忘记密码接口  修改密码接口  type 0 忘记密码，1 修改密码
- (void)requestForgetPasswordWithParams:(NSMutableDictionary *)params andType:(NSInteger)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
