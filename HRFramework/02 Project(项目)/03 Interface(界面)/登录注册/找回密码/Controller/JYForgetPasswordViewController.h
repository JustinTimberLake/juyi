//
//  JYForgetPasswordViewController.h
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

//页面类型
typedef NS_ENUM(NSInteger,JYPwdType) {
    JYPwdType_ForgetPwd, //忘记密码
    JYPwdType_ChangePwd  //修改密码
};

@interface JYForgetPasswordViewController : JYBaseViewController

@property (nonatomic,assign) JYPwdType pwdType;
@end
