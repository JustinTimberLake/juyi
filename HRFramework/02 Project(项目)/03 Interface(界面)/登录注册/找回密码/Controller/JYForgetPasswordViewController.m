//
//  JYForgetPasswordViewController.m
//  JY
//
//  Created by duanhuifen on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYForgetPasswordViewController.h"
#import "JYGetCodeViewModel.h"
#import <libkern/OSAtomic.h>
#import "JYForgetPasswordViewModel.h"

@interface JYForgetPasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (nonatomic,strong) JYGetCodeViewModel *getCodeViewModel;
@property (nonatomic,strong) JYForgetPasswordViewModel *forgetPasswordViewModel;

@end

@implementation JYForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ==================设置UI==================
- (void)setUpUI{
    if (self.pwdType == JYPwdType_ChangePwd) {
        self.naviTitle = @"修改密码";
        self.phoneTextField.text = [User_InfoShared shareUserInfo].personalModel.userPhone;
    }else{
        self.naviTitle = @"找回密码";
    }
    self.view.backgroundColor = [UIColor colorWithHex:0xffffff];
}

#pragma mark ==================action==================
- (IBAction)sureBtnAction:(UIButton *)sender {
    if (!self.phoneTextField.text.length) {
        [self showSuccessTip:@"请输入手机号"];
        return;
    }
    if (![self.phoneTextField.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    if (!self.codeTextField.text.length) {
        [self showSuccessTip:@"请输入验证码"];
        return;
    }
    if (![self.codeTextField.text checkJustNumber]) {
        [self showSuccessTip:@"请输入正确的验证码"];
        return;
    }
    
    if (self.passwordTextField.text.length < 6) {
        [self showSuccessTip:@"密码不应小于6位"];
        return;
    }
    if (self.passwordTextField.text.length > 16) {
        [self showSuccessTip:@"密码不应大于16位"];
        return;
    }
    
    if (![self.passwordTextField.text checkEnglishAndNum]) {
        [self showSuccessTip:@"密码为字母和数字组合"];
        return;
    }

    if (![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text] ) {
        [self showSuccessTip:@"确认密码输入有误"];
        return;
    }
    [self requestChangePasswordWithType:self.pwdType];
}

- (IBAction)sendCodeBtnAction:(UIButton *)sender {
    if (!self.phoneTextField.text.length) {
        [self showSuccessTip:@"请输入手机号"];
        return;
    }
    if (![self.phoneTextField.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    sender.enabled = NO;
    __block int32_t timeOutCount = 60;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1ull * NSEC_PER_SEC, 0);
    dispatch_source_set_cancel_handler(timer, ^{
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
    });
    dispatch_resume(timer);
    
    WEAKSELF
    [self.getCodeViewModel requestGetCodeWithPhoneNumber:self.phoneTextField.text andType:2 success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"已发送您的手机，请查收"];
        dispatch_source_set_event_handler(timer, ^{
            OSAtomicDecrement32(&timeOutCount);
            [sender setTitle:SF(@"(%ds)后重试", timeOutCount)  forState:UIControlStateDisabled];
            [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_LINE_HEAD] forState:UIControlStateDisabled];
            if (timeOutCount == 0) {
                sender.enabled = YES;
                NSLog(@"timersource cancel");
                dispatch_source_cancel(timer);
            }
        });
        
    } failure:^(NSString *errorMsg) {
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
        [weakSelf showSuccessTip:errorMsg];
    }];
    
}


#pragma mark ==================网络请求==================

- (void)requestChangePasswordWithType:(JYPwdType)type{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"phone"] = self.phoneTextField.text;
    dic[@"password"] = self.passwordTextField.text;
    dic[@"code"] = self.codeTextField.text;
    [self.forgetPasswordViewModel requestForgetPasswordWithParams:dic  andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"修改密码成功"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ==================懒加载==================
- (JYGetCodeViewModel *)getCodeViewModel
{
    if (!_getCodeViewModel) {
        _getCodeViewModel = [[JYGetCodeViewModel alloc] init];
    }
    return _getCodeViewModel;
}

- (JYForgetPasswordViewModel *)forgetPasswordViewModel
{
    if (!_forgetPasswordViewModel) {
        _forgetPasswordViewModel = [[JYForgetPasswordViewModel alloc] init];
    }
    return _forgetPasswordViewModel;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
