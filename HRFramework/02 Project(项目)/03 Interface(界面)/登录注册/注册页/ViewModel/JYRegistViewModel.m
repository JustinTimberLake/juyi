//
//  JYRegistViewModel.m
//  JY
//
//  Created by risenb on 2017/9/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRegistViewModel.h"

@implementation JYRegistViewModel
//注册请求
- (void)requestRegistWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOGION_Regist) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//完善信息，第三方注册
- (void)requestThirdRegistWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_LOGION_ThreeRegist));
    [manager POST_URL:JY_PATH(JY_LOGION_ThreeRegist) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,RESULT_DATA);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//绑定手机号（校验手机号密码）
- (void)requestCheckAccountWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_LOGION_CheckAccount));
    [manager POST_URL:JY_PATH(JY_LOGION_CheckAccount) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,RESULT_DATA);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
