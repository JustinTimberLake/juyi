//
//  JYRegistViewModel.h
//  JY
//
//  Created by risenb on 2017/9/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYRegistViewModel : JYBaseViewModel

//注册请求
- (void)requestRegistWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//完善信息，第三方注册
- (void)requestThirdRegistWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//绑定手机号（校验手机号密码）
- (void)requestCheckAccountWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
