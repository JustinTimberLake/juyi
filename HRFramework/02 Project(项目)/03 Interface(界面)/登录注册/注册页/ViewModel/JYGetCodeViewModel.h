//
//  JYGetCodeViewModel.h
//  JY
//
//  Created by risenb on 2017/9/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYGetCodeViewModel : JYBaseViewModel

//1注册 2，忘记密码 3油卡绑定
//获取验证码接口
- (void)requestGetCodeWithPhoneNumber:(NSString *)phoneNum andType:(int)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
