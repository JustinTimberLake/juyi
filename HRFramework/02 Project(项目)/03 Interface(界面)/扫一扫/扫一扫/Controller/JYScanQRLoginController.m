//
//  JYScanQRLoginController.m
//  JY
//
//  Created by Justin on 2018/6/6.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import "JYScanQRLoginController.h"
#import "JYScanQRModel.h"
@interface JYScanQRLoginController ()

@end

@implementation JYScanQRLoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}
- (IBAction)ConfirmLoginClick:(UIButton *)sender {
    if (!self.random.length) {
        return;
    }
    [[[JYScanQRModel alloc]init]requestScanLoginWith:self.random Success:^(NSString *msg, id responseData) {
        [self showSuccessTip:@"登录成功"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:@"登录失败"];
    }];
}

- (IBAction)dismiss:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
