//
//  JYScanQRViewController.h
//  JY
//
//  Created by Stronger_WM on 2017/6/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//  已废弃 改为MMScanViewController

#import "JYBaseViewController.h"
//#import <ZXingObjC/ZXingObjC.h>
#import "ZXingObjC.h"

@interface JYScanQRViewController : JYBaseViewController<ZXCaptureDelegate>
{
    bool bScanState;
}

@property (nonatomic,assign) BOOL isPush;
@end

@interface ScanView : UIView
- (void)setToAnimation;
@end

