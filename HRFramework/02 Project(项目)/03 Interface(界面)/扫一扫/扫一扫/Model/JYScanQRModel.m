//
//  JYScanQRModel.m
//  JY
//
//  Created by Justin on 2018/5/22.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import "JYScanQRModel.h"

@implementation JYScanQRModel
//违章高发地请求
- (void)requestScanLoginWith:(NSString *)random Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"random"] = random;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_CONTACT_Checkimg params:dic success:^(id result) {
        NSLog(@"%@",result);
        if ([result[@"code"] integerValue] == 200) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
        
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
@end
