//
//  JYScanQRModel.h
//  JY
//
//  Created by Justin on 2018/5/22.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYScanQRModel : NSObject
//违章高发地请求
- (void)requestScanLoginWith:(NSString *)random Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
