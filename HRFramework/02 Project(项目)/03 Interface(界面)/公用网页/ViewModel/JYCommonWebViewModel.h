//
//  JYCommonWebViewModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

typedef NS_ENUM(NSInteger,JYWebViewType) {
    JYWebViewType_UserDelegate = 1, //1用户协议
    JYWebViewType_GetScore,//2获得积分
    JYWebViewType_RechargeOilDelegate, //3充值油库协议
    JYWebViewType_Help,//4帮助
    JYWebViewType_AboutUs,//5关于我们
    JYWebViewType_RechargeOilCarDelegate//6油卡充值协议
};

@interface JYCommonWebViewModel : JYBaseViewModel

//3.20.1.	JY-027-001 获取h5内容页URL
- (void)requestGetContentUrlWithType:(JYWebViewType)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
