//
//  JYCommonWebViewModel.m
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommonWebViewModel.h"

@implementation JYCommonWebViewModel
//3.20.1.	JY-027-001 获取h5内容页URL
- (void)requestGetContentUrlWithType:(JYWebViewType)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(type);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_CONTENTURL_GetContentUrl) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSString * contentUrl = RESULT_DATA[@"contentUrl"];
            successBlock(RESULT_MESSAGE,contentUrl);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
