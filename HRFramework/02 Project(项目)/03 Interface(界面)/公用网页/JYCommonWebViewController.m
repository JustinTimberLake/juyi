//
//  JYCommonWebViewController.m
//  JY
//
//  Created by risenb on 2017/8/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommonWebViewController.h"

@interface JYCommonWebViewController ()<UIWebViewDelegate>
@property (nonatomic,strong) UIWebView *webView;
@property (nonatomic,strong) JYCommonWebViewModel *webViewModel;
@end

@implementation JYCommonWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.navTitle;
    self.webView = [[UIWebView alloc] init];
    [self.webView scalesPageToFit];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
//    去掉灰色背景
    self.webView.backgroundColor=[UIColor whiteColor];
    for (UIView *_aView in [self.webView subviews])
    {
        if ([_aView isKindOfClass:[UIScrollView class]])
        {
            [(UIScrollView *)_aView setShowsVerticalScrollIndicator:NO];
            //右侧的滚动条
            
            [(UIScrollView *)_aView setShowsHorizontalScrollIndicator:NO];
            //下侧的滚动条
            
            for (UIView *_inScrollview in _aView.subviews)
            {
                if ([_inScrollview isKindOfClass:[UIImageView class]])
                {
                    _inScrollview.hidden = YES;  //上下滚动出边界时的黑色的图片
                }
            }
        }
    }

    
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsZero);
    }];

    
    if (self.url.length) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    }else{
        [self requestNetUrl];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isHideNav) {
        [self.navigationController setNavigationBarHidden:YES animated:animated];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.isHideNav) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ==================网络请求==================
- (void)requestNetUrl{
    WEAKSELF
    [self.webViewModel requestGetContentUrlWithType:self.webViewType success:^(NSString *msg, id responseData) {
        NSString * urlStr = responseData;
        
        if (urlStr.length) {
            [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]]];
        }
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *URLstr = request.URL.absoluteString;
    if ([URLstr hasPrefix:@"jyios://"]) {
        [self turnPrevriewPage];
     
       return NO;
    }
    NSLog(@"url :%@",URLstr);
    return YES;
}

- (void)turnPrevriewPage{
    [self.navigationController popViewControllerAnimated:YES];
}


- (JYCommonWebViewModel *)webViewModel
{
    if (!_webViewModel) {
        _webViewModel = [[JYCommonWebViewModel alloc] init];
    }
    return _webViewModel;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
