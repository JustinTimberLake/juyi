//
//  JYCommonWebViewController.h
//  JY
//
//  Created by risenb on 2017/8/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"
#import "JYCommonWebViewModel.h"

@interface JYCommonWebViewController : JYBaseViewController
@property (nonatomic,copy) NSString *navTitle;
//如果是像协议之类的不需要使用url, 使用webViewType
@property (nonatomic,copy) NSString *url;

@property (nonatomic,assign) JYWebViewType webViewType;

//是否显示导航
@property (nonatomic,assign,getter=isHideNav) BOOL hideNav;


@end
