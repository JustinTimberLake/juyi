//
//  CZGuideViewController.h
//  CZW
//
//  Created by 王凯 on 16/8/31.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYBaseViewController.h"

typedef void(^ChangeRootViewCoutroller) ();

@interface CZGuideViewController : JYBaseViewController

@property (copy, nonatomic) ChangeRootViewCoutroller changeRootViewCoutroller;

- (void)changeRootViewCoutroller:(ChangeRootViewCoutroller)changeRootViewCoutroller;

@end
