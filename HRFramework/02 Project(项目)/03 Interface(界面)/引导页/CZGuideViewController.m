//
//  CZGuideViewController.m
//  CZW
//
//  Created by 王凯 on 16/8/31.
//  Copyright © 2016年 MAC. All rights reserved.
//


#define SCREEN_BOUNDS    [UIScreen mainScreen].bounds
#define SCREEN_WIDTH     [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT    [UIScreen mainScreen].bounds.size.height

#import "CZGuideViewController.h"
#import "JYBaseTabBarController.h"

@interface CZGuideViewController ()<UIScrollViewDelegate>
{
    BOOL isNetwork;
}

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NSMutableArray *dataSourceArr;
@property (strong, nonatomic) NSArray *dataSourceLocaArr;

@property (nonatomic ,strong) UIPageControl *pageControl;   //页码

@end

@implementation CZGuideViewController

#pragma mark --------- 懒加载 --------

- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.frame = CGRectMake(0, SCREEN_HEIGHT - 50, SCREEN_WIDTH, 20);
    }
    return _pageControl;
}

- (UIScrollView *)scrollView {
    if(!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        [_scrollView setFrame:self.view.bounds];
        [_scrollView setDelegate:self];
        [_scrollView setBounces:NO];
        [_scrollView setPagingEnabled:YES];
        [_scrollView setShowsHorizontalScrollIndicator:NO];
        [_scrollView setShowsVerticalScrollIndicator:NO];
        [self.view addSubview:_scrollView];
    }
    return _scrollView;
}
- (NSMutableArray *)dataSourceArr {
    if(!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
- (NSArray *)dataSourceLocaArr {
    if(!_dataSourceLocaArr) {
        _dataSourceLocaArr = [NSArray arrayWithObjects:@"引导页1",@"引导页2",nil];
    }
    return _dataSourceLocaArr;
}

#pragma mark ------ 生命周期 ------
- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestData];
}

- (void)createGuide:(NSArray *)guideArr {
    //设置引导页
    [self.scrollView setContentSize:CGSizeMake(guideArr.count * SCREEN_WIDTH, SCREEN_HEIGHT)];
    for (int i = 0; i < guideArr.count; i++) {
        UIImageView *imageVieww = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH * i, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [imageVieww setUserInteractionEnabled:YES];
        [self.scrollView addSubview:imageVieww];
        if(isNetwork) {
//            [imageVieww sd_setImageWithURL:[NSURL URLWithString:guideArr[i]]];
        }else{
            [imageVieww setImage:[UIImage imageNamed:guideArr[i]]];
        }
        
        if(i == guideArr.count - 1){
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setImage:[UIImage imageNamed:@"立即体验"] forState:UIControlStateNormal];
            CGFloat top = 1150/1334.0 * SCREEN_HEIGHT;
            CGFloat bottom = 1230/1334.0 * SCREEN_HEIGHT;
            CGFloat width = 330/750.0 *SCREEN_WIDTH;
            CGFloat left = 223/750.0 * SCREEN_WIDTH;
            [btn setFrame:CGRectMake(left,top,width,bottom - top)];
            [imageVieww addSubview:btn];
            [btn addTarget:self action:@selector(startShoping) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    //设置pageControl
    self.pageControl.numberOfPages = guideArr.count;
    [self.view addSubview:self.pageControl];
    self.pageControl.hidden = YES;
}

#pragma mark - 切换
- (void)startShoping {
    NSLog(@"立即体验");
    //   获取当前的最新版本号 2.0
    NSString *curVersion =  [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
    [[NSUserDefaults standardUserDefaults] setObject:curVersion forKey:JYVersionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if(self.changeRootViewCoutroller){
//        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:JY_FIRST_LAUNCH];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        self.changeRootViewCoutroller();
    }
    
    [UIApplication sharedApplication].keyWindow.rootViewController = [[JYBaseTabBarController alloc] init];

    
}
- (void)changeRootViewCoutroller:(ChangeRootViewCoutroller)changeRootViewCoutroller {
    self.changeRootViewCoutroller = changeRootViewCoutroller;
}

#pragma mark - 网络
- (void)requestData {
    // 测试
    isNetwork = NO;
    [self createGuide:self.dataSourceLocaArr];
    //    HRWeak(weakSelf);
    //    NSDictionary *pra = @{@"type":@"2"};
    //    [GGNetworking postWithPath:PATH_Guide params:pra success:^(id response) {
    //        weakSelf.dataSourceArr = response[KEY];
    //        if(weakSelf.dataSourceArr.count) {
    //
    //            if ([CZRegular checkURL:weakSelf.dataSourceArr[0]]){
    //                isNetwork = YES;
    //                [weakSelf createGuide:weakSelf.dataSourceArr];
    //            }else{
    //                isNetwork = NO;
    //                [weakSelf createGuide:weakSelf.dataSourceLocaArr];
    //            }
    //
    //        }else{
    //            isNetwork = NO;
    //            [weakSelf createGuide:weakSelf.dataSourceLocaArr];
    //        }
    //
    //    } fail:^(NSError *error) {
    //        isNetwork = NO;
    //        [weakSelf createGuide:weakSelf.dataSourceLocaArr];
    //    }];
}

#pragma mark - ------- UIScrollViewDelegate -------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetx = scrollView.contentOffset.x;
    NSInteger currentPage = offsetx/SCREEN_WIDTH;
    self.pageControl.currentPage = currentPage;
}

//设置状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
