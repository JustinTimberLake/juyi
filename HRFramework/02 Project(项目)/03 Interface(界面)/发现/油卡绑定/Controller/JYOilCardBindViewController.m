//
//  JYOilCardBindViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOilCardBindViewController.h"
#import "JYOilCardViewModel.h"
#import "JYGetCodeViewModel.h"
#import <libkern/OSAtomic.h>

@interface JYOilCardBindViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *oilCardTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (nonatomic,strong) JYOilCardViewModel *oilViewModel;
@property (nonatomic,strong) JYGetCodeViewModel *getCodeViewModel;
@property (weak, nonatomic) IBOutlet UIView *btnView;
@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,copy) NSString *oilType;//中石化，中石油

@end

@implementation JYOilCardBindViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self config];
    [self configUI];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"绑定油卡";
    
//    [self.btnView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        UIButton * btn = (UIButton *)obj;
//        [btn sizeToFit];
//    }];
    
    [self oilCardTypeBtnAction:[self.btnView viewWithTag:1001]];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

- (IBAction)oilCardTypeBtnAction:(UIButton *)sender {
    self.selectBtn.selected = NO;
    self.selectBtn = sender;
    self.selectBtn.selected = YES;
    self.oilType = SF(@"%ld",sender.tag - 1000);
}

//ui事件，timer事件，noti事件，gesture事件
- (IBAction)codeBtnAction:(UIButton *)sender {
    
    if (!self.phoneTextField.text.length) {
        [self showSuccessTip:@"请输入手机号"];
        return;
    }
    if (![self.phoneTextField.text checkTelephoneNumber]) {
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }
    sender.enabled = NO;
    __block int32_t timeOutCount = 60;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1ull * NSEC_PER_SEC, 0);
    dispatch_source_set_cancel_handler(timer, ^{
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
    });
    dispatch_resume(timer);
    
    WEAKSELF
    [self.getCodeViewModel requestGetCodeWithPhoneNumber:self.phoneTextField.text andType:3 success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"已发送您的手机，请查收"];
        dispatch_source_set_event_handler(timer, ^{
            OSAtomicDecrement32(&timeOutCount);
            [sender setTitle:SF(@"(%ds)后重试", timeOutCount)  forState:UIControlStateDisabled];
            [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_LINE_HEAD] forState:UIControlStateDisabled];
            if (timeOutCount == 0) {
                sender.enabled = YES;
                NSLog(@"timersource cancel");
                dispatch_source_cancel(timer);
            }
        });
        
    } failure:^(NSString *errorMsg) {
        sender.enabled = YES;
        [sender setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (IBAction)registBtnAction:(UIButton *)sender {
    if (!self.nameTextField.text.length) {
        [self showSuccessTip:@"请输入持卡人姓名"];
        return;
    }else if (!self.phoneTextField.text.length){
        [self showSuccessTip:@"请输入持卡人手机号"];
        return;
    }else if (!self.oilCardTextField.text.length) {
        [self showSuccessTip:@"请输入油卡号"];
        return;
    }else if (!self.codeTextField.text.length){
        [self showSuccessTip:@"请输入验证码"];
        return;
    }else if (!self.oilType.length){
        [self showSuccessTip:@"请选择油卡类型"];
    } else{
        [self requestBindOilCard];
    }
}


#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    
}

- (void)requestBindOilCard{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"name"] = self.nameTextField.text;
    dic[@"phone"] = self.phoneTextField.text;
    dic[@"oilCardNum"] = self.oilCardTextField.text;
    dic[@"code"] = self.codeTextField.text;
    dic[@"changeType"] = self.oilType;

    [self.oilViewModel requestBindOilCardWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"绑定成功"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================


- (JYOilCardViewModel *)oilViewModel
{
    if (!_oilViewModel) {
        _oilViewModel = [[JYOilCardViewModel alloc] init];
    }
    return _oilViewModel;
}

- (JYGetCodeViewModel *)getCodeViewModel
{
    if (!_getCodeViewModel) {
        _getCodeViewModel = [[JYGetCodeViewModel alloc] init];
    }
    return _getCodeViewModel;
}


@end
