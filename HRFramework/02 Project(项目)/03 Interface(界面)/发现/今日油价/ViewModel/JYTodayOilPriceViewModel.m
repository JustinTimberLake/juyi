//
//  JYTodayOilPriceViewModel.m
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYTodayOilPriceViewModel.h"
//#import "JYOilPriceModel.h"
#import "JYJuHeOilPriceModel.h"
#import "JYJuHeOilModel.h"


@implementation JYTodayOilPriceViewModel

////获取今日油价
//- (void)requestGetOilPriceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
//    WEAKSELF
//    HRRequestManager * manager = [[HRRequestManager alloc] init];
//    [manager POST_URL:JY_PATH(JY_FIND_GetOilPrice) params:nil success:^(id result) {
//        NSLog(@"%@",result);
//        if (RESULT_SUCCESS) {
//            [weakSelf.listArr removeAllObjects];
//            NSArray * arr = [JYOilPriceModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
//            [weakSelf.listArr addObjectsFromArray:arr];
//            successBlock(RESULT_MESSAGE,nil);
//        }else{
//            failureBlock(RESULT_MESSAGE);
//        }
//    } failure:^(NSDictionary *errorInfo) {
//        failureBlock(NET_NOT_WORK);
//    }];
//}

//获取今日油价 （使用第三方）
- (void)requestJuHeGetOilPriceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"key"] = JY_JUHE_JINRIYOUJIA_APPKEY;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_JUHE_JINRIYOUJIA_URL params:dic success:^(id result) {
        NSLog(@"%@",result);
        if ([result[@"resultcode"] intValue] == 200) {
            [weakSelf.listArr removeAllObjects];
//            数据解析 转换成自己需要的格式
            NSMutableArray * dataArr = [NSMutableArray array];
            for (NSMutableDictionary * dic in result[@"result"]) {
        
                NSMutableDictionary * oilDic = [NSMutableDictionary dictionary];
                oilDic[@"city"] = dic[@"city"];
                NSMutableArray * oilArr = [NSMutableArray array];
                
                [oilArr addObject: [self creatArrWithKey:@"b90" andValue:dic[@"b90"]]];
                [oilArr addObject: [self creatArrWithKey:@"b93" andValue:dic[@"b93"]]];
                [oilArr addObject: [self creatArrWithKey:@"b97" andValue:dic[@"b97"]]];
                [oilArr addObject: [self creatArrWithKey:@"b0" andValue:dic[@"b0"]]];

                oilDic[@"oilArr"] = oilArr;
                [dataArr addObject:oilDic];
            }
            
            NSArray * arr = [JYJuHeOilPriceModel mj_objectArrayWithKeyValuesArray:dataArr];
            [weakSelf.listArr addObjectsFromArray:arr];
            NSLog(@"++++++%@",weakSelf.listArr);
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableDictionary *)creatArrWithKey:(NSString *)key  andValue:(NSString *)value{
    NSMutableDictionary * dic = [NSMutableDictionary  dictionary];
    dic[@"oilType"] = key;
    dic[@"oilPrice"] = value;
    return  dic;
}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}

@end
