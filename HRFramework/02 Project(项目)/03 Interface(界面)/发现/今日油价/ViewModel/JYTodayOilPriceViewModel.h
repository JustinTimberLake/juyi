//
//  JYTodayOilPriceViewModel.h
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYTodayOilPriceViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;
//获取今日油价 （弃用）
//- (void)requestGetOilPriceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取今日油价 （使用第三方）
- (void)requestJuHeGetOilPriceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
