//
//  JYTodayOilPriceViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYTodayOilPriceViewController.h"
#import "JYMyGasolineTableViewCell.h"
#import "JYTodayOilPriceViewModel.h"
#import "JYOilPriceModel.h"
#import "JYJuHeOilPriceModel.h"

static NSString * oilCellId = @"JYMyGasolineTableViewCell";

@interface JYTodayOilPriceViewController ()<
    UITableViewDelegate,
    UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYTodayOilPriceViewModel *viewModel;

@end

@implementation JYTodayOilPriceViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    [self config];
    [self configUI];
    [self requestTodayOilPrice];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"今日油价";
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYMyGasolineTableViewCell" bundle:nil] forCellReuseIdentifier:oilCellId];
    self.myTableView.backgroundColor = RGB(244, 245, 246);
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    
}

- (void)requestTodayOilPrice{
    WEAKSELF
    [self.viewModel requestJuHeGetOilPriceSuccess:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"查询成功"];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

#pragma mark ================== tableview Datasource and Delegate ==================


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JYMyGasolineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:oilCellId forIndexPath:indexPath];
//    JYOilPriceModel * model = self.viewModel.listArr[indexPath.row];
//    [cell updataCellMode:JYCellType_TodayOilPrice];
//    cell.modelArray = model.oilPrices;
//    cell.titleLabel.text = model.cityTitle;
    JYJuHeOilPriceModel * model = self.viewModel.listArr[indexPath.row];
    [cell updataCellMode:JYCellType_TodayOilPrice];
    cell.modelArray = model.oilArr;
    cell.titleLabel.text = model.city;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYJuHeOilPriceModel * model = self.viewModel.listArr[indexPath.row];
    return 105 +  (model.oilArr.count+2-1)/2 * 65;
}


- (JYTodayOilPriceViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYTodayOilPriceViewModel alloc] init];
    }
    return _viewModel;
}


@end
