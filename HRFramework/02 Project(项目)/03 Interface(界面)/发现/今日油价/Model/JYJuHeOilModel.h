//
//  JYJuHeOilModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYJuHeOilModel : JYBaseModel

@property (nonatomic,copy) NSString *oilType;
@property (nonatomic,copy) NSString *oilPrice;
@end
