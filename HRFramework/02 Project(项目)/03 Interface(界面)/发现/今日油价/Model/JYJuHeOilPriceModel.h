//
//  JYJuHeOilPriceModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYJuHeOilModel.h"

@interface JYJuHeOilPriceModel : JYBaseModel

@property (nonatomic,copy) NSString *city;
@property (nonatomic,strong) NSMutableArray<JYJuHeOilModel *> *oilArr;
//@property (nonatomic,copy) NSString *b90;
//@property (nonatomic,copy) NSString *b93;
//@property (nonatomic,copy) NSString *b97;
//@property (nonatomic,copy) NSString *b0;
@end
