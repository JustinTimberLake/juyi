//
//  JYOilPriceModel.h
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYOilGunModel.h"

@interface JYOilPriceModel : JYBaseModel
//城市编码
@property (nonatomic,copy) NSString *cityCode;
//城市名称
@property (nonatomic,copy) NSString *cityTitle;
//油价
@property (nonatomic,strong) NSMutableArray<JYOilGunModel *> *oilPrices;

@end
