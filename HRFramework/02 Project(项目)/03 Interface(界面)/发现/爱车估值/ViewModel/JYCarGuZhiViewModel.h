//
//  JYCarGuZhiViewModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYCarTypeModel.h"
#import "JYCarBrandModel.h"
#import "JYCarPurposeModel.h"

#import "JYJUHECarProvinceModel.h"
#import "JYJUHECarCityModel.h"
#import "JYJUHECarBrandModel.h"
#import "JYJUHECarValueModel.h"
#import "JYJUHECarXiModel.h"
#import "JYJUHECarXingModel.h"


@interface JYCarGuZhiViewModel : JYBaseViewModel

@property (nonatomic,strong) NSArray<JYJUHECarProvinceModel *> *provionceArr;

@property (nonatomic,strong) NSArray<JYJUHECarCityModel *> *cityArr;
//品牌数组
@property (nonatomic,strong) NSMutableArray<JYJUHECarBrandModel *> *brandArr;
//车系数组
@property (nonatomic,strong) NSMutableArray<JYJUHECarXiModel *> *carXiArr;

//车型数组
@property (nonatomic,strong) NSMutableArray<JYJUHECarXingModel *> *carXingArr;

@property (nonatomic,strong) NSArray *yearArr;

@property (nonatomic,strong) JYJUHECarValueModel *valueModel;

/**
 请求车辆用途 (作废)

 @param successBlock call back
 @param failureBlock call back
 */
//- (void)requestCarPurpostSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//
///**
// 请求车辆品牌、系列、型号 （作废）
//
// @param carTypeId 车辆类型id
// @param successBlock call back
// @param failureBlock call back
// */
//- (void)requestCarBrandWithCarType:(NSString *)carTypeId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

/**
 请求车辆类型（作废）

 @param successBlock success call back
 @param failureBlock failure call back
 */
//- (void)requestCarTypeSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

#pragma mark -------------------上面的废弃了------------------------
//爱车估值的省列表
- (void)requestGetAssessmentProvinceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//爱车估值市列表
- (void)requestGetAssessmentCityWithProvince:(NSString *)province Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//爱车估值品牌列表
- (void)requestGetAssessmentBrandWithVehicle:(NSString *)vehicle Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//爱车估值车系列表
- (void)requestGetAssessmentCarWithBrand:(NSString *)brand Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//爱车估值车型列表
- (void)requestGetAssessmentModelsWithSeries:(NSString *)series Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//爱车估值车型年份列表
- (void)requestGetAssessmentModelYearWithCar:(NSString *)car Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//爱车估值二手车价格评估
- (void)requestGetValueAssessmentWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
