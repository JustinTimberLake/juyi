//
//  JYCarGuZhiViewModel.m
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCarGuZhiViewModel.h"
#import "JYJUHECarXiModel.h"

@implementation JYCarGuZhiViewModel

//- (void)requestCarPurpostSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
//{
//    HRRequestManager *manager = [[HRRequestManager alloc] init];
//    [manager POST_URL:JY_PATH(JY_FIND_GetCarPurpose) params:nil success:^(id result) {
//        NSString *msg = [result objectForKey:@"errorMsg"];
//        if ([[result objectForKey:@"status"] boolValue]) {
//            //将数据回调
//            NSArray *dataArr = [result objectForKey:@"data"];
//            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
//
//            //一般是遍历
//            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCarPurposeModel *model = [JYCarPurposeModel mj_objectWithKeyValues:obj];
//                [modelArr addObject:model];
//            }];
//
//            successBlock(msg,modelArr);
//        }
//        else
//        {
//            //返回错误信息
//            failureBlock(msg);
//        }
//    } failure:^(NSDictionary *errorInfo) {
//        failureBlock(NET_NOT_WORK);
//    }];
//}
//
//- (void)requestCarBrandWithCarType:(NSString *)carTypeId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
//{
//    if (!carTypeId || carTypeId.length == 0) {
//        failureBlock(@"carTypeId 参数有误");
//        return;
//    }
//
//    HRRequestManager *manager = [[HRRequestManager alloc] init];
//
//    NSDictionary *param = @{@"carTypeId":carTypeId};
//
//    [manager POST_URL:JY_PATH(JY_FIND_GetBrand) params:param success:^(id result) {
//        NSString *msg = [result objectForKey:@"errorMsg"];
//        if ([[result objectForKey:@"status"] boolValue]) {
//            //将数据回调
//            NSArray *dataArr = [result objectForKey:@"data"];
//            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
//
//            //一般是遍历
//            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCarBrandModel *model = [JYCarBrandModel mj_objectWithKeyValues:obj];
//                [modelArr addObject:model];
//            }];
//
//            successBlock(msg,modelArr);
//        }
//        else
//        {
//            //返回错误信息
//            failureBlock(msg);
//        }
//    } failure:^(NSDictionary *errorInfo) {
//        failureBlock(NET_NOT_WORK);
//    }];
//}
//
//- (void)requestCarTypeSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
//{
//    HRRequestManager *manager = [[HRRequestManager alloc] init];
//    [manager POST_URL:JY_PATH(JY_FIND_GetCarType) params:nil success:^(id result) {
//        NSString *msg = [result objectForKey:@"errorMsg"];
//        if ([[result objectForKey:@"status"] boolValue]) {
//            //将数据回调
//            NSArray *dataArr = [result objectForKey:@"data"];
//            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
//
//            //一般是遍历
//            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCarTypeModel *model = [JYCarTypeModel mj_objectWithKeyValues:obj];
//                [modelArr addObject:model];
//            }];
//            successBlock(msg,modelArr);
//        }
//        else
//        {
//            //返回错误信息
//            failureBlock(msg);
//        }
//    } failure:^(NSDictionary *errorInfo) {
//        failureBlock(NET_NOT_WORK);
//    }];
//}

#pragma mark ------------------上面的废弃了------------------------

//爱车估值的省列表
- (void)requestGetAssessmentProvinceSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_GetAssessmentProvince) params:nil success:^(id result) {
        if (RESULT_SUCCESS) {
            weakSelf.provionceArr = [JYJUHECarProvinceModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//爱车估值市列表
- (void)requestGetAssessmentCityWithProvince:(NSString *)province Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"province"] = province;
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_GetAssessmentCity) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            weakSelf.cityArr = [JYJUHECarCityModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//爱车估值品牌列表
- (void)requestGetAssessmentBrandWithVehicle:(NSString *)vehicle Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"vehicle"] = vehicle;
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_GetAssessmentBrand) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
//            处理品牌数据
            [weakSelf.brandArr removeAllObjects];
            //            处理车型数据
            NSArray* arr = [RESULT_DATA allKeys];
            arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
                NSComparisonResult result = [obj1 compare:obj2];
                return result==NSOrderedDescending;
            }];
            
            for (NSString * key in arr) {
                 NSArray * arr = [JYJUHECarBrandModel mj_objectArrayWithKeyValuesArray:result[@"data"][key]];
                [weakSelf.brandArr addObjectsFromArray:arr];
            }
            NSLog(@"🐷%@",weakSelf.brandArr);
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//爱车估值车系列表
- (void)requestGetAssessmentCarWithBrand:(NSString *)brand Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"brand"] = brand;
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_GetAssessmentCar) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            //            处理车系数据
            [weakSelf.carXiArr removeAllObjects];
            NSArray * arr = RESULT_DATA[@"pinpai_list"];
            for (NSDictionary * dic in arr) {
                
                [weakSelf.carXiArr addObjectsFromArray:[JYJUHECarXiModel mj_objectArrayWithKeyValuesArray:dic[@"xilie"]]];
            }
            
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//爱车估值车型列表
- (void)requestGetAssessmentModelsWithSeries:(NSString *)series Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"series"] = series;
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_GetAssessmentModels) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            [weakSelf.carXingArr removeAllObjects];
            for (NSDictionary * dic  in RESULT_DATA[@"data"]) {
                [weakSelf.carXingArr addObjectsFromArray:[JYJUHECarXingModel mj_objectArrayWithKeyValuesArray:dic[@"chexing_list"]]];
            }
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

    
}

//爱车估值车型年份列表
- (void)requestGetAssessmentModelYearWithCar:(NSString *)car Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"car"] = car;
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_GetAssessmentModelYear) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            //            处理车型数据
            weakSelf.yearArr = RESULT_DATA;
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//爱车估值二手车价格评估
- (void)requestGetValueAssessmentWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_ValueAssessment) params:params success:^(id result) {
        if (RESULT_SUCCESS) {
            weakSelf.valueModel = [JYJUHECarValueModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

#pragma mark ==================懒加载==================

- (NSMutableArray<JYJUHECarBrandModel *> *)brandArr
{
    if (!_brandArr) {
        _brandArr = [NSMutableArray array];
    }
    return _brandArr;
}

- (NSMutableArray *)carXiArr
{
    if (!_carXiArr) {
        _carXiArr = [NSMutableArray array];
    }
    return _carXiArr;
}

- (NSMutableArray<JYJUHECarXingModel *> *)carXingArr
{
    if (!_carXingArr) {
        _carXingArr = [NSMutableArray array];
    }
    return _carXingArr ;
}


@end
