//
//  JYJUHECarValueModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYCarAreaPriceModel.h"

@interface JYJUHECarValueModel : JYBaseModel

// 13.66,    /*收购价格(二手车商收购这辆车的价格)*/
// 12.57,    /*个人交易价格(C2C交易价格)*/
// 14.62     /*卖出价格(二手车商卖出参考价)*/

@property (nonatomic,strong) NSArray * est_price_result;

//     各地区价格
@property (nonatomic,strong) NSArray<JYCarAreaPriceModel*> *est_price_area;

@end
