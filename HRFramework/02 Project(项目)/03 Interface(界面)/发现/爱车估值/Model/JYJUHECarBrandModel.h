//
//  JYJUHECarBrandModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYJUHECarBrandModel : JYBaseModel
//"": "2000386",    /*品牌id*/
@property (nonatomic,copy) NSString *brandId;
//"": "AC",    /*品牌名称*/
@property (nonatomic,copy) NSString *big_ppname;
//"": "A"    /*品牌首字母*/
@property (nonatomic,copy) NSString *pin;

@end
