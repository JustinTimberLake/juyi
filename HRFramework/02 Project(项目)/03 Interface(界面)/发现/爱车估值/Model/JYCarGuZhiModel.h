//
//  JYCarGuZhiModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYCarGuZhiModel : JYBaseModel

@property (nonatomic ,copy) NSString *placeholder;          //
@property (nonatomic ,copy) NSString *itemTitle;            //
@property (nonatomic ,copy) NSString *content;              //

@property (nonatomic ,assign) BOOL canInput;                //是否可以输入

@end
