//
//  JYJUHECarCityModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYJUHECarCityModel : JYBaseModel
//"": "98",    /*城市id*/
@property (nonatomic,copy) NSString *cityID;
//"": "宿迁市",    /*城市名称*/
@property (nonatomic,copy) NSString *cityName;
//"": "11",    /*省份id*/
@property (nonatomic,copy) NSString *proID;
//"": "0.986801",
@property (nonatomic,copy) NSString *value;
//"": "2",
@property (nonatomic,copy) NSString *area;
//"": "suqian",    /*城市名称拼音*/
@property (nonatomic,copy) NSString *pinyin;
//"": "国4排放标准（除低速车、挂车、摩托车和质量大于3.5吨车辆）；不符合标准的，需买方写保证",    /*准入标准*/
@property (nonatomic,copy) NSString *enter;
//"": "25"
@property (nonatomic,copy) NSString *hits;

@end
