//
//  JYCarAreaPriceModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYCarAreaPriceModel : JYBaseModel
//"": 14.08,    /*地区价格*/
@property (nonatomic,copy) NSString *price;
//"": "东北地区",    /*地区名称*/
@property (nonatomic,copy) NSString *area;
//"": "1"    /*地区id*/
@property (nonatomic,copy) NSString *areaid;
@end
