//
//  JYCarTypeModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//  车辆类型

#import "JYBaseModel.h"

@interface JYCarTypeModel : JYBaseModel

@property (nonatomic ,copy) NSString *carTypeId;        //车辆类型id
@property (nonatomic ,copy) NSString *carTypeTitle;        //车辆类型名称

@end
