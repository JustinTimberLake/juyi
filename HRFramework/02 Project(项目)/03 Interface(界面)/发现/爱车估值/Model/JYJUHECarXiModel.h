//
//  JYJUHECarXiModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYJUHECarXiModel : JYBaseModel
//"": "20001414",    /*系列id*/
@property (nonatomic,copy) NSString *xlid;
//"": "雅力士"    /*系列名*/
@property (nonatomic,copy) NSString *xlname;

@end
