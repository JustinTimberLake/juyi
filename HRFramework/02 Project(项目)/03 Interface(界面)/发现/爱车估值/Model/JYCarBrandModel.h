//
//  JYCarBrandModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//  车辆品牌

#import "JYBaseModel.h"

@interface JYCarBrandModel : JYBaseModel

@property (nonatomic ,copy) NSString *brandId;        //品牌id
@property (nonatomic ,copy) NSString *brandTitle;        //品牌
@property (nonatomic ,copy) NSString *brandSeries;        //系列
@property (nonatomic ,copy) NSString *brandModel;        //型号

@end
