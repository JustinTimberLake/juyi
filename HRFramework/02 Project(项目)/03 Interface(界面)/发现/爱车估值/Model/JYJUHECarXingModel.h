//
//  JYJUHECarXingModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYJUHECarXingModel : JYBaseModel
//"": "20022441",    /*车型id*/
@property (nonatomic,copy) NSString *cxId;
//"": "逸致 2015款 180E CVT跨界版",    /*车型名称*/
@property (nonatomic,copy) NSString *cxname;
//"": "2014",    /*上市时间*/
@property (nonatomic,copy) NSString *pyear;
//"": "16.28"    /*发布价格*/
@property (nonatomic,copy) NSString *price;
@end
