//
//  JYCarPurposeModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//  车辆用途

#import "JYBaseModel.h"

@interface JYCarPurposeModel : JYBaseModel

@property (nonatomic ,copy) NSString *carPurposeId;        //车辆用途id
@property (nonatomic ,copy) NSString *carPurposeTitle;        //车辆用途

@end
