//
//  JYCarGuZhiViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCarGuZhiViewController.h"
#import "JYCarGuZhiModel.h"
#import "JYCarGuZhiTableViewCell.h"
#import "JYCarGuZhiFooterView.h"
#import "JYPickerViewAlertController.h"     //选择框
#import "JYLocationVM.h"
#import "JYCarGuZhiViewModel.h"
#import "JYCarGuZhiResultViewController.h"  //评估结果
#import "JYCarGuZhiViewModel.h"
#import "JYProCityCountyPickerViewController.h"
#import "JYDatePickerViewController.h"

static NSString *const CellID = @"JYCarGuZhiTableViewCell";

@interface JYCarGuZhiViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) JYCarGuZhiFooterView *footerView;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@property (nonatomic ,strong) NSMutableArray *provinceArr;         //省数据
@property (nonatomic ,copy) NSString *selectedProvinceCode; //已选的省Code

@property (nonatomic ,strong) NSMutableArray *cityArr;             //市数据
@property (nonatomic ,copy) NSString *selectedCityCode;     //已选的市Code

@property (nonatomic ,strong) NSArray *carTypeArr;   //车辆类型
@property (nonatomic ,copy) NSString *selectedCarTypeId;    //已选车类型id

@property (nonatomic ,strong) NSMutableArray *carBrandArr;  //车辆品牌
@property (nonatomic ,copy) NSString *selectedBrandId;      //已选品牌id

//@property (nonatomic ,strong) NSArray *carPurposeArr;       //车辆用途

@property (nonatomic,copy) NSString *selectYearStr; //已选你年份

@property (nonatomic,strong) NSMutableArray *useYearArr;//启用年份数组
@property (nonatomic,copy) NSString *selectStartYear; //启用年份

@property (nonatomic ,strong) NSArray *monthArr;            //启用月份
@property (nonatomic,copy) NSString *selectMonthId;         //已选启用月份


@property (nonatomic,strong) NSArray *pingGuArr; //评估

@property (nonatomic,strong) NSArray *cheKuangArr;//车况
@property (nonatomic,copy) NSString *cheKuangId; //车况ID

@property (nonatomic,strong) NSArray *yongTuArr;//用途
@property (nonatomic ,copy) NSString *selectedPurposeId;    //已选用途id


@property (nonatomic,copy) NSString *kilometre; //公里数
@property (nonatomic,copy) NSString *buyPrice; //购买价格
@property (nonatomic ,strong) NSArray *itemTitleArr;        //标题


@property (nonatomic,copy) NSString *carBrandName;
//车系
@property (nonatomic,copy) NSString *carXiName;
//车型
@property (nonatomic,copy) NSString *carXingId;
@property (nonatomic,copy) NSString *carXingName;


@property (nonatomic,strong) JYCarGuZhiViewModel *guzhiViewModel; //爱车估值VM
@end

@implementation JYCarGuZhiViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configUI];
    [self config];
    [self configData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//弹窗选择
- (void)showPickerViewIndex:(NSInteger)index
{
    NSMutableArray *strArr = [[NSMutableArray alloc] init];
    
    switch (index) {
        case 0:
        {
//            if (self.provinceArr.count == 0) {
//                [self requestProvince];
//                return;
//            }
////            //查询省份
////            [self.provinceArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
////                JYProvinceModel *model = obj;
////                [strArr addObject:model.provinceTitle];
////            }];
            if (self.guzhiViewModel.provionceArr.count == 0) {
                [self requestProvince];
            }else{
                 [self creatPickerViewWithStrArr:self.provinceArr Index:index];
            }
            
        }
            break;
        case 1:
        {
//            if (self.cityArr.count == 0) {
//                [self requestCity];
//                return;
//            }
//            //查询城市
//            [self.cityArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCityModel *model = obj;
//                [strArr addObject:model.cityTitle];
//            }];
//            if (self.guzhiViewModel.cityArr.count == 0) {
                [self requestCity];
//            }else{
//                [self creatPickerViewWithStrArr:self.cityArr Index:index];
//            }

        }
            break;
        case 2:
        {
//            //车辆类型
//            if (self.carTypeArr.count == 0) {
//                [self requestCarType];
//                return;
//            }
//
//            [self.carTypeArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCarTypeModel *model = obj;
//                [strArr addObject:model.carTypeTitle];
//            }];
            [self creatPickerViewWithStrArr:self.carTypeArr Index:index];
            
        }
            break;
        case 3:
        {
//            //品牌
//            if (self.carBrandArr.count == 0) {
//                [self requestCarBrand];
//                return;
//            }
//            [self.carBrandArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCarBrandModel *model = obj;
//                NSString *str = [NSString stringWithFormat:@"%@    %@    %@",model.brandTitle,model.brandSeries,model.brandModel];
//                [strArr addObject:str];
//            }];
            
            if (self.guzhiViewModel.brandArr.count == 0) {
                [self requestCarBrand];
            }else{
                [self creatPickViewWithCarBrandWithBrandArr:self.guzhiViewModel.brandArr];
            }

        }
            break;
        case 4:
        {
            //车型年份
//            if (!self.guzhiViewModel.yearArr.count) {
                [self requestYear];
//            }else{
//                [self creatPickerViewWithStrArr:self.guzhiViewModel.yearArr Index:index];
//            }
            
        }
            break;
        case 5:
        {
            //精确评估
//            [strArr addObjectsFromArray:@[@"是",@"否"]];
            [self creatPickerViewWithStrArr:self.pingGuArr Index:index];
        }
            break;
        case 6:
        {
            //车况
//            [strArr addObjectsFromArray:@[@"较差",@"一般",@"优秀"]];
            [self creatPickerViewWithStrArr:self.cheKuangArr Index:index];
        }
            break;
        case 7:
        {
//            //车辆用途
//            if (self.carPurposeArr.count == 0) {
//                [self requestCarPurpose];
//                return;
//            }
//
//            [self.carPurposeArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYCarPurposeModel *model = obj;
//                [strArr addObject:model.carPurposeTitle];
//            }];
//            [strArr addObject:@[@"自用",@"公务商用",@"营运"]];
            [self creatPickerViewWithStrArr:self.yongTuArr Index:index];
        }
            break;
        case 8:
        {
            //启用年份
//            [self creatDataPickView];
            [self creatPickerViewWithStrArr:self.useYearArr Index:index];
        }
            break;
        case 9:
        {
            [self creatPickerViewWithStrArr:self.monthArr Index:index];
        }
            break;
        default:
            return;
            break;
    }
    
    
//    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
//    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [alertVC updatePickerViewDataArr:strArr type:PickerViewTypeNormal];
//    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
//        switch (index) {
//            case 0:
//            {
//                //查询省份
//                JYProvinceModel *tpModel = self.provinceArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[0];
//                self.selectedProvinceCode = tpModel.provinceCode;
//                model.content = tpModel.provinceTitle;
//            }
//                break;
//            case 1:
//            {
//                //查询城市
//                JYCityModel *tpModel = self.cityArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[1];
//                self.selectedCityCode = tpModel.cityCode;
//                model.content = tpModel.cityTitle;
//            }
//                break;
//            case 2:
//            {
//                //车辆类型
//                JYCarTypeModel *tpModel = self.carTypeArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[2];
//                self.selectedCarTypeId = tpModel.carTypeId;
//                model.content = tpModel.carTypeTitle;
//            }
//                break;
//            case 3:
//            {
//                //品牌
//                JYCarBrandModel *tpModel = self.carBrandArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[3];
//                self.selectedBrandId = tpModel.brandId;
//                model.content = [NSString stringWithFormat:@"%@    %@    %@",tpModel.brandTitle,tpModel.brandSeries,tpModel.brandModel];
//            }
//                break;
//            case 4:
//            {
//                //车型年份
//
//            }
//                break;
//            case 5:
//            {
//                //精确评估
//                [strArr addObjectsFromArray:@[@"是",@"否"]];
//            }
//                break;
//            case 6:
//            {
//                //车况
//                [strArr addObjectsFromArray:@[@"一般"]];
//            }
//                break;
//            case 7:
//            {
//                //车辆用途
//                JYCarPurposeModel *tpModel = self.carPurposeArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[7];
//                self.selectedPurposeId = tpModel.carPurposeId;
//                model.content = tpModel.carPurposeTitle;
//            }
//                break;
//            case 8:
//            {
//                //启用年份
//            }
//                break;
//            case 9:
//            {
//                //启用月份
//                [strArr addObjectsFromArray:@[
//                                              @"一月",
//                                              @"二月",
//                                              @"三月",
//                                              @"四月",
//                                              @"五月",
//                                              @"六月",
//                                              @"七月",
//                                              @"八月",
//                                              @"九月",
//                                              @"十月",
//                                              @"十一月",
//                                              @"十二月"
//                                              ]];
//            }
//                break;
//            default:
//                return;
//                break;
//        }
//        [self.tableView reloadData];
//    };
//    [self presentViewController:alertVC animated:YES completion:nil];
}

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    [self loadData];
}

- (void)configData{
    self.carTypeArr = @[@"乘用车",@"商用车"];
    self.pingGuArr =  @[@"是",@"否"];
    self.cheKuangArr = @[@"较差",@"一般",@"优秀"];
    self.yongTuArr = @[@"自用",@"公务商用",@"营运"];
    self.monthArr = @[
                      @"1月",
                      @"2月",
                      @"3月",
                      @"4月",
                      @"5月",
                      @"6月",
                      @"7月",
                      @"8月",
                      @"9月",
                      @"10月",
                      @"11月",
                      @"12月"
                      ];
    
//    定义年份数组
    //获取当前时间 （时间格式支持自定义）
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy"];//自定义时间格式
    NSString *currentDateStr = [formatter stringFromDate:[NSDate date]];
    //初始化年数据源数组
    for (NSInteger i = [currentDateStr intValue]; i>= 1970 ; i--) {
        NSString *yearStr = [NSString stringWithFormat:@"%ld",(long)i];
        [self.useYearArr addObject:yearStr];
    }
    
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"爱车估值";
    
    self.tableView.rowHeight = 49;
    [self.tableView registerNib:[UINib nibWithNibName:CellID bundle:nil] forCellReuseIdentifier:CellID];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.footerView = [[[NSBundle mainBundle] loadNibNamed:@"JYCarGuZhiFooterView" owner:self options:nil] lastObject];
    [self.footerView setBtnTitle:@"评估"];
    self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 130);
    
    WEAKSELF
    self.footerView.evaluateBlock = ^{
        [weakSelf turnEvaluatePage];
    };
    
    self.tableView.tableFooterView = self.footerView;
}

- (void)turnEvaluatePage{
    if (!self.cheKuangId.length) {
        [self showSuccessTip:@"请选择车况"];
        return;
    }

    if (!self.selectedPurposeId.length) {
        [self showSuccessTip:@"请选择车辆用途"];
        return;
    }

    if (!self.selectedCityCode.length) {
        [self showSuccessTip:@"请选择城市"];
        return;
    }

    if (!self.selectedProvinceCode.length) {
        [self showSuccessTip:@"请选择省"];
        return;
    }

    if (!self.carXingId.length) {
        [self showSuccessTip:@"请选择车型"];
        return;
    }

    if (!self.selectStartYear.length) {
        [self showSuccessTip:@"请选择启用年份"];
        return;
    }

    if (!self.selectMonthId.length) {
        [self showSuccessTip:@"请选择启用月份"];
        return;
    }

    if (!self.kilometre.length) {
        [self showSuccessTip:@"请填写公里数"];
        return;
    }
    if (!self.buyPrice.length) {
        [self showSuccessTip:@"请填写购买价格"];
        return;
    }
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"carstatus"] = self.cheKuangId ;
    dic[@"purpose"] = self.selectedPurposeId;
    dic[@"city"] = self.selectedCityCode;
    dic[@"province"] = self.selectedProvinceCode;
    dic[@"car"] = self.carXingId;
    dic[@"useddate"] = self.selectStartYear;
    dic[@"useddateMonth"] = self.selectMonthId;
    dic[@"mileage"] = self.kilometre;
    dic[@"price"] = self.buyPrice;
    
    JYCarGuZhiResultViewController *vc = [[JYCarGuZhiResultViewController alloc] init];
    vc.params = dic;
    [self.navigationController pushViewController:vc animated:YES];
}

//创建弹窗
- (void)creatPickerViewWithStrArr:(NSArray *)strArr Index:(NSInteger)index{
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [alertVC updatePickerViewDataArr:strArr type:PickerViewTypeNormal];
    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
        switch (index) {
            case 0:
            {
                //查询省份
                JYJUHECarProvinceModel *proModel = self.guzhiViewModel.provionceArr[selectIndex];
                JYCarGuZhiModel *model = self.dataArr[0];
                self.selectedProvinceCode = proModel.proID;
                model.content = proModel.proName;
            }
                break;
            case 1:
            {
                //查询城市
                JYJUHECarCityModel *cityModel = self.guzhiViewModel.cityArr[selectIndex];
                JYCarGuZhiModel *model = self.dataArr[1];
                self.selectedCityCode = cityModel.cityID;
                model.content = cityModel.cityName;
            }
                break;
            case 2:
            {
                //车辆类型
                NSString * str = self.carTypeArr[selectIndex];
                JYCarGuZhiModel *model = self.dataArr[2];
                
                self.selectedCarTypeId = (selectIndex == 0 )?@"passenger":@"commercial";
                model.content = str;
            }
                break;
            case 3:
            {
//                //品牌
//                JYCarBrandModel *tpModel = self.carBrandArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[3];
//                self.selectedBrandId = tpModel.brandId;
//                model.content = [NSString stringWithFormat:@"%@    %@    %@",tpModel.brandTitle,tpModel.brandSeries,tpModel.brandModel];
            }
                break;
                
            case 4:
            {
                //车型年份
                JYCarGuZhiModel *model = self.dataArr[4];
                self.selectStartYear = self.guzhiViewModel.yearArr[selectIndex];
                model.content =  self.selectStartYear;
                
            }
                break;
            case 5:
            {
                //精确评估
//                [strArr addObjectsFromArray:@[@"是",@"否"]];
                JYCarGuZhiModel *model = self.dataArr[5];
                model.content = self.pingGuArr[selectIndex];
                
            }
                break;
            case 6:
            {
                //车况
//                [strArr addObjectsFromArray:@[@"一般"]];
                JYCarGuZhiModel *model = self.dataArr[6];
                if (selectIndex == 0) {
                    self.cheKuangId = @"3";
                }else if (selectIndex == 1){
                     self.cheKuangId = @"2";
                }else{
                    self.cheKuangId = @"1";
                }
                model.content = self.cheKuangArr[selectIndex];

            }
                break;
            case 7:
            {
                //车辆用途
//                JYCarPurposeModel *tpModel = self.carPurposeArr[selectIndex];
//                JYCarGuZhiModel *model = self.dataArr[7];
//                self.selectedPurposeId = tpModel.carPurposeId;
//                model.content = tpModel.carPurposeTitle;
                JYCarGuZhiModel *model = self.dataArr[7];
                self.selectedPurposeId  = SF(@"%ld",selectIndex + 1);
                model.content = self.yongTuArr[selectIndex];

            }
                break;
            case 8:
            {
                //启用年份
                JYCarGuZhiModel *model = self.dataArr[8];
                self.selectStartYear = self.useYearArr[selectIndex];
                model.content = SF(@"%@年",self.selectStartYear);

            }
                break;
            case 9:
            {
                JYCarGuZhiModel *model = self.dataArr[9];
                NSString * month = self.monthArr[selectIndex];
                month = [month substringToIndex:month.length - 1];
                self.selectMonthId = SF(@"%02d",[month intValue]);
                model.content = self.monthArr[selectIndex];
            }
                break;
            default:
                return;
                break;
        }
        [self.tableView reloadData];
    };
    [self presentViewController:alertVC animated:YES completion:nil];
}

//- (void)creatDataPickView{
//    JYDatePickerViewController * dataPickerVc = [[JYDatePickerViewController alloc] init];
//    dataPickerVc.onlyYear = YES;
//    [self presentViewController:dataPickerVc animated:YES completion:nil];
//
//}

//创建车品牌pickerview
- (void)creatPickViewWithCarBrandWithBrandArr:(NSArray *)brandArr{
    WEAKSELF
    JYProCityCountyPickerViewController * alertVC = [[JYProCityCountyPickerViewController alloc] init];
//    self.alertVC = alertVC;
    alertVC.proCityRow = 3;
    [alertVC updatePickerViewDataArr:brandArr];
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    alertVC.bottomBtnAction = ^(NSString *brandName, NSString *carXiName, NSString *carXingName, NSString *carXingId) {
        weakSelf.carBrandName = brandName;
        weakSelf.carXiName = carXiName;
        weakSelf.carXingName = carXingName;
        weakSelf.carXingId = carXingId;
        
        JYCarGuZhiModel *model = self.dataArr[3];
        model.content = [NSString stringWithFormat:@"%@    %@    %@",weakSelf.carBrandName,weakSelf.carXiName,weakSelf.carXingName];
        [weakSelf.tableView reloadData];
    };
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYCarGuZhiModel *model = self.dataArr[indexPath.row];
    if ([model.itemTitle isEqualToString:self.itemTitleArr[0]]) {
        //查询省份
        [self showPickerViewIndex:0];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[1]]) {
        //查询城市
        [self showPickerViewIndex:1];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[2]]) {
        //车辆类型
        [self showPickerViewIndex:2];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[3]]) {
        //品牌
        [self showPickerViewIndex:3];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[4]]) {
        //车型年份
        [self showPickerViewIndex:4];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[5]]) {
        //精确评估
        [self showPickerViewIndex:5];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[6]]) {
        //车况
        [self showPickerViewIndex:6];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[7]]) {
        //车辆用途
        [self showPickerViewIndex:7];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[8]]) {
        //启用年份
        [self showPickerViewIndex:8];
    }
    if ([model.itemTitle isEqualToString:self.itemTitleArr[9]]) {
        //启用月份
        [self showPickerViewIndex:9];
    }
}

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYCarGuZhiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    [cell updateCellModel:self.dataArr[indexPath.row]];
    [cell updataContentTextFieldKeyboardTypeWithIndexPath:indexPath];
    cell.contentTextFieldBlock = ^(NSString *text) {
        if (indexPath.row == 10) {
            self.kilometre = text;
        }else{
            self.buyPrice = text;
        }
    };
    return cell;
}

#pragma mark - ======================== Net Request ========================

////请求车辆用途
//- (void)requestCarPurpose
//{
//    JYCarGuZhiViewModel *vm = [[JYCarGuZhiViewModel alloc] init];
//    [self showHUD];
//    [vm requestCarPurpostSuccess:^(NSString *msg, id responseData) {
//        [self hideHUD];
//        self.carPurposeArr = responseData;
//    } failure:^(NSString *errorMsg) {
//        [self hideHUD];
//    }];
//}

////请求车辆品牌
//- (void)requestCarBrand
//{
//    if (!self.selectedCarTypeId) {
//        [self showSuccessTip:@"请先选择车辆类型"];
//        return;
//    }
//
//    JYCarGuZhiViewModel *vm = [[JYCarGuZhiViewModel alloc] init];
//    [self showHUD];
//    [vm requestCarBrandWithCarType:self.selectedCarTypeId success:^(NSString *msg, id responseData) {
//        [self hideHUD];
//        [self.carBrandArr removeAllObjects];
//        [self.carBrandArr addObjectsFromArray:responseData];
//    } failure:^(NSString *errorMsg) {
//        [self hideHUD];
//        [self showSuccessTip:errorMsg];
//    }];
//}

////请求车辆类型数据
//- (void)requestCarType
//{
//    JYCarGuZhiViewModel *vm = [[JYCarGuZhiViewModel alloc] init];
//    [self showHUD];
//    [vm requestCarTypeSuccess:^(NSString *msg, id responseData) {
//        [self hideHUD];
//        [self.carTypeArr removeAllObjects];
//        [self.carTypeArr addObjectsFromArray:responseData];
//    } failure:^(NSString *errorMsg) {
//        [self hideHUD];
//        [self showSuccessTip:errorMsg];
//    }];
//}

////请求城市数据
//- (void)requestCity
//{
//    if (!self.selectedProvinceCode) {
//        [self showSuccessTip:@"请先选择省份"];
//        return;
//    }
//
//    JYLocationVM *vm = [[JYLocationVM alloc] init];
//    [self showHUD];
//    [vm requestCityDataWithProvinceCode:self.selectedProvinceCode success:^(NSString *msg, id responseData) {
//        [self hideHUD];
//        self.cityArr = responseData;
//    } failure:^(NSString *errorMsg) {
//        [self hideHUD];
//        [self showSuccessTip:errorMsg];
//    }];
//}

////请求省份数据
//- (void)requestProvince
//{
//    JYLocationVM *vm = [[JYLocationVM alloc] init];
//    [self showHUD];
//    [vm requestProvinceDataSuccess:^(NSString *msg, id responseData) {
//        [self hideHUD];
//        self.provinceArr = responseData;
//    } failure:^(NSString *errorMsg) {
//        [self hideHUD];
//        [self showSuccessTip:errorMsg];
//    }];
//}

#pragma mark ==================网络请求==================

//请求省份数据
- (void)requestProvince
{
    WEAKSELF
    [self showHUD];
    [self.guzhiViewModel requestGetAssessmentProvinceSuccess:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.guzhiViewModel.provionceArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            JYJUHECarProvinceModel *model = obj;
            [weakSelf.provinceArr addObject:model.proName];
        }];
        [weakSelf creatPickerViewWithStrArr:weakSelf.provinceArr Index:0];

    } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
    }];
}
//请求城市数据
- (void)requestCity
{
    WEAKSELF
    if (!self.selectedProvinceCode) {
        [self showSuccessTip:@"请先选择省份"];
        return;
    }
    
    [self showHUD];
    [self.guzhiViewModel requestGetAssessmentCityWithProvince:self.selectedProvinceCode Success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.cityArr removeAllObjects];
        [weakSelf.guzhiViewModel.cityArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            JYJUHECarCityModel *model = obj;
            [weakSelf.cityArr addObject:model.cityName];
        }];
        [weakSelf creatPickerViewWithStrArr:weakSelf.cityArr Index:1];
    } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求车辆品牌
- (void)requestCarBrand
{
    WEAKSELF
    if (!self.selectedCarTypeId) {
        [self showSuccessTip:@"请先选择车辆类型"];
        return;
    }
    [self showHUD];
    [self.guzhiViewModel requestGetAssessmentBrandWithVehicle:self.selectedCarTypeId Success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.guzhiViewModel.brandArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            JYJUHECarBrandModel *model = obj;
            [weakSelf.carBrandArr addObject:model.big_ppname];
        }];
//        [weakSelf creatPickerViewWithStrArr:weakSelf.carBrandArr Index:3];
        [weakSelf creatPickViewWithCarBrandWithBrandArr:weakSelf.guzhiViewModel.brandArr ];
    } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
    }];
    
//    JYCarGuZhiViewModel *vm = [[JYCarGuZhiViewModel alloc] init];
//    [self showHUD];
//    [vm requestCarBrandWithCarType:self.selectedCarTypeId success:^(NSString *msg, id responseData) {
//        [self hideHUD];
//        [self.carBrandArr removeAllObjects];
//        [self.carBrandArr addObjectsFromArray:responseData];
//    } failure:^(NSString *errorMsg) {
//        [self hideHUD];
//        [self showSuccessTip:errorMsg];
//    }];
}

- (void)requestYear{
    WEAKSELF
    if (!weakSelf.carXingId.length) {
        [self showSuccessTip:@"请选择品牌"];
        return;
    }
    [self.guzhiViewModel requestGetAssessmentModelYearWithCar:weakSelf.carXingId Success:^(NSString *msg, id responseData) {
          [weakSelf creatPickerViewWithStrArr:weakSelf.guzhiViewModel.yearArr Index:4];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//- (void)requestCarSeriesWithBrandId:(NSString *)brandId{
//    WEAKSELF
//    [self.guzhiViewModel requestGetAssessmentCarWithBrand:brandId Success:^(NSString *msg, id responseData) {
//
//    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
//    }];
//}


//用于初始化vm，请求当前页面数据
- (void)loadData
{
    [self originalData];
//    [self requestProvince];
}

//原始数据
- (void)originalData
{
    self.dataArr = [[NSMutableArray alloc] initWithCapacity:14];
    
    self.itemTitleArr = @[
                          @"查询省份：",
                          @"查询城市：",
                          @"车辆类型：",
                          @"品牌：",
                          @"车型年份：",
                          @"精确评估：",
                          @"车况：",
                          @"车辆用途：",
                          @"待估车辆的启用年份：",
                          @"待估车辆启用月份：",
                          @"待估车辆的公里数：",
                          @"待估车辆的购买价格：",
                          ];
    NSArray *placeholderArr = @[
                                @"请点击选择省份",
                                @"请点击选择城市",
                                @"请点击选择车辆类型",
                                @"请点击选择车的品牌",
                                @"请点击选择车型年份",
                                @"请点击选择估值等级",
                                @"请点击选择车况",
                                @"请点击选择车辆用途",
                                @"请点击选择启用年份",
                                @"请点击选择启用月份",
                                @"请输入车辆公里数(万公里)",
                                @"请输入购买价格(万元)",
                                ];
    NSArray *opTypeArr = @[
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @NO,
                           @YES,
                           @YES,
                           ];
    
    [self.itemTitleArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        JYCarGuZhiModel *model = [[JYCarGuZhiModel alloc] init];
        model.itemTitle = obj;
        model.placeholder = placeholderArr[idx];
        model.content = nil;
        model.canInput = [opTypeArr[idx] boolValue];
        [self.dataArr addObject:model];
    }];
    
    [self.tableView reloadData];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (NSMutableArray *)carBrandArr
{
    if (!_carBrandArr) {
        _carBrandArr = [[NSMutableArray alloc] init];
    }
    return _carBrandArr;
}

//- (NSMutableArray *)carTypeArr
//{
//    if (!_carTypeArr) {
//        _carTypeArr = [[NSMutableArray alloc] init];
//    }
//    return _carTypeArr;
//}

- (JYCarGuZhiViewModel *)guzhiViewModel
{
    if (!_guzhiViewModel) {
        _guzhiViewModel = [[JYCarGuZhiViewModel alloc] init];
    }
    return _guzhiViewModel;
}


- (NSMutableArray *)provinceArr
{
    if (!_provinceArr) {
        _provinceArr = [NSMutableArray array];
    }
    return _provinceArr;
}

- (NSMutableArray *)cityArr
{
    if (!_cityArr) {
        _cityArr = [NSMutableArray array];
    }
    return _cityArr;
}
- (NSMutableArray *)useYearArr
{
    if (!_useYearArr) {
        _useYearArr = [NSMutableArray array];
    }
    return _useYearArr;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
