//
//  JYCarGuZhiTableViewCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYCarGuZhiModel;

@interface JYCarGuZhiTableViewCell : UITableViewCell

- (void)updateCellModel:(JYCarGuZhiModel *)model;

- (void)updataContentTextFieldKeyboardTypeWithIndexPath:(NSIndexPath *)indexPath;

//填写内容
@property (nonatomic,copy) void(^contentTextFieldBlock) (NSString * text);
@end
