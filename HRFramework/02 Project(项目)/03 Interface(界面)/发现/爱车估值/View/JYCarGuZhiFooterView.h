//
//  JYCarGuZhiFooterView.h
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYCarGuZhiFooterView : UIView

@property (nonatomic ,copy) void (^evaluateBlock)();

- (void)setBtnTitle:(NSString *)title;

@end
