//
//  JYOilCardDetailViewController.h
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//  油卡详情

#import "JYBaseViewController.h"

@interface JYOilCardDetailViewController : JYBaseViewController

@property (nonatomic,strong) NSString *oilCardId;

@end
