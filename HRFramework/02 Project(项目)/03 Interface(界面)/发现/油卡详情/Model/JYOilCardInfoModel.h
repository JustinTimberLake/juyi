//
//  JYOilCardInfoModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYOilCardInfoModel : JYBaseModel
// 油卡ID
@property (nonatomic,copy) NSString *oilCardId;
// 油卡名称
@property (nonatomic,copy) NSString *oilCardTitle;
//卡号
@property (nonatomic,copy) NSString *oilCardNum;
//姓名
@property (nonatomic,copy) NSString *oilCardName;
//手机号
@property (nonatomic,copy) NSString *oilCardPhone;
//油卡类型
@property (nonatomic,copy) NSString *oilCardType;

@end
