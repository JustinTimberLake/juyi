//
//  JYRechargeOilCardViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/12/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargeOilCardViewModel.h"

@implementation JYRechargeOilCardViewModel
//油卡充值接口
- (void)requestRechargeOilCardWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_OILDEPOT_RechargeOilCard) params:params success:^(id result) {
        if (RESULT_SUCCESS) {
            NSString * orderId = RESULT_DATA[@"orderId"];
            successBlock(RESULT_MESSAGE,orderId);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
