//
//  JYRechargeOilCardViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYRechargeOilCardViewModel : JYBaseViewModel

//油卡充值接口
- (void)requestRechargeOilCardWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
