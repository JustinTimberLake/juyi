//
//  JYFindViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/6/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYFindViewController.h"
#import "JYFindHeaderView.h"
#import "JYFindFooterView.h"
#import "JYMyFavoritesMvTableViewCell.h"

#import "JYXinRenFuLiViewController.h"          //新人福利
#import "JYWeiZhangQueryViewController.h"       //违章查询
#import "JYCarGuZhiViewController.h"            //爱车估值
#import "JYXianXingViewController.h"            //限行查询
#import "JYTodayOilPriceViewController.h"       //今日油价
#import "JYWeiZhangGaoFaViewController.h"       //违章高发地
#import "JYOilCardChargeViewController.h"       //油卡充值
#import "JYYingJiDianHuaViewController.h"       //应急电话
#import "JYHeZuoViewController.h"               //合作加盟
#import "JYXiangGuanZiXunViewController.h"      //相关资讯
#import "JYVideoListViewController.h"           //更多视频

#import "JYBaseViewModel.h"
#import "JYMapViewController.h"

#warning TestCode Begin
//测试代码-测试完毕需要删除
#import "JYTestViewController.h"
#import "JYChatViewController.h"
#import "CLTableViewCell.h"
#import "CLPlayerView.h"
#import "CLModel.h"
#import "JYCollectionViewModel.h"
#import "JYGetCouponViewController.h"
#import "JYFindViewModel.h"
#import "CLModel.h"


#warning TestCode End

static NSString *const HeaderId = @"JYFindHeaderView";
static NSString *const CellId = @"JYMyFavoritesMvTableViewCell";
static NSString *const FooterId = @"JYFindFooterView";

@interface JYFindViewController ()<UITableViewDelegate,UITableViewDataSource,CLTableViewCellDelegate>

@property (nonatomic ,strong) UITableView *tableView;

/**CLplayer*/
@property (nonatomic, weak) CLPlayerView *playerView;
/**记录Cell*/
@property (nonatomic, assign) UITableViewCell *cell;

@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;

@property (nonatomic,strong) JYFindViewModel *findViewModel;

@end

@implementation JYFindViewController

static NSString *CLTableViewCellIdentifier = @"CLTableViewCellIdentifier";

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
    self.firstLevel = YES;
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestFindVideoList];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_playerView destroyPlayer];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//push到某个vc
- (void)pushToViewControllerClassName:(NSString *)className
{
    UIViewController *vc = [[NSClassFromString(className) alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    [self loadData];
    
    //测试
    JYBaseViewModel *vm = [[JYBaseViewModel alloc] init];
    [vm jkTestSuccess:^(NSString *msg, id responseData) {
        
    } failure:^(NSString *errorMsg) {
        
    } params:nil];
}

//用于初始化界面
- (void)configUI
{
    [self hideBackNavItem];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 180;
    
    self.tableView.sectionHeaderHeight = 49+(SCREEN_WIDTH-20)/730.0*330+10+(SCREEN_WIDTH-20)/730.0*200+10+49+10;
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:CellId bundle:nil] forCellReuseIdentifier:CellId];
    [self.tableView registerNib:[UINib nibWithNibName:HeaderId bundle:nil] forHeaderFooterViewReuseIdentifier:HeaderId];
    [self.tableView registerNib:[UINib nibWithNibName:FooterId bundle:nil] forHeaderFooterViewReuseIdentifier:FooterId];
    [_tableView registerClass:[CLTableViewCell class] forCellReuseIdentifier:CLTableViewCellIdentifier];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49+(SCREEN_WIDTH-20)/730.0*330+10+(SCREEN_WIDTH-20)/730.0*200+10+49+10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 60;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    WEAK(ws);
    JYFindHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderId];
    headerView.eventCallBackBlock = ^(NSString *eventType){
        if ([eventType isEqualToString:@"新人福利"]) {
            if (![self judgeLogin]) {
                return ;
            }
            JYGetCouponViewController *getCouponVC = [[JYGetCouponViewController alloc] init];
            getCouponVC.newUser = YES;
            [ws.navigationController pushViewController:getCouponVC animated:YES];
#warning TestCode Begin
            //测试代码-测试完毕需要删除
            
//            JYTestViewController *vc = [[JYTestViewController alloc] init];
//            [self.navigationController pushViewController:vc animated:YES];
//            return ;
#warning TestCode End
            
            
#warning TestCode Begin
//            //单聊界面
//            JYChatViewController *chatController = [[JYChatViewController alloc] initWithConversationChatter:@"admin" conversationType:EMConversationTypeChat];
//            chatController.chatTitle = @"客服";
//            [self.navigationController pushViewController:chatController animated:YES];
//            return ;
//#warning TestCode End
//
//            [ws pushToViewControllerClassName:@"JYXinRenFuLiViewController"];
        }

        if ([eventType isEqualToString:@"违章查询"]) {
            [ws pushToViewControllerClassName:@"JYWeiZhangQueryViewController"];
        }
        if ([eventType isEqualToString:@"爱车估值"]) {
            [ws pushToViewControllerClassName:@"JYCarGuZhiViewController"];
        }
        if ([eventType isEqualToString:@"限行查询"]) {
            [ws pushToViewControllerClassName:@"JYXianXingViewController"];
        }
        if ([eventType isEqualToString:@"今日油价"]) {
            [ws pushToViewControllerClassName:@"JYTodayOilPriceViewController"];
        }
        if ([eventType isEqualToString:@"违章高发地"]) {
            JYMapViewController * mapVC = [[JYMapViewController alloc] init];
            mapVC.currentMapMode = JYCurrentMapModeWeiZhangGaoFa;
            [ws.navigationController pushViewController:mapVC animated:YES];
            
//            [ws pushToViewControllerClassName:@"JYWeiZhangGaoFaViewController"];
        }
        if ([eventType isEqualToString:@"油卡充值"]) {
            if(![self judgeLogin]){
                return;
            }
            [ws pushToViewControllerClassName:@"JYOilCardChargeViewController"];
        }
        if ([eventType isEqualToString:@"应急电话"]) {
            [ws pushToViewControllerClassName:@"JYYingJiDianHuaViewController"];
        }
        if ([eventType isEqualToString:@"合作加盟"]) {
            [ws pushToViewControllerClassName:@"JYHeZuoViewController"];
        }
        if ([eventType isEqualToString:@"相关资讯"]) {
            [ws pushToViewControllerClassName:@"JYXiangGuanZiXunViewController"];
        }
    };
    return headerView;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    WEAK(ws);
    JYFindFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:FooterId];
    footerView.moreVideoBlock = ^{
        JYVideoListViewController *vc = [[JYVideoListViewController alloc] init];
        [ws.navigationController pushViewController:vc animated:YES];
    };
    return footerView;
}

#pragma mark ********* UITableViewDataSource *********
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 3;
//}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    JYMyFavoritesMvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    return cell;
//}
#pragma mark - ---------- Protocol Methods（代理方法） ----------
//- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    WEAKSELF
//    JYMyFavoritesMvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyFavoritesMvTableViewCell" forIndexPath:indexPath] ;
//    JYMyLoveMvModel * model =  self.viewModel.myLoveMvArray[indexPath.row];
//    cell.model = model;
//    cell.VideoCollectionBlock = ^(NSString *isCollect) {
//        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect];
//    };
//   return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 180;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.findViewModel.videoListArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    //    JYMyFavoritesMvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    JYMyLoveMvModel * model = self.viewModel.videoListArr[indexPath.row];
    //    cell.model = model;
    //    cell.VideoCollectionBlock = ^(NSString *isCollect) {
    //        if (![self judgeLogin]) {
    //            return ;
    //        }
    //        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
    //    };
    //    [cell showBottomLine:NO];
    //    return cell;
    
    CLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLTableViewCellIdentifier forIndexPath:indexPath];
    cell.indexPath = indexPath;
    CLModel *model = self.findViewModel.videoListArr[indexPath.row];
    cell.collectionBtnClickBlock = ^(NSString *isCollect){
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}
//在willDisplayCell里面处理数据能优化tableview的滑动流畅性，cell将要出现的时候调用
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    CLTableViewCell * myCell = (CLTableViewCell *)cell;
    myCell.model = self.findViewModel.videoListArr[indexPath.row];
    //Cell开始出现的时候修正偏移量，让图片可以全部显示
    [myCell cellOffset];
    //第一次加载动画
    
    [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:myCell.model.pictureUrl] completion:^(BOOL isInCache) {
        if (!isInCache) {
            //主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                CATransform3D rotation;//3D旋转
                rotation = CATransform3DMakeTranslation(0 ,50 ,20);
                //逆时针旋转
                rotation = CATransform3DScale(rotation, 0.8, 0.9, 1);
                rotation.m34 = 1.0/ -600;
                myCell.layer.shadowColor = [[UIColor blackColor]CGColor];
                myCell.layer.shadowOffset = CGSizeMake(10, 10);
                myCell.alpha = 0;
                myCell.layer.transform = rotation;
                [UIView beginAnimations:@"rotation" context:NULL];
                //旋转时间
                [UIView setAnimationDuration:0.6];
                myCell.layer.transform = CATransform3DIdentity;
                myCell.alpha = 1;
                myCell.layer.shadowOffset = CGSizeMake(0, 0);
                [UIView commitAnimations];
            });
        }
    }];
}
//cell离开tableView时调用
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //因为复用，同一个cell可能会走多次
    if ([_cell isEqual:cell]) {
        //区分是否是播放器所在cell,销毁时将指针置空
        [_playerView destroyPlayer];
        _cell = nil;
    }
}
#pragma mark - 点击播放代理
- (void)cl_tableViewCellPlayVideoWithCell:(CLTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF;
    //记录被点击的Cell
    _cell = cell;
    //销毁播放器
    [_playerView destroyPlayer];
    CLPlayerView *playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, cell.width, cell.height)];
    _playerView = playerView;
    [cell.contentView addSubview:_playerView];
    //    //重复播放，默认不播放
    //    _playerView.repeatPlay = YES;
    //    //当前控制器是否支持旋转，当前页面支持旋转的时候需要设置，告知播放器
    //    _playerView.isLandscape = YES;
    //    //设置等比例全屏拉伸，多余部分会被剪切
        _playerView.fillMode = ResizeAspect;
    //    //设置进度条背景颜色
    //    _playerView.progressBackgroundColor = [UIColor purpleColor];
    //    //设置进度条缓冲颜色
    //    _playerView.progressBufferColor = [UIColor redColor];
    //    //设置进度条播放完成颜色
    //    _playerView.progressPlayFinishColor = [UIColor greenColor];
    //    //全屏是否隐藏状态栏
    //    _playerView.fullStatusBarHidden = NO;
    //    //转子颜色
    //    _playerView.strokeColor = [UIColor redColor];
    //视频地址
    _playerView.url = [NSURL URLWithString:cell.model.videoUrl];
    _playerView.title = cell.model.videoTitle;
    _playerView.isCollect = cell.model.isCollect;
    //播放
    [_playerView playVideo];
    //返回按钮点击事件回调
    [_playerView backButton:^(UIButton *button) {
        NSLog(@"返回按钮被点击");
    }];
    //播放完成回调
    [_playerView endPlay:^{
        //销毁播放器
        [_playerView destroyPlayer];
        _playerView = nil;
        _cell = nil;
        NSLog(@"播放完成");
    }];
    
    _playerView.collectionBtnClickBlock = ^(NSString *isCollect) {
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:cell.model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
}
//#pragma mark - 滑动代理
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    // visibleCells 获取界面上能显示出来了cell
//    NSArray<CLTableViewCell *> *array = [self.tableView visibleCells];
//    //enumerateObjectsUsingBlock 类似于for，但是比for更快
//    [array enumerateObjectsUsingBlock:^(CLTableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [obj cellOffset];
//    }];
//}


#pragma mark ==================自定义方法==================
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

//收藏
- (void)requestCollectVideoWithVideoId:(NSString *)videoId andOperat:(NSString *)operat andIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"videoId"] = videoId;
    dic[@"operat"] = operat;
    [self.collectionViewModel requestCollectionWithParams:dic andType:JY_CollectionType_Video success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf reloadDataWithIndexPath:indexPath];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


//请求视频列表
- (void)requestFindVideoList{
    WEAKSELF
    [self.findViewModel requestGetFindVideoListSuccess:^(NSString *msg, id responseData) {
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


- (void)reloadDataWithIndexPath:(NSIndexPath *)indexPath{
//    JYMyLoveMvModel * model = self.findViewModel.videoListArr[indexPath.row];
    CLModel *model = self.findViewModel.videoListArr[indexPath.row];
    if ([model.isCollect intValue] == 1) {
        model.isCollect = @"2";
    }else{
        model.isCollect = @"1";
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (JYFindViewModel *)findViewModel{
    if(!_findViewModel){
        _findViewModel = [[JYFindViewModel alloc] init];
    }
    return _findViewModel;
}

- (JYCollectionViewModel *)collectionViewModel{
    if(!_collectionViewModel){
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}



@end
