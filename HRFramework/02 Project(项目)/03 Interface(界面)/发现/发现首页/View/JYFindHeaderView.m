//
//  JYFindHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYFindHeaderView.h"

@interface JYFindHeaderView ()

@property (weak, nonatomic) IBOutlet UIView *weizhangchaxunItemView;    //违章查询
@property (weak, nonatomic) IBOutlet UIView *aicheguzhiItemView;        //爱车估值
@property (weak, nonatomic) IBOutlet UIView *xianxingchaxunItemView;    //限行查询
@property (weak, nonatomic) IBOutlet UIView *xinrenfuliItemView;        //新人福利
@property (weak, nonatomic) IBOutlet UIView *jinriyoujiaItemView;       //今日油价
@property (weak, nonatomic) IBOutlet UIView *weizhanggaofaItemView;     //违章高发地
@property (weak, nonatomic) IBOutlet UIView *youkachongzhiItemView;     //油卡充值
@property (weak, nonatomic) IBOutlet UIView *yingjidianhuaItemView;     //应急电话

@property (weak, nonatomic) IBOutlet UIView *hezuoItemView;             //合作加盟

@property (weak, nonatomic) IBOutlet UIView *zixunItemView;             //相关资讯


@end

@implementation JYFindHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addGesture];
}

#pragma mark - ======================== Private Methods ========================

//回调block
- (void)invokeBlock:(NSString *)type
{
    if (!type) {
        return;
    }
    
    if (self.eventCallBackBlock) {
        self.eventCallBackBlock(type);
    }
}

//添加手势
- (void)addGesture
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(weizhangchaxunAction)];
    [self.weizhangchaxunItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(aicheguzhiAction)];
    [self.aicheguzhiItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(xianxingchaxunAction)];
    [self.xianxingchaxunItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(xinrenfuliAction)];
    [self.xinrenfuliItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jinriyoujiaAction)];
    [self.jinriyoujiaItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(weizhanggaofaAction)];
    [self.weizhanggaofaItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(youkachongzhiAction)];
    [self.youkachongzhiItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(yingjidianhuaAction)];
    [self.yingjidianhuaItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hezuoAction)];
    [self.hezuoItemView addGestureRecognizer:tap];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zixunAction)];
    [self.zixunItemView addGestureRecognizer:tap];
}

#pragma mark - ======================== Actions ========================

- (void)zixunAction
{
    [self invokeBlock:@"相关资讯"];
}

- (void)hezuoAction
{
    [self invokeBlock:@"合作加盟"];
}

//违章查询
- (void)weizhangchaxunAction
{
    [self invokeBlock:@"违章查询"];
}

//爱车估值
- (void)aicheguzhiAction
{
    [self invokeBlock:@"爱车估值"];
}

//限行查询
- (void)xianxingchaxunAction
{
    [self invokeBlock:@"限行查询"];
}

//新人福利
- (void)xinrenfuliAction
{
    [self invokeBlock:@"新人福利"];
}

//今日油价
- (void)jinriyoujiaAction
{
    [self invokeBlock:@"今日油价"];
}

//违章高发地查询
- (void)weizhanggaofaAction
{
    [self invokeBlock:@"违章高发地"];
}

//油卡充值
- (void)youkachongzhiAction
{
    [self invokeBlock:@"油卡充值"];
}

//应急电话
- (void)yingjidianhuaAction
{
    [self invokeBlock:@"应急电话"];
}


@end
