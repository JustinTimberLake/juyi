//
//  JYFindFooterView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYFindFooterView.h"

@implementation JYFindFooterView

#pragma mark - ======================== Life Cycle ========================

- (void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma mark - ======================== Actions ========================

//查看更多视频
- (IBAction)seeMoreVideo:(UIButton *)sender {
    if (self.moreVideoBlock) {
        self.moreVideoBlock();
    }
}

@end
