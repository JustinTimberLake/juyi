//
//  JYFindFooterView.h
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYFindFooterView : UITableViewHeaderFooterView

@property (nonatomic ,copy) void (^moreVideoBlock)();

@end
