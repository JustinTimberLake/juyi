//
//  JYFindViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/12/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYFindViewModel.h"
#import "CLModel.h"

@implementation JYFindViewModel

//发现首页视频列表（4个）
- (void)requestGetFindVideoListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_FIND_GetFindVideoList) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
            [weakSelf.videoListArr removeAllObjects];
            NSArray * arr = [CLModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.videoListArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray *)videoListArr{
    if(!_videoListArr){
        _videoListArr = [NSMutableArray array];
    }
    return _videoListArr;
}


@end
