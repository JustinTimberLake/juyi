//
//  JYLocationVM.h
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYProvinceModel.h"     //省模型
#import "JYCityModel.h"         //城市模型

@interface JYLocationVM : JYBaseViewModel

//请求市信息
- (void)requestCityDataWithProvinceCode:(NSString *)provinceCode success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//请求省信息
- (void)requestProvinceDataSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
