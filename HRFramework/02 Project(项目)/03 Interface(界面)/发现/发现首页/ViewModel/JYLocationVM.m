//
//  JYLocationVM.m
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYLocationVM.h"

@implementation JYLocationVM

//获取热门城市

//获取市
- (void)requestCityDataWithProvinceCode:(NSString *)provinceCode success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    if (!provinceCode) {
        failureBlock(@"参数provinceCode为空!");
        return;
    }
    
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    NSDictionary *params = @{@"provinceCode":provinceCode};
    
    [manager POST_URL:JY_PATH(JY_LOCATION_GetCity) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *dataArr = [result objectForKey:@"data"];
            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
            
            //一般是遍历
            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                JYCityModel *model = [JYCityModel mj_objectWithKeyValues:obj];
                [modelArr addObject:model];
            }];
            
            successBlock(msg,modelArr);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取省/location/getProvince
- (void)requestProvinceDataSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_LOCATION_GetProvince) params:nil success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *dataArr = [result objectForKey:@"data"];
            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
            for (NSDictionary *dic in dataArr) {
                JYProvinceModel *model = [JYProvinceModel mj_objectWithKeyValues:dic];
                [modelArr addObject:model];
            }
            successBlock(msg,modelArr);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
