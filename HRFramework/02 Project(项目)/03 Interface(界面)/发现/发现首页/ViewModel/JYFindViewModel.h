//
//  JYFindViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYFindViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *videoListArr;

//发现首页视频列表（4个）
- (void)requestGetFindVideoListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
