//
//  JYJUHEXianXingViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYJUHEXianXingViewModel.h"
#import "JYXianXingCityModel.h"

@implementation JYJUHEXianXingViewModel
//获取限行城市
- (void)requestGetXianXingCitySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
     HRRequestManager * managr = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_JUHE_GetRestrictCity));
    [managr POST_URL:JY_PATH(JY_JUHE_GetRestrictCity) params:nil success:^(id result) {
        if(RESULT_SUCCESS){
            NSLog(@"%@",result);
            NSArray * arr =  [JYXianXingCityModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.cityArr addObjectsFromArray:arr];
                successBlock(RESULT_MESSAGE,nil);
            }else{
                failureBlock(RESULT_MESSAGE);
            }

    } failure:^(NSDictionary *errorInfo) {
         failureBlock(ERROR_MESSAGE);
    }];
    
}

//获取限行城市查询结果
- (void)requestXianXingQueryWithCityCode:(NSString *)cityCode andType:(NSString *)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"city"] = cityCode;
    dic[@"type"] = type;
    HRRequestManager * managr = [[HRRequestManager alloc] init];
    [managr POST_URL:JY_PATH(JY_JUHE_RestrictQuery) params:dic success:^(id result) {
        if(RESULT_SUCCESS){
            weakSelf.queryModel = [JYXianXingQueryModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

#pragma mark -------------------懒加载------------------------

- (NSMutableArray *)cityArr{
    if(!_cityArr){
        _cityArr = [NSMutableArray array];
    }
    return _cityArr;
}

                             
@end
                             
