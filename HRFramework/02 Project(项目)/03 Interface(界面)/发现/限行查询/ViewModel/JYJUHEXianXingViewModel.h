//
//  JYJUHEXianXingViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYXianXingQueryModel.h"

@interface JYJUHEXianXingViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *cityArr;
@property (nonatomic,strong) JYXianXingQueryModel *queryModel;

//获取限行城市
- (void)requestGetXianXingCitySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取限行城市查询结果
- (void)requestXianXingQueryWithCityCode:(NSString *)cityCode andType:(NSString *)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
