//
//  JYXianXingResultViewController.m
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYXianXingResultViewController.h"

@interface JYXianXingResultViewController ()
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *weekLab;
@property (weak, nonatomic) IBOutlet UILabel *xianxingDetailLab;
@property (weak, nonatomic) IBOutlet UILabel *xianxingTimeDetailLab;
@property (weak, nonatomic) IBOutlet UILabel *areaDetailLab;
@property (weak, nonatomic) IBOutlet UILabel *todayXianXingLab;
@property (weak, nonatomic) IBOutlet UILabel *xianXingNumLab;
@property (weak, nonatomic) IBOutlet UILabel *holidayDetailLab;
@end

@implementation JYXianXingResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configUI{
    self.naviTitle = @"查询结果";
    self.timeLab.text = self.model.date;
    self.weekLab.text = self.model.week;
    self.xianxingDetailLab.text = self.model.des;
    self.xianxingTimeDetailLab.text = self.model.time;
    self.areaDetailLab.text = self.model.place;
    self.xianXingNumLab.text = self.model.xxweihao;
    self.todayXianXingLab.text = self.model.xxweihao.length ? @"是":@"否";
    self.holidayDetailLab.text = self.model.holiday;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
