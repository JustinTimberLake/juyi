//
//  JYXianXingViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYXianXingViewController.h"
#import "JYXianXingTableViewCell.h"
#import "JYCarGuZhiFooterView.h"
#import "JYPickerViewAlertController.h"
#import "JYXianXingResultViewController.h"
#import "JYJUHEXianXingViewModel.h"
#import "JYXianXingCityModel.h"

static NSString * cellId = @"JYXianXingTableViewCell";

@interface JYXianXingViewController ()<
    UITableViewDelegate,
    UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) NSArray *leftTitleArr;
@property (nonatomic,copy) NSString *selectCityName;
@property (nonatomic,copy) NSString *selectCityCode;
@property (nonatomic,copy) NSString *selectDataName;
@property (nonatomic,copy) NSString *selectData;
@property (nonatomic,strong) JYJUHEXianXingViewModel *viewModel;
@property (nonatomic,strong) NSMutableArray *cityNameArr;
@property (nonatomic,strong) NSMutableArray *timeArr;
@end

@implementation JYXianXingViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
    [self requestGetXianXingCity];
    [self configTimeArr];
}

//用于初始化界面
- (void)configUI
{
    WEAKSELF
    self.naviTitle = @"限行查询";
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYXianXingTableViewCell" bundle:nil] forCellReuseIdentifier:cellId];
    JYCarGuZhiFooterView * footerView = LOADXIB(@"JYCarGuZhiFooterView");
    [footerView setBtnTitle:@"查询"];
    footerView.evaluateBlock = ^{
        [weakSelf requestGetQuery];
    };
    self.myTableView.tableFooterView = footerView;
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api
- (void)requestGetXianXingCity{
    WEAKSELF
    [self.viewModel requestGetXianXingCitySuccess:^(NSString *msg, id responseData) {
        if (weakSelf.viewModel.cityArr.count) {
            [weakSelf.cityNameArr removeAllObjects];
            for (JYXianXingCityModel * model in weakSelf.viewModel.cityArr) {
                [weakSelf.cityNameArr addObject:model.cityname];
            }
            weakSelf.selectCityName = weakSelf.cityNameArr [0];
            JYXianXingCityModel * model = weakSelf.viewModel.cityArr[0];
            weakSelf.selectCityCode = model.city;
        }
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)requestGetQuery{
    WEAKSELF
    if (!self.selectCityCode.length) {
        [self showSuccessTip:@"请选择城市"];
        return;
    }

    [self.viewModel requestXianXingQueryWithCityCode:self.selectCityCode  andType:self.selectData  success:^(NSString *msg, id responseData) {
        
        JYXianXingResultViewController * resultVC = [[JYXianXingResultViewController alloc] init];
        resultVC.model = weakSelf.viewModel.queryModel;
        [weakSelf.navigationController pushViewController:resultVC animated:YES];

    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark -------------------时间的方法------------------------

//获取查询时间
- (void)configTimeArr{
   NSString * today =  [self getCurrentTime];
   NSString * tomorrow = SF(@"明天 （%@）", [self GetDay:[NSDate date] withAfterDay:1]);
   NSString * afterTomorrow = SF(@"后天 （%@）", [self GetDay:[NSDate date] withAfterDay:2]);
    [self.timeArr addObject:today];
    [self.timeArr addObject:tomorrow];
    [self.timeArr addObject:afterTomorrow];
//    设置默认时间
    self.selectDataName = today;
    self.selectData = @"1";
}

//获取当地时间
- (NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return SF(@"今天 （%@）",dateTime);
}
////将字符串转成NSDate类型
//- (NSDate *)dateFromString:(NSString *)dateString {
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
//    NSDate *destDate= [dateFormatter dateFromString:dateString];
//    return destDate;
//}
//传入今天的时间，返回明天的时间
- (NSString *)GetDay:(NSDate *)aDate withAfterDay:(int)afterDay {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:aDate];
    [components setDay:([components day]+afterDay)];
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"MM-dd"];
    return [dateday stringFromDate:beginningOfWeek];
}

#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    self.leftTitleArr = @[@"限行城市",@"查询时间"];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

#pragma mark ==================UITableView 数据源和代理 ==================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JYXianXingTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    cell.leftTitleLab.text = self.leftTitleArr[indexPath.row];
    if (self.selectCityName.length && indexPath.row == 0) {
        cell.contentLab.text = self.selectCityName;
    }else if (self.selectDataName.length && indexPath.row == 1){
        cell.contentLab.text = self.selectDataName;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    NSArray * strArr;
    if (indexPath.row == 0) {
        strArr = weakSelf.cityNameArr;
    }else{
        strArr = self.timeArr;
    }
    
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [alertVC updatePickerViewDataArr:strArr type:PickerViewTypeNormal];
    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
        switch (indexPath.row) {
            case 0:{
                weakSelf.selectCityName =  strArr[selectIndex];
                JYXianXingCityModel * model = weakSelf.viewModel.cityArr[selectIndex];
                weakSelf.selectCityCode = model.city;
            }
                break;
            case 1:
                weakSelf.selectDataName =  strArr[selectIndex];
                weakSelf.selectData = SF(@"%ld",selectIndex + 1);
                break;
    
            default:
                break;
        }
        [weakSelf.myTableView reloadData];
    };
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark -------------------懒加载------------------------

- (JYJUHEXianXingViewModel *)viewModel{
    if(!_viewModel){
        _viewModel = [[JYJUHEXianXingViewModel alloc] init];
    }
    return _viewModel;
}

- (NSMutableArray *)cityNameArr{
    if(!_cityNameArr){
        _cityNameArr = [NSMutableArray array];
    }
    return _cityNameArr;
}

- (NSMutableArray *)timeArr{
    if(!_timeArr){
        _timeArr = [NSMutableArray array];
    }
    return _timeArr;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
