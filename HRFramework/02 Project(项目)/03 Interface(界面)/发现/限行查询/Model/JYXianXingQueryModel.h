//
//  JYXianXingQueryModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYXianXingQueryModel : JYBaseModel
// 2017-12-07  // 日期
@property (nonatomic,copy) NSString *date;
//:星期四    //星期
@property (nonatomic,copy) NSString *week;

//:beijing    //城市代码
@property (nonatomic,copy) NSString *city;

//:北京  //城市名称
@property (nonatomic,copy) NSString *cityname;

//:停驶时间为0时至24时,停驶范围为本市行政区域内所有///*限行描述*/
@property (nonatomic,copy) NSString *des;

//:停驶时间为0时至24时   /*限行时间*/
@property (nonatomic,copy) NSString *time;

//:     限行区域
@property (nonatomic,copy) NSString *place;

//:是限行尾号
@property (nonatomic,copy) NSString *xxweihao;

//:节假信息
@property (nonatomic,copy) NSString *holiday;


@end
