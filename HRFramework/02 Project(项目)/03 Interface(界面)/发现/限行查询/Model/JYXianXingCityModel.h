//
//  JYXianXingCityModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYXianXingCityModel : JYBaseModel
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *cityname;
@end
