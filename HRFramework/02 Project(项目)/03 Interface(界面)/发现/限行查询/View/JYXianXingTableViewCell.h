//
//  JYXianXingTableViewCell.h
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYXianXingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@end
