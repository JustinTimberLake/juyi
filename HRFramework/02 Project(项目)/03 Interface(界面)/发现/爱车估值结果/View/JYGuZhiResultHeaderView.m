//
//  JYGuZhiResultHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGuZhiResultHeaderView.h"

@interface JYGuZhiResultHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *buyPriceLabel;    //收购价格
@property (weak, nonatomic) IBOutlet UILabel *tradePriceLabel;  //个人交易价格
@property (weak, nonatomic) IBOutlet UILabel *salePriceLabel;   //卖出价格

@end

@implementation JYGuZhiResultHeaderView

//- (void)updateHeaderViewModel:(JYCarEvaluateResultModel *)model
//{
//    self.buyPriceLabel.text = [NSString stringWithFormat:@"收购价格：%@",model.buyPrice];
//    self.tradePriceLabel.text = SF(@"个人交易价格：%@",model.tradePrice);
//    self.salePriceLabel.text = SF(@"卖出价格：%@",model.sellPrice);
//}
//

- (void)updataHeaderViewWithData:(id)data{
    NSArray * arr = data;
    NSNumber *firstStr = arr[0];
    NSNumber *secondStr = arr[1];
    NSNumber *thirdStr = arr[2];
    
    self.buyPriceLabel.text =  firstStr>0 ?  [NSString stringWithFormat:@"收购价格：%@",firstStr]:@"";
    self.tradePriceLabel.text =  secondStr > 0 ? SF(@"个人交易价格：%@",secondStr) : @"";
    self.salePriceLabel.text =  thirdStr >0 ? SF(@"卖出价格：%@",thirdStr):@"";
}
@end
