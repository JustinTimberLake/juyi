//
//  JYGuZhiResultHeaderView.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYCarEvaluateResultModel.h"

@interface JYGuZhiResultHeaderView : UITableViewHeaderFooterView

//- (void)updateHeaderViewModel:(JYCarEvaluateResultModel *)model;

- (void)updataHeaderViewWithData:(id)data;

@end
