//
//  JYGuZhiResultTableViewCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYCarAreaPriceModel.h"
//#import "JYCarPriceModel.h"


@interface JYGuZhiResultTableViewCell : UITableViewCell

//- (void)updateCellModel:(JYCarPriceModel *)model;

- (void)updateCellModel:(JYCarAreaPriceModel *)model;


@end
