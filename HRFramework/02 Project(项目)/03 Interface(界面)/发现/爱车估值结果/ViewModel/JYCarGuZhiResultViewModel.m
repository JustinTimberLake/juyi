//
//  JYCarGuZhiResultViewModel.m
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCarGuZhiResultViewModel.h"

@implementation JYCarGuZhiResultViewModel

- (void)requestCarEvaluateParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_FIND_Valuation) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSDictionary *dataDic = [result objectForKey:@"data"];
            
            JYCarEvaluateResultModel *model = [JYCarEvaluateResultModel mj_objectWithKeyValues:dataDic];
            successBlock(msg,model);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
