//
//  JYCarGuZhiResultViewModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYCarEvaluateResultModel.h"

@interface JYCarGuZhiResultViewModel : JYBaseViewModel

/**
 请求爱车估值
 
 @param params 估值参数
 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestCarEvaluateParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
