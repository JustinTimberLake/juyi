//
//  JYCarEvaluateResultModel.m
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCarEvaluateResultModel.h"

@implementation JYCarEvaluateResultModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"cityPrices":@"JYCarPriceModel"};
}

@end
