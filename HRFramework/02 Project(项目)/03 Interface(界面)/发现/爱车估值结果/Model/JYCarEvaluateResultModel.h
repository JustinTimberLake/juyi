//
//  JYCarEvaluateResultModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYCarPriceModel.h"

@interface JYCarEvaluateResultModel : JYBaseModel

@property (nonatomic ,copy) NSString *buyPrice;         //收购价格
@property (nonatomic ,copy) NSString *tradePrice;       //个人交易价格
@property (nonatomic ,copy) NSString *sellPrice;        //卖出价格
@property (nonatomic ,strong) NSArray <JYCarPriceModel *>*cityPrices;

@end
