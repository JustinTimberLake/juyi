//
//  JYCarPriceModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYCarPriceModel : JYBaseModel

@property (nonatomic ,copy) NSString *cityTitle;        //城市
@property (nonatomic ,copy) NSString *cityPrice;        //价格

@end
