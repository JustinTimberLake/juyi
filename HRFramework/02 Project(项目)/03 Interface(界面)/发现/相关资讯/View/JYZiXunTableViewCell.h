//
//  JYZiXunTableViewCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYZiXunModel;

@interface JYZiXunTableViewCell : UITableViewCell

- (void)updateCellModel:(JYZiXunModel *)model;

@end
