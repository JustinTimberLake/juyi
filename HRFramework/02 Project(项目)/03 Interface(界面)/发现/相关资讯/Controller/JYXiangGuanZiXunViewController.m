//
//  JYXiangGuanZiXunViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYXiangGuanZiXunViewController.h"
#import "JYXiangGuanZiXunDetailViewController.h"    //相关资讯详情
#import "JYZiXunTableViewCell.h"
#import "JYZiXunModel.h"
#import "JYZiXunViewModel.h"
#import "JYCommonWebViewController.h"

static NSString *const CellId = @"JYZiXunTableViewCell";

@interface JYXiangGuanZiXunViewController ()<
    UITableViewDelegate,
    UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) JYZiXunViewModel *viewModel;

@end

@implementation JYXiangGuanZiXunViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configUI];
    [self config];
    [self setUpRefreshData];
    [self requestZiXunListWithIsMore:NO];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
//    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"资讯";
    
    self.tableView.rowHeight = 111;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:CellId bundle:nil] forCellReuseIdentifier:CellId];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.listArr.count;
//    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYZiXunTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    [cell updateCellModel:self.viewModel.listArr[indexPath.row]];
//    [cell updateCellModel:self.dataArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JYZiXunModel * model = self.viewModel.listArr[indexPath.row];
    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
    webVC.url = model.newsUrl;
    webVC.navTitle = @"资讯详情";
    [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
//- (void)loadData
//{
//    JYZiXunModel *model = [[JYZiXunModel alloc] init];
//    [self.dataArr addObjectsFromArray:@[model,model]];
//}

- (void)setUpRefreshData{
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestZiXunListWithIsMore:NO];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestZiXunListWithIsMore:YES];
    }];
//    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestZiXunListWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetNewsListWithIsMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf endRefresh];
    }];
}




#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

//- (NSMutableArray *)dataArr
//{
//    if (!_dataArr) {
//        _dataArr = [[NSMutableArray alloc] init];
//    }
//    return _dataArr;
//}


- (JYZiXunViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYZiXunViewModel alloc] init];
    }
    return _viewModel;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
