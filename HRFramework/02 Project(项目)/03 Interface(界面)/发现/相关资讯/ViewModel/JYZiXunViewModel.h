//
//  JYZiXunViewModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYZiXunViewModel : JYBaseViewModel


@property (nonatomic,strong) NSMutableArray *listArr;
//JY-010-006 获取资讯列表
- (void)requestGetNewsListWithIsMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
