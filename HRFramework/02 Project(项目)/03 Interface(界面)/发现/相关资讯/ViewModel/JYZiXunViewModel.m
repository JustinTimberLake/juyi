//
//  JYZiXunViewModel.m
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYZiXunViewModel.h"
#import "JYZiXunModel.h"

@implementation JYZiXunViewModel
//JY-010-006 获取资讯列表
- (void)requestGetNewsListWithIsMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    NSString * pageCount;
    if (self.listArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+2 ] : @"1";
    }
    params[@"pageNum"] = pageCount;
    params[@"pageSize"] = @(10);
    
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_FIND_GetNewsList) params:params success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.listArr removeAllObjects];
            }
            NSArray * arr = [JYZiXunModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}

@end
