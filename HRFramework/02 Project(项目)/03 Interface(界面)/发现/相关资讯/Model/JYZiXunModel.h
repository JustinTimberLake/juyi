//
//  JYZiXunModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYZiXunModel : JYBaseModel

//@property (nonatomic ,copy) NSString *img;
//@property (nonatomic ,copy) NSString *title;
//@property (nonatomic ,copy) NSString *time;

//资讯ID
@property (nonatomic,copy) NSString *newsId;
//标题
@property (nonatomic,copy) NSString *newsTitle;
//图片
@property (nonatomic,copy) NSString *newsImage;
//时间
@property (nonatomic,copy) NSString *newsTime;
//详情URL
@property (nonatomic,copy) NSString *newsUrl;


@end
