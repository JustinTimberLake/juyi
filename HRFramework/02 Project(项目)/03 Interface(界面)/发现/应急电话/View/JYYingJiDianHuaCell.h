//
//  JYYingJiDianHuaCell.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYYingJiTelModel.h"
@interface JYYingJiDianHuaCell : UITableViewCell

@property (nonatomic,strong) JYYingJiTelModel *model;

- (void)updataCellImageWithIndex:(NSInteger)index;
@end
