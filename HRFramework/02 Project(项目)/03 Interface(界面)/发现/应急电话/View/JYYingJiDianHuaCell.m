//
//  JYYingJiDianHuaCell.m
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYYingJiDianHuaCell.h"

@interface JYYingJiDianHuaCell ()

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (nonatomic,strong) NSArray *imageArr;

@end
@implementation JYYingJiDianHuaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imageArr = @[@"绿色用户客服图标",@"橘色商户客服图标",@"救援电话"];
}

- (void)setModel:(JYYingJiTelModel *)model{
    _model = model;
    self.titleLab.text = model.emergencyTitle;
    self.telLab.text = model.emergencyPhone;
}

- (void)updataCellImageWithIndex:(NSInteger)index{
    int row = index % 4;
    self.headerImageView.image = [UIImage imageNamed:self.imageArr[row]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
