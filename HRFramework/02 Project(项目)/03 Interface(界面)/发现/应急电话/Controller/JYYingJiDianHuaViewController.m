//
//  JYYingJiDianHuaViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYYingJiDianHuaViewController.h"
#import "JYYingJiDianHuaCell.h"
#import "JYYingJiDianHuaViewModel.h"
#import "HRCall.h"

static NSString *const cellId = @"JYYingJiDianHuaCell";

@interface JYYingJiDianHuaViewController ()<
    UITableViewDataSource,
    UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYYingJiDianHuaViewModel *viewModel;

@end

@implementation JYYingJiDianHuaViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
    [self requestEmergencyData];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"应急电话";
    
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYYingJiDianHuaCell" bundle:nil] forCellReuseIdentifier:cellId];
    
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    
}

//请求紧急数据
- (void)requestEmergencyData{
    WEAKSELF
    [self.viewModel requestEmergencySuccess:^(NSString *msg, id responseData) {
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================



#pragma mark ==================UITableView 数据源和代理 ==================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYYingJiDianHuaCell * telCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    telCell.model = self.viewModel.listArr[indexPath.row];
    [telCell updataCellImageWithIndex:indexPath.row];
    return telCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JYYingJiTelModel * model = self.viewModel.listArr[indexPath.row];
    [HRCall callPhoneNumber:model.emergencyPhone alert:YES];

}

- (JYYingJiDianHuaViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYYingJiDianHuaViewModel alloc] init];
    }
    return _viewModel;
}


@end
