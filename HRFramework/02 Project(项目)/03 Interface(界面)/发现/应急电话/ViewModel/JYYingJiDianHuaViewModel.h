//
//  JYYingJiDianHuaViewModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYYingJiDianHuaViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;

//应急电话请求
- (void)requestEmergencySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
