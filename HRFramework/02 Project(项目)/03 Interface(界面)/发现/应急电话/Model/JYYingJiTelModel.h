//
//  JYYingJiTelModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYYingJiTelModel : JYBaseModel
// 标题
@property (nonatomic,strong) NSString *emergencyTitle;
// 电话号
@property (nonatomic,strong) NSString *emergencyPhone;

@end
