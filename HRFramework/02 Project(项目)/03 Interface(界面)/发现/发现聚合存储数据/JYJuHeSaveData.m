//
//  JYJuHeSaveData.m
//  JY
//
//  Created by Duanhuifen on 2017/11/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYJuHeSaveData.h"


static JYJuHeSaveData *instance;

@implementation JYJuHeSaveData

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[super allocWithZone:NULL]init];
    });
    return instance;
}

+(instancetype)allocWithZone:(struct _NSZone *)zone{
    return [[self class] shareInstance
            ];
}

-(instancetype)copyWithZone:(struct _NSZone *)zone{
    return [[self class] shareInstance];
}

- (void)saveJuHeProvinceCityDataWithData:(id)data{
//    [CZArchiverOrUnArchiver saveArchiverData:data fileName:JY_JUHE_ACHIVEDATA_WEIZHANG];
}



@end
