//
//  JYWeiZhangQueryResultCell.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWeiZhangQueryResultCell.h"

@interface JYWeiZhangQueryResultCell ()

@property (weak, nonatomic) IBOutlet UILabel *weizhangTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *weizhangLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *weizhangTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *weizhangResultLabel;
@property (weak, nonatomic) IBOutlet UILabel *weizhangStatusLabel;


@end

@implementation JYWeiZhangQueryResultCell

- (void)updateCellModel:(JYWeizhangResultModel *)model
{
    self.weizhangTimeLabel.text = [NSString stringWithFormat:@"违章时间：%@",model.date];
    self.weizhangLocationLabel.text = [NSString stringWithFormat:@"违章地点：%@",model.area];
    self.weizhangTypeLabel.text = [NSString stringWithFormat:@"违章行为：%@",model.act];
    
    //罚款100，扣分3
    NSString *orgStr = [NSString stringWithFormat:@"罚款%@   扣分%@",model.money,model.fen];
    NSRange fineRange = NSMakeRange(2, model.money.length);
    NSRange scoreRange = NSMakeRange(2+model.money.length+3+2, model.fen.length);
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:orgStr];
    [attStr addAttributes:@{
                           NSFontAttributeName:[UIFont systemFontOfSize:17],
                           NSForegroundColorAttributeName:[UIColor colorWithHexString:@"de2529"]
                           } range:fineRange];
    [attStr addAttributes:@{
                            NSFontAttributeName:[UIFont systemFontOfSize:17],
                            NSForegroundColorAttributeName:[UIColor colorWithHexString:@"de2529"]
                            } range:scoreRange];
    
    self.weizhangResultLabel.attributedText = attStr;
    
//    是否处理,1处理 0未处理 空未知
    
    if ([model.handled isEqualToString:@"0"]) {
        self.weizhangStatusLabel.text = @"未缴费";
    }
    if ([model.handled isEqualToString:@"1"]) {
        self.weizhangStatusLabel.text = @"已缴费";
    }
}

@end
