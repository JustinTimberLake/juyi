//
//  JYWeiZhangQueryResultCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "JYWeiZhangQueryResultModel.h"
#import "JYWeizhangResultModel.h"

@interface JYWeiZhangQueryResultCell : UITableViewCell

- (void)updateCellModel:(JYWeizhangResultModel *)model;

@end
