//
//  JYWeiZhangQueryResultViewModel.m
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWeiZhangQueryResultViewModel.h"
#import "JYWeizhangResultModel.h"


@implementation JYWeiZhangQueryResultViewModel

//- (void)requestWeiZhangWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
//{
//    HRRequestManager *manager = [[HRRequestManager alloc] init];
//    [manager POST_URL:JY_PATH(JY_FIND_GetIllegalList) params:params success:^(id result) {
//        NSString *msg = [result objectForKey:@"errorMsg"];
//        if ([[result objectForKey:@"status"] boolValue]) {
//            //将数据回调
//            NSArray *dataArr = [result objectForKey:@"data"];
//            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
//            
//            //一般是遍历
//            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                JYWeiZhangQueryResultModel *model = [JYWeiZhangQueryResultModel mj_objectWithKeyValues:obj];
//                [modelArr addObject:model];
//            }];
//            
//            successBlock(msg,modelArr);
//        }
//        else
//        {
//            //返回错误信息
//            failureBlock(msg);
//        }
//    } failure:^(NSDictionary *errorInfo) {
//        failureBlock(NET_NOT_WORK);
//    }];
//}

//违章查询结果
- (void)requestJuHeWeizhangQueryWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_JUHE_WEIZHANGCHAXUN_QUERY_URL params:params success:^(id result) {
        NSLog(@"%@",result);
        [weakSelf.weizhangListArr removeAllObjects];
        if ([result[@"resultcode"] intValue] == 200) {
            NSArray * arr = [JYWeizhangResultModel mj_objectArrayWithKeyValuesArray:result[@"result"][@"lists"]];
            [weakSelf.weizhangListArr addObjectsFromArray:arr];
            successBlock(result[@"reason"],nil);
        }else{
            failureBlock(result[@"reason"]);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
    
}

- (NSMutableArray *)weizhangListArr
{
    if (!_weizhangListArr) {
        _weizhangListArr = [NSMutableArray array];
    }
    return _weizhangListArr;
}


@end
