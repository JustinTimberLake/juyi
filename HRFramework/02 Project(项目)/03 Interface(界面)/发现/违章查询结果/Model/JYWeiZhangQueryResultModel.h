//
//  JYWeiZhangQueryResultModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYWeiZhangQueryResultModel : JYBaseModel

@property (nonatomic ,copy) NSString *illegalId;        //违章id
@property (nonatomic ,copy) NSString *illegalTime;        //违章时间
@property (nonatomic ,copy) NSString *illegalAddress;        //违章地点
@property (nonatomic ,copy) NSString *illegalBehavior;        //违章行为
@property (nonatomic ,copy) NSString *illegalFine;        //罚款
@property (nonatomic ,copy) NSString *illegalScore;        //扣分
@property (nonatomic ,copy) NSString *illegalState;        //状态1.未处理。2已处理

@end
