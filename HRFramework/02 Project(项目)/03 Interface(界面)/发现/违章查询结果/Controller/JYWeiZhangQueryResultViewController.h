//
//  JYWeiZhangQueryResultViewController.h
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//  违章查询结果

#import "JYBaseViewController.h"

@interface JYWeiZhangQueryResultViewController : JYBaseViewController

@property (nonatomic ,strong) NSDictionary *params;

@end
