//
//  JYWeiZhangQueryResultViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWeiZhangQueryResultViewController.h"
#import "JYWeiZhangQueryResultCell.h"
#import "JYWeiZhangQueryResultViewModel.h"
#import "SNStarsAlertView.h"

static NSString *const CellId = @"JYWeiZhangQueryResultCell";

@interface JYWeiZhangQueryResultViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) JYWeiZhangQueryResultViewModel *rootVM;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

@implementation JYWeiZhangQueryResultViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self config];
    [self configUI];
    [self loadData];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

////用于初始化一些配置(注册通知、注册观察者…)
//- (void)config
//{
//    //初始化vm
//
//    [self loadData];
//}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"违章查询结果";
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:CellId bundle:nil] forCellReuseIdentifier:CellId];
    self.tableView.rowHeight = 140;
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return self.dataArr.count;
    return self.rootVM.weizhangListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYWeiZhangQueryResultCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    [cell updateCellModel:self.rootVM.weizhangListArr[indexPath.row]];
    return cell;
}

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    WEAKSELF
    [self showHUD];
    
    [self.rootVM requestJuHeWeizhangQueryWithParams:self.params success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        if (self.rootVM.weizhangListArr.count == 0) {
            SNStarsAlertView *alert = [[SNStarsAlertView alloc]initWithTitle:@"提示" message:@"暂无违章信息" cancelButtonTitle:@"确定" otherButtonTitle:nil cancelButtonClick:^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            } otherButtonClick:nil];
            [alert show];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (JYWeiZhangQueryResultViewModel *)rootVM
{
    if (!_rootVM) {
        _rootVM = [[JYWeiZhangQueryResultViewModel alloc] init];
    }
    return _rootVM;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
