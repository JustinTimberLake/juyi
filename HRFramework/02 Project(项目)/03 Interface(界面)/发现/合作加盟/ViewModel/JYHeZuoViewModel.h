//
//  JYHeZuoViewModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYHeZuoViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;


//3.10.1.	JY-010-004 获取合作加盟列表
- (void)requestGetUnionsListIsMore:(BOOL)isMore Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
