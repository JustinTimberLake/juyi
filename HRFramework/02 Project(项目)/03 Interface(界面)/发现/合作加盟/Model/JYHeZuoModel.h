//
//  JYHeZuoModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYImageModel.h"


@interface JYHeZuoModel : JYBaseModel

//@property (nonatomic ,copy) NSString *title;        //标题
//@property (nonatomic ,copy) NSString *time;        //时间
//@property (nonatomic ,copy) NSString *content;        //内容
//@property (nonatomic ,strong) NSArray *imgArr;

// 加盟ID
@property (nonatomic,copy) NSString *unionsId;
//标题
@property (nonatomic,copy) NSString *unionsTitle;
//时间
@property (nonatomic,copy) NSString *unionsTime;
//内容
@property (nonatomic,copy) NSString *unionsContent;
//图片
@property (nonatomic,strong) NSArray<JYImageModel *> *unionsImages;
//详情URL
@property (nonatomic,copy) NSString *unionsContentUrl;

@end
