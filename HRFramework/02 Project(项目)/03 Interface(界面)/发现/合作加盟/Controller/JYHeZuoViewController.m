//
//  JYHeZuoViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHeZuoViewController.h"
#import "JYHeZuoCell.h"
#import "JYHeZuoModel.h"
#import "JYHeZuoDetailViewController.h"
#import "JYCommonWebViewController.h"
#import "JYHeZuoViewModel.h"

static NSString *const CellId = @"JYHeZuoCell";

@interface JYHeZuoViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) JYHeZuoViewModel *viewModel;

@end

@implementation JYHeZuoViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configUI];
    [self config];
    [self setUpRefreshData];
    [self requestHeZuoListWithIsMore:NO];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
//    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"合作加盟";
    
    [self.tableView registerClass:[JYHeZuoCell class] forCellReuseIdentifier:CellId];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_GRAY_BG];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark -------------网络请求-------------

- (void)setUpRefreshData{
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestHeZuoListWithIsMore:NO];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestHeZuoListWithIsMore:YES];
    }];
//    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestHeZuoListWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetUnionsListIsMore:isMore Success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.tableView reloadData];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf endRefresh];
    }];
}


#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    JYHeZuoCell* cell = (JYHeZuoCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.returnImageTapIndexpathBlock = ^(NSIndexPath *indexPath) {
//
//    };
    
    
    JYHeZuoModel * model = self.viewModel.listArr[indexPath.row];
    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
    webVC.url = model.unionsContentUrl;
    webVC.navTitle = @"合作详情";
    [self.navigationController pushViewController:webVC animated:YES];

//    应该是网页暂时注释
//    JYHeZuoDetailViewController *vc = [[JYHeZuoDetailViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  [JYHeZuoCell cellHeightAccordingModel:self.viewModel.listArr[indexPath.row]];
//    return [JYHeZuoCell cellHeightAccordingModel:self.dataArr[indexPath.row]];
}

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.listArr.count;
//    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAK(weakSelf)
    JYHeZuoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    [cell updateCellModel:self.viewModel.listArr[indexPath.row] andIndexPath:indexPath];
    cell.returnImageTapIndexpathBlock = ^(NSIndexPath *indexPath) {
        JYHeZuoModel * model = weakSelf.viewModel.listArr[indexPath.row];
        JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
        webVC.url = model.unionsContentUrl;
        webVC.navTitle = @"合作详情";
        [weakSelf.navigationController pushViewController:webVC animated:YES];

    };
//    [cell updateCellModel:self.dataArr[indexPath.row]];
    return cell;
}


#pragma mark - ======================== Net Request ========================


#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

//- (NSMutableArray *)dataArr
//{
//    if (!_dataArr) {
//        _dataArr = [[NSMutableArray alloc] init];
//    }
//    return _dataArr;
//}

- (JYHeZuoViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYHeZuoViewModel alloc] init];
    }
    return _viewModel;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
