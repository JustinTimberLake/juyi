//
//  JYHeZuoCell.m
//  JY
//
//  Created by Stronger_WM on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHeZuoCell.h"
#import "MomentImgsView.h"
#import "JYHeZuoModel.h"

@interface JYHeZuoCell ()

@property (nonatomic ,strong) UIView *titleView;
@property (nonatomic ,strong) UILabel *titLabel;        //标题
@property (nonatomic ,strong) UILabel *timeLabel;       //时间
@property (nonatomic ,strong) UILabel *contentLabel;
@property (nonatomic ,strong) MomentImgsView *imgsView;         //九宫格
@property (nonatomic,strong) NSIndexPath *indexPath;

@end

@implementation JYHeZuoCell

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //cell顶部标题
    self.titleView = [[UIView alloc] init];
    self.titleView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.titleView];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.mas_equalTo(0);
        make.height.mas_equalTo(65);
    }];
    
    self.titLabel = [[UILabel alloc] init];
    self.titLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightBold];
    self.titLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.titleView addSubview:self.titLabel];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 10, 25, 10);
    [self.titLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.titleView).with.insets(padding);
    }];
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = FONT(11);
    self.timeLabel.textColor = [UIColor colorWithHexString:@"999999"];
    [self.titleView addSubview:self.timeLabel];
    
    padding = UIEdgeInsetsMake(40, 10, 1, 10);
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.titleView).with.insets(padding);
    }];
    
    UIImageView *lineImgView = [[UIImageView alloc] init];
    lineImgView.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_LINE];
    [self.titleView addSubview:lineImgView];
    
    padding = UIEdgeInsetsMake(64, 0, 0, 0);
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.titleView).with.insets(padding);
    }];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(15);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.contentView addSubview:self.contentLabel];
}

#pragma mark - ======================== Public Methods ========================

+ (CGFloat)cellHeightAccordingModel:(JYHeZuoModel *)model
{
    CGFloat topH = 65;
    CGFloat contentH = [model.unionsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(15)];
    CGFloat imgsH = [MomentImgsView heightOfImgsAccordingImgCount:model.unionsImages.count];
    return topH + 10 + contentH + 10 + imgsH + 10;
}


- (void)setFrame:(CGRect)frame{
    frame.size.height -= 10;
    [super setFrame:frame];
}
#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateCellModel:(JYHeZuoModel *)model andIndexPath:(NSIndexPath *)indexPath
{
    WEAK(weakSelf)
    self.indexPath = indexPath;
    self.titLabel.text = model.unionsTitle;
//    self.timeLabel.text = [NSString YYYYMMDDHHMMWithTimevalue:model.unionsTime styleWithDot:NO];
    self.timeLabel.text = model.unionsTime;

    self.contentLabel.text = model.unionsContent;
    
    CGFloat contentH = [model.unionsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(15)];
    self.contentLabel.height = contentH;
    self.contentLabel.frame = CGRectMake(10, 75, SCREEN_WIDTH - 20, contentH);
    
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYImageModel * imageModel in model.unionsImages) {
        [imageArr addObject:imageModel.imageUrl];
    }
    
    if (model.unionsImages.count > 0) {
        //高度+10,宽度-70
        CGFloat imgsViewH = [MomentImgsView heightOfImgsAccordingImgCount:model.unionsImages.count];
        
        self.imgsView = [[MomentImgsView alloc] initWithFrame:CGRectMake(10, self.contentLabel.bottom + 10, SCREEN_WIDTH-20, imgsViewH) dataSource:imageArr imgTapBlock:^(NSInteger imgIndex, NSArray *dataSource) {
            if (_returnImageTapIndexpathBlock) {
                weakSelf.returnImageTapIndexpathBlock(weakSelf.indexPath);
            }
        }];
        [self.contentView addSubview:self.imgsView];
    }
    else
    {
        [self.imgsView removeFromSuperview];
        self.imgsView = nil;
    }
}


#pragma mark - ======================== Getter ========================


@end
