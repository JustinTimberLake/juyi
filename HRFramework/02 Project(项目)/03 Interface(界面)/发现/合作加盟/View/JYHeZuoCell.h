//
//  JYHeZuoCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYHeZuoModel;

@interface JYHeZuoCell : UITableViewCell

+ (CGFloat)cellHeightAccordingModel:(JYHeZuoModel *)model;

//- (void)updateCellModel:(JYHeZuoModel *)model;
- (void)updateCellModel:(JYHeZuoModel *)model andIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic,copy) void(^returnImageTapIndexpathBlock)(NSIndexPath *indexPath);

@end
