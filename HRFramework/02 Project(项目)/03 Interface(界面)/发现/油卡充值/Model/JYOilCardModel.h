//
//  JYOilCardModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYOilCardModel : JYBaseModel
// 油卡ID
@property (nonatomic,copy) NSString *oilCardId;
// 油卡名称
@property (nonatomic,copy) NSString *oilCardTitle;
@end
