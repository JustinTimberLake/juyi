//
//  JYOilCardViewModel.m
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOilCardViewModel.h"
#import "JYOilCardModel.h"

@implementation JYOilCardViewModel
//3.10.1.	JY-010-014 获取油卡列表
- (void)requestGetOilCardListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_FIND_GetOilCardList));
    [manager POST_URL:JY_PATH(JY_FIND_GetOilCardList) params:dic success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            [weakSelf.listArr removeAllObjects];
            NSArray * arr = [JYOilCardModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}



//3.10.1.	JY-010-017 解除绑定
- (void)requestRelieveBindWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_FIND_RelieveBind));
    [manager POST_URL:JY_PATH(JY_FIND_RelieveBind) params:params success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//3.10.1.	JY-010-016 绑定油卡
- (void)requestBindOilCardWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_FIND_BindOilCard));
    [manager POST_URL:JY_PATH(JY_FIND_BindOilCard) params:params success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//3.10.1.	JY-010-015 获取油卡详细
- (void)requestGetOilCardInfoWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_FIND_GetOilCardInfo) params:params success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            weakSelf.oilCardInfoModel = [JYOilCardInfoModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}

@end
