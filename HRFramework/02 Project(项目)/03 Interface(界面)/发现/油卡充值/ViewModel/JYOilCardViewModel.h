//
//  JYOilCardViewModel.h
//  JY
//
//  Created by risenb on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYOilCardInfoModel.h"

@interface JYOilCardViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;

@property (nonatomic,strong) JYOilCardInfoModel *oilCardInfoModel;

//3.10.1.	JY-010-014 获取油卡列表
- (void)requestGetOilCardListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.10.1.	JY-010-017 解除绑定
- (void)requestRelieveBindWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.10.1.	JY-010-016 绑定油卡
- (void)requestBindOilCardWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.10.1.	JY-010-015 获取油卡详细
- (void)requestGetOilCardInfoWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
