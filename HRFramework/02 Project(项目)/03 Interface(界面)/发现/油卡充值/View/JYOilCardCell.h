//
//  JYOilCardCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYOilCardModel.h"

@interface JYOilCardCell : UITableViewCell

@property (nonatomic,strong) JYOilCardModel *model;

@property (nonatomic,copy) void(^relieveBindBtnActionBlock)();
@end
