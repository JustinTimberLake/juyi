//
//  JYOilCardCell.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOilCardCell.h"

@interface JYOilCardCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
@implementation JYOilCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setModel:(JYOilCardModel *)model{
    _model = model;
    self.titleLab.text = model.oilCardTitle;
    
}
- (IBAction)relieveBindBtnAction:(UIButton *)sender {
    if (_relieveBindBtnActionBlock) {
        self.relieveBindBtnActionBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
