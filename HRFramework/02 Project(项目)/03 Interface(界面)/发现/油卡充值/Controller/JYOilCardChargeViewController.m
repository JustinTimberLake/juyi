//
//  JYOilCardChargeViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOilCardChargeViewController.h"
#import "JYOilCardCell.h"
#import "JYCarGuZhiFooterView.h"
#import "JYOilCardViewModel.h"
#import "JYOilCardBindViewController.h"
#import "JYOilCardDetailViewController.h"

static NSString *const CellId = @"JYOilCardCell";
static NSString *const HeaderId = @"headerId";

@interface JYOilCardChargeViewController ()<
    UITableViewDelegate,
    UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) JYCarGuZhiFooterView *footerView;
@property (nonatomic,strong) JYOilCardViewModel *oilCardViewModel;

@end

@implementation JYOilCardChargeViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestOilCardList];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    WEAKSELF
    self.naviTitle = @"油卡充值";
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = 93 + (SCREEN_WIDTH - 20)/372.0*180;
    self.tableView.sectionFooterHeight = 0;
    [self.tableView registerNib:[UINib nibWithNibName:CellId bundle:nil] forCellReuseIdentifier:CellId];
    
    self.footerView = [[[NSBundle mainBundle] loadNibNamed:@"JYCarGuZhiFooterView" owner:self options:nil] lastObject];
    [self.footerView setBtnTitle:@"绑定油卡"];
    self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 130);
    

    self.footerView.evaluateBlock = ^{
        //油卡充值
        JYOilCardBindViewController * bindVC = [[JYOilCardBindViewController alloc] init];
        [weakSelf.navigationController pushViewController:bindVC animated:YES];
    };
    
    self.tableView.tableFooterView = self.footerView;
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0.01f;
    }
    return 10;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    }
    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderId];
    if (!header) {
        header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:HeaderId];
    }
    header.backgroundView.backgroundColor = [UIColor colorWithHexString:@"dbdbdb"];
    return header;
}

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.oilCardViewModel.listArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    JYOilCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    JYOilCardModel * model = self.oilCardViewModel.listArr[indexPath.section];
    cell.model = model;
    cell.relieveBindBtnActionBlock = ^{
        [weakSelf requestRelieveBindWithOilCardId:model.oilCardId];
    };
    return cell;
}



#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    
}
//请求加油卡列表
- (void)requestOilCardList{
    WEAKSELF
    [self.oilCardViewModel requestGetOilCardListSuccess:^(NSString *msg, id responseData) {
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//解除绑定接口
- (void)requestRelieveBindWithOilCardId:(NSString *)oilCardId{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"oilCardId"] = oilCardId;
    [self.oilCardViewModel requestRelieveBindWithParams:dic success:^(NSString *msg, id responseData) {
//        刷新列表
        [weakSelf requestOilCardList];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JYOilCardDetailViewController * detailVC = [[JYOilCardDetailViewController alloc] init];
    JYOilCardModel * model = self.oilCardViewModel.listArr[indexPath.section];
    detailVC.oilCardId = model.oilCardId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================


- (JYOilCardViewModel *)oilCardViewModel
{
    if (!_oilCardViewModel) {
        _oilCardViewModel = [[JYOilCardViewModel alloc] init];
    }
    return _oilCardViewModel;
}

@end
