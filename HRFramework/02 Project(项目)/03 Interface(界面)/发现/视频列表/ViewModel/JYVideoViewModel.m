//
//  JYVideoViewModel.m
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYVideoViewModel.h"
//#import "JYShopClassifyModel.h"
#import "ChannelModel.h"
#import "JYMyLoveMvModel.h"
#import "CLModel.h"

@implementation JYVideoViewModel
//获取视频分类
- (void)requestGetVideoClassifySuccess:(SuccessBlock)succssBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_FIND_GetVideoClassify));
    [manager POST_URL:JY_PATH(JY_FIND_GetVideoClassify) params:nil success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            [weakSelf.classfiyListArr removeAllObjects];
            NSArray * arr = [ChannelModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.classfiyListArr addObjectsFromArray:arr];
            succssBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//3.10.1.	JY-010-002 获取视频列表
- (void)requestGetVideoListWithClassfiyId:(NSString *)classfiyId IsMore:(BOOL)isMore Success:(SuccessBlock)succssBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    NSString * pageCount;
    if (weakSelf.videoListArr.count % 10 == 0) {
        pageCount = isMore ? SF(@"%lu", weakSelf.videoListArr.count / 10 + 1):@"1";
    }else{
        pageCount = isMore ? SF(@"%lu", weakSelf.videoListArr.count / 10 + 2):@"1";
    }
    dic[@"classifyId"] = classfiyId;
    dic[@"pageNum"] = pageCount;
    dic[@"pageSize"] = @"10";
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_FIND_GetVideoList) params:dic success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.videoListArr removeAllObjects];
            }
            NSArray * arr = [CLModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.videoListArr addObjectsFromArray:arr];
            succssBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)classfiyListArr
{
    if (!_classfiyListArr) {
        _classfiyListArr = [NSMutableArray array];
    }
    return _classfiyListArr;
}

- (NSMutableArray *)videoListArr
{
    if (!_videoListArr) {
        _videoListArr = [NSMutableArray array];
    }
    return _videoListArr;
}


@end
