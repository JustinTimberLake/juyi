//
//  JYVideoViewModel.h
//  JY
//
//  Created by risenb on 2017/8/8.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYVideoViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *classfiyListArr;

@property (nonatomic,strong) NSMutableArray *videoListArr;
//获取视频分类
- (void)requestGetVideoClassifySuccess:(SuccessBlock)succssBlock failure:(FailureBlock)failureBlock;

//3.10.1.	JY-010-002 获取视频列表
- (void)requestGetVideoListWithClassfiyId:(NSString *)classfiyId IsMore:(BOOL)isMore Success:(SuccessBlock)succssBlock failure:(FailureBlock)failureBlock;


@end
