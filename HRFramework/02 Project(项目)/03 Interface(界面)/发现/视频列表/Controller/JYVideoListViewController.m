//
//  JYVideoListViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYVideoListViewController.h"

#import "ChannelView.h"
#import "ChannelModel.h"
#import "JYMyFavoritesMvTableViewCell.h"
#import "JYVideoViewModel.h"
#import "ChannelModel.h"
#import "JYAutoSizeTitleView.h"
#import "JYCollectionViewModel.h"


#import "CLTableViewCell.h"
#import "CLPlayerView.h"
#import "CLModel.h"



static NSString *const CellId = @"JYMyFavoritesMvTableViewCell";

@interface JYVideoListViewController()<UITableViewDelegate,UITableViewDataSource,ChannelViewDelegate,CLTableViewCellDelegate>

@property (nonatomic ,strong) ChannelView *channelView;
//@property (nonatomic ,strong) UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *titleView;

@property (nonatomic,strong) JYVideoViewModel *viewModel;
@property (nonatomic,copy) NSString *ClassfiyId;
@property (nonatomic,strong) JYAutoSizeTitleView * autoSizeTitleView;
@property (nonatomic,strong) NSMutableArray *titleArr;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;


/**CLplayer*/
@property (nonatomic, weak) CLPlayerView *playerView;
/**记录Cell*/
@property (nonatomic, assign) UITableViewCell *cell;

@end

@implementation JYVideoListViewController

static NSString *CLTableViewCellIdentifier = @"CLTableViewCellIdentifier";

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    [self creatTitle];
    [self setUpRefreshData];
    [self requestClassfiyData];
//    [self requestVideoListWithIsMore:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_playerView destroyPlayer];
}

- (void)setUpUI{
    self.naviTitle = @"视频";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 180;
    //    [self.tableView registerNib:[UINib nibWithNibName:CellId bundle:nil] forCellReuseIdentifier:CellId];
    [_tableView registerClass:[CLTableViewCell class] forCellReuseIdentifier:CLTableViewCellIdentifier];
}


- (void)creatTitle{
    WEAKSELF
    JYAutoSizeTitleView * autoSizeTitleView =[[JYAutoSizeTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    self.autoSizeTitleView = autoSizeTitleView;
    [self.titleView addSubview:autoSizeTitleView];
    
    //    需要设置的属性
    autoSizeTitleView.norColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    autoSizeTitleView.selColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isShowUnderLine = YES;
    autoSizeTitleView.underLineH = 1;
    autoSizeTitleView.underLineColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isUnderLineEqualTitleWidth = YES;
    autoSizeTitleView.fontSize = FONT(15);
    autoSizeTitleView.didSelectItemsWithIndexBlock = ^(NSInteger index) {
        //        if (index == 0) {
        //            weakSelf.home = YES;
        //            weakSelf.secondaryId = @"";
        //            [weakSelf requestHomeTop];
        //        }else{
        //            JYShopSecondaryModel * model = self.classifyViewModel.secondClassifyArr[index - 1];
        //            weakSelf.secondaryId = model.secondaryId;
        //            [weakSelf setUpRefreshData];
        //            weakSelf.home = NO;
        //        }
        ChannelModel * model = self.viewModel.classfiyListArr[index];
        weakSelf.ClassfiyId = model.classifyId;
//        [weakSelf setUpRefreshData];
        
        [self performSelector:@selector(delayMethod) withObject:nil afterDelay:0.2];
        
    };
    //    [self.titleArr addObject:@"首页"];
    //    [autoSizeTitleView updataTitleData:self.titleArr];
}


-(void)delayMethod{
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.videoListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    //    JYMyFavoritesMvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    JYMyLoveMvModel * model = self.viewModel.videoListArr[indexPath.row];
    //    cell.model = model;
    //    cell.VideoCollectionBlock = ^(NSString *isCollect) {
    //        if (![self judgeLogin]) {
    //            return ;
    //        }
    //        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
    //    };
    //    [cell showBottomLine:NO];
    //    return cell;
    
    CLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLTableViewCellIdentifier forIndexPath:indexPath];
    cell.indexPath = indexPath;
    CLModel *model = self.viewModel.videoListArr[indexPath.row];
    cell.model = model;
    cell.collectionBtnClickBlock = ^(NSString *isCollect){
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}
//在willDisplayCell里面处理数据能优化tableview的滑动流畅性，cell将要出现的时候调用
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    CLTableViewCell * myCell = (CLTableViewCell *)cell;
    myCell.model = self.viewModel.videoListArr[indexPath.row];
    //Cell开始出现的时候修正偏移量，让图片可以全部显示
    [myCell cellOffset];
    //第一次加载动画
    
    [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:myCell.model.pictureUrl] completion:^(BOOL isInCache) {
        if (!isInCache) {
            //主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                CATransform3D rotation;//3D旋转
                rotation = CATransform3DMakeTranslation(0 ,50 ,20);
                //逆时针旋转
                rotation = CATransform3DScale(rotation, 0.8, 0.9, 1);
                rotation.m34 = 1.0/ -600;
                myCell.layer.shadowColor = [[UIColor blackColor]CGColor];
                myCell.layer.shadowOffset = CGSizeMake(10, 10);
                myCell.alpha = 0;
                myCell.layer.transform = rotation;
                [UIView beginAnimations:@"rotation" context:NULL];
                //旋转时间
                [UIView setAnimationDuration:0.6];
                myCell.layer.transform = CATransform3DIdentity;
                myCell.alpha = 1;
                myCell.layer.shadowOffset = CGSizeMake(0, 0);
                [UIView commitAnimations];
            });
        }
    }];
}
//cell离开tableView时调用
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //因为复用，同一个cell可能会走多次
    if ([_cell isEqual:cell]) {
        //区分是否是播放器所在cell,销毁时将指针置空
        [_playerView destroyPlayer];
        _cell = nil;
    }
}
#pragma mark - 点击播放代理
- (void)cl_tableViewCellPlayVideoWithCell:(CLTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF;
    //记录被点击的Cell
    _cell = cell;
    //销毁播放器
    [_playerView destroyPlayer];
    CLPlayerView *playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, cell.width, cell.height)];
    _playerView = playerView;
    [cell.contentView addSubview:_playerView];
    //    //重复播放，默认不播放
    //    _playerView.repeatPlay = YES;
    //    //当前控制器是否支持旋转，当前页面支持旋转的时候需要设置，告知播放器
    //    _playerView.isLandscape = YES;
    //    //设置等比例全屏拉伸，多余部分会被剪切
        _playerView.fillMode = ResizeAspect;
    //    //设置进度条背景颜色
    //    _playerView.progressBackgroundColor = [UIColor purpleColor];
    //    //设置进度条缓冲颜色
    //    _playerView.progressBufferColor = [UIColor redColor];
    //    //设置进度条播放完成颜色
    //    _playerView.progressPlayFinishColor = [UIColor greenColor];
    //    //全屏是否隐藏状态栏
    //    _playerView.fullStatusBarHidden = NO;
    //    //转子颜色
    //    _playerView.strokeColor = [UIColor redColor];
    //视频地址
    _playerView.url = [NSURL URLWithString:cell.model.videoUrl];
    _playerView.title = cell.model.videoTitle;
    _playerView.isCollect = cell.model.isCollect;
    //播放
    [_playerView playVideo];
    //返回按钮点击事件回调
    [_playerView backButton:^(UIButton *button) {
        NSLog(@"返回按钮被点击");
    }];
    //播放完成回调
    [_playerView endPlay:^{
        //销毁播放器
        [_playerView destroyPlayer];
        _playerView = nil;
        _cell = nil;
        NSLog(@"播放完成");
    }];
    
    _playerView.collectionBtnClickBlock = ^(NSString *isCollect) {
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:cell.model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
}
//#pragma mark - 滑动代理
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    // visibleCells 获取界面上能显示出来了cell
//    NSArray<CLTableViewCell *> *array = [self.tableView visibleCells];
//    //enumerateObjectsUsingBlock 类似于for，但是比for更快
//    [array enumerateObjectsUsingBlock:^(CLTableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [obj cellOffset];
//    }];
//}

#pragma mark ==================自定义方法==================
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}
#pragma mark - ======================== Net Request ========================



- (void)requestClassfiyData{
    WEAKSELF
    [self.viewModel requestGetVideoClassifySuccess:^(NSString *msg, id responseData) {
        //        [weakSelf updataClassfiyUI];
        [weakSelf updataTitleView];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)setUpRefreshData{
    WEAKSELF
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestVideoListWithIsMore:NO];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestVideoListWithIsMore:YES];
    }];
//    [self.tableView.mj_header beginRefreshing];
}



- (void)requestVideoListWithIsMore:(BOOL)isMore{
    
    WEAKSELF
    [self.viewModel requestGetVideoListWithClassfiyId:self.ClassfiyId IsMore:isMore Success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)requestCollectVideoWithVideoId:(NSString *)videoId andOperat:(NSString *)operat andIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"videoId"] = videoId;
    dic[@"operat"] = operat;
    [self.collectionViewModel requestCollectionWithParams:dic andType:JY_CollectionType_Video success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf reloadDataWithIndexPath:indexPath];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)reloadDataWithIndexPath:(NSIndexPath *)indexPath{
//    JYMyLoveMvModel * model = self.viewModel.videoListArr[indexPath.row];
    CLModel * model = self.viewModel.videoListArr[indexPath.row];
    if ([model.isCollect intValue] == 1) {
        model.isCollect = @"2";
    }else{
        model.isCollect = @"1";
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark - ======================== Update View ========================

//更新titleView
- (void)updataTitleView{
    //    NSMutableArray * titles = [NSMutableArray array];
    [self.titleArr removeAllObjects];
    
    for (ChannelModel * model in self.viewModel.classfiyListArr) {
        [self.titleArr addObject:model.channelTitle];
    }
    //    [self.titleArr insertObject:@"首页" atIndex:0];
    [self.autoSizeTitleView updataTitleData:self.titleArr];
}

#pragma mark - ======================== Getter ========================


#pragma mark ==================lazy==================

- (JYVideoViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYVideoViewModel alloc] init];
    }
    return _viewModel;
}


- (NSMutableArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = [NSMutableArray array];
    }
    return _titleArr;
}

- (JYCollectionViewModel *)collectionViewModel
{
    if (!_collectionViewModel) {
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}



@end

