//
//  CLTableViewCell.h
//  CLPlayerDemo
//
//  Created by JmoVxia on 2017/8/4.
//  Copyright © 2017年 JmoVxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLModel.h"
#import "JYMyLoveMvModel.h"

@class CLTableViewCell;

@protocol CLTableViewCellDelegate <NSObject>

- (void)cl_tableViewCellPlayVideoWithCell:(CLTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath;


@end

@interface CLTableViewCell : UITableViewCell

/**model*/
@property (nonatomic, copy) CLModel *model;
//@property (nonatomic,copy) JYMyLoveMvModel *model;
@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id <CLTableViewCellDelegate> delegate;
@property (nonatomic,copy) void(^collectionBtnClickBlock)(NSString *isCollect);

- (CGFloat)cellOffset;
- (void)setHighLightWithKeywords:(NSString *)keywords;

@end
