//
//  CLTableViewCell.m
//  CLPlayerDemo
//
//  Created by JmoVxia on 2017/8/4.
//  Copyright © 2017年 JmoVxia. All rights reserved.
//

#import "CLTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIView+CLSetRect.h"

#define CellHeight   300
//#define ImageViewHeight 600
#define ImageViewHeight 300

@interface CLTableViewCell ()

/**button*/
@property (nonatomic,strong) UIButton *button;
/**picture*/
@property (nonatomic,strong) UIImageView *pictureView;

@property (nonatomic,copy) UILabel *titleLab;

@property (nonatomic,strong) UIButton *collectBtn;

@end

@implementation CLTableViewCell
#pragma mark - 懒加载
/**button*/
- (UIButton *) button{
    if (_button == nil){
        _button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
//        [_button setBackgroundImage:[self getPictureWithName:@"播放图标"] forState:UIControlStateNormal];
        [_button setImage:[UIImage imageNamed:@"暂停图标"] forState:UIControlStateNormal];
        [_button addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}
/**pictureView*/
- (UIImageView *) pictureView{
    if (_pictureView == nil){
        _pictureView = [[UIImageView alloc] initWithFrame:CGRectMake(0, - (ImageViewHeight - CellHeight) * 0.5, CLscreenWidth, ImageViewHeight)];
        _pictureView.contentMode = UIViewContentModeScaleAspectFill;
        _pictureView.clipsToBounds = YES;
        
    }
    return _pictureView;
}
- (UILabel *)titleLab
{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.width - 44, 15)];
        _titleLab.text = @"视频";
        _titleLab.textColor = [UIColor whiteColor];
        _titleLab.font = FONT(15);
        
    }
    return _titleLab;
}


- (UIButton *)collectBtn
{
    if (!_collectBtn) {
        _collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectBtn setImage:[UIImage imageNamed:@"收藏图标"] forState:UIControlStateNormal];
        [_collectBtn setImage:[UIImage imageNamed:@"已收藏图标"] forState:UIControlStateSelected];
//        _collectBtn.frame = CGRectMake( CGRectGetMaxX(self.titleLab.frame) + 5,10, 24, 24);
        _collectBtn.frame = CGRectMake( SCREEN_WIDTH - 24 - 10  ,10, 24, 24);
        [_collectBtn addTarget:self action:@selector(collectionBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _collectBtn;
}

#pragma mark - 入口
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self initUI];
    }
    return self;
}
- (void)initUI{

    //剪裁看不到的
    self.clipsToBounds       = YES;
    self.selectionStyle      = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview:self.pictureView];
    [self.contentView addSubview:self.button];
    
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.collectBtn];
    
}
-(void)setModel:(CLModel *)model{
    _model = model;
    self.collectBtn.selected = ([model.isCollect intValue] == 1) ? YES :NO;
    self.titleLab.text = model.videoTitle;
    __block UIImage *placeholderImage = [UIImage imageNamed:@"商城活动推荐位3"] ;
    [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:_model.videoImage] completion:^(BOOL isInCache) {
        if (isInCache) {
            //本地存在图片,替换占位图片
            placeholderImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:model.videoImage];
        }
        //主线程
        dispatch_async(dispatch_get_main_queue(), ^{
            [_pictureView sd_setImageWithURL:[NSURL URLWithString:model.videoImage] placeholderImage:placeholderImage];
        });
    }];
}
- (void)playAction:(UIButton *)button{
    if (_delegate && [_delegate respondsToSelector:@selector(cl_tableViewCellPlayVideoWithCell: withIndexPath:)]){
        [_delegate cl_tableViewCellPlayVideoWithCell:self withIndexPath:self.indexPath];
    }
}

- (void)collectionBtnClick:(UIButton *)button{
//    button.selected = !button.selected;
    NSString *isCollect;
    isCollect = button.selected ? @"2" :@"1";
    if (_collectionBtnClickBlock) {
        self.collectionBtnClickBlock(isCollect);
    }
    
}
- (UIImage *)getPictureWithName:(NSString *)name{
    NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"CLPlayer" ofType:@"bundle"]];
    NSString *path   = [bundle pathForResource:name ofType:@"png"];
    return [UIImage imageWithContentsOfFile:path];
}
- (CGFloat)cellOffset{
    /*
     - (CGRect)convertRect:(CGRect)rect toView:(nullable UIView *)view;
     将rect由rect所在视图转换到目标视图view中，返回在目标视图view中的rect
     这里用来获取self在window上的位置
     */
    CGRect toWindow      = [self convertRect:self.bounds toView:self.window];
    //获取父视图的中心
    CGPoint windowCenter = self.superview.center;
    //cell在y轴上的位移
    CGFloat cellOffsetY  = CGRectGetMidY(toWindow) - windowCenter.y;
    //位移比例
    CGFloat offsetDig    = 2 * cellOffsetY / self.superview.frame.size.height ;
    //要补偿的位移,self.superview.frame.origin.y是tableView的Y值，这里加上是为了让图片从最上面开始显示
    CGFloat offset       = - offsetDig * (ImageViewHeight - CellHeight) / 2;
    //让pictureViewY轴方向位移offset
    CGAffineTransform transY = CGAffineTransformMakeTranslation(0,offset);
    _pictureView.transform   = transY;
    return offset;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    _button.CLcenterX     = self.CLwidth/2.0;
    _button.CLcenterY     = self.CLheight/2.0;
    _pictureView.CLwidth  = self.CLwidth;
}

- (void)setFrame:(CGRect)frame{
    frame.origin.y += 10;
    frame.size.height -= 10;
    [super setFrame:frame];
}

- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.titleLab setLabelTitleHighlight:self.model.videoTitle andSearchStr:keywords];
}

@end
