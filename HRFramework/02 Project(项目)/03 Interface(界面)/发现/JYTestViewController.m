//
//  JYTestViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/8/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYTestViewController.h"

@interface JYTestViewController ()
@property (nonatomic ,strong) UITextView *textView;
@end

@implementation JYTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat h = 568;
    CGFloat w = 320;
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, h-148, w, 48)];
    self.textView.text = @"23";
    [self.view addSubview:self.textView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
