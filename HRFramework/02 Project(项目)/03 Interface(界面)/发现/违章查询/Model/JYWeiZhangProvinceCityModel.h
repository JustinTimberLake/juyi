//
//  JYWeiZhangProvinceCityModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYWeiZhangProvinceCityModel : JYBaseModel
//"": "13",
@property (nonatomic,copy) NSString *provinceId;
//"": "1304",
@property (nonatomic,copy) NSString *cityId;
//"": "邯郸",
@property (nonatomic,copy) NSString *cityName;
//"": "河北省",
@property (nonatomic,copy) NSString *provinceName;
//"": "冀D",
@property (nonatomic,copy) NSString *fixName;
//"": "4",
@property (nonatomic,copy) NSString *frameLength;
//"": "0",
@property (nonatomic,copy) NSString *engineLength;
//"": true
@property (nonatomic,copy) NSString *canQuery;
@end
