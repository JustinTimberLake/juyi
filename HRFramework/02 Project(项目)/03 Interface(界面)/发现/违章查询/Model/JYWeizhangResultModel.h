//
//  JYWeizhangResultModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYWeizhangResultModel : JYBaseModel
//"": "2016-10-17 21:30:00",
@property (nonatomic,copy) NSString *date;
//"": "长深高速",
@property (nonatomic,copy) NSString *area;
//"": "驾驶中型以上载客载货汽车、校车、危险物品运输车辆以外的其他机动车行驶超过规定时速20%以上未达到50%的",
@property (nonatomic,copy) NSString *act;
//"": "1636",
@property (nonatomic,copy) NSString *code;
//"": "6",
@property (nonatomic,copy) NSString *fen;
//"": "200",
@property (nonatomic,copy) NSString *money;
//"": "0",
@property (nonatomic,copy) NSString *handled;
//"": "320294Y000276124"
@property (nonatomic,copy) NSString *archiveno;



@end
