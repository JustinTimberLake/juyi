//
//  JYWeizhangCityModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYWeizhangCityModel : JYBaseModel
//"": "天津",
@property (nonatomic,copy) NSString *city_name;
//"": "TJ",
@property (nonatomic,copy) NSString *city_code;
//"": "津",
@property (nonatomic,copy) NSString *abbr;
//"": "1",
@property (nonatomic,copy) NSString *engine;
//"": "6",
@property (nonatomic,copy) NSString *engineno;
//"": "1",
@property (nonatomic,copy) NSString *classa;
//"": "1",
//@property (nonatomic,copy) NSString *class;
//"": "6",
@property (nonatomic,copy) NSString *classno;
//"": "0",
@property (nonatomic,copy) NSString *regist;
//"": "0"
@property (nonatomic,copy) NSString *registno;

@end
