//
//  JYWeizhangProCityModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYWeizhangCityModel.h"

@interface JYWeizhangProCityModel : JYBaseModel
//"": "天津",
@property (nonatomic,copy) NSString *province;
//"": "TJ",
@property (nonatomic,copy) NSString *province_code;

@property (nonatomic,strong) NSArray<JYWeizhangCityModel *> *citys;
@end
