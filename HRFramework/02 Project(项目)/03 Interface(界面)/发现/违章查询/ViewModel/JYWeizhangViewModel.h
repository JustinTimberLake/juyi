//
//  JYWeizhangViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYWeizhangViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray  *listArr;

//请求聚合省市数据
- (void)requestJuHeProvinceCityDataSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
