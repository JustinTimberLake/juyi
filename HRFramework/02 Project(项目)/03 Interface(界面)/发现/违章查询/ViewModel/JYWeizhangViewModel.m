//
//  JYWeizhangViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/11/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWeizhangViewModel.h"
//#import "JYWeiZhangProvinceCityModel.h"
#import "JYWeizhangProCityModel.h"

@implementation JYWeizhangViewModel
//请求聚合省市数据
- (void)requestJuHeProvinceCityDataSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"key"] = JY_JUHE_WEIZHANGCHAXUN_APPKEY;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_JUHE_WEIZHANGCHAXUN_CITY_URL params:dic success:^(id result) {
        NSLog(@"🌺%@",result);
        [weakSelf.listArr removeAllObjects];
        if ([result[@"resultcode"] intValue] == 200) {
//            NSArray * arr = [JYWeiZhangProvinceCityModel mj_objectArrayWithKeyValuesArray:result[@"result"]];
            NSMutableDictionary * dic = result[@"result"];
//            NSMutableArray * keyArr = [NSMutableArray arrayWithArray:[dic allKeys]];
////            NSArray * reverseArr =  [[keyArr reverseObjectEnumerator] allObjects];
////            NSLog(@"++++++++%@",reverseArr);
//            arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
//                NSComparisonResult result = [obj1 compare:obj2];
//                return result==NSOrderedDescending;
//            }];
            NSArray* arr = [dic allKeys];
            arr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
                NSComparisonResult result = [obj1 compare:obj2];
                return result==NSOrderedDescending;
            }];

            for (NSString * key in arr) {
                [weakSelf.listArr addObject:[JYWeizhangProCityModel mj_objectWithKeyValues:dic[key]]];
            }
            NSLog(@"🐷%@",weakSelf.listArr);
            successBlock(result[@"reason"],nil);
        }else{
            failureBlock(result[@"reason"]);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}


@end
