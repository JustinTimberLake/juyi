//
//  JYWeiZhangQueryViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWeiZhangQueryViewController.h"
#import "JYWeiZhangQueryResultViewController.h" //违章查询结果
#import "JYLocationVM.h"    //请求地址
#import "JYPickerViewAlertController.h" //picker弹窗
#import "JYProvinceAbbreviationPickerVC.h"  //车牌简称选择
#import "JYWeizhangViewModel.h"
#import "LZSortTool.h"
//#import "JYWeiZhangProvinceCityModel.h"
#import "JYWeizhangProCityModel.h"
#import "JYWeizhangCityModel.h"

@interface JYWeiZhangQueryViewController ()

@property (weak, nonatomic) IBOutlet UIView *provinceHotView;                   //选择省热区
@property (nonatomic ,strong) UITapGestureRecognizer *chooseProvinceTap;
@property (weak, nonatomic) IBOutlet UILabel *provinceLabel;                    //省标签
@property (nonatomic ,strong) NSMutableArray *provinceArr;
@property (nonatomic ,copy) NSString *selectedProvinceCode;                       //已选的省Code
@property (nonatomic,assign) NSInteger selectProIndex; //已选的第几个省

@property (weak, nonatomic) IBOutlet UIView *cityHotView;                       //选择市热区
@property (nonatomic ,strong) UITapGestureRecognizer *chooseCityTap;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;                        //市标签
@property (nonatomic ,strong) NSMutableArray *cityArr;
@property (nonatomic ,copy) NSString *selectedCityCode;                         //已选市Code

@property (weak, nonatomic) IBOutlet UIButton *chooseProvinceAbbreviationBtn;   //选择省简称
@property (weak, nonatomic) IBOutlet UILabel *provinceAbbreviationLabel;        //简称
@property (weak, nonatomic) IBOutlet UITextField *carNumberTF;                  //车牌号码
@property (weak, nonatomic) IBOutlet UITextField *engineNumberTF;               //发动机号
@property (weak, nonatomic) IBOutlet UITextField *classnoTextfield;             //车架号
@property (weak, nonatomic) IBOutlet UIButton *queryBtn;

@property (nonatomic,strong) JYWeizhangViewModel *weizhangViewModel;  //违章viewModel
//@property (nonatomic,strong) NSMutableArray *weizhangProvinceCityArr;
@property (nonatomic,copy) NSString *selectEngine ;//是否需要发动机号0:不需要 1:需要
@property (nonatomic,copy) NSString *selectEngineno; //需要几位发动机号0:全部 1-9 :需要发动机号后N位
@property (nonatomic,copy) NSString *selectClassa; //是否需要车架号 0:不需要 1:需要
@property (nonatomic,copy) NSString *selectClassNo; //需要几位车架号0:全部 1-9 :需要发动机号后N位

@end

@implementation JYWeiZhangQueryViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    self.chooseProvinceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseProvinceAction)];
    [self.provinceHotView addGestureRecognizer:self.chooseProvinceTap];
    
    self.chooseCityTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseCityAction)];
    [self.cityHotView addGestureRecognizer:self.chooseCityTap];
    
    self.selectedProvinceCode = nil;
    self.selectedCityCode = nil;
    
    self.queryBtn.layer.cornerRadius = 4;
    self.queryBtn.layer.masksToBounds = YES;
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"违章查询";
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

- (IBAction)chooseProvinceAbbreviationBtnAction:(UIButton *)sender {
    
    WEAK(ws);
    
    JYProvinceAbbreviationPickerVC *vc = [[JYProvinceAbbreviationPickerVC alloc] init];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    vc.resultBlock = ^(NSString *result) {
        ws.provinceAbbreviationLabel.text = result;
    };
    
    [self presentViewController:vc animated:YES completion:nil];
}

//城市选择
- (void)chooseCityAction
{
    if (self.selectedProvinceCode == nil) {
        [self showSuccessTip:@"请先选择省"];
        return;
    }
    
//    if (self.cityArr.count == 0) {
//        [self showSuccessTip:@"网络不稳定，请稍后再试"];
//        return;
//    }
    
    WEAKSELF
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    alertVC.pickerViewType = PickerViewTypeNormal;
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
//    //将省模型数据转化为字符串数组
//    NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:self.cityArr.count];
//    [self.cityArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        JYCityModel *model = obj;
//        [tempArr addObject:model.cityTitle];
//    }];
    NSMutableArray * tempArr = [NSMutableArray array];
    
    JYWeizhangProCityModel * model  = self.weizhangViewModel.listArr[self.selectProIndex];
    for (JYWeizhangCityModel *cityModel in model.citys) {
          [tempArr addObject:cityModel.city_name];
    }

  
    
    [alertVC updatePickerViewDataArr:tempArr type:PickerViewTypeNormal];
    
    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
//        JYCityModel *model = self.cityArr[selectIndex];
//        weakSelf.selectedCityCode = model.cityCode;
//        weakSelf.cityLabel.text = model.cityTitle;
        
          JYWeizhangProCityModel * model  = self.weizhangViewModel.listArr[self.selectProIndex];
        JYWeizhangCityModel *cityModel  = model.citys[selectIndex];
        weakSelf.selectedCityCode = cityModel.city_code;
        weakSelf.cityLabel.text = cityModel.city_name;
        weakSelf.selectEngine = cityModel.engine;
        weakSelf.selectEngineno = cityModel.engineno;
        weakSelf.selectClassa = cityModel.classa;
        weakSelf.selectClassNo = cityModel.classno;
        
//        控制发动机的占位文字显示
        if ([weakSelf.selectEngine intValue] == 1 && [weakSelf.selectEngineno intValue]== 0) {
            self.engineNumberTF.placeholder = @"输入全部发动机号";
        }else if ([weakSelf.selectEngine intValue] == 1 && [weakSelf.selectEngineno intValue] > 0) {
            weakSelf.engineNumberTF.placeholder = SF(@"输入后%@位发动机号",self.selectEngineno);
        }
        //        控制发动机的占位文字显示
        if ([weakSelf.selectClassa intValue] == 1 && [weakSelf.selectClassNo intValue]== 0) {
            self.engineNumberTF.placeholder = @"输入全部车架号";
        }else if ([weakSelf.selectClassa intValue] == 1 && [weakSelf.selectClassNo intValue] > 0) {
            weakSelf.classnoTextfield.placeholder = SF(@"输入后%@位车架号",self.selectEngineno);
        }
    };
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

//选择省份
- (void)chooseProvinceAction
{
    WEAKSELF
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    alertVC.pickerViewType = PickerViewTypeNormal;
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
//    //将省模型数据转化为字符串数组
//    NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:self.provinceArr.count];
//    [self.provinceArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        JYProvinceModel *model = obj;
//        [tempArr addObject:model.provinceTitle];
//    }];
//
//    [alertVC updatePickerViewDataArr:tempArr type:PickerViewTypeNormal];
//
//    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
//        JYProvinceModel *model = self.provinceArr[selectIndex];
//        if (![weakSelf.selectedProvinceCode isEqualToString:model.provinceCode]) {
//            weakSelf.selectedProvinceCode = model.provinceCode;
//            weakSelf.provinceLabel.text = model.provinceTitle;
//            [weakSelf.cityArr removeAllObjects];
//            [weakSelf requestCity];
//        }
//    };
    //将省模型数据转化为字符串数组
//    NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:self.weizhangProvinceCityArr.count];
//    [self.weizhangProvinceCityArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSDictionary *dic = obj;
//        [tempArr addObject:dic[@"LZSortToolKey"]];
//    }];
    NSMutableArray * tempArr = [NSMutableArray array];
    for (JYWeizhangProCityModel * model in self.weizhangViewModel.listArr) {
        [tempArr addObject:model.province];
    }
    
    [alertVC updatePickerViewDataArr:tempArr type:PickerViewTypeNormal];
    
    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
        JYWeizhangProCityModel * model  = self.weizhangViewModel.listArr[selectIndex];
        weakSelf.selectedProvinceCode = model.province_code;
        weakSelf.provinceLabel.text = model.province;
        weakSelf.selectProIndex = selectIndex;

//        if (![weakSelf.selectedProvinceCode isEqualToString:model.provinceCode]) {
//            weakSelf.selectedProvinceCode = model.provinceCode;
//            weakSelf.provinceLabel.text = model.provinceTitle;
//            [weakSelf.cityArr removeAllObjects];
//            [weakSelf requestCity];
//        }
    };

    [self presentViewController:alertVC animated:YES completion:nil];
}

//查询
- (IBAction)queryAction:(UIButton *)sender {
    if (!self.selectedProvinceCode.length) {
        [self showSuccessTip:@"请选择省"];
        return;
    }
    if (!self.selectedCityCode.length) {
        [self showSuccessTip:@"请选择市"];
        return;
    }
    if (!self.carNumberTF.text.length) {
        [self showSuccessTip:@"请输入车牌号"];
        return;
    }
    
    if (!self.engineNumberTF.text.length &&[self.selectEngine intValue] == 1 && [self.selectEngineno intValue]== 0) {
        [self showSuccessTip:@"输入全部发动机号"];
        return;
    }
    
    if (!self.engineNumberTF.text.length && [self.selectEngine intValue] == 1 && [self.selectEngineno intValue] > 0) {
        [self showSuccessTip:SF(@"输入后%@位发动机号",self.selectEngineno)];
        return;
    }
    
    if (!self.classnoTextfield.text.length &&[self.selectClassa intValue] == 1 && [self.selectClassNo intValue]== 0) {
        [self showSuccessTip:@"输入全部车架号"];
        return;
    }
    
    if (!self.classnoTextfield.text.length && [self.selectClassa intValue] == 1 && [self.selectClassNo intValue] > 0) {
        [self showSuccessTip:SF(@"输入后%@位车架号",self.selectEngineno)];
        return;
    }
    
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"key"] = JY_JUHE_WEIZHANGCHAXUN_APPKEY;
    dic[@"city"] = self.selectedCityCode;
    
    NSString *carNumber = [NSString stringWithFormat:@"%@%@",self.provinceAbbreviationLabel.text,self.carNumberTF.text];
    dic[@"hphm"] = carNumber;
    dic[@"hpzl"] = @"02";
    dic[@"engineno"] = self.engineNumberTF.text;
    dic[@"classno"] = self.classnoTextfield.text;
    
    JYWeiZhangQueryResultViewController *vc = [[JYWeiZhangQueryResultViewController alloc] init];
//
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:4];
//    [params setObject:self.selectedProvinceCode forKey:@"provinceCode"];
//    [params setObject:self.selectedCityCode forKey:@"cityCode"];
//    NSString *carNumber = [NSString stringWithFormat:@"%@%@",self.provinceAbbreviationLabel.text,self.carNumberTF.text];
//    [params setObject:carNumber forKey:@"carId"];
//    [params setObject:self.engineNumberTF.text forKey:@"motorId"];
//    vc.params = params;
    vc.params = dic;
    [self.navigationController pushViewController:vc animated:YES];
}

////获取新的省数组
//- (void)getSortNewArrWithDataArr:(NSArray *)dataArr{
////    [self.rightIndexArr removeAllObjects];
//    //    NSArray * arr = @[@"鲁迅",@"###",@"刘一",@"赵四",@"钱",@"李三",@"孙五",@"王二",@"黄蓉",@"孙悟空",@"哪吒",@"李天王",@"范冰冰",@"赵丽颖",@"霍建华",@"黄晓明",@"成龙",@"李连杰",@"李小龙",@"曾小贤",@"LiShan"];
//    //    NSArray *arr1 = [LZSortTool sortStrings:arr withSortType:LZSortResultTypeDoubleValues];
//    //    对模型排序
//    NSArray *pArr = [LZSortTool sortObjcsWithProvince:dataArr byKey:@"provinceName" withSortType:LZSortResultTypeDoubleValues];
//    self.weizhangProvinceCityArr = [NSMutableArray arrayWithArray:pArr];
//    NSLog(@"🌺🌺🌺🌺🌺%@",self.weizhangProvinceCityArr);
//
////    for (NSDictionary * dic  in self.proDataSource) {
////        NSString * str = [dic objectForKey:LZSortToolKey];
////        [self.rightIndexArr addObject:str];
////    }
////    [self.tab reloadData];
//}
#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
//    [self requestProvince];
    [self requestJuHeProvinceAndCityData];
}

//请求聚合省市数据
- (void)requestJuHeProvinceAndCityData{
    WEAKSELF
    [self.weizhangViewModel requestJuHeProvinceCityDataSuccess:^(NSString *msg, id responseData) {
//        NSArray * arr = responseData;
//        if (arr.count) {
//            [weakSelf getSortNewArrWithDataArr:arr];
//        }
//        [weakSelf showSuccessTip:msg];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//请求市
- (void)requestCity
{
    JYLocationVM *vm = [[JYLocationVM alloc] init];
    [self showHUD];
    [vm requestCityDataWithProvinceCode:self.selectedProvinceCode success:^(NSString *msg, id responseData) {
        [self hideHUD];
        [self.cityArr addObjectsFromArray:responseData];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//请求省
- (void)requestProvince
{
    JYLocationVM *vm = [[JYLocationVM alloc] init];
    [self showHUD];
    [vm requestProvinceDataSuccess:^(NSString *msg, id responseData) {
        [self hideHUD];
        [self.provinceArr removeAllObjects];
        [self.provinceArr addObjectsFromArray:responseData];
        [self.provinceHotView removeGestureRecognizer:self.chooseProvinceTap];
        [self.provinceHotView addGestureRecognizer:self.chooseProvinceTap];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (NSMutableArray *)cityArr
{
    if (!_cityArr) {
        _cityArr = [[NSMutableArray alloc] init];
    }
    return _cityArr;
}

- (NSMutableArray *)provinceArr
{
    if (!_provinceArr) {
        _provinceArr = [[NSMutableArray alloc] init];
    }
    return _provinceArr;
}

- (JYWeizhangViewModel *)weizhangViewModel
{
    if (!_weizhangViewModel) {
        _weizhangViewModel = [[JYWeizhangViewModel alloc] init];
    }
    return _weizhangViewModel;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
