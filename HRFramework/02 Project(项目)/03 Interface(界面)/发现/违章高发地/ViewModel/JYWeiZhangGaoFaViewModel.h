//
//  JYWeiZhangGaoFaViewModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYWeiZhangGaoFaViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *gaofaArr;
//违章高发地请求
- (void)requestIllegalHighHairSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
