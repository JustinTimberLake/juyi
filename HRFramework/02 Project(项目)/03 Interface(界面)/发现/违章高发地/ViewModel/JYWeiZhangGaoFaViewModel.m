//
//  JYWeiZhangGaoFaViewModel.m
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWeiZhangGaoFaViewModel.h"
#import "JYJUHEWeiZhangGaoFaModel.h"

@implementation JYWeiZhangGaoFaViewModel
//违章高发地请求
- (void)requestIllegalHighHairSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_JUHE_IllegalHighHair) params:nil success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            NSArray * arr = [JYJUHEWeiZhangGaoFaModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.gaofaArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
        
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)gaofaArr{
    if(!_gaofaArr){
        _gaofaArr = [NSMutableArray array];
    }
    return _gaofaArr;
}

@end
