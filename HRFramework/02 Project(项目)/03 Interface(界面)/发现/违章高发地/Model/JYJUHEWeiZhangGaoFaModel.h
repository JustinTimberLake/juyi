//
//  JYJUHEWeiZhangGaoFaModel.h
//  JY
//
//  Created by duanhuifen on 2017/12/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYJUHEWeiZhangGaoFaModel : JYBaseModel
//"": "平四路-桃花桥路",
@property (nonatomic,copy) NSString *title;
//"": [
//             120.6203003,
//             31.3329315
//             ],
@property (nonatomic,strong) NSMutableArray *location;
//"": "苏州市",
@property (nonatomic,copy) NSString *city;
//"": "平四路-桃花桥路",
@property (nonatomic,copy) NSString *address;
//"": "江苏省",
@property (nonatomic,copy) NSString *province;
//"": "平江区",
@property (nonatomic,copy) NSString *district;
//"": "50%信号路口不按规定行驶;33%驾驶机动车违反道路交通信号灯通行的;17%信号灯路口越停车线停车;",
@property (nonatomic,copy) NSString *detail;
//"": 13,
@property (nonatomic,copy) NSString *num;
//"": "中"
@property (nonatomic,copy) NSString *level;
@end
