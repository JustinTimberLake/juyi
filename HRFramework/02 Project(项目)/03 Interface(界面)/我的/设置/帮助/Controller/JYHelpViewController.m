//
//  JYHelpViewController.m
//  JY
//
//  Created by 王方正 on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHelpViewController.h"
#import "JYHelpHeadView.h"
#import "JYHelpTableViewCell.h"

static NSString *const cellIdentifier = @"JYHelpTableViewCell";
static NSString *const headIdentifer = @"JYHelpHeadView";

@interface JYHelpViewController ()
@property (weak, nonatomic) IBOutlet UITableView *myTableview;

@end

@implementation JYHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}

-(void)configUI{
    self.naviTitle = @"帮助";
    [self.myTableview registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.myTableview registerNib:[UINib nibWithNibName:headIdentifer bundle:nil] forHeaderFooterViewReuseIdentifier:headIdentifer];
}

#pragma mark ||============ 代理 && 数据源 ============||
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JYHelpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    JYHelpHeadView *head = [self.myTableview dequeueReusableHeaderFooterViewWithIdentifier:headIdentifer];
    return head;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    <#ViewController#> *cellVC = [[WSQWebViewController alloc]init];
//    
//    [self.navigationController pushViewController:<#viewController#> animated:YES];
//    
}


@end
