//
//  JYHelpHeadView.m
//  JY
//
//  Created by 王方正 on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHelpHeadView.h"

@implementation JYHelpHeadView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"JYHelpHeadView" owner:self options:nil] lastObject];
    }
    return self;
}

@end
