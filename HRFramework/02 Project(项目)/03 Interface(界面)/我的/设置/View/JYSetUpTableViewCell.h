//
//  JYSetUpTableViewCell.h
//  JY
//
//  Created by 王方正 on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSetUpTableViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *index;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@end
