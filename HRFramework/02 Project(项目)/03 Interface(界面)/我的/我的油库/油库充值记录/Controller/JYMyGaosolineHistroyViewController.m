//
//  JYMyGaosolineHistroyViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyGaosolineHistroyViewController.h"
#import "JYSegmentedControl.h"
#import "JYMyGaosolineHistroyTableViewCell.h"
#import "JYOilDepotViewModel.h"

@interface JYMyGaosolineHistroyViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong ,nonatomic)UIView *topBackView;
//segmentControl
@property (strong ,nonatomic)JYSegmentedControl *segmentControl;
////主页面scorllView
//@property (strong, nonatomic)UIScrollView *mainScrollView;
//子控制器数组
@property (strong, nonatomic)NSArray <NSString *>*controllerArr;
//标题数组
@property (strong, nonatomic)NSArray <NSString *>*titleArr;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYOilDepotViewModel *viewModel;
@property (nonatomic,assign) JYOilDepotType oilDepotType;


@end

@implementation JYMyGaosolineHistroyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"历史记录";
    [self initializeData];
    [self configUI];
    [self setUpRefreshData];

}

#pragma mark - ---------- 初始化数据 ----------
- (void)initializeData {
    self.oilDepotType = JYOilDepotType_Buy;
    self.titleArr = @[@"购买记录",@"消耗记录"];
}

#pragma mark - ---------- 构建UI ----------
- (void)configUI {
    WEAKSELF
    [self.view addSubview:self.topBackView];
//    [self.view addSubview:self.mainScrollView];
    [self rightItemTitle:@"清空记录" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
        [weakSelf showPicAlertViewControllerWithTitle:@"确定清空？" andSureBtnTitle:@"确定" sureBtnActionBlock:^{
            [weakSelf requestRemoveAll];
        } closeBtnActionBlock:^{
            
        }];
//        JY_POST_NOTIFICATION(JY_OILDEPOT_REMOVEALL,nil);
    }];
    
//    [self configChildControllers];
    [self createSegMentControl];
//    [self configScrollView];
    
    
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyGaosolineHistroyTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyGaosolineHistroyTableViewCell"];
}

- (void)configChildControllers {
    [self.controllerArr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Class class = NSClassFromString(obj);
        [self addChildViewController:[class new]];
    }];
}

//- (void)configScrollView {
//    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [self.mainScrollView addSubview:obj.view];
//        obj.view.frame = CGRectMake(idx * SCREEN_WIDTH, 0, self.mainScrollView.width, self.mainScrollView.height);
//    }];
//    self.mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * self.childViewControllers.count, self.mainScrollView.size.height);
//    //设置默认controller
//   
//    // YSJ_MyFavoriteVideoViewController *vc = self.childViewControllers[0];
//}

- (void)createSegMentControl {
    WEAKSELF
    JYSegmentedControl *segMentControlView = [[JYSegmentedControl alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    segMentControlView.titleSize = 13;
    [segMentControlView setTitles:self.titleArr];
    [self.topBackView addSubview:segMentControlView];
    segMentControlView.selectedSegmentBlock = ^(NSInteger index){
//        [UIView animateWithDuration:.25f animations:^{
//            self.mainScrollView.contentOffset = CGPointMake((index - 1000) * SCREEN_WIDTH, 0);
//        }];
        

        switch (index-1000) {
            case 0:{
//                JYBuyGaoSolineHistroyViewController *vc = self.childViewControllers[index-1000];
                weakSelf.oilDepotType = JYOilDepotType_Buy;
            }
                break;
                
            case 1:{
                weakSelf.oilDepotType = JYOilDepotType_Consume;
//                JYUserGaosolineHistroyViewController *vc = self.childViewControllers[index-1000];
            }
                break;
        
            default:
                break;
        }
        [weakSelf requestGetOilDepotListWithIsMore:NO];
    };
    [segMentControlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark networkRequest (网络请求)
- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestGetOilDepotListWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestGetOilDepotListWithIsMore:YES];
    }];
    [self.myTableView.mj_header beginRefreshing];
}

//请求列表
- (void)requestGetOilDepotListWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetOilDepotListWithType:self.oilDepotType isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//删除
- (void)requestDeleteOilDepotWithOilDepotId:(NSString *)oilDepotId{
    WEAKSELF
    [self.viewModel requestDelOilDepotWithOilDepotId:oilDepotId success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf requestGetOilDepotListWithIsMore:NO];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//清空所有
- (void)requestRemoveAll{
    WEAKSELF
    [self showHUD];
    [self.viewModel requestRemoveOilDepotWithType:self.oilDepotType success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf requestGetOilDepotListWithIsMore:NO];
    } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
    }];
}


- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}



#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyGaosolineHistroyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyGaosolineHistroyTableViewCell" forIndexPath:indexPath] ;
    cell.model = self.viewModel.listArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}
-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAK(weakSelf);
    JYOilDepotModel * model = self.viewModel.listArr[indexPath.row];
    UITableViewRowAction *rowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        [weakSelf requestDeleteOilDepotWithOilDepotId:model.oilDepotId];
    }];
    rowAction.backgroundColor = [UIColor redColor];
    NSArray *arr = @[rowAction];
    return arr;
}


#pragma mark - ---------- 懒加载 ----------
#pragma mark -topBackView-
- (UIView *)topBackView {
    if (!_topBackView) {
        _topBackView = [[UIView alloc]init];
        _topBackView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    }
    return _topBackView;
}

//#pragma mark -mainScrollView-
//- (UIScrollView *)mainScrollView {
//    if (!_mainScrollView) {
//        _mainScrollView = [[UIScrollView alloc]init];
//        _mainScrollView.frame = CGRectMake(0, 50, SCREEN_WIDTH*(self.controllerArr.count-1), SCREEN_HEIGHT-50-64-49);
//        _mainScrollView.pagingEnabled = YES;
//        _mainScrollView.scrollEnabled = NO;
//    }
//    return _mainScrollView;
//}

- (JYOilDepotViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYOilDepotViewModel alloc] init];
    }
    return _viewModel;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
