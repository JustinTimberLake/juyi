//
//  JYMyGaosolineHistroyTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyGaosolineHistroyTableViewCell.h"

@interface JYMyGaosolineHistroyTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end

@implementation JYMyGaosolineHistroyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(JYOilDepotModel *)model{
    _model = model;
    self.titleLab.text = model.oilDepotTitle;
    self.timeLab.text = [NSString YYYYMMDDWithDataStr:model.oilDepotTime];
    
}

@end
