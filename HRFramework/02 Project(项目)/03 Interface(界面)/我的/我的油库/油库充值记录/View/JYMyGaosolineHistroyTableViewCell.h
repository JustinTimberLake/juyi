//
//  JYMyGaosolineHistroyTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYOilDepotModel.h"


@interface JYMyGaosolineHistroyTableViewCell : UITableViewCell

@property (nonatomic,strong) JYOilDepotModel *model;
@end
