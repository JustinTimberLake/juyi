//
//  JYOilDepotModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/27.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"


@interface JYOilDepotModel : JYBaseModel
// 油记录ID
@property (nonatomic,copy) NSString *oilDepotId;
//内容
@property (nonatomic,copy) NSString *oilDepotTitle;
//时间
@property (nonatomic,copy) NSString *oilDepotTime;
// 1购买记录2消耗记录
@property (nonatomic,copy) NSString *oilDepotType;

@end
