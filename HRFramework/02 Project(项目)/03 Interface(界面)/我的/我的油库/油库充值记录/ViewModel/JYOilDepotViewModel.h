//
//  JYOilDepotViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/27.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYOilDepotModel.h"

typedef NS_ENUM(NSInteger,JYOilDepotType) {
    JYOilDepotType_Buy = 1, //购买记录
    JYOilDepotType_Consume,  //消费
    JYOilDepotType_All       //全部
};

@interface JYOilDepotViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray<JYOilDepotModel *> *listArr;


//获取油库收支记录
- (void)requestGetOilDepotListWithType:(JYOilDepotType)oilDepotType isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除油库收支记录
- (void)requestDelOilDepotWithOilDepotId:(NSString *)oilDepotId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

// 清空油库收支记录
- (void)requestRemoveOilDepotWithType:(JYOilDepotType)oilDepotType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;



@end
