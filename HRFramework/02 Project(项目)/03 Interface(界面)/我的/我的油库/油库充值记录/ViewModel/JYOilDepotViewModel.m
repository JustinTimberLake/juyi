//
//  JYOilDepotViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/9/27.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOilDepotViewModel.h"

@implementation JYOilDepotViewModel

//获取油库收支记录
- (void)requestGetOilDepotListWithType:(JYOilDepotType)oilDepotType isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(oilDepotType);
    NSString * pageCount;
    if (self.listArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+2 ] : @"1";
    }
    dic[@"pageNum"] = pageCount;
    dic[@"pageSize"] = @(10);

    HRRequestManager * manager = [[HRRequestManager alloc]init];
    NSLog(@"%@---%@",JY_PATH(JY_OILDEPOT_GetOilDepotList),dic);
    [manager POST_URL:JY_PATH(JY_OILDEPOT_GetOilDepotList) params:dic success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.listArr removeAllObjects];
            }
            NSArray *arr = [JYOilDepotModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//删除油库收支记录
- (void)requestDelOilDepotWithOilDepotId:(NSString *)oilDepotId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"oilDepotId"] = oilDepotId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_OILDEPOT_DelOilDepot) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

// 清空油库收支记录
- (void)requestRemoveOilDepotWithType:(JYOilDepotType)oilDepotType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(oilDepotType);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_OILDEPOT_RemoveOilDepot) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray<JYOilDepotModel *> *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}


@end
