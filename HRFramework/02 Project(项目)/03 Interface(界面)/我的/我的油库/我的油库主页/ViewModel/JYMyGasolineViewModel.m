//
//  JYMyGasolineViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/31.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyGasolineViewModel.h"

@implementation JYMyGasolineViewModel

- (NSMutableArray<JYMyGasolineModel*>*)gasolinesArray{
    if (!_gasolinesArray) {
        _gasolinesArray = [NSMutableArray array];
    }
    return _gasolinesArray;
}

- (NSMutableArray<JYMyGasolineModel*>*)dieselsArray{
    if (!_dieselsArray) {
        _dieselsArray = [NSMutableArray array];
    }
    return _dieselsArray;
}

- (void)requesMyGasolineListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    NSLog(@"%@--%@",JY_PATH(JY_PERSONCENTER_MyGasoline),params);
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyGasoline) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.gasolinesArray addObjectsFromArray: [JYMyGasolineModel mj_objectArrayWithKeyValuesArray:RESULT_DATA[@"gasolines"]]];
            [self.dieselsArray addObjectsFromArray: [JYMyGasolineModel mj_objectArrayWithKeyValuesArray:RESULT_DATA[@"diesels"]]];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
    

}
@end
