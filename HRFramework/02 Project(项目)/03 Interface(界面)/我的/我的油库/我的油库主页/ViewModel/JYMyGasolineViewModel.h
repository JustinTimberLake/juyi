//
//  JYMyGasolineViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/31.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYMyGasolineModel.h"
@interface JYMyGasolineViewModel : JYBaseViewModel
@property (nonatomic, strong) NSMutableArray <JYMyGasolineModel *>*gasolinesArray;
@property (nonatomic, strong) NSMutableArray <JYMyGasolineModel *>*dieselsArray;

- (void)requesMyGasolineListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;



@end
