//
//  JYMyGasolineModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/31.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
/*
 oilDepotId://油ID
 oilModel://油号
 oilNum://剩余油量
 oilPrice://单价
*/
@interface JYMyGasolineModel : JYBaseModel
@property (nonatomic, copy)NSString *oilDepotId;
@property (nonatomic, copy)NSString *oilModel;
@property (nonatomic, copy)NSString *oilNum;
@property (nonatomic, copy)NSString *oilPrice;
@end
