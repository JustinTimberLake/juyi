//
//  JYMyGasolineViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyGasolineViewController.h"
#import "JYMyGasolineTableViewCell.h"
#import "JYMyGaosolineHistroyViewController.h"
#import "JYRechargGaosolineViewController.h"
#import "JYMyGasolineViewModel.h"
#import "JYMyAccountViewController.h"

@interface JYMyGasolineViewController ()<
  UITableViewDelegate,
  UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) JYMyGasolineViewModel * viewModel;

@end

@implementation JYMyGasolineViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JYMyGasolineViewModel*)viewModel {
    if (!_viewModel) {
        _viewModel = [[JYMyGasolineViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUi];
    [self networkRequest];
}

//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUi{
    WEAKSELF
    self.naviTitle = @"我的油库";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYMyGasolineTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyGasolineTableViewCell"];
    [self rightItemTitle:@"我的账户" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
        JYMyAccountViewController * myAccountVC = [[JYMyAccountViewController alloc] init];
        [weakSelf.navigationController pushViewController:myAccountVC animated:YES];
    }];

}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [self.viewModel requesMyGasolineListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myTableView reloadData];
  } failure:^(NSString *errorMsg) {
        [weakSelf hideHUD];
        [weakSelf showSuccessTip:errorMsg];
  }];
}
#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）
//历史记录
- (IBAction)histroyBtn:(id)sender {
    JYMyGaosolineHistroyViewController * VC = [[JYMyGaosolineHistroyViewController alloc]init];
    [self.navigationController pushViewController:VC animated:YES];
}

//充值油库
- (IBAction)rechargeBtn:(id)sender {
    JYRechargGaosolineViewController *VC = [[JYRechargGaosolineViewController alloc]init];
    [self.navigationController pushViewController:VC animated:YES];
}
#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyGasolineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyGasolineTableViewCell" forIndexPath:indexPath];
    if (indexPath.row == 0) {
        cell.modelArray = self.viewModel.gasolinesArray;
        cell.titleLabel.text = @"汽油";
    }else{
        cell.modelArray = self.viewModel.dieselsArray;
        cell.titleLabel.text = @"柴油";
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
       return 105 + (self.viewModel.dieselsArray.count+2-1)/2 * 65;
    }
   
    return 105 +  (self.viewModel.gasolinesArray.count+2-1)/2 * 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}



@end
