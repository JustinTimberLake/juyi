//
//  JYGasolineCollectionViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYMyGasolineModel.h"
//#import "JYOilGunModel.h"
#import "JYJuHeOilModel.h"

@interface JYGasolineCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *ModelLb;
@property (weak, nonatomic) IBOutlet UILabel *NumLB;
@property (nonatomic, strong)JYMyGasolineModel * model;
//主要用这个模型中的俩属性
//@property (nonatomic,strong) JYOilGunModel *oilModel;

@property (nonatomic,strong)  JYJuHeOilModel*oilModel;



@end
