//
//  JYGasolineCollectionViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGasolineCollectionViewCell.h"

@implementation JYGasolineCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(JYMyGasolineModel *)model{
    _model = model;
    _NumLB.text = [NSString stringWithFormat:@"%@L",_model.oilNum];
    _ModelLb.text = [NSString stringWithFormat:@"%@",_model.oilModel];
}

//- (void)setOilModel:(JYOilGunModel *)oilModel{
//    _oilModel = oilModel;
//    self.NumLB.text = SF(@"%@元/升",oilModel.oilPrice);
//    self.ModelLb.text = SF(@"%@",oilModel.oilModel);
//}

- (void)setOilModel:(JYJuHeOilModel *)oilModel{
    _oilModel = oilModel;
    self.NumLB.text = SF(@"%@元/升",oilModel.oilPrice);
    self.ModelLb.text = SF(@"%@",oilModel.oilType);
}
@end
