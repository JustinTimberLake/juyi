//
//  JYMyGasolineTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,JYCellType) {
    JYCellType_MyGasoline,//我的油库
    JYCellType_TodayOilPrice //今日油价
};

@interface JYMyGasolineTableViewCell : UITableViewCell <
    UICollectionViewDelegate,
    UICollectionViewDataSource
>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic, strong) NSMutableArray * modelArray;
@property (nonatomic,assign) JYCellType cellType;

- (void)updataCellMode:(JYCellType)cellType;
@end
