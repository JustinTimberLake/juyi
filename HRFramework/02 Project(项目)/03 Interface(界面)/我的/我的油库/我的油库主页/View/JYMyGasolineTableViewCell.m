//
//  JYMyGasolineTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyGasolineTableViewCell.h"
#import "JYGasolineCollectionViewCell.h"
@implementation JYMyGasolineTableViewCell
static CGFloat space = 0;
static CGFloat marginSpace = 5;

- (void)awakeFromNib {
    [super awakeFromNib];
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    [self registerCollcetionViewCell];

    // Initialization code
}

- (void)setModelArray:(NSMutableArray *)modelArray{
    _modelArray = modelArray;
    [self.myCollectionView reloadData];
}

- (void)updataCellMode:(JYCellType)cellType{
    self.cellType = JYCellType_TodayOilPrice;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)registerCollcetionViewCell{
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYGasolineCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"JYGasolineCollectionViewCell"];
}
#pragma mark - ---------- CollectionView Delegate && DataSource && FlowLayout ----------
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.modelArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYGasolineCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"JYGasolineCollectionViewCell" forIndexPath:indexPath];
    if (self.cellType == JYCellType_TodayOilPrice) {
        cell.oilModel = self.modelArray[indexPath.row];
    }else{
        cell.model = self.modelArray[indexPath.row];
    }
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH - 45)/2, 65);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collecjtionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 4;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}

@end
