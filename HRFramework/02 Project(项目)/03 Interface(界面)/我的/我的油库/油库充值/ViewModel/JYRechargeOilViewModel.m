//
//  JYRechargeOilViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargeOilViewModel.h"
#import "JYRechargeOilDepotTypeModel.h"

@implementation JYRechargeOilViewModel

//充值油库
- (void)requestRechargeOilWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSLog(@"%@---%@",JY_PATH(JY_PERSONCENTER_MyRechangeGasoline),params);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyRechangeGasoline) params:params success:^(id result) {
        if (RESULT_SUCCESS) {
            self.orderId = RESULT_DATA[@"orderId"];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取充值油库的油型数据
- (void)requestGetOilDepotDateSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSLog(@"%@",JY_PATH(JY_OILDEPOT_GetOilDepotDate));
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_OILDEPOT_GetOilDepotDate) params:nil success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",RESULT_DATA);
            weakSelf.oilTypeModel = [JYRechargeOilDepotTypeModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}




@end
