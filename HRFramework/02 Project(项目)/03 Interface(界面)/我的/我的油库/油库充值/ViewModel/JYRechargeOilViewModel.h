//
//  JYRechargeOilViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYRechargeOilDepotTypeModel.h"

@interface JYRechargeOilViewModel : JYBaseViewModel

@property (nonatomic,copy) NSString *orderId;

@property (nonatomic,strong) JYRechargeOilDepotTypeModel *oilTypeModel;
//充值油库
- (void)requestRechargeOilWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取充值油库的油型数据
- (void)requestGetOilDepotDateSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
