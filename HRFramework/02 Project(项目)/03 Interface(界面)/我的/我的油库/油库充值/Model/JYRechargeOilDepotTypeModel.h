//
//  JYRechargeOilDepotTypeModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYRechargeOilDepotModel.h"

@interface JYRechargeOilDepotTypeModel : JYBaseModel

//柴油
@property (nonatomic,strong) NSArray<JYRechargeOilDepotModel *> *dieselOil;
//汽油
@property (nonatomic,strong) NSArray<JYRechargeOilDepotModel *> *gasoline;


@end
