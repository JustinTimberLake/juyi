//
//  JYRechargeOilDepotTypeModel.m
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargeOilDepotTypeModel.h"

@implementation JYRechargeOilDepotTypeModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"dieselOil":@"JYRechargeOilDepotModel",
             @"gasoline":@"JYRechargeOilDepotModel"
             };
}
@end
