//
//  JYRechargeOilDepotModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYRechargeOilDepotModel : JYBaseModel
//油型ID
@property (nonatomic,copy) NSString *oilDepotId;
//油型名称
@property (nonatomic,copy) NSString *oilTypeTitle;
//油型单价
@property (nonatomic,copy) NSString *oilPrice;
//最大存储数值
@property (nonatomic,copy) NSString *maxStorageValue;

@property (nonatomic,assign,getter=isSelect) BOOL select;


@end
