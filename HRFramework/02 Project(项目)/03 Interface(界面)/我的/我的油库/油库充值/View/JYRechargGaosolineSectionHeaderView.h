//
//  JYRechargGaosolineSectionHeaderView.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYRechargGaosolineSectionHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightPriceLab;

- (void)updataPriceWithPrice:(NSString *)price;

@end
