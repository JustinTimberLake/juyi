//
//  JYRechargGaosolineTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYRechargeOilDepotModel.h"
@interface JYRechargGaosolineTableViewCell : UITableViewCell
@property (nonatomic,strong) JYRechargeOilDepotModel  * model;


@end
