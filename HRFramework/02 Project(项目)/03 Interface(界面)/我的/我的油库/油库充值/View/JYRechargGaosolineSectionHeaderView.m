//
//  JYRechargGaosolineSectionHeaderView.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargGaosolineSectionHeaderView.h"

@implementation JYRechargGaosolineSectionHeaderView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.rightPriceLab.hidden = YES;
}

- (void)updataPriceWithPrice:(NSString *)price{
    self.rightPriceLab.hidden = NO;
    self.rightPriceLab.text = price;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
