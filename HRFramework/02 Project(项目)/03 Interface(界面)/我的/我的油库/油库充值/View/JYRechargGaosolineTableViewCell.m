//
//  JYRechargGaosolineTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargGaosolineTableViewCell.h"

@interface JYRechargGaosolineTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *oilTypeLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UIImageView *selectImageView;

@end

@implementation JYRechargGaosolineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//- (IBAction)selectedBtn:(UIButton *)sender {
//
//    if (sender.selected) {
//        sender.selected = NO;
//    }else{
//        sender.selected = YES;
//
//    }
//
//}

- (void)setModel:(JYRechargeOilDepotModel *)model{
    _model = model;
    self.oilTypeLab.text = SF(@"%@ (最大%@L)",model.oilTypeTitle,model.maxStorageValue);
    self.priceLab.text = SF(@"%@元/升",model.oilPrice);
    self.selectImageView.image = model.isSelect ? [UIImage imageNamed:@"单选-选中"] : [UIImage imageNamed:@"单选-未选中"];
    
}

@end
