//
//  JYRechargGaosolineFooterView.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargGaosolineFooterView.h"
#import "JYCommonWebViewController.h"

@interface JYRechargGaosolineFooterView ()

@property (nonatomic,assign) NSInteger oilNum;//充值油量
@property (nonatomic,assign) float totalPrice;
@property (weak, nonatomic) IBOutlet UIButton *rechargeBtn;

@end

@implementation JYRechargGaosolineFooterView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self payBtnAction:self.accountBtn];
    self.oilNum = 0;
    [self.oilTextField addTarget:self action:@selector(textfieldChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)setOilModel:(JYRechargeOilDepotModel *)oilModel{
    _oilModel = oilModel;
    if (!oilModel.oilTypeTitle.length) {
        self.afterRechargeLab.text = SF(@"%@L",self.oilTextField.text);
    }else{
        self.afterRechargeLab.text = SF(@"%@%@L",oilModel.oilTypeTitle,self.oilTextField.text);

    }
}

//同意按钮点击
- (IBAction)agreeBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
//    if (_returnAgreeBtnAction) {
//        self.returnAgreeBtnAction(sender.selected);
//    }
}

- (IBAction)btnAction:(UIButton *)sender {
    if (!self.oilModel.oilDepotId.length && self.footerViewTipBlock) {
        self.footerViewTipBlock(@"请选择油型");
        return;
    }
    
    if (sender.tag == 1001) {
//        加
        self.oilNum += 10;
//        if (self.oilNum >= [self.oilModel.maxStorageValue intValue]) {
//            self.oilNum = [self.oilModel.maxStorageValue intValue];
//        }
        
    }else{
//        减
        if (self.oilNum <= 10) {
            return;
        }else{
            self.oilNum -= 10;
        }
    }
    
    [self updateRechargeBtnUI];

    [self updataMoneyAndOilNum];
}

//更新充值按钮UI
- (void)updateRechargeBtnUI{
    if (self.oilNum > [self.oilModel.maxStorageValue intValue]) {
        self.oilNum = [self.oilModel.maxStorageValue intValue];
    }
    
    if (self.oilNum ==0) {
        self.rechargeBtn.enabled = NO;
        [self.rechargeBtn setBackgroundColor:[UIColor colorWithHexString:JYUCOLOR_GRAY_BG]];
        [self.rechargeBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
    }else{
        self.rechargeBtn.enabled = YES;
        [self.rechargeBtn setBackgroundColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN]];
        [self.rechargeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    self.oilTextField.text  = SF(@"%ld",(long)self.oilNum);

}
#pragma mark ==================textField代理==================
- (void)textfieldChange:(UITextField *)textfield{
    self.oilNum = [textfield.text intValue];
    if (!self.oilModel.oilDepotId.length && self.footerViewTipBlock) {
        self.footerViewTipBlock(@"请选择油型");
        return;
    }
    if ([textfield.text intValue] <= 0) {
        if (self.footerViewTipBlock) {
            self.footerViewTipBlock(@"请输入大于0的整数");
        }
    }else{
        [self updateRechargeBtnUI];
        [self updataMoneyAndOilNum];
    }
}

//协议按钮点击
- (IBAction)delegateBtnAction:(UIButton *)sender {
    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
    webVC.navTitle = @"用户油卡充值协议";
    webVC.webViewType = JYWebViewType_RechargeOilCarDelegate;
    [self.viewController.navigationController pushViewController:webVC animated:YES];
}

//支付按钮点击
- (IBAction)payBtnAction:(UIButton *)sender {
    self.selectBtn.selected = NO;
    self.selectBtn = sender;
    self.selectBtn.selected = YES;
    
    if (sender.tag == 1000) {
        self.payType = @"账户支付";
    }else if (sender.tag == 1001){
        self.payType = @"微信支付";
    }else{
        self.payType = @"支付宝支付";
    }
    
//    if (_returnPayBtnAction) {
//        self.returnPayBtnAction(self.payType);
//    }
}
- (IBAction)rechargeBtnAction:(UIButton *)sender {
    WEAKSELF
    if (_rechargeBtnAction) {
        self.rechargeBtnAction(weakSelf.agreeBtn.selected,weakSelf.oilModel.oilDepotId,self.oilTextField.text, self.totalMoneyLab.text,self.payType);
    }
}

#pragma mark ==================更新UI==================

//更新油量和总价
- (void)updataMoneyAndOilNum{
    self.afterRechargeLab.text = SF(@"%@%@L",self.oilModel.oilTypeTitle,self.oilTextField.text);
    self.totalPrice = [self.oilTextField.text floatValue] * [self.oilModel.oilPrice floatValue];
    self.totalMoneyLab.text = SF(@"%.2f",self.totalPrice) ;
}

@end
