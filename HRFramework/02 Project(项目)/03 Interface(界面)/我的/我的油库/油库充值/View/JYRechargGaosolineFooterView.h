//
//  JYRechargGaosolineFooterView.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYRechargeOilDepotModel.h"

//typedef NS_ENUM(NSInteger,JYPayType) {
//    JYPayType_Alipay = 1,//支付宝
//    JYPayType_WeChat,//微信
//    JYPayType_Account//账户余额
//};
@interface JYRechargGaosolineFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLab;
@property (weak, nonatomic) IBOutlet UILabel *afterRechargeLab;
@property (weak, nonatomic) IBOutlet UIButton *accountBtn;
@property (weak, nonatomic) IBOutlet UIButton *weiChatBtn;
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UITextField *oilTextField;
@property (nonatomic,strong) UIButton *selectBtn; //已经选中按钮
@property (nonatomic,assign)NSString * payType; //支付类型
@property (nonatomic,strong) JYRechargeOilDepotModel *oilModel;

//支付类型按钮点击
@property (nonatomic,copy) void(^returnPayBtnAction) (NSString * payType);
//同意按钮点击
//@property (nonatomic,copy) void (^returnAgreeBtnAction)(BOOL isAgree);

@property (nonatomic,copy) void (^rechargeBtnAction)(BOOL isAgree ,NSString * oilId,NSString * oilNum,NSString *totalMoney,NSString * payType);

////增加减少按钮的点击
//@property (nonatomic,copy) void(^plusAndMinusBtnActionBlock)(NSString *str);

//通用的提示语block
@property (nonatomic,copy) void (^footerViewTipBlock)(NSString *str);



@end
