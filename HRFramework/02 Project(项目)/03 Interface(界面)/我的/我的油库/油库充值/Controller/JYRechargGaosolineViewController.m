//
//  JYRechargGaosolineViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargGaosolineViewController.h"
#import "JYRechargGaosolineTableViewCell.h"
#import "JYRechargGaosolineSectionHeaderView.h"
#import "JYRechargGaosolineFooterView.h"
#import "JYRechargeOilViewModel.h"
#import "JYPayViewModel.h"
#import "JYAlertPicViewController.h"
#import "JYRechargeOilDepotTypeModel.h"
#import <WXApi.h>


@interface JYRechargGaosolineViewController ()<
  UITableViewDataSource,
  UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
//充值VM
@property (nonatomic,strong) JYRechargeOilViewModel *rechargeOilViewModel;
@property (nonatomic,strong) JYPayViewModel *payViewModel;
//@property (nonatomic,copy) NSString *totalMoney;
@property (nonatomic,copy) NSString *payTypeStr; //支付类型
@property (nonatomic,strong) JYRechargeOilDepotModel *selectOilModel; //选中的油模型
@property (nonatomic,copy) NSString *selectOilType; //选中油型 1柴油 2汽油
@property (nonatomic,copy) NSString *oilId;
@property (nonatomic,copy) NSString *oilNum;
@property (nonatomic,copy) NSString *oilMoney;


@end

@implementation JYRechargGaosolineViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self addNotification];
    [self requestOilType];
}

//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------
- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
//        [self showSuccessTip:noti.object];
        if ([noti.object isEqualToString:@"账户余额不足"]) {
            [self showAlertPicViewWithNotEnoughMoney];
        }
    }
}
#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"充值油库";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYRechargGaosolineTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYRechargGaosolineTableViewCell"];
    
}

#pragma mark networkRequest (网络请求)
- (void)requestOilType{
    WEAKSELF
    [self.rechargeOilViewModel requestGetOilDepotDateSuccess:^(NSString *msg, id responseData) {
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//- (void)requestRechargeOilWithOilId:(NSString *)oilId oilNum:(NSString *)oilNum totalPrice:(NSString *)totalPrice{

- (void)requestRechargeOil{
    WEAKSELF
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    params[@"C"] = [User_InfoShared shareUserInfo].c;
    params[@"oilDepotId"] = self.oilId;
    params[@"oilNum"] = self.oilNum;
    params[@"type"] =  self.selectOilType;
//    params[@"totalPrice"] = self.oilMoney;
    [params setObject:self.oilMoney forKey:@"totalPrice"];
    
    [self.rechargeOilViewModel requestRechargeOilWithParams:params success:^(NSString *msg, id responseData) {
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        if (weakSelf.rechargeOilViewModel.orderId.length) {
            dic[@"orderId"] = weakSelf.rechargeOilViewModel.orderId;
            dic[@"orderType"] = @"油库充值订单";
        }
        JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);

        [weakSelf payTypeWithStr:weakSelf.payTypeStr andOrderId:weakSelf.rechargeOilViewModel.orderId];
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}
#pragma mark ==================支付代码==================

//支付方式选择
- (void)payTypeWithStr:(NSString *)str andOrderId:(NSString *)orderId{
    if ([str isEqualToString:@"账户支付"]) {
        NSLog(@"账户支付");
        if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [self.oilMoney floatValue]) {
            [self showAlertPicViewWithAccountTypeWithOrderId:orderId andTotalPrice:self.oilMoney];

        }else{
            [self showAlertPicViewWithNotEnoughMoney];
        }
    }else if([str isEqualToString:@"微信支付"]){
        NSLog(@"微信支付");
        if (![WXApi isWXAppInstalled]) {
            [self showSuccessTip:@"请安装微信客户端"];
            return;
        }else{
            [self requestWXSignWithOrderNum:orderId];
        }
    }else{
        NSLog(@"支付宝支付");
        [self requestAlipaySignWithOrderNum:orderId];
    }
}


//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
        //        [weakSelf showAlertPicViewWithNotEnoughMoney];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
}



//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            //            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
                //                switch ([code intValue]) {
                //                    case 9000:
                //                        //                        [self popToRootViewControllerAnimated:NO];
                //                    {
                //                        [weakSelf turnPaySuccessPage];
                //                    }
                //                        break;
                //                    case 8000:
                //                        //                        [self showSuccessTip:@"正在处理中"];
                //                        break;
                //                    case 4000:
                //                        //                        [self showSuccessTip:@"支付失败"];
                //                        break;
                //                    case 6001:
                //                        //                        [self showSuccessTip:@"支付已取消"];
                //                        break;
                //                    case 6002:
                //                        //                        [self showSuccessTip:@"网络连接错误"];
                //                        break;
                //                    default:
                //                        //                        [self showSuccessTip:@"支付失败"];
                //                        break;
                //                }
                
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}


#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId andTotalPrice:(NSString *)totalPrice{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalPrice floatValue]);
    
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalPrice,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}




#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    JYRechargGaosolineSectionHeaderView *headerSectionView = XIB(JYRechargGaosolineSectionHeaderView);
//    JYRechargeOilDepotTypeModel * model = self.rechargeOilViewModel.oilTypelistArr[section];
//    headerSectionView.titleLabel.text = [model.type intValue] == 1?@"柴油":@"汽油";
    headerSectionView.titleLabel.text = section == 0 ? @"汽油":@"柴油";
    return headerSectionView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    WEAKSELF
    if (section == 1) {
        JYRechargGaosolineFooterView *footerSectionView = XIB(JYRechargGaosolineFooterView);
        footerSectionView.oilModel = self.selectOilModel;
//        footerSectionView.returnPayBtnAction = ^(NSString * payType) {
////            if (payType == JYPayType_Alipay) {
////                weakSelf.payTypeStr = @"支付宝支付";
////            }else if (payType == JYPayType_WeChat){
////                weakSelf.payTypeStr = @"微信支付";
////            }else {
////                weakSelf.payTypeStr = @"账户支付";
////            }
//            weakSelf.payTypeStr = payType;
//        };
        footerSectionView.footerViewTipBlock = ^(NSString *str) {
            [weakSelf showSuccessTip:str];
        };
        
        footerSectionView.rechargeBtnAction = ^(BOOL isAgree ,NSString * oilId,NSString * oilNum,NSString *totalMoney,NSString * payType) {
            if (!oilId.length) {
                [weakSelf showSuccessTip:@"请选择油型"];
                return ;
            }
            if (![oilNum floatValue]) {
                [weakSelf showSuccessTip:@"请选择油量"];
                return;
            }

            if (!isAgree) {
                [weakSelf showSuccessTip:@"请同意充值协议"];
                return ;
            }
            if ([payType isEqualToString:@"微信支付"] && ![WXApi isWXAppInstalled]) {
                [self showSuccessTip:@"请安装微信客户端"];
                return;
            }
            weakSelf.oilId = oilId;
            weakSelf.oilNum = oilNum;
            weakSelf.oilMoney = totalMoney;
            weakSelf.payTypeStr = payType;
//            [weakSelf requestRechargeOilWithOilId:oilId oilNum:oilNum totalPrice:totalMoney];
            [weakSelf requestRechargeOil];
            
        };
        return footerSectionView;
    }
    return nil;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYRechargGaosolineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYRechargGaosolineTableViewCell" forIndexPath:indexPath] ;
    if (indexPath.section == 0) {
        
        cell.model = self.rechargeOilViewModel.oilTypeModel.gasoline[indexPath.row];
    }else{
        cell.model = self.rechargeOilViewModel.oilTypeModel.dieselOil[indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return 560;
    }
    return 9;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return  self.rechargeOilViewModel.oilTypeModel.gasoline.count;
    }else{
       return  self.rechargeOilViewModel.oilTypeModel.dieselOil.count;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    if (indexPath.section == 0) {
       [self.rechargeOilViewModel.oilTypeModel.gasoline enumerateObjectsUsingBlock:^(JYRechargeOilDepotModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == indexPath.row) {
                obj.select = YES;
                weakSelf.selectOilModel = obj;
                weakSelf.selectOilType = @"2";
            }else{
                obj.select = NO;
            }
        }];
        [self.rechargeOilViewModel.oilTypeModel.dieselOil enumerateObjectsUsingBlock:^(JYRechargeOilDepotModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.select = NO;
        }];

    }else{
        [self.rechargeOilViewModel.oilTypeModel.dieselOil enumerateObjectsUsingBlock:^(JYRechargeOilDepotModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == indexPath.row) {
                obj.select = YES;
                weakSelf.selectOilModel = obj;
                weakSelf.selectOilType = @"1";
            }else{
                obj.select = NO;
            }
        }];
        [self.rechargeOilViewModel.oilTypeModel.gasoline enumerateObjectsUsingBlock:^(JYRechargeOilDepotModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.select = NO;
        }];
    }
    
    [self.myTableView reloadData];
}

- (JYRechargeOilViewModel *)rechargeOilViewModel
{
    if (!_rechargeOilViewModel) {
        _rechargeOilViewModel = [[JYRechargeOilViewModel alloc] init];
    }
    return _rechargeOilViewModel;
}

- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}

@end
