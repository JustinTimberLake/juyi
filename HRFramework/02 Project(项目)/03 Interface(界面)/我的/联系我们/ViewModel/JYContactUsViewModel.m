//
//  JYContactUsViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/12/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYContactUsViewModel.h"

@implementation JYContactUsViewModel


//获取联系我们
- (void)requestContactUsSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_CONTACT_contactUS));
    [manager POST_URL:JY_PATH(JY_CONTACT_contactUS) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"%@",result);
            weakSelf.contactUsModel = [JYContactUsModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
