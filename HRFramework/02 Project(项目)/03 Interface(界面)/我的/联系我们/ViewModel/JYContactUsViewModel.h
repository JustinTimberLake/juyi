//
//  JYContactUsViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYContactUsModel.h"

@interface JYContactUsViewModel : JYBaseViewModel

@property (nonatomic,strong) JYContactUsModel *contactUsModel;

//获取联系我们
- (void)requestContactUsSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
