//
//  JYContactUsViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYContactUsViewController.h"
#import "JYContactUsViewModel.h"
#import "HRCall.h"
#import "JYAlertPicViewController.h"
#import "JYChatViewController.h"

@interface JYContactUsViewController ()
@property (nonatomic,strong) JYContactUsViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UILabel *userTelLab;
@property (weak, nonatomic) IBOutlet UILabel *storeTelLab;
@property (weak, nonatomic) IBOutlet UILabel *onlineLab;

@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *telLab;
@property (weak, nonatomic) IBOutlet UILabel *emailLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;


@end

@implementation JYContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"联系我们";
    [self requestContactUs];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnClick:(UIButton *)sender {

    if (sender.tag == 1000 && self.viewModel.contactUsModel.userService.length) {
        [self showAlertPicViewWithTelNum:self.viewModel.contactUsModel.userService];
    }else if (sender.tag == 1001 && self.viewModel.contactUsModel.merchantService.length){
        [self showAlertPicViewWithTelNum:self.viewModel.contactUsModel.merchantService];
    }else{
        //单聊界面
//        15711453411
        JYChatViewController *chatController = [[JYChatViewController alloc] initWithConversationChatter:self.viewModel.contactUsModel.accountNumber  conversationType:EMConversationTypeChat];
        chatController.chatTitle = @"客服";
        [self.navigationController pushViewController:chatController animated:YES];
    }
}


#pragma mark ==================网络请求==================
- (void)requestContactUs{
    WEAKSELF
    [self.viewModel requestContactUsSuccess:^(NSString *msg, id responseData) {
        [weakSelf updataUI];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ==================更新UI==================
//更新UI
- (void)updataUI{
    self.userTelLab.text = self.viewModel.contactUsModel.userService;
    self.storeTelLab.text = self.viewModel.contactUsModel.merchantService;
    self.onlineLab.text = self.viewModel.contactUsModel.lineService;
    [self.serviceImageView sd_setImageWithURL:[NSURL URLWithString:self.viewModel.contactUsModel.weChatService] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.telLab.text = SF(@"联系电话：%@",self.viewModel.contactUsModel.contactTel);
    self.addressLab.text = SF(@"地址：%@",self.viewModel.contactUsModel.address);
    self.emailLab.text = SF(@"邮箱：%@",self.viewModel.contactUsModel.email);
}


- (void)showAlertPicViewWithTelNum:(NSString *)telNum{
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    
    alertVC.contentText= SF(@"呼叫%@",telNum );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [HRCall callPhoneNumber:telNum alert:NO];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark ==================UITableView 数据源和代理 ==================


- (JYContactUsViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYContactUsViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
