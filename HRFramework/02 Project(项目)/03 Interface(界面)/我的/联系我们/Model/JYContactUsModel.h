//
//  JYContactUsModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYContactUsModel : JYBaseModel
//用户客服
@property (nonatomic,copy) NSString *userService;

//商户客服
@property (nonatomic,copy) NSString *merchantService;

//在线客服
@property (nonatomic,copy) NSString *lineService;

//在线客服的环信ID
@property (nonatomic,copy) NSString *accountNumber;

//我微信公众号
@property (nonatomic,copy) NSString *weChatService;

//联系电话
@property (nonatomic,copy) NSString *contactTel;

//邮箱
@property (nonatomic,copy) NSString *email;

//地址
@property (nonatomic,copy) NSString *address;


@end
