//
//  JYMyAdresseViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYMyAdressModel.h"
#import "JYPickCityModel.h"

@interface JYMyAdresseViewModel : JYBaseViewModel

@property (nonatomic, strong) NSMutableArray <JYMyAdressModel*>* myAdressListArray;
@property (nonatomic, strong) NSMutableArray <JYPickCityModel*>* cityListArray;

@property (nonatomic,strong) JYMyAdressModel *addressDetailModel; //地址详情model

//获取地址列表
- (void)requesAdressListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//获取地址详情
- (void)requesAdressDetailWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//添加地址
- (void)requesAddAdressWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//删除地址
- (void)requesDelAdressWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//修改地址
- (void)requesChangeDefaultAdressWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取省市区地址列表
- (void)requesAdressPickViewListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
