//
//  JYMyAdressMainTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYMyAdressModel.h"
@interface JYMyAdressMainTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLB;
@property (weak, nonatomic) IBOutlet UILabel *detailAdressLB;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumLB;
@property (weak, nonatomic) IBOutlet UIButton *normlBt;
@property (weak, nonatomic) IBOutlet UIImageView *seletcedImageTop;
@property (weak, nonatomic) IBOutlet UIImageView *seletcedImageFoot;
@property (weak, nonatomic) IBOutlet UILabel *moRenLB;
@property (strong, nonatomic) JYMyAdressModel *model;
//底部按钮点击block
@property (nonatomic,copy) void(^BottomBtnActionBlock)(NSInteger tag);
@end
