//
//  JYMyAdressMainTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyAdressMainTableViewCell.h"

@implementation JYMyAdressMainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(JYMyAdressModel *)model{
    _model = model;
    _titleLB.text = [_model.addressName placeholder:@"暂无"];
    _phoneNumLB.text = [_model.addressPhone placeholder:@"暂无"];
    _detailAdressLB.text = [_model.addressInfo placeholder:@"暂无"];
    if ([_model.addressDefault isEqualToString:@"1"]) {
        self.seletcedImageTop.hidden = NO;
        self.seletcedImageFoot.hidden = NO;
//        self.normlBt.selected = YES;
        self.moRenLB.text = @"[默认]";
        [self.normlBt setImage:[UIImage imageNamed:@"圆-选中"] forState:UIControlStateNormal];
    }else{
        self.seletcedImageTop.hidden = YES;
        self.seletcedImageFoot.hidden = YES;
//        self.normlBt.selected = NO;
        self.moRenLB.text = @"";
        [self.normlBt setImage:[UIImage imageNamed:@"圆-未选中"] forState:UIControlStateNormal];

    }
    self.normlBt.enabled = [_model.addressDefault intValue] == 1 ? NO : YES;
}

- (IBAction)bottomBtnAction:(UIButton *)sender {
    if (_BottomBtnActionBlock) {
        self.BottomBtnActionBlock(sender.tag);
    }
}


@end
