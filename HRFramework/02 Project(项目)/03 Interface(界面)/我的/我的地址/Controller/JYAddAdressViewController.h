//
//  JYAddAdressViewController.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,JYAddressType) {
    JYAddressType_Add = 1 ,    //添加
    JYAddressType_Edit,   //编辑
};
@interface JYAddAdressViewController : JYBaseViewController
@property (nonatomic,assign) JYAddressType addressType;
@property (nonatomic,copy) NSString *addressId; //用于获取地址详情
@end
