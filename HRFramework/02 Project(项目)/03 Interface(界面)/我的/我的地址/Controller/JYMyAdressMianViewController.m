//
//  JYMyAdressMianViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyAdressMianViewController.h"
#import "JYMyAdressMainTableViewCell.h"
#import "JYAddAdressViewController.h"
#import "JYMyAdresseViewModel.h"
@interface JYMyAdressMianViewController ()<
 UITableViewDelegate,
 UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) JYMyAdresseViewModel *viewModel;
@property (nonatomic, assign) int pageNum;
@property (nonatomic, assign) int pageSize;
@end

@implementation JYMyAdressMianViewController

- (JYMyAdresseViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyAdresseViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    _pageSize = 10;
    _pageNum = 1;
    [super viewDidLoad];
    [self configUI];
    
//    [self refreshBackNormalFooter];
//    [self networkRequest];
//    [self refreshNormalHeader];
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self networkRequest];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"管理收货地址";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyAdressMainTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyAdressMainTableViewCell"];
    
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
//    [dict setObject:[NSString stringWithFormat:@"%d",_pageNum]forKey:@"pageNum"];
//    [dict setObject:[NSString stringWithFormat:@"%d",_pageSize] forKey:@"pageSize"];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [self.viewModel requesAdressListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myTableView reloadData];
//        [weakSelf.myTableView.mj_header endRefreshing];
//        [weakSelf.myTableView.mj_footer endRefreshing];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
//        [weakSelf.myTableView.mj_header endRefreshing];
//        [weakSelf.myTableView.mj_footer endRefreshing];
    }];
}

//删除地址
- (void)requestDelateAddressWithAddressId:(NSString *)addressId{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"addressId"] = addressId;
    [self.viewModel requesDelAdressWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf networkRequest];
        [weakSelf showSuccessTip:msg];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//编辑地址
- (void)requestEditAddressWithAddressId:(NSString *)addressId{
    JYAddAdressViewController * addVC = [[JYAddAdressViewController alloc]init];
    addVC.addressType = JYAddressType_Edit;
    addVC.addressId = addressId;
    [self.navigationController pushViewController:addVC animated:YES];
}

//设为默认地址
- (void)requestSetUpDefaultAddressWithAddressId:(NSString *)addressId{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"addressId"] = addressId;

    [self.viewModel requesChangeDefaultAdressWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf networkRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）
- (IBAction)addAdressBtn:(id)sender {
    JYAddAdressViewController * addVC = [[JYAddAdressViewController alloc]init];
    addVC.addressType = JYAddressType_Add;
    [self.navigationController pushViewController:addVC animated:YES];
}

#pragma mark - ---------- Public Methods（公有方法） ----------
#pragma mark - 下拉刷新
- (void)refreshNormalHeader{
    WEAK(weakSelf)
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 1;
        [weakSelf.viewModel.myAdressListArray removeAllObjects];
        [weakSelf networkRequest];
    }];
}
#pragma mark - 上拉加载
- (void)refreshBackNormalFooter{
    WEAK(weakSelf)
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        ++ _pageNum;
        [weakSelf networkRequest];
    }];
}

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    
    JYMyAdressMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyAdressMainTableViewCell" forIndexPath:indexPath] ;
    JYMyAdressModel * model = self.viewModel.myAdressListArray[indexPath.row];
    cell.model = model;
    cell.BottomBtnActionBlock = ^(NSInteger tag) {
        if (tag == 1000) {
          [weakSelf requestSetUpDefaultAddressWithAddressId:model.addressId];
        }else if (tag == 1001){
          [weakSelf requestEditAddressWithAddressId:model.addressId];
        }else{
            [weakSelf showDeletePicAlertViewControllerSureBtnActionBlock:^{
                [weakSelf requestDelateAddressWithAddressId:model.addressId];
            } closeBtnActionBlock:^{
                
            }];
        }
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 119;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.myAdressListArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     JYMyAdressModel * model = self.viewModel.myAdressListArray[indexPath.row];
    
    if (_currentSelectAddressBlock && self.selectMode ) {
        self.currentSelectAddressBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
