//
//  JYAddAdressViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAddAdressViewController.h"
#import "JYAdressPickView.h"
#import "JYMyAdresseViewModel.h"
#import "JYPickerViewAlertController.h"
#import "JYLocationViewModel.h"
#import "JYProCityCountyModel.h"

@interface JYAddAdressViewController ()<UITextViewDelegate>

@property (nonatomic, strong) JYMyAdresseViewModel * viewModel;
@property (nonatomic,strong) JYLocationViewModel *locationViewModel;
@property (nonatomic,strong) JYPickerViewAlertController *alertVC;
@property (nonatomic,copy) NSString *proCode;
@property (nonatomic,copy) NSString *cityCode;
@property (nonatomic,copy) NSString *countyCode;
@property (nonatomic,copy) NSString *proTitle;
@property (nonatomic,copy) NSString *cityTitle;
@property (nonatomic,copy) NSString *countyTitle;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UILabel *placeLab;

@property (weak, nonatomic) IBOutlet UILabel *placeHolderLab;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (nonatomic,copy) NSString *addressDefault; //是否是默认地址
@property (nonatomic,copy) NSString *placeId; //修改时为必填参数
@property (weak, nonatomic) IBOutlet UIButton *morenBtn;
@property (strong ,nonatomic) NSArray *cityArray;
@end

@implementation JYAddAdressViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpUI{
    WEAKSELF
    self.naviTitle = self.addressType == JYAddressType_Add ? @"添加收货地址" :@"编辑收货地址";

    [self rightItemTitle:@"保存" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:FONT(15) action:^{
        [weakSelf saveAddress];
    }];
    
    self.detailTextView.delegate = self;
    
    self.addressDefault = @"2";
    
    if (self.addressType == JYAddressType_Edit) {
//        请求详细数据并赋值
        [self requestAddressDetail];
    }
    
    
}

#pragma mark ==================网络请求==================

//获取全部省区市列表
- (void)requestProCityCounty{
    WEAKSELF
    [self.locationViewModel requestGetProCityCountySuccess:^(NSString *msg, id responseData) {
        [weakSelf.alertVC updatePickerViewDataArr:weakSelf.locationViewModel.allProCityCountArr type:PickerViewTypeProCity];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
//请求详细数据
- (void)requestAddressDetail{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"addressId"] = self.addressId;
    [self.viewModel requesAdressDetailWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf updataUIAndData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//添加地址请求接口
- (void)saveAddress{
    WEAKSELF
    
    if (!self.userTextField.text.length) {
        [self showSuccessTip:@"请输入姓名"];
        return;
    }else if (!self.phoneTextField.text.length){
        [self showSuccessTip:@"请输入手机号"];
        return;
    }else if (![self.phoneTextField.text checkTelephoneNumber]){
        [self showSuccessTip:@"请输入正确的手机号"];
        return;
    }else if (!self.proCode.length){
        [self showSuccessTip:@"请选择所在区域"];
        return;
    }else if (!self.detailTextView.text.length){
        [self showSuccessTip:@"请输入详细地址"];
        return;
    }else if (self.detailTextView.text.length < 5){
        [self showSuccessTip:@"最少5个字"];
        return;
    }else if (!self.codeTextField.text.length ){
        [self showSuccessTip:@"请输入邮编"];
        return;
    }
    
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(self.addressType);
    dic[@"addressId"] = self.placeId;
    dic[@"addressName"] = self.userTextField.text;
    dic[@"addressPhone"] = self.phoneTextField.text;
    dic[@"provinceCode"] = self.proCode;
    dic[@"cityCode"] = self.cityCode;
    dic[@"countyCode"] = self.countyCode;
    dic[@"addressInfo"] = self.detailTextView.text;
    dic[@"addressZipCode"] = self.codeTextField.text;
    dic[@"addressDefault"] = self.addressDefault;
    
    [self.viewModel requesAddAdressWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (IBAction)checkAdressBtn:(id)sender {
    WEAKSELF
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    self.alertVC = alertVC;
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    alertVC.pickerViewType = PickerViewTypeProCity;
    alertVC.sureBtnProCityTypeActionBlock = ^(NSString *proId, NSString *proTitle, NSString *cityId, NSString *cityTitle, NSString *countyId, NSString *countyTitle) {
        weakSelf.proCode = proId;
        weakSelf.cityCode = cityId;
        weakSelf.countyCode = countyId;
        weakSelf.proTitle = proTitle;
        weakSelf.cityTitle = cityTitle;
        weakSelf.countyTitle = countyTitle;
//        更新UI
        weakSelf.placeLab.text = SF(@"%@ %@ %@",self.proTitle,self.cityTitle,self.countyTitle);
    };
    
    [weakSelf.alertVC updatePickerViewDataArr:weakSelf.cityArray type:PickerViewTypeProCity];
    
//    if (weakSelf.locationViewModel.allProCityCountArr.count) {
//        [weakSelf.alertVC updatePickerViewDataArr:weakSelf.locationViewModel.allProCityCountArr type:PickerViewTypeProCity];
//    }else{
//        [self requestProCityCounty];
//    }


    [self presentViewController:alertVC animated:YES completion:nil];
    
//    暂时注释
//    JYProCityCountyPickerViewController * alertVC = [[JYProCityCountyPickerViewController alloc] init];
//    self.alertVC = alertVC;
//    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [self requestProCityCounty];
//    [self presentViewController:alertVC animated:YES completion:nil];
    
//    JYAdressPickView *addressView = [[JYAdressPickView alloc]initWithDatas:nil AndGetContent:^(NSString *contentStr) {
//        
//    }];
//    addressView.dataArrayNumberStr = @"three";
//    addressView.addressType = AddressTypeToCity;
//    addressView.frame = SCREEN_BOUNDS;
//    addressView.backgroundColor = RGBColor(0, 0, 0, .7);
//    [APP_DELEGATE.window addSubview:addressView];
//    NSArray *regionArr = [CZArchiverOrUnArchiver loadArchiverDataFileName:LOCAL_REGION_IDENTIFIER];
//    
//        [self.viewModel requesAdressPickViewListWithParams:nil success:^(NSString *msg, id responseData) {
//            [addressView setUpDataWithArr:self.viewModel.cityListArray];
//            [CZArchiverOrUnArchiver saveArchiverData:self.viewModel.cityListArray fileName:LOCAL_REGION_IDENTIFIER];
//        } failure:^(NSString *errorMsg) {
//             [self showSuccessTip:errorMsg];
//        }];
//    
//    addressView.threeChosedAddressBlock = ^(NSString *provinceName,NSString *provinceId,NSString *cityName,NSString *cityId,NSString *areaName,NSString *areaId){
////        weakSelf.provinceNameStr = provinceName;
////        weakSelf.provinceIdStr   = provinceId;
////        weakSelf.cityNameStr     = cityName;
////        weakSelf.cityIdStr       = cityId;
////        weakSelf.areaNameStr     = areaName;
////        weakSelf.areaIdStr       = areaId;
////        weakSelf.detailLabel.text = [NSString stringWithFormat:@"%@%@%@",weakSelf.provinceNameStr,weakSelf.cityNameStr,weakSelf.areaNameStr];
//    };
    

}

- (IBAction)morenBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
//    if (sender.selected) {
//        self.addressDefault = @"1"
//    }
    self.addressDefault = sender.isSelected ? @"1" :@"2";
}

#pragma mark ==================textview的代理==================

- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.placeHolderLab.hidden = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSInteger contentLength = textView.text.length;
    if (contentLength > 0) {
        [self.placeHolderLab setHidden:YES];
    }else{
        [self.placeHolderLab setHidden:NO];
    }
}

//- (void)textViewDidChange:(UITextView *)textView{
//    if (textView.text.length > 50) {
//        self.detailTextView.text = [textView.text substringToIndex:50];
//        [self showSuccessTip:@"最多输入50个字"];
//    }
//}


#pragma mark ==================更新UI==================
- (void)updataUIAndData{
    self.userTextField.text = self.viewModel.addressDetailModel.addressName;
    self.phoneTextField.text = self.viewModel.addressDetailModel.addressPhone;
    self.placeLab.text = SF(@"%@ %@ %@",self.viewModel.addressDetailModel.provinceTitle,self.viewModel.addressDetailModel.cityTitle,self.viewModel.addressDetailModel.countyTitle);
    self.codeTextField.text = self.viewModel.addressDetailModel.addressZipCode;
    self.detailTextView.text = self.viewModel.addressDetailModel.addressInfo;
    self.placeHolderLab.hidden = self.viewModel.addressDetailModel.addressInfo.length ? YES : NO;

    
    self.morenBtn.selected = [self.viewModel.addressDetailModel.addressDefault intValue] == 1 ? YES : NO;
    
    self.placeId = self.addressId;
    self.proCode = self.viewModel.addressDetailModel.provinceCode;
    self.cityCode = self.viewModel.addressDetailModel.cityCode;
    self.countyCode = self.viewModel.addressDetailModel.countyCode;
    self.addressDefault = self.viewModel.addressDetailModel.addressDefault;
    
}

- (JYLocationViewModel *)locationViewModel
{
    if (!_locationViewModel) {
        _locationViewModel = [[JYLocationViewModel alloc] init];
    }
    return _locationViewModel;
}

- (JYMyAdresseViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyAdresseViewModel alloc]init];
    }
    return _viewModel;
}

-(NSArray *)cityArray{
    if (!_cityArray) {
        //JSON文件的路径
        NSString *path = [[NSBundle mainBundle] pathForResource:@"china_city_data" ofType:@"json"];
        
        //加载JSON文件
        NSData *data = [NSData dataWithContentsOfFile:path];
        
        //将JSON数据转为NSArray或NSDictionary
        NSDictionary *dictArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        //        mj_objectArrayWithKeyValuesArray
        NSArray * lianxArray = [JYProCityCountyModel mj_objectArrayWithKeyValuesArray:dictArray];
        
        //赋值
        _cityArray = lianxArray;
    }
    return _cityArray;
}


@end
