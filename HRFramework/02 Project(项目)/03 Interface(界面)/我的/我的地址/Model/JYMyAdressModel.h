//
//  JYMyAdressModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYMyAdressModel : JYBaseModel
@property (nonatomic,copy) NSString *addressInfo;
@property (nonatomic,copy) NSString *addressDefault;
@property (nonatomic,copy) NSString *countyTitle;
@property (nonatomic,copy) NSString *addressName;
@property (nonatomic,copy) NSString *provinceCode;
@property (nonatomic,copy) NSString *addressPhone;
@property (nonatomic,copy) NSString *addressId;
@property (nonatomic,copy) NSString *cityCode;
@property (nonatomic,copy) NSString *provinceTitle;
@property (nonatomic,copy) NSString *cityTitle;
@property (nonatomic,copy) NSString *countyCode;
@property (nonatomic,copy) NSString *addressZipCode;
@end
