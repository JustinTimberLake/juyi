//
//  JYMyAdressModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyAdressModel.h"

@implementation JYMyAdressModel
/*
 addressId://地址ID
 addressName://姓名
 addressPhone://手机
 provinceCode://省编码
 provinceTitle://省名称
 cityCode:// 市编码
 cityTitle://市名称
 countyCode://区县编码
 countyTitle://区县名称
 addressInfo:// 详细地址
 addressZipCode:// 邮编
 addressDefault:// 是否为默认地址（1默认2非默认）

 */
@end
