//
//  JYMyAccountViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyAccountViewController.h"
#import "JYHistoryViewController.h"
#import "JYRechargeViewController.h"
#import "JYMyGasolineViewController.h"

@interface JYMyAccountViewController ()
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@end

@implementation JYMyAccountViewController

#pragma 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUi];
    self.priceLab.text = SF(@"￥%@",[User_InfoShared shareUserInfo].personalModel.userRemainder);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_BALANCE_SUCCESS);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_BALANCE_SUCCESS]) {
        self.priceLab.text = noti.object;
    }
}


#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUi{
    WEAKSELF
    self.naviTitle = @"我的账户";
    [self rightItemTitle:@"我的油库" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
      JYMyGasolineViewController * oilVC = [[JYMyGasolineViewController alloc] init];
        [weakSelf.navigationController pushViewController:oilVC animated:YES];
  }];
    
}
#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）
//历史记录
- (IBAction)historyBtn:(id)sender {
    JYHistoryViewController *vc = [[JYHistoryViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}
//充值
- (IBAction)rechargeBtn:(id)sender {
    JYRechargeViewController *vc = [[JYRechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}
#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
