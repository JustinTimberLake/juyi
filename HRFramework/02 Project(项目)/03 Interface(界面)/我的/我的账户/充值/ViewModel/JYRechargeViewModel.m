//
//  JYRechargeViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargeViewModel.h"
#import "JYRechargeMoneyModel.h"

@implementation JYRechargeViewModel

- (void)requesRechargeWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_Rrcharge) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            self.rechargeOrderId = RESULT_DATA[@"orderId"];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取充值金额数据
- (void)requestGetMoneySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_OILDEPOT_GetMoney) params:nil success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
        weakSelf.rechargeMonryArr =  [JYRechargeMoneyModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}


@end
