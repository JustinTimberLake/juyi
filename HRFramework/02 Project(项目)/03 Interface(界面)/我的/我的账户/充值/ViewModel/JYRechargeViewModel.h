//
//  JYRechargeViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYRechargeViewModel : JYBaseViewModel
@property (nonatomic, copy)NSString * rechargeOrderId;

@property (nonatomic,strong) NSArray *rechargeMonryArr;
//充值
- (void)requesRechargeWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

////获取第三方支付方式
//- (void)requesWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//获取充值金额数据
- (void)requestGetMoneySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
