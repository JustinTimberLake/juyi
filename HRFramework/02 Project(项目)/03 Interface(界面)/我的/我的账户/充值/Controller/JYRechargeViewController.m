//
//  JYRechargeViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYRechargeViewController.h"
#import "JYRechargeViewModel.h"
#import "JYPayViewModel.h"
#import "JYRechargeViewModel.h"
#import "JYRechargeMoneyModel.h"
#import <WXApi.h>

typedef NS_ENUM(NSInteger,JYPayStyle) {
    JYPayStyle_WX,//微信支付
    JYPayStyle_Alipay//支付宝
};


static int const count = 3;
static int const margin = 25;
static int const padding = 20;
#define BtnW (SCREEN_WIDTH - margin * 2 - (count - 1)* padding ) / count
#define BtnH 35


@interface JYRechargeViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UITextField *textFD;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *zhiFuBaoBtn;
@property (nonatomic, strong)JYRechargeViewModel * viewModel;
@property (nonatomic, assign)int selectPrice;
@property (nonatomic, assign)NSInteger selectBtnTag;
@property (weak, nonatomic) IBOutlet UILabel *accountMoneyLab;
@property (weak, nonatomic) IBOutlet UILabel *endMoneyLab;
@property (nonatomic,copy) NSString *chongZhiMoneyStr;
@property (nonatomic,assign) JYPayStyle payStyle; //支付方式
@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (weak, nonatomic) IBOutlet UIView *moneyBtnView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moneyViewH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *totalViewH;
@property (nonatomic,strong) NSMutableArray *btnArr;


@end

@implementation JYRechargeViewController

- (JYRechargeViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYRechargeViewModel alloc]init];
    }
    return _viewModel;
}
#pragma 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
//    _selectPrice = 100;
//    _selectBtnTag = 1000;
    [self configUi];
    [self loadData];
    [self addNotification];
    [self requestGetMoney];
}


#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUi{
    self.naviTitle = @"充值";
    [self payBtnAction:self.wechatBtn];
    
    self.textFD.delegate = self;
    [self.textFD addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
  
    
}

- (void)loadData{
    if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > 0) {
        
        self.accountMoneyLab.text = SF(@"￥%@",[User_InfoShared shareUserInfo].personalModel.userRemainder);
    }else{
        self.accountMoneyLab.text = @"￥0.00";
    }
//    初始化数据
//    self.chongZhiMoneyStr = @"100";
    self.payStyle = JYPayStyle_WX;
    [self updataEndMoney];
}

//更新网络数据
- (void)updataData{
    if (self.viewModel.rechargeMonryArr.count) {
        JYRechargeMoneyModel * model = self.viewModel.rechargeMonryArr[0];
        self.chongZhiMoneyStr = model.amount;
    }else{
        self.chongZhiMoneyStr = @"0.00";
    }
}

#pragma mark ==================通知==================
- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
        [self showSuccessTip:noti.object];
    }
}

- (void)updataEndMoney{
    self.endMoneyLab.text = SF(@"%.2f元",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue]+ [self.chongZhiMoneyStr floatValue]);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (self.selectBtnTag) {
        ((UIButton*)[self.moneyBtnView viewWithTag:_selectBtnTag]).selected = NO;
        ((UIButton*)[self.moneyBtnView viewWithTag:_selectBtnTag]).backgroundColor = [UIColor whiteColor];
        self.chongZhiMoneyStr = @"0.00";
        [self updataEndMoney];
    }
}


- (void)textFieldTextChange:(UITextField *)textField{
    self.chongZhiMoneyStr = textField.text;
    if ([textField.text integerValue]) {
        
        [self.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[UIButton class]]) {
                UIButton *btn = obj;
                btn.enabled = NO;
            }
        }];
    }else{
        if (self.selectBtnTag) {
            UIButton * btn = ((UIButton*)[self.moneyBtnView viewWithTag:_selectBtnTag]);
            btn.selected = YES;
            btn.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            NSString * btnText = btn.currentTitle;
            self.chongZhiMoneyStr = [btnText substringToIndex:btnText.length - 1];
            
            [self.moneyBtnView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj isKindOfClass:[UIButton class]]) {
                    UIButton *btn = obj;
                    btn.enabled = YES;
                }
            }];
        }
    }
    [self updataEndMoney];

}
#pragma mark networkRequest (网络请求)

//获取充值金额数据
- (void)requestGetMoney{
    WEAKSELF
    [self.viewModel requestGetMoneySuccess:^(NSString *msg, id responseData) {
//        [weakSelf updataData];
        [weakSelf creatMontyBtn];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark actions （点击事件）
- (IBAction)selectedPrcie:(UIButton*)sender {

    [self.textFD resignFirstResponder];
    self.textFD.text = @"";

    sender.selected = YES;
    if (_selectBtnTag && _selectBtnTag != sender.tag) {
        ((UIButton*)[self.moneyBtnView viewWithTag:_selectBtnTag]).selected = NO;
    }
    _selectBtnTag = sender.tag;
    NSString * btnText = sender.currentTitle;
   self.chongZhiMoneyStr = [btnText substringToIndex:btnText.length - 1];
    
    
    [self.btnArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton * btn = obj;
        if (btn.selected) {
            btn.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
        }else{
            btn.backgroundColor = [UIColor whiteColor];
        }
    }];


    [self updataEndMoney];
}

- (IBAction)payBtnAction:(UIButton *)sender {
    sender.selected = YES;
    if (sender.tag == 1000) {
        self.payStyle = JYPayStyle_WX;
        self.zhiFuBaoBtn.selected = NO;
    }else{
        self.payStyle = JYPayStyle_Alipay;
        self.wechatBtn.selected = NO;
    }
//    self.wechatBtn.selected = !self.zhiFuBaoBtn.selected;
}

#pragma mark IBActions （点击事件xib）
- (IBAction)rachangeBtn:(UIButton*)sender {
    
    if (!self.chongZhiMoneyStr.length) {
        [self showSuccessTip:@"请选择或填写充值金额"];
        return;
    }
    if (self.payStyle = JYPayStyle_WX && ![WXApi isWXAppInstalled]) {
        [self showSuccessTip:@"请安装微信客户端"];
        return;
    }
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    dict[@"money"] = self.chongZhiMoneyStr;
    [self.viewModel requesRechargeWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        if (weakSelf.viewModel.rechargeOrderId.length) {
            dic[@"orderId"] = weakSelf.viewModel.rechargeOrderId;
            dic[@"orderType"] = @"余额充值订单";
        }

        JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
        [weakSelf startToPay];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
       
    }];
}

#pragma mark - ---------- Public Methods（公有方法） ----------
//开始支付
- (void)startToPay{
    if (self.payStyle == JYPayStyle_WX) {
        [self requestWXSignWithOrderNum:self.viewModel.rechargeOrderId];
    }else{
        [self requestAlipaySignWithOrderNum:self.viewModel.rechargeOrderId];
    }
}

- (void)creatMontyBtn{
    if (!self.viewModel.rechargeMonryArr.count) {
        self.moneyViewH.constant = 0;
    }else{
        self.moneyViewH.constant = [self creatBtnandGetHeightWithView:self.moneyBtnView andDataArr:self.viewModel.rechargeMonryArr andIndex:1000];
    }
    self.totalViewH.constant = 60 + 50 + 10 + 60 + self.moneyViewH.constant + 387;
}


- (CGFloat)creatBtnandGetHeightWithView:(UIView *)view andDataArr:(NSArray *)dataArr andIndex:(NSInteger)index{
    int row;
    int line ;
    int h;
    for (int i = 0; i < dataArr.count; i++) {
        row = i % count;
        line = i / count;
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        JYRechargeMoneyModel * moneyModel = dataArr[i];
        [btn setTitle:SF(@"%@元",moneyModel.amount) forState:UIControlStateNormal];
        btn.layer.cornerRadius = 3;
        btn.layer.borderColor = [UIColor colorWithHexString:JYUCOLOR_LINE].CGColor;
        btn.layer.borderWidth = 0.5;
        btn.clipsToBounds = YES;
        btn.tag = index +i;
        btn.frame = CGRectMake(margin + (BtnW + padding) * row,(BtnH + padding / 2) * line, BtnW, BtnH);
//        btn.backgroundColor = RGB(242, 242, 242);
        btn.titleLabel.font = FONT(13);
        [btn setTitleColor:RGB0X(0x333333) forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(selectedPrcie:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        [self.btnArr addObject:btn];
    }
    UIButton * lastBtn = view.subviews.lastObject;
    h = CGRectGetMaxY(lastBtn.frame) + 15;
    return h;
}


//- (NSString *)returnTitleSelectOnlyOneItem:(UIButton *)btn WithArr:(NSArray *)arr {
//    WEAKSELF
//    [arr enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (obj.tag != btn.tag) {
//            [obj setTitleColor:RGB0X(0x666666) forState:UIControlStateNormal];
//            [obj setBackgroundColor:RGB(242, 242, 242)];
//        }else{
//            [obj setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [obj setBackgroundColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN]];
//            NSString * btnText = obj.currentTitle;
//            weakSelf.chongZhiMoneyStr = [btnText substringToIndex:btnText.length - 1];
//        }
//    }];
//    return self.chongZhiMoneyStr;
//}


//- (void)btnAction:(UIButton *)btn{
//    [self.textFD resignFirstResponder];
//    self.textFD.text = @"";
//
//    [self returnTitleSelectOnlyOneItem:btn WithArr:self.btnArr];
//
//    [self updataEndMoney];
//}




#pragma mark ==================支付相关代码==================

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            //            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
                //                switch ([code intValue]) {
                //                    case 9000:
                //                        //                        [self popToRootViewControllerAnimated:NO];
                //                    {
                //                        [weakSelf turnPaySuccessPage];
                //                    }
                //                        break;
                //                    case 8000:
                //                        //                        [self showSuccessTip:@"正在处理中"];
                //                        break;
                //                    case 4000:
                //                        //                        [self showSuccessTip:@"支付失败"];
                //                        break;
                //                    case 6001:
                //                        //                        [self showSuccessTip:@"支付已取消"];
                //                        break;
                //                    case 6002:
                //                        //                        [self showSuccessTip:@"网络连接错误"];
                //                        break;
                //                    default:
                //                        //                        [self showSuccessTip:@"支付失败"];
                //                        break;
                //                }
                
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------


- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}
- (NSMutableArray *)btnArr
{
    if (!_btnArr) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
