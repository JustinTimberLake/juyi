//
//  JYRechargeMoneyModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYRechargeMoneyModel : JYBaseModel
//ID
@property (nonatomic,copy) NSString *moneyId;

//金额
@property (nonatomic,copy) NSString *amount;

@end
