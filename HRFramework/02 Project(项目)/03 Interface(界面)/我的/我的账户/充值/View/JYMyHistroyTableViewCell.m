//
//  JYMyHistroyTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyHistroyTableViewCell.h"

@implementation JYMyHistroyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(JYHistoryModel *)model{
    _model = model;
    _titleLB.text  = _model.accountTitle;
    _contextLB.text = _model.accountContent;
    _dateLB.text =[NSString YYYYMMDDWithDataStr:model.accountTime];

}
@end
