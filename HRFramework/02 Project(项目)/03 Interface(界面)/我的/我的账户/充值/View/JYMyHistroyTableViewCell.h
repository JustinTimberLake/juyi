//
//  JYMyHistroyTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYHistoryModel.h"
@interface JYMyHistroyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLB;
@property (weak, nonatomic) IBOutlet UILabel *dateLB;
@property (weak, nonatomic) IBOutlet UILabel *contextLB;
@property (nonatomic, strong)JYHistoryModel * model;
@end
