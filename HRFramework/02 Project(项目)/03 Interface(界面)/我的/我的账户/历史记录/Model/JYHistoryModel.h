//
//  JYHistoryModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYHistoryModel : JYBaseModel
/*
 accountContent = "\U5185\U5bb9";
 accountId = 1;
 accountTime = 1500001088;
 accountTitle = "\U6536\U652f\U6807\U98981";

 */
@property (nonatomic, copy)NSString *accountContent;
@property (nonatomic, copy)NSString *accountId;
@property (nonatomic, copy)NSString *accountTime;
@property (nonatomic, copy)NSString *accountTitle;
@end
