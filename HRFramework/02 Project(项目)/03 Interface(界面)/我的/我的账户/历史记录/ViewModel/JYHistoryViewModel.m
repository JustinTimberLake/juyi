//
//  JYHistoryViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYHistoryViewModel.h"

@implementation JYHistoryViewModel

- (NSMutableArray<JYHistoryModel*>*)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
//请求历史列表
- (void)requesHistoryListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_PERSONCENTER_MyAccountList));
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyAccountList) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.dataArray addObjectsFromArray: [JYHistoryModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];


}

//删除
- (void)requesDelHistoryWithAccountId:(NSString *)accountId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"accountId"] = accountId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_DelAccount) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//清空记录
- (void)requesRemoveAccountSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_RemoveAccount) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
