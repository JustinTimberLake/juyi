//
//  JYHistoryViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYHistoryModel.h"
@interface JYHistoryViewModel : JYBaseViewModel
@property (nonatomic, strong) NSMutableArray <JYHistoryModel *>* dataArray;

//请求历史列表
- (void)requesHistoryListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除记录
- (void)requesDelHistoryWithAccountId:(NSString *)accountId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//清空记录
- (void)requesRemoveAccountSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
