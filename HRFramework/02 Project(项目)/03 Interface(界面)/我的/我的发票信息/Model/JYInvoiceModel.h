//
//  JYInvoiceModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYInvoiceModel : JYBaseModel
//发票记录ID
@property (nonatomic,copy) NSString *invHisId;
//加油站标题
@property (nonatomic,copy) NSString *shopTitle;
// 加油站图片
@property (nonatomic,copy) NSString *shopImage;
//订单号
@property (nonatomic,copy) NSString *orderId;
//枪号
@property (nonatomic,copy) NSString *gunNum;
//型号
@property (nonatomic,copy) NSString *oilModel;
//单价
@property (nonatomic,copy) NSString *oilPrice;
//油量
@property (nonatomic,copy) NSString *oilNum;
//实付款
@property (nonatomic,copy) NSString *totalPrice;

@end
