//
//  JYGetInvoiceModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYGetInvoiceModel : JYBaseModel
//发票ID
@property (nonatomic,copy) NSString *invoiceId;
// 发票标题
@property (nonatomic,copy) NSString *invoiceTitle;

@end
