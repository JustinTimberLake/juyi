
//
//  JYInvoiceViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYInvoiceViewModel.h"
#import "JYInvoiceModel.h"
#import "JYGetInvoiceModel.h"

@implementation JYInvoiceViewModel
//获取发票记录
- (void)requestGetInvoiceHistoryIsMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    NSString * pageCount;
    if (self.listArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+2 ] : @"1";
    }
    dic[@"pageNum"] = pageCount;
    dic[@"pageSize"] = @(10);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@--%@",JY_PATH(JY_INVOICE_GetInvoiceHistory),dic);
    [manager POST_URL:JY_PATH(JY_INVOICE_GetInvoiceHistory) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.listArr removeAllObjects];
            }
            NSArray * arr = [JYInvoiceModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
    

}

//获取发票列表
- (void)requestGetInvoiceListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    [manager POST_URL:JY_PATH(JY_INVOICE_GetInvoice) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            weakSelf.InvoiceListArr = [JYGetInvoiceModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
//            [weakSelf.InvoiceListArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//创建发票抬头
- (void)requestCreatInvoiceWithUser_type:(NSString *)user_type InvoiceTitle:(NSString *)invoiceTitle TaxNumber:(NSString *)TaxNumber invoice_type:(NSString *)invoice_type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"invoiceTitle"] = invoiceTitle;//抬头
    dic[@"TaxNumber"] = TaxNumber;
    dic[@"user_type"] = user_type;//类型 1个人 2企业
    dic[@"invoice_type"] = invoice_type;//票类型 1普票 2专票
    [manager POST_URL:JY_PATH(JY_INVOICE_CreateInvoice) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//删除发票
- (void)requestDelInvoiceWithInvoiceId:(NSString *)invoiceId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"invoiceId"] = invoiceId;
    
    [manager POST_URL:JY_PATH(JY_INVOICE_DelInvoice) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//删除发票记录
- (void)requestDeleteInvoiceHistoryWithInvHisId:(NSString *)invHisId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{

}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}

- (NSMutableArray *)InvoiceListArr
{
    if (!_InvoiceListArr) {
        _InvoiceListArr = [NSMutableArray array];
    }
    return _InvoiceListArr;
}


@end
