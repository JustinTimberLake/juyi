//
//  JYInvoiceViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYInvoiceViewModel : JYBaseViewModel

//发票记录数据
@property (nonatomic,strong) NSMutableArray *listArr;
//发票列表数据
@property (nonatomic,strong) NSMutableArray *InvoiceListArr;


//获取发票记录
- (void)requestGetInvoiceHistoryIsMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取发票列表
- (void)requestGetInvoiceListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//创建发票抬头
- (void)requestCreatInvoiceWithUser_type:(NSString *)user_type InvoiceTitle:(NSString *)invoiceTitle TaxNumber:(NSString *)TaxNumber invoice_type:(NSString *)invoice_type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除发票
- (void)requestDelInvoiceWithInvoiceId:(NSString *)invoiceId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除发票记录
- (void)requestDeleteInvoiceHistoryWithInvHisId:(NSString *)invHisId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
