//
//  JYInvoiceHistroyViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYInvoiceHistroyViewController.h"
#import "JYServiceOrderTableViewCell.h"
#import "JYInvoiceViewModel.h"
@interface JYInvoiceHistroyViewController ()<
 UITableViewDelegate,
 UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYInvoiceViewModel *viewModel;

@end

@implementation JYInvoiceHistroyViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self setUpSearchRefreshData];
}

//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"发票记录";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceOrderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceOrderTableViewCell"];
    
}
#pragma mark networkRequest (网络请求)
- (void)setUpSearchRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestListWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestListWithIsMore:YES];
    }];
    [self.myTableView.mj_header beginRefreshing];
}

//请求数据
- (void)requestListWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetInvoiceHistoryIsMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}
////删除发票记录
//- (void)requestDelateInvoiceHistoryWithinvHisId:(NSString *)invHisId{
//
//}
//结束刷新
- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYServiceOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceOrderTableViewCell"];
    cell.type = StatusInvoice;
    cell.invoiceModel = self.viewModel.listArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 201;
}
//不确定有没有这个需求
//- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
//    WEAKSELF
//    JYGetInvoiceModel * model = self.viewModel.InvoiceListArr[indexPath.row];
//    
//    UITableViewRowAction * delateAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//        
//        [weakSelf requestDelateInvoiceWithInvoiceId:model.invoiceId];
//    }];
//    return @[delateAction];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

- (JYInvoiceViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYInvoiceViewModel alloc] init];
    }
    return _viewModel;
}


@end
