//
//  JYCreatInvoiceHeaderViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/9/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCreatInvoiceHeaderViewController.h"
#import "JYInvoiceViewModel.h"

@interface JYCreatInvoiceHeaderViewController ()
@property (weak, nonatomic) IBOutlet UIButton *unitBtn;//单位
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numberH;//税号高度


@property (nonatomic,strong) JYInvoiceViewModel *viewModel;
@end

@implementation JYCreatInvoiceHeaderViewController{
    UIButton *_selectBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    _selectBtn = self.unitBtn;
}

- (void)setUpUI{
    self.naviTitle = @"发票抬头";
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)savaInvoiceTitle{
    
    if (!self.nameTextField.text.length) {
        [self showSuccessTip:_selectBtn.tag == 1?@"请输入单位的名称":@"请输入抬头的名称"];
        return;
    }
    if (_selectBtn.tag ==1 &&!self.numberTextField.text.length) {
        [self showSuccessTip:@"请输入纳税人识别号"];
        return;
    }
    WEAKSELF
    _numberTextField.text = _selectBtn.tag == 1?_numberTextField.text:@"";
    [self.viewModel requestCreatInvoiceWithUser_type:_selectBtn.tag == 1?@"2":@"1" InvoiceTitle:_nameTextField.text TaxNumber:_numberTextField.text invoice_type:@"1" success:^(NSString *msg, id responseData) {
        if (_addInvoiceHeaderSuccessBlock) {
            weakSelf.addInvoiceHeaderSuccessBlock(self.nameTextField.text);
        }
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (JYInvoiceViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYInvoiceViewModel alloc] init];
    }
    return _viewModel;
}
- (IBAction)InvoiceType:(UIButton *)sender {
    if (sender.selected == YES) {
        return;
    }
    _selectBtn.selected = NO;
    sender.selected = YES;
    _selectBtn = sender;
    _numberH.constant = sender.tag == 1?56:0;
    _nameTextField.placeholder = sender.tag == 1?@"请输入单位的名称":@"请输入抬头的名称";
    
}

- (IBAction)saveClick:(UIButton *)sender {
    [self savaInvoiceTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
