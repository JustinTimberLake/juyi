//
//  JYMyPointsViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyPointsViewController.h"
#import "JYMyPointsHistroyViewController.h"
#import "JYCommonWebViewController.h"
#import "JYRebateStoreController.h"
#import "JYMyScoreViewModel.h"
#import "JYProductListViewController.h"
@interface JYMyPointsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (nonatomic,strong) JYMyScoreViewModel *scoreViewModel;
@property (weak, nonatomic) IBOutlet UILabel *tipLab;

@end

@implementation JYMyPointsViewController

#pragma 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUi];
    [self requestGetMyPoints];
}



#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUi{
    self.naviTitle = @"我的积分";
    self.tipLab.text = @"我的积分";
    [self rightItemTitle:@"历史记录" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
    JYMyPointsHistroyViewController *vc = [[JYMyPointsHistroyViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
}
#pragma mark networkRequest (网络请求)

- (void)requestGetMyPoints{
    WEAKSELF
    [self.scoreViewModel requestGetMyPointsSuccessBlock:^(NSString *msg, id responseData) {
        NSString * myPoints = responseData;
        [weakSelf updataUIWithMyPoints:myPoints];
    } failureBlock:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
#pragma mark ==================更新UI==================
- (void)updataUIWithMyPoints:(NSString *)points{
    self.priceLab.text = [points intValue] > 0 ? points:@"0";
}

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）
//兑换商品
- (IBAction)exchangeProductBtn:(id)sender {
//    JYRebateStoreController *rebateVC = [[JYRebateStoreController alloc] init];
//    [self.navigationController pushViewController:rebateVC animated:YES];
    
    JYProductListViewController * productVC = [[JYProductListViewController alloc] init];
    productVC.queryType = JYQueryType_Classity;
    productVC.classifyId = @"113";
    productVC.secondaryId = @"116";
    [self.navigationController pushViewController:productVC animated:YES];
}

//积分介绍
- (IBAction)pointDetailBtn:(id)sender {
    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
    webVC.navTitle = @"获得积分技巧";
    webVC.webViewType = JYWebViewType_GetScore;
    [self.navigationController pushViewController:webVC animated:YES];
}
#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

- (JYMyScoreViewModel *)scoreViewModel
{
    if (!_scoreViewModel) {
        _scoreViewModel = [[JYMyScoreViewModel alloc] init];
    }
    return _scoreViewModel;
}


@end
