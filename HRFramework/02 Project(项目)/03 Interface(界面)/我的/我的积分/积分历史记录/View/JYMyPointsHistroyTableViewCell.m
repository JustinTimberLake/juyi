//
//  JYMyPointsHistroyTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyPointsHistroyTableViewCell.h"

@interface JYMyPointsHistroyTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;


@end

@implementation JYMyPointsHistroyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(JYScoreModel *)model{
    _model = model;
    self.titleLab.text = model.scoreTitle;
//    self.timeLab.text = [NSString YYYYMMDDWithTimevalue:model.scoreTime];
    self.timeLab.text = [NSString YYYYMMDDWithDataStr:model.scoreTime];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
