//
//  JYScoreModel.h
//  JY
//
//  Created by risenb on 2017/9/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYScoreModel : JYBaseModel
// 我的积分ID
@property (nonatomic,copy) NSString *scoreId;
//内容
@property (nonatomic,copy) NSString *scoreTitle;
//时间
@property (nonatomic,copy) NSString *scoreTime;
// 1获取记录2使用记录
@property (nonatomic,copy) NSString *scoreType;

@end
