//
//  JYSoceHistoryViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/8/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

typedef NS_ENUM(NSInteger,ScoreType) {
    ScoreType_Get = 1, //获取记录
    ScoreType_Use, //使用记录
    ScoreType_All //全部
};
@interface JYSoceHistoryViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;

//获取我的积分记录
- (void)requestGetScoreListWithType:(ScoreType)type isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除积分记录
- (void)requestDelScoreWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//清空记录
- (void)requestRemoveScoreWithType:(ScoreType)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
