//
//  JYSoceHistoryViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/8/14.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSoceHistoryViewModel.h"
#import "JYScoreModel.h"

@implementation JYSoceHistoryViewModel
//获取我的积分记录
- (void)requestGetScoreListWithType:(ScoreType)type isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(type);
    NSString * pageCount;
    if (self.listArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+2 ] : @"1";
    }
    
    dic[@"pageNum"] = pageCount;
    dic[@"pageSize"] = @(10);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@__%@",JY_PATH(JY_PERSONCENTER_SocreList),dic);
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_SocreList) params:dic success:^(id result) {
        NSLog(@"%@",result);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.listArr removeAllObjects];
            }
            NSArray * arr = [JYScoreModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//删除积分记录
- (void)requestDelScoreWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_DelScore) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//清空记录
- (void)requestRemoveScoreWithType:(ScoreType)type success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"type"] = @(type);
    NSLog(@"%@---%@",JY_PATH(JY_PERSONCENTER_RemoveScore),dic);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_RemoveScore) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock (RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}


@end
