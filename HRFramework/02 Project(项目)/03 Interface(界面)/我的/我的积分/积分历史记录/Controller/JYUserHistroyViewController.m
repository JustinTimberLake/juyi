//
//  JYUserHistroyViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUserHistroyViewController.h"
#import "JYMyPointsHistroyTableViewCell.h"
#import "JYSoceHistoryViewModel.h"

@interface JYUserHistroyViewController ()<
  UITableViewDataSource,
  UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYSoceHistoryViewModel *viewModel;
@property (nonatomic,assign) ScoreType scoreType;

@end

@implementation JYUserHistroyViewController


#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configData];
    [self setUpRefreshData];
    [self addNoti];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshDataWithIsMore:NO];

}

- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
    [self.myTableView.mj_header beginRefreshing];
}


- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}

//请求获取积分接口
- (void)refreshDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetScoreListWithType:self.scoreType isMore:isMore success:^(NSString *msg, id responseData) {
        if (!weakSelf.viewModel.listArr.count) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"scoreType"] = @(self.scoreType);
            JY_POST_NOTIFICATION(JY_SCORE_Remainder, dic);
        }

        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//删除记录接口
- (void)requestDeleteScoreWithScoreId:(NSString *)scoreId{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"scoreId"] = scoreId;
    [self.viewModel requestDelScoreWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf refreshDataWithIsMore:NO];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyPointsHistroyTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyPointsHistroyTableViewCell"];
    
}
- (void)configData{
    self.scoreType = ScoreType_Use;
}

#pragma mark ==================通知==================
- (void)addNoti{
    JY_ADD_NOTIFICATION(JY_MYSCORE_REMOVEALL);
}

- (void)getNotification:(NSNotification *)noti{
    NSDictionary * dic = noti.userInfo;
    NSString *scoreType = dic[@"scoreType"];
    if ([scoreType intValue] == self.scoreType) {
        [self refreshDataWithIsMore:NO];
    }
}


#pragma mark networkRequest (网络请求)


#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyPointsHistroyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyPointsHistroyTableViewCell"];
    cell.model = self.viewModel.listArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}
-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    UITableViewRowAction *rowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        JYScoreModel * model  = weakSelf.viewModel.listArr[indexPath.row];
        [weakSelf requestDeleteScoreWithScoreId:model.scoreId];
    }];
    rowAction.backgroundColor = [UIColor redColor];
    NSArray *arr = @[rowAction];
    return arr;
}

- (JYSoceHistoryViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYSoceHistoryViewModel alloc] init];
    }
    return _viewModel;
}


@end
