//
//  JYMyPointsHistroyViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyPointsHistroyViewController.h"
#import "JYSegmentedControl.h"
#import "JYSoceHistoryViewModel.h"

#import "JYAutoSizeTitleView.h"
#import "JYMyPointsHistroyTableViewCell.h"

@interface JYMyPointsHistroyViewController ()<UITableViewDelegate,UITableViewDataSource>

//@property (strong ,nonatomic)UIView *topBackView;
//segmentControl
//@property (strong ,nonatomic)JYSegmentedControl *segmentControl;
////主页面scorllView
//@property (strong, nonatomic)UIScrollView *mainScrollView;
//
////子控制器数组
//@property (strong, nonatomic)NSArray <NSString *>*controllerArr;
////标题数组
//@property (strong, nonatomic)NSArray <NSString *>*titleArr;

@property (nonatomic,assign) ScoreType scoreType;
//@property (nonatomic,copy) NSString *nowScoreType;
//@property (nonatomic,assign) BOOL remainder; //没有数量了
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) JYSoceHistoryViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (nonatomic,strong) NSArray *titleArr;
@end

@implementation JYMyPointsHistroyViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self creatTitle];
    [self setUpRefreshData];
//    [self addNoti];
}

#pragma mark ==================通知==================
//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self refreshDataWithIsMore:NO];
//}

- (void)creatTitle{
    self.titleArr = @[@"获取记录",@"使用记录"];
    WEAKSELF
    JYAutoSizeTitleView * autoSizeTitleView = [[JYAutoSizeTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
//    self.autoSizeTitleView = autoSizeTitleView;
    [self.titleView addSubview:autoSizeTitleView];
    
    //    需要设置的属性
    autoSizeTitleView.norColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    autoSizeTitleView.selColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isShowUnderLine = YES;
    autoSizeTitleView.underLineH = 2;
    autoSizeTitleView.underLineColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isUnderLineEqualTitleWidth = YES;
    autoSizeTitleView.fontSize = FONT(15);    autoSizeTitleView.didSelectItemsWithIndexBlock = ^(NSInteger index) {
         weakSelf.scoreType = index + 1;
        [weakSelf refreshDataWithIsMore:NO];
    };
    //    [self.titleArr addObject:@"首页"];
    [autoSizeTitleView updataTitleData:self.titleArr];
    
}

#pragma mark ==================网络请求==================

- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
    [self.myTableView.mj_header beginRefreshing];
}


- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}

//请求获取积分接口
- (void)refreshDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetScoreListWithType:self.scoreType isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//清除接口
- (void)requestRemoveScoreList{
    WEAKSELF
    [self.viewModel requestRemoveScoreWithType:self.scoreType success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"清除成功"];
        [weakSelf refreshDataWithIsMore:NO];

    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
//删除记录接口
- (void)requestDeleteScoreWithScoreId:(NSString *)scoreId{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"scoreId"] = scoreId;
    [self.viewModel requestDelScoreWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf refreshDataWithIsMore:NO];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    WEAKSELF
    self.title = @"历史记录";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyPointsHistroyTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyPointsHistroyTableViewCell"];
    [self rightItemTitle:@"清除" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
            [weakSelf showPicAlertViewControllerWithTitle:@"确定清除？" andSureBtnTitle:@"确定" sureBtnActionBlock:^{
                [weakSelf requestRemoveScoreList];
            } closeBtnActionBlock:^{

            }];

    }];

    self.scoreType = ScoreType_Get;
}

#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyPointsHistroyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyPointsHistroyTableViewCell"];
    cell.model = self.viewModel.listArr[indexPath.row];
    cell.selectionStyle = 0;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    UITableViewRowAction *rowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        JYScoreModel * model  = weakSelf.viewModel.listArr[indexPath.row];
        [weakSelf requestDeleteScoreWithScoreId:model.scoreId];
    }];
    rowAction.backgroundColor = [UIColor redColor];
    NSArray *arr = @[rowAction];
    return arr;
}


#pragma mark - ---------- Lazy Loading（懒加载） ----------
-(JYSoceHistoryViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel= [[JYSoceHistoryViewModel alloc]init];
    }
    return _viewModel;
}

@end
