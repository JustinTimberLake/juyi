//
//  JYMineViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/6/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMineViewController.h"
#import "JYMainTableViewCell.h"
#import "JYMyDataViewController.h"
#import "JYSetUpViewController.h"
#import "JYMyAccountViewController.h"//我的账户
#import "JYMyPointsViewController.h"//我的积分
#import "JYMyGasolineViewController.h"//我的油库
#import "JYMyMessgeViewController.h"//我的消息
#import "JYMyPublishViewController.h"//我发布的
#import "JYMyFavoritesViewController.h"//我的收藏
#import "JYMyOrderMianViewController.h"//我的订单
#import "JYMyAdressMianViewController.h"//我的地址
#import "JYMyRedPikeViewController.h"//优惠卷
#import "JYMyInvoiceViewController.h"//发票
#import "JYOpinionViewController.h"
#import "JYGetUserViewModel.h"
#import "JYContactUsViewController.h"

static NSString *const cellIdentifier = @"JYMainTableViewCell";

@interface JYMineViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;//myT
@property (strong, nonatomic) IBOutlet UIView *headview;
@property (weak, nonatomic) IBOutlet UIButton *setUpBtn;
@property (weak, nonatomic) IBOutlet UIView *headImageView;//白色描边用
@property (weak, nonatomic) IBOutlet UIImageView *headImage;//带点击事件
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) JYGetUserViewModel *userInfoViewModel;

@end

@implementation JYMineViewController
#pragma mark ||============<#Name#>============||
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self adaptUI];
    [self addNotification];

 
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    WEAKSELF
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    if ([User_InfoShared shareUserInfo].c.length) {
         [weakSelf requestGetUserInfo];
    }
    
////    [CZArchiverOrUnArchiver saveArchiverData:@"1" fileName:LOGIN_BACK];
//    //判断C串是否存在
//    [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
//        //    获取用户数据请求
//        [weakSelf requestGetUserInfo];
//    }];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_LOGOUT);
    
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_LOGOUT]) {
        [self updataUI];
    }
}

#pragma mark ||============<#Name#>============||
-(void)configUI{
    if (kDevice_Is_iPhoneX) {
        [self.setUpBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.headview.mas_top).offset(50);
        }];
    }
    self.tableview.tableHeaderView = self.headview;
    [self.tableview registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
//    self.headImageView.width = (70 / 275) * SCREEN_WIDTH;
//    self.headImageView.height = (70 / 667) *SCREEN_HEIGHT;
//    
    self.headImageView.layer.masksToBounds = YES;
    self.headImageView.layer.cornerRadius = self.headImageView.width / 2;
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.width / 2;
    
    
    
}

- (void)adaptUI{
    if (@available(iOS 11.0, *)) {
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark ==================网络请求==================

- (void)requestGetUserInfo{
    WEAKSELF
    [self.userInfoViewModel requestGetUserInfoSuccess:^(NSString *msg, id responseData) {
        [weakSelf updataUI];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ==================更新界面==================
- (void)updataUI{
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:[User_InfoShared shareUserInfo].personalModel.userHead] placeholderImage:DEFAULT_HEADER_IMG];
    NSString * nameStr;
    if ([User_InfoShared shareUserInfo].c.length) {
        nameStr = @"修改昵称";
    }else{
        nameStr = @"请登录/注册";
    }
    self.nameLabel.text = [User_InfoShared shareUserInfo].personalModel.userNick.length ?[User_InfoShared shareUserInfo].personalModel.userNick:nameStr ;
}

#pragma mark ||============Action============||
//头像手势
- (IBAction)headImageBtnClick:(id)sender {
    if (![self judgeLogin]) {
        return;
    }
    JYMyDataViewController *vc = [[JYMyDataViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

//设置
- (IBAction)setUpBtnClick:(id)sender {
    JYSetUpViewController *vc = [[JYSetUpViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

//我的账户那些button  tag: 300 - 304
//搭配枚举
- (IBAction)mainBtnClick:(UIButton *)sender {
    if (![self judgeLogin]) {
        return;
    }
    switch (sender.tag) {
        case 300:{
            JYMyAccountViewController *myAccountVc = [[JYMyAccountViewController alloc]init];
            myAccountVc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myAccountVc animated:YES];
            NSLog(@"点击了第%ld个按钮",sender.tag - 300);
        }
            break;
        case 301:{
            JYMyPointsViewController *myPointsVc = [[JYMyPointsViewController alloc]init];
            myPointsVc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myPointsVc animated:YES];
            NSLog(@"点击了第%ld个按钮",sender.tag - 300);
        }
            break;
        case 302:{
            JYMyGasolineViewController *myGasolineVc = [[JYMyGasolineViewController alloc]init];
            myGasolineVc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myGasolineVc animated:YES];
            NSLog(@"点击了第%ld个按钮",sender.tag - 300);
        }
            break;
        case 303:{
            JYMyOrderMianViewController *myOrder = [[JYMyOrderMianViewController alloc]init];
            myOrder.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myOrder animated:YES];
        }
            break;
        case 304:{
            JYContactUsViewController * contactUsVC = [[JYContactUsViewController alloc] init];
            [self.navigationController pushViewController: contactUsVC animated:YES];
        }
            break;
            
    }
}

#pragma mark ||============代理&&数据源============||
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    return [[UIView alloc]init];
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 10.0;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    }
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    JYMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    cell.cellIndex = indexPath;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![self judgeLogin]) {
        return;
    }
    
    switch (indexPath.row) {
        case 0:{
            JYMyMessgeViewController *myMsVC = [[JYMyMessgeViewController alloc]init];
            myMsVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myMsVC animated:YES];
        }
            break;
        
        case 1:{
            JYMyFavoritesViewController *myFavoriteVC = [[JYMyFavoritesViewController alloc]init];
            myFavoriteVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myFavoriteVC animated:YES];
        }
            break;
       
        case 2:{
            JYMyRedPikeViewController *myFavoriteVC = [[JYMyRedPikeViewController alloc]init];
            myFavoriteVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myFavoriteVC animated:YES];
        }
            break;
        case 3:{
            JYMyPublishViewController *myPublishVC = [[JYMyPublishViewController alloc]init];
            myPublishVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myPublishVC animated:YES];
        }
            break;
        case 4:{
            JYMyAdressMianViewController *myFavoriteVC = [[JYMyAdressMianViewController alloc]init];
            myFavoriteVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myFavoriteVC animated:YES];
            }
            break;
       
        case 5:{
            JYMyInvoiceViewController *myFavoriteVC = [[JYMyInvoiceViewController alloc]init];
            myFavoriteVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myFavoriteVC animated:YES];
        }
            break;
            
        case 6:{
            JYOpinionViewController *myFavoriteVC = [[JYOpinionViewController alloc]init];
            myFavoriteVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:myFavoriteVC animated:YES];
        }
            default:
            break;
    }
}

#pragma mark ==================自定义方法==================
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}


- (JYGetUserViewModel *)userInfoViewModel
{
    if (!_userInfoViewModel) {
        _userInfoViewModel = [[JYGetUserViewModel alloc] init];
    }
    return _userInfoViewModel;
}

//设置状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
