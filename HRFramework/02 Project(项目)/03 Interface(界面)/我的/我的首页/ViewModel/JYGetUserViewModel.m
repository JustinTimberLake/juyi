//
//  JYGetUserViewModel.m
//  JY
//
//  Created by risenb on 2017/9/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetUserViewModel.h"

@implementation JYGetUserViewModel
//获取用户信息
- (void)requestGetUserInfoSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    NSLog(@"%@",JY_PATH(JY_LOGION_GetUser));
    [manager POST_URL:JY_PATH(JY_LOGION_GetUser) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [User_InfoShared shareUserInfo].personalModel = [JYPersonInfoModel mj_objectWithKeyValues:RESULT_DATA];
            
            [JY_USERDEFAULTS setObject:[User_InfoShared shareUserInfo].personalModel.userHead forKey:USER_HEADEIMAGE];
            [JY_USERDEFAULTS setObject:[User_InfoShared shareUserInfo].personalModel.userNick forKey:USER_NICKNAME];
            [JY_USERDEFAULTS synchronize];
            

            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
@end
