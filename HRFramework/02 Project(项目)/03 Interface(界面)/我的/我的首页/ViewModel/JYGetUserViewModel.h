//
//  JYGetUserViewModel.h
//  JY
//
//  Created by risenb on 2017/9/15.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYGetUserViewModel : JYBaseViewModel
//获取用户信息
- (void)requestGetUserInfoSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
