//
//  JYMainTableViewCell.h
//  JY
//
//  Created by 王方正 on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYMainTableViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *cellIndex;

@end
