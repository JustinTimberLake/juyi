//
//  JYMainTableViewCell.m
//  JY
//
//  Created by 王方正 on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMainTableViewCell.h"

@interface JYMainTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;

@property (nonatomic, strong) NSArray *imagArr;
@property (nonatomic, strong) NSArray *titleArr;

@end

@implementation JYMainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imagArr = @[@"消息",
                     @"收藏",
                     @"优惠券",
                     @"发布",
                     @"地址",
                     @"发票",
                     @"意见反馈"];
    
    self.titleArr = @[@"我的消息",
                      @"我的收藏",
                      @"我的优惠券",
                      @"我的发布",
                      @"我的地址",
                      @"我的发票信息",
                      @"意见反馈"];
    
//    [(UITableView *)(self.superview.superview) indexPathForCell:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

-(void)setCellIndex:(NSIndexPath *)cellIndex{
    _cellIndex = cellIndex;
    
    [self.cellImage setImage:[UIImage imageNamed:self.imagArr[cellIndex.row]]];
    self.cellTitle.text = self.titleArr[cellIndex.row];
    
}


@end
