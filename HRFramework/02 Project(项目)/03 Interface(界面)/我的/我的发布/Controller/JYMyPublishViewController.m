//
//  JYMyPublishViewController.m
//  JY
//
//  Created by risenb on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyPublishViewController.h"
#import "JYCommonCommentTableViewCell.h"
#import "JYMyPublishViewModel.h"
#import "JYCommunityModel.h"

static NSString * const commentCellId = @"JYCommonCommentTableViewCell";

@interface JYMyPublishViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYMyPublishViewModel *viewModel;

@end

@implementation JYMyPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self requestMyPublish];
}

- (void)configUI{
    self.naviTitle = @"我的发布";
    [self.myTableView registerNib:[UINib nibWithNibName:@"JYCommonCommentTableViewCell" bundle:nil] forCellReuseIdentifier:commentCellId];
}
#pragma mark --------------------网络请求----------------------------
//请求我的发布
- (void)requestMyPublish{
    WEAKSELF
    [self.viewModel requestGetMyBbsListSuccess:^(NSString *msg, id responseData) {
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//删除我的发布
- (void)requestDeleteMyBbsWithBbsId:(NSString *)bbsId{
    WEAKSELF
    [self.viewModel requestDeleteMyBBSWithBBSid:bbsId Success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf requestMyPublish];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark ------------------tableView数据源和代理------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    JYCommonCommentTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:commentCellId];
//    cell.cellMode = CellMode_Bbs;
    JYCommunityModel * model = self.viewModel.listArr[indexPath.row];
    cell.bbsModel = model;
    [cell setUpCellMode:CellMode_Bbs];
    cell.deleteBBSIdBlock = ^{
        
        [weakSelf showDeletePicAlertViewControllerSureBtnActionBlock:^{
            [weakSelf requestDeleteMyBbsWithBbsId:model.bbsId];
        } closeBtnActionBlock:^{
            
        }];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [JYCommonCommentTableViewCell cellHeightWithBBSModel:self.viewModel.listArr[indexPath.row]];
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    tableView.estimatedRowHeight = tableView.rowHeight;
//    tableView.rowHeight = UITableViewAutomaticDimension;
//    return tableView.rowHeight;
//}
//暂时注释
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 300;
////    return [JYCommonCommentTableViewCell cellHeightWithModel:self.getStationInfoViewModel.commentArr[indexPath.row]];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (JYMyPublishViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYMyPublishViewModel alloc] init];
    }
    return _viewModel;
}

@end
