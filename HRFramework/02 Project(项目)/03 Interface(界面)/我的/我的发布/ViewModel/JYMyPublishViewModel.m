//
//  JYMyPublishViewModel.m
//  JY
//
//  Created by risenb on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyPublishViewModel.h"
#import "JYCommunityModel.h"

@implementation JYMyPublishViewModel

//我发布的接口
- (void)requestGetMyBbsListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    NSLog(@"%@",JY_PATH(JY_MYPUBLISH_GetMyBbsList));
    [manager POST_URL:JY_PATH(JY_MYPUBLISH_GetMyBbsList) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [weakSelf.listArr removeAllObjects];
           NSArray * arr = [JYCommunityModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
//删除我的发布
- (void)requestDeleteMyBBSWithBBSid:(NSString *)bbsId Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"bbsId"] = bbsId;
    NSLog(@"%@",JY_PATH(JY_MYPUBLISH_DeleteMyBbs));
    [manager POST_URL:JY_PATH(JY_MYPUBLISH_DeleteMyBbs) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
    
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}


@end
