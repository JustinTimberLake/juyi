//
//  JYMyPublishViewModel.h
//  JY
//
//  Created by risenb on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYMyPublishViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;
//我发布的接口
- (void)requestGetMyBbsListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//删除我的发布
- (void)requestDeleteMyBBSWithBBSid:(NSString *)bbsId Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
