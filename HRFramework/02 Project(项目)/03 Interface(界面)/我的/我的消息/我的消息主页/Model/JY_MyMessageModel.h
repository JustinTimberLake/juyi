//
//  JY_MyMessageModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JY_MyMessageModel : JYBaseModel
/*
 messageId://消息ID
 messageTitle://标题
 messageContent://内容
 messageTime://时间
 messageRead://是否已读(1已读2未读)

 */
@property (copy,nonatomic)NSString *messageId;
@property (copy,nonatomic)NSString *messageTitle;
@property (copy,nonatomic)NSString *messageContent;
@property (copy,nonatomic)NSString *messageTime;
@property (copy,nonatomic)NSString *messageRead;
@property (nonatomic,assign,getter=isSelected) BOOL selected;

//@property (assign,nonatomic)BOOL isSelected;
@end
