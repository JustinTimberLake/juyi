//
//  JYMyMessgeViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyMessgeViewController.h"
#import "JYSegmentedControl.h"
#import "JYMySystemMessageViewController.h"
#import "JYKeFuMessageViewController.h"

@interface JYMyMessgeViewController ()

//segmentControl
@property (strong ,nonatomic)JYSegmentedControl *segmentControl;
//主页面scorllView
@property (strong, nonatomic)UIScrollView *mainScrollView;

//子控制器数组
@property (strong, nonatomic)NSArray <NSString *>*controllerArr;

@end

@implementation JYMyMessgeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeData];
    [self configUI];
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - ---------- 初始化数据 ----------
- (void)initializeData {
    self.controllerArr = @[@"JYMySystemMessageViewController",@"JYKeFuMessageViewController"];
    
}

#pragma mark - ---------- 构建UI ----------
- (void)configUI {
    [self.view addSubview:self.mainScrollView];
    [self configChildControllers];
    [self configScrollView];
    [self creatNavRightButton];
    [self centerItemTitleArray:@[@"系统消息",@"客服咨询"] action:^(NSInteger tag) {
        [UIView animateWithDuration:.25f animations:^{
            self.mainScrollView.contentOffset = CGPointMake((tag -1000) * SCREEN_WIDTH, 0);
        }];
        switch(tag-1000) {
            case 0:{
                JYMySystemMessageViewController *vc = self.childViewControllers[tag-1000];
            }
                break;
                
            case 1:{
                JYKeFuMessageViewController *vc = self.childViewControllers[tag-1000];
            }
                
            default:
                break; 
        }
        if (tag-1000 == 0) {
            [self creatNavRightButton];
        }else{
            self.navigationItem.rightBarButtonItem = nil;
        }
        
    }];
}

- (void)creatNavRightButton{
    [self rightItemTitle:@"编辑" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
        if (((UIButton*)self.navigationItem.rightBarButtonItem.customView).selected) {
            ((UIButton*)self.navigationItem.rightBarButtonItem.customView).selected = NO;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"HiddenAlert" object:nil userInfo:nil];
        }else{
            ((UIButton*)self.navigationItem.rightBarButtonItem.customView).selected = YES;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"pushAlert" object:nil userInfo:nil];
        }
    }];

}
- (void)configChildControllers {
    [self.controllerArr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Class class = NSClassFromString(obj);
        [self addChildViewController:[class new]];
    }];
}

- (void)configScrollView {
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.mainScrollView addSubview:obj.view];
        obj.view.frame = CGRectMake(idx * SCREEN_WIDTH, 0, self.mainScrollView.width, self.mainScrollView.height);
    }];
    self.mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * self.childViewControllers.count, self.mainScrollView.size.height);
    //设置默认controller
    // YSJ_MyFavoriteVideoViewController *vc = self.childViewControllers[0];
}

#pragma mark - ---------- 懒加载 ----------


#pragma mark -mainScrollView-
- (UIScrollView *)mainScrollView {
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc]init];
        _mainScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64);
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.scrollEnabled = NO;
    }
    return _mainScrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
