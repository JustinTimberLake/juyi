//
//  JY_MyMessageViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JY_MyMessageModel.h"
@interface JY_MyMessageViewModel : JYBaseViewModel
@property (nonatomic, strong) NSMutableArray <JY_MyMessageModel*>* mySystemMessage;
@property (nonatomic, strong) NSMutableArray <JY_MyMessageModel*>* myServiceMessage;

@property (nonatomic,strong) JY_MyMessageModel *messageDetailInfo;
//请求系统消息列表
- (void)requesSystMessageIsMore:(BOOL)isMore Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//请求客服消息列表
- (void)requesServiceMessageWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除系统消息
- (void)requesDelSysMessageWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//批量删除系统消息
- (void)requestBatchDeleteWithMessageId:(NSString *)messageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取消息详情
- (void)requestGetMessageInfoWithMessageId:(NSString *)messageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//系统消息标记为已读
- (void)requestBatchReadMessageWithMessageIds:(NSString *)messageIds success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
