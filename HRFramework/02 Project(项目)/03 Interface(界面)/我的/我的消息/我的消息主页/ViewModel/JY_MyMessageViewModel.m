//
//  JY_MyMessageViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JY_MyMessageViewModel.h"

@implementation JY_MyMessageViewModel

- (NSMutableArray<JY_MyMessageModel*>*)myServiceMessage{
    if (!_myServiceMessage) {
        _myServiceMessage = [NSMutableArray array];
    }
    return _myServiceMessage;
}

- (NSMutableArray<JY_MyMessageModel*>*)mySystemMessage{
    if (!_mySystemMessage) {
        _mySystemMessage = [NSMutableArray array];
    }
    return _mySystemMessage;
}
//请求系统消息列表
- (void)requesSystMessageIsMore:(BOOL)isMore Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    NSString * pageCount;
    if (self.mySystemMessage.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.mySystemMessage.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.mySystemMessage.count/10+2 ] : @"1";
    }
    
    dic[@"pageNum"] = pageCount;
    dic[@"pageSize"] = @(10);
    NSLog(@"%@",JY_PATH(JY_PERSONCENTR_MyMessage));
    [manager POST_URL:JY_PATH(JY_PERSONCENTR_MyMessage) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.mySystemMessage removeAllObjects];
            }

            [self.mySystemMessage addObjectsFromArray: [JY_MyMessageModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
        
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//请求客服消息列表
- (void)requesServiceMessageWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTR_MyMessage) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.mySystemMessage addObjectsFromArray: [JY_MyMessageModel mj_objectArrayWithKeyValuesArray:result[RESULT_DATA]]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//删除消息
- (void)requesDelSysMessageWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_PERSONCENTER_DelMySysMessage));
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_DelMySysMessage) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


//批量删除系统消息
- (void)requestBatchDeleteWithMessageId:(NSString *)messageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"messageIds"] = messageId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_BatchDelete) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取消息详情
- (void)requestGetMessageInfoWithMessageId:(NSString *)messageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"messageId"] = messageId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTR_getMessageInfo) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
           weakSelf.messageDetailInfo = [JY_MyMessageModel mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//系统消息标记为已读
- (void)requestBatchReadMessageWithMessageIds:(NSString *)messageIds success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"messageIds"] = messageIds;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_MESSAGE_BatchRead) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

@end
