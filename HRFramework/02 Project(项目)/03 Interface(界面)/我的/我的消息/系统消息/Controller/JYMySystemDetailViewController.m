//
//  JYMySystemDetailViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/12/27.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMySystemDetailViewController.h"
#import "JY_MyMessageViewModel.h"

@interface JYMySystemDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (nonatomic,strong) JY_MyMessageViewModel *viewModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *totalH;

@end

@implementation JYMySystemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self requestMessageInfoWithMessageId:self.messageId];
    
}

- (void)configUI{
    self.naviTitle = @"消息详情";
}

#pragma mark ==================更新UI==================
- (void)updataUI{
    self.titleLab.text = self.viewModel.messageDetailInfo.messageTitle;
    self.timeLab.text = [NSString YYYYMMDDWithDataStr:self.viewModel.messageDetailInfo.messageTime];
    self.detailLab.text = self.viewModel.messageDetailInfo.messageContent;
    
    CGFloat height = [NSString heightOfText:self.viewModel.messageDetailInfo.messageContent textWidth:SCREEN_WIDTH - 2 *10 font:FONT(15)];
    
    self.totalH.constant = 40 + height + 10 * 2;
}

#pragma mark ==================网络请求==================
- (void)requestMessageInfoWithMessageId:(NSString *)messageId{
    WEAKSELF
    [self.viewModel requestGetMessageInfoWithMessageId:messageId success:^(NSString *msg, id responseData) {
        [weakSelf updataUI];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ==================懒加载==================

- (JY_MyMessageViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JY_MyMessageViewModel alloc] init];
    }
    return _viewModel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
