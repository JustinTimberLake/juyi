//
//  JYMySystemMessageViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/13.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMySystemMessageViewController.h"
#import "JYSystemMessageTableViewCell.h"
#import "JY_MyMessageViewModel.h"
#import "JYCommonWebViewController.h"
#import "JYMySystemDetailViewController.h"

@interface JYMySystemMessageViewController ()<
  UITableViewDataSource,
  UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
//@property (nonatomic, assign) int pageNum;
//@property (nonatomic, assign) int pageSize;
@property (nonatomic, strong) JY_MyMessageViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *aleartView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSize;
@property (weak, nonatomic) IBOutlet UIButton *allSelectedBtn;
@property (nonatomic, assign) BOOL isEidt;
@property (nonatomic, assign) BOOL isAllStatus;
@property (nonatomic,strong) NSMutableArray *allSelectArr;
@property (nonatomic,strong) NSMutableArray *currentSelectArr;

@end

@implementation JYMySystemMessageViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JY_MyMessageViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JY_MyMessageViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    [super viewDidLoad];
//    _pageSize = 10;
//    _pageNum = 1;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushAlret) name:@"pushAlert" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hiddenAlret) name:@"HiddenAlert" object:nil];
    [self configUI];
    [self setUpRefreshData];
//    [self refreshBackNormalFooter];
//    [self networkRequest];
//    [self refreshNormalHeader];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshDataWithIsMore:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYSystemMessageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYSystemMessageTableViewCell"];
   
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    [self showHUD];
//    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
//    [dict setObject:[NSString stringWithFormat:@"%d",_pageNum]forKey:@"pageNum"];
//    [dict setObject:[NSString stringWithFormat:@"%d",_pageSize] forKey:@"pageSize"];
//    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];

//    [self.viewModel requesSystMessageWithParams:dict success:^(NSString *msg, id responseData) {
//      [weakSelf hideHUD];
//      [weakSelf.myTableView reloadData];
//      [weakSelf.myTableView.mj_header endRefreshing];
//      [weakSelf.myTableView.mj_footer endRefreshing];
//      
//  } failure:^(NSString *errorMsg) {
//      [weakSelf showSuccessTip:errorMsg];
//      [weakSelf hideHUD];
//      [weakSelf.myTableView.mj_header endRefreshing];
//      [weakSelf.myTableView.mj_footer endRefreshing];
//  }];
}

//请求养车周边店铺列表
- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
    [self.myTableView.mj_header beginRefreshing];
}


- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}

- (void)refreshDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requesSystMessageIsMore:isMore Success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//删除消息
- (void)delMessageRequestWithMessageId:(NSString*)messageId{
    WEAK(weakSelf)
//    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
//    [dict setObject:[NSString stringWithFormat:@"%@",Id]forKey:@"messageId"];
//    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
//    [self.viewModel requesDelSysMessageWithParams:dict success:^(NSString *msg, id responseData) {
//        [weakSelf refreshDataWithIsMore:NO];
//        [weakSelf endRefresh];
//    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
//        [weakSelf hideHUD];
//        [weakSelf endRefresh];
//    }];
    [self.viewModel requestBatchDeleteWithMessageId:messageId success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf refreshDataWithIsMore:NO];
        [weakSelf endRefresh];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf endRefresh];
    }];
}

//标记为已读
- (void)requestBetchReadMessageWithMessageIds:(NSString *)messageIds{
    WEAKSELF
    [self showHUD];
    [self.viewModel requestBatchReadMessageWithMessageIds:messageIds success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf refreshDataWithIsMore:NO];
        [weakSelf endRefresh];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf endRefresh];
    }];
}
#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）
- (IBAction)allSelectBtn:(UIButton*)sender {
//    if (sender.selected ) {
//        sender.selected = NO;
//        _isAllStatus = NO;
//    }else{
//        sender.selected = YES;
//        _isAllStatus = YES;
//    }
//    [self.myTableView reloadData];
    sender.selected = !sender.selected;
    if (sender.selected) {
        for (JY_MyMessageModel * model in self.viewModel.mySystemMessage) {
            model.selected = YES;
        }
    }else{
        for (JY_MyMessageModel * model in self.viewModel.mySystemMessage) {
            model.selected = NO;
        }
    }
    [self.myTableView reloadData];
    
}

- (IBAction)delBtn:(id)sender {
    for (JY_MyMessageModel * model in self.viewModel.mySystemMessage) {
        
        if (model.isSelected) {
            [self.allSelectArr addObject:model.messageId];
        }
    }
    
    if (!self.allSelectArr.count) {
        [self showSuccessTip:@"请选择要删除的消息"];
        return;
    }else{
        NSString * messageId = [self.allSelectArr componentsJoinedByString:@","];
        [self delMessageRequestWithMessageId:messageId];
    }
}

- (IBAction)tagBtn:(id)sender {
    
    for (JY_MyMessageModel * model in self.viewModel.mySystemMessage) {
        
        if (model.isSelected) {
            [self.allSelectArr addObject:model.messageId];
        }
    }
    if (!self.allSelectArr.count) {
        [self showSuccessTip:@"请选择要标记的消息"];
        return;
    }else{
        NSString * messageId = [self.allSelectArr componentsJoinedByString:@","];
        [self requestBetchReadMessageWithMessageIds:messageId];
    }

}
#pragma mark - ---------- Public Methods（公有方法） ----------
//#pragma mark - 下拉刷新
//- (void)refreshNormalHeader{
//    WEAK(weakSelf)
//    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        _pageNum = 1;
//        [weakSelf.viewModel.mySystemMessage removeAllObjects];
//        [weakSelf networkRequest];
//    }];
//}
//#pragma mark - 上拉加载
//- (void)refreshBackNormalFooter{
//    WEAK(weakSelf)
//    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        ++ _pageNum;
//        [weakSelf networkRequest];
//    }];
//}

#pragma mark self declare （本类声明）
- (void)pushAlret{
    _isEidt = YES;
    [UIView animateWithDuration:.25f animations:^{
        self.bottomSize.constant = 0;

    }];
    [self.myTableView reloadData];
}
- (void)hiddenAlret{
    _isEidt = NO;
    _isAllStatus = NO;
    _allSelectedBtn.selected = NO;
    [UIView animateWithDuration:.25f animations:^{
        self.bottomSize.constant = -50;
        
    }];
    [self.myTableView reloadData];

}
#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYSystemMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYSystemMessageTableViewCell" forIndexPath:indexPath] ;
    cell.model = self.viewModel.mySystemMessage[indexPath.row];
    if (_isEidt) {
//        if (_isAllStatus) {
//            cell.isAllSelected = YES;
//        }else{
//            cell.isAllSelected = NO;
//        }
        cell.isEdit = YES;
    }else{
        cell.isEdit = NO;
//        cell.isAllSelected = NO;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.mySystemMessage.count;
}
-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAK(weakSelf);
    UITableViewRowAction *rowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        [weakSelf delMessageRequestWithMessageId:self.viewModel.mySystemMessage[indexPath.row].messageId];
        
    }];
    rowAction.backgroundColor = [UIColor redColor];
    NSArray *arr = @[rowAction];
    return arr;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isEidt) {
        JY_MyMessageModel * model = self.viewModel.mySystemMessage[indexPath.row];
        if (model.isSelected) {
            model.selected = NO;
        }else{
            model.selected = YES;
        }
        [self.myTableView reloadData];
        
        _isAllStatus = NO;
        [self.currentSelectArr removeAllObjects];
        for (JY_MyMessageModel * model in self.viewModel.mySystemMessage) {
            if (model.isSelected) {
                [self.currentSelectArr addObject:model];
            }
        }
        
        if (self.currentSelectArr.count == self.viewModel.mySystemMessage.count) {
            _isAllStatus = YES;
        }
        
        if (_isAllStatus) {
            self.allSelectedBtn.selected = YES;
        }else{
            self.allSelectedBtn.selected = NO;
        }
        
    }else{
        JYMySystemDetailViewController * detailVC = [[JYMySystemDetailViewController alloc] init];
        JY_MyMessageModel * model = self.viewModel.mySystemMessage[indexPath.row];
        detailVC.messageId = model.messageId;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}


- (NSMutableArray *)allSelectArr
{
    if (!_allSelectArr) {
        _allSelectArr = [NSMutableArray array];
    }
    return _allSelectArr;
}

- (NSMutableArray *)currentSelectArr
{
    if (!_currentSelectArr) {
        _currentSelectArr = [NSMutableArray array];
    }
    return _currentSelectArr;
}

@end
