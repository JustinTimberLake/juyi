//
//  JYSystemMessageTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSystemMessageTableViewCell.h"

@implementation JYSystemMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.redTipLab.layer.cornerRadius = self.redTipLab.width / 2;
    self.redTipLab.clipsToBounds = YES;
   
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(JY_MyMessageModel *)model{
    _model = model;
    self.titleLabel.text = model.messageTitle;
    self.contextLabel.text = model.messageContent;
    self.timeLabel.text = model.messageTime;
    self.selectedBtn.selected = model.isSelected ? YES : NO;
    self.redTipLab.hidden = [model.messageRead intValue] == 1 ? YES : NO;
}
- (IBAction)selectedBtn:(UIButton*)sender {
//    if (sender.selected) {
//        sender.selected = NO;
//    }else{
//        sender.selected = YES;
//    }
}

- (void)setIsEdit:(BOOL)isEdit{
    _isEdit = isEdit;
    if (_isEdit) {
        self.leadingView.constant = 0;
    
    }else{
        self.leadingView.constant = -30;
        self.selectedBtn.selected = NO;
    }
   
}
//- (void)setIsAllSelected:(BOOL)isAllSelected{
//    _isAllSelected = isAllSelected;
//    if (_isAllSelected) {
//        self.selectedBtn.selected = YES;
//    }else{
//        self.selectedBtn.selected = NO;
//    }
//}
@end
