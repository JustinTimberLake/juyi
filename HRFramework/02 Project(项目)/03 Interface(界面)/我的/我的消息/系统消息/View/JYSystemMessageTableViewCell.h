//
//  JYSystemMessageTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JY_MyMessageModel.h"
@interface JYSystemMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (strong, nonatomic)JY_MyMessageModel * model;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contextLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingView;
@property (assign, nonatomic) BOOL isEdit;//是否是编辑状态
@property (weak, nonatomic) IBOutlet UILabel *redTipLab;
@property (assign, nonatomic) BOOL isAllSelected;//是否是全选状态
@end
