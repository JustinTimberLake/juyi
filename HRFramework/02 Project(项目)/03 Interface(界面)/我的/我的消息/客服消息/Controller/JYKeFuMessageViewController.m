//
//  JYKeFuMessageViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/12/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYKeFuMessageViewController.h"
#import "IQKeyboardManager.h"
#import "JYChatViewController.h"

@interface JYKeFuMessageViewController ()
@property (nonatomic,strong) NSMutableArray *conversations;
@end

@implementation JYKeFuMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showRefreshHeader = YES;
    [self configUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getConverSation];
   
    //首次进入加载数据
    [self tableViewDidTriggerHeaderRefresh];
}

- (void)configUI{
    [self configKeyBoard];
    [self configConversionUI];
    
    self.dataSource = self;
    self.delegate = self;
}
- (void)getConverSation{
    //    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:HFHuanxinName type:EMConversationTypeChat createIfNotExist:YES];
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    [self.conversations removeAllObjects];
    [self.conversations addObjectsFromArray:conversations];
}



- (void)configKeyBoard {
    IQKeyboardManager *manger = [IQKeyboardManager sharedManager];
    manger.enable = NO;
    //    manger.shouldResignOnTouchOutside = YES;
    //    manger.shouldToolbarUsesTextFieldTintColor = YES;
    manger.enableAutoToolbar = NO;
}

- (void)configConversionUI{
    [EaseConversationCell appearance].titleLabel.text = @"客服";
    
//    [[EaseBaseMessageCell appearance] setAvatarSize:40];
//    [EaseConversationCell appearance].avatarView.image = DEFAULT_HEADER_IMG;
    [EaseConversationCell appearance].avatarView.clipsToBounds = YES;
    [EaseConversationCell appearance].titleLabelColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
//    [EaseConversationCell appearance].titleLabelFont = FONT(15);
    [EaseConversationCell appearance].timeLabelColor = [UIColor colorWithHexString:JYUCOLOR_LINE_HEAD];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EMConversation * sation = self.conversations[indexPath.row];
    //单聊界面
    JYChatViewController *chatController = [[JYChatViewController alloc] initWithConversationChatter:sation.conversationId  conversationType:EMConversationTypeChat];
    chatController.chatTitle = @"客服";
    [self.navigationController pushViewController:chatController animated:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)conversations
{
    if (!_conversations) {
        _conversations = [NSMutableArray array];
    }
    return _conversations;
}

- (id<IConversationModel>)conversationListViewController:(EaseConversationListViewController *)conversationListViewController modelForConversation:(EMConversation *)conversation{
        id<IConversationModel> model = nil;
        model = [[EaseConversationModel alloc] initWithConversation:conversation];
//        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];//默认头像
        //        NSString *url = [NSString ddl_webImageRequestWithUrl:self.userInformation.url];
        model.avatarImage = [UIImage imageNamed:@"logo"];//头像网络地址
        model.title = @"客服";//用户昵称
        return model;

}


///// 重写EaseMessageViewController.h中的方法.
//- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController modelForMessage:(EMMessage *)message
//{
//
//    if (message.direction == EMMessageDirectionSend){ /// 用户发送
//        //用户可以根据自己的用户体系，根据message设置用户昵称和头像
//        id<IMessageModel> model = nil;
//        model = [[EaseMessageModel alloc] initWithMessage:message];
//        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];//默认头像
//        //        NSString *url = [NSString ddl_webImageRequestWithUrl:self.userInformation.url];
//        model.avatarURLPath = [User_InfoShared shareUserInfo].personalModel.userHead;//头像网络地址
//        model.nickname = [User_InfoShared shareUserInfo].personalModel.userNick;//用户昵称
//        return model;
//    } else {
//        //用户可以根据自己的用户体系，根据message设置用户昵称和头像
//        id<IMessageModel> model = nil;
//        model = [[EaseMessageModel alloc] initWithMessage:message];
//        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];//默认头像
//        //        NSString *url = [NSString ddl_webImageRequestWithUrl:self.friend_url];
//        //        model.avatarURLPath = url;//头像网络地址
//        model.nickname = @"客服";//用户昵称
//        return model;
//    }
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
