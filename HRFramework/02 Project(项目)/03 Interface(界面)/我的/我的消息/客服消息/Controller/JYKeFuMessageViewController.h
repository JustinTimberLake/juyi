//
//  JYKeFuMessageViewController.h
//  JY
//
//  Created by Duanhuifen on 2017/12/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"
#import "EaseConversationListViewController.h"

@interface JYKeFuMessageViewController : EaseConversationListViewController<EaseConversationListViewControllerDelegate,EaseConversationListViewControllerDataSource>

@end
