//
//  JY_MyFavoriteViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JY_MyFavoriteViewModel.h"

@implementation JY_MyFavoriteViewModel
- (NSMutableArray <JYShopListModel*>*)myLoveStationArray{
    if (!_myLoveStationArray) {
        _myLoveStationArray = [NSMutableArray array];
    }
    return _myLoveStationArray;
}
- (NSMutableArray <JYMyProductModel*>*)myProducutArray{
    if (!_myProducutArray) {
        _myProducutArray = [NSMutableArray array];
    }
    return _myProducutArray;
}
- (NSMutableArray <JYShopListModel*>*)myLoveSellerArray{
    if (!_myLoveSellerArray) {
        _myLoveSellerArray = [NSMutableArray array];
    }
    return _myLoveSellerArray;
}
//- (NSMutableArray <JYMyLoveMvModel*>*)myLoveMvArray{
//    if (!_myLoveMvArray) {
//        _myLoveMvArray = [NSMutableArray array];
//    }
//    return _myLoveMvArray;
//}

- (NSMutableArray <CLModel*>*)myLoveMvArray{
    if (!_myLoveMvArray) {
        _myLoveMvArray = [NSMutableArray array];
    }
    return _myLoveMvArray;
}

- (void)requesMyLoveStationListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyFavoriteStation) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.myLoveStationArray addObjectsFromArray: [JYShopListModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (void)requesMyLoveProductListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    NSLog(@"🌺%@--%@",JY_PATH(JY_PERSONCENTER_MyFavoriteProduct),params);
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyFavoriteProduct) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.myProducutArray addObjectsFromArray: [JYMyProductModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (void)requesMyLoveSellerListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyFavoriteSeller) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.myLoveSellerArray addObjectsFromArray: [JYShopListModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

- (void)requesMyLoveMvListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyFavoriteMv) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.myLoveMvArray addObjectsFromArray: [CLModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}
@end



