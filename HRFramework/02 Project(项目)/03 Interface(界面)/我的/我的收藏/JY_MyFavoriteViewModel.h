//
//  JY_MyFavoriteViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYShopListModel.h"
#import "JYMyProductModel.h"
#import "JYMyLoveMvModel.h"
#import "CLModel.h"

@interface JY_MyFavoriteViewModel : JYBaseViewModel
@property (nonatomic, strong)NSMutableArray <JYShopListModel*>* myLoveStationArray;//油站
@property (nonatomic, strong)NSMutableArray <JYMyProductModel*>* myProducutArray;//商品
@property (nonatomic, strong)NSMutableArray <JYShopListModel*>* myLoveSellerArray;//商家
@property (nonatomic, strong)NSMutableArray <CLModel*>* myLoveMvArray;//视频
//油站
- (void)requesMyLoveStationListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//商品
- (void)requesMyLoveProductListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//商家
- (void)requesMyLoveSellerListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//视频
- (void)requesMyLoveMvListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end


