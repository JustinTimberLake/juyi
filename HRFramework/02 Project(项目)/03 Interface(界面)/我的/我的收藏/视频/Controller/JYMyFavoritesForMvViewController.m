//
//  JYMyFavoritesForMvViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyFavoritesForMvViewController.h"
#import "JYMyFavoritesMvTableViewCell.h"
#import "JY_MyFavoriteViewModel.h"
#import "JYCollectionViewModel.h"
#import "CLPlayerView.h"
#import "CLTableViewCell.h"

@interface JYMyFavoritesForMvViewController ()
<
UITableViewDataSource,
UITableViewDelegate,
CLTableViewCellDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, assign) int pageNum;
@property (nonatomic, assign) int pageSize;
@property (nonatomic, strong) JY_MyFavoriteViewModel *viewModel;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;
//@property (nonatomic,assign) JY_CollectionType collectionType;
/**CLplayer*/
@property (nonatomic, weak) CLPlayerView *playerView;
/**记录Cell*/
@property (nonatomic, assign) UITableViewCell *cell;
@end


static NSString *CLTableViewCellIdentifier = @"CLTableViewCellIdentifier";

@implementation JYMyFavoritesForMvViewController

#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    _pageSize = 10;
    _pageNum = 1;
//    self.collectionType = JY_CollectionType_Video;
    [super viewDidLoad];
    [self configUI];
    [self refreshBackNormalFooter];
    [self networkRequest];
    [self refreshNormalHeader];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self refreshNormalHeader];
    [self.myTableView.mj_header beginRefreshing];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
//    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyFavoritesMvTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyFavoritesMvTableViewCell"];
     [self.myTableView registerClass:[CLTableViewCell class] forCellReuseIdentifier:CLTableViewCellIdentifier];
    
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%d",_pageNum]forKey:@"pageNum"];
    [dict setObject:[NSString stringWithFormat:@"%d",_pageSize] forKey:@"pageSize"];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [self.viewModel requesMyLoveMvListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myTableView reloadData];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
    }];
}

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------
#pragma mark - 下拉刷新
- (void)refreshNormalHeader{
    WEAK(weakSelf)
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 1;
        [weakSelf.viewModel.myLoveMvArray removeAllObjects];
        [weakSelf networkRequest];
    }];
}
#pragma mark - 上拉加载
- (void)refreshBackNormalFooter{
    WEAK(weakSelf)
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        ++ _pageNum;
        [weakSelf networkRequest];
    }];
}

- (void)requestCollectVideoWithVideoId:(NSString *)videoId andOperat:(NSString *)operat andIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"videoId"] = videoId;
    dic[@"operat"] = operat;
    [self.collectionViewModel requestCollectionWithParams:dic andType:JY_CollectionType_Video success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf.myTableView.mj_header beginRefreshing];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}
#pragma mark - ---------- Protocol Methods（代理方法） ----------
//- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    WEAKSELF
//    JYMyFavoritesMvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyFavoritesMvTableViewCell" forIndexPath:indexPath] ;
//    JYMyLoveMvModel * model =  self.viewModel.myLoveMvArray[indexPath.row];
//    cell.model = model;
//    cell.VideoCollectionBlock = ^(NSString *isCollect) {
//        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect];
//    };
//   return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 180;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.myLoveMvArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WEAKSELF
    //    JYMyFavoritesMvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    JYMyLoveMvModel * model = self.viewModel.videoListArr[indexPath.row];
    //    cell.model = model;
    //    cell.VideoCollectionBlock = ^(NSString *isCollect) {
    //        if (![self judgeLogin]) {
    //            return ;
    //        }
    //        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
    //    };
    //    [cell showBottomLine:NO];
    //    return cell;
    
    CLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLTableViewCellIdentifier forIndexPath:indexPath];
    cell.indexPath = indexPath;
    CLModel *model = self.viewModel.myLoveMvArray[indexPath.row];
    cell.collectionBtnClickBlock = ^(NSString *isCollect){
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180;
}
//在willDisplayCell里面处理数据能优化tableview的滑动流畅性，cell将要出现的时候调用
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    CLTableViewCell * myCell = (CLTableViewCell *)cell;
    myCell.model = self.viewModel.myLoveMvArray[indexPath.row];
    //Cell开始出现的时候修正偏移量，让图片可以全部显示
    [myCell cellOffset];
    //第一次加载动画
    
    [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:myCell.model.pictureUrl] completion:^(BOOL isInCache) {
        if (!isInCache) {
            //主线程
            dispatch_async(dispatch_get_main_queue(), ^{
                CATransform3D rotation;//3D旋转
                rotation = CATransform3DMakeTranslation(0 ,50 ,20);
                //逆时针旋转
                rotation = CATransform3DScale(rotation, 0.8, 0.9, 1);
                rotation.m34 = 1.0/ -600;
                myCell.layer.shadowColor = [[UIColor blackColor]CGColor];
                myCell.layer.shadowOffset = CGSizeMake(10, 10);
                myCell.alpha = 0;
                myCell.layer.transform = rotation;
                [UIView beginAnimations:@"rotation" context:NULL];
                //旋转时间
                [UIView setAnimationDuration:0.6];
                myCell.layer.transform = CATransform3DIdentity;
                myCell.alpha = 1;
                myCell.layer.shadowOffset = CGSizeMake(0, 0);
                [UIView commitAnimations];
            });
        }
    }];
}
//cell离开tableView时调用
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //因为复用，同一个cell可能会走多次
    if ([_cell isEqual:cell]) {
        //区分是否是播放器所在cell,销毁时将指针置空
        [_playerView destroyPlayer];
        _cell = nil;
    }
}
#pragma mark - 点击播放代理
- (void)cl_tableViewCellPlayVideoWithCell:(CLTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF;
    //记录被点击的Cell
    _cell = cell;
    //销毁播放器
    [_playerView destroyPlayer];
    CLPlayerView *playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake(0, 0, cell.width, cell.height)];
    _playerView = playerView;
    [cell.contentView addSubview:_playerView];
    //    //重复播放，默认不播放
    //    _playerView.repeatPlay = YES;
    //    //当前控制器是否支持旋转，当前页面支持旋转的时候需要设置，告知播放器
    //    _playerView.isLandscape = YES;
    //    //设置等比例全屏拉伸，多余部分会被剪切
    //    _playerView.fillMode = ResizeAspectFill;
    //    //设置进度条背景颜色
    //    _playerView.progressBackgroundColor = [UIColor purpleColor];
    //    //设置进度条缓冲颜色
    //    _playerView.progressBufferColor = [UIColor redColor];
    //    //设置进度条播放完成颜色
    //    _playerView.progressPlayFinishColor = [UIColor greenColor];
    //    //全屏是否隐藏状态栏
    //    _playerView.fullStatusBarHidden = NO;
    //    //转子颜色
    //    _playerView.strokeColor = [UIColor redColor];
    //视频地址
    _playerView.url = [NSURL URLWithString:cell.model.videoUrl];
    _playerView.title = cell.model.videoTitle;
    _playerView.isCollect = cell.model.isCollect;
    //播放
    [_playerView playVideo];
    //返回按钮点击事件回调
    [_playerView backButton:^(UIButton *button) {
        NSLog(@"返回按钮被点击");
    }];
    //播放完成回调
    [_playerView endPlay:^{
        //销毁播放器
        [_playerView destroyPlayer];
        _playerView = nil;
        _cell = nil;
        NSLog(@"播放完成");
    }];
    
    _playerView.collectionBtnClickBlock = ^(NSString *isCollect) {
        if (![self judgeLogin]) {
            return ;
        }
        [weakSelf requestCollectVideoWithVideoId:cell.model.videoId andOperat:isCollect andIndexPath:indexPath];
    };
}
//#pragma mark - 滑动代理
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    // visibleCells 获取界面上能显示出来了cell
//    NSArray<CLTableViewCell *> *array = [self.tableView visibleCells];
//    //enumerateObjectsUsingBlock 类似于for，但是比for更快
//    [array enumerateObjectsUsingBlock:^(CLTableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [obj cellOffset];
//    }];
//}


#pragma mark ==================自定义方法==================
//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

#pragma mark ==================懒加载==================

- (JYCollectionViewModel *)collectionViewModel
{
    if (!_collectionViewModel) {
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}

- (JY_MyFavoriteViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JY_MyFavoriteViewModel alloc]init];
    }
    return _viewModel;
}


@end
