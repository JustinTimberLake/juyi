//
//  JYMyFavoritesMvTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyFavoritesMvTableViewCell.h"
#import "RHPlayerView.h"
#import "RHVideoModel.h"
#import "JYBaseTabBarController.h"
#import "JYBaseNatigationViewController.h"

@interface JYMyFavoritesMvTableViewCell () <RHPlayerViewDelegate>
@property (nonatomic, strong)RHPlayerView * player;
@property (nonatomic,copy) NSString *isCollect; //收藏
@end

@interface JYMyFavoritesMvTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;


@end
@implementation JYMyFavoritesMvTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configUi];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configUi{
    WEAKSELF
    [self.slider setThumbImage:[UIImage imageNamed:@"视频播放-圆"] forState:UIControlStateNormal];
    self.bottomLine.hidden = YES;

//    默认隐藏
    self.playTime.hidden = YES;
    self.progress.hidden = YES;
    self.slider.hidden = YES;
    self.allScreenBtn.hidden = YES;
    self.totalTime.hidden = YES;
    self.titleTimeLabel.hidden = NO;
    self.timerImage.hidden = NO;
    
//    JYBaseTabBarController *tabVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//    JYBaseNatigationViewController * navVC =tabVC.selectedViewController;
//
    _player = [[RHPlayerView alloc] initWithFrame:CGRectMake(0, 0,0,0) currentVC:[self viewController]];

    _player.delegate = self;
    [self addSubview:_player];
    [_player mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(@5);
        make.left.mas_equalTo(@10);
        make.right.mas_equalTo(@-10);
        make.bottom.mas_equalTo(@-5);
    }];
    
    _player.VideoCollectionBlock = ^(NSString *isCollect) {
        if (weakSelf.VideoCollectionBlock) {
            weakSelf.VideoCollectionBlock(isCollect);
        }
    };
}

- (void)setModel:(JYMyLoveMvModel *)model{
    _model = model;
    _titleLabel.text = [_model.videoTitle placeholder:@"暂无标题"];
    _totalTime.text = [_model.videoTime placeholder:@"暂无"];

    RHVideoModel * videoModel = [[RHVideoModel alloc]initWithVideoId:_model.videoId title:_model.videoTitle url:_model.videoUrl currentTime:[_model.videoTime floatValue]];
    videoModel.isCollect = _model.isCollect;
    [_player setCoverImage:_model.videoImage];
    [_player setVideoModels:[NSMutableArray arrayWithObject:videoModel] playVideoId:@""];
 

    [self.videoImageView sd_setImageWithURL:[NSURL URLWithString:model.videoImage] placeholderImage:[UIImage imageNamed:@"视频图片-暗影"]];
    self.isLovedBtn.selected = ([model.isCollect intValue] == 1) ? YES :NO;

}

- (void)showBottomLine:(BOOL)isShow{
    self.bottomLine.hidden = isShow ? NO:YES;
}

//收藏
- (IBAction)isLovedBtn:(UIButton *)sender {
//    if (sender.selected) {
//        sender.selected = NO;
//        self.isCollect = @"2";
//    }else{
//        sender.selected = YES;
//        self.isCollect = @"1";
//    }
//
//    if (_VideoCollectionBlock) {
//        self.VideoCollectionBlock(self.isCollect);
//    }
}

//全屏
- (IBAction)allAcreenBtn:(UIButton *)sender {
//    sender.selected = !sender.selected;
//    NSLog(@"点击了全屏");
////    self.player
//    JY_POST_NOTIFICATION(JY_VIDEO_FULLSCREEN, @(sender.selected));
}

//播放
- (IBAction)playAndStopBtn:(UIButton *)sender {
    if (!sender.selected) {
        sender.selected = YES;
        self.playTime.hidden = NO;
        self.progress.hidden = NO;
        self.slider.hidden = NO;
        self.allScreenBtn.hidden = NO;
        self.totalTime.hidden = NO;
        self.titleTimeLabel.hidden = YES;
        self.timerImage.hidden = YES;
    }else{
        sender.selected = NO;
        self.playTime.hidden = YES;
        self.progress.hidden = YES;
        self.slider.hidden = YES;
        self.allScreenBtn.hidden = YES;
        self.totalTime.hidden = YES;
        self.titleTimeLabel.hidden = NO;
        self.timerImage.hidden = NO;
    }
    
}
//获取控制器
- (UIViewController *)viewController
{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}


// 是否允许播放
- (BOOL)playerViewShouldPlay {
    
    return YES;
}
// 当前播放的
- (void)playerView:(RHPlayerView *)playView didPlayVideo:(RHVideoModel *)videoModel index:(NSInteger)index {
    
    
}
// 当前播放结束的
- (void)playerView:(RHPlayerView *)playView didPlayEndVideo:(RHVideoModel *)videoModel index:(NSInteger)index {
    
    
}
// 当前正在播放的  会调用多次  更新当前播放时间
- (void)playerView:(RHPlayerView *)playView didPlayVideo:(RHVideoModel *)videoModel playTime:(NSTimeInterval)playTime {
    
    
}

@end
