//
//  JYMyFavoritesMvTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYMyLoveMvModel.h"
@interface JYMyFavoritesMvTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *isLovedBtn;//收藏按钮
@property (weak, nonatomic) IBOutlet UIProgressView *progress;//缓冲条
@property (weak, nonatomic) IBOutlet UIButton *playBtn;//播放按钮
@property (weak, nonatomic) IBOutlet UILabel *playTime;//播放时间点
@property (weak, nonatomic) IBOutlet UILabel *totalTime;//总时间
@property (weak, nonatomic) IBOutlet UIButton *allScreenBtn;//全屏按钮
@property (weak, nonatomic) IBOutlet UISlider *slider;//滑块
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//标题
@property (weak, nonatomic) IBOutlet UILabel *titleTimeLabel;//标题时间
@property (weak, nonatomic) IBOutlet UIImageView *timerImage;//时间图标
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (strong, nonatomic) JYMyLoveMvModel * model;
//视频收藏BLOCK
@property (nonatomic,copy) void (^VideoCollectionBlock)(NSString * isCollect);
//控制底部线
- (void)showBottomLine:(BOOL)isShow;

@end
