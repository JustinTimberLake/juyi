//
//  JYMyLoveMvModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYMyLoveMvModel : JYBaseModel
/*
 isCollect = 1;
 videoId = 2;
 videoImage = "http://img1.gtimg.com/20/2009/200957/20095709_1200x1000_0.jpg";
 videoTime = 100;
 videoTitle = "\U6807\U98982";
 videoUrl = "http://182.92.160.32:8081/tianping.mp4";

 */
@property (nonatomic, copy)NSString *isCollect;
@property (nonatomic, copy)NSString *videoId;
@property (nonatomic,copy) NSString *videoImage;
@property (nonatomic, copy)NSString *videoTime;
@property (nonatomic, copy)NSString *videoTitle;
@property (nonatomic, copy)NSString *videoUrl;

@end
