//
//  JY_MyFavoriteStationModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JY_MyFavoriteStationModel : JYBaseModel
/*
 hxUserId = 123;
 isCollect = 1;
 shopAddress = "\U5e97\U94fa\U5730\U5740";
 shopEvaluate = 5;
 shopId = 1;
 shopImage = "http://img27.hc360.cn/27/busin/177/317/b/27-177317277.jpg";
 shopLat = "116.361725";
 shopLong = "39.928879";
 shopTel = 7856745;
 shopTitle = "\U5e97\U94fa\U6807\U98981";

 */
@property (nonatomic, copy)NSString *isCollect;
@property (nonatomic, copy)NSString *shopAddress;
@property (nonatomic, copy)NSString *shopEvaluate;
@property (nonatomic, copy)NSString *shopId;
@property (nonatomic, copy)NSString *shopImage;
@property (nonatomic, copy)NSString *shopLat;
@property (nonatomic, copy)NSString *shopLong;
@property (nonatomic, copy)NSString *shopTel;
@property (nonatomic, copy)NSString *shopTitle;
@end
