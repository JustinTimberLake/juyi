//
//  JYMyFavoritesForStationViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyFavoritesForStationViewController.h"
#import "JYStoreListTableViewCell.h"
#import "JY_MyFavoriteViewModel.h"
#import "JYDetailViewController.h"
#import "JYCollectionViewModel.h"

@interface JYMyFavoritesForStationViewController ()
<
UITableViewDataSource,
UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, strong) JY_MyFavoriteViewModel *viewModel;
@property (nonatomic,strong) JYCollectionViewModel *collectionViewModel;
@end

@implementation JYMyFavoritesForStationViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JY_MyFavoriteViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JY_MyFavoriteViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    _pageSize = 10;
    _pageNum = 1;

    [self configUI];
    [self refreshBackNormalFooter];
    [self networkRequest];
    [self refreshNormalHeader];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.myTableView.mj_header beginRefreshing];
//    [self refreshNormalHeader];
//    [self networkRequest];
//    [self reloadRequestWithArr:self.viewModel.myLoveStationArray];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYStoreListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYStoreListTableViewCell"];
    
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)_pageNum]forKey:@"pageNum"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)_pageSize] forKey:@"pageSize"];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    if (_pageNum == 1) {
        [self.viewModel.myLoveStationArray removeAllObjects];
    }
    [self.viewModel requesMyLoveStationListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myTableView reloadData];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
    }];
}


//- (void)reloadRequestWithArr:(NSMutableArray *)arr{
//    NSInteger pageCount;
//    if (arr.count % 10 == 0) {
//        pageCount = arr.count/10+1;
//    } else {
//        pageCount = arr.count/10+2;
//    }
//    _pageNum = pageCount;
//    _pageSize = 10;
//
//    [self networkRequest];
//}

//取消收藏
- (void)requestCollectionOperationWithShopId:(NSString *)shopId{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"shopId"] = shopId;
    dic[@"operat"] = @"2";
    [self.collectionViewModel requestCollectionWithParams:dic andType:JY_CollectionType_OilStation success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:@"操作成功"];
        [weakSelf networkRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------
#pragma mark - 下拉刷新
- (void)refreshNormalHeader{
    WEAK(weakSelf)
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 1;
        [weakSelf.viewModel.myLoveStationArray removeAllObjects];
        [weakSelf networkRequest];
    }];
}
#pragma mark - 上拉加载
- (void)refreshBackNormalFooter{
    WEAK(weakSelf)
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        ++ _pageNum;
        [weakSelf networkRequest];
    }];
}

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYStoreListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYStoreListTableViewCell" forIndexPath:indexPath] ;
    cell.gasStation = YES;
    JYShopListModel *listModel = self.viewModel.myLoveStationArray[indexPath.row];
    cell.listModel = listModel;
    
    WEAKSELF;
    cell.navBlock = ^(void){
        if (![weakSelf judgeLogin]) {
            return ;
        }
            //导航
            [JYStoreListTableViewCell gotoMap:listModel andViewController:weakSelf];
       
    };
    return cell;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
     JYShopListModel * model = self.viewModel.myLoveStationArray[indexPath.row];
    
    UITableViewRowAction * delateAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"取消收藏" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        [weakSelf requestCollectionOperationWithShopId:model.shopId];
    }];
    return @[delateAction];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.myLoveStationArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JYShopListModel * model = self.viewModel.myLoveStationArray[indexPath.row];
    
    JYDetailViewController *detailVC = [[JYDetailViewController alloc] init];
    detailVC.detailType = DetailTypeAddOil;
    detailVC.shopId = model.shopId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

//判断登录
- (BOOL)judgeLogin{
    return  [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
    }];
}

- (JYCollectionViewModel *)collectionViewModel{
    if(!_collectionViewModel){
        _collectionViewModel = [[JYCollectionViewModel alloc] init];
    }
    return _collectionViewModel;
}


@end
