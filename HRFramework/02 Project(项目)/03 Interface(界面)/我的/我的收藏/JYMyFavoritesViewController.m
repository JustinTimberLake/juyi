//
//  JYMyFavoritesViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyFavoritesViewController.h"
#import "JYSegmentedControl.h"
#import "JYMyFavoritesForMvViewController.h"
#import "JYMyFavoritesForSellerViewController.h"
#import "JYMyFavoritesForProductViewController.h"
#import "JYMyFavoritesForStationViewController.h"
@interface JYMyFavoritesViewController ()
@property (strong ,nonatomic)UIView *topBackView;
//segmentControl
@property (strong ,nonatomic)JYSegmentedControl *segmentControl;
//主页面scorllView
@property (strong, nonatomic)UIScrollView *mainScrollView;

//子控制器数组
@property (strong, nonatomic)NSArray <NSString *>*controllerArr;
//标题数组
@property (strong, nonatomic)NSArray <NSString *>*titleArr;
@end

@implementation JYMyFavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的收藏";
    [self initializeData];
    [self configUI];
}

#pragma mark - ---------- 初始化数据 ----------
- (void)initializeData {
    self.controllerArr = @[@"JYMyFavoritesForStationViewController",@"JYMyFavoritesForProductViewController",@"JYMyFavoritesForSellerViewController",@"JYMyFavoritesForMvViewController"];
    self.titleArr = @[@"油站",@"商品",@"商家",@"视频"];
}

#pragma mark - ---------- 构建UI ----------
- (void)configUI {
    [self.view addSubview:self.topBackView];
    [self.view addSubview:self.mainScrollView];
    [self configChildControllers];
    [self createSegMentControl];
    [self configScrollView];
}

- (void)configChildControllers {
    [self.controllerArr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Class class = NSClassFromString(obj);
        [self addChildViewController:[class new]];
    }];
}

- (void)configScrollView {
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.mainScrollView addSubview:obj.view];
        obj.view.frame = CGRectMake(idx * SCREEN_WIDTH, 0, self.mainScrollView.width, self.mainScrollView.height);
    }];
    self.mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * self.childViewControllers.count, self.mainScrollView.size.height);
    //设置默认controller
    // YSJ_MyFavoriteVideoViewController *vc = self.childViewControllers[0];
}

- (void)createSegMentControl {
    JYSegmentedControl *segMentControlView = [[JYSegmentedControl alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
     segMentControlView.titleSize = 15;
    [segMentControlView setTitles:self.titleArr];
    [self.topBackView addSubview:segMentControlView];
    segMentControlView.selectedSegmentBlock = ^(NSInteger index){
        [UIView animateWithDuration:.25f animations:^{
            self.mainScrollView.contentOffset = CGPointMake((index -1000) * SCREEN_WIDTH, 0);
        }];
        
        switch (index-1000) {
            case 0:{
                JYMyFavoritesForStationViewController *vc = self.childViewControllers[index-1000];
            }
                break;
                
            case 1:{
                JYMyFavoritesForProductViewController *vc = self.childViewControllers[index-1000];
            }
                break;
            case 2:{
                JYMyFavoritesForSellerViewController *vc = self.childViewControllers[index-1000];
            }
                break;
            case 3:{
                JYMyFavoritesForMvViewController *vc = self.childViewControllers[index-1000];
            }
               

            default:
                break;
        }
    };
    [segMentControlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

#pragma mark - ---------- 懒加载 ----------
#pragma mark -topBackView-
- (UIView *)topBackView {
    if (!_topBackView) {
        _topBackView = [[UIView alloc]init];
        _topBackView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
    }
    return _topBackView;
}

#pragma mark -mainScrollView-
- (UIScrollView *)mainScrollView {
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc]init];
        _mainScrollView.frame = CGRectMake(0, 51, SCREEN_WIDTH, SCREEN_HEIGHT-51-64);
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.scrollEnabled = NO;
    }
    return _mainScrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
