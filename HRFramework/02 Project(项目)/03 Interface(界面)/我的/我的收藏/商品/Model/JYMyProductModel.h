//
//  JYMyProductModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYMyProductModel : JYBaseModel
/*
 goodsId = 1;
 goodsImage = "http://img27.hc360.cn/27/busin/177/317/b/27-177317277.jpg";
 goodsPrice = 2435;
 goodsScore = 300;
 goodsSubtitle = "\U526f\U6807\U9898(\U7eff\U8272\U5b57\U90e8\U5206)";
 goodsTitle = "\U6807\U98981";
 */
//养车店铺列表 商品列表 都是这个模型
//商品ID
@property (nonatomic,copy) NSString *goodsId;

//标题
@property (nonatomic,copy) NSString *goodsTitle;

//副标题(绿色字部分)
@property (nonatomic,copy) NSString *goodsSubtitle;

//图片
@property (nonatomic,copy) NSString *goodsImage;

//价格
@property (nonatomic,copy) NSString *goodsPrice;

//积分
@property (nonatomic,copy) NSString *goodsScore;
@end
