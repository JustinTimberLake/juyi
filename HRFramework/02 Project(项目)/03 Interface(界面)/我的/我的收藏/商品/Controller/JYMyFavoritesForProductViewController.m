//
//  JYMyFavoritesForProductViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyFavoritesForProductViewController.h"
#import "JYProductCell.h"
#import "JY_MyFavoriteViewModel.h"
#import "JYDetailViewController.h"
@interface JYMyFavoritesForProductViewController ()<
 UICollectionViewDelegate,
 UICollectionViewDataSource
>
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, strong) JY_MyFavoriteViewModel *viewModel;
@end

@implementation JYMyFavoritesForProductViewController

- (JY_MyFavoriteViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JY_MyFavoriteViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    _pageSize = 10;
    _pageNum = 1;
    [super viewDidLoad];
    [self configUI];
    [self refreshBackNormalFooter];
    [self networkRequest];
    [self refreshNormalHeader];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.myCollectionView.mj_header beginRefreshing];
//    [self refreshNormalHeader];
//    [self networkRequest];
//    [self reloadRequestWithArr:self.viewModel.myProducutArray];
}

- (void)configUI{
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"JYProductCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"JYProductCell"];

}
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)_pageNum]forKey:@"pageNum"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)_pageSize] forKey:@"pageSize"];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [self.viewModel requesMyLoveProductListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myCollectionView reloadData];
        [weakSelf.myCollectionView.mj_header endRefreshing];
        [weakSelf.myCollectionView.mj_footer endRefreshing];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf.myCollectionView.mj_header endRefreshing];
        [weakSelf.myCollectionView.mj_footer endRefreshing];
    }];
}

//- (void)reloadRequestWithArr:(NSMutableArray *)arr{
//    NSInteger pageCount;
//    if (arr.count % 10 == 0) {
//        pageCount = arr.count/10+1;
//    } else {
//        pageCount = arr.count/10+2;
//    }
//    _pageNum = pageCount;
//    _pageSize = 10;
//    if (_pageNum == 1) {
//        [self.viewModel.myProducutArray removeAllObjects];
//    }
//    [self networkRequest];
//}

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------
#pragma mark - 下拉刷新
- (void)refreshNormalHeader{
    WEAK(weakSelf)
    self.myCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 1;
        [weakSelf.viewModel.myProducutArray removeAllObjects];
        [weakSelf networkRequest];
    }];
}
#pragma mark - 上拉加载
- (void)refreshBackNormalFooter{
    WEAK(weakSelf)
    self.myCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        ++ _pageNum;
        [weakSelf networkRequest];
    }];
}

#pragma mark - ---------- CollectionView Delegate && DataSource && FlowLayout ----------
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.viewModel.myProducutArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"JYProductCell" forIndexPath:indexPath];
    cell.model = self.viewModel.myProducutArray[indexPath.row];
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    return CGSizeMake((SCREEN_WIDTH- 20 -5 )/2, 240);
    return CGSizeMake((SCREEN_WIDTH - 20 - 5  )/2,  [JYProductCell cellH]);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collecjtionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    JYMyProductModel * model = self.viewModel.myProducutArray[indexPath.row];
    JYDetailViewController *detailVC = [[JYDetailViewController alloc] init];
    detailVC.detailType = DetailTypeStore;
    detailVC.goodsId = model.goodsId;
    [self.navigationController pushViewController:detailVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
