//
//  JYMyDataViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYMyDataViewModel : JYBaseViewModel

//修改资料
- (void)requestChangeUserWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//修改头像
- (void)requestChangeHeadImageWithHeadImage:(NSString *)headerImageStr success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
