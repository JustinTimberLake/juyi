//
//  JYMyDataTableViewCell.h
//  JY
//
//  Created by 王方正 on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYMyDataTableViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *cellIndex;
@property (weak, nonatomic) IBOutlet UITextField *cellContentTF;
//内容编辑block
@property (nonatomic,copy) void(^TextFieldContentBlock)(NSString * text);

@end
