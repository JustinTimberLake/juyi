//
//  JYAuthViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYAuthViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *driveArr; //驾驶证数组

@property (nonatomic,strong) NSMutableArray *licenseArr;//车辆认证数组

//提交车辆认证接口
- (void)requestSubmitLicenseWithImageStr:(NSString *)imageStr success:(SuccessBlock)successBlock failure:(FailureBlock)faiureBlock;

//获取车辆认证
- (void)requestGetLicenseSuccessBlock:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//提交驾驶证认证
- (void)requestSubmitDriveWithParams:(NSMutableDictionary *)dic success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取驾驶证认证
- (void)requestGetDriveSuccess:(SuccessBlock)successBlcok failure:(FailureBlock)failureBlock;

//删除车辆认证
- (void)requestDeleteLicenseImageId:(NSString *)imageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//删除驾驶证认证
- (void)requestDeleteDriveImageId:(NSString *)imageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
