//
//  JYAuthViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAuthViewModel.h"
#import "JYImageModel.h"
#import "JYDriverImageModel.h"

@implementation JYAuthViewModel
//提交车辆认证接口
- (void)requestSubmitLicenseWithImageStr:(NSString *)imageStr success:(SuccessBlock)successBlock failure:(FailureBlock)faiureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_USER_SubmitLicense));
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"image"] = imageStr;
    [manager POST_URL:JY_PATH(JY_USER_SubmitLicense) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            faiureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        faiureBlock(ERROR_MESSAGE);
    }];
}

//获取车辆认证
- (void)requestGetLicenseSuccessBlock:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    NSLog(@"%@",JY_PATH(JY_USER_GetLicense));
    [manager POST_URL:JY_PATH(JY_USER_GetLicense) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA );
        if (RESULT_SUCCESS) {
         weakSelf.licenseArr = [JYImageModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
//            NSString * imagUrl = RESULT_DATA[@"imageUrl"];
//            NSArray * imageArr = [imagUrl componentsSeparatedByString:@","];
//            NSMutableArray * endImageArr = [NSMutableArray array];
//
//            for (NSString * url in imageArr) {
//                if ([url hasPrefix:@"http"]) {
//                    [endImageArr addObject:url];
//                }else{
//                    NSString * httpUrl ;
//                    httpUrl = SF(@"%@%@",JY_HTTP_PREFIX,url);
//                    [endImageArr addObject:httpUrl];
//                }
//            }
//            successBlock(RESULT_MESSAGE,endImageArr);
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//提交驾驶证认证
- (void)requestSubmitDriveWithParams:(NSMutableDictionary *)dic success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_USER_SubmitDrive) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//获取驾驶证认证
- (void)requestGetDriveSuccess:(SuccessBlock)successBlcok failure:(FailureBlock)failureBlock{
    WEAKSELF
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    NSLog(@"%@",JY_PATH(JY_USER_GetDrive));
    [manager POST_URL:JY_PATH(JY_USER_GetDrive) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            weakSelf.driveArr = [JYDriverImageModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
//            NSString * behindUrl  = RESULT_DATA[@"behindUrl"];
//            NSString * frontUrl = RESULT_DATA[@"frontUrl"];
//            if (behindUrl.length) {
//                [weakSelf.driveArr addObject:behindUrl];
//            }else if (frontUrl.length){
//                [weakSelf.driveArr addObject:frontUrl];
//            }
//            NSArray * arr = @[behindUrl,frontUrl];
//            [weakSelf.driveArr addObjectsFromArray:arr];
            successBlcok(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//删除车辆认证
- (void)requestDeleteLicenseImageId:(NSString *)imageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"licenseId"] = imageId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_USER_DeleteLicense) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//删除驾驶证认证
- (void)requestDeleteDriveImageId:(NSString *)imageId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"driveId"] = imageId;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_USER_DeleteDrive) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)driveArr
{
    if (!_driveArr) {
        _driveArr = [NSMutableArray array];
    }
    return _driveArr;
}

- (NSMutableArray *)licenseArr
{
    if (!_licenseArr) {
        _licenseArr = [NSMutableArray array];
    }
    return _licenseArr;
}


@end
