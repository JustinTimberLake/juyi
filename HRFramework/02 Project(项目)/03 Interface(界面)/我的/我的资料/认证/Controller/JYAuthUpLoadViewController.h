//
//  JYAuthUpLoadViewController.h
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,JYAuthType) {
    JYAuthType_Car,//车辆认证
    JYAuthType_Driver//驾驶证认证
};

@interface JYAuthUpLoadViewController : JYBaseViewController

@property (nonatomic,assign) JYAuthType authType;
@property (nonatomic,assign) NSInteger maxImgCount; //图片最大数

@end
