//
//  JYAuthUpLoadViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAuthUpLoadViewController.h"
#import "JYAuthImageCollectionViewCell.h"
#import "ZLPhotoActionSheet.h"
#import "JYImgSelectModel.h"
#import "JYAlertChooseImageViewController.h"
#import "JYUploadImgsTool.h"
#import "JYAuthViewModel.h"
#import "JYImageModel.h"
#import "JYDriverImageModel.h"
#import "JYAuthorTool.h"


#define RowCount  2
#define Margin 10
#define ItemW  (SCREEN_WIDTH - Margin * 3)/RowCount
#define ItemH  140
#define HeaderH  55

static NSString * cellId = @"JYAuthImageCollectionViewCell";
//static NSInteger maxImgCount = 2;   //暂定最多可选2张图片

typedef void(^hideChooseTitleVC)();

@interface JYAuthUpLoadViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic ,assign) int selectedImgCount;   //已选图片张数
@property (nonatomic,strong) NSMutableArray *imgsArr; //图片数组
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (nonatomic,strong) JYAlertChooseImageViewController * alertVC;
@property (nonatomic,copy) hideChooseTitleVC hideVC;
@property (nonatomic,strong) JYUploadImgsTool *uploadViewModel;
@property (nonatomic,strong) NSMutableArray *imgUrlArray;
@property (nonatomic,strong) JYAuthViewModel *authViewModel;
@end

@implementation JYAuthUpLoadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    [self configData];
    [self requestGetLicenseOrDriveImage];
    
}
#pragma mark ==================初始化数据 和UI==================
- (void)configData{
  
    JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
    model.isAddBtn = YES;
    model.img = nil;
    [self.imgsArr addObject:model];
}

- (void)setUpUI{
    
    if (self.authType == JYAuthType_Car) {
        self.naviTitle = @"车辆认证";
        self.titleLab.text = @"上传行驶证:";
    }else{
        self.naviTitle = @"驾驶证认证";
        self.titleLab.text = @"上传驾驶证:(正反各一张)";
    }
    
    self.selectedImgCount = 0;
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(ItemW, ItemH);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    
    UICollectionView * collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,  HeaderH, SCREEN_WIDTH, SCREEN_HEIGHT -  HeaderH) collectionViewLayout:layout];
    self.collectionView = collectionView;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:collectionView];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"JYAuthImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellId];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //暂定最多显示九张
    if (self.imgsArr.count == self.maxImgCount) {
        return self.maxImgCount;
    }
    return self.imgsArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    JYAuthImageCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    JYImgSelectModel * model = self.imgsArr[indexPath.row];
    [cell updateCellModel:model atIndexPath:indexPath];
    cell.addImageBlock = ^{
        weakSelf.selectedImgCount = 0 ;
        [weakSelf presentViewController:weakSelf.alertVC animated:YES completion:nil];
        weakSelf.alertVC.bottomBtnActionBlock = ^(NSInteger tag) {
            if (tag == 1000) {
                [weakSelf openCamera:^{
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                }];
            }else if (tag == 1001){
                [weakSelf openLibrary:^{
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                }];
            }else{
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
        };

        
//        ZLPhotoActionSheet *picker = [[ZLPhotoActionSheet alloc] init];
//        picker.maxSelectCount = maxImgCount-self.selectedImgCount;
//        picker.maxPreviewCount = maxImgCount;
//        
//        [picker showPhotoLibraryWithSender:self
//                     lastSelectPhotoModels:nil
//                                completion:^(NSArray<UIImage *> * _Nonnull selectPhotos, NSArray<ZLSelectPhotoModel *> * _Nonnull selectPhotoModels) {
//                                    [weakSelf updateViewAfterAddImgs:selectPhotos];
//                                }];
    };
    
    cell.deleteImageBlock = ^{
        [weakSelf deleteImageWithImageId:model.imageId AtIndexPath:indexPath];
    };
    return cell;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}


//打开相机
- (void)openCamera:(hideChooseTitleVC)hideVC{
    self.hideVC = hideVC;
    WEAKSELF
    [JYAuthorTool requestCameraAuthorization:^(JYAuthorState status) {
        if (status == JYAuthorStateAuthorized) {
            UIImagePickerController * imagePickVC = [[UIImagePickerController alloc] init];
            imagePickVC.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePickVC.allowsEditing = YES;
            imagePickVC.delegate = self;
            [weakSelf presentViewController:imagePickVC animated:YES completion:nil];
        }else{
            [weakSelf showAlertViewControllerWithTitle:@"请先在手机设置-隐私-相机-里面打开该应用权限" andLeftBtnStr:@"前往" andRightBtnStr:@"知道了"];
        }
    }];
}

//打开相册
- (void)openLibrary:(hideChooseTitleVC)hideVC{
    WEAKSELF
    self.hideVC = hideVC;
    [JYAuthorTool requestImagePickerAuthorization:^(JYAuthorState status) {
        if (status == JYAuthorStateAuthorized) {
            ZLPhotoActionSheet *picker = [[ZLPhotoActionSheet alloc] init];
            picker.maxSelectCount = self.maxImgCount-self.selectedImgCount;
            picker.maxPreviewCount = self.maxImgCount;
            
            [picker showPhotoLibraryWithSender:self lastSelectPhotoModels:nil completion:^(NSArray<UIImage *> * _Nonnull selectPhotos, NSArray<ZLSelectPhotoModel *> * _Nonnull selectPhotoModels) {
                                        hideVC();
                                        //                                if (selectPhotos.count) {
                                        //                                    weakSelf.headImage.image = selectPhotos[0];
                                        //                                    [weakSelf requestUpLoadImage];
                                        //                                }
                                        [weakSelf requestUpLoadImageWithArr: [selectPhotos mutableCopy]];
                                        //                                暂时隐藏
                                        //                                [weakSelf updateViewAfterAddImgs:selectPhotos];
                                    }];
            
        }else{
            [weakSelf showAlertViewControllerWithTitle:@"请先在手机设置-隐私-相册-里面打开该应用权限" andLeftBtnStr:@"前往" andRightBtnStr:@"知道了"];
        }
    }];
    
    
}



//- (void)dealImageFromUrlToModelWithArr:(NSArray *)arr{
//    for (NSString * imageUrl  in arr) {
//        JYImgSelectModel * model = [[JYImgSelectModel alloc] init];
//        model.isAddBtn = NO;
//        model.imageUrl = imageUrl;
//        [self.imgsArr addObject:model];
//    }
//    
//}

#pragma mark ==================网络请求==================

- (void)requestGetLicenseOrDriveImage{
    WEAKSELF
    if (self.authType == JYAuthType_Car) {
        
        [self.authViewModel requestGetLicenseSuccessBlock:^(NSString *msg, id responseData) {
//            NSArray * arr = responseData;
            [weakSelf updateViewAfterAddImgs:weakSelf.authViewModel.licenseArr];
//            [weakSelf.imgsArr addObjectsFromArray:weakSelf.authViewModel.licenseArr];
//            [weakSelf.collectionView reloadData];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }else{
        [self.authViewModel requestGetDriveSuccess:^(NSString *msg, id responseData) {
//            [weakSelf dealImageFromUrlToModelWithArr:weakSelf.authViewModel.driveArr];
//            更新操作
            [weakSelf updateViewAfterAddImgs:weakSelf.authViewModel.driveArr];
//            [weakSelf.imgsArr addObjectsFromArray:weakSelf.authViewModel.driveArr];
//            [weakSelf.collectionView reloadData];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }
}

- (void)requestUpLoadImageWithArr:(NSMutableArray *)imageArr{
    WEAKSELF
    //    [self.uploadViewModel UploadImgsWithImg:self.headImage.image Index:0 Success:^(NSString *imgStr, NSInteger index) {
    //        NSString * imgstr = imgStr;
    //        [weakSelf requestUpLoadContentWithImage:imgstr];
    //    } Fail:^(id errMsg) {
    //        [weakSelf showSuccessTip:errMsg];
    //    }];
    
    //    NSMutableArray * arr = [NSMutableArray arrayWithObject:@[self.headImage.image]];
    
    [self.uploadViewModel UploadImgs:imageArr];
    self.uploadViewModel.CompliteBlock = ^(NSMutableArray *imgStrArr ,NSMutableArray *imageHttpArr, NSMutableArray *failArrIndex) {
        if (failArrIndex.count ) {
            [weakSelf showSuccessTip:@"上传失败"];
        }else{
            weakSelf.imgUrlArray =  [imgStrArr mutableCopy];
            NSMutableString *imgStr = [NSMutableString string];
            [weakSelf.imgUrlArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if (idx == 0) {
                    [imgStr appendString:obj];
                }else{
                    [imgStr appendFormat:@",%@",obj];
                }
            }];
            [weakSelf requestUpLoadImageWithImageStr:imgStr];
            
        }
    };
}

//上传图片
- (void)requestUpLoadImageWithImageStr:(NSString *)imageStr{
    WEAKSELF
    
    if (self.authType == JYAuthType_Car) {
        [weakSelf.authViewModel requestSubmitLicenseWithImageStr:imageStr success:^(NSString *msg, id responseData) {
            [weakSelf requestGetLicenseOrDriveImage];
//            [weakSelf updateViewAfterAddImgs:weakSelf.imgsArr];
            [weakSelf showSuccessTip:msg];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }else{
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"front"] = imageStr;
        [weakSelf.authViewModel requestSubmitDriveWithParams:dic success:^(NSString *msg, id responseData) {
            [weakSelf requestGetLicenseOrDriveImage];
            [weakSelf showSuccessTip:msg];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }
}

//删除图片
- (void)deleteImageWithImageId:(NSString *)imageId AtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    if (self.authType == JYAuthType_Car) {
        [self.authViewModel requestDeleteLicenseImageId:imageId success:^(NSString *msg, id responseData) {
            [weakSelf showSuccessTip:@"删除成功"];
//            [weakSelf updateViewAfterDelImgAtIndexPath:indexPath];
            [weakSelf deleteImageUIWithIndexPath:indexPath];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }else{
        [self.authViewModel requestDeleteDriveImageId:imageId success:^(NSString *msg, id responseData) {
            [weakSelf showSuccessTip:@"删除成功"];
            [weakSelf deleteImageUIWithIndexPath:indexPath];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }
    
}

//删除后的UI操作
- (void)deleteImageUIWithIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         [weakSelf updateViewAfterDelImgAtIndexPath:indexPath];
    });
}

#pragma mark ==================UiimagePickerViewController代理==================

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    WEAKSELF
    [picker dismissViewControllerAnimated:YES completion:^{
        weakSelf.hideVC();
        UIImage * newImage = info[UIImagePickerControllerOriginalImage];
        NSArray *arr = @[newImage];
//        [weakSelf updateViewAfterAddImgs:arr];
        [weakSelf requestUpLoadImageWithArr: [arr mutableCopy]];

    }];
}




#pragma mark ==================更新图片操作==================
//里面放uiimage 数组
//- (void)updateViewAfterAddImgs:(NSArray<UIImage *>*)imgs{
//    for (UIImage *tpImg in imgs) {
//        if (self.selectedImgCount == self.maxImgCount-1) {
//            JYImgSelectModel *model = self.imgsArr.lastObject;
//            model.isAddBtn = NO;
//            model.img = tpImg;
//            self.selectedImgCount++;
//        }
//        else
//        {
//            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
//            model.isAddBtn = NO;
//            model.img = tpImg;
//            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
//            self.selectedImgCount++;
//        }
//    }
//    [self.collectionView reloadData];
//}
////里面放链接数组
//- (void)updateViewAfterAddImgs:(NSArray<NSString *>*)imgs{
////    NSArray * imageArr = [imagUrl componentsSeparatedByString:@","];
////    NSMutableArray * endImageArr = [NSMutableArray array];
////
////    for (NSString * url in imageArr) {
////        if ([url hasPrefix:@"http"]) {
////            [endImageArr addObject:url];
////        }else{
////            NSString * httpUrl ;
////            httpUrl = SF(@"%@%@",JY_HTTP_PREFIX,url);
////            [endImageArr addObject:httpUrl];
////        }
//
//    for (NSString *imageUrl in imgs) {
//        if (self.selectedImgCount == self.maxImgCount-1) {
//            JYImgSelectModel *model = self.imgsArr.lastObject;
//            model.isAddBtn = NO;
////            model.img = tpImg;
//            model.imageUrl = imageUrl;
//            self.selectedImgCount++;
//        }
//        else
//        {
//            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
//            model.isAddBtn = NO;
////            model.img = tpImg;
//            model.imageUrl = imageUrl;
//            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
//            self.selectedImgCount++;
//        }
//    }
//    [self.collectionView reloadData];
//}
//里面放链接数组
- (void)updateViewAfterAddImgs:(NSArray *)imgs{
    //    NSArray * imageArr = [imagUrl componentsSeparatedByString:@","];
    //    NSMutableArray * endImageArr = [NSMutableArray array];
    //
    //    for (NSString * url in imageArr) {
    //        if ([url hasPrefix:@"http"]) {
    //            [endImageArr addObject:url];
    //        }else{
    //            NSString * httpUrl ;
    //            httpUrl = SF(@"%@%@",JY_HTTP_PREFIX,url);
    //            [endImageArr addObject:httpUrl];
    //        }
    [self.imgsArr removeAllObjects];
    [self configData];
    if (self.authType == JYAuthType_Car) {
        for (JYImageModel * imageModel in imgs) {
            if (self.selectedImgCount == self.maxImgCount-1) {
                JYImgSelectModel *model = self.imgsArr.lastObject;
                model.isAddBtn = NO;
                //            model.img = tpImg;
                model.imageUrl = imageModel.imageUrl;
                model.imageId = imageModel.licenseId;
                self.selectedImgCount++;
            }
            else
            {
                JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
                model.isAddBtn = NO;
                //            model.img = tpImg;
                model.imageUrl = imageModel.imageUrl;
                model.imageId = imageModel.licenseId;
                [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
                self.selectedImgCount++;
            }
        }
    }else{
        for (JYDriverImageModel * imageModel in imgs) {
            if (self.selectedImgCount == self.maxImgCount-1) {
                JYImgSelectModel *model = self.imgsArr.lastObject;
                model.isAddBtn = NO;
                //            model.img = tpImg;
                model.imageUrl = imageModel.driveUrl;
                model.imageId = imageModel.driveId;
                self.selectedImgCount++;
            }
            else
            {
                JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
                model.isAddBtn = NO;
                //            model.img = tpImg;
                model.imageUrl = imageModel.driveUrl;
                model.imageId = imageModel.driveId;
                [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
                self.selectedImgCount++;
            }
        }
    }
    
    [self.collectionView reloadData];
}



//删除图片更新界面
- (void)updateViewAfterDelImgAtIndexPath:(NSIndexPath *)indexP
{
    //如果在选满图片之后删除
    if (self.selectedImgCount == self.maxImgCount) {
        [self.imgsArr removeObjectAtIndex:indexP.row];
        
        JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
        model.isAddBtn = YES;
        [self.imgsArr addObject:model];
    }
    
    if (self.selectedImgCount <self.maxImgCount) {
        [self.imgsArr removeObjectAtIndex:indexP.row];
    }
    
    self.selectedImgCount--;
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)imgsArr
{
    if (!_imgsArr) {
        _imgsArr = [[NSMutableArray alloc] init];
    }
    return _imgsArr;
}

- (JYAlertChooseImageViewController *)alertVC
{
    if (!_alertVC) {
        _alertVC =  [[JYAlertChooseImageViewController alloc] init];
        _alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;;
    }
    return _alertVC;
}


- (JYUploadImgsTool *)uploadViewModel
{
    if (!_uploadViewModel) {
        _uploadViewModel = [[JYUploadImgsTool alloc] init];
    }
    return _uploadViewModel;
}

- (NSMutableArray *)imgUrlArray
{
    if (!_imgUrlArray) {
        _imgUrlArray = [NSMutableArray array];
    }
    return _imgUrlArray;
}

- (JYAuthViewModel *)authViewModel
{
    if (!_authViewModel) {
        _authViewModel = [[JYAuthViewModel alloc] init];
    }
    return _authViewModel;
}


@end
