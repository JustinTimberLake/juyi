//
//  JYAuthenViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/9/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAuthenViewController.h"
#import "JYAuthUpLoadViewController.h"

@interface JYAuthenViewController ()

@end

@implementation JYAuthenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"认证";
}


//
- (IBAction)driverAuthBtnAction:(UIButton *)sender {
    JYAuthUpLoadViewController * authVC = [[JYAuthUpLoadViewController alloc] init];
    if (sender.tag == 1000) {
        authVC.authType = JYAuthType_Car;
        authVC.maxImgCount = 2;
    }else{
        authVC.authType = JYAuthType_Driver;
        authVC.maxImgCount = 2;
    }
    [self.navigationController pushViewController:authVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
