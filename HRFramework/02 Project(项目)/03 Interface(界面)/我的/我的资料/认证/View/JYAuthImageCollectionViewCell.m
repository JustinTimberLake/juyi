//
//  JYAuthImageCollectionViewCell.m
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAuthImageCollectionViewCell.h"
#import "JYImgSelectModel.h"

@interface JYAuthImageCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *topImageVIew;
@property (nonatomic ,strong) NSIndexPath *currentIndexPath;    //当前位置

@end
@implementation JYAuthImageCollectionViewCell

- (void)updateCellModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath
{
    self.currentIndexPath = indexPath;
    
    if (model.isAddBtn) {
        self.topImageVIew.image = [UIImage imageNamed:@"+上传行驶证"];
        self.deleteBtn.hidden = YES;
        self.topImageVIew.userInteractionEnabled = YES;
    }
    else
    {
//        self.topImageVIew.image = model.img;
        [self.topImageVIew sd_setImageWithURL:[NSURL URLWithString:model.imageUrl] placeholderImage:JY_PLACEHOLDER_IMAGE];
        self.deleteBtn.hidden = NO;
        self.topImageVIew.userInteractionEnabled = NO;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    self.deleteBtn.hidden = YES;
    self.topImageVIew.userInteractionEnabled = YES;
    
    UITapGestureRecognizer * addTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addTap:)];
    [self.topImageVIew addGestureRecognizer:addTap];
}


- (void)addTap:(UITapGestureRecognizer *)tap{
    if (_addImageBlock) {
        self.addImageBlock();
    }
}


- (IBAction)cancleBtnAction:(UIButton *)sender {
    if (_deleteImageBlock) {
        self.deleteImageBlock();
    }
}

@end
