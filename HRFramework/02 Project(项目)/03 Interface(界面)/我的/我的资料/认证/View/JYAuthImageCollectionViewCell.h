//
//  JYAuthImageCollectionViewCell.h
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYImgSelectModel;
@interface JYAuthImageCollectionViewCell : UICollectionViewCell
//添加手势
@property (nonatomic,copy) void(^addImageBlock)();
//删除手势
@property (nonatomic,copy) void(^deleteImageBlock)();

- (void)updateCellModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath;
@end
