//
//  JYDriverImageModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYDriverImageModel : JYBaseModel
@property (nonatomic,copy) NSString *driveId;
@property (nonatomic,copy) NSString *driveUrl;
@end
