//
//  JYMyRedPickViewModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyRedPickViewModel.h"

@implementation JYMyRedPickViewModel

- (NSMutableArray<JYMyRedPickModel*>*)unUsedRedPickArray{
    if (!_unUsedRedPickArray) {
        _unUsedRedPickArray = [NSMutableArray array];
    }
    return _unUsedRedPickArray;
}
- (NSMutableArray<JYMyRedPickModel*>*)usedRedPickArray{
    if (!_usedRedPickArray) {
        _usedRedPickArray = [NSMutableArray array];
    }
    return _usedRedPickArray;
}
- (NSMutableArray<JYMyRedPickModel*>*)overdueRedPickArray{
    if (!_overdueRedPickArray) {
        _overdueRedPickArray = [NSMutableArray array];
    }
    return _overdueRedPickArray;
}


- (void)requesUnusedRedPickListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyRedPick) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.unUsedRedPickArray addObjectsFromArray: [JYMyRedPickModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];


}

//已使用
- (void)requesUsedRedPickListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyRedPick) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.usedRedPickArray addObjectsFromArray: [JYMyRedPickModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
    


}
//已过期
- (void)requesOverdueRedPcikListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager  = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyRedPick) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            [self.overdueRedPickArray addObjectsFromArray: [JYMyRedPickModel mj_objectArrayWithKeyValuesArray:RESULT_DATA]];
            
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//领取优惠券
- (void)requestReceiveCouponWithCouponId:(NSString *)couponId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"couponId"] = couponId;
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@-%@",JY_PATH(JY_PERSONCENTER_DrawRedPick),dic);
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_DrawRedPick) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//获取优惠券（综合版）
- (void)requestGetCouponListWithState:(CouponState)state isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"state"] = @(state);
    NSString * pageCount;
    if (self.listArr.count % 10 == 0) {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+1 ] : @"1";
    } else {
        pageCount = isMore ? [NSString stringWithFormat:@"%lu",(long)self.listArr.count/10+2 ] : @"1";
    }
    dic[@"pageNum"] = pageCount;
    dic[@"pageSize"] = @(10);
    
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    
    [manager POST_URL:JY_PATH(JY_PERSONCENTER_MyRedPick) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            if (!isMore) {
                [weakSelf.listArr removeAllObjects];
            }
            NSArray * arr = [JYMyRedPickModel mj_objectArrayWithKeyValuesArray:RESULT_DATA];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,arr);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}

@end
