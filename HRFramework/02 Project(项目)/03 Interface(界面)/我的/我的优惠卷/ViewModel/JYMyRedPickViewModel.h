//
//  JYMyRedPickViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYMyRedPickModel.h"

typedef NS_ENUM(NSInteger,CouponState) {
    CouponState_ReadyUsed = 1,//待使用
    CouponState_AlreadyUsed,//已使用
    CouponState_AlreadyOutTime, //已过期
    CouponState_Get ,//领取
    CouponState_NewUser  //新人福利
};

@interface JYMyRedPickViewModel : JYBaseViewModel

@property (nonatomic, strong)NSMutableArray <JYMyRedPickModel *>* unUsedRedPickArray;
@property (nonatomic, strong)NSMutableArray <JYMyRedPickModel *>* usedRedPickArray;
@property (nonatomic, strong)NSMutableArray <JYMyRedPickModel *>* overdueRedPickArray;

@property (nonatomic,strong) NSMutableArray *listArr; //公共方法使用的数组

//待使用
- (void)requesUnusedRedPickListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//已使用
- (void)requesUsedRedPickListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//已过期
- (void)requesOverdueRedPcikListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取优惠券（综合版）
- (void)requestGetCouponListWithState:(CouponState)state isMore:(BOOL)isMore success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//领取优惠券
- (void)requestReceiveCouponWithCouponId:(NSString *)couponId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
