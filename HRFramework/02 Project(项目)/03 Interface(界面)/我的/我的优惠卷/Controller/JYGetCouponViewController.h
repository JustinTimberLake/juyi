//
//  JYGetCouponViewController.h
//  JY
//
//  Created by risenb on 2017/9/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYGetCouponViewController : JYBaseViewController
//是否是新人
@property (nonatomic,assign,getter=isNewUser) BOOL newUser;

@end
