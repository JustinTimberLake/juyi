//
//  JYUnusedRedPickViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUnusedRedPickViewController.h"
#import "JYMyRedPickTableViewCell.h"
#import "JYMyRedPickViewModel.h"
@interface JYUnusedRedPickViewController ()<
 UITableViewDelegate,
 UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, assign) int pageNum;
@property (nonatomic, assign) int pageSize;
@property (nonatomic, strong) JYMyRedPickViewModel *viewModel;
@end


@implementation JYUnusedRedPickViewController
#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JYMyRedPickViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyRedPickViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    _pageSize = 10;
    _pageNum = 1;
    [super viewDidLoad];
    [self configUI];
    [self refreshBackNormalFooter];
    [self networkRequest];
    [self refreshNormalHeader];
    
}

- (void)setState:(NSString *)state{
    _state = state;
    [self refreshNormalHeader];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyRedPickTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyRedPickTableViewCell"];
    
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%d",_pageNum]forKey:@"pageNum"];
    [dict setObject:[NSString stringWithFormat:@"%d",_pageSize] forKey:@"pageSize"];
//    [dict setObject:@"1" forKey:@"state"];
    dict[@"state"] = self.state;
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [self.viewModel requesUnusedRedPickListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myTableView reloadData];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
    }];
}

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------
#pragma mark - 下拉刷新
- (void)refreshNormalHeader{
    WEAK(weakSelf)
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 1;
        [weakSelf.viewModel.unUsedRedPickArray removeAllObjects];
        [weakSelf networkRequest];
    }];
}
#pragma mark - 上拉加载
- (void)refreshBackNormalFooter{
    WEAK(weakSelf)
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        ++ _pageNum;
        [weakSelf networkRequest];
    }];
}

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyRedPickTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyRedPickTableViewCell" forIndexPath:indexPath] ;
    cell.model =self.viewModel.unUsedRedPickArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 132;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.unUsedRedPickArray.count;
}

@end
