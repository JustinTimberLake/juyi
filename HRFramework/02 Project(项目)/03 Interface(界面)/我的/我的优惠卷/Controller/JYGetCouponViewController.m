//
//  JYGetCouponViewController.m
//  JY
//
//  Created by risenb on 2017/9/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYGetCouponViewController.h"
#import "JYCommonWebViewController.h"
#import "JYMyRedPickTableViewCell.h"
#import "JYMyRedPickViewModel.h"

@interface JYGetCouponViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYMyRedPickViewModel *viewModel;
@property (nonatomic,assign) CouponState couponState;

@end

@implementation JYGetCouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    [self configData];
    [self setUpRefreshData];
    [self refreshDataWithIsMore:NO];
}

#pragma mark ==================UI设置==================
- (void)setUpUI{
    self.title = self.newUser?@"新人福利":@"领取优惠券";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyRedPickTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyRedPickTableViewCell"];
}

- (void)configData{
    self.couponState = self.isNewUser ? CouponState_NewUser :CouponState_Get;
    
}

#pragma mark ==================网络请求==================

- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
    
}

- (void)refreshDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetCouponListWithState:self.couponState isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//优惠券领取请求
- (void)requestReceiveCouponWithCouponId:(NSString *)couponId{
    WEAKSELF
    [self.viewModel requestReceiveCouponWithCouponId:couponId success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf refreshDataWithIsMore:NO];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}


#pragma mark ==================UITableView 数据源和代理 ==================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyRedPickTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyRedPickTableViewCell" forIndexPath:indexPath] ;
    cell.model = self.viewModel.listArr[indexPath.row];
    cell.receiveBlock = ^{
        JYMyRedPickModel * model = self.viewModel.listArr[indexPath.row];
        [self requestReceiveCouponWithCouponId:model.couponId];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 132;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
//    JYMyRedPickModel * model = self.viewModel.listArr[indexPath.row];
//    webVC.url = model.couponUrl;
//    webVC.navTitle = @"优惠券";
//    [self.navigationController pushViewController:webVC animated:YES];
//    
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (JYMyRedPickViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYMyRedPickViewModel alloc] init];
    }
    return _viewModel;
}


@end
