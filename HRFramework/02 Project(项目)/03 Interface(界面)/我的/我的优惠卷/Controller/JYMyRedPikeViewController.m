//
//  JYMyRedPikeViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/12.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyRedPikeViewController.h"
#import "JYSegmentedControl.h"
#import "JYUnusedRedPickViewController.h"
#import "JYGetCouponViewController.h"
#import "JYMyRedPickViewModel.h"
#import "JYAutoSizeTitleView.h"
#import "JYMyRedPickTableViewCell.h"
#import "JYCommonWebViewController.h"

@interface JYMyRedPikeViewController ()
//@property (strong ,nonatomic)UIView *topBackView;
////segmentControl
//@property (strong ,nonatomic)JYSegmentedControl *segmentControl;
////主页面scorllView
//@property (strong, nonatomic)UIScrollView *mainScrollView;
////子控制器数组
//@property (strong, nonatomic)NSArray <NSString *>*controllerArr;
//上面的方法不用
//标题数组
@property (strong, nonatomic)NSArray <NSString *>*titleArr;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYMyRedPickViewModel *viewModel;
@property (nonatomic,assign) CouponState couponState; //优惠券类型
@property (nonatomic,strong) JYAutoSizeTitleView * autoSizeTitleView;
@property (weak, nonatomic) IBOutlet UIView *titleView;

@end

@implementation JYMyRedPikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的优惠券";
//    [self initializeData];
    [self configUI];
    [self setUpRefreshData];
    [self creatTitle];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

- (void)configUI{
    WEAKSELF
    [self rightItemTitle:@"领取优惠券" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
        JYGetCouponViewController * getCouponVC = [[JYGetCouponViewController  alloc] init];
        getCouponVC.newUser = NO;
        [weakSelf.navigationController pushViewController:getCouponVC animated:YES];
    }];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYMyRedPickTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYMyRedPickTableViewCell"];

}
- (void)creatTitle{
    WEAKSELF
    self.titleArr = @[@"待使用",@"已使用",@"已过期"];
    JYAutoSizeTitleView * autoSizeTitleView = [[JYAutoSizeTitleView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
//    self.autoSizeTitleView = autoSizeTitleView;
    [self.titleView addSubview:autoSizeTitleView];
    
    //    需要设置的属性
    autoSizeTitleView.norColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    autoSizeTitleView.selColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isShowUnderLine = YES;
    autoSizeTitleView.underLineH = 1;
    autoSizeTitleView.underLineColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    autoSizeTitleView.isUnderLineEqualTitleWidth = YES;
    autoSizeTitleView.fontSize = FONT(15);
    autoSizeTitleView.didSelectItemsWithIndexBlock = ^(NSInteger index) {
        switch (index) {
            case 0:
                weakSelf.couponState = CouponState_ReadyUsed;
                break;
            case 1:
                weakSelf.couponState = CouponState_AlreadyUsed;
                break;
            case 2:
                weakSelf.couponState = CouponState_AlreadyOutTime;
                break;
            default:
                break;
        }
         [self performSelector:@selector(delayMethod) withObject:nil afterDelay:0.2];
        
//        [weakSelf setUpRefreshData];
    };
//    [self.titleArr addObject:@"首页"];
    [autoSizeTitleView updataTitleData:self.titleArr];
}

-(void)delayMethod{
    [self.myTableView.mj_header beginRefreshing];
}
#pragma mark ==================网络请求==================

- (void)setUpRefreshData{
    WEAKSELF
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:NO];
    }];
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf refreshDataWithIsMore:YES];
    }];
//    [self.myTableView.mj_header beginRefreshing];
}

- (void)refreshDataWithIsMore:(BOOL)isMore{
    WEAKSELF
    [self.viewModel requestGetCouponListWithState:self.couponState isMore:isMore success:^(NSString *msg, id responseData) {
        [weakSelf endRefresh];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf endRefresh];
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)endRefresh{
    [self.myTableView.mj_header endRefreshing];
    [self.myTableView.mj_footer endRefreshing];
}

#pragma mark ==================UITableView 数据源和代理 ==================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYMyRedPickTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYMyRedPickTableViewCell" forIndexPath:indexPath] ;
    cell.model = self.viewModel.listArr[indexPath.row];
//    cell.couponState = self.couponState;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 132;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
//    JYMyRedPickModel * model = self.viewModel.listArr[indexPath.row];
//    webVC.url = model.couponUrl;
//    webVC.navTitle = @"优惠券";
//    [self.navigationController pushViewController:webVC animated:YES];
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}
//#pragma mark - ---------- 初始化数据 ----------
//- (void)initializeData {
//    self.controllerArr = @[@"JYUnusedRedPickViewController",@"JYUnusedRedPickViewController",@"JYUnusedRedPickViewController"];
//    self.titleArr = @[@"待使用",@"已使用",@"已过期"];
//}

//#pragma mark - ---------- 构建UI ----------
//- (void)configUI {
//    WEAKSELF
//    [self.view addSubview:self.topBackView];
//    [self.view addSubview:self.mainScrollView];
//    [self rightItemTitle:@"领取优惠卷" color:[UIColor colorWithHexString:JYUCOLOR_TITLE] font:[UIFont systemFontOfSize:15] action:^{
//        JYGetCouponViewController * getCouponVC = [[JYGetCouponViewController  alloc] init];
//        [weakSelf.navigationController pushViewController:getCouponVC animated:YES];    
//    }];
//    
//    [self configChildControllers];
//    [self createSegMentControl];
//    [self configScrollView];
//}

//- (void)configChildControllers {
//    [self.controllerArr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        Class class = NSClassFromString(obj);
//        [self addChildViewController:[class new]];
//    }];
//}

//- (void)configScrollView {
//    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [self.mainScrollView addSubview:obj.view];
//        obj.view.frame = CGRectMake(idx * SCREEN_WIDTH, 0, self.mainScrollView.width, self.mainScrollView.height);
//    }];
//    self.mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * self.childViewControllers.count, self.mainScrollView.size.height);
//    //设置默认controller
//    // YSJ_MyFavoriteVideoViewController *vc = self.childViewControllers[0];
//}
//
//- (void)createSegMentControl {
//    JYSegmentedControl *segMentControlView = [[JYSegmentedControl alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
//    segMentControlView.titleSize = 15;
//    [segMentControlView setTitles:self.titleArr];
//    [self.topBackView addSubview:segMentControlView];
//    segMentControlView.selectedSegmentBlock = ^(NSInteger index){
//        [UIView animateWithDuration:.25f animations:^{
//            self.mainScrollView.contentOffset = CGPointMake((index - 1000) * SCREEN_WIDTH, 0);
//        }];
//        
//        JYUnusedRedPickViewController *vc = self.childViewControllers[index-1000];
//        vc.state = SF(@"%ld",(long)index-1000 + 1);
//
//        
////        switch (index-1000) {
////            case 0:{
////                JYUnusedRedPickViewController *vc = self.childViewControllers[index-1000];
////                vc.state = SF(@"%ld",(long)index-1000 + 1);
////                
////            }
////                break;
////                
////            case 1:{
////                JYUnusedRedPickViewController *vc = self.childViewControllers[index-1000];
////            }
////                break;
////                
////            default:
////                break;
////        }
//    };
//    [segMentControlView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(0);
//    }];
//}
//
//#pragma mark - ---------- 懒加载 ----------
//#pragma mark -topBackView-
//- (UIView *)topBackView {
//    if (!_topBackView) {
//        _topBackView = [[UIView alloc]init];
//        _topBackView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
//    }
//    return _topBackView;
//}
//
//#pragma mark -mainScrollView-
//- (UIScrollView *)mainScrollView {
//    if (!_mainScrollView) {
//        _mainScrollView = [[UIScrollView alloc]init];
//        _mainScrollView = [[UIScrollView alloc]init];
//        _mainScrollView.frame = CGRectMake(0, 51, SCREEN_WIDTH, SCREEN_HEIGHT-51-64);
//       
//        _mainScrollView.pagingEnabled = YES;
//        _mainScrollView.scrollEnabled = NO;
//    }
//    return _mainScrollView;
//}
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (JYMyRedPickViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYMyRedPickViewModel alloc] init];
    }
    return _viewModel;
}

@end
