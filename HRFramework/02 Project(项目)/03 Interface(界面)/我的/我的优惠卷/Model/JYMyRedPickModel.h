//
//  JYMyRedPickModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYMyRedPickModel : JYBaseModel
/*
 couponId:// 优惠券ID
 couponTitle://标题
 couponSubheading://副标题
 couponPrice://金额
 couponImage://图片
 couponState:// 1待使用2已使用3已过期4领取
 couponStartTime://开始时间
 couponEndTime://结束时间
 couponUrl://详情
 type://1.商户2.油站3.平台
 */
@property (nonatomic, copy)NSString *couponId;
@property (nonatomic, copy)NSString *couponTitle;
@property (nonatomic, copy)NSString *couponSubheading;
@property (nonatomic, copy)NSString *couponPrice;
@property (nonatomic, copy)NSString *couponImage;
@property (nonatomic, copy)NSString *couponState;
@property (nonatomic, copy)NSString *couponStartTime;
@property (nonatomic, copy)NSString *couponEndTime;
@property (nonatomic, copy)NSString *couponUrl;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy) NSString *min_consum_amount;
@property (nonatomic, assign) NSInteger platform_coupon;

@end
