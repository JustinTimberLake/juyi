//
//  JYMyRedPickTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyRedPickTableViewCell.h"

@interface JYMyRedPickTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UIImageView *BgimageView;

@property (weak, nonatomic) IBOutlet UILabel *littleTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *detaileLabel;
@property (weak, nonatomic) IBOutlet UIButton *receiveBtn;

@property (weak, nonatomic) IBOutlet UILabel *tipLab;
@property (weak, nonatomic) IBOutlet UILabel *rmbLab;

@end

#define shanghuColor RGB0X(0x4CD964)//商户
#define youzhanColor RGB0X(0x5AC8FA)//油站
#define pingtaiColor RGB0X(0xFF9500)//平台商户
#define pingtaiyouzhanColor RGB0X(0xFF3B1E)//平台油站
#define usedColor RGB0X(0x000000)//过期

@implementation JYMyRedPickTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _receiveBtn.layer.cornerRadius = 4;
    _receiveBtn.layer.masksToBounds = YES;
    if (SCREEN_WIDTH <=320) {
        self.startTimeLB.font = [UIFont systemFontOfSize:7.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(JYMyRedPickModel *)model{
    _model = model;
    self.titleLB.text = [model.couponTitle placeholder:@"暂无"];
    self.priceLB.text = model.couponPrice;
    self.tipLab.text = [NSString stringWithFormat:@"满%ld减%ld元",(long)[model.min_consum_amount integerValue],(long)[model.couponPrice integerValue]];
    _littleTitleLab.text = model.couponSubheading;
    _receiveBtn.userInteractionEnabled = [model.couponState integerValue]==4?YES:NO;
 
    self.startTimeLB.text = SF(@"%@-%@",[NSString YYYYMMDDWithDataStr:model.couponStartTime],[NSString YYYYMMDDWithDataStr:model.couponEndTime]);
//     couponState:// 1待使用2已使用3已过期4领取5领取
    switch ([model.couponState intValue]) {
        case 1://待使用
        case 2://已使用
        case 4://立即领取
            if ([model.couponState intValue] == 1) {
                [_receiveBtn setTitle:@"待使用" forState:UIControlStateNormal];
            }else if([model.couponState intValue] == 2){
               [_receiveBtn setTitle:@"已使用" forState:UIControlStateNormal];
            }else{
                [_receiveBtn setTitle:@"立即领取" forState:UIControlStateNormal];
            }
            // type://1.商户2.油站 3.平台
            if (model.type == 1) {
                _BgimageView.image = [UIImage imageNamed:@"商户优惠券"];
                _arrow.image = [UIImage imageNamed:@"商户箭头"];
                _titleLB.textColor = shanghuColor;
                _detaileLabel.textColor = shanghuColor;
                [_receiveBtn setBackgroundColor:shanghuColor];
            }else if(model.type ==2){
                _BgimageView.image = [UIImage imageNamed:@"油站优惠券"];
                _arrow.image = [UIImage imageNamed:@"油站箭头"];
                _titleLB.textColor = youzhanColor;
                _detaileLabel.textColor = youzhanColor;
                [_receiveBtn setBackgroundColor:youzhanColor];
            }else{//3 1.商户 2.油站
                if (model.platform_coupon == 2) {
                    _BgimageView.image = [UIImage imageNamed:@"平台油站优惠券"];
                    _arrow.image = [UIImage imageNamed:@"平台油站箭头"];
                    _titleLB.textColor = pingtaiyouzhanColor;
                    _detaileLabel.textColor = pingtaiyouzhanColor;
                    [_receiveBtn setBackgroundColor:pingtaiyouzhanColor];
                    
                }else{
                    _BgimageView.image = [UIImage imageNamed:@"平台优惠券"];
                    _arrow.image = [UIImage imageNamed:@"平台箭头"];
                    _titleLB.textColor = pingtaiColor;
                    _detaileLabel.textColor = pingtaiColor;
                    [_receiveBtn setBackgroundColor:pingtaiColor];
                }
                
            }
            _priceLB.textColor = [UIColor whiteColor];
            _rmbLab.textColor = [UIColor whiteColor];
            _tipLab.textColor = [UIColor whiteColor];
            [_receiveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        
        case 3://已过期
        case 5://已失效
            if ([model.couponState intValue] == 3) {
                [_receiveBtn setTitle:@"已过期" forState:UIControlStateNormal];
            }else{
                [_receiveBtn setTitle:@"已失效" forState:UIControlStateNormal];
            }
            _BgimageView.image = [UIImage imageNamed:@"已过期已使用优惠券"];
            _arrow.image = [UIImage imageNamed:@"过期箭头"];
            _titleLB.textColor = usedColor;
            _detaileLabel.textColor = usedColor;
            _priceLB.textColor = usedColor;
            _tipLab.textColor = usedColor;
            [_receiveBtn setBackgroundColor:RGB0X(0xe4e4e4)];
            [_receiveBtn setTitleColor:usedColor forState:UIControlStateNormal];
            _rmbLab.textColor = usedColor;
            break;

        default:
            break;
    }

}

- (IBAction)receiveClick:(UIButton *)sender {
    if (self.receiveBlock) {
        self.receiveBlock();
    }
}

@end
