//
//  JYMyRedPickTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYMyRedPickModel.h"


@interface JYMyRedPickTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *startTimeLB;
@property (weak, nonatomic) IBOutlet UILabel *priceLB;
@property (weak, nonatomic) IBOutlet UILabel *titleLB;
@property (strong, nonatomic) JYMyRedPickModel * model;
//@property (nonatomic,assign) CouponState couponState;
@property (nonatomic, copy) void (^receiveBlock)(void);
@end
