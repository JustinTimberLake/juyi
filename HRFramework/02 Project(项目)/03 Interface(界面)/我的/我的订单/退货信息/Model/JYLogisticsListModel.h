//
//  JYLogisticsListModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYLogisticsListModel : JYBaseModel
//物流公司ID
@property (nonatomic,copy) NSString *logisticsId;
//物流公司名称
@property (nonatomic,copy) NSString *logisticsTitle;

@property (nonatomic,assign,getter=isSelected) BOOL selected;

@end
