//
//  JYWriteLogisticsViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYWriteLogisticsViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;
//获取物流公司
- (void)requestGetLogisticsListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//填写物流信息
- (void)requestWriteLogisticsWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
