//
//  JYWriteLogisticsViewController.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYWriteLogisticsViewController : JYBaseViewController

@property (nonatomic,copy) NSString *orderId;
@end
