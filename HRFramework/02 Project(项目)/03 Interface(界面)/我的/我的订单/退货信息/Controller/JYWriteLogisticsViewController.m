//
//  JYWriteLogisticsViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYWriteLogisticsViewController.h"
#import "JYReturnReasonTableViewCell.h"
#import "JYRechargGaosolineSectionHeaderView.h"
#import "JYWriteLogisticsViewModel.h"

@interface JYWriteLogisticsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic,strong) JYWriteLogisticsViewModel *viewModel;
@property (nonatomic,copy) NSString *logisticsId;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UITextField *logistNumTextField;

@end

@implementation JYWriteLogisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
    [self requestGetLogisticsList];
    
}

- (void)setUpUI{
    self.naviTitle = @"退货信息";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.estimatedRowHeight = 50;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYReturnReasonTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYReturnReasonTableViewCell"];
    self.myTableView.tableFooterView = self.footerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark ==================action==================
- (IBAction)commitBtnAction:(UIButton *)sender {
    
    [self requestWriteLogistics];
}


#pragma mark ==================网络请求==================

- (void)requestGetLogisticsList{
    WEAKSELF
    [self.viewModel requestGetLogisticsListSuccess:^(NSString *msg, id responseData) {
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)requestWriteLogistics{
    if (!self.logisticsId.length) {
        [self showSuccessTip:@"请选择快递公司"];
        return;
    }
    if (!self.logistNumTextField.text.length) {
        [self showSuccessTip:@"请输入快递单号"];
        return
        ;
    }
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderId"] = self.orderId;
    dic[@"logisticsId"] = self.logisticsId;
    dic[@"logisticsNum"] = self.logistNumTextField.text;
    
    [self.viewModel requestWriteLogisticsWithParams:dic success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark ==================UITableView 数据源和代理 ==================
#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    JYReturnReasonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYReturnReasonTableViewCell" forIndexPath:indexPath] ;
    cell.LogisticsModel = self.viewModel.listArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.listArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section == 1) {
//        return 20+60;
//    }
//    return 9;
//}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    JYRechargGaosolineSectionHeaderView *headerSectionView = XIB(JYRechargGaosolineSectionHeaderView);
    headerSectionView.titleLabel.text = @"选择快递";
    return headerSectionView;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
//    if (indexPath.section == 0) {
        [self.viewModel.listArr enumerateObjectsUsingBlock:^(JYLogisticsListModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == indexPath.row) {
                obj.selected = YES;
                weakSelf.logisticsId = obj.logisticsId;
            }else{
                obj.selected = NO;
            }
        }];
        [self.myTableView reloadData];
//    }
}

- (JYWriteLogisticsViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYWriteLogisticsViewModel alloc ]init];;
    }
    return _viewModel;
}

@end
