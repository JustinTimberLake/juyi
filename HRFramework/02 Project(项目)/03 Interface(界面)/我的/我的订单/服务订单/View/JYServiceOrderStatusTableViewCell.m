//
//  JYServiceOrderStatusTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYServiceOrderStatusTableViewCell.h"

@interface JYServiceOrderStatusTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *payTitleLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderTopH;


@end

@implementation JYServiceOrderStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(JYServiceOrderModel *)model{
    _model = model;
    if ([_model.orderState isEqualToString:@"1"]) {
        _payStatusLB.text = @"待支付";
    }else if ([_model.orderState isEqualToString:@"2"]){
        _payStatusLB.text = @"待使用";
    }else if ([_model.orderState isEqualToString:@"3"]){
        _payStatusLB.text = @"待评价";
    }else if ([_model.orderState isEqualToString:@"4"]){
        _payStatusLB.text = @"已完成";
    }else if ([_model.orderState isEqualToString:@"5"]){
        _payStatusLB.text = @"已关闭";
    }
//    支付方式(1支付宝2微信3帐户余额4积分抵扣5油库抵扣6优惠券抵扣)
    if ([_model.payMode isEqualToString:@"1"]) {
        _payModelLB.text = @"支付宝";
    }else if ([_model.payMode isEqualToString:@"2"]){
       _payModelLB.text = @"微信";
    }else if ([_model.payMode isEqualToString:@"3"]){
      _payModelLB.text = @"账户余额";
    }else if ([_model.payMode isEqualToString:@"4"]){
        _payModelLB.text = @"积分抵扣";
    }else if ([_model.payMode isEqualToString:@"5"]){
        _payModelLB.text = @"油库抵扣";
    }else{
      _payModelLB.text = @"优惠券抵扣";
    }
    _orderNumLB.text = _model.orderId;
    self.payTimeLB.text = model.orderTime;
    
    self.payTitleLab.hidden = [model.orderState intValue] == 1 ? YES : NO;
    self.payModelLB.hidden = self.payTitleLab.isHidden;
    self.orderTopH.constant = self.payTitleLab.isHidden ? 10 : 36;
    
//    _payTimeLB.text =  [NSString stringWithFormat:@"%@",[_model.orderTime timeStamp13ToDateWithFormatter:@"YYYY-MM-dd"]];

}


- (void)setShitiModel:(JYProductDetailModel *)shitiModel{
    _shitiModel = shitiModel;
    if ([shitiModel.orderState isEqualToString:@"1"]) {
        _payStatusLB.text = @"待支付";
    }else if ([shitiModel.orderState isEqualToString:@"2"]){
        _payStatusLB.text = @"待使用";
    }else if ([shitiModel.orderState isEqualToString:@"3"]){
        _payStatusLB.text = @"待评价";
    }else if ([shitiModel.orderState isEqualToString:@"4"]){
        _payStatusLB.text = @"已完成";
    }else if ([shitiModel.orderState isEqualToString:@"5"]){
        _payStatusLB.text = @"已关闭";
    }
    
    //    支付方式(1支付宝2微信3帐户余额4积分抵扣5油库抵扣6优惠券抵扣)
    if ([shitiModel.payMode isEqualToString:@"1"]) {
        _payModelLB.text = @"支付宝";
    }else if ([shitiModel.payMode isEqualToString:@"2"]){
        _payModelLB.text = @"微信";
    }else if ([shitiModel.payMode isEqualToString:@"3"]){
        _payModelLB.text = @"账户余额";
    }else if ([shitiModel.payMode isEqualToString:@"4"]){
        _payModelLB.text = @"积分抵扣";
    }else if ([shitiModel.payMode isEqualToString:@"5"]){
        _payModelLB.text = @"油库抵扣";
    }else{
        _payModelLB.text = @"优惠券抵扣";
    }
    
    _orderNumLB.text = shitiModel.orderId;
    self.payTimeLB.text = shitiModel.orderTime;
    
    self.payTitleLab.hidden = [shitiModel.orderState intValue] == 1 ? YES : NO;
    self.payModelLB.hidden = self.payTitleLab.isHidden;
    self.orderTopH.constant = self.payTitleLab.isHidden ? 10 : 36;

}
@end
