//
//  JYServiceOrderStatusTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYServiceOrderModel.h"
#import "JYProductDetailModel.h"
@interface JYServiceOrderStatusTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *payStatusLB;
@property (weak, nonatomic) IBOutlet UILabel *payModelLB;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLB;
@property (weak, nonatomic) IBOutlet UILabel *payTimeLB;
@property (nonatomic, strong)JYServiceOrderModel *model;

@property (nonatomic,strong) JYProductDetailModel *shitiModel;
@end
