//
//  JYShiTiDetailTableViewCell.m
//  JY
//
//  Created by Duanhuifen on 2017/12/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYShiTiDetailTableViewCell.h"

@interface JYShiTiDetailTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *scoreLab;
@property (weak, nonatomic) IBOutlet UILabel *discountLab;

@end

@implementation JYShiTiDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(JYProductDetailModel *)model{
    _model = model;
    
    self.scoreLab.text = [model.invoice floatValue] > 0 ? model.score:@"否";
    self.discountLab.text = [model.coupon floatValue] > 0? SF(@"优惠券%@元",model.coupon):@"否";

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
