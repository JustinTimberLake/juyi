//
//  JYQRCodeServiceDetailTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYQRCodeServiceDetailTableViewCell.h"

@interface JYQRCodeServiceDetailTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *qrCodeImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *tipLab;

@end

@implementation JYQRCodeServiceDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(JYServiceOrderModel *)model{
    _model = model;
    [self.qrCodeImageView sd_setImageWithURL:[NSURL URLWithString:model.qrcode] placeholderImage:JY_PLACEHOLDER_IMAGE];
}

- (void)setShitiModel:(JYProductDetailModel *)shitiModel{
    _shitiModel = shitiModel;
    [self.qrCodeImageView sd_setImageWithURL:[NSURL URLWithString:shitiModel.goods[0].qrcode] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
    self.titleLab.text = @"服务码";
    self.tipLab.text = @"提示:让员工扫二维码可以马上享受服务!";

}


@end
