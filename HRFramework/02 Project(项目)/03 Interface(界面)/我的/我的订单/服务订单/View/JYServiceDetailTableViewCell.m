//
//  JYServiceDetailTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYServiceDetailTableViewCell.h"

@implementation JYServiceDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(JYServiceOrderModel *)model{
    _model = model;
    
    self.myGasolineLB.text = [model.oilDepot floatValue] >0 ? SF(@"%@:%@L",model.oilModel,model.oilDepot) :@"否";
    self.myIntLB.text = [model.score floatValue] > 0 ? model.score:@"否";
    self.myRedPageLB.text = [model.coupon floatValue] > 0? SF(@"加油优惠券%@元",model.coupon):@"否";
    self.invoctionLB.text = [_model.invoice placeholder:@"否"];
}
@end
