//
//  JYServiceHeaderTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYServiceOrderModel.h"
@interface JYServiceHeaderTableViewCell : UITableViewCell
@property (nonatomic, strong)JYServiceOrderModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *imageTitle;
@property (weak, nonatomic) IBOutlet UILabel *shopName;
@property (weak, nonatomic) IBOutlet UILabel *shopAdressLB;
@property (weak, nonatomic) IBOutlet UILabel *shopPhoneLB;
@property (weak, nonatomic) IBOutlet UILabel *gunsLB;
@property (weak, nonatomic) IBOutlet UILabel *oilLB;
@property (weak, nonatomic) IBOutlet UILabel *oilPriceLB;
@property (weak, nonatomic) IBOutlet UILabel *oilTotalLB;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@end
