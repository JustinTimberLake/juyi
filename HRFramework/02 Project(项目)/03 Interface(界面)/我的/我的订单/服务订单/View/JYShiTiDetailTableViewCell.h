//
//  JYShiTiDetailTableViewCell.h
//  JY
//
//  Created by Duanhuifen on 2017/12/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYProductDetailModel.h"

@interface JYShiTiDetailTableViewCell : UITableViewCell
@property (nonatomic,strong) JYProductDetailModel *model;

@end
