//
//  JYServiceDetailTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYServiceOrderModel.h"
@interface JYServiceDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *myGasolineLB;
@property (weak, nonatomic) IBOutlet UILabel *myIntLB;
@property (weak, nonatomic) IBOutlet UILabel *myRedPageLB;
@property (weak, nonatomic) IBOutlet UILabel *invoctionLB;
@property (nonatomic, strong)JYServiceOrderModel * model;
@end
