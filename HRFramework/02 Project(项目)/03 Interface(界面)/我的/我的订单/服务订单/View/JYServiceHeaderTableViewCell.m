//
//  JYServiceHeaderTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYServiceHeaderTableViewCell.h"
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件

@implementation JYServiceHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(JYServiceOrderModel *)model{
    _model = model;
    [_imageTitle sd_setImageWithURL:[NSURL URLWithString:_model.shopImage] placeholderImage:[UIImage imageNamed:@"店铺列表"]];
    _shopName.text = [_model.shopTitle placeholder:@"暂无"];
    _shopAdressLB.text = [_model.shopAddress placeholder:@"暂无"];
    _shopPhoneLB.text = [_model.shopTel placeholder:@"暂无"];
    _oilLB.text = [NSString stringWithFormat:@"%@",_model.oilModel];
    _gunsLB.text = [NSString stringWithFormat:@"%@油枪",_model.gunNum];
    _oilPriceLB.text = [NSString stringWithFormat:@"%@元/升",_model.oilPrice];
    _oilTotalLB.text = [NSString stringWithFormat:@"%@升",_model.oilNum];
    CGFloat startLat = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLAT] floatValue];
    CGFloat startLng = [[JY_USERDEFAULTS objectForKey:JY_CURRENT_CITYLNG] floatValue];
    _distanceLabel.text = [self getTheDistanceWithStartLat:startLat startLng:startLng andEndLat:[model.shopLat floatValue] endLng:[model.shopLong floatValue]];
}


//获得两点之间的距离
- (NSString *)getTheDistanceWithStartLat:(CGFloat)startLat startLng:(CGFloat)startLng andEndLat:(CGFloat)endLat endLng:(CGFloat )endLng{
    BMKMapPoint start = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(startLat,startLng));
    BMKMapPoint end = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(endLat,endLng));
    CLLocationDistance distance = BMKMetersBetweenMapPoints(start, end);
    NSString *distanceStr;
    //    if (distance > 1000) {
    distanceStr = [NSString stringWithFormat:@"%.2fkm",distance/1000];
    //    }else{
    //        distanceStr = [NSString stringWithFormat:@"%.2fm",distance];
    //    }
    return distanceStr;
}
@end
