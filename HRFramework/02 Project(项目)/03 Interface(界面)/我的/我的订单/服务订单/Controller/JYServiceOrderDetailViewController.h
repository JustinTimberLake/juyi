//
//  JYServiceOrderDetailViewController.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"
typedef NS_ENUM(NSInteger,JYDetailType) {
    JYDetailType_Service, //服务订单
    JYDetailType_ShiTi    //实体订单
};

@interface JYServiceOrderDetailViewController : JYBaseViewController
//订单ID
@property (nonatomic,copy) NSString *orderId;
//是否是查看模式
@property (nonatomic,assign,getter=isCheckMode) BOOL checkMode;

@property (nonatomic,assign) JYDetailType detailType;

//是否从扫一扫进入
//@property (nonatomic,assign,getter=isQrEnterMode) BOOL qrEnterMode;


@end
