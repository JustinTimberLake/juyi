//
//  JYServiceOrderDetailViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//  员工扫码执行的接口

#import "JYServiceOrderDetailViewController.h"
#import "JYServiceHeaderTableViewCell.h"
#import "JYServiceDetailTableViewCell.h"
#import "JYQRCodeServiceDetailTableViewCell.h"
#import "JYServiceOrderStatusTableViewCell.h"
#import "JYMyOrderViewModel.h"
#import "JYPayViewModel.h"
#import "JYAlipayClient.h"
#import "JYPaySuccessViewController.h"
#import "JYBaseTabBarController.h"
#import "JYApplyRefundViewModel.h"
#import "JYAlertPicViewController.h"
#import "JYProductOderDetailTableViewCell.h"
#import "JYShiTiDetailTableViewCell.h"
#import "JYEvaluateViewController.h"
#import "JYDetailViewController.h"
#import "JYStoreViewController.h"
#import "SocketRocketUtility.h"
#import "User_InfoShared.h"
typedef NS_ENUM(NSInteger,OrderState) {
    OrderState_ReadyPay,//待支付
    OrderState_ReadyUse,//待使用
    OrderState_Other//其他状态，评价，关闭，完成，
};

@interface JYServiceOrderDetailViewController ()<
    UITableViewDataSource,
    UITableViewDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) JYMyOrderViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;
@property (nonatomic,assign) OrderState orderState; //订单状态
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftBtn;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightBtn;

@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (nonatomic,strong) JYApplyRefundViewModel *applyRefundViewModel;

//// 删除、取消、退款 服务订单类型
//@property (nonatomic,assign) DelServiceOrderType delorderType;

@end

@implementation JYServiceOrderDetailViewController{
    NSTimer *_timer;
}

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JYMyOrderViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyOrderViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self networkRequest];
    [self addNotification];
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:NSClassFromString(@"JYScanQRViewController")]) {
            [array removeObject:vc];
            self.navigationController.viewControllers = array;
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //关闭
//    [[SocketRocketUtility instance]SRWebSocketClose];

}
#pragma mark ==================通知==================


- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
    JY_ADD_NOTIFICATION(kWebSocketDidOpenNote);
    JY_ADD_NOTIFICATION(kWebSocketdidReceiveMessageNote);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
        [self showSuccessTip:noti.object];
    }else if([noti.name isEqualToString:kWebSocketDidOpenNote] ){
        //webSocket连接成功
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"type"] = @(2);
        dic[@"orderId"] = self.orderId;
        [[SocketRocketUtility instance] sendData:[self DataTOjsonString:dic]];
    }else if ([noti.name isEqualToString:kWebSocketdidReceiveMessageNote]){
        NSDictionary *message = noti.object;
        NSLog(@"%@",message);
//        self.viewModel.serviceDetail.orderState = @"3";
//        [self updataOrderStateWithDetailType:self.detailType];
//        [self updataBottomUIWithDetailType:self.detailType];
//        [self.myTableView reloadData];
//        [[SocketRocketUtility instance]SRWebSocketClose];
    }
    
}

-(NSString*)DataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}


- (void)dealloc{
    [self destroyTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)backAction{
    if (self.checkMode) {
        JYBaseTabBarController * tabBarVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        tabBarVC.selectedIndex = 3;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"订单详情";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceHeaderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceHeaderTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceDetailTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYQRCodeServiceDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYQRCodeServiceDetailTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYServiceOrderStatusTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYServiceOrderStatusTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYProductOderDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYProductOderDetailTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYShiTiDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYShiTiDetailTableViewCell"];

    self.bottomLeftBtn.hidden = YES;
    
}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAKSELF
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    dict[@"orderId"] = self.orderId;
    
//    [self.viewModel requesMyServiceOrderDetailWithParams:dict  success:^(NSString *msg, id responseData) {
//        [weakSelf hideHUD];
//        [weakSelf updataOrderStateWithDetailType:weakSelf.detailType];
//        [weakSelf updataBottomUIWithDetailType:weakSelf.detailType];
//        [weakSelf.myTableView reloadData];
//    } failure:^(NSString *errorMsg) {
//        [weakSelf showSuccessTip:errorMsg];
//        [weakSelf hideHUD];
//    }];
    
    [self.viewModel requesMyServiceOrderDetailWithParams:dict andType:self.detailType success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf updataOrderStateWithDetailType:weakSelf.detailType];
        [weakSelf updataBottomUIWithDetailType:weakSelf.detailType];
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
    }];
}

// 删除、取消 服务订单
- (void)requestDeleteOrderWithOrderId:(NSString *)orderId andType:(DelOrderType)type{
    WEAKSELF
    [self.viewModel requestMyServiceDelServiceOrderWithOrderId:orderId andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//未发货，退款
- (void)requestApplyRefundWithOrderId:(NSString *)orderId andType:(JYApplyRefundType)type{
    WEAKSELF
    [self.applyRefundViewModel requestApplyRefundWithNotSendProductWithOrderId:orderId andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf networkRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


// 1待支付2待使用3待评价4已完成5已关闭
//更新订单状态
- (void)updataOrderStateWithDetailType:(JYDetailType)detailType{
    NSString * state;
    if (detailType == JYDetailType_Service) {
        state = self.viewModel.serviceDetail.orderState;
    }else{
        state = self.viewModel.shitiDetail.orderState;
    }

    if ([state intValue] == 1) {
        self.orderState = OrderState_ReadyPay;
    }else if ([state intValue] == 2){
        self.orderState = OrderState_ReadyUse;//待使用
        //开启链接
//        [[SocketRocketUtility instance]SRWebSocketOpenWithURLString:JY_USER_WebSocket];
        //计时器，循环执行
        _timer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                        target:self
                                                      selector:@selector(timeStack)
                                                      userInfo:nil
                                                       repeats:YES];
        
    }else{
        self.orderState = OrderState_Other;
    }
}


-(void)timeStack{
    [self.viewModel requestCheckOrderpayWithOrderId:self.orderId success:^(NSString *msg, id responseData) {
        if ([responseData integerValue] == 3) {
            self.viewModel.serviceDetail.orderState = @"3";
            [self updataOrderStateWithDetailType:self.detailType];
            [self updataBottomUIWithDetailType:self.detailType];
            [self.myTableView reloadData];
            [self destroyTimer];
        }
    } failure:^(NSString *errorMsg) {
        
    }];
}

//更新底部UI和按钮状态
// 1待支付2待使用3待评价4已完成5已关闭
- (void)updataBottomUIWithDetailType:(JYDetailType)detailType{
    
    NSString * orderState;
    if (detailType == JYDetailType_Service) {
        orderState = self.viewModel.serviceDetail.orderState;
        self.totalPriceLab.text = SF(@"%@",self.viewModel.serviceDetail.totalPrice);
    }else{
         orderState = self.viewModel.shitiDetail.orderState;
        self.totalPriceLab.text = SF(@"%@",self.viewModel.shitiDetail.totalPrice);
    }
    
//    更新底部按钮样式
    self.bottomLeftBtn.hidden = (self.orderState == OrderState_ReadyUse) ? NO:YES;
    if ([orderState intValue] == 4 || [orderState intValue] == 5) {
        [self.bottomRightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        [self.bottomRightBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
    }
    
    switch ([orderState intValue]) {
        case 1:
            [self.bottomRightBtn setTitle:@"去支付" forState:UIControlStateNormal];
            break;
        case 2:
            [self.bottomRightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
            break;
        case 3:
            [self.bottomRightBtn setTitle:@"去评价" forState:UIControlStateNormal];
            break;
        case 4:{
            [self.bottomRightBtn setTitle:@"已完成" forState:UIControlStateNormal];
        }
            break;

        case 5:{
            [self.bottomRightBtn setTitle:@"已关闭" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
}

- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId andTotalPrice:(NSString *)totalPrice{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalPrice floatValue]);
    
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalPrice,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}


#pragma mark actions （点击事件）
- (IBAction)bottomBtnAction:(UIButton *)sender {
    WEAKSELF
    if (sender.tag == 1000) {
        [self showDeletePicAlertViewControllerSureBtnActionBlock:^{
            [weakSelf requestDeleteOrderWithOrderId:self.orderId andType:DelOrderType_Del];
        } closeBtnActionBlock:^{
            
        }];
    }else{
        sender.enabled = [sender.currentTitle isEqualToString:@"已关闭"] ?NO :YES;
        if ([sender.currentTitle isEqualToString:@"去支付"]) {
//            [self showAlertWithPickerViewType:PickerViewTypePay];
            [self showAlertPayWays];
            self.returnPayTypeBlock = ^(NSString *str) {
                [weakSelf payTypeWithStr:str andOrderId:weakSelf.orderId];
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                if (weakSelf.orderId.length) {
                    dic[@"orderId"] = weakSelf.orderId;
                    dic[@"orderType"] = weakSelf.detailType == JYDetailType_Service ?@"服务订单":@"实体订单";
                }
                JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
            };

        }else if ([sender.currentTitle isEqualToString:@"去评价"]){
            JYEvaluateViewController * evaluateVC = [[JYEvaluateViewController alloc] init];
            evaluateVC.orderId = weakSelf.orderId;
            evaluateVC.orderType = OrderType_Service;
            [self.navigationController pushViewController:evaluateVC animated:YES];

        }else if ([sender.currentTitle isEqualToString:@"申请退款"]){
            
            [self requestApplyRefundWithOrderId:self.orderId andType:JYApplyRefundType_Service];
//            废弃
//            [self requestDeleteOrderWithOrderId:self.viewModel.serviceDetail.orderId andType:DelOrderType_Refund];
        }
    }}


#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
       switch (indexPath.section) {
        case 0:{
            if (self.detailType == JYDetailType_Service) {
                
                JYServiceHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceHeaderTableViewCell"];
                cell.model = self.viewModel.serviceDetail;
                return cell;
            }else{
                JYProductOderDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYProductOderDetailTableViewCell"forIndexPath:indexPath];
                cell.shitiModel  = self.viewModel.shitiDetail;
                return cell;
            }
            
        }
            break;
        case 1:{
            if (self.detailType == JYDetailType_Service) {
                JYServiceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceDetailTableViewCell"];
                cell.model = self.viewModel.serviceDetail;
                return cell;
            }else{
                JYShiTiDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYShiTiDetailTableViewCell"];
                cell.model = self.viewModel.shitiDetail;
                return cell;

                
            }
        }
            break;
            
        case 2:{
            if (self.orderState == OrderState_ReadyUse) {
                JYQRCodeServiceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYQRCodeServiceDetailTableViewCell"];
                if (self.detailType == JYDetailType_Service) {
                    cell.model = self.viewModel.serviceDetail;
                }else{
                    cell.shitiModel = self.viewModel.shitiDetail;
                }
                return cell;
            }else{
                JYServiceOrderStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceOrderStatusTableViewCell"];
                if (self.detailType == JYDetailType_Service) {
                    
                    cell.model = self.viewModel.serviceDetail;
                }else{
                     cell.shitiModel = self.viewModel.shitiDetail;
                }
                return cell;

            }
        }
            
        case 3:{
            if (self.orderState == OrderState_ReadyUse) {
                JYServiceOrderStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceOrderStatusTableViewCell"];
                if (self.detailType == JYDetailType_Service) {
                    
                    cell.model = self.viewModel.serviceDetail;
                }else{
                    cell.shitiModel = self.viewModel.shitiDetail;
                }
                return cell;
            }else{
                return nil;
            }
        }

            break;
        default:
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            if (self.detailType == JYDetailType_Service) {
                return 190;
                
            }else{
               return 141 ;
            }
        }
            break;
        case 1:{
            if (self.detailType == JYDetailType_Service) {
                return 203;
            }else{
                return 101;
            }
        }
            break;
        case 2:{
            if (self.orderState == OrderState_ReadyUse) {
                return 316;
            }else if(self.orderState == OrderState_ReadyPay){
                return 90;
            }else{
                return 125;
            }
        }
            break;
        case 3:{
            if (self.orderState == OrderState_ReadyPay) {
                return 90;
            }else{
                return 115;
            }
        }
        default:
            break;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.orderState == OrderState_ReadyUse) {
        return 4;
    }else{
        return 3;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_GRAY_BG];
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section != 0) {
        return;
    }
    if (self.detailType == JYDetailType_Service) {
        //油站
        JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
        detailVC.detailType = DetailTypeAddOil;
        detailVC.shopId = self.viewModel.serviceDetail.shopId;
        [self.navigationController pushViewController:detailVC animated:YES];
    }else{
        JYStoreViewController * storeVC = [[JYStoreViewController alloc] init];
        storeVC.shopType = JYShopType_keepCar;
        storeVC.shopId = self.viewModel.shitiDetail.shopId;
        [self.navigationController pushViewController:storeVC animated:YES];
    }
}

#pragma mark ==================支付代码==================

//支付方式选择
- (void)payTypeWithStr:(NSString *)str andOrderId:(NSString *)orderId{
    if ([str isEqualToString:@"账户支付"]) {
        NSLog(@"账户支付");
        if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [self.totalPriceLab.text floatValue]) {
            [self showAlertPicViewWithAccountTypeWithOrderId:orderId andTotalPrice:self.totalPriceLab.text];;
            
        }else{
            [self showAlertPicViewWithNotEnoughMoney];
        }
    }else if([str isEqualToString:@"微信支付"]){
        NSLog(@"微信支付");
        if (![WXApi isWXAppInstalled]) {
            [self showSuccessTip:@"请安装微信客户端"];
            return;
        }else{
            
            [self requestWXSignWithOrderNum:orderId];
        }
    }else{
        NSLog(@"支付宝支付");
        [self requestAlipaySignWithOrderNum:orderId];
    }
}


//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
//        if (self.isQrEnterMode) {
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        //        [weakSelf showAlertPicViewWithNotEnoughMoney];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
}



//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            //            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
//                switch ([code intValue]) {
//                    case 9000:
//                        //                        [self popToRootViewControllerAnimated:NO];
//                    {
//                        [weakSelf turnPaySuccessPage];
//                    }
//                        break;
//                    case 8000:
//                        //                        [self showSuccessTip:@"正在处理中"];
//                        break;
//                    case 4000:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                    case 6001:
//                        //                        [self showSuccessTip:@"支付已取消"];
//                        break;
//                    case 6002:
//                        //                        [self showSuccessTip:@"网络连接错误"];
//                        break;
//                    default:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                }
                
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//跳转到支付成功页面
- (void)turnPaySuccessPage{
    //    UIApplication *app = [UIApplication sharedApplication];
    //    JYBaseTabBarController *tabbar = (JYBaseTabBarController*)app.delegate.window.rootViewController;
    //    UINavigationController *nav= tabbar.viewControllers[tabbar.selectedIndex];
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    [self.navigationController pushViewController:successVC animated:YES];
}


- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}

- (JYApplyRefundViewModel *)applyRefundViewModel
{
    if (!_applyRefundViewModel) {
        _applyRefundViewModel = [[JYApplyRefundViewModel alloc] init];
    }
    return _applyRefundViewModel;
}

//销毁定时器
- (void)destroyTimer{
    [_timer invalidate];
    _timer = nil;
}

@end
