//
//  JYServiceOrderModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/8/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYServiceOrderModel : JYBaseModel
/*
 shopEvaluate://加油站评价
 shopAddress://加油站地址
 shopTel://加油站电话
 shopLong://加油站经度
 shopLat://加油站纬度
 coupon://优惠券
 oilDepot://油库
 score://积分
 invoice://发票信息
 orderTime://下单时间
 payMode://支付方式(1支付宝2微信3余额4油库)
 

 */
@property (nonatomic, copy)NSString *orderId;
@property (nonatomic, copy)NSString *shopId;
@property (nonatomic, copy)NSString *shopTitle;
@property (nonatomic, copy)NSString *shopImage;
@property (nonatomic, copy)NSString *gunNum;
@property (nonatomic, copy)NSString *oilModel;
@property (nonatomic, copy)NSString *oilPrice;
@property (nonatomic, copy)NSString *oilNum;
@property (nonatomic, copy)NSString *orderState;
@property (nonatomic, copy)NSString *totalPrice;
@property (nonatomic, copy)NSString *shopEvaluate;
@property (nonatomic, copy)NSString *shopAddress;
@property (nonatomic, copy)NSString *shopTel;
@property (nonatomic, copy)NSString *shopLong;
@property (nonatomic, copy)NSString *shopLat;
@property (nonatomic, copy)NSString *coupon;
@property (nonatomic, copy)NSString *oilDepot;
@property (nonatomic, copy)NSString *score;
@property (nonatomic, copy)NSString *invoice;
@property (nonatomic, copy)NSString *orderTime;
@property (nonatomic, copy)NSString *payMode;
@property (nonatomic,copy) NSString *qrcode;

@end
