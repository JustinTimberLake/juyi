//
//  JYPayViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/11/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPayViewModel.h"
#import "WeXinRequest.h"

@implementation JYPayViewModel

//JY-012-005 获取第三方支付信息(所有支付)
- (void)requestGetPayWithOrderId:(NSString *)orderId payType:(JYPayType)payType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderId"] = orderId;
    dic[@"payType"] = @(payType);
    NSLog(@"%@---%@",JY_PATH(JY_PAY_GetPay),dic);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PAY_GetPay) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSLog(@"++++%@",result);
            NSString *str = result[@"data"][@"money"];
            [User_InfoShared shareUserInfo].personalModel.userRemainder = str;
            JY_POST_NOTIFICATION(JY_NOTI_PAY_BALANCE_SUCCESS, str);
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//JY-012-006 获取支付宝待签名字符串接口
- (void)requestGetAlipayPaySignWithOrderSn:(NSString *)orderSn success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"orderSn"] = orderSn;
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@---%@",JY_PATH(JY_PAY_GetAlipayPaySign),dic);
    [manager POST_URL:JY_PATH(JY_PAY_GetAlipayPaySign) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            NSString * sign = RESULT_DATA[@"sign"];
            successBlock(RESULT_MESSAGE,sign);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestGetWxPaySignWithOrderSn:(NSString *)orderSn success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"orderSn"] = orderSn;
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@===%@",JY_PATH(JY_PAY_GetWxPaySign),dic);
    [manager POST_URL:JY_PATH(JY_PAY_GetWxPaySign) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            self.wxRequest = [WeXinRequest mj_objectWithKeyValues:RESULT_DATA];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//JY-012-010 账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
//    JY_PAY_AccountBalancePay
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderSn"] = orderId;
    NSLog(@"%@ --%@",JY_PATH(JY_PAY_AccountBalancePay),dic);
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_PAY_AccountBalancePay) params:dic success:^(id result) {
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


@end
