//
//  JYPayViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/11/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

typedef NS_ENUM(NSInteger,JYPayType) {
    JYPayType_Alipay = 1,//支付宝
    JYPayType_WeChat,//微信
    JYPayType_Account//账户余额
};

@interface JYPayViewModel : JYBaseViewModel

@property (nonatomic,strong) WeXinRequest *wxRequest;

//JY-012-005 获取第三方支付信息(所有支付)
- (void)requestGetPayWithOrderId:(NSString *)orderId payType:(JYPayType)payType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//JY-012-006 获取支付宝待签名字符串接口
- (void)requestGetAlipayPaySignWithOrderSn:(NSString *)orderSn success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestGetWxPaySignWithOrderSn:(NSString *)orderSn success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//JY-012-010 账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;



@end
