//
//  JYEvaluateViewController.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"
//#import "JYProductDetailModel.h"

typedef NS_ENUM(NSInteger,OrderType) {
    OrderType_Service = 1,//服务订单
    OrderType_Product, //商品订单
    OrderType_ShiTi //实体订单
};
@interface JYEvaluateViewController : JYBaseViewController

@property (nonatomic,copy) NSString *orderId;

@property (nonatomic,assign) OrderType orderType;

//@property (nonatomic,strong) JYProductDetailModel *detailModel;
@property (nonatomic,strong) NSString *shopImage;

@end
