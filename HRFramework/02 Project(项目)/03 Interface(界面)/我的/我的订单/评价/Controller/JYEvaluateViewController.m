//
//  JYEvaluateViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYEvaluateViewController.h"
#import "JYOpinionCollectionReusableView.h"
#import "JYUploadImgsTool.h"
#import "JYImgSelectModel.h"
#import "ZLPhotoActionSheet.h"
#import "JYUploadImageCollectionViewCell.h"
#import "JYOpinionFootCollectionReusableView.h"
#import "JYEvaluateHeaderView.h"
#import "JYEvaluateViewModel.h"
#import "JYAuthorTool.h"

@interface JYEvaluateViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property (strong, nonatomic) NSMutableArray <NSData *>* imageArray;
//@property (nonatomic,strong) JYOptionViewModel *viewModel;
//@property (nonatomic,strong) JYOpinionCollectionReusableView * header;
@property (nonatomic,strong) JYEvaluateHeaderView *header;
@property (nonatomic ,assign) NSInteger selectedImgCount;   //已选图片张数
@property (nonatomic ,assign) NSInteger maxImgCount;   //最大图片数
@property (nonatomic,strong) JYUploadImgsTool *uploadViewModel;
@property (nonatomic,strong) NSMutableArray *imgUrlArray;
@property (nonatomic,copy) NSString *imageStr;//图片str
@property (nonatomic,strong) NSMutableArray *imgsArr; //图片的模型数组
@property (nonatomic,strong) JYEvaluateViewModel *viewModel;
@property (nonatomic,strong) NSMutableArray<UIImage *> *picArr; //图片数组

@end

@implementation JYEvaluateViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (NSMutableArray<NSData*>*)imageArray{
    if (!_imageArray) {
        _imageArray = [NSMutableArray array];
        [_imageArray addObject:[NSData new]];//插入一条加号数据
    }
    return _imageArray;
}

#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self configData];
}


- (void)configData{
    self.selectedImgCount = 0;
    
    JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
    model.isAddBtn = YES;
    model.img = nil;
    [self.imgsArr addObject:model];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"发表评价";
    self.myCollection.delegate = self;
    self.myCollection.dataSource = self;
    [self.myCollection registerNib:[UINib nibWithNibName:@"JYUploadImageCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"JYUploadImageCollectionViewCell"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"JYEvaluateHeaderView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"JYEvaluateHeaderView"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"JYOpinionFootCollectionReusableView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"JYOpinionFootCollectionReusableView"];
    self.maxImgCount = 3;
}
#pragma mark networkRequest (网络请求)


//打开相册
- (void)openLibrary{
    WEAKSELF 
    [JYAuthorTool requestImagePickerAuthorization:^(JYAuthorState status) {
        if (status == JYAuthorStateAuthorized) {
            ZLPhotoActionSheet *picker = [[ZLPhotoActionSheet alloc] init];
            picker.maxSelectCount = self.maxImgCount-self.selectedImgCount;
            picker.maxPreviewCount = self.maxImgCount;
            
            [picker showPhotoLibraryWithSender:self
                         lastSelectPhotoModels:nil
                                    completion:^(NSArray<UIImage *> * _Nonnull selectPhotos, NSArray<ZLSelectPhotoModel *> * _Nonnull selectPhotoModels) {
                                        //                                hideVC();
                                        [weakSelf.picArr addObjectsFromArray:selectPhotos];
                                        [weakSelf updateViewAfterAddImgs:selectPhotos];

//                                        [weakSelf requestUpLoadImageWithArr: [selectPhotos mutableCopy]];
                                    }];

        }else{
          [weakSelf showAlertViewControllerWithTitle:@"请先在手机设置-隐私-相册-里面打开该应用权限" andLeftBtnStr:@"前往" andRightBtnStr:@"知道了"];
        }
    }];
}



- (void)requestUpLoadImageWithArr:(NSMutableArray *)imageArr finishBlock:(void (^)(NSString * imagesStr))finishBlock{
    
    WEAKSELF
    //    [self.uploadViewModel UploadImgsWithImg:self.headImage.image Index:0 Success:^(NSString *imgStr, NSInteger index) {
    //        NSString * imgstr = imgStr;
    //        [weakSelf requestUpLoadContentWithImage:imgstr];
    //    } Fail:^(id errMsg) {
    //        [weakSelf showSuccessTip:errMsg];
    //    }];
    
    //    NSMutableArray * arr = [NSMutableArray arrayWithObject:@[self.headImage.image]];
    
    [self.uploadViewModel UploadImgs:imageArr];
    self.uploadViewModel.CompliteBlock = ^(NSMutableArray *imgStrArr ,NSMutableArray *imageHttpArr, NSMutableArray *failArrIndex) {
        if (failArrIndex.count ) {
            [weakSelf showSuccessTip:@"上传失败"];
        }else{
            weakSelf.imgUrlArray =  [imageHttpArr mutableCopy];
            
            NSMutableString *imgStr = [NSMutableString string];
            [imgStrArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if (idx == 0) {
                    [imgStr appendString:obj];
                }else{
                    [imgStr appendFormat:@",%@",obj];
                }
            }];
            weakSelf.imageStr = [imgStr copy];
            if (finishBlock) {
                finishBlock(weakSelf.imageStr);
            }
//            [weakSelf updateViewAfterAddImgs:weakSelf.imgUrlArray];
            //            [weakSelf requestUpLoadImageWithImageStr:imgStr];
            
        }
    };
}
//更新数据images 类型
- (void)updateViewAfterAddImgs:(NSArray<UIImage *>*)imgs{
    
    for (UIImage *image in imgs) {
        if (self.selectedImgCount == self.maxImgCount-1) {
            JYImgSelectModel *model = self.imgsArr.lastObject;
            model.isAddBtn = NO;
            model.img = image;
            //            model.imageUrl = imageUrl;
            self.selectedImgCount++;
        }
        else
        {
            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
            model.isAddBtn = NO;
            model.img = image;
            //            model.imageUrl = imageUrl;
            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
            self.selectedImgCount++;
        }
    }
    [self.myCollection reloadData];
}

- (void)updataViewAfterDeleteImageAtIndex:(NSInteger)index{
    //    [self.picArr removeObjectAtIndex:index];
    
    if (self.selectedImgCount == self.maxImgCount) {
        JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
        model.isAddBtn = YES;
        model.img = nil;
        [self.imgsArr addObject:model];
    }
    if (self.selectedImgCount <= self.maxImgCount) {
        [self.imgsArr removeObjectAtIndex:index];
        [self.picArr removeObjectAtIndex:index];
    }
    self.selectedImgCount -- ;
    
    [self.myCollection reloadData];
}


//- (void)requestUpLoadImageWithImageStr:(NSString *)imgStr{
//    [self requestFeedbackWithImageStr:imgStr];
//}
//url 类型的
//- (void)updateViewAfterAddImgs:(NSArray<NSString *>*)imgs{
//    //    NSArray * imageArr = [imagUrl componentsSeparatedByString:@","];
//    //    NSMutableArray * endImageArr = [NSMutableArray array];
//    //
//    //    for (NSString * url in imageArr) {
//    //        if ([url hasPrefix:@"http"]) {
//    //            [endImageArr addObject:url];
//    //        }else{
//    //            NSString * httpUrl ;
//    //            httpUrl = SF(@"%@%@",JY_HTTP_PREFIX,url);
//    //            [endImageArr addObject:httpUrl];
//    //        }
//
//    for (NSString *imageUrl in imgs) {
//        if (self.selectedImgCount == self.maxImgCount-1) {
//            JYImgSelectModel *model = self.imgsArr.lastObject;
//            model.isAddBtn = NO;
////                        model.img = tpImg;
//            model.imageUrl = imageUrl;
//            self.selectedImgCount++;
//        }
//        else
//        {
//            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
//            model.isAddBtn = NO;
////                        model.img = tpImg;
//            model.imageUrl = imageUrl;
//            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
//            self.selectedImgCount++;
//        }
//    }
//    [self.myCollection reloadData];
//}

//评价请求
- (void)requestEvaluate{
    if (self.header.starScore == 0) {
        [self showSuccessTip:@"请选择星级评价"];
        return;
    }
//    if (!self.header.contentTextView.text.length) {
//        [self showSuccessTip:@"请输入评价内容"];
//        return;
//    }
    if (self.header.contentTextView.text.length <6) {
        [self showSuccessTip:@"评价内容不少于6个字"];
        return;
    }
    if (self.header.contentTextView.text.length >1000) {
        [self showSuccessTip:@"评价内容不能大于1000个字"];
        return;
    }
//    if (!self.picArr.count) {
//        [self showSuccessTip:@"请选择上传图片"];
//        return;
//    }
    
    [self requestUpLoadImageWithArr:self.picArr finishBlock:^(NSString *imagesStr) {
        WEAKSELF
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[@"C"] = [User_InfoShared shareUserInfo].c;
        dic[@"orderType"] = @(weakSelf.orderType);
        dic[@"orderId"] = weakSelf.orderId;
        dic[@"star"] = @(weakSelf.header.starScore);
        dic[@"content"] = weakSelf.header.contentTextView.text;
        if (imagesStr.length) {
            dic[@"images"] = imagesStr;
        }
        
        [weakSelf.viewModel requestEvaluateWithParams:dic Success:^(NSString *msg, id responseData) {
            [weakSelf showSuccessTip:msg];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        } failure:^(NSString *errorMsg) {
            [weakSelf showSuccessTip:errorMsg];
        }];
    }];
    
  
}
#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    if (kind == UICollectionElementKindSectionHeader) {
        JYEvaluateHeaderView * header = [_myCollection dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"JYEvaluateHeaderView" forIndexPath:indexPath];
        self.header = header;
        [header.shopImageView sd_setImageWithURL:[NSURL URLWithString:self.shopImage] placeholderImage:JY_PLACEHOLDER_IMAGE];
        return header;
    }
    JYOpinionFootCollectionReusableView * foot = [_myCollection dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"JYOpinionFootCollectionReusableView" forIndexPath:indexPath];
    foot.commitBtnActionBlock = ^{
        [weakSelf requestEvaluate];
    };
    return foot;
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //    return self.imageArray.count;
    //暂定最多显示九张
    if (self.imgsArr.count == self.maxImgCount) {
        return self.maxImgCount;
    }
    return self.imgsArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYUploadImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"JYUploadImageCollectionViewCell" forIndexPath:indexPath];
    WEAK(weakSelf)
    //    cell.imageData = self.imageArray[indexPath.row];
//    [cell updateCellModel:self.imgsArr[indexPath.row] atIndexPath:indexPath];
    [cell updateCelWithImgsModel:self.imgsArr[indexPath.row] atIndexPath:indexPath];
    
    cell.addBlock = ^{
        [weakSelf openLibrary];
        //        WKNavigationController *navC = [[WKNavigationController alloc] initWithDelegate:weakSelf];
        //        [navC setShowCount:8];
        //        [weakSelf presentViewController:navC animated:YES completion:nil];
    };
    
    
    cell.delBlock = ^{
        [weakSelf updataViewAfterDeleteImageAtIndex:indexPath.row];
//        [weakSelf.imageArray removeObjectAtIndex:indexPath.row];
//        [weakSelf.myCollection reloadData];
    };
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(SCREEN_WIDTH,179);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(SCREEN_WIDTH,240);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH -10)/4, (SCREEN_WIDTH - 10)/4);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 10, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collecjtionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (void)pickerSelectedImages:(NSArray *)images {
    NSRange range =NSMakeRange(0, [images count]);
    
    [self.imageArray insertObjects:images atIndexes:[NSIndexSet indexSetWithIndexesInRange:range]];
    [self.myCollection reloadData];
}

- (JYUploadImgsTool *)uploadViewModel
{
    if (!_uploadViewModel) {
        _uploadViewModel = [[JYUploadImgsTool alloc] init];
    }
    return _uploadViewModel;
}

- (NSMutableArray *)imgsArr
{
    if (!_imgsArr) {
        _imgsArr = [NSMutableArray array];
    }
    return _imgsArr;
}

- (JYEvaluateViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYEvaluateViewModel alloc] init];
    }
    return _viewModel;
}
- (NSMutableArray<UIImage *> *)picArr
{
    if (!_picArr) {
        _picArr = [NSMutableArray array];
    }
    return _picArr;
}

@end
