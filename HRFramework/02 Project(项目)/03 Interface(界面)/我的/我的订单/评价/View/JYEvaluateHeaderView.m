//
//  JYEvaluateHeaderView.m
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYEvaluateHeaderView.h"


@interface JYEvaluateHeaderView ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;

@property (nonatomic,strong) NSArray *btnArr;

@property (weak, nonatomic) IBOutlet UILabel *placeHolderLab;
@property (weak, nonatomic) IBOutlet UILabel *countLab;
@property (weak, nonatomic) IBOutlet UILabel *tipLab;


@end

@implementation JYEvaluateHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.countLab.hidden = YES;
    self.btnArr = @[_btn1,_btn2,_btn3,_btn4,_btn5];
    self.contentTextView.delegate = self;
}
- (IBAction)starBtnAction:(UIButton *)sender {
    [self changeBtnState:sender andBtnArr:self.btnArr];
}

#pragma  mark -
-(void)changeBtnState:(UIButton *)btn andBtnArr:(NSArray *)BtnArr
{
    for (UIButton *TempBtn in BtnArr) {
        if (TempBtn.tag<=btn.tag) {
            TempBtn.selected = YES;
        }
        else {
            TempBtn.selected = NO;
        }
    }
    int num=(int)btn.tag;
    
    if (btn.tag<=5) {
        self.starScore = num;
    }
    
    [self updataTipLabWithStarNum:self.starScore];
}

#pragma mark ==================textview代理==================

- (void)textViewDidChange:(UITextView *)textView{
    self.placeHolderLab.hidden = textView.text.length ? YES : NO;
//    if (textView.text.length > 50) {
//        textView.text = [textView.text substringToIndex:50];
//    }
//    self.countLab.text = SF(@"%lu/50",(unsigned long)textView.text.length);
}

//（一星（很差）,二星（比较差）,三星（一般）,四星（比较好）,五星（很好））
- (void)updataTipLabWithStarNum:(NSInteger)starNum{
    if (starNum == 1) {
        self.tipLab.text = @"很差";
    }else if (starNum == 2){
        self.tipLab.text = @"比较差";
    }else if (starNum == 3){
        self.tipLab.text = @"一般";
    }else if (starNum == 4){
        self.tipLab.text = @"比较好";
    }else if (starNum == 5){
        self.tipLab.text = @"很好";
    }
}

@end
