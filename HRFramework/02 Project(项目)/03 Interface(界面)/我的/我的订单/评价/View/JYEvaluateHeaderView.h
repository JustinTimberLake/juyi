//
//  JYEvaluateHeaderView.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYEvaluateHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (nonatomic,assign) NSInteger starScore; //星级评分

@property (weak, nonatomic) IBOutlet UIImageView *shopImageView;

@end
