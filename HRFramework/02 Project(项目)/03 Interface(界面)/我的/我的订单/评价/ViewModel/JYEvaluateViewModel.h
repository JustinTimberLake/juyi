//
//  JYEvaluateViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"


@interface JYEvaluateViewModel : JYBaseViewModel

//评价接口
- (void)requestEvaluateWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


@end
