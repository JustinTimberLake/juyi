//
//  JYEvaluateViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/9/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYEvaluateViewModel.h"

@implementation JYEvaluateViewModel
//评价接口
- (void)requestEvaluateWithParams:(NSMutableDictionary *)params Success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_GOODSORDER_Evaluate));
    [manager POST_URL:JY_PATH(JY_GOODSORDER_Evaluate) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
