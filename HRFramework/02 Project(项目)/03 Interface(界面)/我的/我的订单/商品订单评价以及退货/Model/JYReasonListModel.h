//
//  JYReasonListModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYReasonListModel : JYBaseModel
//退货理由ID
@property (nonatomic,copy) NSString *reasonId;
//退货理由内容
@property (nonatomic,copy) NSString *reasonContent;

@property (nonatomic,assign,getter=isSelected) BOOL selected;


@end
