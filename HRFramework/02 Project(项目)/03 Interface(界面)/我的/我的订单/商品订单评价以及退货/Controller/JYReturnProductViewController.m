//
//  JYReturnProductViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYReturnProductViewController.h"
#import "JYBackViewTableViewCell.h"
#import "JYRechargGaosolineSectionHeaderView.h"
#import "JYReturnReasonTableViewCell.h"
#import "WKPickerViewController.h"
#import "JYApplyRefundViewModel.h"
#import "ZLPhotoActionSheet.h"
#import "JYUploadImgsTool.h"
#import "JYImgSelectModel.h"
#import "JYTextViewCell.h"

@interface JYReturnProductViewController ()<
 UITableViewDataSource,
 UITableViewDelegate,
 WKPickerDelegate
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
//@property (strong, nonatomic) NSMutableArray <NSData *>* imageArray;//图片数据

@property (nonatomic,strong) JYApplyRefundViewModel *viewModel;
@property (nonatomic ,assign) NSInteger selectedImgCount;   //已选图片张数
@property (nonatomic ,assign) NSInteger maxImgCount;   //最大图片数
@property (nonatomic,strong) JYUploadImgsTool *uploadViewModel;
@property (nonatomic,strong) NSMutableArray *imgUrlArray;
@property (nonatomic,copy) NSString *imageStr;//图片str
@property (nonatomic,strong) NSMutableArray *imgsArr; //图片的模型数组
@property (nonatomic,copy) NSString *reasonId;
@property (nonatomic,strong) NSArray *refundTypeArr; //退款类型
@property (nonatomic,strong) NSArray *titleArr; //标题数组
//@property (nonatomic,strong) NSMutableArray *priceArr; //价钱数组
@property (nonatomic,copy) NSString *refundPrice; //退款价格
@property (nonatomic,assign) JYRefundPriceType refundPriceType;
@property (nonatomic,assign) NSInteger currentRow;
@property (nonatomic,copy) NSString *commentText;



@end

@implementation JYReturnProductViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
//- (NSMutableArray<NSData*>*)imageArray{
//    if (!_imageArray) {
//        _imageArray = [NSMutableArray array];
//        [_imageArray addObject:[NSData new]];//插入一条加号数据
//    }
//    return _imageArray;
//}
#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self requestGetReason];
    [self configData];
    
    
}

- (void)configData{
    self.maxImgCount = 9;
    self.selectedImgCount = 0;
    self.currentRow = 3;
    
    JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
    model.isAddBtn = YES;
    model.img = nil;
    [self.imgsArr addObject:model];
    
    self.refundTypeArr = @[@"仅退款",@"退货退款"];
    self.titleArr = @[@"服务类型",@"退款金额",@"退货原因",@"",@"上传图片"];
    
//    [self.priceArr removeAllObjects];
//    NSArray * arr = @[@"",@"",@"",@"",@"",];
//    [self.priceArr addObjectsFromArray:arr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"申请退货";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.estimatedRowHeight = 50;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYReturnReasonTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYReturnReasonTableViewCell"];
     [_myTableView registerNib:[UINib nibWithNibName:@"JYBackViewTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYBackViewTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYTextViewCell" bundle:nil] forCellReuseIdentifier:@"JYTextViewCell"];
    
}
#pragma mark networkRequest (网络请求)

//获取退款原因接口
- (void)requestGetReason{
    WEAKSELF
    [self.viewModel requestGetReasonListSuccess:^(NSString *msg, id responseData) {
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

- (void)requestRefundPriceWithOrderId:(NSString *)orderId{
    WEAKSELF
    
    [self.viewModel requestGetReturnPriceWithOrderId:orderId andRefundType:self.refundPriceType success:^(NSString *msg, id responseData) {
        NSString *str = responseData;
//        NSLog(@"%@",str);
        weakSelf.refundPrice = str;
        [weakSelf.myTableView reloadData];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//提交申请退货接口
- (void)requestApplyRefund{
    WEAKSELF
    if (self.refundPriceType == 3) {
        [self showSuccessTip:@"请选择服务类型"];
        return;
    }
    if (!self.reasonId.length) {
        [self showSuccessTip:@"请选择退货原因"];
        return;
    }
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderId"] =  self.orderId;
    dic[@"reasonId"] = self.reasonId;
    dic[@"content"] = self.commentText;
    dic[@"refundType"] = @(self.refundPriceType);
    dic[@"returnPrice"] = self.refundPrice;
    dic[@"images"] = self.imageStr;
     [self.viewModel requestApplyRefundWithParams:dic success:^(NSString *msg, id responseData) {
         [weakSelf showSuccessTip:msg];
         [weakSelf.navigationController popViewControllerAnimated:YES];
     } failure:^(NSString *errorMsg) {
         [weakSelf showSuccessTip:errorMsg];
     }];
}
#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

- (void)commitApplyRefund:(UIButton *)btn{
    [self requestApplyRefund];
}

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark ==================相册==================
//打开相册
- (void)openLibrary{
    WEAKSELF
    ZLPhotoActionSheet *picker = [[ZLPhotoActionSheet alloc] init];
    picker.maxSelectCount = self.maxImgCount-self.selectedImgCount;
    picker.maxPreviewCount = self.maxImgCount;
    
    [picker showPhotoLibraryWithSender:self
                 lastSelectPhotoModels:nil
                            completion:^(NSArray<UIImage *> * _Nonnull selectPhotos, NSArray<ZLSelectPhotoModel *> * _Nonnull selectPhotoModels) {
                                //                                hideVC();
                                
                                [weakSelf requestUpLoadImageWithArr: [selectPhotos mutableCopy]];
                            }];
}
- (void)requestUpLoadImageWithArr:(NSMutableArray *)imageArr{
    WEAKSELF    
    [self.uploadViewModel UploadImgs:imageArr];
    self.uploadViewModel.CompliteBlock = ^(NSMutableArray *imgStrArr ,NSMutableArray *imageHttpArr, NSMutableArray *failArrIndex) {
        if (failArrIndex.count ) {
            [weakSelf showSuccessTip:@"上传失败"];
        }else{
            weakSelf.imgUrlArray =  [imageHttpArr mutableCopy];
            
            NSMutableString *imgStr = [NSMutableString string];
            [imgStrArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                if (idx == 0) {
                    [imgStr appendString:obj];
                }else{
                    [imgStr appendFormat:@",%@",obj];
                }
            }];
            weakSelf.imageStr = [imgStr copy];
            [weakSelf updateViewAfterAddImgs:weakSelf.imgUrlArray];
            //            [weakSelf requestUpLoadImageWithImageStr:imgStr];
            
        }
    };
}

- (void)updateViewAfterAddImgs:(NSArray<NSString *>*)imgs{
    //    NSArray * imageArr = [imagUrl componentsSeparatedByString:@","];
    //    NSMutableArray * endImageArr = [NSMutableArray array];
    //
    //    for (NSString * url in imageArr) {
    //        if ([url hasPrefix:@"http"]) {
    //            [endImageArr addObject:url];
    //        }else{
    //            NSString * httpUrl ;
    //            httpUrl = SF(@"%@%@",JY_HTTP_PREFIX,url);
    //            [endImageArr addObject:httpUrl];
    //        }
    
    for (NSString *imageUrl in imgs) {
        if (self.selectedImgCount == self.maxImgCount-1) {
            JYImgSelectModel *model = self.imgsArr.lastObject;
            model.isAddBtn = NO;
            //            model.img = tpImg;
            model.imageUrl = imageUrl;
            self.selectedImgCount++;
        }
        else
        {
            JYImgSelectModel *model = [[JYImgSelectModel alloc] init];
            model.isAddBtn = NO;
            //            model.img = tpImg;
            model.imageUrl = imageUrl;
            [self.imgsArr insertObject:model atIndex:self.selectedImgCount];
            self.selectedImgCount++;
        }
    }
    [self.myTableView reloadData];
}



#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }else if (section == 1){
        return 0;
    }else if (section == 2) {
        return self.viewModel.listArr.count;;
    }else if (section == 3) {
        return 1;
    }else{
        return 1;
    }
    return self.viewModel.listArr.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
     JYReturnReasonTableViewCell *reasonCell = [tableView dequeueReusableCellWithIdentifier:@"JYReturnReasonTableViewCell"] ;
    
    reasonCell.selectionStyle = 0;
    
    JYTextViewCell * textViewCell = [tableView dequeueReusableCellWithIdentifier:@"JYTextViewCell"];
    
    if (indexPath.section == 0) {
        if (self.currentRow == indexPath.row) {
            [reasonCell cellSelect];
        }else{
            [reasonCell cellUnSelect];
        }

        [reasonCell updataTitleWithData:self.refundTypeArr IndexPath:indexPath];
        return reasonCell;
    }else if (indexPath.section == 1){
        return nil;
    }else if(indexPath.section == 2){
        reasonCell.model = self.viewModel.listArr[indexPath.row];
        return reasonCell;
    }else if (indexPath.section == 3){
        if (self.commentText.length) {
            textViewCell.textView.text = self.commentText;
            textViewCell.placeHolderLab.hidden = YES;
        }
        textViewCell.returnTextViewTextBlock = ^(NSString *text) {
            self.commentText = text;
        };
        return textViewCell;
    } else if (indexPath.section == 4) {
        JYBackViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYBackViewTableViewCell"] ;
        WEAK(weakSelf)
//        cell.imageArray = self.imageArray;
        cell.imageArr = self.imgsArr;
        cell.delImagBlock = ^(NSInteger index) {
//            [weakSelf.imageArray removeObjectAtIndex:index];
//            [weakSelf.myTableView reloadData];
        };
        
        cell.upLoadBlock = ^{
//            WKNavigationController *navC = [[WKNavigationController alloc] initWithDelegate:weakSelf];
//            [navC setShowCount:8];
//            [weakSelf presentViewController:navC animated:YES completion:nil];
            [weakSelf openLibrary];
        };
        return cell;
    }
   

    return reasonCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 50;
    }else if(indexPath.section == 1){
        return 0;
    }else if (indexPath.section == 2){
        return 50;
    }else if (indexPath.section == 3){
        return 135;
    }else if (indexPath.section == 4){
        float height = SCREEN_WIDTH/4;
        NSInteger cout = self.imgsArr.count ;
        return (cout+4-1)/4 * height + 20;
    }
    return 50;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 3) {
        return 0.01;
    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 4) {
        return 20+60;
    }
    return 9;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    JYRechargGaosolineSectionHeaderView *headerSectionView = XIB(JYRechargGaosolineSectionHeaderView);
    
    headerSectionView.titleLabel.text = self.titleArr[section];
    
//    headerSectionView.rightPriceLab.text = self.priceArr[section];
//    if (section == 1) {
//        headerSectionView.rightPriceLab.hidden = NO;
//        headerSectionView.rightPriceLab.text = self.viewModel.refundPrice.length ? SF(@"%@",self.viewModel.refundPrice) :@"";
//    }else{
//        headerSectionView.rightPriceLab.text = @"";
//    }
    
    if (section == 3) {
        return nil;
    }else{
        if (section == 1) {
            headerSectionView.rightPriceLab.hidden = NO;
            if (self.refundPrice.length) {
                headerSectionView.rightPriceLab.text  = SF(@"￥%@",self.refundPrice);
            }else{
                headerSectionView.rightPriceLab.text  = @"";
            }
//            headerSectionView.rightPriceLab.text = self.refundPrice.length ?SF(@"￥%@",self.refundPrice):@"";
        }
        return headerSectionView;
        
    }
        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    
    if (indexPath.section == 0) {

        JYReturnReasonTableViewCell *lastCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentRow inSection:0]];
        [lastCell cellUnSelect];

        JYReturnReasonTableViewCell * newCell = [tableView cellForRowAtIndexPath:indexPath];
        self.currentRow = indexPath.row;
        [newCell cellSelect];
        
    
        self.refundPriceType = (indexPath.row == 0) ? JYRefundPriceType_OnlyMoney :JYRefundPriceType_MoneyAndGoods;
        
        [self requestRefundPriceWithOrderId:self.orderId];
    }
    if (indexPath.section == 2) {
        [self.viewModel.listArr enumerateObjectsUsingBlock:^(JYReasonListModel *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (idx == indexPath.row) {
                obj.selected = YES;
                weakSelf.reasonId = obj.reasonId;
            }else{
                obj.selected = NO;
            }
        }];
        [self.myTableView reloadData];
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 4) {
        UIView * view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
        [btn addTarget:self action:@selector(commitApplyRefund:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:@"提交申请" forState:UIControlStateNormal];
        [btn setTintColor:[UIColor whiteColor]];
        btn.frame = CGRectMake(28, 20, SCREEN_WIDTH - 28*2, 49);
        btn.layer.cornerRadius = 6;
        [view addSubview:btn];
        return view;
 
    }
    return nil;
   }


//- (void)pickerSelectedImages:(NSArray *)images {
//    NSRange range =NSMakeRange(0, [images count]);
//   
//    [self.imageArray insertObjects:images atIndexes:[NSIndexSet indexSetWithIndexesInRange:range]];
//    [self.myTableView reloadData];
//}


- (JYApplyRefundViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[JYApplyRefundViewModel alloc] init];
    }
    return _viewModel;
}

- (JYUploadImgsTool *)uploadViewModel
{
    if (!_uploadViewModel) {
        _uploadViewModel = [[JYUploadImgsTool alloc] init];
    }
    return _uploadViewModel;
}

- (NSMutableArray *)imgsArr
{
    if (!_imgsArr) {
        _imgsArr = [NSMutableArray array];
    }
    return _imgsArr;
}

//- (NSMutableArray *)priceArr
//{
//    if (!_priceArr) {
//        _priceArr = [NSMutableArray array];
//    }
//    return _priceArr;
//}


@end
