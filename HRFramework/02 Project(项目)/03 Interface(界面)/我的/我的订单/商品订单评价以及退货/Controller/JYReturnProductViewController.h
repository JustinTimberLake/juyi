//
//  JYReturnProductViewController.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYReturnProductViewController : JYBaseViewController
@property (nonatomic,copy) NSString *orderId;
@end
