//
//  JYReturnReasonTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYReasonListModel.h"
#import "JYLogisticsListModel.h"

@interface JYReturnReasonTableViewCell : UITableViewCell

@property (nonatomic,strong) JYReasonListModel *model;

@property (nonatomic,strong) JYLogisticsListModel *LogisticsModel;


//根据数据更新title 数据
- (void)updataTitleWithData:(NSArray *)data IndexPath:(NSIndexPath *)indexPath;

//更新cell 的选中情况 （适用于没有模型的情况）
//- (void)updataCellSelectUIWithIndexPath:(NSIndexPath *)indexPath;

- (void)cellSelect;

- (void)cellUnSelect;
@end
