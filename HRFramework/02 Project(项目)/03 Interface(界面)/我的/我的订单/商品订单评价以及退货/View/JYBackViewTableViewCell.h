//
//  JYBackViewTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^uploadImageBlock)();
typedef void (^delBlock)(NSInteger);
@interface JYBackViewTableViewCell : UITableViewCell<
  UICollectionViewDataSource,
  UICollectionViewDelegate

>

@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
//@property (strong, nonatomic) NSMutableArray <NSData *>* imageArray;//图片数组
@property (nonatomic,strong) NSMutableArray *imageArr; //图片数组
@property (copy, nonatomic) uploadImageBlock upLoadBlock;
@property (copy, nonatomic) delBlock delImagBlock;
@end
