//
//  JYUploadImageCollectionViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUploadImageCollectionViewCell.h"
#import "JYImgSelectModel.h"

@interface JYUploadImageCollectionViewCell ()
@property (nonatomic,assign) NSIndexPath *currentIndexPath;
@property (weak, nonatomic) IBOutlet UIImageView *deleteImageView;
@end

@implementation JYUploadImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)delBtn:(id)sender {
    if (self.delBlock) {
        self.delBlock();
    }
}

- (IBAction)UploadImage:(id)sender {
    if (self.addBlock) {
        self.addBlock();
    }
}

//- (void)setImageData:(NSData *)imageData {
//    _imageData = imageData;
//    if (imageData.length > 0) {
//        self.imageView.image = [UIImage imageWithData:imageData];
//        self.upLoadBtn.hidden = YES;
//        self.imageView.hidden = NO;
//        self.delBtn.hidden = NO;
//    }else{
//        self.upLoadBtn.hidden = NO;
//        self.imageView.hidden = YES;
//        self.delBtn.hidden = YES;
//        
//    }
//}
//里面包含着imageUrl
- (void)updateCellModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath
{
    self.currentIndexPath = indexPath;
    
    if (model.isAddBtn) {
        self.imageView.image = [UIImage imageNamed:@"加图片+"];
        self.delBtn.hidden = YES;
        self.deleteImageView.hidden = YES;
        self.upLoadBtn.hidden = NO;
    }
    else
    {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl] placeholderImage:JY_PLACEHOLDER_IMAGE];
        //        self.imgView.image = model.img;
        self.delBtn.hidden = NO;
        self.deleteImageView.hidden = NO;
        self.upLoadBtn.hidden = YES;
    }
}

//里面包含着img
- (void)updateCelWithImgsModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath{
    self.currentIndexPath = indexPath;
    
    if (model.isAddBtn) {
        self.imageView.image = [UIImage imageNamed:@"加图片+"];
        self.delBtn.hidden = YES;
        self.deleteImageView.hidden = YES;
        self.upLoadBtn.hidden = NO;
    }
    else
    {
        UIImage *img =  model.img;
        self.imageView.image = img;
        self.delBtn.hidden = NO;
        self.deleteImageView.hidden = NO;
        self.upLoadBtn.hidden = YES;
    }
}

@end
