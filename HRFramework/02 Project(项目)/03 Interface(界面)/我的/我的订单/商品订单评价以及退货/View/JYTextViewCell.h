//
//  JYTextViewCell.h
//  JY
//
//  Created by Duanhuifen on 2017/12/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYTextViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLab;


@property (nonatomic,copy) void(^returnTextViewTextBlock)(NSString * text);

@end
