//
//  JYUploadImageCollectionViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYImgSelectModel;
typedef void (^addImageBlock)();
typedef void (^delImageBlock)();
@interface JYUploadImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *delBtn;
@property (weak, nonatomic) IBOutlet UIButton *upLoadBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSData *imageData;
@property (assign, nonatomic) BOOL isFist;
@property (copy, nonatomic) delImageBlock delBlock;
@property (copy, nonatomic) addImageBlock addBlock;

- (void)updateCellModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath;

- (void)updateCelWithImgsModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath;


@end
