//
//  JYOpinionFootCollectionReusableView.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOpinionFootCollectionReusableView.h"

@implementation JYOpinionFootCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    self.button.layer.cornerRadius = 3;
    self.button.layer.masksToBounds = YES;
}
- (IBAction)commitBtnAction:(UIButton *)sender {
    if (_commitBtnActionBlock) {
        self.commitBtnActionBlock();
    }
}

@end
