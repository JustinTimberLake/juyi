//
//  JYOpinionCollectionReusableView.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYOpinionCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@end
