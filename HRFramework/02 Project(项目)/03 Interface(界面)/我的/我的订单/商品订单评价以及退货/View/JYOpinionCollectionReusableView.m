//
//  JYOpinionCollectionReusableView.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOpinionCollectionReusableView.h"

@interface JYOpinionCollectionReusableView ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLab;

@end

@implementation JYOpinionCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentTextView.delegate = self;
}

#pragma mark ==================textview代理==================

- (void)textViewDidChange:(UITextView *)textView{
    self.placeHolderLab.hidden = textView.text.length ? YES : NO;
}


@end
