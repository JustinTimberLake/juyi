//
//  JYReturnReasonTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYReturnReasonTableViewCell.h"


@interface JYReturnReasonTableViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
@implementation JYReturnReasonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setModel:(JYReasonListModel *)model{
    _model = model;
    self.titleLab.text = model.reasonContent;
    self.selectBtn.selected = model.isSelected ? YES : NO;
}

- (void)setLogisticsModel:(JYLogisticsListModel *)LogisticsModel{
    _LogisticsModel = LogisticsModel;
    self.titleLab.text = LogisticsModel.logisticsTitle;
    self.selectBtn.selected = LogisticsModel.isSelected ? YES : NO;
}

//根据数据更新title 数据
- (void)updataTitleWithData:(NSArray *)data IndexPath:(NSIndexPath *)indexPath;{
    if (indexPath.row == 0) {
        self.titleLab.text = data[0];
    }else{
        self.titleLab.text = data[1];
    }
}


////更新cell 的选中情况 （适用于没有模型的情况）
//- (void)updataCellSelectUIWithIndexPath:(NSIndexPath *)indexPath{
//    self.selectBtn.selected = NO;
//    self.selectBtn.selected = (indexPath == self.indexPath) ?YES : NO;
//    
//}


- (void)cellSelect{
    self.selectBtn.selected = YES;
}

- (void)cellUnSelect{
    self.selectBtn.selected = NO;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
