//
//  JYOpinionFootCollectionReusableView.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/17.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYOpinionFootCollectionReusableView : UICollectionReusableView
//按钮点击
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic,copy) void(^commitBtnActionBlock)();
@end
