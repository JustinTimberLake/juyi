//
//  JYBackViewTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBackViewTableViewCell.h"
#import "JYUploadImageCollectionViewCell.h"
@implementation JYBackViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.myCollection.delegate = self;
    self.myCollection.dataSource = self;
    [self registerCollcetionViewCell];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)setImageArray:(NSMutableArray<NSData *> *)imageArray{
//    _imageArray = imageArray;
//    [self.myCollection reloadData];
//}

- (void)setImageArr:(NSMutableArray *)imageArr{
    _imageArr = imageArr;
    [self.myCollection reloadData];
}

- (void)registerCollcetionViewCell{
    [self.myCollection registerNib:[UINib nibWithNibName:@"JYUploadImageCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"JYUploadImageCollectionViewCell"];
}
#pragma mark - ---------- CollectionView Delegate && DataSource && FlowLayout ----------
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return self.imageArray.count;
    return  self.imageArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    JYUploadImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"JYUploadImageCollectionViewCell" forIndexPath:indexPath];
        WEAK(weakSelf)
//        cell.imageData = self.imageArray[indexPath.row];
    [cell updateCellModel:self.imageArr[indexPath.row] atIndexPath:indexPath];

        cell.addBlock = ^{
            weakSelf.upLoadBlock();
        
        };

        
        cell.delBlock = ^{
            weakSelf.delImagBlock(indexPath.row);
        };

    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((SCREEN_WIDTH -10)/4, (SCREEN_WIDTH - 10)/4);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collecjtionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


@end
