//
//  JYApplyRefundViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

typedef NS_ENUM(NSInteger,JYApplyRefundType) {
    JYApplyRefundType_Service = 1,//服务
    JYApplyRefundType_ShiTi,//实体
    JYApplyRefundType_Product //商品
};


typedef NS_ENUM(NSInteger,JYRefundPriceType) {
    JYRefundPriceType_OnlyMoney = 1, //退款
    JYRefundPriceType_MoneyAndGoods //退货退款
};

@interface JYApplyRefundViewModel : JYBaseViewModel

@property (nonatomic,strong) NSMutableArray *listArr;

@property (nonatomic,copy) NSString *refundPrice; //退款的钱
//获取退货理由
- (void)requestGetReasonListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//退款 商品订单
- (void)requestApplyRefundWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//未发货情况下退款 (实体，商品，服务)
- (void)requestApplyRefundWithNotSendProductWithOrderId:(NSString *)orderId andType:(JYApplyRefundType)applyRefundType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//获取退款价格
- (void)requestGetReturnPriceWithOrderId:(NSString *)orderId andRefundType:(JYRefundPriceType)refundPriceType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;



@end
