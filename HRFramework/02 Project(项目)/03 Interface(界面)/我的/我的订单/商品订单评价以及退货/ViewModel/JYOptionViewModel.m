//
//  JYOptionViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/9/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYOptionViewModel.h"

@implementation JYOptionViewModel
//意见反馈接口
- (void)requestFeedbackWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_FEEDBACK_feedback));
    [manager POST_URL:JY_PATH(JY_FEEDBACK_feedback) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}
@end
