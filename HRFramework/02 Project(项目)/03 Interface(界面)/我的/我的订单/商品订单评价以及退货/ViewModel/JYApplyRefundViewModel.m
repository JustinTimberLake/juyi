//
//  JYApplyRefundViewModel.m
//  JY
//
//  Created by Duanhuifen on 2017/9/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYApplyRefundViewModel.h"
#import "JYReasonListModel.h"

@implementation JYApplyRefundViewModel
//获取退货理由
- (void)requestGetReasonListSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_GOODSORDER_GetReasonList) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            NSArray * arr = [JYReasonListModel mj_objectArrayWithKeyValuesArray:RESULT_DATA ];
            [weakSelf.listArr addObjectsFromArray:arr];
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//退款 商品订单
- (void)requestApplyRefundWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_GOODSORDER_ApplyRefund) params:params success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

//未发货情况下退款 (实体，商品，服务)
- (void)requestApplyRefundWithNotSendProductWithOrderId:(NSString *)orderId andType:(JYApplyRefundType)applyRefundType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderId"] = orderId;
    dic[@"type"] = @(applyRefundType);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_PAY_ApplyRefund));
    [manager POST_URL:JY_PATH(JY_PAY_ApplyRefund) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
            successBlock(RESULT_MESSAGE,nil);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];

}

//获取退款价格
- (void)requestGetReturnPriceWithOrderId:(NSString *)orderId andRefundType:(JYRefundPriceType)refundPriceType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock{
    WEAKSELF
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[@"C"] = [User_InfoShared shareUserInfo].c;
    dic[@"orderId"] = orderId;
    dic[@"refundType"] = @(refundPriceType);
    HRRequestManager * manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_GOODSORDER_GetReturnPrice));
    [manager POST_URL:JY_PATH(JY_GOODSORDER_GetReturnPrice) params:dic success:^(id result) {
        NSLog(@"%@",RESULT_DATA);
        if (RESULT_SUCCESS) {
//            weakSelf.refundPrice = RESULT_DATA[@"returnPrice"];
            
            NSString * str = SF(@"%@",result[@"data"][@"returnPrice"]) ;
            successBlock(RESULT_MESSAGE,str);
        }else{
            failureBlock(RESULT_MESSAGE);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}


- (NSMutableArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}


@end
