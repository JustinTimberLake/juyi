//
//  JYOptionViewModel.h
//  JY
//
//  Created by Duanhuifen on 2017/9/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYOptionViewModel : JYBaseViewModel

//意见反馈接口
- (void)requestFeedbackWithParams:(NSMutableDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;




@end
