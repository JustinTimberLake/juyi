//
//  JYMyOrderModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/8/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
@class JYMyProductOrderModel;
@interface JYMyOrderModel : JYBaseModel
/*orderId://订单号
 shopId://加油站ID
 shopTitle:// 加油站标题
 shopImage:// 加油站图片
 gunNum://枪号
 oilModel://型号
 oilPrice://单价
 oilNum://油量
 orderState:// 1待支付2待使用3待评价4已完成5已关闭
 totalPrice://总价
*/
@property (nonatomic, copy)NSString *orderId;
@property (nonatomic, copy)NSString *shopId;
@property (nonatomic, copy)NSString *shopTitle;
@property (nonatomic, copy)NSString *shopImage;
@property (nonatomic, copy)NSString *gunNum;
@property (nonatomic, copy)NSString *oilModel;
@property (nonatomic, copy)NSString *oilPrice;
@property (nonatomic, copy)NSString *oilNum;
@property (nonatomic, copy)NSString *orderState;
@property (nonatomic, copy)NSString *totalPrice;
@property (nonatomic, strong)NSMutableArray <JYMyProductOrderModel *>* goods;
@end

@interface JYMyProductOrderModel : JYBaseModel
/*        goods =         (
 {
 goodsId = 1;
 goodsImage = "https://image3.pengfu.com/origin/170712/5965c37801e5f.jpg";
 goodsNum = 3;
 goodsTitle = "\U5546\U54c1\U6807\U9898";
 item1name = "\U989c\U8272";
 item1value = "\U7ea2\U8272";
 item2name = "\U5c3a\U7801";
 item2value = M;
 */
@property (nonatomic, copy)NSString *goodsId;
@property (nonatomic, copy)NSString *goodsImage;
@property (nonatomic, copy)NSString *goodsNum;
@property (nonatomic, copy)NSString *goodsTitle;
@property (nonatomic, copy)NSString *item1name;
@property (nonatomic, copy)NSString *item1value;
@property (nonatomic, copy)NSString *item2name;
@property (nonatomic, copy)NSString *item2value;
//商品单价
@property (nonatomic,copy) NSString *productPrice;
//运费
@property (nonatomic,copy) NSString *freight;

@property (nonatomic,copy) NSString *qrcode;//二维码

@end

