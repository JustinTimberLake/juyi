//
//  JYServiceOrderInfoModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"

@interface JYServiceOrderInfoModel : JYBaseModel
//订单号
@property (nonatomic,copy) NSString * orderId;
//加油站ID
@property (nonatomic,copy) NSString * shopId;
// 加油站标题
@property (nonatomic,copy) NSString * shopTitle;
// 加油站图片
@property (nonatomic,copy) NSString *shopImage ;
//枪号
@property (nonatomic,copy) NSString * gunNum;
//型号
@property (nonatomic,copy) NSString * oilModel;
//单价
@property (nonatomic,copy) NSString * oilPrice;
//油量
@property (nonatomic,copy) NSString *oilNum ;
// 1待支付2待使用3待评价4已完成5已关闭
@property (nonatomic,copy) NSString * orderState;
//总价
@property (nonatomic,copy) NSString * totalPrice;

@end
