   //
//  JYMyOrderModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/8/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyOrderModel.h"

@implementation JYMyOrderModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"goods":@"JYMyProductOrderModel"};
}

@end

@implementation JYMyProductOrderModel

@end

