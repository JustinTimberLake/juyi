//
//  JYServiceOrderModel.h
//  JY
//
//  Created by Duanhuifen on 2017/12/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYServiceOrderInfoModel.h"
#import "JYMyOrderModel.h"

@interface JYMyServiceOrderModel : JYBaseModel
//1服务订单2实体服务订单
@property (nonatomic,copy)NSString * orderType;

//服务订单信息
//暂时注释，看结果是否可以
//@property (nonatomic,strong) JYServiceOrderInfoModel *serviceOrderInfo;
@property (nonatomic,strong) JYMyOrderModel *serviceOrderInfo;


//实体服务订单信息
@property (nonatomic,strong) JYMyOrderModel *substantialityOrderInfo;


@end
