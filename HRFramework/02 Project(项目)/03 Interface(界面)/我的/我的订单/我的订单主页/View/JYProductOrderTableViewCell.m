//
//  JYProductOrderTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductOrderTableViewCell.h"

@interface JYProductOrderTableViewCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shopTitleW;

@end


@implementation JYProductOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(JYMyOrderModel *)model{
    _model = model;
    self.shopName.text = _model.shopTitle;
  
    CGFloat width = [NSString widthOfText:model.shopTitle textHeight:MAXFLOAT font:FONT(14)];

    self.shopTitleW.constant = width > SCREEN_WIDTH - 270 ?  SCREEN_WIDTH - 270 :width;
    
    _orderNumLB.text = [_model.orderId placeholder:@"暂无"];
    _totlePriceLB.text = [[NSString stringWithFormat:@"%.2lf",[_model.totalPrice floatValue]] placeholder:@"暂无"];
    _product.text = [_model.goods[0].goodsTitle placeholder:@"暂无"];
    
    self.item1TitleLab.text = SF(@"%@:%@",_model.goods[0].item1name,_model.goods[0].item1value);
    self.item2TitleLab.text = SF(@"%@:%@",_model.goods[0].item2name,_model.goods[0].item2value);
    self.countTitleLab.text = SF(@"数量:x%@",_model.goods[0].goodsNum);
    [self.productIM sd_setImageWithURL:[NSURL URLWithString:_model.goods[0].goodsImage] placeholderImage:DEFAULT_COURSE];
    self.totleCountLB.text = SF(@"共%@件",_model.goods[0].goodsNum);
    
    self.item1TitleLab.hidden = YES;
    self.item2TitleLab.hidden = YES;
    if (model.goods[0].item1name.length) {
        self.item1TitleLab.hidden = NO;
        self.item1TitleLab.text = SF(@"%@:%@",model.goods[0].item1name,model.goods[0].item1value);
    }
    
    if (model.goods[0].item2name.length) {
        self.item2TitleLab.hidden = NO;
        self.item2TitleLab.text = SF(@"%@:%@",model.goods[0].item2name,model.goods[0].item2value);
    }
    
//    是否有通用模式暂不清楚，勿删
//    if ([_model.orderState isEqualToString:@"1"]) {
//        self.statusLB.text = @"待支付";
//    }else if ([_model.orderState isEqualToString:@"2"]){
//        self.statusLB.text = @"待使用";
//    }else if ([_model.orderState isEqualToString:@"3"]){
//        self.statusLB.text = @"待评价";
//    }else if ([_model.orderState isEqualToString:@"4"]){
//        self.statusLB.text = @"已完成";
//    }else if ([_model.orderState isEqualToString:@"5"]){
//        self.statusLB.text = @"已关闭";
//    }
    [self updataMyProductOrderBtnUIWithOrderState:model.orderState];
}

//商品订单模式下状态按钮ui
//最新状态：1待付款2取消订单3待发货4已退款5已发货6已收货7待评价8退货中9已退货10退款失败11已关闭12已完成13  退货审核中14  退货审成功 15退货失败，16退货成功
- (void)updataMyProductOrderBtnUIWithOrderState:(NSString *)orderState{
    switch ([orderState intValue]) {
        case 1:{
//            self.statusLB.text = @"待支付";
            [self.rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
            [self.leftBtn setTitle:@"取消订单" forState:UIControlStateNormal];
            
        }
            break;
        case 3:{
//            self.statusLB.text = @"待发货";
            [self.rightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
        }
            break;
        case 4:{
//            self.statusLB.text = @"已退款";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        case 5:{
//            self.statusLB.text = @"待收货";
            [self.leftBtn setTitle:@"申请退货" forState:UIControlStateNormal];
            [self.rightBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        }
            break;
        case 6: case 7:{
//            self.statusLB.text = @"待评价";
            [self.leftBtn setTitle:@"申请退货" forState:UIControlStateNormal];
            [self.rightBtn setTitle:@"去评价" forState:UIControlStateNormal];
        }
            break;
            
        case 8:{
//            self.statusLB.text = @"退货中";
            [self.rightBtn setTitle:@"退货中" forState:UIControlStateNormal];
        }
            break;
            
        case 11:case 2:{
//            self.statusLB.text = @"已关闭";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;

            
        case 12:{
//            self.statusLB.text = @"已完成";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        case 13:{
//            self.statusLB.text = @"退货审核中";
            [self.rightBtn setTitle:@"审核中" forState:UIControlStateNormal];
        }
            break;
        case 14:{
//            self.statusLB.text = @"退货审核通过";
            [self.rightBtn setTitle:@"    填写退货信息    " forState:UIControlStateNormal];
        }
            break;
            
        case 15:{
//            self.statusLB.text = @"退货失败";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
            
        case 16:{
//            self.statusLB.text = @"已退货";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
    self.leftBtn.hidden = ([orderState intValue] == 1 || [orderState intValue] == 6||[orderState intValue] == 5  ||[orderState intValue] == 7 ) ? NO : YES;
    
    self.rightBtn.hidden = ([orderState intValue] == 8 ||[orderState intValue] == 13 ) ? YES : NO;
    
    if ([orderState intValue] == 1 ||[orderState intValue] == 5 ||[orderState intValue] == 3||[orderState intValue] == 6||[orderState intValue] == 7) {
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"黄色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }else{
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
    }
}


#pragma mark ==================实体模式==================
- (void)setShitiModel:(JYMyOrderModel *)shitiModel{
    _shitiModel = shitiModel;
    self.shopName.text = shitiModel.shopTitle ;
    _orderNumLB.text = [shitiModel.orderId placeholder:@"暂无"];
    _totlePriceLB.text = [[NSString stringWithFormat:@"%.2lf",[shitiModel.totalPrice floatValue]] placeholder:@"暂无"];
    _product.text = [shitiModel.goods[0].goodsTitle placeholder:@"暂无"];
    [self.productIM sd_setImageWithURL:[NSURL URLWithString:shitiModel.goods[0].goodsImage] placeholderImage:DEFAULT_COURSE];
    [self updataMyServiceOrderBtnUIWithOrderState:shitiModel.orderState];
    self.countTitleLab.hidden = YES;
    
    self.item1TitleLab.hidden = YES;
    self.item2TitleLab.hidden = YES;
    if (shitiModel.goods[0].item1name.length) {
        self.item1TitleLab.hidden = NO;
        self.item1TitleLab.text = SF(@"%@:%@",shitiModel.goods[0].item1name,shitiModel.goods[0].item1value);
    }
    
    if (shitiModel.goods[0].item2name.length) {
        self.item2TitleLab.hidden = NO;
        self.item2TitleLab.text = SF(@"%@:%@",shitiModel.goods[0].item2name,shitiModel.goods[0].item2value);
    }
    [self updataMyServiceOrderBtnUIWithOrderState:shitiModel.orderState];
    
}


//服务订单模式下按钮ui
- (void)updataMyServiceOrderBtnUIWithOrderState:(NSString *)orderState{
    switch ([orderState intValue]) {
        case 1:{
//            self.statusLB.text = @"待支付";
            [self.rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
        }
            break;
        case 2:{
//            self.statusLB.text = @"待使用";
            [self.rightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
        }
            break;
        case 3:{
//            self.statusLB.text = @"待评价";
            [self.rightBtn setTitle:@"去评价" forState:UIControlStateNormal];
        }
            break;
        case 4:{
//            self.statusLB.text = @"已完成";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        case 5:case 6:{
//            self.statusLB.text = @"已关闭";
            [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;

        default:
            break;
    }
    
    self.leftBtn.hidden = [orderState intValue] == 1 ? NO : YES;
    if ([orderState intValue] == 1 ||[orderState intValue] == 2||[orderState intValue] == 3) {
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"黄色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
        
    }
}


- (IBAction)btnAction:(UIButton *)sender {
    if (_bottomBtnActionBlock) {
        self.bottomBtnActionBlock(sender);
    }
}


@end
