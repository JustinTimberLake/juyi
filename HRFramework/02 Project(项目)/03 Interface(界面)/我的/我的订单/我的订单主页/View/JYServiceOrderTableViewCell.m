//
//  JYServiceOrderTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYServiceOrderTableViewCell.h"

@implementation JYServiceOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}
- (void)setType:(OrderPayStatusType)type{
    if (type == StatusInvoice) {
//        self.payStatusLabel.hidden = YES;
        self.leftBtn.hidden = YES;
        [self.rightBtn setTitle:@"已申请发票" forState:UIControlStateNormal];
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
        self.totalLabel.text = @"";
    }else{
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"黄色矩形按钮"] forState:UIControlStateNormal];
    }
}

- (void)setInvoiceModel:(JYInvoiceModel *)invoiceModel{
    _invoiceModel = invoiceModel;
    [self.stationNameLabel setTitle:invoiceModel.shopTitle forState:UIControlStateNormal];
//    [self.stationNameLabel setImage:[UIImage imageNamed:invoiceModel.shopImage] forState:UIControlStateNormal];
    self.orderNumLabel.text = invoiceModel.orderId;
    [self.image sd_setImageWithURL:[NSURL URLWithString:invoiceModel.shopImage] placeholderImage:JY_PLACEHOLDER_IMAGE];
    self.guns.text = SF(@"%@油枪",invoiceModel.gunNum);
    self.gasolineType.text = SF(@"%@",invoiceModel.oilModel);
    _priceLB.text =  [[NSString stringWithFormat:@"%@元/升",invoiceModel.oilPrice] placeholder:@"暂无"];
    _addTotalLB.text =  [[NSString stringWithFormat:@"%@升",invoiceModel.oilNum] placeholder:@"暂无"];
    _priceLabel.text = SF(@"%@",invoiceModel.totalPrice);
}
/*
 /*orderId://订单号
 shopId://加油站ID
 shopTitle:// 加油站标题
 shopImage:// 加油站图片
 gunNum://枪号
 oilModel://型号
 oilPrice://单价
 oilNum://油量
 orderState:// 1待支付2待使用3待评价4已完成5已关闭
 totalPrice://总价
 */


- (void)setModel:(JYMyOrderModel *)model{
    _model = model;
    [_stationNameLabel setTitle:_model.shopTitle forState:UIControlStateNormal];
    _orderNumLabel.text = [_model.orderId placeholder:@"暂无"];
    _priceLabel.text = [[NSString stringWithFormat:@"%.2lf",[_model.totalPrice floatValue]] placeholder:@"暂无"];
    _guns.text = [[NSString stringWithFormat:@"%@油枪",_model.gunNum] placeholder:@"暂无"];
    _gasolineType.text = [[NSString stringWithFormat:@"%@",_model.oilModel] placeholder:@"暂无"];
    _priceLB.text =  [[NSString stringWithFormat:@"%@元/升",_model.oilPrice] placeholder:@"暂无"];
    _addTotalLB.text =  [[NSString stringWithFormat:@"%@升",_model.oilNum] placeholder:@"暂无"];
     [self.image sd_setImageWithURL:[NSURL URLWithString:_model.shopImage] placeholderImage:DEFAULT_COURSE];
    [self updataMyServiceOrderBtnUIWithOrderState:model.orderState];
    
}

//服务订单模式下按钮ui
- (void)updataMyServiceOrderBtnUIWithOrderState:(NSString *)orderState{
    switch ([orderState intValue]) {
        case 1:{
//            self.payStatusLabel.text = @"待支付";
            [self.rightBtn setTitle:@"去支付" forState:UIControlStateNormal];
        }
            break;
        case 2:{
//             self.payStatusLabel.text = @"待使用";
            [self.rightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
        }
            break;
        case 3:{
//            self.payStatusLabel.text = @"待评价";
             [self.rightBtn setTitle:@"去评价" forState:UIControlStateNormal];
        }
            break;
        case 4:{
//            self.payStatusLabel.text = @"已完成";
             [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        case 5:case 6:{
//            self.payStatusLabel.text = @"已关闭";
             [self.rightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;

        default:
            break;
    }
    
    self.leftBtn.hidden = [orderState intValue] == 1 ? NO : YES;
    if ([orderState intValue] == 1 ||[orderState intValue] == 2||[orderState intValue] == 3) {
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"黄色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];

    }
}

//服务订单模式下按钮的点击
- (IBAction)btnAction:(UIButton *)sender {
    if (_bottomBtnAction) {
        self.bottomBtnAction(sender);
    }
    
}




@end
