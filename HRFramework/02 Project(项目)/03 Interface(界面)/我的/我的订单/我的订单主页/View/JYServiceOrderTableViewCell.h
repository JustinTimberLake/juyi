//
//  JYServiceOrderTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYMyOrderModel.h"
#import "JYInvoiceModel.h"

typedef  NS_ENUM (NSInteger, OrderPayStatusType) {
    StatusNor=0,   //默认状态
    StatusInvoice,     //发票记录状态
   };

@interface JYServiceOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *guns;//油枪型号
@property (weak, nonatomic) IBOutlet UIImageView *image;//图片
@property (weak, nonatomic) IBOutlet UILabel *gasolineType;//油型
@property (weak, nonatomic) IBOutlet UILabel *priceLB;//单价
@property (weak, nonatomic) IBOutlet UILabel *addTotalLB;//加油量
@property (weak, nonatomic) IBOutlet UIButton *stationNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *payStatusLabel;
@property (nonatomic, assign) OrderPayStatusType type;
@property (nonatomic, strong) JYMyOrderModel * model;
@property (nonatomic,strong) JYInvoiceModel *invoiceModel;



@property (nonatomic,copy) void(^bottomBtnAction)(UIButton *btn);

@end
