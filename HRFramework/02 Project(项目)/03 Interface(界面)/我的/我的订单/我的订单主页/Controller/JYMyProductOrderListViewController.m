//
//  JYMyProductOrderListViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/8/2.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyProductOrderListViewController.h"
#import "JYProductOrderTableViewCell.h"
#import "JYServiceOrderTableViewCell.h"
#import "JYSegmentedControl.h"
#import "JYServiceOrderDetailViewController.h"
#import "JYProductOrderViewController.h"
#import "JYMyOrderViewModel.h"
#import "JYEvaluateViewController.h"
#import "JYReturnProductViewController.h"
#import "JYPayViewModel.h"
#import "JYPaySuccessViewController.h"
#import "JYWeChatClient.h"
#import "JYApplyRefundViewModel.h"
#import "JYAlertPicViewController.h"
#import "JYRechargeViewController.h"
#import "JYWriteLogisticsViewController.h"


@interface JYMyProductOrderListViewController ()<
UITableViewDelegate,
UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UIView *topBackView;
@property (assign, nonatomic) int cellStyle;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
//segmentControl
@property (strong ,nonatomic) JYSegmentedControl *segmentControl;
//判断订单的状态
@property (nonatomic, assign) int status;
@property (nonatomic, assign) int pageNum;
@property (nonatomic, assign) int pageSize;
@property (nonatomic, strong) JYMyOrderViewModel *viewModel;
@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (nonatomic,strong) JYApplyRefundViewModel *applyRefundViewModel;
@end

@implementation JYMyProductOrderListViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JYMyOrderViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyOrderViewModel alloc]init];
    }
    return _viewModel;
}
#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    _pageNum = 1;
    _pageSize = 10;
    _status = 6; //6默认是全部
    [self configUI];
    [self createSegMentControl];
//    [self networkRequest];
//    [self refreshNormalHeader];
    [self refreshBackNormalFooter];
    [self addNotification];
}

- (void)viewWillAppear:(BOOL)animated{
    [self refreshNormalHeader];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark ==================通知==================


- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
        [self showSuccessTip:noti.object];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYProductOrderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYProductOrderTableViewCell"];
  }
- (void)createSegMentControl {
    JYSegmentedControl *segMentControlView =[[JYSegmentedControl alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    segMentControlView.titleSize = 13;
    [segMentControlView setTitles:@[@"全部",@"待支付",@"待收货",@"待评价",@"已关闭"]];
    [self.topBackView addSubview:segMentControlView];
    segMentControlView.selectedSegmentBlock = ^(NSInteger index){
        [self.viewModel.serviceOderArray removeAllObjects];
        if (index == 1000) {
            self.status = 6;
//            [self networkRequest];
//            [self refreshNormalHeader];
        }else if (index == 1001){
            self.status = 1;
//            [self networkRequest];
//            [self refreshNormalHeader];
        }else if (index == 1002){
            self.status = 2;
//            [self networkRequest];
//            [self refreshNormalHeader];
        }else if (index == 1003){
            self.status =3;
//            [self networkRequest];
//            [self refreshNormalHeader];
        }else if (index == 1004){
            self.status =5;
//            [self networkRequest];
//            [self refreshNormalHeader];
        }
        [self performSelector:@selector(delayMethod) withObject:nil afterDelay:0.2];
    };
    [segMentControlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}


-(void)delayMethod{
    [self.myTableView.mj_header beginRefreshing];
}


#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%d",_pageNum]forKey:@"pageNum"];
    [dict setObject:[NSString stringWithFormat:@"%d",_pageSize] forKey:@"pageSize"];
    [dict setObject:[NSString stringWithFormat:@"%d",_status] forKey:@"state"];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [self.viewModel requesMyProductOrderListWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf.myTableView reloadData];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
  
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
        [weakSelf.myTableView.mj_header endRefreshing];
        [weakSelf.myTableView.mj_footer endRefreshing];
    }];
}

//删除和取消商品订单
- (void)requestDelGoodsOrderWithOrderId:(NSString *)orderId andType:(DelOrderType)delGoodsOrderType{
    WEAKSELF
    [self.viewModel requestDelGoodsOrderWithOrderId:orderId andType:delGoodsOrderType success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
//        [weakSelf refreshNormalHeader];
        
        NSArray *array =  [NSArray arrayWithArray:self.viewModel.productOrderArray];
        
        for (JYMyOrderModel * serviceModel in array) {
            if ([serviceModel.orderId isEqualToString:orderId]) {
                if (delGoodsOrderType == DelOrderType_Del) {
                    [self.viewModel.productOrderArray removeObject:serviceModel];
                    [self.myTableView reloadData];
                }else{
                    serviceModel.orderState = @"11";
                    [self.myTableView reloadData];
                }
            }
        }

        
        
        
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//确认收货
- (void)requestConfirmReceiptWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.viewModel requestConfirmReceiptWithOrderId:orderId success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf refreshNormalHeader];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}


#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------
#pragma mark - 下拉刷新
- (void)refreshNormalHeader{
    WEAK(weakSelf)
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 1;
        [weakSelf.viewModel.productOrderArray removeAllObjects];
        [weakSelf networkRequest];
    }];
    [self.myTableView.mj_header beginRefreshing];
}
#pragma mark - 上拉加载
- (void)refreshBackNormalFooter{
    WEAK(weakSelf)
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        ++ _pageNum;
        [weakSelf networkRequest];
    }];
}

//未发货，退款
- (void)requestApplyRefundWithOrderId:(NSString *)orderId andType:(JYApplyRefundType)type{
    WEAKSELF
    [self.applyRefundViewModel requestApplyRefundWithNotSendProductWithOrderId:orderId andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
//        [weakSelf refreshNormalHeader];
        NSArray *array =  [NSArray arrayWithArray:self.viewModel.productOrderArray];
        
        for (JYMyOrderModel * serviceModel in array) {
            if ([serviceModel.orderId isEqualToString:orderId]) {
                serviceModel.orderState = @"11";
                [self.myTableView reloadData];
            }
        }
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        //        [weakSelf showAlertPicViewWithNotEnoughMoney];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
    
}

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WEAKSELF
    JYProductOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYProductOrderTableViewCell" forIndexPath:indexPath];
    JYMyOrderModel * model = self.viewModel.productOrderArray[indexPath.row];
    cell.model = model;
//    商品订单中的退货不是用的取消的接口
    cell.bottomBtnActionBlock = ^(UIButton *btn) {
        if (btn.tag == 1000) {
            if ([btn.currentTitle isEqualToString:@"取消订单"]) {
                [weakSelf showPicAlertViewControllerWithTitle:@"确定取消?" andSureBtnTitle:@"确定" sureBtnActionBlock:^{
                    [weakSelf requestDelGoodsOrderWithOrderId:model.orderId andType:DelOrderType_Cancle];
                } closeBtnActionBlock:^{
                    
                }];
            }else{
//                未发货情况
                if ([model.orderState intValue] == 3) {
                    [weakSelf.applyRefundViewModel requestApplyRefundWithNotSendProductWithOrderId:model.orderId andType:JYApplyRefundType_Product success:^(NSString *msg, id responseData) {
//                        还可以优化，暂时就这样
                        [weakSelf showSuccessTip:msg];
//                        [weakSelf networkRequest];
                        [weakSelf refreshNormalHeader];
                    } failure:^(NSString *errorMsg) {
                        [weakSelf showSuccessTip:errorMsg];
                    }];
                }
            else{
                
//                                    申请退货
                    JYReturnProductViewController * vc = [[JYReturnProductViewController alloc]init];
                    vc.orderId = model.orderId;
                    [self.navigationController pushViewController:vc animated:YES];
                }

            }
        }else{
            if ([btn.currentTitle isEqualToString:@"去支付"]) {
//                [self showAlertWithPickerViewType:PickerViewTypePay];
                [self showAlertPayWays];
                self.returnPayTypeBlock = ^(NSString *str) {
                    [weakSelf payTypeWithStr:str andOrderId:model.orderId andTotalPrice:model.totalPrice];
                    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                    if (model.orderId.length) {
                        dic[@"orderId"] = model.orderId;
                        dic[@"orderType"] = @"商品订单";
                    }
                    JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
                    
                };

            }else if ([btn.currentTitle isEqualToString:@"去评价"]){
                
                JYMyOrderModel * model = self.viewModel.productOrderArray[indexPath.row];
                
                JYEvaluateViewController * evaluateVC = [[JYEvaluateViewController alloc] init];
                evaluateVC.orderId = model.orderId;
                evaluateVC.orderType = OrderType_Product;
                evaluateVC.shopImage = model.shopImage;
                [self.navigationController pushViewController:evaluateVC animated:YES];
            }else if ([btn.currentTitle isEqualToString:@"确认收货"]){
                
                [self requestConfirmReceiptWithOrderId:model.orderId];
                
            }else if ([btn.currentTitle isEqualToString:@"删除订单"]){
                [self showDeletePicAlertViewControllerSureBtnActionBlock:^{
                    //                删除订单
                    [self requestDelGoodsOrderWithOrderId:model.orderId andType:DelOrderType_Del];

                } closeBtnActionBlock:^{
                    
                }];
            }else if ([btn.currentTitle containsString:@"填写退货信息"]){
                JYWriteLogisticsViewController * logistVC = [[JYWriteLogisticsViewController alloc] init];
                logistVC.orderId = model.orderId;
                [self.navigationController pushViewController:logistVC animated:YES];

            }else {
                //                未发货情况 申请退款
                if ([model.orderState intValue] == 3) {
                    
                    [weakSelf showPicAlertViewControllerWithTitle:@"确定退款?" andSureBtnTitle:@"确定" sureBtnActionBlock:^{
                        [weakSelf.applyRefundViewModel requestApplyRefundWithNotSendProductWithOrderId:model.orderId andType:JYApplyRefundType_Product success:^(NSString *msg, id responseData) {
                            //                        还可以优化，暂时就这样
                            [weakSelf showSuccessTip:msg];
                            //                        [weakSelf networkRequest];
                            //                        [weakSelf refreshNormalHeader];
                            
                            NSArray *array =  [NSArray arrayWithArray:self.viewModel.productOrderArray];
                            
                            for (JYMyOrderModel * serviceModel in array) {
                                if ([serviceModel.orderId isEqualToString:model.orderId]) {
                                    serviceModel.orderState = @"11";
                                    [self.myTableView reloadData];
                                }
                            }
                            
                        } failure:^(NSString *errorMsg) {
                            [weakSelf showSuccessTip:errorMsg];
                        }];
                    } closeBtnActionBlock:^{
                        
                    }];
                    
                }
                else{
                    
                    //                                    申请退货
                    JYReturnProductViewController * vc = [[JYReturnProductViewController alloc]init];
                    vc.orderId = model.orderId;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }    
        }

    };
    
        return cell;
//    }else{
//        JYServiceOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYServiceOrderTableViewCell" forIndexPath:indexPath];
//        cell.model = self.viewModel.serviceOderArray[indexPath.row];
//        return cell;
//    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 201;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.viewModel.productOrderArray.count;
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JYProductOrderViewController * VC = [[JYProductOrderViewController alloc]init];
    if (self.viewModel.productOrderArray.count>indexPath.row) {
        JYMyOrderModel * model = self.viewModel.productOrderArray[indexPath.row];
        VC.orderId = model.orderId;
        [self.navigationController pushViewController:VC animated:YES];
    }
   
    
}

#pragma mark ==================支付代码==================

//支付方式选择
- (void)payTypeWithStr:(NSString *)str andOrderId:(NSString *)orderId andTotalPrice:(NSString *)totalPrice{
    if ([str isEqualToString:@"账户支付"]) {
        if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [totalPrice floatValue]) {
            [self showAlertPicViewWithAccountTypeWithOrderId:orderId andTotalPrice:totalPrice];
            
        }else{
            [self showAlertPicViewWithNotEnoughMoney];
        }

    }else if([str isEqualToString:@"微信支付"]){
        if (![WXApi isWXAppInstalled]) {
            [self showSuccessTip:@"请安装微信客户端"];
            return;
        }else{
            [self requestWXSignWithOrderNum:orderId];
        }
    }else{
        [self requestAlipaySignWithOrderNum:orderId];
    }
}

- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId andTotalPrice:(NSString *)totalPrice{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalPrice floatValue]);
    
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalPrice,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}

//- (void)showAlertPicViewWithNotEnoughMoney{
//    WEAKSELF
//    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
//    alertVC.contentText= @"您当前的余额不足\n请充值后付款!";
//    alertVC.btnText = @"去充值";
//    alertVC.sureBtnActionBlock = ^(){
//
//        JYRechargeViewController * rechargeVC = [[JYRechargeViewController alloc] init];
//        [weakSelf.navigationController pushViewController:rechargeVC animated:YES];
//    };
//    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [self presentViewController:alertVC animated:YES completion:nil];
//}

//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
            //            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                JY_POST_NOTIFICATION(JY_NOTI_PAY_ALIPAY, nil);
//                switch ([code intValue]) {
//                    case 9000:
//                        //                        [self popToRootViewControllerAnimated:NO];
//                    {
//                        [weakSelf turnPaySuccessPage];
//                    }
//                        break;
//                    case 8000:
//                        //                        [self showSuccessTip:@"正在处理中"];
//                        break;
//                    case 4000:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                    case 6001:
//                        //                        [self showSuccessTip:@"支付已取消"];
//                        break;
//                    case 6002:
//                        //                        [self showSuccessTip:@"网络连接错误"];
//                        break;
//                    default:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                }
                
            }];
            
        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
//        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//跳转到支付成功页面
- (void)turnPaySuccessPage{
    //    UIApplication *app = [UIApplication sharedApplication];
    //    JYBaseTabBarController *tabbar = (JYBaseTabBarController*)app.delegate.window.rootViewController;
    //    UINavigationController *nav= tabbar.viewControllers[tabbar.selectedIndex];
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    [self.navigationController pushViewController:successVC animated:YES];
}


- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}

- (JYApplyRefundViewModel *)applyRefundViewModel
{
    if (!_applyRefundViewModel) {
        _applyRefundViewModel = [[JYApplyRefundViewModel alloc] init];
    }
    return _applyRefundViewModel;
}


@end
