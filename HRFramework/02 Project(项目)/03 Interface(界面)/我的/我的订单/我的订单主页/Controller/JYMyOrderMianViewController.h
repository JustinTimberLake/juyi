//
//  JYMyOrderMianViewController.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYMyOrderMianViewController : JYBaseViewController
//是否是查看模式
@property (nonatomic,assign,getter=isCheckMode) BOOL checkMode;

@end
