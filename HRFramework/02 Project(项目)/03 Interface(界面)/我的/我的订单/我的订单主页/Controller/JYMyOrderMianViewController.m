//
//  JYMyOrderMianViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMyOrderMianViewController.h"
#import "JYMyProductOrderListViewController.h"
#import "JYMyServiceOrderListViewController.h"
#import "JYSegmentedControl.h"
#import "JYBaseTabBarController.h"

@interface JYMyOrderMianViewController ()
@property (strong ,nonatomic)UIView *topBackView;
//segmentControl
@property (strong ,nonatomic)JYSegmentedControl *segmentControl;
//主页面scorllView
@property (strong, nonatomic)UIScrollView *mainScrollView;

//子控制器数组
@property (strong, nonatomic)NSArray <NSString *>*controllerArr;
//标题数组
@property (strong, nonatomic)NSArray <NSString *>*titleArr;
@end

@implementation JYMyOrderMianViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeData];
    [self configUI];
}

#pragma mark - ---------- 初始化数据 ----------
- (void)initializeData {
    self.controllerArr = @[@"JYMyServiceOrderListViewController",@"JYMyProductOrderListViewController"];
}

#pragma mark - ---------- 构建UI ----------
- (void)configUI {
    [self.view addSubview:self.mainScrollView];
    [self configChildControllers];
    [self configScrollView];
    [self createSegMentControl];
}

- (void)configChildControllers {
    [self.controllerArr enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Class class = NSClassFromString(obj);
        [self addChildViewController:[class new]];
    }];
}

//返回点击
- (void)backAction{
    if (self.checkMode) {
        JYBaseTabBarController * tabBarVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        tabBarVC.selectedIndex = 3;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)configScrollView {
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.mainScrollView addSubview:obj.view];
        obj.view.frame = CGRectMake(idx * SCREEN_WIDTH, 0, self.mainScrollView.width, self.mainScrollView.height);
    }];
    self.mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * self.childViewControllers.count, self.mainScrollView.size.height);
    //设置默认controller
    // YSJ_MyFavoriteVideoViewController *vc = self.childViewControllers[0];
}

- (void)createSegMentControl {
    WEAK(weakSelf)
    [self centerItemTitleArray:@[@"服务订单",@"商品订单"] action:^(NSInteger index) {
        NSLog(@"%ld",index);
        [UIView animateWithDuration:.25f animations:^{
            weakSelf.mainScrollView.contentOffset = CGPointMake((index -1000) * SCREEN_WIDTH, 0);
        }];
        
//        switch (index-1000) {
//            case 0:{
//                JYMyServiceOrderListViewController *vc = self.childViewControllers[index-1000];
//            }
//                break;
//                
//            case 1:{
//                JYMyProductOrderListViewController *vc = self.childViewControllers[index-1000];
//            }
//                break;
//                      default:
//                break;
//        }
    }];
    
}
 
#pragma mark - ---------- 懒加载 ----------
#pragma mark -topBackView-
#pragma mark -mainScrollView-
- (UIScrollView *)mainScrollView {
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc]init];
        _mainScrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64);
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.scrollEnabled = NO;
    }
    return _mainScrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
