//
//  JYMyOrderViewModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/8/1.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"
#import "JYMyOrderModel.h"
#import "JYServiceOrderModel.h"
#import "JYProductDetailModel.h"
#import "JYMyServiceOrderModel.h"

// 删除、取消、退款 服务订单类型
typedef NS_ENUM(NSInteger,DelOrderType) {
    DelOrderType_Del = 1,    //删除
    DelOrderType_Cancle,     //取消
//    DelOrderType_Refund      //退款 （商品订单中没有这一项） 接口有变化，退款不用这个接口了
    
};


@interface JYMyOrderViewModel : JYBaseViewModel
@property (nonatomic, strong) NSMutableArray <JYMyServiceOrderModel*>* serviceOderArray;
@property (nonatomic, strong) NSMutableArray <JYMyOrderModel*>* productOrderArray;
//服务订单模型
@property (nonatomic, strong) JYServiceOrderModel * serviceDetail;
//商品订单模型
@property (nonatomic, strong) JYProductDetailModel * productDetail;
//实体订单模型
@property (nonatomic, strong) JYProductDetailModel * shitiDetail;



#pragma mark ------------------服务订单------------------------
//请求服务订单列表
- (void)requesMyServiceOrderListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//请求服务订单详情
//- (void)requesMyServiceOrderDetailWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//综合体  0 是服务订单，  2 是实体订单
- (void)requesMyServiceOrderDetailWithParams:(NSDictionary *)params andType:(int)detailType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;


//JY-015-003 删除、取消、服务订单 ()
- (void)requestMyServiceDelServiceOrderWithOrderId:(NSString *)orderId andType:(DelOrderType)delServiceOrderType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//#pragma mark ------------------实体订单------------------------
////请求实体订单详情 （实体订单的列表和操作同服务订单是一样的）
//- (void)requesMyShiTiOrderDetailWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
//
#pragma mark ------------------商品订单------------------------

//请求商品订单列表
- (void)requesMyProductOrderListWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//请求商品订单详情
- (void)requesMyProductOrderDetailWithParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//商品订单的删除和取消
- (void)requestDelGoodsOrderWithOrderId:(NSString *)orderId andType:(DelOrderType)delGoodsOrderType success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//确认收货
- (void)requestConfirmReceiptWithOrderId:(NSString *)orderId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

//检测订单是否付款
- (void)requestCheckOrderpayWithOrderId:(NSString *)orderId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
