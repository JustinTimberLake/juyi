//
//  JYProductPayedTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JYProductDetailModel;
@interface JYProductPayedTableViewCell : UITableViewCell
@property (nonatomic,strong) JYProductDetailModel *model;

@end
