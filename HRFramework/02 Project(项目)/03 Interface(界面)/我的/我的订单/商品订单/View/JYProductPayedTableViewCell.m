//
//  JYProductPayedTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductPayedTableViewCell.h"
#import "JYProductDetailModel.h"
#import "JYCommonWebViewController.h"


@interface JYProductPayedTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *payTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *payStyleLab;
@property (weak, nonatomic) IBOutlet UILabel *wuliuTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLab;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLab;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *wuliuLab;
@property (weak, nonatomic) IBOutlet UIButton *wuliuInfoBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderNumTopH;

@end
@implementation JYProductPayedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

//1待付款2取消订单3待发货4已退款5已发货6已收货7待评价8退货中9已退货10退款失败11已关闭12已完成
- (void)setModel:(JYProductDetailModel *)model{
    _model = model;
    self.orderNumTopH.constant =  ([model.orderState intValue] == 1)? 10 : 36;
    self.wuliuTitleLab.hidden =  ([model.orderState intValue] == 1)? YES : NO;
    self.wuliuLab.hidden = self.wuliuTitleLab.isHidden;
    self.wuliuInfoBtn.hidden = self.wuliuTitleLab.isHidden;
    
    self.payTitleLab.hidden = self.wuliuTitleLab.isHidden;
    self.payStyleLab.hidden = self.payTitleLab.hidden;
//    1待付款2取消订单3待发货4已退款5已发货6已收货7待评价8退货中9已退货10退款失败11已关闭12已完成13  退货审核中14  退货审成功
    if ([_model.orderState isEqualToString:@"1"]) {
        self.orderStateLab.text = @"待付款";
    }else if ([_model.orderState isEqualToString:@"2"]){
        self.orderStateLab.text = @"取消订单";
    }else if ([_model.orderState isEqualToString:@"3"]){
          self.orderStateLab.text = @"待发货";
    }else if ([_model.orderState isEqualToString:@"4"]){
         self.orderStateLab.text = @"已退款";
    }else if ([_model.orderState isEqualToString:@"5"]){
        self.orderStateLab.text = @"已发货";
    }else if ([_model.orderState isEqualToString:@"6"]){
        self.orderStateLab.text = @"已收货";
    }else if ([_model.orderState isEqualToString:@"7"]){
        self.orderStateLab.text = @"待评价";
    }else if ([_model.orderState isEqualToString:@"8"]){
        self.orderStateLab.text = @"退货中";
    }else if ([_model.orderState isEqualToString:@"9"]){
        self.orderStateLab.text = @"已退货";
    }else if ([_model.orderState isEqualToString:@"10"]){
        self.orderStateLab.text = @"退款失败";
    }else if ([_model.orderState isEqualToString:@"11"]){
        self.orderStateLab.text = @"已关闭";
    }else if ([_model.orderState isEqualToString:@"12"]){
        self.orderStateLab.text = @"已完成";
    }else if ([_model.orderState isEqualToString:@"13"]){
        self.orderStateLab.text = @"退货审核中";
    }else if ([_model.orderState isEqualToString:@"14"]){
        self.orderStateLab.text = @"退货审核成功";
    }else if ([_model.orderState isEqualToString:@"15"]){
        self.orderStateLab.text = @"退货失败";
    }else if ([_model.orderState isEqualToString:@"16"]){
        self.orderStateLab.text = @"退货成功";
    }
    
//    支付方式(1支付宝2微信3帐户余额4积分抵扣5油库抵扣6优惠券抵扣)
    if ([_model.payMode isEqualToString:@"1"]) {
        self.payStyleLab.text = @"支付宝";
    }else if ([_model.payMode isEqualToString:@"2"]){
        self.payStyleLab.text = @"微信";
    }else if ([_model.payMode isEqualToString:@"3"]){
        self.payStyleLab.text = @"帐户余额";
    }else if ([_model.payMode isEqualToString:@"4"]){
        self.payStyleLab.text = @"积分抵扣";
    }else if ([_model.payMode isEqualToString:@"5"]){
        self.payStyleLab.text = @"油库抵扣";
    }else{
        self.payStyleLab.text = @"优惠券抵扣";
    }
    self.orderNumLab.text = model.orderId;
    self.orderTimeLab.text = model.orderTime;
    self.wuliuLab.text = model.logisticsTime;

}


- (IBAction)wuliuInfoBtnAction:(UIButton *)sender {
    JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
    webVC.navTitle = @"物流信息";
    webVC.url = self.model.getLogistics;
    [self.viewController.navigationController pushViewController:webVC animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
