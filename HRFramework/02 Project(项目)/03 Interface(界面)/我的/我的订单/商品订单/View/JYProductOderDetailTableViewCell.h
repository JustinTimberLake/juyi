//
//  JYProductOderDetailTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYProductDetailModel.h"
@interface JYProductOderDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *shopNameLB;
@property (weak, nonatomic) IBOutlet UIImageView *shopImage;
@property (weak, nonatomic) IBOutlet UILabel *shopTiltelLB;
@property (weak, nonatomic) IBOutlet UILabel *item1TitleLab;
@property (weak, nonatomic) IBOutlet UILabel *item2TitleLab;
@property (weak, nonatomic) IBOutlet UILabel *countTitleLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *item2LeftDistance;

@property (nonatomic, strong)JYProductDetailModel *model;
@property (nonatomic,strong) JYProductDetailModel *shitiModel;

@end
