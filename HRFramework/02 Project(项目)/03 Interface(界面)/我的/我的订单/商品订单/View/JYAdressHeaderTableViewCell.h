//
//  JYAdressHeaderTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYProductDetailModel.h"
@interface JYAdressHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *personNameLB;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumLB;
@property (weak, nonatomic) IBOutlet UILabel *personAdressLB;
@property (weak, nonatomic) IBOutlet UIButton *arrow;
@property (nonatomic, strong)JYProductDetailModel * model;
@end
