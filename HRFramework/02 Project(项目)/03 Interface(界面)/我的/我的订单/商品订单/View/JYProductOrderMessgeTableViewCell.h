//
//  JYProductOrderMessgeTableViewCell.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JYProductDetailModel.h"
@interface JYProductOrderMessgeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *peiSongTypeLB;
@property (weak, nonatomic) IBOutlet UILabel *leaveMsgLB;
@property (weak, nonatomic) IBOutlet UILabel *redPickLB;
@property (weak, nonatomic) IBOutlet UILabel *intsLB;
@property (nonatomic, strong) JYProductDetailModel * model;
@end
