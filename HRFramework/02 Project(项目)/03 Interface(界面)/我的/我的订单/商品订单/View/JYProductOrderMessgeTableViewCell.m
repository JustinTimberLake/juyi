//
//  JYProductOrderMessgeTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductOrderMessgeTableViewCell.h"

@implementation JYProductOrderMessgeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(JYProductDetailModel *)model{
    _model = model;
    _peiSongTypeLB.text = [NSString stringWithFormat:@"￥%@",_model.goods.firstObject.freight];
    _redPickLB.text = [_model.coupon floatValue] >0 ?SF(@"%@",_model.coupon):@"否";
    _intsLB.text = [_model.score floatValue] > 0 ? SF(@"%@",_model.score) : @"否";
    _leaveMsgLB.text = _model.leaveMsg;
}
@end
