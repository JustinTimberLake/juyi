//
//  JYProductOderDetailTableViewCell.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductOderDetailTableViewCell.h"


@implementation JYProductOderDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(JYProductDetailModel *)model{
    _model = model;
    
    [_shopNameLB setTitle:_model.shopTitle forState:UIControlStateNormal];
//    [_shopNameLB sd_setImageWithURL:[NSURL URLWithString:model.shopImage] forState:UIControlStateNormal];
    [_shopImage sd_setImageWithURL:[NSURL URLWithString:_model.goods[0].goodsImage] placeholderImage:JY_PLACEHOLDER_IMAGE];
    _shopTiltelLB.text = _model.goods[0].goodsTitle;
    
    if (model.goods[0].item1name.length) {
        self.item1TitleLab.hidden = NO;
        self.item1TitleLab.text = SF(@"%@:%@",model.goods[0].item1name,model.goods[0].item1value);
        self.item2LeftDistance.constant = 20;
    }else{
        self.item2LeftDistance.constant = 0;
    }
    
    if (model.goods[0].item2name.length) {
        self.item2TitleLab.hidden = NO;
        self.item2TitleLab.text = SF(@"%@:%@",model.goods[0].item2name,model.goods[0].item2value);
    }else{
        self.item2TitleLab.hidden = YES;
    }
    
    self.countTitleLab.text = SF(@"数量:x%@",model.goods[0].goodsNum);
}

- (void)setShitiModel:(JYProductDetailModel *)shitiModel{
    _shitiModel = shitiModel;
    [_shopNameLB setTitle:shitiModel.shopTitle forState:UIControlStateNormal];
//    [_shopNameLB sd_setImageWithURL:[NSURL URLWithString:shitiModel.shopImage] forState:UIControlStateNormal];

    [_shopImage sd_setImageWithURL:[NSURL URLWithString:shitiModel.goods[0].goodsImage] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
    _shopTiltelLB.text = shitiModel.goods[0].goodsTitle;
    self.countTitleLab.hidden = YES;
    self.item1TitleLab.hidden = YES;
    self.item2TitleLab.hidden = YES;
    if (shitiModel.goods[0].item1name.length) {
        self.item1TitleLab.hidden = NO;
        self.item1TitleLab.text = SF(@"%@:%@",shitiModel.goods[0].item1name,shitiModel.goods[0].item1value);

    }else{
        self.item2LeftDistance.constant = 0;
    }

    if (shitiModel.goods[0].item2name.length) {
        self.item2TitleLab.hidden = NO;
        self.item2TitleLab.text = SF(@"%@:%@",shitiModel.goods[0].item2name,shitiModel.goods[0].item2value);
    }
}

@end
