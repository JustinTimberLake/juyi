//
//  JYProductDetailModel.h
//  JY
//
//  Created by YunPeng-Gao on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYMyOrderModel.h"
#import "JYMyAdressModel.h"
@interface JYProductDetailModel : JYBaseModel
@property (nonatomic, copy)NSString *orderId;
@property (nonatomic, copy)NSString *shopId;
@property (nonatomic, copy)NSString *shopTitle;
@property (nonatomic, copy)NSString *shopImage;
@property (nonatomic, copy)NSString *gunNum;
@property (nonatomic, copy)NSString *oilModel;
@property (nonatomic, copy)NSString *oilPrice;
@property (nonatomic, copy)NSString *oilNum;
@property (nonatomic, copy)NSString *orderState;
@property (nonatomic, copy)NSString *totalPrice;
@property (nonatomic, copy)NSString *shopEvaluate;
@property (nonatomic, copy)NSString *shopAddress;
@property (nonatomic, copy)NSString *shopTel;
@property (nonatomic, copy)NSString *shopLong;
@property (nonatomic, copy)NSString *shopLat;
@property (nonatomic, copy)NSString *coupon;
@property (nonatomic, copy)NSString *oilDepot;
@property (nonatomic, copy)NSString *score;
@property (nonatomic, copy)NSString *invoice;
@property (nonatomic, copy)NSString *orderTime;
@property (nonatomic, copy)NSString *payMode;
@property (nonatomic, copy)NSString *goodsClassNum;//商品件数;
@property (nonatomic, copy)NSString *leaveMsg;
@property (nonatomic, copy)NSString *logisticsId;
@property (nonatomic, copy)NSString *logisticsTime;
@property (nonatomic, copy)NSString *logisticsType;
@property (nonatomic,copy) NSString *getLogistics; //物流链接
@property (nonatomic, strong)NSMutableArray <JYMyProductOrderModel *>* goods;
@property (nonatomic, strong) JYMyAdressModel * address;
@end
