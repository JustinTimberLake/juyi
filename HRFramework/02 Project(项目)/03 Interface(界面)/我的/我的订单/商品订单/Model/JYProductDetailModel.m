//
//  JYProductDetailModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/8/7.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductDetailModel.h"

@implementation JYProductDetailModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"goods":@"JYMyProductOrderModel",
             @"address":@"JYMyAdressModel"};
}

@end
