//
//  JYProductOrderViewController.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYProductOrderViewController : JYBaseViewController

//订单ID
@property (nonatomic,copy) NSString *orderId;
//是否是查看模式
@property (nonatomic,assign,getter=isCheckMode) BOOL checkMode;
@end
