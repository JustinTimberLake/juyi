//
//  JYProductOrderViewController.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProductOrderViewController.h"
#import "JYProductPayedTableViewCell.h"
#import "JYAdressHeaderTableViewCell.h"
#import "JYProductOderDetailTableViewCell.h"
#import "JYProductOrderMessgeTableViewCell.h"
#import "JYReturnProductViewController.h"
#import "JYMyOrderViewModel.h"
#import "JYWriteLogisticsViewController.h"
#import "JYEvaluateViewController.h"
#import "JYPayViewModel.h"
//#import "JYPayTool.h"
#import "JYAlipayClient.h"
#import "JYPaySuccessViewController.h"
#import "JYBaseTabBarController.h"
#import "JYApplyRefundViewModel.h"
#import "JYAlertPicViewController.h"
#import "JYDetailViewController.h"

//#import "JYPickerViewAlertController.h"
//#import "JYPaySuccessViewController.h"
//#import "JYPayTool.h"

@interface JYProductOrderViewController ()<
   UITableViewDelegate,
   UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) JYMyOrderViewModel * viewModel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftBtn;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightBtn;
@property (nonatomic,strong) JYPayViewModel *payViewModel;
@property (nonatomic,strong) JYApplyRefundViewModel *applyRefundViewModel;
////弹窗的类型 （普通/支付）
//@property (nonatomic, assign ) PickerViewType pickerViewType;
//@property (nonatomic,strong) JYPickerViewAlertController * alertVC;



@end

@implementation JYProductOrderViewController

#pragma mark - ----------   Lifecycle（生命周期） ----------
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self addNotification];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self networkRequest];
}

- (void)backAction{
    if (self.checkMode) {
        JYBaseTabBarController * tabBarVC = (JYBaseTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        tabBarVC.selectedIndex = 3;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark ==================通知==================


- (void)addNotification{
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_SUCCESS);
    JY_ADD_NOTIFICATION(JY_NOTI_PAY_FAILURE);
}

- (void)getNotification:(NSNotification *)noti{
    if ([noti.name isEqualToString:JY_NOTI_PAY_SUCCESS]) {
        [self showSuccessTip:@"充值成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }else if ([noti.name isEqualToString:JY_NOTI_PAY_FAILURE] ){
        [self showSuccessTip:noti.object];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）
- (void)configUI{
    self.naviTitle = @"订单详情";
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYProductPayedTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYProductPayedTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYAdressHeaderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYAdressHeaderTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYProductOderDetailTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYProductOderDetailTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JYProductOrderMessgeTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JYProductOrderMessgeTableViewCell"];

}
#pragma mark networkRequest (网络请求)
- (void)networkRequest{
    WEAK(weakSelf)
    [self showHUD];
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    dict[@"orderId"] = self.orderId;
    [self.viewModel requesMyProductOrderDetailWithParams:dict success:^(NSString *msg, id responseData) {
        [weakSelf hideHUD];
        [weakSelf updataBottomUI];
        [weakSelf.myTableView reloadData];
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        [weakSelf hideHUD];
    }];

}

//账户余额支付接口
- (void)requestAccountBalancePayWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.payViewModel requestAccountBalancePayWithOrderId:orderId success:^(NSString *msg, id responseData) {
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
        //        [weakSelf showAlertPicViewWithNotEnoughMoney];
        JY_POST_NOTIFICATION(JY_NOTI_PAY_ACCOUNT, nil);
        
    }];
}


//3.3.1    JY-012-007 获取微信支付请求预支付id接口
- (void)requestWXSignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetWxPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        //        weakSelf.payViewModel.wxRequest
        [[JYWeChatClient sharedInstance] pay:weakSelf.payViewModel.wxRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//请求支付宝签名
- (void)requestAlipaySignWithOrderNum:(NSString *)orderNum{
    WEAKSELF
    [self.payViewModel requestGetAlipayPaySignWithOrderSn:orderNum success:^(NSString *msg, id responseData) {
        NSString * sign = responseData;
        if (sign.length) {
//            [[JYPayTool shareInstance] payWithAlipayWithOrderString:sign andAppScheme:@"JYUser"];
            [[JYAlipayClient sharedInstance] pay:sign scheme:JY_ALIPAY_APPSCHEME result:^(NSString *code, NSString *msg) {
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                if (weakSelf.orderId.length) {
                    dic[@"orderId"] = weakSelf.orderId;
                    dic[@"orderType"] = @"商品订单";
                }
                JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);

//                switch ([code intValue]) {
//                    case 9000:
//                        //                        [self popToRootViewControllerAnimated:NO];
//                    {
//                        [weakSelf turnPaySuccessPage];
//                    }
//                        break;
//                    case 8000:
//                        //                        [self showSuccessTip:@"正在处理中"];
//                        break;
//                    case 4000:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                    case 6001:
//                        //                        [self showSuccessTip:@"支付已取消"];
//                        break;
//                    case 6002:
//                        //                        [self showSuccessTip:@"网络连接错误"];
//                        break;
//                    default:
//                        //                        [self showSuccessTip:@"支付失败"];
//                        break;
//                }

            }];

        }
    } failure:^(NSString *errorMsg) {
        [self showSuccessTip:errorMsg];
    }];
}

//商品订单的删除和取消
- (void)requestDelGoodsOrderWithOrderId:(NSString *)orderId andType:(DelOrderType)delGoodsOrderType{
    WEAKSELF
    [self.viewModel requestDelGoodsOrderWithOrderId:orderId andType:delGoodsOrderType success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        if (delGoodsOrderType == DelOrderType_Del ) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [weakSelf networkRequest];
        }
//        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//未发货，退款
- (void)requestApplyRefundWithOrderId:(NSString *)orderId andType:(JYApplyRefundType)type{
    WEAKSELF
    [self.applyRefundViewModel requestApplyRefundWithNotSendProductWithOrderId:orderId andType:type success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf networkRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}



//确认收货
- (void)requestConfirmReceiptWithOrderId:(NSString *)orderId{
    WEAKSELF
    [self.viewModel requestConfirmReceiptWithOrderId:orderId success:^(NSString *msg, id responseData) {
        [weakSelf showSuccessTip:msg];
        [weakSelf networkRequest];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

#pragma mark IBActions （点击事件xib）
- (IBAction)bottomBtnAction:(UIButton *)sender {
////    测试
//    JYReturnProductViewController * vc = [[JYReturnProductViewController alloc]init];
//    vc.orderId = self.orderId;
//    [self.navigationController pushViewController:vc animated:YES];
//    测试评价
//    
//    JYEvaluateViewController * evaluateVC = [[JYEvaluateViewController alloc] init];
//    evaluateVC.orderId = self.orderId;
//    evaluateVC.orderType = OrderType_Product;
//    [self.navigationController pushViewController:evaluateVC animated:YES];
    
    WEAKSELF

    if (sender.tag == 1000) {
        if ([sender.currentTitle isEqualToString:@"取消订单"]) {
            [weakSelf showPicAlertViewControllerWithTitle:@"确定取消?" andSureBtnTitle:@"确定" sureBtnActionBlock:^{
                [weakSelf requestDelGoodsOrderWithOrderId:self.orderId andType:DelOrderType_Cancle];
            } closeBtnActionBlock:^{
                
            }];

        }else{
            if ([self.viewModel.productDetail.orderState intValue] == 3) {
                [self requestApplyRefundWithOrderId:self.orderId andType:JYApplyRefundType_Product];
            }else{
//                        申请退货
                JYReturnProductViewController * vc = [[JYReturnProductViewController alloc]init];
                vc.orderId = self.orderId;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }else{
        if ([sender.currentTitle isEqualToString:@"去支付"]) {
//            [self showAlertWithPickerViewType:PickerViewTypePay];
            [self showAlertPayWays];
            self.returnPayTypeBlock = ^(NSString *str) {
                [weakSelf payTypeWithStr:str];
//                 JY_POST_NOTIFICATION(JY_PAY_ORDERID, weakSelf.orderId);
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                if (weakSelf.orderId.length) {
                    dic[@"orderId"] = weakSelf.orderId;
                    dic[@"orderType"] = @"商品订单";
                }
                JY_POST_NOTIFICATION(JY_PAY_ORDERID, dic);
            };
            
        }else if ([sender.currentTitle isEqualToString:@"确认收货"]){
            
            [self requestConfirmReceiptWithOrderId:self.orderId];
            
        }else if ([sender.currentTitle isEqualToString:@"去评价"]){
            
            JYEvaluateViewController * evaluateVC = [[JYEvaluateViewController alloc] init];
            evaluateVC.orderId = self.orderId;
            evaluateVC.orderType = OrderType_Product;
            evaluateVC.shopImage = self.viewModel.productDetail.shopImage;
            [self.navigationController pushViewController:evaluateVC animated:YES];
        }else if ([sender.currentTitle isEqualToString:@"删除订单"]){
            [self showDeletePicAlertViewControllerSureBtnActionBlock:^{
                [weakSelf requestDelGoodsOrderWithOrderId:self.orderId andType:DelOrderType_Del];
            } closeBtnActionBlock:^{
                
            }];

        }else if ([sender.currentTitle containsString:@"填写退货信息"]){
            JYWriteLogisticsViewController * logistVC = [[JYWriteLogisticsViewController alloc] init];
            logistVC.orderId = self.orderId;
            [self.navigationController pushViewController:logistVC animated:YES];
        }else{
            if ([self.viewModel.productDetail.orderState intValue] == 3) {
                [self requestApplyRefundWithOrderId:self.orderId andType:JYApplyRefundType_Product];
            }else{
                //                        申请退货
                JYReturnProductViewController * vc = [[JYReturnProductViewController alloc]init];
                vc.orderId = self.orderId;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }
//        else if ([sender.currentTitle isEqualToString:@"已退货"]){
//
//        }
}

#pragma mark - ---------- Public Methods（公有方法） ----------
//支付方式选择
- (void)payTypeWithStr:(NSString *)str{
    if ([str isEqualToString:@"账户支付"]) {
        NSLog(@"账户支付");
        if ([[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] > [self.totalPriceLab.text floatValue]) {
            [self showAlertPicViewWithAccountTypeWithOrderId:self.orderId andTotalPrice:self.viewModel.productDetail.totalPrice];;
            
        }else{
            [self showAlertPicViewWithNotEnoughMoney];
        }

    }else if([str isEqualToString:@"微信支付"]){
        NSLog(@"微信支付");
        if (![WXApi isWXAppInstalled]) {
            [self showSuccessTip:@"请安装微信客户端"];
            return;
        }else{
            
            [self requestWXSignWithOrderNum:self.orderId];
        }
    }else{
        NSLog(@"支付宝支付");
        [self requestAlipaySignWithOrderNum:self.orderId];
    }
}

//跳转到支付成功页面
- (void)turnPaySuccessPage{
//    UIApplication *app = [UIApplication sharedApplication];
//    JYBaseTabBarController *tabbar = (JYBaseTabBarController*)app.delegate.window.rootViewController;
//    UINavigationController *nav= tabbar.viewControllers[tabbar.selectedIndex];
    JYPaySuccessViewController *successVC = [[JYPaySuccessViewController alloc]init];
    [self.navigationController pushViewController:successVC animated:YES];
}
//更新底部UI和按钮状态
//最新状态：1待付款2取消订单3待发货4已退款5已发货6已收货7待评价8退货中9已退货10退款失败11已关闭12已完成13  退货审核中14  退货审成功

- (void)updataBottomUI{
    self.totalPriceLab.text = SF(@"%@",self.viewModel.productDetail.totalPrice);
    NSString * orderState = self.viewModel.productDetail.orderState;

    switch ([orderState intValue]) {
        case 1:{
            [self.bottomRightBtn setTitle:@"去支付" forState:UIControlStateNormal];
            [self.bottomLeftBtn setTitle:@"取消订单" forState:UIControlStateNormal];
            
        }
            break;
        case 3:{
            [self.bottomRightBtn setTitle:@"申请退款" forState:UIControlStateNormal];
        }
            break;
        case 4:{
            [self.bottomRightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        case 5:{
            [self.bottomLeftBtn setTitle:@"申请退货" forState:UIControlStateNormal];
            [self.bottomRightBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        }
            break;
        case 6: case 7:{
            [self.bottomLeftBtn setTitle:@"申请退货" forState:UIControlStateNormal];
            [self.bottomRightBtn setTitle:@"去评价" forState:UIControlStateNormal];
        }
            break;
            
        case 8:{

            //            [self.rightBtn setTitle:@"退货中" forState:UIControlStateNormal];
        }
            break;
            
        case 11:case 2:{
            [self.bottomRightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
            
            
        case 12:{
            [self.bottomRightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        case 13:{
            //            [self.rightBtn setTitle:@"审核中" forState:UIControlStateNormal];
        }
            break;
        case 14:{
            [self.bottomRightBtn setTitle:@"    填写退货信息    " forState:UIControlStateNormal];
        }
            break;
            
        case 15:{
            [self.bottomRightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
            
        case 16:{
            [self.bottomRightBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
    self.bottomLeftBtn.hidden = ([orderState intValue] == 1 || [orderState intValue] == 6||[orderState intValue] == 5  ||[orderState intValue] == 7  ) ? NO : YES;
    
    self.bottomRightBtn.hidden = ([orderState intValue] == 8 ||[orderState intValue] == 13 ) ? YES : NO;
    
    if ([orderState intValue] == 1 ||[orderState intValue] == 5 ||[orderState intValue] == 3||[orderState intValue] == 6||[orderState intValue] == 7) {
        [self.bottomRightBtn setBackgroundImage:[UIImage imageNamed:@"黄色矩形按钮"] forState:UIControlStateNormal];
        [self.bottomRightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    }else{
        [self.bottomRightBtn setBackgroundImage:[UIImage imageNamed:@"灰色矩形按钮"] forState:UIControlStateNormal];
        [self.bottomRightBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
    }
    
}


- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId andTotalPrice:(NSString *)totalPrice{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    NSString * accountStr = SF(@"%.2f",[[User_InfoShared shareUserInfo].personalModel.userRemainder floatValue] - [totalPrice floatValue]);
    
    alertVC.contentText= SF(@"确认支付%@元？\n支付后账户余额：%@元",totalPrice,accountStr );
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        [weakSelf requestAccountBalancePayWithOrderId:orderId];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}



//- (void)showAlertWithPickerViewType:(PickerViewType)pickerViewType{
//    WEAKSELF
//    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
//    self.alertVC = alertVC;
//    alertVC.pickerViewType = pickerViewType;
//    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    
//    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
//        switch (pickerViewType) {
//            case PickerViewTypePay:{
//                if (btnTag == 1001 && selectIndex == 0) {
//                    //                    账户支付
////                    [weakSelf showAlertPicViewWithAccountType];
//                }else if (btnTag == 1001 && selectIndex == 1){
//                    //                    微信支付
////                    [weakSelf showAlertPicViewWithNotEnoughMoney];
//                }else{
//                    
//                    [[JYPayTool shareInstance] payWithAlipayWithOrderString:@"" andAppScheme:@"JYUser"];
////                    //                    支付宝支付
////                    JYPaySuccessViewController * payVC = [[JYPaySuccessViewController alloc] init];
////                    [weakSelf.navigationController pushViewController:payVC animated:YES];
//                }
//            }
//                break;
//            default:
//                break;
//        }
//        
//    };
//    
//    [self presentViewController:alertVC animated:YES completion:nil];
//}
//


#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            JYAdressHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYAdressHeaderTableViewCell" forIndexPath:indexPath];
            cell.model  = self.viewModel.productDetail;
            cell.arrow.hidden = YES;
            return cell;
        }
            break;
        case 1:{
            JYProductOderDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYProductOderDetailTableViewCell"forIndexPath:indexPath];
             cell.model  = self.viewModel.productDetail;
            return cell;
        }
            break;
            
        case 2:{
            JYProductOrderMessgeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYProductOrderMessgeTableViewCell"forIndexPath:indexPath];
             cell.model  = self.viewModel.productDetail;
            return cell;
        }
            
        case 3:{
            JYProductPayedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JYProductPayedTableViewCell"forIndexPath:indexPath];
            cell.model = self.viewModel.productDetail;
            return cell;
        }
            
            break;
        default:
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return 126;
            break;
        case 1:
            return 141;
            break;
        case 2:
            return 203;
            break;
        case 3:{
            if ([self.viewModel.productDetail.orderState intValue] == 1) {
                return 90;
            }else{
                return 150;
            }
        }
        default:
            break;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 9;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 2) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_GRAY_BG];
        return view;
    }
    return nil;
   
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        if (!self.viewModel.productDetail.goods.count) {
            return;
        }
        JYDetailViewController *controller = [[JYDetailViewController alloc]init];
        controller.detailType = DetailTypeStore;
        JYMyProductOrderModel *model = self.viewModel.productDetail.goods[0];
        controller.goodsId = model.goodsId;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - ---------- Lazy Loading（懒加载） ----------
- (JYMyOrderViewModel*)viewModel{
    if (!_viewModel) {
        _viewModel = [[JYMyOrderViewModel alloc]init];
    }
    return _viewModel;
}

- (JYPayViewModel *)payViewModel
{
    if (!_payViewModel) {
        _payViewModel = [[JYPayViewModel alloc] init];
    }
    return _payViewModel;
}


- (JYApplyRefundViewModel *)applyRefundViewModel
{
    if (!_applyRefundViewModel) {
        _applyRefundViewModel = [[JYApplyRefundViewModel alloc] init];
    }
    return _applyRefundViewModel ;
}

@end
