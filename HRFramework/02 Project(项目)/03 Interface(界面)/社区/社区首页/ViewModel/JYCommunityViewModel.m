//
//  JYCommunityViewModel.m
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommunityViewModel.h"
#import "ChannelModel.h"
#import "JYCommunityModel.h"

@interface JYCommunityViewModel ()

@property (nonatomic ,copy) NSString *pageNum;        //当前页数
@property (nonatomic ,copy) NSString *pageSize;       //每页的条数

@property (nonatomic ,strong) NSMutableArray *bbsListArr;   //

@end

@implementation JYCommunityViewModel

- (void)requestPullUpLoadBBS:(NSString *)bbsId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    
    self.pageNum = [NSString stringWithFormat:@"%ld",self.pageNum.integerValue + 1];
    self.pageSize = @"10";
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:3];
    [params setObject:bbsId forKey:@"classifyId"];
    [params setObject:self.pageSize forKey:@"pageSize"];
    [params setObject:self.pageNum forKey:@"pageNum"];
    
    [manager POST_URL:JY_PATH(JY_BBS_GetBbsList) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *dataArr = [result objectForKey:@"data"];
            [self.bbsListArr removeAllObjects];
            if ([dataArr isKindOfClass:[NSArray class]]) {
                [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    JYCommunityModel *model = [JYCommunityModel mj_objectWithKeyValues:obj];
                    [self.bbsListArr addObject:model];
                }];
            }
            
            successBlock(msg,self.bbsListArr);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (void)reqeustBBS:(NSString *)bbsId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    
    self.pageNum = @"1";
    self.pageSize = @"10";
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:3];
    [params setObject:bbsId forKey:@"classifyId"];
    [params setObject:self.pageSize forKey:@"pageSize"];
    [params setObject:self.pageNum forKey:@"pageNum"];
    NSLog(@"%@",JY_PATH(JY_BBS_GetBbsList));
    [manager POST_URL:JY_PATH(JY_BBS_GetBbsList) params:params success:^(id result) {
        NSLog(@"%@",result);
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *dataArr = [result objectForKey:@"data"];
            [self.bbsListArr removeAllObjects];
            
            //一般是遍历
            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                JYCommunityModel *model = [JYCommunityModel mj_objectWithKeyValues:obj];
                [self.bbsListArr addObject:model];
            }];
            
            successBlock(msg,self.bbsListArr);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (void)requsetUnreadNumSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:1];
    
    if ([User_InfoShared shareUserInfo].c) {
        [params setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    }
    
    [manager POST_URL:JY_PATH(JY_BBS_GetUnreadNum) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSDictionary *dataDic = [result objectForKey:@"data"];
            NSString *unreadNum = SF(@"%@",[dataDic objectForKey:@"unreadNum"]);
            successBlock(msg,unreadNum);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (void)requestColumnDataSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_BBS_GetBbsClassify) params:nil success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *dataArr = [result objectForKey:@"data"];
            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
            
            //一般是遍历
            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ChannelModel *model = [ChannelModel mj_objectWithKeyValues:obj];
//                if (idx == 0) {
//                    model.isSelected = YES;
//                }
                [modelArr addObject:model];
            }];
            
            successBlock(msg,modelArr);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (NSMutableArray *)bbsListArr
{
    if (!_bbsListArr) {
        _bbsListArr = [[NSMutableArray alloc] init];
    }
    return _bbsListArr;
}

@end
