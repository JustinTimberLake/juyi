//
//  JYCommunityViewModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYCommunityViewModel : JYBaseViewModel

/**
 上拉加在帖子

 @param bbsId 帖子id
 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestPullUpLoadBBS:(NSString *)bbsId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

/**
 请求帖子

 @param bbsId 帖子ID
 @param successBlock call back
 @param failureBlock call back
 */
- (void)reqeustBBS:(NSString *)bbsId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

/**
 获取未读消息数量

 @param successBlock call back
 @param failureBlock call back
 */
- (void)requsetUnreadNumSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

/**
 获取社区主页顶部栏目数据

 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestColumnDataSuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
