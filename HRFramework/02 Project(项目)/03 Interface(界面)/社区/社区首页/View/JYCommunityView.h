//
//  JYCommunityView.h
//  JY
//
//  Created by Stronger_WM on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChannelModel;

@interface JYCommunityView : UIView

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,copy) void(^opBlock)(NSString * opType,id params);

//更新类别数据
- (void)updateChannelView:(NSArray <ChannelModel *>*)datas;

//更新未读消息数量
- (void)updateUnreadNum:(NSString *)num;

//更新列表数据
- (void)updateView:(id)datas;

//更新banner
- (void)updateADBannerView:(NSArray *)data;


@end
