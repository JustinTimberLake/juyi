//
//  JYCommunityTableViewCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYCommunityModel;

@interface JYCommunityTableViewCell : UITableViewCell

@property (nonatomic,strong) JYCommunityModel *model; //(高亮时候用)

+ (CGFloat)cellHeightAccordingModel:(JYCommunityModel *)model;

- (void)updateCellModel:(JYCommunityModel *)model;

- (void)setHighLightWithKeywords:(NSString *)keywords;

@end
