//
//  JYCommunityView.m
//  JY
//
//  Created by Stronger_WM on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommunityView.h"
#import "SDCycleScrollView.h"
#import "JYCommunityTableViewCell.h"
#import "ChannelView.h"
#import "ChannelModel.h"
#import "JYCommunityModel.h"
#import "JYHomeBannerModel.h"

static NSString *const CellId = @"JYCommunityTableViewCell";

@interface JYCommunityView ()<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,ChannelViewDelegate>

@property (nonatomic ,strong) SDCycleScrollView *bannerView;

@property (nonatomic ,strong) ChannelView *channelView;

@property (nonatomic ,strong) UIView *messageBGV;       //新消息的背景view
@property (nonatomic ,strong) UIButton *messageBtn;     //243.58
@property (nonatomic ,strong) UILabel *messCountLabel;

@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) NSArray *channelDataArr; //分类数组

@end

@implementation JYCommunityView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

#warning TestCode Begin
//测试代码-测试完毕需要删除

- (void)testData
{
    NSMutableArray *testArr = [[NSMutableArray alloc] init];
    
    NSArray *titles = @[@"全部",@"分类1",@"分类2",@"分类3",@"分类4"];
    NSArray *selecs = @[@NO,@NO ,@NO, @YES,@NO];
    [titles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ChannelModel *model = [[ChannelModel alloc] init];
        model.channelTitle = obj;
        model.isSelected = [selecs[idx] boolValue];
        [testArr addObject:model];
    }];
    
    [self.channelView updateView:testArr];
}

#warning TestCode End

//上拉加在
- (void)pullUpLoad
{
    if (self.opBlock) {
        self.opBlock(@"上拉加载",nil);
    }
}

//下拉刷新
- (void)pullDownRefresh
{
    if (self.opBlock) {
        self.opBlock(@"下拉刷新",nil);
    }
}

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.backgroundColor = [UIColor whiteColor];
    
    //column
    
    self.channelView = [[ChannelView alloc] init];
    [self addSubview:self.channelView];
    self.channelView.delegate = self;
    
    [self.channelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    //new message
    self.messageBGV = [[UIView alloc] init];
    self.messageBGV.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.messageBGV];
    
    [self.messageBGV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(50);
        make.leading.trailing.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    self.messageBtn = [[UIButton alloc] init];
    [self.messageBtn setImage:[UIImage imageNamed:@"按钮"] forState:UIControlStateNormal];
    [self.messageBtn addTarget:self action:@selector(clickNewMessBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.messageBtn bringSubviewToFront:self.messageBtn.titleLabel];
    [self.messageBGV addSubview:self.messageBtn];
    
    [self.messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(168);
        make.center.equalTo(self.messageBGV).with.centerOffset(self.messageBGV.center);
    }];
    
    self.messCountLabel = [[UILabel alloc] init];
    self.messCountLabel.textColor = [UIColor whiteColor];
    self.messCountLabel.font = FONT(14);
    self.messCountLabel.text = @"未读回复消息（5）";
    self.messCountLabel.textAlignment = NSTextAlignmentCenter;
    [self.messageBtn addSubview:self.messCountLabel];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(2, 2, 2, 2);
    [self.messCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.messageBtn).with.insets(padding);
    }];
    
    //tableview
//    self.bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HB_IPONE6_ADAPTOR_HEIGHT(190)) delegate:self placeholderImage:JY_PLACEHOLDER_IMAGE];
//    self.bannerView.backgroundColor = [UIColor whiteColor];
//    self.bannerView.infiniteLoop = YES;
//    self.bannerView.autoScroll = YES;
//    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
//    self.tableView.tableHeaderView = self.bannerView;
    [self addSubview:self.tableView];
    
    self.backgroundColor = [UIColor redColor];
    
    padding = UIEdgeInsetsMake(50, 0, 0, 0);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
    
    [self.tableView registerClass:[JYCommunityTableViewCell class] forCellReuseIdentifier:CellId];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullDownRefresh)];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(pullUpLoad)];
}

#pragma mark - ======================== Publish Methods ========================

#pragma mark - ======================== Protocol ========================

#pragma mark ********* SDCycleScrollViewDelegate *********

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    
    if (self.opBlock) {
        self.opBlock(@"banner点击",SF(@"%ld",(long)index));
    }
    NSLog(@"点击banner index = %ld",index);
}

#pragma mark ********* UITableViewDelegate *********

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYCommunityModel *model = self.dataArr[indexPath.row];
    if (self.opBlock) {
        self.opBlock(@"跳转到帖子详情",model.bbsId);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYCommunityModel *model = self.dataArr[indexPath.row];
    CGFloat rowHeight = [JYCommunityTableViewCell cellHeightAccordingModel:model];
    return rowHeight;
}

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYCommunityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    [cell updateCellModel:self.dataArr[indexPath.row]];
    return cell;
}

#pragma mark - ======================== Actions ========================

- (void)clickNewMessBtnAction:(UIButton *)btn
{
    //跳转到未读回复
    if (self.opBlock) {
        self.opBlock(@"跳转到未读回复", nil);
    }
}

#pragma mark - ======================== Update View ========================

- (void)updateChannelView:(NSArray <ChannelModel *>*)datas
{
    self.channelDataArr = datas;
    [self.channelView updateView:datas];
}

- (void)updateView:(id)datas
{
    [self.dataArr removeAllObjects];
    [self.dataArr addObjectsFromArray:datas];
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];

    [self.tableView reloadData];
}

//更新banner
- (void)updateADBannerView:(NSArray *)data{
    NSMutableArray * imageArr = [NSMutableArray array];
    for (JYHomeBannerModel * model in data) {
        [imageArr addObject:model.bannerImage];
    }
    self.bannerView.imageURLStringsGroup = imageArr;
    
}

- (void)updateUnreadNum:(NSString *)num
{
    if ([num isKindOfClass:[NSString class]]) {
        if (num.integerValue <=0) {
            [self showUnreadMessageView:NO];
        }
        else
        {
            [self showUnreadMessageView:YES];
            self.messCountLabel.text = SF(@"未读回复消息（%@）",num);
        }
    }
    else
    {
        [self showUnreadMessageView:NO];
    }
}

//显示隐藏未读消息按钮
- (void)showUnreadMessageView:(BOOL)show
{
    __block CGFloat tableTop = 0;
    
    if (show) {
        self.messageBGV.hidden = NO;
        tableTop = 100;
    }
    else
    {
        tableTop = 50;
        self.messageBGV.hidden = YES;
    }
    
    UIEdgeInsets padding = UIEdgeInsetsMake(tableTop, 0, 0, 0);
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark ==================channelViewDelegate==================
- (void)channelViewDidSelectChannelAtIndex:(NSInteger)index{
    ChannelModel * model = self.channelDataArr[index];
    if (_opBlock) {
        self.opBlock(@"分类点击", model.classifyId);
    }
}

#pragma mark - ======================== Getter ========================

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}


@end
