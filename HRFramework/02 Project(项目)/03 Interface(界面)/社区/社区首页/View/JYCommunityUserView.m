//
//  JYCommunityUserView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommunityUserView.h"
#import "JYCommunityModel.h"
#import "JYBBSModel.h"
#import "NSString+TimeStamp.h"

@interface JYCommunityUserView ()

@property (nonatomic ,strong) UIView *userInfoBgView;           //用户信息容器
@property (nonatomic ,strong) UIImageView *avatarImgView;       //头像
@property (nonatomic ,strong) UILabel *timeLabel;               //发布时间
@property (nonatomic ,strong) UILabel *nicknameLabel;           //昵称
@property (nonatomic ,strong) UIButton *momentMarkBtn;          //动态标记

@end

@implementation JYCommunityUserView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.backgroundColor = [UIColor whiteColor];
    
    //用户信息容器
    self.userInfoBgView = [[UIView alloc] init];
    [self addSubview:self.userInfoBgView];
    
    //头像
    self.avatarImgView = [[UIImageView alloc] init];
    self.avatarImgView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarImgView.layer.cornerRadius = 20;
    self.avatarImgView.clipsToBounds = YES;
    [self.userInfoBgView addSubview:self.avatarImgView];
    
    //昵称
    self.nicknameLabel = [[UILabel alloc] init];
    self.nicknameLabel.textColor = [UIColor colorWithHexString:JYUCOLOR_TITLE];
    self.nicknameLabel.font = FONT(14);
    self.nicknameLabel.textAlignment = NSTextAlignmentLeft;
    [self.userInfoBgView addSubview:self.nicknameLabel];
    
    //标记
    self.momentMarkBtn = [[UIButton alloc] init];
    self.momentMarkBtn.clipsToBounds = YES;
    self.momentMarkBtn.layer.cornerRadius = 3;
    self.momentMarkBtn.layer.borderWidth = 1;
    self.momentMarkBtn.layer.borderColor = [UIColor colorWithHexString:@"e81414"].CGColor;
    [self.momentMarkBtn setImage:[UIImage imageNamed:@"热门"] forState:UIControlStateNormal];
    [self.momentMarkBtn setTitle:@"热门" forState:UIControlStateNormal];
    self.momentMarkBtn.titleLabel.font = FONT(12);
    [self.momentMarkBtn setTitleColor:[UIColor colorWithHexString:@"e81414"] forState:UIControlStateNormal];
    [self.userInfoBgView addSubview:self.momentMarkBtn];
    
    //时间
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.font = FONT(11);
    self.timeLabel.textColor = [UIColor colorWithHexString:@"999999"];
    [self.userInfoBgView addSubview:self.timeLabel];
    
    [self configSubviewLayout];
}

#pragma mark - ======================== Publish Methods ========================

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateUserViewJYBBSModel:(JYBBSModel *)model
{
    [self.avatarImgView sd_setImageWithURL:[NSURL URLWithString:model.bbsHead] placeholderImage:DEFAULT_HEADER_IMG];
    self.nicknameLabel.text = model.bbsNick.length ? model.bbsNick : @"俥驿用户";
//    self.momentMarkBtn.hidden = YES;
//    self.timeLabel.text = [NSString YYYYMMDDHHMMWithTimevalue:model.bbsTime styleWithDot:NO];
    self.timeLabel.text = model.bbsTime;
    if (model.bbsLabel.length && [model.bbsLabel isEqualToString:@"热门"]) {
        self.momentMarkBtn.layer.borderColor = [UIColor colorWithHexString:@"e81414"].CGColor;
        [self.momentMarkBtn setImage:[UIImage imageNamed:@"热门"] forState:UIControlStateNormal];
        [self.momentMarkBtn setTitle:@"热门" forState:UIControlStateNormal];
        [self.momentMarkBtn setTitleColor:[UIColor colorWithHexString:@"e81414"] forState:UIControlStateNormal];
    }
    self.momentMarkBtn.hidden = model.bbsLabel.length ? NO : YES;
//    else if(model.bbsLabel.length ){
//        NSString * str ;
//        if ( [model.bbsLabel isEqualToString:@"上新"] ) {
//            str = @"上新";
//        }else{
//            str = @"精选";
//        }
//        self.momentMarkBtn.layer.borderColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN].CGColor;
//        [self.momentMarkBtn setImage:nil forState:UIControlStateNormal];
//        [self.momentMarkBtn setTitle:str forState:UIControlStateNormal];
//        [self.momentMarkBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
//    }

}

- (void)updateUserViewJYBBSCommentModel:(JYBBSCommentModel *)model
{
    [self.avatarImgView sd_setImageWithURL:[NSURL URLWithString:model.commentHead] placeholderImage:DEFAULT_HEADER_IMG];
    self.nicknameLabel.text = model.commentNick.length ?model.commentNick :@"俥驿用户";
    self.momentMarkBtn.hidden = YES;
//    self.timeLabel.text = [NSString YYYYMMDDHHMMWithTimevalue:model.commentTime styleWithDot:NO];
    self.timeLabel.text = model.commentTime;
}

- (void)updateUserViewJYCommunityModel:(JYCommunityModel *)model
{
    [self.avatarImgView sd_setImageWithURL:[NSURL URLWithString:model.bbsHead] placeholderImage:DEFAULT_HEADER_IMG];
    self.nicknameLabel.text = model.bbsNick.length ? model.bbsNick : @"俥驿用户";
    if (model.bbsLabel.length && [model.bbsLabel isEqualToString:@"热门"]) {
        self.momentMarkBtn.layer.borderColor = [UIColor colorWithHexString:@"e81414"].CGColor;
        [self.momentMarkBtn setImage:[UIImage imageNamed:@"热门"] forState:UIControlStateNormal];
        [self.momentMarkBtn setTitle:@"热门" forState:UIControlStateNormal];
        [self.momentMarkBtn setTitleColor:[UIColor colorWithHexString:@"e81414"] forState:UIControlStateNormal];
    }
    self.momentMarkBtn.hidden = model.bbsLabel.length ? NO : YES;
    //    else if(model.bbsLabel.length ){
//        NSString * str ;
//        if ( [model.bbsLabel isEqualToString:@"上新"] ) {
//            str = @"上新";
//        }else{
//            str = @"精选";
//        }
//        self.momentMarkBtn.layer.borderColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN].CGColor;
//        [self.momentMarkBtn setImage:nil forState:UIControlStateNormal];
//        [self.momentMarkBtn setTitle:str forState:UIControlStateNormal];
//        [self.momentMarkBtn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateNormal];
//    }
//    if (model.isHot) {
//        self.momentMarkBtn.hidden = NO;
//    }
//    else
//    {
//        self.momentMarkBtn.hidden = YES;
//    }
    
//    self.timeLabel.text = [NSString YYYYMMDDHHMMWithTimevalue:model.bbsTime styleWithDot:NO];
    self.timeLabel.text = model.bbsTime;
}

//子视图布局
- (void)configSubviewLayout
{
    //用户信息容器
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.userInfoBgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
    
    //头像
    [self.avatarImgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.equalTo(self.userInfoBgView).with.offset(self.userInfoBgView.centerY);
        make.width.height.mas_equalTo(40);
    }];
    
    //昵称
    [self.nicknameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.avatarImgView.mas_top);
        make.leading.mas_equalTo(60);
        make.height.mas_equalTo(23);
        make.width.mas_lessThanOrEqualTo(150);
    }];
    
    //标记
    [self.momentMarkBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(60);
        make.top.mas_equalTo(self.nicknameLabel.mas_bottom);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(15);
    }];
    
    //时间
    [self.timeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(-10);
        make.width.mas_equalTo(120);
        make.top.mas_equalTo(self.nicknameLabel.mas_top);
    }];
}

//- (void)setNeedsUpdateConstraints
//{
//    [super setNeedsUpdateConstraints];
//    
//    [self configSubviewLayout];
//}
//
//- (void)setNeedsDisplay
//{
//    [super setNeedsDisplay];
//    
//    [self configSubviewLayout];
//}
//
//- (void)setNeedsLayout
//{
//    [super setNeedsLayout];
//    
//    [self configSubviewLayout];
//}

#pragma mark - ======================== Getter ========================


@end
