//
//  JYCommunityTableViewCell.m
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommunityTableViewCell.h"
#import "JYCommunityUserView.h"
#import "MomentImgsView.h"
#import "JYCommunityModel.h"
#import "HR_ZoomViewVC.h"

@interface JYCommunityTableViewCell ()

@property (nonatomic ,strong) JYCommunityUserView *userView;
@property (nonatomic ,strong) UILabel *contentLabel;            //动态内容
@property (nonatomic ,strong) MomentImgsView *imgsView;         //九宫格

@end

@implementation JYCommunityTableViewCell

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.userView = [[JYCommunityUserView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    [self.contentView addSubview:self.userView];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-20, 0)];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(14);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.contentView addSubview:self.contentLabel];
}


#pragma mark - ======================== Public Methods ========================

+ (CGFloat)cellHeightAccordingModel:(JYCommunityModel *)model
{
    CGFloat userH = 70;
    //    截取60个字儿
    NSString * text;
    if (model.bbsContent.length > 60) {
        text = [model.bbsContent substringToIndex:60];
    }else{
        text = model.bbsContent;
    }
    CGFloat contentH = [text stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    CGFloat imgsH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
    if (text.length) {
        return userH + contentH + 10 + imgsH + 10;
    }else{
        return userH + imgsH + 10;
    }
}

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateCellModel:(JYCommunityModel *)model
{
    _model = model;
    WEAKSELF
    [self.userView updateUserViewJYCommunityModel:model];
    
    self.contentLabel.text = model.bbsContent;
    
//    截取60个字儿
    NSString * text;
    if (model.bbsContent.length > 60) {
        text = [model.bbsContent substringToIndex:60];
    }else{
        text = model.bbsContent;
    }
    
    CGFloat contentH = [text stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    if (text.length) {
        self.contentLabel.height = contentH + 10;
    }else{
        self.contentLabel.height = 0;
    }
    
    NSMutableArray *tempImgsArr = [[NSMutableArray alloc] initWithCapacity:model.bbsImages.count];
    [model.bbsImages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[JYImageModel class]]) {
            JYImageModel *imgModel = (JYImageModel *)obj;
            [tempImgsArr addObject:imgModel.imageUrl];
        }
    }];
    
    if (model.bbsImages.count > 0) {
        //高度+10,宽度-70
        CGFloat imgsViewH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
        
        [self.imgsView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        

        self.imgsView = [[MomentImgsView alloc] initWithFrame:CGRectMake(10, self.contentLabel.bottom + 10, SCREEN_WIDTH-20, imgsViewH) dataSource:tempImgsArr imgTapBlock:^(NSInteger imgIndex, NSArray *dataSource) {
                HR_ZoomViewVC *imgBorwserVC = [[HR_ZoomViewVC alloc] init];
                imgBorwserVC.idx = imgIndex+1;
                //        imgBorwserVC.dataSourceArrForURLsGroup = [WS.Model.GoodsModel.productImgs copy];
                imgBorwserVC.dataSourceArrForURLsGroup = dataSource;
//            暂时注释
//                imgBorwserVC.contentText = model.bbsContent;
                imgBorwserVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:imgBorwserVC animated:YES completion:nil];
        }];
        [self.contentView addSubview:self.imgsView];
    }
    else
    {
        [self.imgsView removeFromSuperview];
        self.imgsView = nil;
    }
}
- (void)setHighLightWithKeywords:(NSString *)keywords{
    [self.contentLabel setLabelTitleHighlight:self.model.bbsContent andSearchStr:keywords];
}



#pragma mark - ======================== Getter ========================


@end
