//
//  JYCommunityUserView.h
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYCommunityModel;
@class JYBBSModel;
@class JYBBSCommentModel;

@protocol JYCommunityUserViewDelegate <NSObject>

- (void)clickUserAvatar:(NSString *)userId;

@end

@interface JYCommunityUserView : UIView

@property (nonatomic ,weak) id<JYCommunityUserViewDelegate> delegate;

- (void)updateUserViewJYBBSModel:(JYBBSModel *)model;
- (void)updateUserViewJYBBSCommentModel:(JYBBSCommentModel *)model;
- (void)updateUserViewJYCommunityModel:(JYCommunityModel *)model;

@end
