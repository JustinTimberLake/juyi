//
//  JYCommunityViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/6/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYCommunityViewController.h"
#import "JYCommunityView.h"
#import "JYCommunityModel.h"
#import "JYCommunityViewModel.h"
#import "ChannelModel.h"                    //分类

#import "JYPublishMomentViewController.h"   //发布动态
#import "JYMomentDetailViewController.h"    //帖子详情
#import "JYUnreadMesReplyViewController.h"  //未读回复
#import "JYHomeViewModel.h"
#import "JYHomeBannerModel.h"
//#import "JYActivityDetailViewController.h"
//#import "JYMomentDetailViewController.h"
//#import "JYStoreListViewController.h"
#import "JYDetailViewController.h"
//#import "JYProductListViewController.h"
//#import "JYStoreViewController.h"
#import "JYCommonWebViewController.h"
#import "JYVideoListViewController.h"


@interface JYCommunityViewController ()

@property (nonatomic ,strong) JYCommunityView *rootView;
@property (nonatomic ,strong) JYCommunityViewModel *rootVM;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@property (nonatomic ,copy) NSString *currentClassifyId;        //当前分类id
@property (nonatomic,strong) NSMutableArray *categoryArr;

@property (nonatomic,strong) JYHomeViewModel *homeViewModel;

@property (nonatomic,copy) NSString *bannerIndex;
@end

@implementation JYCommunityViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //防止卡死
    self.firstLevel = YES;
    [self configUI];
//    [self config];
    [self configCategory];
    [self requestColumnData];
//    [self requestUnreadNum];
//    [self requestADBanner];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self requestUnreadNum];
    [self config];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    [self hideBackNavItem];
    
    [self rightImageItem:@"发布" action:^{
        BOOL isLogin = [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:nil];
        if (!isLogin) {
            return ;
        }
        JYPublishMomentViewController *vc = [[JYPublishMomentViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    self.rootView.frame = CGRectMake(0, 64+50, SCREEN_WIDTH, SCREEN_HEIGHT - 64-100);
    
    [self.view addSubview:self.rootView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.rootView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    
    //事件回调
    WEAK(ws);
    self.rootView.opBlock = ^(NSString *type ,id params){
        if ([type isEqualToString:@"跳转到帖子详情"]) {
            JYMomentDetailViewController *vc = [[JYMomentDetailViewController alloc] init];
            vc.bbsId = params;
            [ws.navigationController pushViewController:vc animated:YES];
        }
        if ([type isEqualToString:@"上拉加载"]) {
            [ws pullUpLoadBBS];
        }
        if ([type isEqualToString:@"下拉刷新"]) {
            [ws requestBBS];
        }
        if ([type isEqualToString:@"跳转到未读回复"]) {
            JYUnreadMesReplyViewController *vc = [[JYUnreadMesReplyViewController alloc] init];
            [ws.navigationController pushViewController:vc animated:YES];
        }
        if ([type isEqualToString:@"分类点击"]) {
            ws.currentClassifyId = params;
            [ws requestBBS];
        }
        
        if ([type isEqualToString:@"banner点击"]) {
            ws.bannerIndex = params;
            [ws bannerClickWithIndex:[ws.bannerIndex integerValue]];
        }
    };
}

#pragma mark - ======================== Public Methods ========================

- (void)bannerClickWithIndex:(NSInteger)index{
#pragma mark ------------SDCycleScrollView 代理--------------
    JYHomeBannerModel * model = self.homeViewModel.ADBannerArr[index];
    
    //(1活动详情，2帖子详情，3油站列表，4店铺列表，5油站详情，6店铺详情，7商品列表，8商品详情，9 url)
    
    //   最新的： (1商品 2是油站3 是视频4是资讯5URL)
    switch ([model.bannerType intValue]) {
        case 1:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeStore;
            detailVC.goodsId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
            
        case 2:{
            JYDetailViewController * detailVC = [[JYDetailViewController alloc] init];
            detailVC.detailType = DetailTypeAddOil;
            detailVC.shopId = model.bannerContent;
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 3:{
            JYVideoListViewController * videoListVC = [[JYVideoListViewController alloc] init];
            [self.navigationController pushViewController:videoListVC animated:YES];
        }
            break;
        case 4:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            webVC.navTitle = @"资讯详情";
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
        case 5:{
            JYCommonWebViewController * webVC = [[JYCommonWebViewController alloc] init];
            webVC.url = model.bannerContent;
            [self.navigationController pushViewController:webVC animated:YES];
        }
            break;
            
            
        default:
            break;
    }

    

}

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* specifically protocol *********

#pragma mark - ======================== Net Request ========================
//请求首页banner
- (void)requestADBanner{
    WEAKSELF
    
    [self.homeViewModel requestGetADPositionWithType:@"2" success:^(NSString *msg, id responseData) {
//        [weakSelf updataBanner];
        [weakSelf.rootView updateADBannerView:self.homeViewModel.ADBannerArr];
    } failure:^(NSString *errorMsg) {
        [weakSelf showSuccessTip:errorMsg];
    }];
}

//- (void)updataBanner{
//    NSMutableArray * imageArr = [NSMutableArray array];
//    for (JYHomeBannerModel * model in self.homeViewModel.ADBannerArr) {
//        [imageArr addObject:model.bannerImage];
//    }
//    self.scrollView.imageURLStringsGroup = imageArr;
//}



//请求帖子数据（上拉刷新）
- (void)pullUpLoadBBS
{
    if (!self.currentClassifyId) {
        [self.rootView updateView:@[]];
        [self loadData];
        return;
    }
    
//    [self showHUD];
    
    [self.rootVM requestPullUpLoadBBS:self.currentClassifyId success:^(NSString *msg, id responseData) {
        [self hideHUD];
//        [self.dataArr removeAllObjects];
        NSArray *array = [NSArray mj_objectArrayWithKeyValuesArray:responseData];
        if(array.count){
            [self.dataArr addObjectsFromArray:array];
            [self.rootView updateView:self.dataArr];
        }else{
             [self.rootView.tableView.mj_footer endRefreshing];
        }
        
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//请求帖子数据（下拉刷新）
- (void)requestBBS
{
    if (!self.currentClassifyId) {
        [self.rootView updateView:@[]];
        [self loadData];
        return;
    }
    
//    [self showHUD];
    [self.rootVM reqeustBBS:self.currentClassifyId success:^(NSString *msg, id responseData) {
        [self hideHUD];
        [self.dataArr removeAllObjects];
        [self.dataArr addObjectsFromArray:responseData];
        if(self.dataArr.count){
            
        }
        [self.rootView updateView:self.dataArr];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//请求未读消息数量
- (void)requestUnreadNum
{
    if ([User_InfoShared shareUserInfo].c.length) {
        [self.rootVM requsetUnreadNumSuccess:^(NSString *msg, id responseData) {
            [self.rootView updateUnreadNum:responseData];
//            [self.rootView updateUnreadNum:@"9999999"];
        } failure:^(NSString *errorMsg) {
            [self.rootView updateUnreadNum:nil];
            [self showSuccessTip:errorMsg];
        }];
    }
    
//    //判断登录
//    BOOL isLogin = [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
//
//    }];
//
//    if (isLogin) {
//        [self.rootVM requsetUnreadNumSuccess:^(NSString *msg, id responseData) {
//            [self.rootView updateUnreadNum:responseData];
////            [self.rootView updateUnreadNum:@"9999999"];
//        } failure:^(NSString *errorMsg) {
//            [self.rootView updateUnreadNum:nil];
//            [self showSuccessTip:errorMsg];
//        }];
//    }
}

//请求顶部类别数据
- (void)requestColumnData
{
    WEAKSELF
//    [self showHUD];
    [self.rootVM requestColumnDataSuccess:^(NSString *msg, id responseData) {
        [self hideHUD];
        if ([responseData isKindOfClass:[NSArray class]]) {
//            NSArray *tempArr = responseData;
//            if (tempArr.count > 0) {
//                ChannelModel *model = tempArr[0];
//                self.currentClassifyId = model.classifyId;
//                [self requestBBS];
//                [self.rootView updateChannelView:responseData];
//            }
            
            NSArray *tempArr = responseData;
            if (tempArr.count) {
                [weakSelf.categoryArr addObjectsFromArray:tempArr];
                [weakSelf.rootView updateChannelView:weakSelf.categoryArr];
            }
        }
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//用于初始化vm，请求当前页面数据
- (void)loadData
{
//    [self configCategory];
//    [self requestColumnData];
    [self requestUnreadNum];
//    [self requestADBanner];
}

//初始化分类的全部数据
- (void)configCategory{
    [self.categoryArr removeAllObjects];
    
    ChannelModel *model = [[ChannelModel alloc] init];
    model.channelTitle = @"全部";
    model.classifyId = @"0";
    model.isSelected = YES;
    
    [self.categoryArr addObject:model];
    self.currentClassifyId = model.classifyId;
    [self requestBBS];
    [self.rootView updateChannelView:self.categoryArr];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];
    }
    return _dataArr;
}

- (JYCommunityView *)rootView
{
    if (!_rootView) {
        _rootView = [[JYCommunityView alloc] init];
    }
    return _rootView;
}

- (JYCommunityViewModel *)rootVM
{
    if (!_rootVM) {
        _rootVM = [[JYCommunityViewModel alloc] init];
    }
    return _rootVM;
}

- (NSMutableArray *)categoryArr{
    if(!_categoryArr){
        _categoryArr = [NSMutableArray array];
    }
    return _categoryArr;
}


- (JYHomeViewModel *)homeViewModel
{
    if (!_homeViewModel) {
        _homeViewModel = [[JYHomeViewModel alloc] init];
    }
    return _homeViewModel;
}


@end
