//
//  JYCommunityModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseModel.h"
#import "JYImageModel.h"

@interface JYCommunityModel : JYBaseModel

@property (nonatomic ,assign) BOOL isHot;           //是否是热门

@property (nonatomic ,copy) NSString *bbsId;        //帖子id
@property (nonatomic ,copy) NSString *bbsNick;        //昵称
@property (nonatomic ,copy) NSString *bbsHead;        //头像
@property (nonatomic ,copy) NSString *bbsTime;        //时间
@property (nonatomic ,copy) NSString *bbsContent;        //内容
@property (nonatomic ,copy) NSString *bbsLabel;        //标签
@property (nonatomic ,strong) NSMutableArray *bbsImages;       //动态图片

@end
