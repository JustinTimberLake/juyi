//
//  JYUnreadMesReplyViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUnreadMesReplyViewController.h"
#import "JYUnreadMesReplyViewModel.h"
#import "JYBBSModel.h"
#import "JYUnreadMesTableHeaderView.h"
#import "JYReplyCommentCell.h"
#import "JYMomentDetailViewController.h"    //帖子详情

static NSString *const SectionHeaderId = @"JYUnreadMesTableHeaderView";
static NSString *const CellId = @"JYReplyCommentCell";

@interface JYUnreadMesReplyViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic ,strong) JYUnreadMesReplyViewModel *rootVM;
@property (nonatomic ,strong) NSArray *dataArr;

@end

@implementation JYUnreadMesReplyViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self config];
    [self configUI];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"未读回复";
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    [self.tableView registerClass:[JYReplyCommentCell class] forCellReuseIdentifier:CellId];
    [self.tableView registerClass:[JYUnreadMesTableHeaderView class] forHeaderFooterViewReuseIdentifier:SectionHeaderId];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UITableViewDelegate *********

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //跳转到帖子详情
    JYBBSModel *bbsModel = self.dataArr[indexPath.section];
    JYMomentDetailViewController *vc = [[JYMomentDetailViewController alloc] init];
    vc.bbsId = bbsModel.bbsId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYBBSModel *bbsModel = self.dataArr[indexPath.section];
    JYBBSCommentModel *model = [[bbsModel allComment] objectAtIndex:indexPath.row];
    return [JYReplyCommentCell cellHeightAccordingJYBBSCommentModel:model];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    JYBBSModel *bbsModel = self.dataArr[section];
    return [JYUnreadMesTableHeaderView headerHeightAccordingJYBBSModel:bbsModel];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    JYUnreadMesTableHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderId];
    WEAK(ws);
    headerView.clickHeaderBlock = ^(NSString *bbsId) {
        JYMomentDetailViewController *vc = [[JYMomentDetailViewController alloc] init];
        vc.bbsId = bbsId;
        [ws.navigationController pushViewController:vc animated:YES];
    };
    JYBBSModel *bbsModel = self.dataArr[section];
    [headerView updateViewJYBBSModel:bbsModel];
    return headerView;
}

#pragma mark ********* UITableViewDataSource *********
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    JYBBSModel *bbsModel = self.dataArr[section];
    return [bbsModel allComment].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYReplyCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    JYBBSModel *bbsModel = self.dataArr[indexPath.section];
    [cell updateCellJYBBSCommentModel:[[bbsModel allComment] objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    BOOL isLogin = [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
        
    }];
    
    if (!isLogin) {
        return;
    }
    
    [self showHUD];
    [self.rootVM requestUnreadMesReplySuccess:^(NSString *msg, id responseData) {
        [self hideHUD];
        self.dataArr = responseData;
        [self.tableView reloadData];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================

- (JYUnreadMesReplyViewModel *)rootVM
{
    if (!_rootVM) {
        _rootVM = [[JYUnreadMesReplyViewModel alloc] init];
    }
    return _rootVM;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
