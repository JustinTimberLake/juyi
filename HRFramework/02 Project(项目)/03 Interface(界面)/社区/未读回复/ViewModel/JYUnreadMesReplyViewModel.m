//
//  JYUnreadMesReplyViewModel.m
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUnreadMesReplyViewModel.h"
#import "JYBBSModel.h"

@implementation JYUnreadMesReplyViewModel

- (void)requestUnreadMesReplySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:1];
    
    if ([User_InfoShared shareUserInfo].c) {
        [params setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    }
    
    NSLog(@"%@",JY_PATH(JY_BBS_GetUnreadList));
    [manager POST_URL:JY_PATH(JY_BBS_GetUnreadList) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *dataArr = [result objectForKey:@"data"];
            NSMutableArray *modelArr = [[NSMutableArray alloc] init];
            
            //一般是遍历
            [dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                JYBBSModel *model = [JYBBSModel mj_objectWithKeyValues:obj];
                [modelArr addObject:model];
            }];
            
            successBlock(msg,modelArr);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
