//
//  JYUnreadMesReplyViewModel.h
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "HRBaseViewModel.h"

@interface JYUnreadMesReplyViewModel : HRBaseViewModel


/**
 请求未读消息

 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestUnreadMesReplySuccess:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
