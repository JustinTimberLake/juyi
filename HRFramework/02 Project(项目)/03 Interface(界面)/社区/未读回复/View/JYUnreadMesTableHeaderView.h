//
//  JYUnreadMesTableHeaderView.h
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYBBSModel;

@interface JYUnreadMesTableHeaderView : UITableViewHeaderFooterView

@property (nonatomic ,copy) void (^clickHeaderBlock)(NSString *bbsId);

+ (CGFloat)headerHeightAccordingJYBBSModel:(JYBBSModel *)model;

- (void)updateViewJYBBSModel:(JYBBSModel *)model;

@end
