//
//  JYUnreadMesTableHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUnreadMesTableHeaderView.h"
#import "JYCommunityUserView.h"
#import "MomentImgsView.h"
#import "JYBBSModel.h"
#import "HR_ZoomViewVC.h"

@interface JYUnreadMesTableHeaderView ()

@property (nonatomic ,strong) JYCommunityUserView *userView;
@property (nonatomic ,strong) UILabel *contentLabel;            //动态内容
@property (nonatomic ,strong) MomentImgsView *imgsView;         //九宫格

@property (nonatomic ,strong) JYBBSModel *bbsModel;

@end

@implementation JYUnreadMesTableHeaderView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.userView = [[JYCommunityUserView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    [self.contentView addSubview:self.userView];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-20, 0)];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(14);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.contentView addSubview:self.contentLabel];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToMomentDetail:)];
    [self addGestureRecognizer:tap];
}

#pragma mark - ======================== Public Methods ========================

+ (CGFloat)headerHeightAccordingJYBBSModel:(JYBBSModel *)model
{
    CGFloat userH = 70;
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    CGFloat imgsH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
    return userH + contentH + 10 + imgsH + 10;
}

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

//点击跳转到帖子详情
- (void)tapToMomentDetail:(UITapGestureRecognizer *)tap
{
    if (self.bbsModel.bbsId) {
        if (self.clickHeaderBlock) {
            self.clickHeaderBlock(self.bbsModel.bbsId);
        }
    }
}

#pragma mark - ======================== Update View ========================

- (void)updateViewJYBBSModel:(JYBBSModel *)model
{
    [self.userView updateUserViewJYBBSModel:model];
    
    self.bbsModel = model;
    
    self.contentLabel.text = model.bbsContent;
    
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    self.contentLabel.height = contentH + 10;
    
    if (model.bbsImages.count > 0) {
        //高度+10,宽度-70
        CGFloat imgsViewH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
        
        NSArray *imgUrlArr = [JYImgModel imgUrlArrFromArr:model.bbsImages];
        
        self.imgsView = [[MomentImgsView alloc] initWithFrame:CGRectMake(10, self.contentLabel.bottom + 10, SCREEN_WIDTH-20, imgsViewH) dataSource:imgUrlArr imgTapBlock:^(NSInteger imgIndex, NSArray *dataSource) {
                HR_ZoomViewVC *imgBorwserVC = [[HR_ZoomViewVC alloc] init];
                imgBorwserVC.idx = imgIndex+1;
                //        imgBorwserVC.dataSourceArrForURLsGroup = [WS.Model.GoodsModel.productImgs copy];
                imgBorwserVC.dataSourceArrForURLsGroup = dataSource;
                //            暂时注释
                //                imgBorwserVC.contentText = model.bbsContent;
                imgBorwserVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:imgBorwserVC animated:YES completion:nil];
        }];
        [self.contentView addSubview:self.imgsView];
        
    }
    else
    {
        [self.imgsView removeFromSuperview];
        self.imgsView = nil;
    }
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
