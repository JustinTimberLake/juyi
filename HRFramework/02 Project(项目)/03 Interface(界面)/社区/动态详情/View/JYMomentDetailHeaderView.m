//
//  JYMomentDetailHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMomentDetailHeaderView.h"
#import "JYCommunityUserView.h"
#import "MomentImgsView.h"
#import "JYCommunityModel.h"

@interface JYMomentDetailHeaderView ()

@property (nonatomic ,strong) JYCommunityUserView *userView;
@property (nonatomic ,strong) UILabel *contentLabel;            //动态内容
@property (nonatomic ,strong) MomentImgsView *imgsView;         //九宫格

@property (nonatomic ,strong) UIView *writeCommentBgView;       //评论

@end

@implementation JYMomentDetailHeaderView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.userView = [[JYCommunityUserView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    [self.contentView addSubview:self.userView];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-20, 0)];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(14);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.contentView addSubview:self.contentLabel];
}

#pragma mark - ======================== Public Methods ========================

+ (CGFloat)headerHeightAccordingModel:(JYCommunityModel *)model
{
    CGFloat userH = 70;
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    CGFloat imgsH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
    return userH + contentH + 10 + imgsH + 50 + 10;
}

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateViewModel:(JYCommunityModel *)model
{
    [self.userView updateUserViewModel:model];
    
    self.contentLabel.text = model.bbsContent;
    
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    self.contentLabel.height = contentH + 10;
    
    if (model.bbsImages.count > 0) {
        //高度+10,宽度-70
        CGFloat imgsViewH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
        
        NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:model.bbsImages.count];
        [model.bbsImages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [tempArr addObject:[obj objectForKey:@"imageUrl"]];
        }];
        
        self.imgsView = [[MomentImgsView alloc] initWithFrame:CGRectMake(10, self.contentLabel.bottom + 10, SCREEN_WIDTH-20, imgsViewH) dataSource:tempArr imgTapBlock:^(NSInteger imgIndex, NSArray *dataSource) {
            
        }];
        [self.contentView addSubview:self.imgsView];
        [self initWriteCommentBgViewTop:self.imgsView.bottom];
    }
    else
    {
        [self.imgsView removeFromSuperview];
        self.imgsView = nil;
        [self initWriteCommentBgViewTop:self.contentLabel.bottom];
    }
}

- (void)initWriteCommentBgViewTop:(CGFloat)top
{
    for (UIView *subV in self.writeCommentBgView.subviews) {
        [subV removeFromSuperview];
    }
    
    [self.writeCommentBgView removeFromSuperview];
    self.writeCommentBgView = nil;
    
    self.writeCommentBgView = [[UIView alloc] initWithFrame:CGRectMake(0, top, SCREEN_WIDTH, 50)];
    [self.contentView addSubview:self.writeCommentBgView];
    
    UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH, 30)];
    tLabel.text = @"评论";
    tLabel.font = FONT(15);
    tLabel.textAlignment = NSTextAlignmentCenter;
    [self.writeCommentBgView addSubview:tLabel];
    
    UIImageView *tImgV = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 68)/2.0, 42, 68, 3)];
    tImgV.image = [UIImage imageNamed:@"双线条"];
    [self.writeCommentBgView addSubview:tImgV];
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
