//
//  JYMomentDetailHeaderView.h
//  JY
//
//  Created by Stronger_WM on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYCommunityModel;

@interface JYMomentDetailHeaderView : UITableViewHeaderFooterView

+ (CGFloat)headerHeightAccordingModel:(JYCommunityModel *)model;

//更新界面
- (void)updateViewModel:(JYCommunityModel *)model;

@end
