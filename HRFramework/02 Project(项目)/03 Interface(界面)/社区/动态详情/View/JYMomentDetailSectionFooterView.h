//
//  JYMomentDetailSectionFooterView.h
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//  帖子详情界面回复按钮

#import <UIKit/UIKit.h>

@interface JYMomentDetailSectionFooterView : UITableViewHeaderFooterView

@property (nonatomic ,copy) void (^replyBlock)(NSInteger section);

- (void)updateSectionFooterSection:(NSInteger)section;

@end
