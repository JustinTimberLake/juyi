//
//  JYMomentDetailTableHeaderView.h
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//  帖子详情tableHeaderView

#import <UIKit/UIKit.h>

@class JYBBSModel;
@class JYCommunityModel;

@interface JYMomentDetailTableHeaderView : UIView

+ (CGFloat)headerHeightAccordingJYCommunityModel:(JYCommunityModel *)model;
+ (CGFloat)headerHeightAccordingJYBBSModel:(JYBBSModel *)model;

- (void)updateViewJYBBSModel:(JYBBSModel *)model;
- (void)updateViewJYCommunityModel:(JYCommunityModel *)model;

@end
