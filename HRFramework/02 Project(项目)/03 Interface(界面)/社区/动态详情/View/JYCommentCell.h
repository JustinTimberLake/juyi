//
//  JYCommentCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYCommunityModel;
@class JYBBSCommentModel;

@interface JYCommentCell : UITableViewCell

+ (CGFloat)cellHeightAccordingJYCommunityModel:(JYCommunityModel *)model;
+ (CGFloat)cellHeightAccordingJYBBSCommentModel:(JYBBSCommentModel *)model;

- (void)updateCellJYBBSCommentModel:(JYBBSCommentModel *)model;
- (void)updateCellJYCommunityModel:(JYCommunityModel *)model;

@end
