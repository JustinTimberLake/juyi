//
//  JYMomentDetailTableHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMomentDetailTableHeaderView.h"
#import "JYCommunityUserView.h"
#import "MomentImgsView.h"
#import "JYCommunityModel.h"
#import "JYBBSModel.h"
#import "HR_ZoomViewVC.h"

@interface JYMomentDetailTableHeaderView ()

@property (nonatomic ,strong) JYCommunityUserView *userView;
@property (nonatomic ,strong) UILabel *contentLabel;            //动态内容
@property (nonatomic ,strong) MomentImgsView *imgsView;         //九宫格

@property (nonatomic ,strong) UIView *writeCommentBgView;       //评论

@end

@implementation JYMomentDetailTableHeaderView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.userView = [[JYCommunityUserView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    [self addSubview:self.userView];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-20, 0)];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(14);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self addSubview:self.contentLabel];
}

#pragma mark - ======================== Public Methods ========================

+ (CGFloat)headerHeightAccordingJYCommunityModel:(JYCommunityModel *)model
{
    CGFloat userH = 70;
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    CGFloat imgsH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
    return userH + contentH + 10 + imgsH + 50 + 10;
}

+ (CGFloat)headerHeightAccordingJYBBSModel:(JYBBSModel *)model
{
    CGFloat userH = 70;
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    CGFloat imgsH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
    return userH + contentH + 10 + imgsH + 50 + 10;
}

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateViewJYBBSModel:(JYBBSModel *)model
{
    [self.userView updateUserViewJYBBSModel:model];
    
    self.contentLabel.text = model.bbsContent;
    
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    self.contentLabel.height = contentH + 10;
    
    if (model.bbsImages.count > 0) {
        //高度+10,宽度-70
        CGFloat imgsViewH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
        
        NSArray *imgUrlArr = [JYImgModel imgUrlArrFromArr:model.bbsImages];
        
        self.imgsView = [[MomentImgsView alloc] initWithFrame:CGRectMake(10, self.contentLabel.bottom + 10, SCREEN_WIDTH-20, imgsViewH) dataSource:imgUrlArr imgTapBlock:^(NSInteger imgIndex, NSArray *dataSource) {
                HR_ZoomViewVC *imgBorwserVC = [[HR_ZoomViewVC alloc] init];
                imgBorwserVC.idx = imgIndex+1;
                //        imgBorwserVC.dataSourceArrForURLsGroup = [WS.Model.GoodsModel.productImgs copy];
                imgBorwserVC.dataSourceArrForURLsGroup = dataSource;
            
                imgBorwserVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:imgBorwserVC animated:YES completion:nil];
        }];
        [self addSubview:self.imgsView];
        [self initWriteCommentBgViewTop:self.imgsView.bottom];
    }
    else
    {
        [self.imgsView removeFromSuperview];
        self.imgsView = nil;
        [self initWriteCommentBgViewTop:self.contentLabel.bottom];
    }
}


- (void)updateViewJYCommunityModel:(JYCommunityModel *)model
{
    [self.userView updateUserViewJYCommunityModel:model];
    
    self.contentLabel.text = model.bbsContent;
    
    CGFloat contentH = [model.bbsContent stringHeightAtWidth:SCREEN_WIDTH-20 font:FONT(14)];
    self.contentLabel.height = contentH + 10;
    
    if (model.bbsImages.count > 0) {
        //高度+10,宽度-70
        CGFloat imgsViewH = [MomentImgsView heightOfImgsAccordingImgCount:model.bbsImages.count];
        
        NSMutableArray *tempArr = [[NSMutableArray alloc] initWithCapacity:model.bbsImages.count];
        [model.bbsImages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [tempArr addObject:[obj objectForKey:@"imageUrl"]];
        }];
        
        self.imgsView = [[MomentImgsView alloc] initWithFrame:CGRectMake(10, self.contentLabel.bottom + 10, SCREEN_WIDTH-20, imgsViewH) dataSource:tempArr imgTapBlock:^(NSInteger imgIndex, NSArray *dataSource) {
            
        }];
        [self addSubview:self.imgsView];
        [self initWriteCommentBgViewTop:self.imgsView.bottom];
    }
    else
    {
        [self.imgsView removeFromSuperview];
        self.imgsView = nil;
        [self initWriteCommentBgViewTop:self.contentLabel.bottom];
    }
}

- (void)initWriteCommentBgViewTop:(CGFloat)top
{
    for (UIView *subV in self.writeCommentBgView.subviews) {
        [subV removeFromSuperview];
    }
    
    [self.writeCommentBgView removeFromSuperview];
    self.writeCommentBgView = nil;
    
    self.writeCommentBgView = [[UIView alloc] initWithFrame:CGRectMake(0, top, SCREEN_WIDTH, 50)];
    [self addSubview:self.writeCommentBgView];
    
    UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH, 30)];
    tLabel.text = @"评价";
    tLabel.font = FONT(15);
    tLabel.textAlignment = NSTextAlignmentCenter;
    [self.writeCommentBgView addSubview:tLabel];
    
    UIImageView *tImgV = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 68)/2.0, 42, 68, 3)];
    tImgV.image = [UIImage imageNamed:@"双线条"];
    [self.writeCommentBgView addSubview:tImgV];
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
