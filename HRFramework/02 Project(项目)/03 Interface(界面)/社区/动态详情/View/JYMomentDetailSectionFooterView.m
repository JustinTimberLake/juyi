//
//  JYMomentDetailSectionFooterView.m
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMomentDetailSectionFooterView.h"

@interface JYMomentDetailSectionFooterView ()

@property (nonatomic ,strong) UIButton *replyBtn;
@property (nonatomic ,assign) NSInteger mySection;

@end

@implementation JYMomentDetailSectionFooterView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.replyBtn = [[UIButton alloc] init];
    [self.replyBtn setTitle:@"回复" forState:UIControlStateNormal];
    [self.replyBtn setTitleColor:[UIColor colorWithHexString:@"666666"] forState:UIControlStateNormal];
    [self.replyBtn addTarget:self action:@selector(replyAction:) forControlEvents:UIControlEventTouchUpInside];
    self.replyBtn.titleLabel.font = FONT(12);
    [self.contentView addSubview:self.replyBtn];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, SCREEN_WIDTH - 45, 0, 15);
    [self.replyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(padding);
    }];
}

#pragma mark - ======================== Public Methods ========================

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

//回复评价
- (void)replyAction:(UIButton *)btn
{
    if (self.replyBlock) {
        self.replyBlock(self.mySection);
    }
}

#pragma mark - ======================== Update View ========================

- (void)updateSectionFooterSection:(NSInteger)section
{
    self.mySection = section;
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
