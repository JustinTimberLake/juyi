//
//  JYReplyCommentCell.h
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYBBSCommentModel;

@interface JYReplyCommentCell : UITableViewCell

+ (CGFloat)cellHeightAccordingJYBBSCommentModel:(JYBBSCommentModel *)model;
- (void)updateCellJYBBSCommentModel:(JYBBSCommentModel *)model;

@end
