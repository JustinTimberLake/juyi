//
//  JYReplyCommentCell.m
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYReplyCommentCell.h"
#import "JYBBSModel.h"

@interface JYReplyCommentCell ()

@property (nonatomic ,strong) UILabel *contentLabel;            //动态内容
@property (nonatomic ,strong) UIView *replyContentView;              //评论回复的背景
@property (nonatomic ,strong) UIImageView *arrowImg;            //上三角12*8

@end

@implementation JYReplyCommentCell

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.replyContentView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH-20, 0)];
    self.replyContentView.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    [self.contentView addSubview:self.replyContentView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 10, 0, 10);
    [self.replyContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(padding);
    }];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-40, 0)];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(13);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.replyContentView addSubview:self.contentLabel];
    
    padding = UIEdgeInsetsMake(12, 10, 8, 10);
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.replyContentView).with.insets(padding);
    }];
    
    self.arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(20, -8, 12, 8)];
    self.arrowImg.image = [UIImage imageNamed:@"上三角"];
    [self.replyContentView addSubview:self.arrowImg];
}

#pragma mark - ======================== Public Methods ========================

+ (CGFloat)cellHeightAccordingJYBBSCommentModel:(JYBBSCommentModel *)model
{
    CGFloat contentH = [model.commentContent stringHeightAtWidth:SCREEN_WIDTH-40 font:FONT(13)];
    return contentH + 20;
}

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateCellJYBBSCommentModel:(JYBBSCommentModel *)model
{
    NSMutableAttributedString *attContent = nil;
    if (model.commentReplyId) {
        //回复评论
        NSString *nick = model.commentNick;
        NSString *oNick = model.commentReplyNick;
        NSString *tempContent = [NSString stringWithFormat:@"%@回复%@：%@",nick,oNick,model.commentContent];
        attContent = [[NSMutableAttributedString alloc] initWithString:tempContent];
        [attContent addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"0089ec"] range:NSMakeRange(0, nick.length)];
        [attContent addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"0089ec"] range:NSMakeRange(nick.length+2, oNick.length)];
    }
    else
    {
        //仅评论
        NSString *nick = model.commentNick;
        NSString *tempContent = [NSString stringWithFormat:@"%@：%@",nick,model.commentContent];
        attContent = [[NSMutableAttributedString alloc] initWithString:tempContent];
        [attContent addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"0089ec"] range:NSMakeRange(0, nick.length)];
    }

    self.contentLabel.attributedText = attContent;
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
