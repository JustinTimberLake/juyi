//
//  JYMomentDetailSectionHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMomentDetailSectionHeaderView.h"
#import "JYCommunityUserView.h"
#import "JYCommunityModel.h"
#import "JYBBSModel.h"

@interface JYMomentDetailSectionHeaderView ()

@property (nonatomic ,strong) JYCommunityUserView *userView;
@property (nonatomic ,strong) UILabel *contentLabel;            //动态内容

@end

@implementation JYMomentDetailSectionHeaderView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.userView = [[JYCommunityUserView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    [self.contentView addSubview:self.userView];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-20, 0)];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = FONT(14);
    self.contentLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self.contentView addSubview:self.contentLabel];
}

#pragma mark - ======================== Public Methods ========================

+ (CGFloat)sectionheaderHeightAccordingJYBBSCommentModel:(JYBBSCommentModel *)model
{
    CGFloat userViewHeight = 70;
    CGFloat contentHeight = [model.commentContent stringHeightAtWidth:(SCREEN_WIDTH -20) font:FONT(14)];
    CGFloat gap = 10;   //内容与其上下控件的间隙
    return userViewHeight + contentHeight + gap;
}

+ (CGFloat)sectionheaderHeightAccordingJYCommunityModel:(JYCommunityModel *)model
{
    CGFloat userViewHeight = 70;
    CGFloat contentHeight = [model.bbsContent stringHeightAtWidth:(SCREEN_WIDTH -20) font:FONT(14)];
    CGFloat gap = 10;   //内容与其上下控件的间隙
    return userViewHeight + contentHeight + gap;
}

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateViewJYCommunityModel:(JYCommunityModel *)model
{
    [self.userView updateUserViewJYCommunityModel:model];
    
    self.contentLabel.text = model.bbsContent;
    self.contentLabel.height = [model.bbsContent stringHeightAtWidth:(SCREEN_WIDTH - 20) font:FONT(14)] + 10;
}

- (void)updateViewJYBBSCommentModel:(JYBBSCommentModel *)model
{
    [self.userView updateUserViewJYBBSCommentModel:model];
    
    self.contentLabel.text = model.commentContent;
    self.contentLabel.height = [model.commentContent stringHeightAtWidth:(SCREEN_WIDTH - 20) font:FONT(14)] + 10;
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
