//
//  JYMomentDetailViewModel.h
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "HRBaseViewModel.h"

@interface JYMomentDetailViewModel : HRBaseViewModel

/**
 写评论

 @param params @{@"C":@"标识",@"bbsId":@"帖子id",@"commentId":@"非必选评价id",@"commentReplyId":@"非必选回复id",@"commentContent":@"内容"}
 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestWriteComment:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

/**
 请求某个帖子的详情

 @param bbsId 帖子id
 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestBBSDetail:(NSString *)bbsId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
