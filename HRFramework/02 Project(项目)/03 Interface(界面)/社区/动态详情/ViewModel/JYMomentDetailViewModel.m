//
//  JYMomentDetailViewModel.m
//  JY
//
//  Created by Stronger_WM on 2017/8/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMomentDetailViewModel.h"
#import "JYBBSModel.h"

@implementation JYMomentDetailViewModel

- (void)requestWriteComment:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    NSLog(@"%@",JY_PATH(JY_BBS_WriteComment));
    [manager POST_URL:JY_PATH(JY_BBS_WriteComment) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            successBlock(msg,nil);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

- (void)requestBBSDetail:(NSString *)bbsId success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:1];
    
    if (bbsId) {
        [params setObject:bbsId forKey:@"bbsId"];
    }
    
    [manager POST_URL:JY_PATH(JY_BBS_GetBbsInfo) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        NSLog(@"%@",result);
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSDictionary *dataDic = [result objectForKey:@"data"];
            JYBBSModel *model = [JYBBSModel mj_objectWithKeyValues:dataDic];
            successBlock(msg,model);
        }
        else
        {
            //返回错误信息
            failureBlock(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failureBlock(ERROR_MESSAGE);
    }];
}

@end
