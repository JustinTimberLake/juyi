//
//  JYBBSModel.m
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBBSModel.h"

@implementation JYImgModel

+ (NSArray <NSString *>*)imgUrlArrFromArr:(NSArray <JYImgModel *>*)imgModelArr
{
    NSMutableArray *resultArr = [[NSMutableArray alloc] initWithCapacity:imgModelArr.count];
    [imgModelArr enumerateObjectsUsingBlock:^(JYImgModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [resultArr addObject:obj.imageUrl];
    }];
    
    return resultArr;
}

@end

@implementation JYBBSCommentModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"commentReply":@"JYBBSCommentModel"
             };
}

@end

@implementation JYBBSModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"bbsComment":@"JYBBSCommentModel",
             @"bbsImages":@"JYImgModel"
             };
}

- (NSArray <JYBBSCommentModel *>*)allComment
{
    NSMutableArray *resultArr = [[NSMutableArray alloc] init];
    for (JYBBSCommentModel *model in self.bbsComment) {
        [resultArr addObject:model];
        for (JYBBSCommentModel *tpModel in model.commentReply) {
            [resultArr addObject:tpModel];
        }
    }
    return resultArr;
}

@end
