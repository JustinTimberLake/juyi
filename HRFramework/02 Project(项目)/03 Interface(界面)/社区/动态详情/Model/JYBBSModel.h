//
//  JYBBSModel.h
//  JY
//
//  Created by Stronger_WM on 2017/8/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "HRBaseModel.h"

/**
 图片model
 */
@interface JYImgModel : HRBaseModel

@property (nonatomic ,copy) NSString *imageUrl;        //图片

+ (NSArray <NSString *>*)imgUrlArrFromArr:(NSArray <JYImgModel *>*)imgModelArr;

@end

/**
 评论model
 */
@interface JYBBSCommentModel : HRBaseModel

@property (nonatomic ,copy) NSString *commentId;        //评价id
@property (nonatomic ,copy) NSString *commentHead;        //头像
@property (nonatomic ,copy) NSString *commentNick;        //昵称
@property (nonatomic ,copy) NSString *commentTime;        //时间
@property (nonatomic ,copy) NSString *commentContent;        //内容

@property (nonatomic ,strong) NSArray *commentReply;        //回复
@property (nonatomic ,copy) NSString *commentReplyId;        //回复id
@property (nonatomic ,copy) NSString *commentReplyHead;        //被回复人的头像
@property (nonatomic ,copy) NSString *commentReplyNick;        //被回复人的昵称

@end

/**
 帖子model
 */
@interface JYBBSModel : HRBaseModel

@property (nonatomic ,copy) NSString *bbsId;        //帖子id
@property (nonatomic ,copy) NSString *bbsNick;        //昵称
@property (nonatomic ,copy) NSString *bbsHead;        //头像
@property (nonatomic ,copy) NSString *bbsTime;        //时间
@property (nonatomic ,copy) NSString *bbsContent;        //内容
@property (nonatomic ,copy) NSString *bbsLabel;        //标签

@property (nonatomic ,strong) NSArray *bbsImages;       //图片
@property (nonatomic ,strong) NSArray *bbsComment;      //评价


/**
 获取所有评论，包括评论和回复

 @return JYBBSCommentModel为元素的数组
 */
- (NSArray <JYBBSCommentModel *>*)allComment;

@end
