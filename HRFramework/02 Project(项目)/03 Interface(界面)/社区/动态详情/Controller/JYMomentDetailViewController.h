//
//  JYMomentDetailViewController.h
//  JY
//
//  Created by Stronger_WM on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//  帖子详情vc

#import "JYBaseViewController.h"

@class JYCommunityModel;        //测试用

@interface JYMomentDetailViewController : JYBaseViewController

@property (nonatomic ,copy) NSString *bbsId;        //帖子id

@end

