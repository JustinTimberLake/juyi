//
//  JYMomentDetailViewController.m
//  JY
//
//  Created by Stronger_WM on 2017/7/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMomentDetailViewController.h"
#import "JYMomentDetailViewModel.h"
#import "JYMomentDetailTableHeaderView.h"
#import "JYMomentDetailSectionHeaderView.h"
#import "JYReplyCommentCell.h"
#import "JYMomentDetailSectionFooterView.h"
#import "JYBBSModel.h"
#import "ChatKeyBoard.h"    //评论的键盘
#import "IQKeyboardManager.h"
static NSString *const cellID = @"JYReplyCommentCell";
static NSString *const sectionHeaderId = @"JYMomentDetailSectionHeaderView";
static NSString *const sectionFooterId = @"JYMomentDetailSectionFooterView";

@interface JYMomentDetailViewController ()<UITableViewDelegate,UITableViewDataSource,ChatKeyBoardDelegate,ChatKeyBoardDataSource>

@property (nonatomic ,strong) JYMomentDetailViewModel *rootVM;
@property (nonatomic ,strong) JYBBSModel *bbsModel;

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIButton *writeCommentBtn;    //写评论按钮
@property (nonatomic ,strong) JYMomentDetailTableHeaderView *headerView;

@property (nonatomic ,strong) ChatKeyBoard *chatKeyBoard;
@property (nonatomic, assign) BOOL needUpdateOffset;//控制是否刷新table的offset
@property (nonatomic, assign) CGFloat history_Y_offset;//记录table的offset.y
@property (nonatomic, assign) CGFloat seletedCellHeight;//记录点击cell的高度，高度由代理传过来

@property (nonatomic ,assign) NSInteger currentSection; //当前评论的组

@property (nonatomic ,copy) NSString *sendType;        //1.评论，2回复



@end

@implementation JYMomentDetailViewController

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configUI];
    [self config];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    [self loadData];
    
    //全局设置
    IQKeyboardManager *manger = [IQKeyboardManager sharedManager];
    manger.enable = YES;
    manger.shouldResignOnTouchOutside = YES;
    manger.shouldToolbarUsesTextFieldTintColor = YES;
    manger.enableAutoToolbar = NO;
    manger.keyboardDistanceFromTextField = 10.0f;
}


//用于初始化界面
- (void)configUI
{
    self.naviTitle = @"帖子详情";
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerClass:[JYReplyCommentCell class] forCellReuseIdentifier:cellID];
    [self.tableView registerClass:[JYMomentDetailSectionHeaderView class] forHeaderFooterViewReuseIdentifier:sectionHeaderId];
    [self.tableView registerClass:[JYMomentDetailSectionFooterView class] forHeaderFooterViewReuseIdentifier:sectionFooterId];
    
    self.writeCommentBtn = [[UIButton alloc] init];
    self.writeCommentBtn.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
    [self.writeCommentBtn setImage:[UIImage imageNamed:@"写评论"] forState:UIControlStateNormal];
    self.writeCommentBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [self.writeCommentBtn setTitle:@"写评论" forState:UIControlStateNormal];
    [self.writeCommentBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:self.writeCommentBtn];
    [self.writeCommentBtn addTarget:self action:@selector(writeCommentAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 49, 0);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    
    [self.writeCommentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.mas_equalTo(0);
        make.height.mas_equalTo(49);
    }];
}

//获取cellModel
- (JYBBSCommentModel *)cellModelAtIndexPath:(NSIndexPath *)indexPath
{
    JYBBSCommentModel *model = self.bbsModel.bbsComment[indexPath.section];
    JYBBSCommentModel *cellModel = model.commentReply[indexPath.row];
    return cellModel;
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件
- (void)writeCommentAction
{
    BOOL isLogin = [[User_InfoShared shareUserInfo] isLoginWithViewController:self LoginC:^(NSString *c) {
        
    }];
    if (!isLogin) {
        return ;
    }
    
    self.sendType = @"1";
    self.seletedCellHeight = 0.0;
    self.needUpdateOffset = YES;
    self.chatKeyBoard.placeHolder = @"评论";
    [self.chatKeyBoard keyboardUpforComment];
}

#pragma mark - ======================== Protocol ========================

#pragma mark ********* ChatKeyBoardDelegate *********

//点击输入框右侧的发送或者键盘的发送按钮，触发该代理方法
- (void)chatKeyBoardSendText:(NSString *)text
{
    if (text.length == 0) {
        [self showSuccessTip:@"请填写内容"];
        return;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:5];
    [params setObject:[User_InfoShared shareUserInfo].c forKey:@"C"];
    [params setObject:text forKey:@"commentContent"];
    [params setObject:self.bbsId forKey:@"bbsId"];
    
    if ([self.sendType isEqualToString:@"1"]) {
        //评论
        [self requestWriteComment:params];
    }
    
    if ([self.sendType isEqualToString:@"2"]) {
        //回复
        JYBBSCommentModel *model = self.bbsModel.bbsComment[self.currentSection];
        [params setObject:model.commentId forKey:@"commentId"];
        [self requestWriteComment:params];
    }
    
    //收起键盘
    [self.chatKeyBoard keyboardDownForComment];
    self.chatKeyBoard.placeHolder = nil;
}

#pragma mark ********* UITableViewDelegate *********

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYBBSCommentModel *model = [self cellModelAtIndexPath:indexPath];
    return [JYReplyCommentCell cellHeightAccordingJYBBSCommentModel:model];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    JYBBSCommentModel *model = self.bbsModel.bbsComment[section];
    return [JYMomentDetailSectionHeaderView sectionheaderHeightAccordingJYBBSCommentModel:model];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    JYBBSCommentModel *model = self.bbsModel.bbsComment[section];
    JYMomentDetailSectionHeaderView *sectionHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionHeaderId];
    [sectionHeader updateViewJYBBSCommentModel:model];
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    JYMomentDetailSectionFooterView *sectionFooter = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionFooterId];
    
    WEAK(ws);
    sectionFooter.replyBlock = ^(NSInteger section) {
        //先检查是否登录了
        BOOL isLogin = [[User_InfoShared shareUserInfo] isLoginWithViewController:ws LoginC:nil];
        if (!isLogin) {
            return ;
        }
        [ws.chatKeyBoard keyboardDownForComment];
        JYBBSCommentModel *model = ws.bbsModel.bbsComment[section];
        ws.seletedCellHeight = 0.0;
        ws.needUpdateOffset = YES;
        ws.chatKeyBoard.placeHolder = [NSString stringWithFormat:@"回复%@：",model.commentNick];
        ws.sendType = @"2";
        ws.currentSection = section;
        [ws.chatKeyBoard keyboardUpforComment];
    };
    
    [sectionFooter updateSectionFooterSection:section];
    
    return sectionFooter;
}

#pragma mark ********* UITableViewDataSource *********

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.bbsModel.bbsComment.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    JYBBSCommentModel *model = self.bbsModel.bbsComment[section];
    return model.commentReply.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JYBBSCommentModel *model = self.bbsModel.bbsComment[indexPath.section];
    JYBBSCommentModel *cellModel = model.commentReply[indexPath.row];
    
    JYReplyCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    [cell updateCellJYBBSCommentModel:cellModel];
    return cell;
}

#pragma mark - ======================== Net Request ========================

- (void)requestWriteComment:(NSDictionary *)params
{
    [self showHUD];
    [self.rootVM requestWriteComment:params success:^(NSString *msg, id responseData) {
        [self hideHUD];
        [self requestBBSDetail];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//请求帖子详情信息
- (void)requestBBSDetail
{
    [self showHUD];
    [self.rootVM requestBBSDetail:self.bbsId success:^(NSString *msg, id responseData) {
        [self hideHUD];
        self.bbsModel = responseData;
        [self updateView];
    } failure:^(NSString *errorMsg) {
        [self hideHUD];
        [self showSuccessTip:errorMsg];
    }];
}

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    [self requestBBSDetail];
}

#pragma mark - ======================== Update View ========================

- (void)updateView
{
    self.headerView = nil;
    
    //更新头部
    CGFloat h = [JYMomentDetailTableHeaderView headerHeightAccordingJYBBSModel:self.bbsModel];
    
    self.headerView = [[JYMomentDetailTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, h)];
    [self.headerView updateViewJYBBSModel:self.bbsModel];
    self.tableView.tableHeaderView = self.headerView;
    
    //update tableView
    [self.tableView reloadData];
}


#pragma mark - ======================== Getter ========================

- (JYMomentDetailViewModel *)rootVM
{
    if (!_rootVM) {
        _rootVM = [[JYMomentDetailViewModel alloc] init];
    }
    return _rootVM;
}

-(ChatKeyBoard *)chatKeyBoard{
    if (_chatKeyBoard==nil) {
        _chatKeyBoard =[ChatKeyBoard keyBoardWithNavgationBar:YES tabBar:NO];
        _chatKeyBoard.delegate = self;
        _chatKeyBoard.dataSource = self;
        _chatKeyBoard.keyBoardStyle = KeyBoardStyleComment;
        _chatKeyBoard.allowFace = YES;
        _chatKeyBoard.allowVoice = NO;
        _chatKeyBoard.allowMore = NO;
        _chatKeyBoard.allowSwitchBar = NO;
        _chatKeyBoard.placeHolder = @"评论";
        [self.view addSubview:_chatKeyBoard];
    }
    [self.view bringSubviewToFront:_chatKeyBoard];
    return _chatKeyBoard;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
