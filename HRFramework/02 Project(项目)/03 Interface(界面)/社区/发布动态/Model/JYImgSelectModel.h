//
//  JYImgSelectModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//  发布动态图片选择的模型

#import "JYBaseModel.h"

@interface JYImgSelectModel : JYBaseModel

@property (nonatomic ,assign) BOOL isAddBtn;
@property (nonatomic ,strong) id img;

#pragma mark -------------额外增加-------------
@property (nonatomic,copy) NSString *imageUrl;
@property (nonatomic,copy) NSString *imageId;

@end
