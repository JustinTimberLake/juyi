//
//  JYPublishMomentFooterView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPublishMomentFooterView.h"
#import "JYPickerViewAlertController.h"
#import "ChannelModel.h"

@interface JYPublishMomentFooterView ()

@property (weak, nonatomic) IBOutlet UIView *chooseTypeBgView;  //选择分类热点
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;        //分类标签
@property (nonatomic ,strong) NSMutableArray *typeArr;
@property (nonatomic,strong) NSMutableArray *classArr;//数组模型
@property (nonatomic,strong) JYPickerViewAlertController * alertVC;

@end

@implementation JYPublishMomentFooterView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer *chooseTypeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseType)];
    [self.chooseTypeBgView addGestureRecognizer:chooseTypeTap];
    self.typeArr = [NSMutableArray array];
//    self.typeArr = [NSArray array];
//    self.typeArr = @[@"类型1",@"类型2",@"类型3"];
}

- (void)chooseType
{
    WEAK(ws);
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    self.alertVC = alertVC;
    alertVC.pickerViewType = PickerViewTypeNormal;
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    alertVC.dataArr = self.typeArr;
    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
        ChannelModel * model = ws.classArr[selectIndex];
        ws.typeLabel.text = model.channelTitle;
        if (_selectChannelBlock) {
            ws.selectChannelBlock(model.classifyId);
        }
        
    };
    
    UIViewController *rootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootVC presentViewController:alertVC animated:YES completion:nil];
}

//更新分类数据
- (void)updataCategoryWithArr:(NSArray *)categoryArr{
    self.classArr = [NSMutableArray arrayWithArray:categoryArr];
    [self.typeArr removeAllObjects];
    if (categoryArr.count) {
        for ( ChannelModel * model in categoryArr) {
            [self.typeArr addObject:model.channelTitle];
        }
        ChannelModel * model = categoryArr[0];
        self.typeLabel.text = model.channelTitle;
        if (_selectChannelBlock) {
            self.selectChannelBlock(model.classifyId);
        }
        [self.alertVC updatePickerViewDataArr:self.typeArr type:PickerViewTypeNormal];
    }
}


- (IBAction)publishAction:(UIButton *)sender {
    if (self.publishBlock) {
        self.publishBlock();
    }
}

- (NSMutableArray *)classArr
{
    if (!_classArr) {
        _classArr = [NSMutableArray array];
    }
    return _classArr;
}

@end
