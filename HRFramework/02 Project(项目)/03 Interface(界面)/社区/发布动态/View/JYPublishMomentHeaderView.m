//
//  JYPublishMomentHeaderView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPublishMomentHeaderView.h"
#import "RFTextView.h"

@interface JYPublishMomentHeaderView ()<RFTextViewDelegate>

@property (nonatomic ,strong) RFTextView *textView;     //
@property (nonatomic ,strong) UILabel *charCountLabel;  //字数统计

@end

@implementation JYPublishMomentHeaderView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.backgroundColor = [UIColor whiteColor];
    
    self.textView = [[RFTextView alloc] init];
    self.textView.layer.borderWidth = 0;
    self.textView.delegate = self;
    self.textView.placeHolder = @"请输入内容..";
    [self addSubview:self.textView];
    
    self.charCountLabel = [[UILabel alloc] init];
    self.charCountLabel.text = @"0/500";
    self.charCountLabel.textAlignment = NSTextAlignmentRight;
    self.charCountLabel.textColor = [UIColor colorWithHexString:@"999999"];
    self.charCountLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.charCountLabel];
    self.charCountLabel.hidden = YES;
    
    UIImageView *line = [[UIImageView alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_LINE];
    [self addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(10);
        make.bottom.equalTo(self).with.offset(-30);
        make.height.mas_equalTo(1);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"添加图片";
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = FONT(15);
    titleLabel.textColor = [UIColor colorWithHexString:@"333333"];
    [self addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.bottom.mas_equalTo(0);
        make.trailing.mas_equalTo(10);
        make.height.mas_equalTo(30);
    }];
    
    [self configSubviewLayout];
}

#pragma mark - ======================== Public Methods ========================

#pragma mark - ======================== Protocol ========================

#pragma mark ********* RFTextViewDelegate *********


#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

//子视图布局
- (void)configSubviewLayout
{
    [self.textView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.leading.mas_equalTo(10);
        make.trailing.mas_equalTo(-10);
        make.bottom.mas_equalTo(-55);
    }];
    
    [self.charCountLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-31);
        make.trailing.mas_equalTo(-10);
        make.leading.mas_equalTo(10);
        make.height.mas_equalTo(24);
    }];
}

#pragma mark - ======================== Getter ========================

- (void)textViewDidChange:(UITextView *)textView{
    NSString *msg = @"";
    if (textView.text.length > 1000) {
        textView.text = [textView.text substringToIndex:1000];
        msg = @"最多1000个字";
    }
//    self.charCountLabel.text = SF(@"%lu/500",(unsigned long)textView.text.length);
    if (_returnContentBlock) {
        self.returnContentBlock(textView.text,msg);
    }
}

@end
