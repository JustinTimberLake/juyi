//
//  JYPublishImgCell.h
//  JY
//
//  Created by Stronger_WM on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYImgSelectModel;

@interface JYPublishImgCell : UICollectionViewCell

@property (nonatomic ,copy ,nonnull) void (^delImgBlock)(NSIndexPath * _Nonnull indexPath);
@property (nonatomic ,copy ,nonnull) void (^addImgBlock)();

- (void)updateCellModel:(nullable JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath;

- (void)updateCelWithImgsModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath;

@end
