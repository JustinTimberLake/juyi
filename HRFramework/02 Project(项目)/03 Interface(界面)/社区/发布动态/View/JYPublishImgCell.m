//
//  JYPublishImgCell.m
//  JY
//
//  Created by Stronger_WM on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPublishImgCell.h"
#import "JYImgSelectModel.h"

@interface JYPublishImgCell ()

@property (weak, nonatomic) IBOutlet UIView *deleteHotView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (nonatomic ,strong) NSIndexPath *currentIndexPath;    //当前位置

@end

@implementation JYPublishImgCell

- (void)updateCellModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath
{
    self.currentIndexPath = indexPath;
    
    if (model.isAddBtn) {
        self.imgView.image = [UIImage imageNamed:@"加图片+"];
        self.deleteHotView.hidden = YES;
    }
    else
    {
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl] placeholderImage:JY_PLACEHOLDER_IMAGE];
//        self.imgView.image = model.img;
        self.deleteHotView.hidden = NO;
    }
}

//里面包含着img
- (void)updateCelWithImgsModel:(JYImgSelectModel *)model atIndexPath:(nonnull NSIndexPath *)indexPath{
    self.currentIndexPath = indexPath;
    
    if (model.isAddBtn) {
        self.imgView.image = [UIImage imageNamed:@"加图片+"];
        self.deleteHotView.hidden = YES;
         self.imgView.userInteractionEnabled = YES;
        
    }
    else
    {
        self.imgView.image = model.img;
        //        self.imgView.image = model.img;
        self.deleteHotView.hidden = NO;
         self.imgView.userInteractionEnabled = NO;
        
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *delTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(delImgGesture)];
    [self.deleteHotView addGestureRecognizer:delTap];
    
    UITapGestureRecognizer *addTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImgGesture)];
    [self.imgView addGestureRecognizer:addTap];
}

- (void)addImgGesture
{
    if (self.addImgBlock) {
        self.addImgBlock();
    }
}

- (void)delImgGesture
{
    if (self.delImgBlock) {
        self.delImgBlock(self.currentIndexPath);
    }
}

@end
