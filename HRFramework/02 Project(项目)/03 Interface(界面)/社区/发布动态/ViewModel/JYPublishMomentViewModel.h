//
//  JYPublishMomentViewModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

@interface JYPublishMomentViewModel : JYBaseViewModel

/**
 发布帖子

 @param params @{@"C":@"用户标识",@"content":@"内容",@"classifyId":@"类型id",@"图片":@""}
 @param successBlock call back
 @param failureBlock call back
 */
- (void)requestPublishParams:(NSDictionary *)params success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;

@end
