
/*~!
 | @FUNC  项目基类标签控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseTabBarController.h"

@interface JYBaseTabBarController : HRBaseTabBarController

@end
