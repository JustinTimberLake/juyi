

#import "JYBaseTabBarController.h"
#import "JYHomeViewController.h"        //首页
#import "JYCommunityViewController.h"   //社区
#import "JYScanQRViewController.h"      //扫
#import "JYFindViewController.h"        //发现
#import "JYMineViewController.h"        //我的
#import "JYTabBar.h"
#import "MMScanViewController.h"

@interface JYBaseTabBarController ()<UITabBarControllerDelegate,JYTabBarDelegate>

@end

@implementation JYBaseTabBarController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ----------   Lifecycle（生命周期） ----------

+ (void)initialize
{
    // 通过appearance统一设置所有UITabBarItem的文字属性Ô
    // 后面带有UI_APPEARANCE_SELECTOR的方法, 都可以通过appearance对象来统一设置
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:11];
    attrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"666666"];
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = attrs[NSFontAttributeName];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"333333"];
    
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    [self setupChildVc:[[JYHomeViewController alloc] init]
                 title:@"首页"
                 image:@"首页"
         selectedImage:@"首页选中"];
    
    [self setupChildVc:[[JYCommunityViewController alloc] init]
                 title:@"社区"
                 image:@"社区"
         selectedImage:@"社区选中"];
    
    [self setupChildVc:[[JYFindViewController alloc] init]
                 title:@"发现"
                 image:@"发现"
         selectedImage:@"发现选中"];
    
    [self setupChildVc:[[JYMineViewController alloc] init]
                 title:@"我的"
                 image:@"我的"
         selectedImage:@"我的选中"];
    
    //改变tabbar的文字标题的高度
    [self.childViewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, 1);
    }];
    
    [self hidesBottomBarWhenPushed];
    [self setupTabBarUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // 清除内存中的图片缓存
    // 1.停止当前下载
    [[SDWebImageManager sharedManager] cancelAll];
    
    // 2.清空内存缓存
    [[SDWebImageManager sharedManager].imageCache clearMemory];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ---------- Private Methods（私有方法） ----------

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）

-(void)setupTabBarUI{
    //kvo形式添加自定义的 UITabBar
    JYTabBar *tab = [JYTabBar instanceCustomTabBarWithType:5];
    tab.backgroundImage = [UIImage imageWithColor:[UIColor colorWithHexString:@"f5f5f5"]];
    tab.centerBtnTitle = @"";
    tab.centerBtnIcon = @"扫一扫";
    tab.tabDelegate = self;
    [self setValue:tab forKeyPath:@"tabBar"];
    
    //去除顶部很丑的border
    [[UITabBar appearance] setShadowImage:[UIImage new]];
    
    //自定义分割线颜色
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(self.tabBar.bounds.origin.x-0.5, self.tabBar.bounds.origin.y, self.tabBar.bounds.size.width+1, 1)];
//    bgView.layer.borderColor = [UIColor colorWithHexString:@"dbdbdb"].CGColor;
    bgView.backgroundColor = [UIColor colorWithHexString:@"dbdbdb"];
//    bgView.layer.borderWidth = 0.5;
    [tab insertSubview:bgView atIndex:0];
    tab.opaque = YES;
}

//设置控制器的属性、标题
- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    vc.navigationItem.title = title;
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [[UIImage imageNamed:image]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.hidesBottomBarWhenPushed = NO;
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    UINavigationBar*bar=nav.navigationBar;
    [bar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    bar.tintColor = [UIColor blackColor];
    [bar setTitleTextAttributes:@{NSFontAttributeName :FONT(18),NSForegroundColorAttributeName:RGB0X(0x333333)}];

    [self addChildViewController:nav];
}

#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

#pragma mark ********* JYTabBarDelegate *********

-(void)tabBar:(JYTabBar *)tabBar clickCenterButton:(UIButton *)sender{
    
    JYScanQRViewController * sellVC = [[JYScanQRViewController alloc]init];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:sellVC];
//    UINavigationBar*bar=nav.navigationBar;
//    [bar setBackgroundImage:[UIImage imageNamed:@"导航栏背景"] forBarMetrics:UIBarMetricsDefault];
//    [bar setTitleTextAttributes:@{NSFontAttributeName :FONT(17),NSForegroundColorAttributeName:RGB0X(0xffffff)}];
//    [self presentViewController:nav animated:YES completion:nil];
//    [[UIApplication sharedApplication].keyWindow.rootViewController.navigationController pushViewController:sellVC animated:YES];
    
    MMScanViewController *scanVc = [[MMScanViewController alloc] initWithQrType:MMScanTypeQrCode onFinish:^(NSString *result, NSError *error) {
        if (error) {
            NSLog(@"error: %@",error);
        } else {
            NSLog(@"扫描结果：%@",result);
//            [self showInfo:result];
        }
    }];
    [self.navigationController pushViewController:scanVc animated:YES];
//    [self.navigationController pushViewController:scanVc animated:YES];
    scanVc.hidesBottomBarWhenPushed = YES;
    UINavigationController *nav= self.viewControllers[self.selectedIndex];
    [nav pushViewController:scanVc animated:YES];
}

#pragma mark ********* UITabBarControllerDelegate *********

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    UINavigationController *nav = (UINavigationController *)viewController;
    UIViewController *rootVC = nav.viewControllers[0];

    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{

}

@end

