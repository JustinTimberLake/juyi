//
//  ZKTabBar.h
//  ZK
//
//  Created by duanhuifen on 17/4/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JYTabBar;

@protocol JYTabBarDelegate <NSObject>

-(void)tabBar:(JYTabBar *)tabBar clickCenterButton:(UIButton *)sender;

@end
@interface JYTabBar : UITabBar
@property (nonatomic, weak) id<JYTabBarDelegate> tabDelegate;
@property (nonatomic, strong) NSString *centerBtnTitle;
@property (nonatomic, strong) NSString *centerBtnIcon;
+(instancetype)instanceCustomTabBarWithType:(int)type;
@end
