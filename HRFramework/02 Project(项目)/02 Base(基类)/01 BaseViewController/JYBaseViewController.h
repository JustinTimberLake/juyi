
/*~!
 | @FUNC  项目基类视图控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseViewController.h"
#import "PickerViewType.h"

@interface JYBaseViewController : HRBaseViewController

/**
 *  @author 王方正, 16-10-17 15:16:13
 *
 *  隐藏导航栏的返回按钮
 */
- (void)hideBackNavItem;

/**
 *  @author 王蒙, 16-08-03 16:08:39
 *
 *  导航栏放回按钮事件
 */
- (void)backAction;

/**
 *  @author 王方正, 16-10-17 15:16:13
 *
 *  小菊花
 */
- (void)showHUD;
- (void)hideHUD;

/**
 *  @author 高云鹏
 *
 *  导航栏中间按钮
 */
- (void)centerItemTitleArray:(NSArray<NSString*>*)titleArray action:(void(^)(NSInteger))action;


/**
 *  @author 王方正, 16-10-17 15:16:13
 *
 *  提示信息，1.5秒后自动消失
 *
 *  @param tipStr 要提示的信息
 */
- (void)showSuccessTip:(NSString *)tipStr;

- (void)showNoDataView;
- (void)hideNoDateView;

//支付弹窗
- (void)showAlertWithPickerViewType:(PickerViewType)pickerViewType;
//账户余额不足弹窗
- (void)showAlertPicViewWithNotEnoughMoney;

////展示用户要消费的账户余额弹窗
//- (void)showAlertPicViewWithAccountTypeWithOrderId:(NSString *)orderId;
////JYAlertPicView确定按钮点击
//@property (nonatomic,copy) void(^returnJYAlertPicViewSureBtnBlock)();

//返回支付类型
@property (nonatomic,copy) void(^returnPayTypeBlock)(NSString *str);

//正常状态的选择
@property (nonatomic,copy) void(^returnNormalBlock)(NSInteger selectIndex);


@property (nonatomic,copy) void (^dismissBlock)();


//显示alert弹窗,左右两个按钮
- (void)showAlertViewControllerWithTitle:(NSString *)title andLeftBtnStr:(NSString *)leftBtnStr andRightBtnStr:(NSString *)rightBtnStr;

//图片alert弹窗(通用)
- (void)showPicAlertViewControllerWithTitle:(NSString *)title andSureBtnTitle:(NSString *)sureBtnTitle sureBtnActionBlock:(void (^)())sureBtnActionBlock closeBtnActionBlock:(void (^)())closeBtnAction;


//图片alert删除弹窗
- (void)showDeletePicAlertViewControllerSureBtnActionBlock:(void (^)())sureBtnActionBlock closeBtnActionBlock:(void (^)())closeBtnAction;

//支付选择
- (void)showAlertPayWays;

@property (nonatomic,assign,getter=isFirstLevel) BOOL firstLevel;

@end
