

#import "JYBaseViewController.h"
#import "JYSegmentedControl.h"
#import "UIViewController+HUD.h"

#import "JYPickerViewAlertController.h"
#import "JYPaySuccessViewController.h"
#import "JYPayTool.h"
#import "JYAlertPicViewController.h"
#import "JYRechargeViewController.h"
#import "SNStarsAlertView.h"
#import "JYPayViewController.h"
@interface JYBaseViewController ()<MBProgressHUDDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) MBProgressHUD *progressHUD;

//rightItem 点击回调
@property (copy, nonatomic) void(^rightItemActionBlock)();
@property (copy, nonatomic) void(^rightItemsActionBlock)(NSInteger index);
@property (copy, nonatomic) void(^centerItemsActionBlock)(NSInteger index);
@property (nonatomic ,strong) UIView *noDataView;

//支付弹窗
@property (nonatomic,strong) JYPickerViewAlertController * alertVC ;

@property (nonatomic,assign) BOOL isCanSideBack;
@end

@implementation JYBaseViewController

- (void)hideBackNavItem
{
    self.navigationItem.leftBarButtonItem = nil;
    [self.navigationItem setHidesBackButton:YES];
}

#pragma mark - 生命周期
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //统一色板颜色
    self.view.backgroundColor = RGB0X(0Xf5f5f5);
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40)];
    view.backgroundColor = [UIColor clearColor];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 9, 12, 22)];
    imgView.image = [UIImage imageNamed:@"返回箭头"];
    [view addSubview:imgView];
    
    UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backAction)];
    [view addGestureRecognizer:backTap];
    
    UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backBarItem;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.navigationBar.translucent = NO;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //隐藏导航栏
    [self.navigationController setNavigationBarHidden:NO animated:TRUE];
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //    self.hidesBottomBarWhenPushed = NO;
    //隐藏导航栏
    //    [self.navigationController setNavigationBarHidden:YES animated:TRUE];
    
    
}

//内存警告
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // 清除内存中的图片缓存
    
    // 1.停止当前下载
    [[SDWebImageManager sharedManager] cancelAll];
    
    // 2.清空内存缓存
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    NSLog(@"⚠️⚠️⚠️%@内存警告⚠️⚠️⚠️", NSStringFromClass([self class]));
}

#pragma mark - actions
//返回按钮事件
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- 菊花
- (void)showHUD
{
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.progressHUD];
    self.progressHUD.dimBackground = NO;
    self.progressHUD.delegate = self;
    self.progressHUD.labelText = @"正在加载...";
    [self.progressHUD show:YES];
}

- (void)hideHUD
{
    [self.progressHUD hide:YES];
    [self.progressHUD removeFromSuperview];
    self.progressHUD = nil;
}

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [self.progressHUD removeFromSuperview];
    self.progressHUD = nil;
}

#pragma mark - 提示信息

- (void)showSuccessTip:(NSString *)tipStr
{
//    [MBProgressHUD showSuccess:tipStr];
    [self HUDText:tipStr];
}

//中间按钮
- (void)centerItemTitleArray:(NSArray<NSString*>*)titleArray action:(void(^)(NSInteger))action{
    self.centerItemsActionBlock = action;
    JYSegmentedControl *segMentControlView = [[JYSegmentedControl alloc]initWithFrame:CGRectMake(0, 0, 170, 44)];
    segMentControlView.titleSize = 15;
    [segMentControlView setTitles:titleArray];
    segMentControlView.selectedSegmentBlock = ^(NSInteger indexSegment) {
        self.centerItemsActionBlock(indexSegment);
    };
    self.navigationItem.titleView = segMentControlView;
   

}
//1.3.1 右按钮标题
- (void)rightItemTitle:(NSString *)title color:(UIColor *)color font:(UIFont *)font action:(void(^)())action {
    self.rightItemActionBlock = action;
    UIButton *itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [itemButton setTitle:title forState:UIControlStateNormal];
    [itemButton setTitleColor:color forState:UIControlStateNormal];
    [itemButton.titleLabel setFont:font];
    [itemButton sizeToFit];
    [itemButton addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:itemButton];
    self.navigationItem.rightBarButtonItem = item;
}


//rightItem 点击事件
- (void)rightItemAction:(UIButton *)sender {
    if (self.rightItemActionBlock) {
        self.rightItemActionBlock();
    }
    if (self.rightItemsActionBlock) {
        self.rightItemsActionBlock(sender.tag);
    }
}

//设置状态栏颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - ======================== Getter ========================

- (void)showNoDataView
{
    self.noDataView.hidden = NO;
    [self.view sendSubviewToBack:self.noDataView];
}

- (void)hideNoDateView
{
    self.noDataView.hidden = YES;
}

- (UIView *)noDataView
{
    if (!_noDataView) {
        _noDataView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _noDataView.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:_noDataView];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 88, 123)];
        imgView.image = [UIImage imageNamed:@"无数据"];
        [_noDataView addSubview:imgView];
        imgView.center = _noDataView.center;
    }
    _noDataView.hidden = YES;
    return _noDataView;
}

- (void)showAlertPayWays{
    JYPayViewController  *payVC = [[JYPayViewController alloc]init];
    payVC.SureBtnActionBlock = ^(NSInteger selectIndex) {
        switch (selectIndex) {
            case 10:
            {
                if (_returnPayTypeBlock) {
                    self.returnPayTypeBlock(@"账户支付");
                }
            }
                break;
            case 11:
            {
                if (_returnPayTypeBlock) {
                    self.returnPayTypeBlock(@"微信支付");
                }
            }
                break;
            case 12:
            {
                if (_returnPayTypeBlock) {
                    self.returnPayTypeBlock(@"支付宝支付");
                }
            }
                break;
            default:
                break;
        }
    };
    payVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:payVC animated:YES completion:nil];

}

- (void)showAlertWithPickerViewType:(PickerViewType)pickerViewType{
    JYPickerViewAlertController * alertVC = [[JYPickerViewAlertController alloc] init];
    self.alertVC = alertVC;
    alertVC.pickerViewType = pickerViewType;
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;

    alertVC.SureBtnActionBlock = ^(PickerViewType pickerViewType, NSInteger btnTag, NSInteger selectIndex) {
        switch (pickerViewType) {
            case PickerViewTypePay:{
                if (btnTag == 1001 && selectIndex == 0) {
                    if (_returnPayTypeBlock) {
                        self.returnPayTypeBlock(@"账户支付");
                    }
                }else if (btnTag == 1001 && selectIndex == 1){
                    if (_returnPayTypeBlock) {
                        self.returnPayTypeBlock(@"微信支付");
                    }
                }else{
                    if (_returnPayTypeBlock) {
                        self.returnPayTypeBlock(@"支付宝支付");
                    }
                    //                    [[JYPayTool shareInstance] payWithAlipayWithOrderString:@"" andAppScheme:@"JYUser"];
                    //                    //                    支付宝支付
                    //                    JYPaySuccessViewController * payVC = [[JYPaySuccessViewController alloc] init];
                    //                    [weakSelf.navigationController pushViewController:payVC animated:YES];
                }
            }
                break;
            case PickerViewTypeNormal:{
                if (_returnNormalBlock) {
                    self.returnNormalBlock(selectIndex);
                }
//                if (selectIndex == 0) {
//                    [weakSelf turnCreatInvoiceHeader];
//                    NSLog(@"去添加");
//                }else{
//                    if (weakSelf.invoiceViewModel.InvoiceListArr.count) {
//                        JYGetInvoiceModel * model = weakSelf.invoiceViewModel.InvoiceListArr[selectIndex - 1];
//                        weakSelf.invoiceId = model.invoiceId;
//                        weakSelf.invoiceTitle = model.invoiceTitle;
//                        [weakSelf.orderTableView reloadData];
//                    }
//                }
            }
                break;
            default:
                break;
        }
        
    };
    alertVC.dismissBlock = ^{
        if (_dismissBlock) {
            self.dismissBlock();
        }
    };
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

//账户余额不足弹窗
- (void)showAlertPicViewWithNotEnoughMoney{
    WEAKSELF
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.contentText= @"您当前的余额不足\n请充值后付款!";
    alertVC.btnText = @"去充值";
    alertVC.sureBtnActionBlock = ^(){
        
        JYRechargeViewController * rechargeVC = [[JYRechargeViewController alloc] init];
        [weakSelf.navigationController pushViewController:rechargeVC animated:YES];
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}



- (void)showAlertViewControllerWithTitle:(NSString *)title andLeftBtnStr:(NSString *)leftBtnStr andRightBtnStr:(NSString *)rightBtnStr{
    WEAKSELF
    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:leftBtnStr style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        NSURL *settingUrl = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:settingUrl]) {
            [[UIApplication sharedApplication] openURL:settingUrl];
        }
    }]];
    
    [alertVC addAction:[UIAlertAction actionWithTitle:rightBtnStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

//图片alert弹窗
- (void)showPicAlertViewControllerWithTitle:(NSString *)title andSureBtnTitle:(NSString *)sureBtnTitle sureBtnActionBlock:(void (^)())sureBtnActionBlock closeBtnActionBlock:(void (^)())closeBtnAction{
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.contentText = title;
    alertVC.btnText = sureBtnTitle;
    alertVC.sureBtnActionBlock = ^(){
        if (sureBtnActionBlock) {
            sureBtnActionBlock();
        }
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

//图片alert删除弹窗
- (void)showDeletePicAlertViewControllerSureBtnActionBlock:(void (^)())sureBtnActionBlock closeBtnActionBlock:(void (^)())closeBtnAction{
    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
    alertVC.contentText = @"确定删除？";
    alertVC.btnText = @"确定";
    alertVC.sureBtnActionBlock = ^(){
        if (sureBtnActionBlock) {
            sureBtnActionBlock();
        }
    };
    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:alertVC animated:YES completion:nil];
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.isFirstLevel) {
        [self forbiddenSideBack];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self resetSideBack];
}

//* 禁用边缘返回
-(void)forbiddenSideBack{
    self.isCanSideBack = NO;
    //关闭ios右滑返回
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

// 恢复边缘返回
- (void)resetSideBack {
    self.isCanSideBack = YES;
    //开启ios右滑返回
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer*)gestureRecognizer {
    return self.isCanSideBack;
}


@end

