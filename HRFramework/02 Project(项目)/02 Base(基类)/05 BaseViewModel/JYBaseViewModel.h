
/*~!
 | @FUNC  项目ViewModel基类
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseViewModel.h"

typedef void (^SuccessBlock)(NSString *msg,id responseData);
typedef void (^FailureBlock)(NSString *errorMsg);

@interface JYBaseViewModel : HRBaseViewModel

- (void)jkTestSuccess:(SuccessBlock)success failure:(FailureBlock)failure params:(id)params;

@end
