

#import "JYBaseViewModel.h"

@implementation JYBaseViewModel

- (void)jkTestSuccess:(SuccessBlock)success failure:(FailureBlock)failure params:(id)params
{
    HRRequestManager *manager = [[HRRequestManager alloc] init];
    [manager POST_URL:JY_PATH(JY_HOME_GetHomeType) params:params success:^(id result) {
        NSString *msg = [result objectForKey:@"errorMsg"];
        if ([[result objectForKey:@"status"] boolValue]) {
            //将数据回调
            NSArray *modelArr = nil;
            success(msg,modelArr);
        }
        else
        {
            //返回错误信息
            failure(msg);
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(ERROR_MESSAGE);
    }];
}

@end
