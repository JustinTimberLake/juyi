//
//  JYTurnSearchView.m
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYTurnSearchView.h"

@interface JYTurnSearchView ()<UITextFieldDelegate>
@property (nonatomic,strong) UIView * searchView ;
@property (nonatomic,strong) UIView * searchSquareView ;
@property (nonatomic,strong) UIImageView * searchIconImageView ;
@property (nonatomic,strong) UITextField * searchTextField ;
@property (nonatomic,strong) UIButton * searchBtn;

@end

@implementation JYTurnSearchView

#define SearchViewHeight  32
#define SearchIconW 16

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
//        self.frame = CGRectMake(35, 20, SCREEN_WIDTH - 35-50, 44);
        [self creatSearchView];
 
    }
    return  self;
}

//    创建搜索框
- (void)creatSearchView{
    UIView * searchView = [[UIView alloc]initWithFrame:self.bounds];
    searchView.backgroundColor = [UIColor clearColor];
    self.searchView = searchView;
    [self addSubview:searchView];
    
    UIView * searchSquareView = [[UIView alloc]initWithFrame:CGRectMake(0, 7, searchView.width, SearchViewHeight)];
    //    UIImageView * bgImageView = [[UIImageView alloc]initWithFrame:searchSquareView.bounds];
    //    bgImageView.image = [UIImage imageNamed:@"搜索框"];
    //    [self.searchSquareView addSubview:bgImageView];
    self.searchSquareView = searchSquareView;
    [self.searchView addSubview:searchSquareView];
    searchSquareView.layer.cornerRadius = SearchViewHeight / 2;
    searchSquareView.clipsToBounds = YES;
    searchSquareView.backgroundColor = RGB0X(0xf0f0f0);
    
    UIImageView * searchIconImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"放大镜"]];
    searchIconImageView.frame = CGRectMake(15, 7, SearchIconW, SearchIconW);
    self.searchIconImageView = searchIconImageView;
    [self.searchSquareView addSubview:searchIconImageView];
    
    UITextField * searchTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.searchIconImageView.frame) + 15, 0, self.searchSquareView.width - (CGRectGetMaxX(self.searchIconImageView.frame) + 15), SearchViewHeight)];
    self.searchTextField = searchTextField;
    [self.searchSquareView addSubview:searchTextField];
    searchTextField.font = FONT(14);
    searchTextField.textColor = RGB0X(0xb7b7b7);
    searchTextField.placeholder = @"输入关键字";
    searchTextField.enabled = NO;
    searchTextField.delegate = self;
    searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    searchTextField.returnKeyType = UIReturnKeySearch;

    UIButton * searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.searchBtn = searchBtn;
    searchBtn.frame = CGRectMake(CGRectGetMaxX(self.searchIconImageView.frame), 0, self.searchSquareView.width - self.searchIconImageView.width, SearchViewHeight);
    [self.searchSquareView addSubview:searchBtn];
    [searchBtn addTarget:self action:@selector(searchBtnAction:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)searchBtnAction:(UIButton *)btn{
    if (_startTurnSearchActionBlock) {
        _startTurnSearchActionBlock();
    }
}
- (void)setPlaceHolder:(NSString *)placeHolder{
    self.searchTextField.placeholder = placeHolder;
}

- (void)setCurrentSearch:(BOOL)currentSearch{
    self.searchTextField.enabled = currentSearch ? YES : NO;
    self.searchBtn.hidden = currentSearch ? YES : NO;
    
}

#pragma mark ==================textfieldDelegate==================

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (!textField.text.length) {
        NSLog(@"请输入搜索内容");
        return NO;
    }
    [self.searchTextField resignFirstResponder];
    
    if (_startSearchWithTextFieldBlock) {
        self.startSearchWithTextFieldBlock(textField.text);
    }
    return YES;
}

@end
