//
//  JYTurnSearchView.h
//  JY
//
//  Created by duanhuifen on 2017/6/30.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JYTurnSearchView : UIView
//开始跳转页面进行搜索
@property (nonatomic,copy) void (^startTurnSearchActionBlock)();
//当前页面搜索
@property (nonatomic,copy) void (^startSearchWithTextFieldBlock)(NSString *text);

//占位文字
@property (nonatomic,strong) NSString * placeHolder;
//是否是当前页面搜索 或者跳转页面
@property (nonatomic,assign,getter=isCurrentSearch) BOOL currentSearch;


@end
