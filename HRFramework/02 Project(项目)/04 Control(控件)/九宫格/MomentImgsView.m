//
//  MomentImgsView.m
//  NineImgs
//
//  Created by Stronger_WM on 2017/3/27.
//  Copyright © 2017年 risenb. All rights reserved.
//

#import "MomentImgsView.h"
//#import "ZKMomentModel.h"

@interface MomentImgsView ()

@property (nonatomic ,strong) NSArray *dataSource;
@property (nonatomic, copy) TapBlcok  tapBlock;
@property (nonatomic ,assign) NSInteger imgIndex;   //图片下标

@end

@implementation MomentImgsView

+ (CGFloat)heightOfImgsAccordingImgCount:(NSInteger)imgCount
{
    if (imgCount > 0) {
        NSInteger rowCount = imgCount / 3+1;
        if (imgCount % 3 == 0) {
            rowCount = rowCount-1;
        }
        CGFloat rowHeight = (SCREEN_WIDTH - 30)/3.0 + 5;
        return rowCount*rowHeight + 10;
    }
    else
    {
        return 10;
    }
}

- (instancetype)initWithFrame:(CGRect)frame dataSource:(NSArray *)dataSource imgTapBlock:(TapBlcok )tapBlock
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tapBlock = tapBlock;
        self.dataSource = [dataSource copy];
        [self configSubviews];
    }
    return self;
}

- (void)configSubviews
{
    CGFloat containterW = [UIScreen mainScreen].bounds.size.width - 20;
    
    CGFloat gap = 5;
    CGFloat imgWidth = (containterW - 2*gap)/3.0;
    CGFloat imgHeight = imgWidth;
    
    for (int i = 0; i < self.dataSource.count; i++) {
        
        NSInteger currentRow = i/3;
        NSInteger currentColumn = i%3;
        
        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.clipsToBounds = YES;
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.frame = CGRectMake(gap * currentColumn + imgWidth * currentColumn, gap * currentRow + imgHeight * currentRow, imgWidth, imgHeight);
        [self addSubview:imgView];
        
        NSString *imgSource = self.dataSource[i];
        if ([imgSource containsString:@"http"]) {
            [imgView sd_setImageWithURL:[NSURL URLWithString:imgSource] placeholderImage:[UIImage imageNamed:@"商城活动推荐位2"]];
        }
        else
        {
            imgView.image = [UIImage imageNamed:imgSource];
        }
        
        imgView.tag = i;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPicktureAction:)];
        imgView.userInteractionEnabled = YES;
        [imgView addGestureRecognizer:tap];
    }
}

#pragma mark - ======================== Actions ========================

- (void)tapPicktureAction:(UITapGestureRecognizer *)tap
{
    NSLog(@"点击了第%ld张图片",tap.view.tag);
    if (self.tapBlock) {
        self.tapBlock(tap.view.tag,self.dataSource);
    }
}

@end
