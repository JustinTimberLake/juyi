//
//  MomentImgsView.h
//  NineImgs
//
//  Created by Stronger_WM on 2017/3/27.
//  Copyright © 2017年 risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TapBlcok)(NSInteger imgIndex,NSArray *dataSource);

@interface MomentImgsView : UIView

+ (CGFloat)heightOfImgsAccordingImgCount:(NSInteger)imgCount;

- (instancetype)initWithFrame:(CGRect)frame dataSource:(NSArray *)dataSource imgTapBlock:(TapBlcok)tapBlock;

@end
