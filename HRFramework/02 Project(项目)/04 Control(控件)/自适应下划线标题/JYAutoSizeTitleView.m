//
//  JYAutoSizeTitleView.m
//  JY
//
//  Created by duanhuifen on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAutoSizeTitleView.h"

// 默认标题间距
static CGFloat const margin = 10;
// 下划线默认高度
static CGFloat const YZUnderLineH = 2;

@interface JYAutoSizeTitleView ()<UIScrollViewDelegate>

@end

@implementation JYAutoSizeTitleView
//scrollView的宽度
//#define contentWith self.bounds.size.width

- (instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        [self creatTitle];
    }
    return self;
}

- (void)creatTitle{
    NSArray * arr = @[@"demo1",@"demo1",@"demo1",@"demo1",@"demo1"];
    [self.titleArr addObjectsFromArray:arr];
    
    _selIndex = -1;
    
    UIScrollView * titleScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    self.titleScrollView = titleScrollView;
    [self addSubview:titleScrollView];
    titleScrollView.delegate = self;
    
    UIView *underLineView = [[UIView alloc] init];
//    underLineView.backgroundColor = _underLineColor?_underLineColor:[UIColor redColor];
    [self.titleScrollView addSubview:underLineView];
    self.underLine = underLineView;
    
    self.titleHeight = self.bounds.size.height;
    
    self.norColor = self.norColor ? self.norColor : [UIColor darkGrayColor];
    self.selColor = self.selColor ? self.selColor :[UIColor redColor];
    self.isShowUnderLine =  YES ;
    self.underLineH = self.underLineH ? self.underLineH : 1;
    self.underLineColor = self.underLineColor ? self.underLineColor:[UIColor redColor];
    self.isUnderLineEqualTitleWidth = YES ;
    self.underLine.backgroundColor =  self.underLineColor? self.underLineColor :[UIColor redColor];

//    [self setUpTitleWidth];
//    [self setUpAllTitle];
}
//重写属性
- (void)updataTitleData:(NSArray *)titleArr{
    [self.titleArr removeAllObjects];
    [self.titleArr addObjectsFromArray:titleArr];
    [self setUpTitleWidth];
    [self setUpAllTitle];
}


#pragma mark - 添加标题方法
// 计算所有标题宽度
- (void)setUpTitleWidth
{
    [self.titleWidths removeAllObjects];
    // 判断是否能占据整个屏幕
    NSUInteger count = self.titleArr.count;
    
    CGFloat totalWidth = 0;
    
    // 计算所有标题的宽度
    for (NSString *title in self.titleArr) {
        
        CGRect titleBounds = [title boundingRectWithSize:CGSizeMake(MAXFLOAT, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.fontSize} context:nil];
        
        CGFloat width = titleBounds.size.width;
        
        [self.titleWidths addObject:@(width)];
        
        totalWidth += width;
    }
    
    if (totalWidth > self.bounds.size.width) {
        
        _titleMargin = margin;
        
        self.titleScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, _titleMargin);
        
        return;
    }
    
    CGFloat titleMargin = (self.bounds.size.width - totalWidth) / (count + 1);
    
    _titleMargin = titleMargin < margin? margin: titleMargin;
    
    self.titleScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, _titleMargin);
}


// 设置所有标题
- (void)setUpAllTitle
{
    self.underLine.backgroundColor =  self.underLineColor? self.underLineColor :[UIColor redColor];
    [self.titleLabels removeAllObjects];
    [self.titleScrollView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj isKindOfClass:[UILabel class]]) {
            [obj removeFromSuperview];
        }
    }];
    // 遍历所有的子控制器
    NSUInteger count = self.titleArr.count;
    
    // 添加所有的标题
    CGFloat labelW = _titleWidth;
    CGFloat labelH = self.titleHeight;
    CGFloat labelX = 0;
    CGFloat labelY = 0;
    
    for (int i = 0; i < count; i++) {
        
        
        UILabel *label = [[UILabel alloc] init];
        
        label.tag = i;
        
        // 设置按钮的文字颜色
        label.textColor = self.norColor;
        
        label.font = self.fontSize;
        
        // 设置按钮标题
        label.text = self.titleArr[i];
        
        labelW = [self.titleWidths[i] floatValue];
        
        // 设置按钮位置
        UILabel *lastLabel = [self.titleLabels lastObject];
        
        labelX = _titleMargin + CGRectGetMaxX(lastLabel.frame);
        
        label.frame = CGRectMake(labelX, labelY, labelW, labelH);
        
        label.userInteractionEnabled = YES;
        
        // 监听标题的点击
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(titleClick:)];
        [label addGestureRecognizer:tap];
        
        // 保存到数组
        [self.titleLabels addObject:label];
        
        [_titleScrollView addSubview:label];
        
        if (i == _selectIndex) {
            [self titleClick:tap];
        }
    }
    
    // 设置标题滚动视图的内容范围
    UILabel *lastLabel = self.titleLabels.lastObject;
    _titleScrollView.contentSize = CGSizeMake(CGRectGetMaxX(lastLabel.frame), 0);
    _titleScrollView.showsHorizontalScrollIndicator = NO;
    
}
// 标题按钮点击
- (void)titleClick:(UITapGestureRecognizer *)tap
{
    NSLog(@"点击了");
    // 记录是否点击标题
    _isClickTitle = YES;
    
    // 获取对应标题label
    UILabel *label = (UILabel *)tap.view;
    
    // 获取当前角标
    NSInteger i = label.tag;
    
    
    for (UILabel * label in self.titleLabels) {
        label.textColor = self.norColor;
    }
    
    // 选中label
    [self selectLabel:label];
    
    // 内容滚动视图滚动到对应位置
    CGFloat offsetX = i * self.bounds.size.width;
    
    // 记录上一次偏移量,因为点击的时候不会调用scrollView代理记录，因此需要主动记录
    _lastOffsetX = offsetX;
    if (i == _selIndex) {
        return;
    }
    
    _selIndex = i;
    
    // 点击事件处理完成
    _isClickTitle = NO;
    
    
    
    
    if (_didSelectItemsWithIndexBlock) {
        self.didSelectItemsWithIndexBlock(i);
    }
}

- (void)selectLabel:(UILabel *)label
{
    // 修改标题选中颜色
    label.textColor = self.selColor;
    
    // 设置标题居中
    [self setLabelTitleCenter:label];
    
    // 设置下标的位置
    if (_isShowUnderLine) {
        [self setUpUnderLine:label];
    }
    
}

// 设置下标的位置
- (void)setUpUnderLine:(UILabel *)label
{
    // 获取文字尺寸
    CGRect titleBounds = [label.text boundingRectWithSize:CGSizeMake(MAXFLOAT, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.fontSize} context:nil];
    
    CGFloat underLineH = _underLineH?_underLineH:YZUnderLineH;
    
    self.underLine.y = label.height - underLineH;
    self.underLine.height = underLineH;
    
    
    // 最开始不需要动画
    if (self.underLine.x == 0) {
        if (_isUnderLineEqualTitleWidth) {
            self.underLine.width = titleBounds.size.width;
        } else {
            self.underLine.width = label.width;
        }
        
        self.underLine.centerX = label.centerX;
        return;
    }
    
    // 点击时候需要动画
    [UIView animateWithDuration:0.25 animations:^{
        if (_isUnderLineEqualTitleWidth) {
            self.underLine.width = titleBounds.size.width;
        } else {
            self.underLine.width = label.width;
        }
        self.underLine.centerX = label.centerX;
    }];
    
}

// 让选中的按钮居中显示
- (void)setLabelTitleCenter:(UILabel *)label
{
    
    // 设置标题滚动区域的偏移量
    CGFloat offsetX = label.center.x - self.bounds.size.width * 0.5;
    
    if (offsetX < 0) {
        offsetX = 0;
    }
    
    // 计算下最大的标题视图滚动区域
    CGFloat maxOffsetX = self.titleScrollView.contentSize.width - self.bounds.size.width + _titleMargin;
    
    if (maxOffsetX < 0) {
        maxOffsetX = 0;
    }
    
    if (offsetX > maxOffsetX) {
        offsetX = maxOffsetX;
    }
    
    // 滚动区域
    [self.titleScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
    
}

- (NSMutableArray *)titleArr
{
    if (!_titleArr) {
        _titleArr = [NSMutableArray array];
    }
    return _titleArr;
}

- (NSMutableArray *)titleWidths
{
    if (_titleWidths == nil) {
        _titleWidths = [NSMutableArray array];
    }
    return _titleWidths;
}

- (NSMutableArray *)titleLabels
{
    if (_titleLabels == nil) {
        _titleLabels = [NSMutableArray array];
    }
    return _titleLabels;
}

- (UIView *)underLine
{
    if (_underLine == nil) {
        
      
        
    }
    return _isShowUnderLine?_underLine : nil;
}

@end
