//
//  JYAutoSizeTitleView.h
//  JY
//
//  Created by duanhuifen on 2017/7/11.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYAutoSizeTitleView : UIView
@property (nonatomic,strong) UIScrollView * titleScrollView;
@property (nonatomic,strong) NSMutableArray * titleArr;
@property (nonatomic,strong) NSMutableArray * titleWidths;
/** 标题间距 */
@property (nonatomic, assign) CGFloat titleMargin;
/** 标题宽度 */
@property (nonatomic, assign) CGFloat titleWidth;
/** 标题高度 */
@property (nonatomic, assign) CGFloat titleHeight;
/** 正常标题颜色 */
@property (nonatomic, strong) UIColor *norColor;
/** 选中标题颜色 */
@property (nonatomic, strong) UIColor *selColor;

@property (nonatomic,strong) NSMutableArray * titleLabels;
/** 根据角标，选中对应的控制器 */
@property (nonatomic, assign) NSInteger selectIndex;

/** 计算上一次选中角标 */
@property (nonatomic, assign) NSInteger selIndex;

/** 记录上一次内容滚动视图偏移量 */
@property (nonatomic, assign) CGFloat lastOffsetX;

/** 记录是否点击 */
@property (nonatomic, assign) BOOL isClickTitle;
/** 是否需要下标*/
@property (nonatomic, assign) BOOL isShowUnderLine;
/** 下标颜色 */
@property (nonatomic, strong) UIColor *underLineColor;
/** 下标高度*/
@property (nonatomic, assign) CGFloat underLineH;
//下划线
@property (nonatomic,strong) UIView * underLine;
//文字大小
@property (nonatomic,strong) UIFont * fontSize;

/** 下标宽度是否等于标题宽度 */
@property (nonatomic, assign) BOOL isUnderLineEqualTitleWidth;

- (void)updataTitleData:(NSArray *)titleArr;

@property (nonatomic,copy) void (^didSelectItemsWithIndexBlock)(NSInteger index);

@end
