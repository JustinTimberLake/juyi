
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HRHUDPosition) {
    HRHUDPositionCenter,    // 中
    HRHUDPositionBottom,    // 下
    HRHUDPositionTop,       // 上
};

@interface UIViewController (HUD)

//文本停留时间，默认2s
@property (assign, nonatomic) NSTimeInterval hudTextShowTime;

//文本停留位置，默认 HRHUDPositionCenter
@property (assign, nonatomic) HRHUDPosition hudPositon;

/**
 提示文本信息
 */
- (void)HUDText:(NSString *)text;

/**
 提示文本信息

 @param text content
 @param completion completion
 */
- (void)HUDText:(NSString *)text completion:(void (^)())completion;

/**
 显示菊花
 */
- (void)showHUD;

/**
 隐藏菊花
 */
- (void)hideHUD;

@end
