

#import "UIViewController+HUD.h"
#import "MBProgressHUD.h"

static char *hudTextShowTimeKey = "hudTextShowTimeKey";
static char *hudPositonKey = "hudPositonKey";
static char *HUDKey = "HUDKey";
static char *hudCountKey = "hudCountKey";

@interface UIViewController ()

@property (strong, nonatomic) MBProgressHUD *HUD;
@property (assign, nonatomic) NSInteger hudCount;

@end

@implementation UIViewController (HUD)

#pragma mark - ---------- setter && getter ----------

- (NSTimeInterval)hudTextShowTime {
    return [objc_getAssociatedObject(self, hudTextShowTimeKey) floatValue];
}
- (void)setHudTextShowTime:(NSTimeInterval)hudTextShowTime {
    objc_setAssociatedObject(self, hudTextShowTimeKey, @(hudTextShowTime), OBJC_ASSOCIATION_ASSIGN);
}
- (HRHUDPosition)hudPositon {
    return (HRHUDPosition)[objc_getAssociatedObject(self, hudPositonKey) integerValue];
}
- (void)setHudPositon:(HRHUDPosition)hudPositon {
    return objc_setAssociatedObject(self, hudPositonKey, @(hudPositon), OBJC_ASSOCIATION_ASSIGN);
}
- (void)setHUD:(MBProgressHUD *)HUD {
    objc_setAssociatedObject(self, HUDKey, HUD, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (MBProgressHUD *)HUD {
    return objc_getAssociatedObject(self, HUDKey);
}

- (void)setHudCount:(NSInteger )hudCount {
    objc_setAssociatedObject(self, hudCountKey, @(hudCount), OBJC_ASSOCIATION_ASSIGN);
}

- (NSInteger)hudCount {
    return [objc_getAssociatedObject(self, hudCountKey) integerValue];
}

#pragma mark - ---------- Public Methods ----------

- (void)HUDText:(NSString *)text {
    if (!text.length) {
        return;
    }
    __block MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = text;
    hud.mode = MBProgressHUDModeText;
    
    switch (self.hudPositon) {
    case HRHUDPositionTop:
        hud.yOffset = - [UIScreen mainScreen].bounds.size.height / 2 + 100;
        break;
    case HRHUDPositionCenter:
            hud.yOffset = 0;
        break;
    case HRHUDPositionBottom:
            hud.yOffset = [UIScreen mainScreen].bounds.size.height / 2 - 100;
        break;
    }
    
    [hud show:YES];
    hud.animationType = MBProgressHUDAnimationFade;
    hud.userInteractionEnabled = NO;
    
    [hud showAnimated:YES whileExecutingBlock:^{
        //对话框显示时需要执行的操作，子线程
        if (self.hudTextShowTime > 0.001) {
            sleep(self.hudTextShowTime);
        } else {
            sleep(1.25);
        }
    } completionBlock:^{
        [hud removeFromSuperview];
        hud = nil;
        //操作执行完后取消对话框,主线程
    }];
}

- (void)HUDText:(NSString *)text completion:(void (^)())completion {
    if (!text.length) {
        return;
    }
    __block MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = text;
    hud.mode = MBProgressHUDModeText;
    
    switch (self.hudPositon) {
        case HRHUDPositionTop:
            hud.yOffset = - [UIScreen mainScreen].bounds.size.height / 2 + 100;
            break;
        case HRHUDPositionCenter:
            hud.yOffset = 0;
            break;
        case HRHUDPositionBottom:
            hud.yOffset = [UIScreen mainScreen].bounds.size.height / 2 - 100;
            break;
    }
    
    [hud show:YES];
    hud.animationType = MBProgressHUDAnimationFade;
    hud.userInteractionEnabled = NO;
    
    [hud showAnimated:YES whileExecutingBlock:^{
        //对话框显示时需要执行的操作，子线程
        if (self.hudTextShowTime > 0.001) {
            sleep(self.hudTextShowTime);
        } else {
            sleep(1.25);
        }
    } completionBlock:^{
        //操作执行完后取消对话框,主线程
        [hud removeFromSuperview];
        hud = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion();
            }
        });
    }];
}

- (void)showHUD {
    if (self.hudCount) {
        self.hudCount++;
    } else {
        GCD_AS_M_Q(^{
            if (!self.HUD) {
                self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
                [self.view addSubview:self.HUD];
                self.HUD.opacity = 0.7;
                self.HUD.labelText = @"加载中...";
            }
            //如果设置此属性则当前的view置于后台
            self.HUD.dimBackground = NO;
            [self.HUD show:YES];
            self.hudCount = 1;
        })
    }
}

- (void)hideHUD {
    self.hudCount--;
    if (self.hudCount <= 0) {
        GCD_AS_M_Q(^{
            [self.HUD removeFromSuperview];
            self.HUD = nil;
        });
    }
}


@end
