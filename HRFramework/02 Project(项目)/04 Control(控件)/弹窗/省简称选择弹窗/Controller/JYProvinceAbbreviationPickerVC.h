//
//  JYProvinceAbbreviationPickerVC.h
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYProvinceAbbreviationPickerVC : JYBaseViewController

@property (nonatomic ,copy) void (^resultBlock)(NSString * result);

@end
