//
//  JYProvinceAbbreviationPickerVC.m
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYProvinceAbbreviationPickerVC.h"
#import "JYProvinceAbbreviationItem.h"

static NSString *const ItemId = @"JYProvinceAbbreviationItem";

@interface JYProvinceAbbreviationPickerVC ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>

@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) UICollectionViewFlowLayout *layout;

@property (nonatomic ,strong) NSArray *dataArr;

@end

@implementation JYProvinceAbbreviationPickerVC

#pragma mark - ======================== Life Cycle ========================

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configUI];
    [self config];
}

#pragma mark ********* Memeory Warning *********

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//用于初始化一些配置(注册通知、注册观察者…)
- (void)config
{
    //初始化vm
    
    [self loadData];
}

//用于初始化界面
- (void)configUI
{
    UIColor *bgColor = [UIColor blackColor];
    
    self.view.backgroundColor = [bgColor colorWithAlphaComponent:0.3];
    
    self.layout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat itemWidth = (SCREEN_WIDTH - 8*6 - 5*2)/9.0;
    self.layout.itemSize = CGSizeMake(itemWidth, itemWidth +5);
    self.layout.minimumLineSpacing = 15;
    self.layout.minimumInteritemSpacing = 6;
    self.layout.sectionInset = UIEdgeInsetsMake(9, 5, 10, 5);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.layout];
    self.collectionView.backgroundColor = [UIColor colorWithHexString:@"ccd0d4"];
    [self.view addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    CGFloat cTop = SCREEN_HEIGHT - 216;
    UIEdgeInsets padding = UIEdgeInsetsMake(cTop, 0, 0, 0);
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    
    [self.collectionView registerNib:[UINib nibWithNibName:ItemId bundle:nil] forCellWithReuseIdentifier:ItemId];
}

#pragma mark - ======================== Public Methods ========================

//.h里面公开的api


#pragma mark - ======================== Actions ========================

//ui事件，timer事件，noti事件，gesture事件

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UICollectionViewDelegateFlowLayout *********

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.resultBlock) {
        self.resultBlock(self.dataArr[indexPath.row]);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ********* UICollectionViewDataSource *********

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JYProvinceAbbreviationItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ItemId forIndexPath:indexPath];
    cell.itemLabel.text = self.dataArr[indexPath.row];
    cell.clipsToBounds = YES;
    cell.layer.cornerRadius = 3;
    return cell;
}

#pragma mark - ======================== Net Request ========================

//用于初始化vm，请求当前页面数据
- (void)loadData
{
    self.dataArr = @[
                     @"京",@"津",@"冀",@"晋",@"蒙",@"辽",@"吉",@"黑",@"沪",
                     @"苏",@"浙",@"皖",@"闽",@"赣",@"鲁",@"豫",@"鄂",@"湘",
                     @"粤",@"桂",@"琼",@"川",@"贵",@"云",@"渝",@"藏",@"陕",
                     @"甘",@"青",@"宁",@"新",@"港",@"澳",@"台",
                     ];
    
    [self.collectionView reloadData];
}

#pragma mark - ======================== Update View ========================

//请求成功更新界面的api

#pragma mark - ======================== Getter ========================



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
