//
//  JYProvinceAbbreviationItem.h
//  JY
//
//  Created by Stronger_WM on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYProvinceAbbreviationItem : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemLabel;

@end
