//
//  JYAlertPicViewController.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewController.h"

@interface JYAlertPicViewController : HRBaseViewController

@property (nonatomic,strong) NSString * contentText;
@property (nonatomic,copy) NSString * btnText;
//黄色确定按钮点击
@property (nonatomic,copy) void (^sureBtnActionBlock) () ;
@end
