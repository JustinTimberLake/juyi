//
//  JYAlertPicView.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYAlertPicView : UIView
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

- (void)showAlertPicView;
- (void)hideAlertPicView;
@end
