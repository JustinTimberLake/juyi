//
//  JYPickerViewAlertView.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYPickerViewAlertView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
