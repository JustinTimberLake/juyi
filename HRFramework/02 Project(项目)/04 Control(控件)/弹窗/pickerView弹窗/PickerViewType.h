//
//  PickerViewType.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#ifndef PickerViewType_h
#define PickerViewType_h


typedef NS_ENUM(NSInteger,PickerViewType) {
    PickerViewTypeNormal,//默认只显示文字的
    PickerViewTypePay,  //支付类型
    PickerViewTypeProCity//省市区类型

};


#endif /* PickerViewType_h */
