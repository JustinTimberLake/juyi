//
//  JYDatePickerViewController.m
//  JY
//
//  Created by risenb on 2017/9/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYDatePickerViewController.h"

@interface JYDatePickerViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;
@property (nonatomic,strong) NSDate *currentDate;
@property (nonatomic,copy) NSString *dataStr;
@end

@implementation JYDatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datePickerView.datePickerMode = UIDatePickerModeDate;
    self.datePickerView.locale =  [NSLocale localeWithLocaleIdentifier:@"zh_CN"];
    [self.datePickerView addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    [self consultDateWithDate:[NSDate date]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bottomBtnAction:(UIButton *)sender {
    WEAKSELF
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //点击取消
    if (sender.tag == 1000) {
        return;
    }
    if (sender.tag == 1001) {
        if (_sureBtnAction) {
            weakSelf.sureBtnAction(self.currentDate,self.dataStr);
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dateChange:(UIDatePicker *)datePicker{
    [self consultDateWithDate:datePicker.date];
//    NSDate *theDate = datePicker.date;
//    self.currentDate = theDate;
//    NSLog(@"%@",[theDate descriptionWithLocale:[NSLocale currentLocale]]);
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"YYYY.MM.dd";
//    self.dataStr = [dateFormatter stringFromDate:theDate];
//    NSLog(@"%@",self.dataStr);
}

- (void)consultDateWithDate:(NSDate *)date{
    self.currentDate = date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY.MM.dd";
    self.dataStr = [dateFormatter stringFromDate:date];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
