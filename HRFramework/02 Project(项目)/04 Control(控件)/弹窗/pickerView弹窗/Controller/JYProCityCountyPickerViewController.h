//
//  JYProCityCountyPickerViewController.h
//  JY
//
//  Created by risenb on 2017/9/19.
//  Copyright © 2017年 Risenb. All rights reserved.
//

//#import "HRBaseViewController.h"
#import "JYBaseViewController.h"

typedef NS_ENUM(NSInteger,JYPickType) {
    JYPickType_NotRequest,
    JYPickType_Request
};

@interface JYProCityCountyPickerViewController : JYBaseViewController
//省市区的时候显示几列
@property (nonatomic,assign) NSInteger proCityRow;

@property (nonatomic,assign) JYPickType pickType;

- (void)updatePickerViewDataArr:(NSArray *)aArray;

//底部按钮点击
@property (nonatomic,copy) void(^bottomBtnAction)(NSString * brandName,NSString *carXiName,NSString * carXingName,NSString * carXingId);
@end
