//
//  JYDatePickerViewController.h
//  JY
//
//  Created by risenb on 2017/9/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "HRBaseViewController.h"

@interface JYDatePickerViewController : HRBaseViewController

//只显示年份
//@property (nonatomic,assign,getter=isOnlyYear) BOOL onlyYear;

@property (nonatomic,copy) void(^sureBtnAction)(NSDate *date,NSString * dateStr);
@end
