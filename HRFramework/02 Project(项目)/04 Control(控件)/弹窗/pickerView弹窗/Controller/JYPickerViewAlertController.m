//
//  JYPickerViewAlertController.m
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPickerViewAlertController.h"
#import "JYPickerViewAlertView.h"
#import "JYJYPickerViewPayStyleView.h"
#import "JYProCityCountyModel.h"
#import "JYCityModel.h"
#import "JYCountyModel.h"

//#import "JYAlertPicViewController.h"
//#import "JYAlertPicView.h"

@interface JYPickerViewAlertController ()<
    UIPickerViewDelegate,
    UIPickerViewDataSource
    >
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBtnH;

//支付模式数组
@property (nonatomic,strong) NSArray * payStyleTitleArr;
@property (nonatomic,strong) NSArray * payStyleImageArr;

@property (nonatomic, assign) NSInteger selectIndex;
//省市区模式下使用
@property (nonatomic, assign) NSUInteger provinceIndex;
@property (nonatomic, assign) NSUInteger cityIndex;
@property (nonatomic, assign) NSUInteger areaIndex;
@property (nonatomic,strong) NSArray *proCityArr;



//@property (nonatomic,strong) JYAlertPicView * alertPicView;

@end

@implementation JYPickerViewAlertController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.proCityRow = self.proCityRow > 0 ?self.proCityRow :3;
    self.bottomBtnH.constant = self.IsHideBottomBtn ? 0 : 49;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma mark --------------action -------------------------------

- (IBAction)bottomBtnAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //点击取消
    if (sender.tag == 1000) {
        if (_dismissBlock) {
            self.dismissBlock();
        }
        return;
    }
    
    //点击确定
    if (sender.tag == 1001 && self.pickerViewType != PickerViewTypeProCity) {
        if (_SureBtnActionBlock) {
            self.SureBtnActionBlock(self.pickerViewType, sender.tag, self.selectIndex);
        }
    }else{
        if (self.proCityArr.count) {
            JYProCityCountyModel * proModel = self.proCityArr[self.provinceIndex];
            JYCityModel * cityModel = proModel.cityList[self.cityIndex];
            JYCountyModel * countyModel = cityModel.cityList[self.areaIndex];
            
            if (_sureBtnProCityTypeActionBlock) {
                self.sureBtnProCityTypeActionBlock(proModel.id, proModel.name, cityModel.id, cityModel.name, countyModel.id, countyModel.name);
            }
        }
    }
}

#pragma mark --------------pickerView 数据源和代理 -------------------------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (self.pickerViewType == PickerViewTypeProCity) {
        return self.proCityRow;
    }
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (self.pickerViewType == PickerViewTypeNormal) {
        return self.dataArr.count ;
    }else if (self.pickerViewType == PickerViewTypeProCity){
        if (self.proCityArr.count) {
            if (component == 0) {
                return self.proCityArr.count;
            } else if (component == 1) {
                JYProCityCountyModel *Pmodel = self.proCityArr[self.provinceIndex];
                if (Pmodel.cityList.count > 0) {
                    return Pmodel.cityList.count;
                }else{
                    return 0;
                }
            } else if (component == 2) {
                JYProCityCountyModel *Pmodel = self.proCityArr[self.provinceIndex];
                if (Pmodel.cityList.count > 0) {
                    JYCityModel *Cmodel = Pmodel.cityList[self.cityIndex];
                    if (Cmodel.cityList.count > 0) {
                        return Cmodel.cityList.count;
                    }else{
                        return 0;
                    }
                }else{
                    return 0;
                }
            }
        }
        return 0;
    }else{
        return self.payStyleTitleArr.count;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    switch (self.pickerViewType) {
        case PickerViewTypeNormal:
        {
            JYPickerViewAlertView * alertView = LOADXIB(@"JYPickerViewAlertView");
            alertView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
            alertView.titleLab.text = self.dataArr[row];
            return alertView;
        }
            break;
        case PickerViewTypePay:
        {
            JYJYPickerViewPayStyleView * payAlertView = LOADXIB(@"JYJYPickerViewPayStyleView");
            payAlertView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 50);
            payAlertView.titleLab.text = self.payStyleTitleArr[row];
            payAlertView.picImageView.image = [UIImage imageNamed:self.payStyleImageArr[row]];
            return payAlertView;
        }
            break;
        case PickerViewTypeProCity:
        {
            JYPickerViewAlertView * alertView = LOADXIB(@"JYPickerViewAlertView");
            alertView.frame = CGRectMake(0, 0, SCREEN_WIDTH / self.proCityRow, 50);
//            alertView.titleLab.text = self.proCityArr[row];
//            return alertView;
            
            
            JYProCityCountyModel *model;
            if (component == 0) {
                model = self.proCityArr[row];
                 alertView.titleLab.text  = model.name;
                return alertView;
            } else if (component == 1) {
                JYProCityCountyModel *Pmodel = self.proCityArr[self.provinceIndex];
                JYCityModel *  model = Pmodel.cityList[row];
                alertView.titleLab.text = model.name;
                return alertView;
            } else if (component == 2) {
                JYProCityCountyModel *Pmodel = self.proCityArr[self.provinceIndex];
                JYCityModel *Cmodel = Pmodel.cityList[self.cityIndex];
                JYCountyModel * model = Cmodel.cityList[row];
                alertView.titleLab.text = model.name;
                return alertView;
            }
            alertView.titleLab.text = @"";
            return alertView;
        }
            break;
        default:
            break;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (self.pickerViewType == PickerViewTypeProCity) {
        return SCREEN_WIDTH/self.proCityRow;
    }
    return SCREEN_WIDTH;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (self.pickerViewType == PickerViewTypePay) {
        self.selectIndex = row;
    }else if (self.pickerViewType == PickerViewTypeProCity){
        if (component == 0) {
            self.provinceIndex = [pickerView selectedRowInComponent:0];
            self.cityIndex = 0;
            self.areaIndex = 0;
            [pickerView reloadAllComponents];
            [pickerView selectRow:0 inComponent:1 animated:NO];
            if (self.proCityRow == 3) {
                [pickerView selectRow:0 inComponent:2 animated:NO];
            }
            
            //        [pickerView reloadComponent:1];
            //        [pickerView reloadComponent:2];
        }else if (component == 1){
            //        [pickerView reloadComponent:2];
            self.cityIndex = [pickerView selectedRowInComponent:1];
            self.areaIndex = 0;
            [pickerView reloadAllComponents];
            if (self.proCityRow == 3) {
                [pickerView selectRow:0 inComponent:2 animated:NO];
            }
            
        }else{
            if (self.proCityRow == 3) {
                self.areaIndex = [pickerView selectedRowInComponent:2];
            }
            [pickerView reloadAllComponents];
        }
    }else{
        if (self.IsHideBottomBtn && _NoBottomBtnSelectIndexBlock ) {
            self.NoBottomBtnSelectIndexBlock(row);
        }
    }
    self.selectIndex = row;
}

//- (void)showAlertPicView{
//    JYAlertPicViewController * alertVC = [[JYAlertPicViewController alloc] init];
//    alertVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [self presentViewController:alertVC animated:YES completion:nil];
//    
////    [[UIApplication sharedApplication].keyWindow addSubview:self.alertPicView];
//}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (_dismissBlock) {
        self.dismissBlock();
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ======================== Update View ========================

- (void)updatePickerViewDataArr:(NSArray *)aArray type:(PickerViewType)pikerViewType
{
    self.pickerViewType = pikerViewType;
    
    if (pikerViewType == PickerViewTypeProCity) {

        self.proCityArr = aArray;
    }else{
        self.dataArr = aArray;
    }
    [self.pickerView reloadAllComponents];
}

#pragma mark - ======================== Getter ========================

- (NSArray *)dataArr
{
    if (!_dataArr) {
//        _dataArr = @[@"去添加+",@"北京鸿睿思博科技有限公司",@"北京大风车科技有限公司",@"北京百度科技公司"];
        _dataArr = @[];
    }
    return _dataArr;
}

- (NSArray *)payStyleTitleArr
{
    if (!_payStyleTitleArr) {
        _payStyleTitleArr = @[@"账户余额支付",@"微信支付",@"支付宝支付"];
    }
    return _payStyleTitleArr;
}

- (NSArray *)payStyleImageArr
{
    if (!_payStyleImageArr) {
        _payStyleImageArr = @[@"账户余额",@"微信支付",@"支付宝支付"];
    }
    return _payStyleImageArr;
}

//- (JYAlertPicView *)alertPicView
//{
//    if (!_alertPicView) {
//        _alertPicView = LOADXIB(@"JYAlertPicView");
//    }
//    return _alertPicView;
//}

- (NSArray *)proCityArr
{
    if (!_proCityArr) {
        _proCityArr = [NSArray array];
    }
    return _proCityArr;
}

@end
