//
//  JYAlertChooseImageViewController.m
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAlertChooseImageViewController.h"

@interface JYAlertChooseImageViewController ()

@end

@implementation JYAlertChooseImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)bottomBtnAction:(UIButton *)sender {
    if (_bottomBtnActionBlock) {
        self.bottomBtnActionBlock(sender.tag);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
