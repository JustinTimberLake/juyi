//
//  JYPickerViewAlertController.h
//  JY
//
//  Created by duanhuifen on 2017/7/6.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "HRBaseViewController.h"
#import "PickerViewType.h"

@interface JYPickerViewAlertController : HRBaseViewController

@property (nonatomic, assign ) PickerViewType pickerViewType;
@property (nonatomic,assign,getter=IsHideBottomBtn) BOOL hideBottomBtn;

@property (nonatomic,strong) NSArray * dataArr;
//省市区的时候显示几列
@property (nonatomic,assign) NSInteger proCityRow;

//点击确定按钮
@property (nonatomic,copy) void (^SureBtnActionBlock)(PickerViewType pickerViewType,NSInteger btnTag, NSInteger selectIndex);

//没有确定按钮的选中Block (应该是没有用)
@property (nonatomic,copy) void (^NoBottomBtnSelectIndexBlock)(NSInteger selectIndex);

@property (nonatomic,copy) void (^dismissBlock)();

//省市区模式下确认按钮点击
@property (nonatomic,copy) void (^sureBtnProCityTypeActionBlock)(NSString * proId,NSString *proTitle,NSString * cityId,NSString * cityTitle,NSString *countyId,NSString *countyTitle);

//更新数据
- (void)updatePickerViewDataArr:(NSArray *)aArray type:(PickerViewType)pikerViewType;
@end
