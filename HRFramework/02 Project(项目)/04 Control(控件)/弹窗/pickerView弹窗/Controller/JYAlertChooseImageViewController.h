//
//  JYAlertChooseImageViewController.h
//  JY
//
//  Created by Duanhuifen on 2017/9/21.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "HRBaseViewController.h"

@interface JYAlertChooseImageViewController : HRBaseViewController

@property (nonatomic,copy) void(^bottomBtnActionBlock)(NSInteger tag);
@end
