//
//  JYSelectBtn.m
//  JY
//
//  Created by risenb on 2017/8/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSelectBtn.h"

@implementation JYSelectBtn

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 修改按钮内部子控件位置
    if (self.titleLabel.x > self.imageView.x) {
        // 设置label
        self.titleLabel.x = self.imageView.x;
        // 设置ImageView
        self.imageView.x = CGRectGetMaxX(self.titleLabel.frame) + 5;
    }
    
    NSLog(@"titleLabel:%f imageView:%f",self.titleLabel.x,self.imageView.x);
}
//暂时隐藏
- (void)setTitle:(nullable NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];
    
    [self sizeToFit];
}

- (void)setImage:(nullable UIImage *)image forState:(UIControlState)state
{
    [super setImage:image forState:state];
    [self sizeToFit];
    
}

@end
