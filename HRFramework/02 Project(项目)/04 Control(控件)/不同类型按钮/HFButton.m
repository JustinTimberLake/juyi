//
//  HFButton.h
//  ZRHY
//
//  Created by duanhuifen on 16/7/21.
//  Copyright © 2016年 辛忠志. All rights reserved.
//

#import "HFButton.h"

@implementation HFButton

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 修改按钮内部子控件位置
    if (self.titleLabel.x > self.imageView.x) {
        // 设置label
        self.titleLabel.x = self.imageView.x;
        // 设置ImageView
        self.imageView.x = CGRectGetMaxX(self.titleLabel.frame) + 5;
    }
    
    NSLog(@"titleLabel:%f imageView:%f",self.titleLabel.x,self.imageView.x);
}


@end
