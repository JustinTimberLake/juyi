//
//  JYMiddleBtn.m
//  JY
//
//  Created by duanhuifen on 2017/7/5.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYMiddleBtn.h"

@implementation JYMiddleBtn

- (void)layoutSubviews{
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(0, 0, self.width, self.height * 0.6);
    self.imageView.contentMode = UIViewContentModeBottom;
    //    self.imageView.center = CGPointMake(self.width, self.height * 0.6);
    self.titleLabel.frame = CGRectMake(0, self.height * 0.6, self.width, self.height * 0.4);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}
@end
