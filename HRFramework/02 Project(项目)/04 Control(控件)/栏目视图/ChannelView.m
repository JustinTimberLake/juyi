//
//  ChannelView.m
//  JY
//
//  Created by Stronger_WM on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "ChannelView.h"
#import "ChannelItem.h"
#import "ChannelModel.h"

static NSString *const CHANNEL_ITEM_ID = @"ChannelItem";

@interface ChannelView ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) UICollectionViewFlowLayout *layout;
@property (nonatomic ,strong) NSMutableArray *channelArr;

@end

@implementation ChannelView

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{
    self.layout = [[UICollectionViewFlowLayout alloc] init];
    self.layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.layout];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self addSubview:self.collectionView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
    
    [self.collectionView registerClass:[ChannelItem class] forCellWithReuseIdentifier:CHANNEL_ITEM_ID];
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:JYUCOLOR_LINE];
    [self addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.trailing.leading.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
}

#pragma mark - ======================== Public Methods ========================

#pragma mark - ======================== Protocol ========================

#pragma mark ********* UICollectionViewDataSource,UICollectionViewDelegateFlowLayout *********

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.channelArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ChannelModel *model = obj;
        model.isSelected = NO;
        if (idx == indexPath.row) {
            model.isSelected = YES;
        }
    }];
    
    if (self.delegate) {
        [self.delegate channelViewDidSelectChannelAtIndex:indexPath.row];
    }
    
    [self reload];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ChannelModel *model = self.channelArr[indexPath.row];
    CGFloat tw = [model.channelTitle stringWidthAtHeight:50 font:FONT(15)];
    return CGSizeMake(tw + 30, 50);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.channelArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ChannelItem *item = [collectionView dequeueReusableCellWithReuseIdentifier:CHANNEL_ITEM_ID forIndexPath:indexPath];
    [item updateItem:self.channelArr[indexPath.row]];
    return item;
}

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateView:(NSArray <ChannelModel *>*)datas
{
    if ([datas isKindOfClass:[NSArray class]]) {
        [self.channelArr removeAllObjects];
        [self.channelArr addObjectsFromArray:datas];
    }
    
    [self reload];
}

- (void)reload
{
    [self.collectionView reloadData];
}

#pragma mark - ======================== Getter ========================

- (NSMutableArray *)channelArr
{
    if (!_channelArr) {
        _channelArr = [[NSMutableArray alloc] init];
    }
    return _channelArr;
}

@end
