//
//  ChannelItem.h
//  JY
//
//  Created by Stronger_WM on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChannelModel;

@interface ChannelItem : UICollectionViewCell

- (void)updateItem:(ChannelModel *)channelModel;

@end
