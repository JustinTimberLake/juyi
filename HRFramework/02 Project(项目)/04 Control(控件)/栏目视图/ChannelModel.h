//
//  ChannelModel.h
//  JY
//
//  Created by Stronger_WM on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelModel : NSObject

@property (nonatomic ,copy) NSString *channelTitle;         //标题
@property (nonatomic,copy) NSString *classifyId;            //分类id
@property (nonatomic ,assign) BOOL isSelected;              //是否被选中，默认为NO

@end
