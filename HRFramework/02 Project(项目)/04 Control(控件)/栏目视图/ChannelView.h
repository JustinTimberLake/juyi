//
//  ChannelView.h
//  JY
//
//  Created by Stronger_WM on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChannelViewDelegate <NSObject>

- (void)channelViewDidSelectChannelAtIndex:(NSInteger)index;

@end

@class ChannelModel;

@interface ChannelView : UIView

@property (nonatomic ,weak) id<ChannelViewDelegate> delegate;

- (void)updateView:(NSArray <ChannelModel *>*)datas;

@end
