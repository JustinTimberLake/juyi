//
//  ChannelItem.m
//  JY
//
//  Created by Stronger_WM on 2017/7/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "ChannelItem.h"
#import "ChannelModel.h"

@interface ChannelItem ()

@property (nonatomic ,strong) ChannelModel *itemModel;

@property (nonatomic ,strong) UILabel *channelTitleLabel;   //频道名称
@property (nonatomic ,strong) UIImageView *indicaterImgView;      //指示器

@end

@implementation ChannelItem

#pragma mark - ======================== Life Cycle ========================

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
        [self configSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self config];
    [self configSubviews];
}

#pragma mark ********* Dealloc *********

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - ======================== Private Methods ========================

//和界面无关的设置
- (void)config
{
    
}

//子视图
- (void)configSubviews
{    
    self.channelTitleLabel = [[UILabel alloc] init];
    self.channelTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.channelTitleLabel.font = FONT(15);
    [self.contentView addSubview:self.channelTitleLabel];
    
    self.indicaterImgView = [[UIImageView alloc] init];
    self.indicaterImgView.image = [UIImage imageNamed:@"切换横条"];
    [self.contentView addSubview:self.indicaterImgView];
    
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.channelTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(padding);
    }];
    
    [self.indicaterImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(48);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(1);
        make.centerX.equalTo(self.contentView.mas_centerX);
    }];
}

#pragma mark - ======================== Publish Methods ========================

#pragma mark - ======================== Protocol ========================

#pragma mark - ======================== Actions ========================

#pragma mark - ======================== Update View ========================

- (void)updateItem:(ChannelModel *)channelModel
{
    if ([channelModel isKindOfClass:[channelModel class]]) {
        _itemModel = channelModel;
    }
    
    if (_itemModel) {
        //选中效果
        if (_itemModel.isSelected) {
            self.indicaterImgView.hidden = NO;
            self.channelTitleLabel.textColor = [UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN];
        }
        else
        {
            self.indicaterImgView.hidden = YES;
            self.channelTitleLabel.textColor = [UIColor colorWithHexString:@"333333"];
        }
        
        self.channelTitleLabel.text = _itemModel.channelTitle;
    }
}

//子视图布局
- (void)configSubviewLayout
{
    
}

#pragma mark - ======================== Getter ========================


@end
