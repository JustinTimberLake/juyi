//
//  JYSegmentedControl.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JYSegmentedControl : UIView

@property (strong, nonatomic) NSArray *titles;

/**
 选中 segment
 */
@property (copy, nonatomic) void(^selectedSegmentBlock)(NSInteger indexSegment);

@property (assign, nonatomic) float titleSize;

@end
