//
//  JYSegmentedControl.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/4.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSegmentedControl.h"
@interface JYSegmentedControl ()

@property (nonatomic, strong)UIImageView * imageView;
@property (nonatomic, assign)NSInteger selectTag;
@end

@implementation JYSegmentedControl

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.selectTag = 1000;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
  }


#pragma mark - ||============ ReWrite setter method ============||

- (void)setTitles:(NSArray *)titles {
    _titles = titles;
    [self creatButton];
}

- (void)creatButton{
    for (int i = 0 ; i <_titles.count; i++) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:_titles[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_TITLE] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithHexString:JYUCOLOR_YELLOW_MAIN] forState:UIControlStateSelected];
        btn.titleLabel.font = [UIFont systemFontOfSize:_titleSize];
        if (i == 0) {
            btn.selected = YES;
            self.imageView.frame = CGRectMake(10, self.size.height -1, self.size.width/_titles.count - 20, 2);
             [self addSubview:_imageView];
        }
        btn.tag =1000+ i;
        [btn addTarget:self action:@selector(touchSegmentAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = CGRectMake(self.size.width/_titles.count *i,0 , self.size.width/_titles.count, self.size.height);
        [self addSubview:btn];
    }

}


#pragma mark - ||============ Event action ============||
- (IBAction)touchSegmentAction:(UIButton *)sender {
    
    if (self.selectTag == sender.tag) {
        return;
    }
    
    ((UIButton *)[self viewWithTag:self.selectTag]).selected = NO;
    sender.selected = YES;
    CGPoint center = sender.center;
    center.y = _imageView.center.y;
    _imageView.center = center;
    self.selectTag = sender.tag;
    if (self.selectedSegmentBlock) {
        self.selectedSegmentBlock(self.selectTag);
    }
}


- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"黄色矩形底纹"]];
       
    }
    return _imageView;
}



@end
