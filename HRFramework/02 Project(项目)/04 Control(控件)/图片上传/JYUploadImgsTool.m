//
//  ZKUploadImgsTool.m
//  ZK
//
//  Created by Risenb on 2017/4/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYUploadImgsTool.h"

@interface JYUploadImgsTool ()

@property (assign, nonatomic) NSInteger curson;

@property (strong,nonatomic) NSMutableArray *SuccessArr;
@property (nonatomic,strong) NSMutableArray *successImageHttpArr;;
@property (strong,nonatomic) NSMutableArray *FailArr;


@end


@implementation JYUploadImgsTool

-(instancetype)init{
    if (self = [super init]) {
        self.curson = 0;
    }
    return self;
}

//多图上传
-(void)UploadImgs:(NSMutableArray*)imgArr{

    self.SuccessArr = [NSMutableArray array];
    self.FailArr = [NSMutableArray array];
    self.successImageHttpArr = [NSMutableArray array];
    self.curson = 0;

    [self SubmitImageWithIndex:self.curson ImgArr:imgArr];
}

-(void)uploadImgsComplete{
    if (self.CompliteBlock) {
        self.CompliteBlock(self.SuccessArr, self.successImageHttpArr,self.FailArr);
    }
}

//递归上传
-(void)SubmitImageWithIndex:(NSInteger)index ImgArr:(NSMutableArray*)imgArr{

    if (self.curson >= imgArr.count) {
        [self uploadImgsComplete];
        return;
    }

    [self UploadImgsWithImg:imgArr[index] Index:index Success:^(NSString *imgStr ,NSString *imageHttpStr,NSInteger index) {
        self.curson ++;
        [self.SuccessArr addObject:imgStr];
        [self.successImageHttpArr addObject:imageHttpStr];

        [self SubmitImageWithIndex:self.curson ImgArr:imgArr];

    } Fail:^(id errMsg) {
        self.curson ++;
        [self.FailArr addObject:@(index)];
        [self SubmitImageWithIndex:self.curson ImgArr:imgArr];
    }];

}



//单个图片上传函数
-(void)UploadImgsWithImg:(UIImage*)img Index:(NSInteger)index Success:(void(^)(NSString *imgStr ,NSString *imageHttpStr,NSInteger index))success Fail:(void(^)(id errMsg))fail{

    //转化部分
//    UIImage *newImg = [self imageByScalingAndCroppingForSize:CGSizeMake(100, 100) Img:img];
    NSData *imgData = UIImageJPEGRepresentation(img, 0.1);

    NSString *baseStr = [imgData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];

    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:baseStr forKey:@"img"];
    [para setObject:@"2" forKey:@"type"];

    //上传部分
    [self NetWorkingWithURLPath:JY_PATH(JY_USER_ImgUpFile) Params:para Success:^(id result) {
        
        NSDictionary *dict = result[@"data"];
        NSString *imgStr = dict[@"imgUrl"];
        NSString *imageHttpStr = dict[@"imgUrlHttp"];
        if (success) {
            success(imgStr ,imageHttpStr,index);
        }
    } Fail:^(id errMsg) {
        fail(errMsg);
    }];
}



//网络请求方法
-(void)NetWorkingWithURLPath:(NSString*)path Params:(NSMutableDictionary *)params Success:(void(^)(id result))success Fail:(void(^)(id errMsg))fail{

    NSMutableDictionary *innerDict = [NSMutableDictionary dictionaryWithDictionary:params];

    HRRequestManager *manager = [[HRRequestManager alloc] init];
    NSLog(@"🌺%@",path);
    [manager POST_URL:path params:innerDict success:^(id result) {

        BOOL iSstatus = [result[@"status"] boolValue];
        if (iSstatus) {
            if (success) {
                //处理数据
                success(result);
            }
        }else{
            if (fail) {
                fail(result[@"errorMsg"]);
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        if (fail) {
            fail(@"网络请求失败");
        }
    }];
 }


- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize Img:(UIImage *)img
{
    UIImage *sourceImage = img;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);

    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;

        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;

        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }

    UIGraphicsBeginImageContext(targetSize); // this will crop

    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;

    [sourceImage drawInRect:thumbnailRect];

    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");

    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

@end
