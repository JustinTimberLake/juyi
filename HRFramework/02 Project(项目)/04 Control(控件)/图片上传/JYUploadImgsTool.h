//
//  ZKUploadImgsTool.h
//  ZK
//
//  Created by Risenb on 2017/4/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JYBaseViewModel.h"

@interface JYUploadImgsTool : JYBaseViewModel


//多图上传
-(void)UploadImgs:(NSMutableArray*)imgArr;
@property (copy, nonatomic) void(^CompliteBlock)(NSMutableArray *imgStrArr ,NSMutableArray *imageHttpArr, NSMutableArray *failArrIndex);


//单个图片上传函数
-(void)UploadImgsWithImg:(UIImage*)img Index:(NSInteger)index Success:(void(^)(NSString *imgStr ,NSString *imageHttpStr,NSInteger index))success Fail:(void(^)(id errMsg))fail;

@end
