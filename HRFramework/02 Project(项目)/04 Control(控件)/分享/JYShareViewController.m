//
//  SHSelectDateController.m
//  SanHeRealEstate
//
//  Created by duanhuifen on 17/2/7.
//  Copyright © 2017年 CAPF. All rights reserved.
//

#import "JYShareViewController.h"

@interface JYShareViewController ()



@end

@implementation JYShareViewController

- (void)viewDidLoad {
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancleBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareBtnAction:(UIButton *)sender {
    if (_ShareBtnActionBlock) {
        _ShareBtnActionBlock(sender.tag);
    }
    NSLog(@"点击了按钮");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
