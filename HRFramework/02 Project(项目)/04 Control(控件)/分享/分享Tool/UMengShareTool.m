//
//  UMengShareTool.m
//  友盟测试文档
//
//  Created by Risenb on 2017/4/25.
//  Copyright © 2017年 ytc. All rights reserved.
//

#import "UMengShareTool.h"
#import "SNStarsAlertView.h"
#import <UIKit/UIKit.h>

@implementation UMengShareTool

//UMSocialPlatformType
+(void)ShareWithModel:(UShareCustomModel*)infoModel Success:(void(^)(id data))successO Fail:(void(^)(NSError * error))faileO{

//    if (![UMengShareTool RightfulMdoelWithModel:infoModel]) {
//        NSError *error = [NSError errorWithDomain:NSOSStatusErrorDomain code:0 userInfo:@{@"错误原因":@"参数添加错误"}];
//        faileO(error);
//        return;
//    }

    if ([infoModel.shareTitle isKindOfClass:[NSString class]]) {
        if (infoModel.shareTitle.length == 0) {
            infoModel.shareTitle = @"     ";
        }
    }

    __block id imgUrlOrImage;
    //因为新浪微博不能分享http链接的图片,因此先进行下载,然后传递image 暂时注释判断条件，好像都不可以分享http 链接开头的
//    if (infoModel.plantFormType == 0) {

        [UMengShareTool getImgWithUrl:infoModel.shareImg Fail:^{
            imgUrlOrImage = [UIImage imageNamed:@"logo"];

            [UMengShareTool Ture_ShareMethodWithModel:infoModel ImgOrURL:imgUrlOrImage Success:^(id data) {
                successO(data);
            } Fail:^(NSError *error) {
                faileO(error);
            }];

        } Success:^(UIImage *img) {
            imgUrlOrImage = img;

            [UMengShareTool Ture_ShareMethodWithModel:infoModel ImgOrURL:img Success:^(id data) {
                successO(data);
            } Fail:^(NSError *error) {
                faileO(error);
            }];

        }];
//    }else{
//
//        [UMengShareTool Ture_ShareMethodWithModel:infoModel ImgOrURL:infoModel.shareImg Success:^(id data) {
//            successO(data);
//        } Fail:^(NSError *error) {
//            faileO(error);
//        }];
//
//    }

}

//友盟函数
+(void)Ture_ShareMethodWithModel:(UShareCustomModel*)infoModel ImgOrURL:(id)imgOrUrl Success:(void(^)(id data))success Fail:(void(^)(NSError * error))faile{

    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//    测试用的分享图片
//    NSString *img = @"http://img0.imgtn.bdimg.com/it/u=3597382613,1842885761&fm=23&gp=0.jpg";

    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:infoModel.shareTitle descr:infoModel.shareContent thumImage:imgOrUrl];
    //
    //设置网页地址
    if (!infoModel.shareUrl.length) {
        shareObject.webpageUrl = @"http://www.baidu.com";
    }else{
        shareObject.webpageUrl = infoModel.shareUrl;
    }
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;

    //调用前发送通知给AppDelegate.带去以分享的对象,分享成功则发送给后台
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ShareModel" object:infoModel];
    switch (infoModel.plantFormType) {
            //微信
        case UMSocialPlatformType_WechatSession:
        case UMSocialPlatformType_WechatTimeLine:
        {
            BOOL isInstalled = [[[UMSocialManager defaultManager] platformProviderWithPlatformType:infoModel.plantFormType] umSocial_isInstall];
            if (!isInstalled){
                [self showPointAlertWithTitle:@"您的手机未安装微信或手机版本过低"];
                return;
            }
            break;
            
        }
            break;
            //QQ
        case UMSocialPlatformType_QQ:
        case UMSocialPlatformType_Qzone:{
            BOOL isInstalled = [[[UMSocialManager defaultManager] platformProviderWithPlatformType:infoModel.plantFormType] umSocial_isInstall];
            if (!isInstalled) {
                [self showPointAlertWithTitle:@"您的手机未安装QQ或手机版本过低"];
                return;
            }
        }
            break;
            //微博
        case UMSocialPlatformType_Sina:
        {
            BOOL isInstalled = [[[UMSocialManager defaultManager] platformProviderWithPlatformType:infoModel.plantFormType] umSocial_isInstall];
            if (!isInstalled) {
                [self showPointAlertWithTitle:@"您的手机未安装微博或手机版本过低"];
                return;
            }
        }
            break;
            
        default:
            break;
    }
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:infoModel.plantFormType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
            //失败回调
            faile(error);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                success(resp.message);
            }else{
                UMSocialLogInfo(@"response data is %@",data);
                success(data);
            }
//            [[NSNotificationCenter defaultCenter] postNotificationName:UM_SHARE_SUCCESS_BlockMessage object:nil];
        }
    }];


}

+(void)showPointAlertWithTitle:(NSString *)title{
    SNStarsAlertView *alert = [[SNStarsAlertView alloc]initWithTitle:@"提醒" message:title cancelButtonTitle:@"知道了" otherButtonTitle:nil cancelButtonClick:nil otherButtonClick:nil];
    [alert show];
}

+(void)getImgWithUrl:(NSString*)url Fail:(void(^)())fail Success:(void(^)(UIImage *img ))Success{

    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:url] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize)
     {
         //处理下载进度
     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
         if (error) {
             fail();
         }
         if (image) {
             Success(image);
         }
     }
     ];

}



+(BOOL)RightfulMdoelWithModel:(UShareCustomModel*)model{

    if (model.shareImg.length == 0) {
        return NO;
    }
    if (model.shareTitle.length == 0) {
        return YES;
    }
    if (model.shareContent.length == 0) {
        return NO;
    }
    if (model.shareUrl.length == 0) {
        return NO;
    }
    return YES;
}

/**
 
 typedef NS_ENUM(NSInteger,UMSocialPlatformType)
 {
 UMSocialPlatformType_UnKnown            = -2,
 //预定义的平台
 UMSocialPlatformType_Predefine_Begin    = -1,
 UMSocialPlatformType_Sina               = 0, //新浪
 UMSocialPlatformType_WechatSession      = 1, //微信聊天
 UMSocialPlatformType_WechatTimeLine     = 2,//微信朋友圈
 UMSocialPlatformType_WechatFavorite     = 3,//微信收藏
 UMSocialPlatformType_QQ                 = 4,//QQ聊天页面
 UMSocialPlatformType_Qzone              = 5,//qq空间
 UMSocialPlatformType_TencentWb          = 6,//腾讯微博
 UMSocialPlatformType_AlipaySession      = 7,//支付宝聊天页面
 UMSocialPlatformType_YixinSession       = 8,//易信聊天页面
 UMSocialPlatformType_YixinTimeLine      = 9,//易信朋友圈
 UMSocialPlatformType_YixinFavorite      = 10,//易信收藏
 UMSocialPlatformType_LaiWangSession     = 11,//点点虫（原来往）聊天页面
 UMSocialPlatformType_LaiWangTimeLine    = 12,//点点虫动态
 UMSocialPlatformType_Sms                = 13,//短信
 UMSocialPlatformType_Email              = 14,//邮件
 UMSocialPlatformType_Renren             = 15,//人人
 UMSocialPlatformType_Facebook           = 16,//Facebook
 UMSocialPlatformType_Twitter            = 17,//Twitter
 UMSocialPlatformType_Douban             = 18,//豆瓣
 UMSocialPlatformType_KakaoTalk          = 19,//KakaoTalk
 UMSocialPlatformType_Pinterest          = 20,//Pinteres
 UMSocialPlatformType_Line               = 21,//Line

 UMSocialPlatformType_Linkedin           = 22,//领英

 UMSocialPlatformType_Flickr             = 23,//Flickr

 UMSocialPlatformType_Tumblr             = 24,//Tumblr
 UMSocialPlatformType_Instagram          = 25,//Instagram
 UMSocialPlatformType_Whatsapp           = 26,//Whatsapp
 UMSocialPlatformType_DingDing           = 27,//钉钉

 UMSocialPlatformType_YouDaoNote         = 28,//有道云笔记
 UMSocialPlatformType_EverNote           = 29,//印象笔记
 UMSocialPlatformType_GooglePlus         = 30,//Google+
 UMSocialPlatformType_Pocket             = 31,//Pocket
 UMSocialPlatformType_DropBox            = 32,//dropbox
 UMSocialPlatformType_VKontakte          = 33,//vkontakte
 UMSocialPlatformType_FaceBookMessenger  = 34,//FaceBookMessenger

 UMSocialPlatformType_Predefine_end      = 999,

 //用户自定义的平台
 UMSocialPlatformType_UserDefine_Begin = 1000,
 UMSocialPlatformType_UserDefine_End = 2000,
 };


 */



@end
