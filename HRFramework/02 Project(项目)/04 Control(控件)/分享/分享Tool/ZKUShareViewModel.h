//
//  ZKUShareViewModel.h
//  ZK
//
//  Created by Risenb on 2017/5/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYBaseViewModel.h"

#import "UShareCustomModel.h"

@interface ZKUShareViewModel : JYBaseViewModel

//分享信息 - 类型(1圈子2商品3邀请好友)
-(void)RequestData_ShareInfo_WithUsrId:(NSString*)userId ShareType:(NSInteger)shareType MomentId:(NSInteger)momentId Success:(void(^)(UShareCustomModel *shareModel))success Fail:(void(^)(NSString *errMsg))fail;


//回调信息 - 类型(1圈子2商品3邀请好友)
-(void)RequestData_BackInfo_WithUsrId:(NSString*)userId ShareType:(NSString*)shareType MomentId:(NSString*)momentId ShareSource:(NSString*)shareSource Success:(void(^)(NSString *isSuccess))success Fail:(void(^)(NSString *errMsg))fail;

@end
