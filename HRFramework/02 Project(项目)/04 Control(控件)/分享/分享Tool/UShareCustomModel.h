//
//  UShareCustomModel.h
//  友盟测试文档
//
//  Created by Risenb on 2017/4/25.
//  Copyright © 2017年 ytc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JYBaseModel.h"

@interface UShareCustomModel : JYBaseModel


//标题
@property (copy, nonatomic) NSString *shareTitle;

//描述
@property (copy, nonatomic) NSString *shareContent;

//图片地址
@property (copy, nonatomic) NSString *shareImg;

//跳转地址
@property (copy, nonatomic) NSString *shareUrl;

//分享平台
@property (assign, nonatomic) NSInteger plantFormType;


//#pragma mark - ---------- 额外信息  ----------
//
////分享类型:类型(1圈子2商品3邀请好友)
@property (copy, nonatomic) NSString *shareType;
//
////如果shareType为3可不传，否则必传
//@property (copy, nonatomic) NSString *momentId;



/**

 UMSocialPlatformType_Sina               = 0, //新浪
 UMSocialPlatformType_WechatSession      = 1, //微信聊天
 UMSocialPlatformType_WechatTimeLine     = 2,//微信朋友圈
 UMSocialPlatformType_WechatFavorite     = 3,//微信收藏
 UMSocialPlatformType_QQ                 = 4,//QQ聊天页面
 UMSocialPlatformType_Qzone              = 5,//qq空间

 */



@end
