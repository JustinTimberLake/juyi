//
//  ZKUShareViewModel.m
//  ZK
//
//  Created by Risenb on 2017/5/3.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "ZKUShareViewModel.h"

@implementation ZKUShareViewModel


//回调信息 - 类型(1圈子2商品3邀请好友)
//-(void)RequestData_BackInfo_WithUsrId:(NSString*)userId ShareType:(NSString*)shareType MomentId:(NSString*)momentId ShareSource:(NSString*)shareSource Success:(void(^)(NSString *isSuccess))success Fail:(void(^)(NSString *errMsg))fail{
//
//    NSMutableDictionary *para = [NSMutableDictionary dictionary];
//    para[@"shareType"] = shareType;
//    para[@"shareSource"] = shareSource;
//    if (userId) {
//        para[@"userId"] = userId;
//    }
//
//    if (![shareType isEqualToString:@"3"]) {
//        para[@"momentId"] = momentId;
//    }
//
//    [self NetWorkingWithURLPath:JK_ShareBackInfo Params:para Success:^(id result) {
//
//        NSString *isSuccess = result[@"status"];
//
//        if ([isSuccess isEqualToString:@"1"]) {
//            if (success) {
//                success(isSuccess);
//            }
//        }else{
//            fail(@"0");
//        }
//    } Fail:^(id errMsg) {
//        fail(errMsg);
//    }];
//
//}
//
////分享信息 - 类型(1圈子2商品3邀请好友)
//-(void)RequestData_ShareInfo_WithUsrId:(NSString*)userId ShareType:(NSInteger)shareType MomentId:(NSInteger)momentId Success:(void(^)(UShareCustomModel *shareModel))success Fail:(void(^)(NSString *errMsg))fail{
//
//    NSMutableDictionary *para = [NSMutableDictionary dictionary];
//
//    [para setObject:@(shareType) forKey:@"shareType"];
//
//    if (shareType == 3) {
//        [para setObject:userId forKey:@"userId"];
//        if (momentId > 0) {
//            [para setObject:@(momentId) forKey:@"momentId"];
//        }
//    }else{
//        [para setObject:@(momentId) forKey:@"momentId"];
//        if (userId) {
//            [para setObject:userId forKey:@"userId"];
//        }
//    }
//
//    [self NetWorkingWithURLPath:JK_ShareInfo Params:para Success:^(id result) {
//
//        NSDictionary *dict = result[@"data"];
//
//        UShareCustomModel *mdoel = [[UShareCustomModel alloc]initWithDictionary:dict];
//        mdoel.shareType = [NSString stringWithFormat:@"%zd",shareType];
//
//        if (shareType != 3) {
//            mdoel.momentId = [NSString stringWithFormat:@"%zd",momentId];
//        }
//
//        if (success) {
//            success(mdoel);
//        }
//
//    } Fail:^(id errMsg) {
//        fail(errMsg);
//    }];
//
//}
//
//
//
//
////网络请求方法
//-(void)NetWorkingWithURLPath:(NSString*)path Params:(NSMutableDictionary *)params Success:(void(^)(id result))success Fail:(void(^)(id errMsg))fail{
//
//    [self signURL:JK_PATH(path) completionBlock:^(NSString *eventuallyURL, NSString *md5str) {
//        if (md5str) {
//
//            NSMutableDictionary *innerDict = [NSMutableDictionary dictionaryWithDictionary:params];
//
//            [innerDict setObject:md5str forKey:@"sign"];
//
//            HRRequestManager *manager = [[HRRequestManager alloc] init];
//            [manager POST_URL:eventuallyURL params:innerDict success:^(id result) {
//                BOOL iSstatus = [result[@"status"] boolValue];
//
//                if (iSstatus) {
//
//                    if (success) {
//                        //处理数据
//                        success(result);
//                    }
//
//                }else{
//
//                    if (fail) {
//                        fail(result[@"errorMsg"]);
//                    }
//
//                }
//                
//            } failure:^(NSDictionary *errorInfo) {
//                if (fail) {
//                    fail(@"网络请求失败");
//                }
//            }];
//        }else{
//            fail(@"签名加载失败");
//        }
//    }];
//    
//}
//


@end
