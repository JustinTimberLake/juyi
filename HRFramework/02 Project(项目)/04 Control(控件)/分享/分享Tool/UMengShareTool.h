//
//  UMengShareTool.h
//  友盟测试文档
//
//  Created by Risenb on 2017/4/25.
//  Copyright © 2017年 ytc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UMShare/UMShare.h>

#import "UShareCustomModel.h"

@interface UMengShareTool : NSObject

/**
 
 UMSocialPlatformType_Sina               = 0, //新浪
 UMSocialPlatformType_WechatSession      = 1, //微信聊天
 UMSocialPlatformType_WechatTimeLine     = 2,//微信朋友圈
 UMSocialPlatformType_WechatFavorite     = 3,//微信收藏
 UMSocialPlatformType_QQ                 = 4,//QQ聊天页面
 UMSocialPlatformType_Qzone              = 5,//qq空间
 
 */

+(void)ShareWithModel:(UShareCustomModel*)infoModel Success:(void(^)(id data))success Fail:(void(^)(NSError * error))faile;



@end
