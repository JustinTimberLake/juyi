//
//  SHSelectDateController.h
//  SanHeRealEstate
//
//  Created by duanhuifen on 17/2/7.
//  Copyright © 2017年 CAPF. All rights reserved.
//

#import "HRBaseViewController.h"

@interface JYShareViewController : HRBaseViewController

//分享按钮的点击
@property (nonatomic,copy) void(^ShareBtnActionBlock)(NSInteger tag);
@end
