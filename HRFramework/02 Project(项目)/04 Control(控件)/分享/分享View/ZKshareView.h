//
//  ZKshareView.h
//  ZK
//
//  Created by MAC on 17/3/27.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UShareCustomModel.h"

@interface ZKshareView : UIView

@property (copy, nonatomic) void(^Success)(NSString *str);

@property (copy, nonatomic) void(^Faile)(NSString *file);

@property (strong,nonatomic) UShareCustomModel *Model;

@end
