//
//  SHSelectDateController.m
//  SanHeRealEstate
//
//  Created by duanhuifen on 17/2/7.
//  Copyright © 2017年 CAPF. All rights reserved.
//

#import "JYShareViewController.h"
#import "UMengShareTool.h"
#import "UShareCustomModel.h"
//#import <UMSocialCore/UMSocialCore.h>

//typedef NS_ENUM(NSInteger,JYPlatformType) {
//    JYPlatformType_WechatSession,//微信好友
//    JYPlatformType_WechatTimeLine,//朋友圈
//    JYPlatformType_QQ,//QQ好友
//    JYPlatformType_Sina//新浪
//};
//
@interface JYShareViewController ()

//@property (nonatomic,assign) JYPlatformType platformType;
//@property (nonatomic,assign) UMSocialPlatformType platformType;

@property (nonatomic,assign) NSInteger typeCode;

@end

@implementation JYShareViewController

- (void)viewDidLoad {
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancleBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**
 
 UMSocialPlatformType_Sina               = 0, //新浪
 UMSocialPlatformType_WechatSession      = 1, //微信聊天
 UMSocialPlatformType_WechatTimeLine     = 2,//微信朋友圈
 UMSocialPlatformType_WechatFavorite     = 3,//微信收藏
 UMSocialPlatformType_QQ                 = 4,//QQ聊天页面
 UMSocialPlatformType_Qzone              = 5,//qq空间
 
 */

//微信好友  //微信朋友圈  //qq好友  //新浪微博
- (IBAction)shareBtnAction:(UIButton *)sender {
    WEAKSELF
    [self dismissViewControllerAnimated:YES completion:nil];
//    if (_ShareBtnActionBlock) {
//        _ShareBtnActionBlock(sender.tag);
//    }
    
    if (sender.tag  == 1000) {
        self.typeCode = 1;
    }else if (sender.tag == 1001){
        self.typeCode = 2;
    }else if (sender.tag == 1002){
        self.typeCode = 4;
    }else if (sender.tag == 1003){
        self.typeCode = 0;
    }
    
    [UMengShareTool ShareWithModel:[self CreatModelWithType:self.typeCode] Success:^(id data) {
        [self showSuccessTip:@"分享成功"];
    } Fail:^(NSError *error) {
        [self showSuccessTip:@"分享失败"];
    }];
}


//生成分享对象
- (UShareCustomModel*)CreatModelWithType:(NSInteger)platform{
    self.model.plantFormType = platform;
    return self.model;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}




@end
