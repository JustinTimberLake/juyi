//
//  ZKshareView.m
//  ZK
//
//  Created by MAC on 17/3/27.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "ZKshareView.h"

#import "UMengShareTool.h"


@interface ZKshareView ()
@end

@implementation ZKshareView


/**

 UMSocialPlatformType_Sina               = 0, //新浪
 UMSocialPlatformType_WechatSession      = 1, //微信聊天
 UMSocialPlatformType_WechatTimeLine     = 2,//微信朋友圈
 UMSocialPlatformType_WechatFavorite     = 3,//微信收藏
 UMSocialPlatformType_QQ                 = 4,//QQ聊天页面
 UMSocialPlatformType_Qzone              = 5,//qq空间

 */


-(instancetype)init
{
   
    self = [[[NSBundle mainBundle] loadNibNamed:@"ZKshareView" owner:self options:nil] lastObject];
    self.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.5];

    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
}

#pragma mark -- 详情界面

- (IBAction)closeButtonAction:(UIButton *)sender {
    [self removeFromSuperview];
}

//朋友圈
- (IBAction)pengYouQuanButtoAction:(UIButton *)sender {

    [self removeFromSuperview];

    [UMengShareTool ShareWithModel:[self CreatModelWithType:2] Success:^(id data) {
        if (self.Success) {
            self.Success(@"分享成功");
        }

    } Fail:^(NSError *error) {
        if (self.Faile) {
            self.Faile(@"分享失败.请重试");
        }
    }];

}
//QQ
- (IBAction)QQButtonAction:(UIButton *)sender {
[self removeFromSuperview];
    [UMengShareTool ShareWithModel:[self CreatModelWithType:4] Success:^(id data) {
        if (self.Success) {
            self.Success(@"分享成功");
        }

    } Fail:^(NSError *error) {
        if (self.Faile) {
            self.Faile(@"分享失败.请重试");
        }
    }];

}

//QQ空间
- (IBAction)QQSpaceButtonAction:(UIButton *)sender {
[self removeFromSuperview];
    [UMengShareTool ShareWithModel:[self CreatModelWithType:5] Success:^(id data) {
        if (self.Success) {
            self.Success(@"分享成功");
        }

    } Fail:^(NSError *error) {
        if (self.Faile) {
            self.Faile(@"分享失败.请重试");
        }
    }];

}

//微信
- (IBAction)wechatButtonAction:(UIButton *)sender {
[self removeFromSuperview];
    [UMengShareTool ShareWithModel:[self CreatModelWithType:1] Success:^(id data) {
        if (self.Success) {
            self.Success(@"分享成功");
        }

    } Fail:^(NSError *error) {
        if (self.Faile) {
            self.Faile(@"分享失败.请重试");
        }
    }];
}

//新浪
- (IBAction)sinaButtonAction:(UIButton *)sender {
    [self removeFromSuperview];
    [UMengShareTool ShareWithModel:[self CreatModelWithType:0] Success:^(id data) {
        if (self.Success) {
            self.Success(@"分享成功");
        }

    } Fail:^(NSError *error) {
        if (self.Faile) {
            self.Faile(@"分享失败.请重试");
        }
    }];

}

//生成分享对象
-(UShareCustomModel*)CreatModelWithType:(NSInteger)platform{
    UShareCustomModel *model = self.Model;
    model.plantFormType = platform;
    return model;
}

@end
