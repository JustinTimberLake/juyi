//
//  JYSearchView.h
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYSearchView : UIView
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;
//开始编辑block
@property (nonatomic,copy) void(^searchTextFieldEditBlock)(NSString * txt);
//取消按钮点击block
@property (nonatomic,copy) void(^cancleBtnActionBlock)();
@end
