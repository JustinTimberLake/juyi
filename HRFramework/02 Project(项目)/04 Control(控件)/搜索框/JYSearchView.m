//
//  JYSearchView.m
//  JY
//
//  Created by duanhuifen on 2017/6/29.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYSearchView.h"

@interface JYSearchView ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *searchIconImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTextfieldLeftM;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTextfieldRightM;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchIconRightM;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBgRightM;


@end
@implementation JYSearchView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.cancleBtn.hidden = YES;
    self.searchTextField.delegate = self;
//    self.frame = CGRectMake(0, 10, SCREEN_WIDTH, 32);
    [self.searchTextField addTarget:self action:@selector(textfieldTextChange:) forControlEvents:UIControlEventEditingChanged];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.searchIconRightM.constant = - (self.width /2 - 25 - self.searchIconImageView.width / 2);
    self.searchTextfieldLeftM.constant = 53;
    self.searchTextfieldRightM.constant = 65;
    self.cancleBtn.hidden = NO;
    self.searchBgRightM.constant = 65;
    self.searchTextField.textAlignment = NSTextAlignmentLeft;
}

- (void)textfieldTextChange:(UITextField *)textField{
    if (_searchTextFieldEditBlock) {
        _searchTextFieldEditBlock(textField.text);
    }
    
}
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    if (_searchTextFieldEditBlock) {
//        _searchTextFieldEditBlock(textField.text);
//    }
//    return YES;
//}

- (IBAction)cancleBtnAction:(UIButton *)sender {
//    暂时先写-90
    self.searchIconRightM.constant = -90;
    self.searchTextfieldLeftM.constant = 0;
    self.searchTextfieldRightM.constant = 0;
    self.cancleBtn.hidden = YES;
    self.searchBgRightM.constant = 10;
    self.searchTextField.textAlignment = NSTextAlignmentCenter;
    [self.searchTextField resignFirstResponder];
    self.searchTextField.text = nil;
    if (_cancleBtnActionBlock) {
        _cancleBtnActionBlock();
    }
}


@end
