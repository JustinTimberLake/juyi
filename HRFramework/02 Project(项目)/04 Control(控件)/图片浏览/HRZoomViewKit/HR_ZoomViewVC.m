//
//  HR_ZoomViewVC.m
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "HR_ZoomViewVC.h"
#import "HR_ZoomViewDelegate.h"
#import "HR_ZoomViewItem.h"


@interface HR_ZoomViewVC ()

@property (strong, nonatomic) UICollectionView *collectionVeiw;

@property (strong, nonatomic) UICollectionViewFlowLayout *layout;

@property (strong, nonatomic) HR_ZoomViewDelegate *zoomViewDelegate;

@property (strong, nonatomic) HR_ZoomViewItem *zoomViewItem;

@property (strong, nonatomic) UILabel *pageLabel;

@property (assign, nonatomic) NSInteger count;

@property (nonatomic,strong) UIView * discribeBgView;
//文字描述
@property (nonatomic,strong) UITextView *discribeTextView;

@property (nonatomic,strong) UILabel *discribeLab;

@end

@implementation HR_ZoomViewVC

static CGFloat const Padding = 10;
static CGFloat const TopMargin = 84;


CGSize screenSize () {
    return [UIScreen mainScreen].bounds.size;
}

CGRect collectionRect () {
    return CGRectMake(0, 0, screenSize().width , screenSize().height);
   
}

- (UICollectionView *)collectionVeiw {
    if(!_collectionVeiw) {
        _collectionVeiw = [[UICollectionView alloc] initWithFrame:collectionRect() collectionViewLayout:self.layout];
        _collectionVeiw.pagingEnabled = YES;
        _collectionVeiw.showsHorizontalScrollIndicator = NO;
        _collectionVeiw.bounces = NO;
    }
    return _collectionVeiw;
}

- (UICollectionViewFlowLayout *)layout {
    if(!_layout) {
        _layout = [[UICollectionViewFlowLayout alloc] init];
        _layout.itemSize = CGSizeMake(screenSize().width, screenSize().height - TopMargin * 2);
        _layout.minimumLineSpacing = 0;
        _layout.minimumInteritemSpacing = 0;
        _layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return _layout;
}

- (HR_ZoomViewDelegate *)zoomViewDelegate {
    if(!_zoomViewDelegate) {
        _zoomViewDelegate = [HR_ZoomViewDelegate new];
    }
    return _zoomViewDelegate;
}

- (UIView *)discribeBgView
{
    if (!_discribeBgView) {
        _discribeBgView = [[UIView alloc] init];
        _discribeBgView.frame = CGRectMake(0, screenSize().height - 100, screenSize().width, 100);
        _discribeBgView.backgroundColor = [UIColor clearColor];//[[UIColor blackColor] colorWithAlphaComponent:0.6];
    }
    return _discribeBgView;
}

- (UILabel *)discribeLab
{
    if (!_discribeLab) {
        _discribeLab = [[UILabel alloc] init];
        _discribeLab.frame = CGRectMake(Padding, Padding, screenSize().width - Padding * 2, _discribeBgView.height);
        _discribeLab.backgroundColor = [UIColor clearColor];
        _discribeLab.text = self.contentText;
        _discribeLab.numberOfLines = 0;
        _discribeLab.font = FONT(14);
        _discribeLab.textColor = [UIColor whiteColor];
        [_discribeBgView addSubview:_discribeTextView];

    }
    return _discribeLab;
}

//- (UITextView *)discribeTextView
//{
//    if (!_discribeTextView) {
//        _discribeTextView = [[UITextView alloc] init];
//        _discribeTextView.frame = CGRectMake(Padding, Padding, _discribeBgView.width - Padding * 2, _discribeBgView.height);
//        _discribeTextView.backgroundColor = [UIColor clearColor];
//        _discribeTextView.text = self.contentText;
//        _discribeTextView.editable = NO;
//        _discribeTextView.font = [UIFont systemFontOfSize:14];
//        _discribeTextView.textColor = [UIColor whiteColor];
//        [_discribeBgView addSubview:_discribeTextView];
//        
//    }
//    return _discribeTextView;
//}


- (UILabel *)pageLabel {
    if(!_pageLabel) {
        _pageLabel = [[UILabel alloc] init];
        [_pageLabel setFrame:CGRectMake(0, 20, screenSize().width, 30)];
        [_pageLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [_pageLabel setTextColor:[UIColor whiteColor]];
        [_pageLabel setTextAlignment:NSTextAlignmentCenter];
        [_pageLabel setText:@"0/0"];
    }
    return _pageLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadUI];
    [self initData];
    [self changePage];
    [self loadConfig];
    [self addNotification];
    
}

- (void)initData {
    
    if(_dataSourceArrForNamesGroup != nil && _dataSourceArrForNamesGroup.count != 0) {
        if(_dataSourceArrForNamesGroup.count < _idx) {_idx = 1;}
        self.zoomViewDelegate = [[HR_ZoomViewDelegate alloc] initWithImageNamesGroup:_dataSourceArrForNamesGroup
                                                                               index:_idx];
        
    }
    if(_dataSourceArrForURLsGroup != nil && _dataSourceArrForURLsGroup.count !=0) {
        if(_dataSourceArrForURLsGroup.count < _idx) {_idx = 1;}
        self.zoomViewDelegate = [[HR_ZoomViewDelegate alloc] initWithImageURLStringsGroup:_dataSourceArrForURLsGroup
                                                                                    index:_idx];
    }
    if(_dataSourceArrForPhotoLibraryGroup != nil && _dataSourceArrForPhotoLibraryGroup.count !=0) {
        if(_dataSourceArrForPhotoLibraryGroup.count < _idx) {_idx = 1;}
        self.zoomViewDelegate = [[HR_ZoomViewDelegate alloc] initWithImagePhotoLibraryGroup:_dataSourceArrForPhotoLibraryGroup
                                                                                      index:_idx];
    }
    
    if (self.contentText.length) {
        CGFloat contentH = [self.contentText stringHeightAtWidth:(screenSize().width - Padding * 2) font:FONT(14)];
        CGFloat bgViewH =  contentH + Padding + 25 ;
        _discribeBgView.frame = CGRectMake(0, screenSize().height - bgViewH, screenSize().width, bgViewH);
        _discribeLab.frame = CGRectMake(Padding, 0, _discribeBgView.width - Padding * 2, _discribeBgView.height);
    }
    
}

- (void)changePage {
//    WEAKSELF
    if (_dataSourceArrForNamesGroup!=nil && _dataSourceArrForNamesGroup.count!=0) {
        self.count = _dataSourceArrForNamesGroup.count;
    }
    if (_dataSourceArrForURLsGroup!=nil && _dataSourceArrForURLsGroup.count!=0) {
        self.count = _dataSourceArrForURLsGroup.count;
    }
    if (_dataSourceArrForPhotoLibraryGroup!=nil && _dataSourceArrForPhotoLibraryGroup.count!=0) {
        self.count = _dataSourceArrForPhotoLibraryGroup.count;
    }

    [self.pageLabel setText:[NSString stringWithFormat:@"%ld/%ld", _idx, self.count]];
    [self.zoomViewDelegate changePageBlock:^(NSInteger page) {
        [self.pageLabel setText:[NSString stringWithFormat:@"%ld/%ld", page + 1, self.count]];
        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:page inSection:0];
//       HR_ZoomViewItem * cell = (HR_ZoomViewItem *) [weakSelf.collectionVeiw cellForItemAtIndexPath:indexPath];
//        cell.getImageViewBlock = ^(UIImageView *imageView) {
//            if (imageView.height < 325) {
//                weakSelf.discribeBgView.frame = CGRectMake(0, screenSize().height - 220, screenSize().width, 220);
//            }else{
//                weakSelf.discribeBgView.frame = CGRectMake(0, screenSize().height - 100, screenSize().width, 100);
//            }
//        };
    }];
//    self.zoomViewDelegate.GetImgSizeBlock = ^(CGFloat imageH) {
//        [weakSelf changeDiscribeBgViewWithHeight:imageH];
//    };
}

- (void)loadUI {
    [self.view addSubview:self.collectionVeiw];
    [self.view addSubview:self.pageLabel];
    [self.view addSubview:self.discribeBgView];
    [self.discribeBgView addSubview:self.discribeLab];
//    [self.discribeBgView addSubview:self.discribeTextView];

    
}

- (void)loadConfig {
    [self.collectionVeiw registerClass:[HR_ZoomViewItem class] forCellWithReuseIdentifier:@"HR_ZoomViewItem"];
    [self.collectionVeiw setDelegate:self.zoomViewDelegate];
    [self.collectionVeiw setDataSource:self.zoomViewDelegate];
    [self.collectionVeiw setContentOffset:CGPointMake((_idx-1) * screenSize().width, self.collectionVeiw.contentOffset.y) animated:NO];
}

- (void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tapGestureAction:) name:@"TAPGESTUREACTION" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getImageHeight:) name:@"GETNEWIMAGESIZE" object:nil];
}

- (void)tapGestureAction:(NSNotification *)notification {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)getImageHeight:(NSNotification *)notification{
//   CGFloat imageSizeH = [notification.userInfo[@"imageSizeH"] floatValue];
//    [self changeDiscribeBgViewWithHeight:imageSizeH];
//}

//- (void)changeDiscribeBgViewWithHeight:(CGFloat)height{
//    if (height < 325) {
//        self.discribeBgView.frame = CGRectMake(0, screenSize().height - 220, screenSize().width, 220);
//    }else{
//        self.discribeBgView.frame = CGRectMake(0, screenSize().height - 100, screenSize().width, 100);
//    }
//
//}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
