//
//  HR_ZoomViewDelegate.h
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^ChangePageBlack)(NSInteger page);

@class HR_ZoomViewItem;

@interface HR_ZoomViewDelegate : NSObject<UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

/** Description:网络图片 */
@property (strong, nonatomic,readonly) NSMutableArray *imgURLGroup;

/** Description:本地图片 */
@property (strong, nonatomic,readonly) NSMutableArray *imgNamesGroup;

/** Description:页数更新回调 */
@property (copy, nonatomic) ChangePageBlack changePage;

- (void)changePageBlock:(ChangePageBlack)changePageBlock;

- (instancetype)initWithImagePhotoLibraryGroup:(NSArray *)imagePhotoLibraryGroup index:(NSInteger)idx;

- (instancetype)initWithImageURLStringsGroup:(NSArray *)imageURLStringsGroup index:(NSInteger)idx;

- (instancetype)initWithImageNamesGroup:(NSArray *)imageNamesGroup index:(NSInteger)idx;
//获取图片尺寸
//@property (nonatomic,copy) void(^GetImgSizeBlock)(CGFloat imageH);

@end
