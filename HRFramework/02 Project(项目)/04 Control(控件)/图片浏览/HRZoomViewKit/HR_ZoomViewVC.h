//
//  HR_ZoomViewVC.h
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface HR_ZoomViewVC : UIViewController

/** Description:本地图片 */
@property (strong, nonatomic) NSArray *dataSourceArrForNamesGroup;
/** Description:网络图片 */
@property (strong, nonatomic) NSArray *dataSourceArrForURLsGroup;
/** Description:网络图片 */
@property (strong, nonatomic) NSArray *dataSourceArrForPhotoLibraryGroup;
/** Description:下标 从0开始 */
@property (assign, nonatomic) NSInteger idx;

@property (nonatomic,copy) NSString * contentText;


@end
