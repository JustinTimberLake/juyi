//
//  HR_ZoomViewDelegate.m
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "HR_ZoomViewDelegate.h"
#import "HR_ZoomViewItem.h"


@interface HR_ZoomViewDelegate ()

@property (strong, nonatomic) NSMutableArray *imageUrlStringsGroup;
@property (strong, nonatomic) NSMutableArray *imageNamesGroup;
@property (strong, nonatomic) NSMutableArray *imagePhotoLibraryGroup;
@property (assign, nonatomic) NSInteger idx;

@end

@implementation HR_ZoomViewDelegate


- (instancetype)initWithImagePhotoLibraryGroup:(NSArray *)imagePhotoLibraryGroup index:(NSInteger)idx {
    self = [super init];
    if(self) {
        _imagePhotoLibraryGroup = [NSMutableArray arrayWithArray:imagePhotoLibraryGroup];
        _idx = idx;
    }
    return self;
}

- (instancetype)initWithImageURLStringsGroup:(NSArray *)imageURLStringsGroup index:(NSInteger)idx {
    self = [super init];
    if(self) {
        _imageUrlStringsGroup = [NSMutableArray arrayWithArray:imageURLStringsGroup];
        _idx = idx;
    }
    return self;
}

- (instancetype)initWithImageNamesGroup:(NSArray *)imageNamesGroup index:(NSInteger)idx {
    self = [super init];
    if(self) {
        _imageNamesGroup = [NSMutableArray arrayWithArray:imageNamesGroup];
        _idx = idx;
    }
    return self;
}

- (NSMutableArray *)imgURLGroup {
    return _imageUrlStringsGroup;
}
- (NSMutableArray *)imgNamesGroup {
    return _imageNamesGroup;
}
- (NSMutableArray *)imagePhotoLibraryGroup {
    return _imagePhotoLibraryGroup;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_imageNamesGroup!=nil && _imageNamesGroup.count!=0) {
        return _imageNamesGroup.count;
    }
    if (_imageUrlStringsGroup!=nil && _imageUrlStringsGroup.count!=0) {
        return _imageUrlStringsGroup.count;
    }
    if (_imagePhotoLibraryGroup!=nil && _imagePhotoLibraryGroup.count!=0) {
        return _imagePhotoLibraryGroup.count;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"HR_ZoomViewItem";
    HR_ZoomViewItem * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    if(!cell) {
        cell = [[HR_ZoomViewItem alloc] init];
    }
    if(_imageNamesGroup != nil && _imageNamesGroup.count != 0) {
        [cell setLocalStr:_imageNamesGroup[indexPath.row]];
    }
    if(_imageUrlStringsGroup != nil && _imageUrlStringsGroup.count != 0) {
        [cell setUrlStr:_imageUrlStringsGroup[indexPath.row]];
    }
    if(_imagePhotoLibraryGroup != nil && _imagePhotoLibraryGroup.count != 0) {
        [cell setPhotoImg:_imagePhotoLibraryGroup[indexPath.row]];
    }
    
    cell.backgroundColor = [UIColor colorWithRed:arc4random()%256/255.0 green:arc4random()%256/255.0 blue:arc4random()%256/255.0 alpha:1];
    
    return cell;
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
      if(self.changePage) {
        self.changePage(scrollView.contentOffset.x / [UIScreen mainScreen].bounds.size.width);
    }
}

//- (void)addObserver{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getImageHeight:) name:@"GETNEWIMAGESIZE" object:nil];
//}
//- (void)getImageHeight:(NSNotification *)noti{
//    CGFloat imageH = [noti.userInfo[@"imageSizeH"] floatValue];
//    if (self.GetImgSizeBlock) {
//        self.GetImgSizeBlock(imageH);
//    }
//
//}

- (void)changePageBlock:(ChangePageBlack)changePageBlock {
    self.changePage = changePageBlock;
}

//- (void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}
@end
