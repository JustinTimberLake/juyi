//
//  HR_ZoomViewItem.m
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "HR_ZoomViewItem.h"
#import "HR_ImageZoomView.h"

@interface HR_ZoomViewItem()
{
    HR_ImageZoomView *imageZoomView;
}

@end

@implementation HR_ZoomViewItem

- (void)setPhotoImg:(UIImage *)photoImg
{
    _photoImg = photoImg;
    [[self loadUI]  loadPhotoImage:_photoImg];
}

- (void)setLocalStr:(NSString *)localStr {
    _localStr = localStr;
    [[self loadUI]  loadLocalImage:_localStr];
}

- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
    [[self loadUI] loadImgWithUrl:_urlStr];

}

- (HR_ImageZoomView *)loadUI {
    if(imageZoomView == nil) {
        imageZoomView = [[HR_ImageZoomView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        
        [self.contentView addSubview:imageZoomView];
    }
    return imageZoomView;
}

@end
