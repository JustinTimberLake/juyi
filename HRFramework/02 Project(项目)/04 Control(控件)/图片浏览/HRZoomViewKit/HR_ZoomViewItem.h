//
//  HR_ZoomViewItem.h
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HR_ZoomViewItem : UICollectionViewCell

@property (strong, nonatomic) NSString *localStr;

@property (strong, nonatomic) NSString *urlStr;

@property (nonatomic ,strong) UIImage *photoImg;

/** Description:下标 */
@property (assign, nonatomic) NSInteger idx;


@end
