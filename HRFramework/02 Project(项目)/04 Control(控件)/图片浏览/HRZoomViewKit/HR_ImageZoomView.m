//
//  HR_ImageZoomView.m
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "HR_ImageZoomView.h"
//#import "UIImageView+WebCache.h"
#define HandDoubleTap 2
#define HandOneTap 1
#define MaxZoomScaleNum 4.0
#define MinZoomScaleNum 1.0


@interface HR_ImageZoomView ()<NSURLSessionDownloadDelegate>
{
    NSURLConnection* connection;
    
    NSMutableData* data;
}

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIView *containerView;


@end

@implementation HR_ImageZoomView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self loadUI];
        [self addGesture];
    }
    return self;
}


//获取图片和显示视图宽度的比例系数
- (float)getImgWidthFactor {
    return   self.bounds.size.width / self.image.size.width;
}
//获取图片和显示视图高度的比例系数
- (float)getImgHeightFactor {
    return  self.bounds.size.height / self.image.size.height;
}
//获取尺寸
- (CGSize)newSizeByoriginalSize:(CGSize)oldSize maxSize:(CGSize)mSize
{
    if (oldSize.width <= 0 || oldSize.height <= 0) {
        return CGSizeZero;
    }
    
    CGSize newSize = CGSizeZero;
    if (oldSize.width > mSize.width || oldSize.height > mSize.height) {
        //按比例计算尺寸
        float bs = [self getImgWidthFactor];
        float newHeight = oldSize.height * bs;
        newSize = CGSizeMake(mSize.width, newHeight);
        
        if (newHeight > mSize.height) {
            bs = [self getImgHeightFactor];
            float newWidth = oldSize.width * bs;
            newSize = CGSizeMake(newWidth, mSize.height);
        }
    }else {
        
        newSize = oldSize;
    }
    return newSize;
}

- (UIScrollView *)scrollView {
    if(!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.delegate = self;
        _scrollView.bounces = NO;
        _scrollView.maximumZoomScale = MaxZoomScaleNum;
        _scrollView.minimumZoomScale = MinZoomScaleNum;
        _scrollView.zoomScale = MinZoomScaleNum;
    }
    return _scrollView;
}

- (UIView *)containerView {
    if(!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:self.bounds];
    }
    return _containerView;
}

- (UIImageView *)imageView {
    if(!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    return _imageView;
}



- (void)addGesture {
    //双击
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(TapsAction:)];
    [doubleTapGesture setNumberOfTapsRequired:HandDoubleTap];
    [self.containerView addGestureRecognizer:doubleTapGesture];
    //单击
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(TapsAction:)];
    [tapGesture setNumberOfTapsRequired:HandOneTap];
    [self.containerView addGestureRecognizer:tapGesture];
    //双击失败之后执行单击
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapsAction:)];
    [self.scrollView addGestureRecognizer:tapGesture];
}

- (void)loadUI {
    self.backgroundColor = [UIColor blackColor];
    self.clipsToBounds = YES;
    self.contentMode = UIViewContentModeScaleAspectFill;
    [self.scrollView addSubview:self.containerView];
    [self addSubview:self.scrollView];
    [self.containerView addSubview:self.imageView];
}

#pragma mark- 手势事件
- (void)TapsAction:(UITapGestureRecognizer *)tap
{
    CGPoint point = [tap locationInView:self];
    NSLog(@"x:%f y:%f", point.x, point.y);
    NSInteger tapCount = tap.numberOfTapsRequired;
    if (HandDoubleTap == tapCount) {
        NSLog(@"双击");
        if (self.scrollView.minimumZoomScale <= self.scrollView.zoomScale && self.scrollView.maximumZoomScale > self.scrollView.zoomScale) {
            [self.scrollView setZoomScale:self.scrollView.maximumZoomScale animated:YES];
            
        }else {
            [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:YES];
        }
    }else if (HandOneTap == tapCount) {
        NSLog(@"单击");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TAPGESTUREACTION" object:nil];
    }
}

#pragma mark- Properties

- (UIImage *)image {
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image {
    if(self.imageView == nil){
        self.imageView = [UIImageView new];
        self.imageView.clipsToBounds = YES;
    }
    self.imageView.image = image;
}

//相册图片
- (void)loadPhotoImage:(UIImage *)photoImg {
    self.imageView.image = photoImg;
    [self setImageViewWithImg:self.image];
}


// 本地图片
- (void)loadLocalImage:(NSString *)imgName {
    self.image = [UIImage imageNamed:imgName];
    self.imageView.image = self.image;
    [self setImageViewWithImg:self.image];
}
// 网络图片
- (void)loadImgWithUrl:(NSString *)url {
    __weak typeof(self) weakSelf = self;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [weakSelf setImageViewWithImg:image];
    }];
}

- (void)setImageViewWithImg:(UIImage *)img {
    CGSize showSize = [self newSizeByoriginalSize:img.size maxSize:self.bounds.size];
    self.imageView.frame = CGRectMake(0, 0, showSize.width, showSize.height);
    self.scrollView.zoomScale = MinZoomScaleNum;
    self.scrollView.contentOffset = CGPointZero;
    self.containerView.bounds = self.imageView.bounds;
    self.scrollView.zoomScale  = self.scrollView.minimumZoomScale;
    [self scrollViewDidZoom:self.scrollView];
    
//    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
//    dic[@"imageSizeH"] = @(showSize.height);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"GETNEWIMAGESIZE" object:nil userInfo:dic];

}

- (void) config {
    
}

#pragma mark- Scrollview delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.containerView;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat Ws = self.scrollView.frame.size.width - self.scrollView.contentInset.left - self.scrollView.contentInset.right;
    CGFloat Hs = self.scrollView.frame.size.height - self.scrollView.contentInset.top - self.scrollView.contentInset.bottom;
    CGFloat W = self.containerView.frame.size.width;
    CGFloat H = self.containerView.frame.size.height;
    
    CGRect rct = self.containerView.frame;
    rct.origin.x = MAX((Ws-W)*0.5, 0);
    rct.origin.y = MAX((Hs-H)*0.5, 0);
    self.containerView.frame = rct;
    
}
- (void)loadImgurl:(NSURL *)url {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForRequest = 20;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration
                                                          delegate:self
                                                     delegateQueue:[[NSOperationQueue alloc]init]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDownloadTask *downTask = [session downloadTaskWithRequest:request];
    [downTask resume];
}

- (NSString *)filePathWithFileName:(NSString *)fileName {
    return [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:fileName];
}
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    CGFloat progress = 1.0 * totalBytesWritten / totalBytesExpectedToWrite; NSLog(@"%f",progress);
}
- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *filePath = [self filePathWithFileName:downloadTask.response.suggestedFilename];
        [[NSFileManager defaultManager] moveItemAtURL:location toURL:[NSURL fileURLWithPath:filePath] error:nil];
        UIImage *img = [UIImage imageWithContentsOfFile:filePath];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setImageViewWithImg:img];
        });
    });
    
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
}
@end
