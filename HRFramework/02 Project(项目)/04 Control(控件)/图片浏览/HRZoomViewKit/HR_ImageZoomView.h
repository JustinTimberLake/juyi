//
//  HR_ImageZoomView.h
//  HRZoomViewKit
//
//  Created by 王凯 on 2017/3/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TapGestureActionBlock) ();

@interface HR_ImageZoomView : UIView <UIScrollViewDelegate>

- (void)loadPhotoImage:(UIImage *)photoImg;
- (void)loadLocalImage:(NSString *)imgName;
- (void)loadImgWithUrl:(NSString *)url;
@end
