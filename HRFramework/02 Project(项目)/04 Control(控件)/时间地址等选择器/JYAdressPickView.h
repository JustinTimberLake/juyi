//
//  JYAdressPickView.h
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, AddressType) {
    AddressTypeDefail,
    AddressTypeToYear,
    AddressTypeToCity,
};

typedef void(^ChosedAddressBlock)(NSString *provinceName,NSString *rovinceId,NSString *cityName,NSString *cityId);

typedef void(^threeChosedAddressBlock)(NSString *provinceName,NSString *provinceId,NSString *cityName,NSString *cityId,NSString *areaName,NSString *areaId);

@interface JYAdressPickView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
//地址类型
@property (nonatomic, assign) AddressType addressType;

@property (nonatomic, copy) ChosedAddressBlock chosedAddressBlock;
@property (copy, nonatomic) threeChosedAddressBlock threeChosedAddressBlock;
/** 判断三级联动 （dataArrayNumberStr == three） */
@property (copy, nonatomic) NSString *dataArrayNumberStr;

-(instancetype)initWithDatas:(NSMutableArray *)datasArr AndGetContent:(void(^)(NSString *))contentStr;

- (void)setUpDataWithArr:(NSMutableArray *)arr;
@end
