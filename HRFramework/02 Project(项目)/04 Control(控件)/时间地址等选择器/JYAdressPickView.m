//
//  JYAdressPickView.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/24.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAdressPickView.h"
#import "JYPickCityModel.h"
@interface JYAdressPickView ()
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (nonatomic, strong) NSMutableArray *addressArr;

@property (nonatomic, copy) void(^addressBlock)(NSString *addressStr);
/** 省 */
@property (nonatomic, assign) NSInteger selProvinceIndex;
/** 市 */
@property (nonatomic, assign) NSInteger selCityIndex;


@property (nonatomic,copy) NSString * provinceName;

@property (nonatomic,copy) NSString * provinceId;

@property (nonatomic,copy) NSString * cityName;

@property (nonatomic,copy) NSString * cityId;

@property (copy, nonatomic) NSString *areaName;

@property (copy, nonatomic) NSString *areaId;

@end

@implementation JYAdressPickView

-(instancetype)initWithDatas:(NSMutableArray *)datasArr AndGetContent:(void(^)(NSString *))contentStr{
    self = [[[NSBundle mainBundle]loadNibNamed:@"JYAdressPickView" owner:self options:nil]lastObject];
    if (self) {
        
        self.addressBlock = contentStr;
        [self initWithData];
    }
    
    return self;
}

-(void) initWithData{
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
}

- (void)setUpDataWithArr:(NSMutableArray *)arr{
    self.addressArr = arr;
    
    JYPickCityModel * provinceModel = self.addressArr[0];
    JYPickCityModel * cityModel = provinceModel.citys[0];
    JYPickCityModel * areaModel = cityModel.countys[0];
    self.provinceName = provinceModel.provinceTitle;
    self.provinceId = provinceModel.provinceCode;
    self.cityName = cityModel.cityTitle;
    self.cityId = cityModel.cityCode;
    self.areaName = areaModel.countyTitle;
    self.areaId = areaModel.countyCode;
   // NSLog(@"~~~%@",self.);
    [self.pickerView reloadAllComponents];
}
#pragma mark IBAction
//取消
- (IBAction)cancelAction:(id)sender {
    [self removeFromSuperview];
}
//确定
- (IBAction)submitAction:(id)sender {
    
    if ([self.dataArrayNumberStr isEqualToString:@"three"]) {
        if (self.threeChosedAddressBlock) {
            self.threeChosedAddressBlock(self.provinceName,self.provinceId,self.cityName,self.cityId,self.areaName,self.areaId);
        }
    }else{
        if (self.chosedAddressBlock) {
            self.chosedAddressBlock(self.provinceName,self.provinceId,self.cityName,self.cityId);
        }
    }
    [self removeFromSuperview];
}

#pragma mark UIPickerDelegate UIPickerDasource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (self.addressType == AddressTypeToCity) {
        return [self.dataArrayNumberStr isEqualToString:@"three"] ? 3 : 2;
    }else if(self.addressType == AddressTypeDefail){
        return 3;
    }else{
        return 1;
    }
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (self.addressType == AddressTypeToCity && self.addressArr.count) {
        if (component == 0) {
            return self.addressArr.count;
        }else if (component == 1){
            JYPickCityModel * provinceModel = self.addressArr[_selProvinceIndex];
            NSLog(@"%@",self.addressArr[_selProvinceIndex]);
            return provinceModel.citys.count;
        }else if ([self.dataArrayNumberStr isEqualToString:@"three"]){
            if (component == 2) {
                JYPickCityModel * provinceModel = self.addressArr[_selProvinceIndex];
                if (provinceModel.citys.count > 0) {//防止省下 无市县
                    JYPickCityModel * cityModel = provinceModel.citys[_selCityIndex];
                    return cityModel.countys.count;
                } else {
                    return 0;
                }
            }
        }
    }
    return 10;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.addressType == AddressTypeToCity ? SCREEN_WIDTH/2 : SCREEN_WIDTH/3, 50*(SCREEN_HEIGHT/667))];
    label.font = [UIFont systemFontOfSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = RGB0X(0x333333);
    if (self.addressType == AddressTypeToCity && self.addressArr.count) {
        if (component == 0) {
            JYPickCityModel * provinceModel = self.addressArr[row];
            label.text = provinceModel.provinceTitle;
        }else if (component == 1){
            JYPickCityModel * provinceModel = self.addressArr[_selProvinceIndex];
            JYPickCityModel * cityModel = provinceModel.citys[row];
            label.text = cityModel.cityTitle;
        }else if ([self.dataArrayNumberStr isEqualToString:@"three"]){
            if (component == 2) {
                JYPickCityModel * provinceModel = self.addressArr[_selProvinceIndex];
                JYPickCityModel * cityModel = provinceModel.citys[_selCityIndex];
                JYPickCityModel * areaModel = cityModel.countys[row];
                label.text = areaModel.countyTitle;
            }
        }
    }
    return label;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50*(SCREEN_HEIGHT/667);
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (self.addressType == AddressTypeToCity && self.addressArr.count) {
        if (component == 0) {
            _selProvinceIndex = row;
            [pickerView reloadComponent:1];
            if ([self.dataArrayNumberStr isEqualToString:@"three"]) {
                [pickerView reloadComponent:2];
            }
            [pickerView selectRow:0 inComponent:1 animated:YES];
        }
        if ([self.dataArrayNumberStr isEqualToString:@"three"]) {
            if (component == 1) {
                _selCityIndex =row;
                [pickerView reloadComponent:2];
            }
        }
        JYPickCityModel * provinceModel = self.addressArr[_selProvinceIndex];
        // 获取第1列选中的行
        if (provinceModel.citys.count > 0) {
            NSInteger cityIndex = [pickerView selectedRowInComponent:1];
            JYPickCityModel * cityModel = provinceModel.citys[cityIndex];
            self.provinceName = provinceModel.provinceTitle;
            self.provinceId = provinceModel.provinceCode;
            self.cityName = cityModel.cityTitle;
            self.cityId = cityModel.cityCode;
            if ([self.dataArrayNumberStr isEqualToString:@"three"]){
                NSInteger areaIndex = [pickerView selectedRowInComponent:2];
                JYPickCityModel * areaModel = cityModel.countys[areaIndex];
                self.areaName = areaModel.countyTitle;
                self.areaId = areaModel.countyCode;
            }
        } else {
            self.provinceName = provinceModel.provinceTitle;
//            self.provinceId = provinceModel.provinceCode;
//            self.cityName = provinceModel.name;
//            self.cityId = provinceModel.regionId;
//            self.areaName = provinceModel.name;
//            self.areaId = provinceModel.regionId;
        }
        
    }
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    if (self.addressType == AddressTypeToCity && self.addressArr.count) {
//        if (component == 0) {
//            _selProvinceIndex = row;
//            [pickerView reloadComponent:1];
//            [pickerView selectRow:0 inComponent:1 animated:YES];
//        }
//        HQYMRegionModel * provinceModel = self.addressArr[_selProvinceIndex];
//        // 获取第1列选中的行
//        NSInteger cityIndex = [pickerView selectedRowInComponent:1];
//        HQYMRegionModel * cityModel = provinceModel.list[cityIndex];
//
//
//        self.provinceName = provinceModel.name;
//        self.provinceId = provinceModel.regionId;
//        self.cityName = cityModel.name;
//        self.cityId = cityModel.regionId;
//
//    }
//    return @"";
//}

#pragma mark 懒加载
-(NSMutableArray *)addressArr{
    if (!_addressArr) {
        _addressArr = [NSMutableArray array];
    }
    return _addressArr;
}
@end
