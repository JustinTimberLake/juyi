//
//  JYPickCityModel.m
//  JY
//
//  Created by YunPeng-Gao on 2017/7/25.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPickCityModel.h"

@implementation JYPickCityModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"list":@"JYPickCityModel"};
}

@end
