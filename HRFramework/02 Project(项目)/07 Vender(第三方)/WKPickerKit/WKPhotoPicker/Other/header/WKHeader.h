//
//  WKHeader.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#ifndef WKHeader_h
#define WKHeader_h

/** ------------------ config -----------------  */

#import "WKConfig.h"

/** ------------------ Framework -----------------  */

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAssetImageGenerator.h>
#import <AVFoundation/AVAsset.h>
#import <Photos/Photos.h>

/** ------------------ Model - Tool ----------------  */

#import "WKAlbum.h"
#import "WKTool.h"
#import "WKAlertManager.h"
#import "WKAlbumClient.h"

/** ------------------ Category --------------------  */

#import "UIView+responder.h"

/** ------------------ View - cell -----------------  */

#import "WKAlbumsListCell.h"
#import "WKPickerThumCell.h"
#import "CollectionViewCell.h"
#import "WKOriginalAssetListCell.h"

/** ------------------ Controller ------------------  */

#import "WKAlbumsListViewController.h"
#import "WKPickerViewController.h"
#import "WKOriginalAssetListController.h"

/** ------------------ NavigationController --------  */

#import "WKNavigationController.h"

/** ------------------ Model -----------------------  */

#import "WKAssetModel.h"

/** ------------------ ViewModel -------------------  */

#import "WKAssetViewModel.h"

#import "WKPhotoView.h"

#endif /* WKHeader_h */
