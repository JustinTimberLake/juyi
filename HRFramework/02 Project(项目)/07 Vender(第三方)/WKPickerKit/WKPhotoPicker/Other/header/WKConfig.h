//
//  WKConfig.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <objc/message.h>


/** --------------- typedef Block & Enum --------------- */

// Block //
// 封装
typedef void(^WKAssetEncapsulation)();
// 选中
typedef void(^WKAssetSelected)();
// 取消选中
typedef void(^WKAssetUnSelected)();
// 所有选中的图片
typedef void(^WKAssetSelectedAllImage)(NSArray *images);
// 视频压缩
typedef void(^CompressionSuccessBlock) (NSString *path, CGFloat dataSize, NSData *data, CGFloat totaSize);

// Enum //
typedef NS_ENUM(NSInteger, WKAlertDataSourceType) {
    WKAlertDataSourceTypeForAlbum,
    WKAlertDataSourceTypeForCamera,
    WKAlertDataSourceTypeForVideo,
    WKAlertDataSourceTypeForAlbumAndCamera,
    WKAlertDataSourceTypeForAlbumAndVideo,
    WKAlertDataSourceTypeForCameraAndVideo,
    WKAlertDataSourceTypeAll
};



/** --------------- Macro --------------- */

/** 过期提醒 */
#define WKPhotoDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)
/** 弱引用 */
#define WKWeakSelf __weak typeof(self) weakSelf = self;
/** 物理宽度 */
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
/** 物理高度 */
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
/** 物理规模 */
#define SCREEN_SCALE [UIScreen mainScreen].scale
/** 总指挥部 */
#define WKClient [WKAlbumClient sharedInstance]
/** 根控制器 */
#define WKRootVC [UIApplication sharedApplication].keyWindow.rootViewController
/** 视频路径 */
#define CompressionVideoPaht [NSHomeDirectory() stringByAppendingFormat:@"/Documents/CompressionVideoField"]


/** ----------------- WKPhotoConst -----------  */

UIKIT_EXTERN const CGFloat WKSpacing;
UIKIT_EXTERN NSString *const WKPickerThumCellIdentifier;
UIKIT_EXTERN NSString *const WKCollectionViewCellIdentifier;
UIKIT_EXTERN NSString *const WKOriginalAssetListCellIdentifier;
UIKIT_EXTERN NSString *const WKAddImgNamed; // 添加图片的按钮图片

/** ----------------- Methods - C -----------  */

CGFloat compressionCoefficient ();      // 图片压缩系数
CGSize itemSize (int count);            // 相册展示size
CGSize photoViewitemSize (int count);   // 回显ViewSize
CGSize originalAssetListitemSize ();    // 原图展示size

CGRect layoutFrameForLess4 (float x, float y, int count);
CGRect layoutFrameForLess (float x, float y, int count);







