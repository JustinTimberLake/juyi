//
//  WKNavigationController.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKNavigationController.h"
#import "WKAlbumsListViewController.h"
#import "WKHeader.h"

@interface WKNavigationController ()

@property (strong, nonatomic) NSMutableArray *myChildControllers;

@property (strong, nonatomic) WKAlbum *album;

@end

@implementation WKNavigationController

@synthesize delegate = _delegate;



- (instancetype)initWithDelegate:(id<WKPickerDelegate>)delegate {
    if(self = [super init]) {
        
        [self setViewControllers:self.myChildControllers animated:YES];
        self.wdelegate = delegate;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction)];
    
    
}

- (void)cancelAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (NSMutableArray *)myChildControllers {
    if(!_myChildControllers) {
        _myChildControllers = [NSMutableArray arrayWithObjects:[NSClassFromString(@"WKAlbumsListViewController") new],[NSClassFromString(@"WKPickerViewController") new], nil];
        
        
    }
    return _myChildControllers;
}

- (WKAlbum *)album {
    if(!_album) {
        _album = [[WKAlbum alloc] init];
    }
    return _album;
}

- (void)setShowCount:(NSInteger)showCount {
    _showCount = showCount;
    WKClient.showCount = _showCount;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
