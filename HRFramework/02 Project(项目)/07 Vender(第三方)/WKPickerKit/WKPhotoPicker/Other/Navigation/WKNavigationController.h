//
//  WKNavigationController.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol WKPickerDelegate;

@interface WKNavigationController : UINavigationController

/** Description:代理 */
@property (assign, nonatomic) id<WKPickerDelegate> wdelegate;

- (instancetype)initWithDelegate:(id<WKPickerDelegate>)delegate;

/** Description:限定显示的个数 */
@property (assign, nonatomic) NSInteger showCount;

- (void)cancelAction;

@end

@protocol WKPickerDelegate <NSObject>

- (void)pickerSelectedAssets:(NSArray *)assets;

- (void)pickerSelectedImages:(NSArray *)images;

@end


