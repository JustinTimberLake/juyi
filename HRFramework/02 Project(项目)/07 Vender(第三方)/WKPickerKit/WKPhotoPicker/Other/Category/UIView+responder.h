//
//  UIView+responder.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/12.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (responder)

- (UIViewController *)viewController;

@end
