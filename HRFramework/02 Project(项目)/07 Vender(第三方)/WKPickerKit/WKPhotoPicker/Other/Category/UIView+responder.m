//
//  UIView+responder.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/12.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "UIView+responder.h"

@implementation UIView (responder)

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

@end
