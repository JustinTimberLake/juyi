//
//  WKAlertManager.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/13.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WKHeader.h"

@interface WKAlertManager : NSObject

/**
 提示框

 @param message 提示信息
 */
+ (void)alertMsg:(NSString *)message vc:(UIViewController *)vc;

/**
 照片选择

 @param alertDataSourceType 资源类型
 @param albumHandler 相册调用
 @param cameraHandler 相机调用
 @param videoHandler 视频调用
 @param cancelHandler 取消
 */
+ (void)alertDataSourceType:(WKAlertDataSourceType)alertDataSourceType
               albumHandler:(void(^)())albumHandler
              cameraHandler:(void(^)())cameraHandler
               videoHandler:(void(^)())videoHandler
              cancelHandler:(void(^)())cancelHandler;

@end
