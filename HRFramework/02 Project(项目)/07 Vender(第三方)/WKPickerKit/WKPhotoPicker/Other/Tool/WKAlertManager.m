//
//  WKAlertManager.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/13.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKAlertManager.h"
#import "WKHeader.h"

@implementation WKAlertManager



#pragma mark - 提示框
+ (void)alertMsg:(NSString *)message vc:(UIViewController *)vc {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"确定"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {}];
    [alertController addAction:alertAction];
    
    [vc presentViewController:alertController animated:YES completion:nil];
}

- (void)selectedPhotoSource {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选取资源"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *camera_pic = [UIAlertAction actionWithTitle:@"拍照"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *camera_video = [UIAlertAction actionWithTitle:@"视频"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *photo = [UIAlertAction actionWithTitle:@"从相册中选取"
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * _Nonnull action) {
        

    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    
    [alertController addAction:camera_pic];
    [alertController addAction:camera_video];
    [alertController addAction:photo];
    [alertController addAction:cancel];
    [WKRootVC presentViewController:alertController animated:YES completion:nil];
}

+ (void)alertDataSourceType:(WKAlertDataSourceType)alertDataSourceType
               albumHandler:(void(^)())albumHandler
              cameraHandler:(void(^)())cameraHandler
               videoHandler:(void(^)())videoHandler
              cancelHandler:(void(^)())cancelHandler  {
    
    NSMutableArray <UIAlertAction *> *actions = [NSMutableArray array];
    switch (alertDataSourceType) {
        case WKAlertDataSourceTypeForAlbum:
        {
            UIAlertAction *action = [[self class] alertWithTitle:@"相册" Action:^{
                albumHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                                style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:action];
            [actions addObject:cancel];
            break;
        }
        case WKAlertDataSourceTypeForCamera:
        {
            UIAlertAction *action = [[self class] alertWithTitle:@"拍照" Action:^{
                cameraHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:action];
            [actions addObject:cancel];
            break;
        }
        case WKAlertDataSourceTypeForVideo:
        {
            UIAlertAction *action = [[self class] alertWithTitle:@"视频" Action:^{
                videoHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:action];
            [actions addObject:cancel];
            break;
        }
        case WKAlertDataSourceTypeForAlbumAndCamera:
        {
            UIAlertAction *album_action = [[self class] alertWithTitle:@"相册" Action:^{
                albumHandler();
            }];
            UIAlertAction *camera_action = [[self class] alertWithTitle:@"拍照" Action:^{
                cameraHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:album_action];
            [actions addObject:camera_action];
            [actions addObject:cancel];
            break;
        }
        case WKAlertDataSourceTypeForAlbumAndVideo:
        {
            UIAlertAction *album_action = [[self class] alertWithTitle:@"相册" Action:^{
                albumHandler();
            }];
            UIAlertAction *video_action = [[self class] alertWithTitle:@"视频" Action:^{
                videoHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:album_action];
            [actions addObject:video_action];
            [actions addObject:cancel];
            break;
        }
        case WKAlertDataSourceTypeForCameraAndVideo:
        {
            UIAlertAction *video_action = [[self class] alertWithTitle:@"视频" Action:^{
                videoHandler();
            }];
            UIAlertAction *camera_action = [[self class] alertWithTitle:@"拍照" Action:^{
                cameraHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:video_action];
            [actions addObject:camera_action];
            [actions addObject:cancel];
            break;
        }
            
        default:
        {
            UIAlertAction *album_action = [[self class] alertWithTitle:@"相册" Action:^{
                albumHandler();
            }];
            UIAlertAction *camera_action = [[self class] alertWithTitle:@"拍照" Action:^{
                cameraHandler();
            }];
            UIAlertAction *video_action = [[self class] alertWithTitle:@"视频" Action:^{
                videoHandler();
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                             style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               cancelHandler();
                                                           }];
            [actions addObject:album_action];
            [actions addObject:camera_action];
            [actions addObject:video_action];
            [actions addObject:cancel];
            break;
        }
    }
    [[self class] alertSetActions:actions];
}

+ (UIAlertAction *)alertWithTitle:(NSString *)title Action:(void(^)())wAction {
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        wAction();
    }];
    return action;
}



+ (void)alertSetActions:(NSArray <UIAlertAction *>*)actions {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"选取资源" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    for (UIAlertAction *action in actions) {
        [alertController addAction:action];
    }
    [WKRootVC presentViewController:alertController animated:YES completion:nil];
}




@end
