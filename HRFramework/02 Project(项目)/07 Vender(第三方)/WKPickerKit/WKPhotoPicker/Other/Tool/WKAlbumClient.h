//
//  WKAlbumClient.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/11.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WKHeader.h"

@interface WKAlbumClient : NSObject

/** Description:SelectedAssetArr */
@property (strong, nonatomic) NSMutableArray *selectedAssetArr;
/** Description:缩略图 */
@property (strong, nonatomic) NSMutableArray *selectedAssetThumArr;

/** Description:限制展示个数 */
@property (assign, nonatomic) NSInteger showCount;

+ (instancetype)sharedInstance;


/**
 展示缩略图

 @param asset 图片资源
 @param size size
 @param resizeMode size属性
 @param completion 完成
 */
- (void)requestImageForAsset:(PHAsset *)asset
                        size:(CGSize)size
                  resizeMode:(PHImageRequestOptionsResizeMode)resizeMode
                  completion:(void (^)(UIImage *image))completion;


/**
 展示原图

 @param asset 图片资源
 @param options options
 @param completion 完成
 */
- (void)requestImageForAsset:(PHAsset *)asset
                     options:(PHImageRequestOptions *)options
                  completion:(void (^)(UIImage *image))completion;

@end
