//
//  WKAlbums.m
//  PhotoDemo
//
//  Created by 王凯 on 2016/12/31.
//  Copyright © 2016年 王凯. All rights reserved.
//

#import "WKAlbum.h"
#import "WKHeader.h"
@implementation WKAlbum

#pragma mark - 获取所有相册列表
- (NSArray<WKAlbum *> *)getPhotoAblumList
{
    NSMutableArray<WKAlbum *> *photoAblumList = [NSMutableArray array];

    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    [smartAlbums enumerateObjectsUsingBlock:^(PHAssetCollection * _Nonnull collection, NSUInteger idx, BOOL *stop) {

        if (!([collection.localizedTitle isEqualToString:@"Recently Deleted"] ||
              [collection.localizedTitle isEqualToString:@"Videos"])) {
            NSArray<PHAsset *> *assets = [self getAssetsInAssetCollection:collection ascending:NO];
            if (assets.count > 0) {
                WKAlbum *ablum = [[WKAlbum alloc] init];
                ablum.title = [self transformAblumTitle:collection.localizedTitle];
                ablum.count = assets.count;
                ablum.headImageAsset = assets.firstObject;
                ablum.assetCollection = collection;
                [photoAblumList addObject:ablum];
            }
        }
    }];
    
    PHFetchResult *userAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection * _Nonnull collection, NSUInteger idx, BOOL * _Nonnull stop) {
        NSArray<PHAsset *> *assets = [self getAssetsInAssetCollection:collection ascending:NO];
        if (assets.count > 0) {
            WKAlbum *ablum = [[WKAlbum alloc] init];
            ablum.title = collection.localizedTitle;
            ablum.count = assets.count;
            ablum.headImageAsset = assets.firstObject;
            ablum.assetCollection = collection;
            [photoAblumList addObject:ablum];
        }
    }];
    
    NSMutableArray *arr = [NSMutableArray array];
    for (WKAlbum *album in photoAblumList) {
        if([album.title isEqualToString:@"我的照片流"]) {
            [arr addObject:album];
        }
    }
    for (WKAlbum *album in photoAblumList) {
        if([album.title isEqualToString:@"Camera Roll"] || [album.title isEqualToString:@"相机胶卷"]) {
            [arr addObject:album];
        }
    }
    for (int i = 0; i < photoAblumList.count; i++) {
        WKAlbum *album = photoAblumList[i];
        if([album.title isEqualToString:@"我的照片流"] || [album.title isEqualToString:@"Camera Roll"] || [album.title isEqualToString:@"相机胶卷"]) {
            [photoAblumList removeObject:album];
        }
    }
//    for (WKAlbum *album in photoAblumList) {
//        
//    }
    
    
    
//    NSSet *set = [NSSet setWithArray:[arr arrayByAddingObjectsFromArray:photoAblumList]];
//    
//    NSArray *sortArr = @[[[NSSortDescriptor alloc] initWithKey:nil ascending:YES]];
//    
//    NSArray *photoArr = [set sortedArrayUsingDescriptors:sortArr];
    
    
    return [arr arrayByAddingObjectsFromArray:photoAblumList];
}

- (NSString *)transformAblumTitle:(NSString *)title
{
    NSLog(@"相册标题：%@", title);
    
    if ([title isEqualToString:@"Slo-mo"] || [title isEqualToString:@"慢动作"]) {
        return @"慢动作";
    } else if ([title isEqualToString:@"Recently Added"] || [title isEqualToString:@"最近添加"]) {
        return @"最近添加";
    } else if ([title isEqualToString:@"Favorites"] || [title isEqualToString:@"最爱"]) {
        return @"最爱";
    } else if ([title isEqualToString:@"Recently Deleted"] || [title isEqualToString:@"最近删除"]) {
        return @"最近删除";
    } else if ([title isEqualToString:@"Videos"] || [title isEqualToString:@"视频"]) {
        return @"视频";
    } else if ([title isEqualToString:@"All Photos"] || [title isEqualToString:@"所有照片"]) {
        return @"所有照片";
    } else if ([title isEqualToString:@"Selfies"] || [title isEqualToString:@"自拍"]) {
        return @"自拍";
    } else if ([title isEqualToString:@"Screenshots"] || [title isEqualToString:@"屏幕快照"]) {
        return @"屏幕快照";
    } else if ([title isEqualToString:@"Camera Roll"] || [title isEqualToString:@"相机胶卷"]) {
        return @"相机胶卷";
    } else if ([title isEqualToString:@"Panoramas"] || [title isEqualToString:@"全景照片"]) {
        return @"全景照片";
    } else if ([title isEqualToString:@"Bursts"] || [title isEqualToString:@"连拍快照"]) {
        return @"连拍快照";
    }
    return nil;
}

- (PHFetchResult *)fetchAssetsInAssetCollection:(PHAssetCollection *)assetCollection ascending:(BOOL)ascending
{
    PHFetchOptions *option = [[PHFetchOptions alloc] init];
    option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:ascending]];
    PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:assetCollection options:option];
    return result;
}

#pragma mark - 获取相册内所有照片资源
- (NSArray<PHAsset *> *)getAllAssetInPhotoAblumWithAscending:(BOOL)ascending
{
    NSMutableArray<PHAsset *> *assets = [NSMutableArray array];
    
    PHFetchOptions *option = [[PHFetchOptions alloc] init];
    //ascending 为YES时，按照照片的创建时间升序排列;为NO时，则降序排列
    option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:ascending]];
    
    PHFetchResult *result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:option];
    
    [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PHAsset *asset = (PHAsset *)obj;
        [assets addObject:asset];
    }];
    
    return assets;
}

#pragma mark - 获取指定相册内的所有图片
- (NSArray<PHAsset *> *)getAssetsInAssetCollection:(PHAssetCollection *)assetCollection ascending:(BOOL)ascending
{
    NSMutableArray<PHAsset *> *arr = [NSMutableArray array];
    
    PHFetchResult *result = [self fetchAssetsInAssetCollection:assetCollection ascending:ascending];
    [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (((PHAsset *)obj).mediaType == PHAssetMediaTypeImage) {
            [arr addObject:obj];
        }
    }];
    return arr;
}

#pragma mark - 获取asset对应的图片 - 暂不使用
- (void)requestImageForAsset:(PHAsset *)asset size:(CGSize)size resizeMode:(PHImageRequestOptionsResizeMode)resizeMode completion:(void (^)(UIImage *))completion
{
    PHImageRequestOptions *option = [[PHImageRequestOptions alloc] init];
    /**
     resizeMode：对请求的图像怎样缩放。有三种选择：None，默认加载方式；Fast，尽快地提供接近或稍微大于要求的尺寸；Exact，精准提供要求的尺寸。
     deliveryMode：图像质量。有三种值：Opportunistic，在速度与质量中均衡；HighQualityFormat，不管花费多长时间，提供高质量图像；FastFormat，以最快速度提供好的质量。
     这个属性只有在 synchronous 为 true 时有效。
     */
    option.resizeMode = PHImageRequestOptionsResizeModeExact;
    option.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    option.synchronous = YES;
    option.networkAccessAllowed = YES;
    //param：targetSize 即你想要的图片尺寸，若想要原尺寸则可输入PHImageManagerMaximumSize
    [[PHCachingImageManager defaultManager] requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeAspectFill options:option resultHandler:^(UIImage * _Nullable image, NSDictionary * _Nullable info) {
        completion(image);
    }];
}

@end
