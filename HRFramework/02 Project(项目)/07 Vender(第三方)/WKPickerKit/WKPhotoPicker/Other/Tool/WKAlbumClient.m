//
//  WKAlbumClient.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/11.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKAlbumClient.h"
#import "WKHeader.h"

@implementation WKAlbumClient

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id __singleton__;
    dispatch_once( &once, ^{ __singleton__ = [[self alloc] init]; } );
    return __singleton__;
}

- (NSMutableArray *)selectedAssetArr {
    if(!_selectedAssetArr) {
        _selectedAssetArr = [NSMutableArray array];
    }
    return _selectedAssetArr;
}


#pragma mark - 获取asset对应的图片 - 缩略
/**
 展示缩略图
 
 @param asset 图片资源
 @param size size
 @param resizeMode size属性
 @param completion 完成
 */
- (void)requestImageForAsset:(PHAsset *)asset size:(CGSize)size resizeMode:(PHImageRequestOptionsResizeMode)resizeMode completion:(void (^)(UIImage *image))completion
{ 
    PHImageRequestOptions *option = [[PHImageRequestOptions alloc] init];
    /**
     resizeMode：对请求的图像怎样缩放。有三种选择：
                                    None，默认加载方式；
                                    Fast，尽快地提供接近或稍微大于要求的尺寸；
                                    Exact，精准提供要求的尺寸。
     deliveryMode：图像质量。有三种值：
                            Opportunistic，在速度与质量中均衡；
                            HighQualityFormat，不管花费多长时间，提供高质量图像；
                            FastFormat，以最快速度提供好的质量。
     这个属性只有在 synchronous 为 true 时有效。
     */
    option.resizeMode = resizeMode;
    option.deliveryMode = PHImageRequestOptionsDeliveryModeOpportunistic;
    option.synchronous = YES;
    option.networkAccessAllowed = YES;
    //param：targetSize 即你想要的图片尺寸，若想要原尺寸则可输入PHImageManagerMaximumSize
    [[PHCachingImageManager defaultManager] requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeAspectFill options:option resultHandler:^(UIImage * _Nullable image, NSDictionary * _Nullable info) {
        completion(image);
    }];
}

- (void)requestImageForAsset:(PHAsset *)asset options:(PHImageRequestOptions *)options completion:(void (^)(UIImage *image))completion {
    [[PHCachingImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        UIImage *image = [UIImage imageWithData:imageData];
        completion(image);
    }];
}

@end
