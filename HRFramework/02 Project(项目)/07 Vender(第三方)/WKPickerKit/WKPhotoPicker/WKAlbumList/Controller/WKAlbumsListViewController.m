//
//  WKAlbumsListViewController.m
//  PhotoDemo
//
//  Created by 王凯 on 2016/12/31.
//  Copyright © 2016年 王凯. All rights reserved.
//

#import "WKAlbumsListViewController.h"
#import "WKHeader.h"

static NSString *identifier = @"WKAlbumsListCell";

@interface WKAlbumsListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) WKAlbum *albums;

@property (weak, nonatomic) IBOutlet UITableView *albunListTableView;

@property (nonatomic, strong) NSMutableArray *dataSource;


@end

@implementation WKAlbumsListViewController

- (NSMutableArray *)dataSource {
    if(!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (WKAlbum *)albums {
    if(!_albums) {
        _albums = [[WKAlbum alloc] init];
    }
    return _albums;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"照片";
    // 注册cell
    [self registerCell];
    // 抓取相册权限
    [self authorizationStatus];
    // 配置取消按钮
    [self configRightItem];
    
    WKWeakSelf;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.dataSource addObjectsFromArray: [[self.albums getPhotoAblumList] mutableCopy]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.albunListTableView reloadData];
        });
    });
}
- (void)configRightItem {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction)];
}
- (void)cancelAction {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)registerCell {
    [self.albunListTableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WKAlbumsListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell) {
        cell = [[WKAlbumsListCell alloc] init];
    }
    cell.album = self.dataSource[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self pushToPicker:indexPath.row animated:YES];
}

- (void)pushToPicker:(NSInteger)idx animated:(BOOL)animated {
    WKPickerViewController *picker = [[WKPickerViewController alloc] init];
    WKAlbum *album = self.dataSource[idx];
    picker.subject = album.title;
    picker.assetDataSourceArr = [self.albums getAssetsInAssetCollection:album.assetCollection ascending:YES];
    [self.navigationController pushViewController:picker animated:animated];
}

- (void)authorizationStatus {
    WKWeakSelf
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.albunListTableView reloadData];
                });
            }
        }];
    }
}


@end
