//
//  WKAlbumsListCell.h
//  PhotoDemo
//
//  Created by 王凯 on 2016/12/31.
//  Copyright © 2016年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WKAlbum;
@interface WKAlbumsListCell : UITableViewCell
/** Description:相册资源 */
@property (copy, nonatomic) WKAlbum *album;
/** Description:当前相册的第一张图片 */
@property (weak, nonatomic) IBOutlet UIImageView *firstImage;
/** Description:相册名称 */
@property (weak, nonatomic) IBOutlet UILabel *albumTitle;
/** Description:当前相册的图片数量 */
@property (weak, nonatomic) IBOutlet UILabel *albumCount;

@end
