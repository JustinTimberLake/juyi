//
//  WKAlbumsListCell.m
//  PhotoDemo
//
//  Created by 王凯 on 2016/12/31.
//  Copyright © 2016年 王凯. All rights reserved.
//

#import "WKAlbumsListCell.h"
#import "WKHeader.h"

@implementation WKAlbumsListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setAlbum:(WKAlbum *)album {
    _album = album;
    __weak typeof(self)weakSelf = self;
    [self imagePackedWithAsset:_album.headImageAsset hanlder:^(UIImage *image) {
        [weakSelf.firstImage setImage:image];
    }];
    [_albumTitle setText:_album.title];
    [_albumCount setText:[NSString stringWithFormat:@"(%ld)", _album.count]];
}

- (void)imagePackedWithAsset:(PHAsset *)asset hanlder:(void(^)(UIImage *image))handler {
    //资源转图片
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = CGSizeMake(48.5, 48.5);
    CGSize imageSize = CGSizeMake(cellSize.width*scale, cellSize.height*scale);
    
    [self.album requestImageForAsset:asset size:imageSize resizeMode:PHImageRequestOptionsResizeModeFast completion:^(UIImage *image) {
        handler(image);
    }];

}

@end
