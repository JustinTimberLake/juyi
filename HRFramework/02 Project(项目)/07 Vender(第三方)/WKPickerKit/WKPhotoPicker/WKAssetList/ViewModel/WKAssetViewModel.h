//
//  WKAssetViewModel.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/11.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WKHeader.h"

@interface WKAssetViewModel : NSObject

@property (strong, nonatomic) NSMutableArray <WKAssetModel *>*assetDataSourceArr;

/** Description:数据源个数 */
@property (assign, nonatomic, readonly) NSInteger assetsCount;

/**
 封装PHAsset到数据模型

 @param assets PHAsset
 @param comple 完成
 */
- (void)wk_encapsulationWithAssets:(NSArray *)assets comple:(WKAssetEncapsulation)comple;

/**
 获取所有资源中的图片（选中）

 @param arr 图片组
 @param comple 完成
 */
- (void)wk_conversionImageForAssets:(NSArray *)arr comple:(WKAssetSelectedAllImage)comple;

/**
 根据下标获取asset

 @param idx 下标
 @return 当前数据模型
 */
- (WKAssetModel *)wk_assetWithIndex:(NSInteger)idx;

@end
