//
//  WKAssetViewModel.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/11.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKAssetViewModel.h"
#import "WKHeader.h"

@interface WKAssetViewModel()



@end

@implementation WKAssetViewModel


@synthesize assetsCount = _assetsCount;

- (NSMutableArray *)assetDataSourceArr {
    if(!_assetDataSourceArr) {
        _assetDataSourceArr = [NSMutableArray array];
    }
    return _assetDataSourceArr;
}

/**
 封装PHAsset到数据模型
 
 @param assets PHAsset
 @param comple 完成
 */
- (void)wk_encapsulationWithAssets:(NSArray *)assets comple:(WKAssetEncapsulation)comple {
    WKWeakSelf
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [assets enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            WKAssetModel *asset = [[WKAssetModel alloc] init];
            asset.asset = obj;
            asset.isSelector = NO;
            [weakSelf.assetDataSourceArr addObject:asset];
        }];
        for (WKAssetModel *selectedAsset in WKClient.selectedAssetArr) {
            for (WKAssetModel *currentAsset in weakSelf.assetDataSourceArr) {
                if([selectedAsset.asset.localIdentifier isEqualToString:currentAsset.asset.localIdentifier]) {
                    currentAsset.isSelector = YES;
                }
            }
        }
        _assetsCount = self.assetDataSourceArr.count;
        dispatch_async(dispatch_get_main_queue(), ^{
            comple();
        });
    });
}


/**
 获取所有资源中的图片（选中）
 
 @param arr 图片组
 @param comple 完成
 */
- (void)wk_conversionImageForAssets:(NSArray *)arr comple:(WKAssetSelectedAllImage)comple {
    NSMutableArray *dataSource = [NSMutableArray array];
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[PHCachingImageManager defaultManager] requestImageDataForAsset:((WKAssetModel *)obj).asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            NSData *data = UIImageJPEGRepresentation([UIImage imageWithData:imageData], 0.2);
            [dataSource addObject:data];
            if(dataSource.count == arr.count) {
                comple(dataSource);
            }
        }];
    }];
    
    
}


/**
 根据下标获取asset
 
 @param idx 下标
 @return 当前数据模型
 */
- (WKAssetModel *)wk_assetWithIndex:(NSInteger)idx {
    return self.assetDataSourceArr[idx];
}




@end
