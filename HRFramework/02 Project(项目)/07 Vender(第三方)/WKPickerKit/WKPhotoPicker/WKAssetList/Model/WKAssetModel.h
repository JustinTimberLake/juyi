//
//  WKAssetModel.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/11.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

@interface WKAssetModel : NSObject

/** Description:Asset */
@property (strong, nonatomic) PHAsset *asset;

/** Description:是否被选中 */
@property (assign, nonatomic) BOOL isSelector;

/** Description:<#description#> */
@property (strong, nonatomic) UIImage *big;

/** Description:是否是视频 */
@property (assign, nonatomic) BOOL isVideo;

@end
