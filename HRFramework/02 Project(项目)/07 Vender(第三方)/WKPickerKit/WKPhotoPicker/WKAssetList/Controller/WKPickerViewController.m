//
//  WKPickerViewController.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKPickerViewController.h"
#import "WKHeader.h"

@interface WKPickerViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (strong, nonatomic) WKCollectionViewFlowLayout *layout;
@property (strong, nonatomic) WKAssetViewModel *assetViewModel;
@property (strong, nonatomic) WKAlbum *album;


@end

@implementation WKPickerViewController

- (IBAction)sendAction:(id)sender {
    
    WKNavigationController *nav = (WKNavigationController *)self.navigationController;
    
    [self.assetViewModel wk_conversionImageForAssets:WKClient.selectedAssetArr comple:^(NSArray *images) {
        if([nav.wdelegate respondsToSelector:@selector(pickerSelectedImages:)]) {
            [nav.wdelegate pickerSelectedImages:images];
        }
        if([nav.wdelegate respondsToSelector:@selector(pickerSelectedAssets:)]) {
            [nav.wdelegate pickerSelectedAssets:WKClient.selectedAssetArr];
            
        }
        [self cancelAction];
//        [WKClient.selectedAssetArr removeAllObjects];
    }];
    
}

- (WKAlbum *)album {
    if(!_album) {
        _album = [[WKAlbum alloc] init];
    }
    return _album;
}

- (WKCollectionViewFlowLayout *)layout {
    if(!_layout) {
        _layout = [[WKCollectionViewFlowLayout alloc] init];
    }
    return _layout;
}

- (WKAssetViewModel *)assetViewModel {
    if(!_assetViewModel) {
        _assetViewModel = [[WKAssetViewModel alloc] init];
    }
    return _assetViewModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configCollectionAndLayout];
    [self authorizationStatus];
    [self configRightItem];
    [self configTitle];
    
    if(WKClient.selectedAssetArr.count > 0) {
        [self.sendBtn setBackgroundColor:[UIColor blueColor]];
        [self.sendBtn setTitle:[NSString stringWithFormat:@"发送(%ld)", WKClient.selectedAssetArr.count] forState:UIControlStateNormal];
        self.sendBtn.enabled = YES;
    }
}

- (void)configTitle {
    if(self.subject.length) {
        self.title = self.subject;
    }else{
        self.title = @"我的照片流";
    }
}

- (void)configAssetData {
    WKWeakSelf
    
    if(self.assetDataSourceArr.count) {
        [self.assetViewModel wk_encapsulationWithAssets:self.assetDataSourceArr comple:^{
            [weakSelf.collectionView reloadData];
        }];
    }else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            WKAlbum *album = [self.album getPhotoAblumList][0];
            self.assetDataSourceArr = [NSMutableArray arrayWithArray:[self.album getAssetsInAssetCollection:album.assetCollection ascending:YES]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.assetViewModel wk_encapsulationWithAssets:self.assetDataSourceArr comple:^{
                    [weakSelf.collectionView reloadData];
                }];
            });
        });
    }
}

- (void)configCollectionAndLayout {
    [self.collectionView setCollectionViewLayout:self.layout];
    [self.collectionView registerNib:[UINib nibWithNibName:WKPickerThumCellIdentifier bundle:nil] forCellWithReuseIdentifier:WKPickerThumCellIdentifier];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.assetViewModel.assetsCount;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WKWeakSelf
    WKPickerThumCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WKPickerThumCellIdentifier forIndexPath:indexPath];
    if(!cell) {
        cell = [[WKPickerThumCell alloc] init];
    }
    cell.assetModel = [self.assetViewModel wk_assetWithIndex:indexPath.row];;
    cell.indexPath = indexPath;
    [cell setAssetSelected:^{
//        if(WKClient.selectedAssetArr.count == WKClient.showCount) {
//            [WKAlertManager alertMsg:[NSString stringWithFormat:@"最多展示：%ld张", WKClient.showCount] vc:self];
//        }else{
            [weakSelf changeSendBtnForAdd];
//        }
    }];
    [cell setXxxblock:^(UIImage *big) {
        self.assetViewModel.assetDataSourceArr[indexPath.row].big = big;
    }];
    [cell setAssetUnSelected:^{
        [weakSelf changeSendForUn];
    }];
    return cell;
}
#pragma mark - 改变发送按钮 - 添加
- (void)changeSendBtnForAdd {
    if(WKClient.selectedAssetArr.count > 0) {
        [self.sendBtn setBackgroundColor:[UIColor blueColor]];
        [self.sendBtn setTitle:[NSString stringWithFormat:@"确定(%ld)", WKClient.selectedAssetArr.count] forState:UIControlStateNormal];
        self.sendBtn.enabled = YES;
    }
}
#pragma mark - 改变发送按钮 - 减少
- (void)changeSendForUn {
    if(WKClient.selectedAssetArr.count == 0) {
        [self.sendBtn setBackgroundColor:[UIColor lightGrayColor]];
        [self.sendBtn setTitle:@"确定" forState:UIControlStateNormal];
        self.sendBtn.enabled = NO;
    }else{
        if(WKClient.selectedAssetArr.count > 0) {
            [self.sendBtn setBackgroundColor:[UIColor blueColor]];
            [self.sendBtn setTitle:[NSString stringWithFormat:@"确定(%ld)", WKClient.selectedAssetArr.count] forState:UIControlStateNormal];
            self.sendBtn.enabled = YES;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    WKOriginalAssetListController *original = [NSClassFromString(@"WKOriginalAssetListController") new];
    original.dataSource = self.assetDataSourceArr;
    original.idx = indexPath.item;
    [self.navigationController pushViewController:original animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)configRightItem {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction)];
}
- (void)cancelAction {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)authorizationStatus {
    
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                [self configAssetData];
            }
        }];
    }else{
        [self configAssetData];
    }
}

@end


@interface WKCollectionViewFlowLayout ()

@end

@implementation WKCollectionViewFlowLayout

- (instancetype)init {
    self = [super init];
    if(self) {
        self.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        self.itemSize = itemSize(4);
        self.minimumLineSpacing = 5.0f;
        self.minimumInteritemSpacing = 5.00f;
    }
    return self;
}



@end

