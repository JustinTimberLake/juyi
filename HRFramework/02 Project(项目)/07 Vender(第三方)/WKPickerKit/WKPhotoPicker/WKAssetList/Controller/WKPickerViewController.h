//
//  WKPickerViewController.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WKHeader.h"


@interface WKPickerViewController : UIViewController

/** Description:Asset数据源 */
@property (strong, nonatomic) NSArray *assetDataSourceArr;

/** Description:title */
@property (copy, nonatomic) NSString *subject;





@end

@interface WKCollectionViewFlowLayout : UICollectionViewFlowLayout

@end

