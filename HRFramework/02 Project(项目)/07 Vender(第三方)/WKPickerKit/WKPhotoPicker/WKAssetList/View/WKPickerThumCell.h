//
//  WKPickerThumCell.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WKHeader.h"



@class WKAssetModel;
@interface WKPickerThumCell : UICollectionViewCell

/** Description:Asset数据源 */
@property (strong, nonatomic) WKAssetModel *assetModel;

/** Description:NSIndexPath */
@property (strong, nonatomic) NSIndexPath *indexPath;

/** Description:selectedBlock */
@property (copy, nonatomic) WKAssetSelected assetSelected;
@property (copy, nonatomic) WKAssetUnSelected assetUnSelected;

@property (weak, nonatomic) IBOutlet UIImageView *picImg;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;

/** Description:<#description#> */
@property (copy, nonatomic) void(^xxxblock)(UIImage *BIGmg);


@end
