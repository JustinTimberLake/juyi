//
//  WKPickerThumCell.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/5.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKPickerThumCell.h"
#import "WKHeader.h"

@interface WKPickerThumCell ()



@end

@implementation WKPickerThumCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAssetModel:(WKAssetModel *)assetModel {
    _assetModel = assetModel;
    [self configData:_assetModel];
}

- (void)configData:(WKAssetModel *)asset {
    WKWeakSelf
    if (asset.big) {
        weakSelf.picImg.image = asset.big;
    } else {
        [self imagePackedWithAsset:asset.asset hanlder:^(UIImage *image) {
            [weakSelf.picImg setImage:image];
        }];
    }
    
    if(asset.isSelector) {
        [self.selectedBtn setSelected:YES];
        [self.maskView setHidden:NO];
    }else{
        [self.selectedBtn setSelected:NO];
        [self.maskView setHidden:YES];
    }
    
    
}

#pragma mark - 资源转图片
- (void)imagePackedWithAsset:(PHAsset *)asset hanlder:(void(^)(UIImage *image))handler {
    
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = itemSize(4);
    CGSize imageSize = CGSizeMake(cellSize.width*scale, cellSize.height*scale);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WKClient requestImageForAsset:asset size:imageSize resizeMode:PHImageRequestOptionsResizeModeExact completion:^(UIImage *image) {
            if (self.xxxblock) {
                self.xxxblock(image);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(image);
                
            });
        }];
    });
}

#pragma mark - SelectedBtnAction
- (IBAction)selectedAction:(id)sender {
    
    if(!self.selectedBtn.selected) {
        if(WKClient.selectedAssetArr.count == WKClient.showCount) {
            [WKAlertManager alertMsg:[NSString stringWithFormat:@"最多展示：%ld张", WKClient.showCount] vc:self.viewController];
            return;
        }
        [self.maskView setHidden:NO];
        
        [WKClient.selectedAssetArr addObject:_assetModel];
        if(self.assetSelected) {
            self.assetSelected();
        }
    }else{
        
        [self.maskView setHidden:YES];
        
        if([self isContains:_assetModel]) {
            [self deleteCurrentAsset:_assetModel];
        }else{
            [WKClient.selectedAssetArr removeObject:_assetModel];
        }
        if(self.assetUnSelected) {
            self.assetUnSelected();
        }
    }
    self.selectedBtn.selected = !self.selectedBtn.selected;
}

#pragma mark - 遍历当前图片是否存在
- (BOOL)isContains:(WKAssetModel *)asset {
    for (WKAssetModel *model in WKClient.selectedAssetArr) {
        if([model.asset.localIdentifier isEqualToString:asset.asset.localIdentifier]) {
            return YES;
        }
    }
    return NO;
}
#pragma mark - 删除当前图片
- (void)deleteCurrentAsset:(WKAssetModel *)asset {
    for (WKAssetModel *model in WKClient.selectedAssetArr) {
        if([model.asset.localIdentifier isEqualToString:asset.asset.localIdentifier]) {
            [WKClient.selectedAssetArr removeObject:model];
            break;
        }
    }
}


@end
