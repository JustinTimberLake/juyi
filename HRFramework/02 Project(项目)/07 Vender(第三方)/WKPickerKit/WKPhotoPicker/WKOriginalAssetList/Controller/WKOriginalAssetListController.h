//
//  WKOriginalAssetListController.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/13.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WKOriginalAssetListController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

/** Description:数据源 */
@property (copy, nonatomic) NSArray *dataSource;

/** Description:当前坐标 */
@property (assign, nonatomic) NSInteger idx;

@end

@interface WKOriginalAssetListFlowLayout : UICollectionViewFlowLayout

@end
