//
//  WKOriginalAssetListController.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/13.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKOriginalAssetListController.h"
#import "WKHeader.h"

@interface WKOriginalAssetListController ()

@property (strong, nonatomic) WKOriginalAssetListFlowLayout *layout;

@property (weak, nonatomic) IBOutlet UICollectionView *originalCollection;


@end

@implementation WKOriginalAssetListController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self registerCollection];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.originalCollection setContentOffset:CGPointMake(self.idx * SCREEN_WIDTH, self.originalCollection.contentOffset.y) animated:NO];
        [self.originalCollection reloadData];
    });
    
    
}

- (void)registerCollection {
    [self.originalCollection setCollectionViewLayout:self.layout];
    [self.originalCollection registerNib:[UINib nibWithNibName:WKOriginalAssetListCellIdentifier bundle:nil] forCellWithReuseIdentifier:WKOriginalAssetListCellIdentifier];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataSource.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WKOriginalAssetListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WKOriginalAssetListCellIdentifier forIndexPath:indexPath];
    if(!cell) {
        cell = [[WKOriginalAssetListCell alloc] init];
    }
    cell.asset = self.dataSource[indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (WKOriginalAssetListFlowLayout *)layout {
    if(!_layout) {
        _layout = [[WKOriginalAssetListFlowLayout alloc] init];
    }
    return _layout;
}


@end

@interface WKOriginalAssetListFlowLayout ()

@end

@implementation WKOriginalAssetListFlowLayout

- (instancetype)init {
    self = [super init];
    if(self) {
        self.sectionInset = UIEdgeInsetsMake(-64, 0, 0, 0);
        self.itemSize = originalAssetListitemSize();
        self.minimumLineSpacing = 0.0f;
        self.minimumInteritemSpacing = 0.00f;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return self;
}






@end
