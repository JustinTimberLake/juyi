//
//  WKOriginalAssetListCell.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/13.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKOriginalAssetListCell.h"
#import "WKAlbum.h"


@implementation WKOriginalAssetListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAsset:(PHAsset *)asset {
    WKWeakSelf
    
//    [WKClient requestImageForAsset:asset size:PHImageManagerMaximumSize resizeMode:PHImageRequestOptionsResizeModeExact completion:^(UIImage *image) {
//        
//    }];
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = originalAssetListitemSize();
    CGSize imageSize = CGSizeMake(cellSize.width*scale, cellSize.height*scale);

    [WKClient requestImageForAsset:asset size:imageSize resizeMode:PHImageRequestOptionsResizeModeFast completion:^(UIImage *image) {
        [weakSelf.originalImg setImage:image];
    }];
}

@end
