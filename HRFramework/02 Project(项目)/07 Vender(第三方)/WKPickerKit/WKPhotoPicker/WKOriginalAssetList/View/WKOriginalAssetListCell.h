//
//  WKOriginalAssetListCell.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/13.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WKHeader.h"


@interface WKOriginalAssetListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *originalImg;
/** Description:PHAsset */
@property (strong, nonatomic) PHAsset *asset;



@end
