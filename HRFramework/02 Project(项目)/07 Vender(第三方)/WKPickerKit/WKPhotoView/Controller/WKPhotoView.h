//
//  WKPhotoView.h
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/12.
//  Copyright © 2017年 王凯. All rights reserved.
//

/** 注* 要在当前控制器的viewDidLoad方法里添加 此方法 self.automaticallyAdjustsScrollViewInsets = NO; 防止collection偏移*/

#import <UIKit/UIKit.h>
#import "WKHeader.h"

@protocol WKPickerViewDelegate;

@interface WKPhotoView : UIView

/** Description:代理 */
@property (assign, nonatomic) id<WKPickerViewDelegate> delegate;

/** Description:资源类型 */
@property (assign, nonatomic) WKAlertDataSourceType alertDataSourceType;

/** Description:限定显示的个数 */
@property (assign, nonatomic) NSInteger showCount;


WKPhotoView *KphotoView();

- (WKPhotoView *(^)(id<WKPickerViewDelegate>))kDelegate;

- (WKPhotoView *(^)(WKAlertDataSourceType))kAlertDatSourceType;

- (WKPhotoView *(^)(NSInteger))kShowCount;

- (WKPhotoView *(^)(UIView *))kAddForView;

- (WKPhotoView *(^)(CGFloat x, CGFloat y, CGFloat w, CGFloat h))kFrame;


+ (void)presentSourceType:(WKAlertDataSourceType)sourceType delegate:(id<WKPickerDelegate>)delegate vc:(UIViewController *)vc;

@end

@protocol WKPickerViewDelegate <NSObject>

@required



/**
 WK_当前选择的所有文件
 
 @param pickerView pickerView
 @param datas      含有所有数据（NSData）的的数据集合，可以重复添加
 @param count      当前数据集合的数量
 */
- (void)pickerView:(WKPhotoView *)pickerView selectedEndForDatas:(NSArray *)datas count:(NSInteger)count;

/**
 WK_当前选择的当前文件
 
 @param pickerVuew pickerView
 @param data       当前文件的数据
 @param image      当前文件的截图（如果是视频文件，该截图为当前视频的前1s的帧图片）
 */
- (void)pickerVeiw:(WKPhotoView *)pickerVuew currentFile:(NSData *)data forImage:(UIImage *)image;

/**
 WK_删除所选文件
 
 @param pickerView pickerView
 @param data 当前文件的数据
 @param image 当前文件的图片
 */
- (void)pickerView:(WKPhotoView *)pickerView deleteCurrentData:(NSData *)data forImage:(UIImage *)image WKPhotoDeprecated("暂时弃用");

@end
