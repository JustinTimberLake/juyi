//
//  WKPhotoView.m
//  WKPhotoPickerDemo
//
//  Created by 王凯 on 2017/1/12.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import "WKPhotoView.h"
#import "WKHeader.h"

@interface WKPhotoView ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
WKPickerDelegate
>

@property (strong, nonatomic) UICollectionView *collectionView;

@property (strong, nonatomic) UICollectionViewFlowLayout *layout;

@property (strong, nonatomic) NSMutableArray *dataSourceArr;

@property (strong, nonatomic) NSMutableArray *dataSourceArrForData;

@end

@implementation WKPhotoView

- (NSMutableArray *)dataSourceArrForData {
    if(!_dataSourceArrForData) {
        _dataSourceArrForData = [NSMutableArray array];
    }
    return _dataSourceArrForData;
}

- (NSMutableArray *)dataSourceArr {
    if(!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}

- (UICollectionView *)collectionView {
    if(!_collectionView) {
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64) collectionViewLayout:self.layout];
        [_collectionView setDelegate:self];
        [_collectionView setDataSource:self];
        [_collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:WKCollectionViewCellIdentifier];
    }
    return _collectionView;
}

- (UICollectionViewFlowLayout *)layout {
    if(!_layout) {
        _layout = [[UICollectionViewFlowLayout alloc] init];
        _layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        _layout.itemSize = photoViewitemSize(4);
        _layout.minimumLineSpacing = 10.00f;
        _layout.minimumInteritemSpacing = 10.00f;
    }
    return _layout;
}

//返回分区个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
//返回每个分区的item个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataSourceArr.count;
}
//返回每个item
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WKWeakSelf
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WKCollectionViewCellIdentifier forIndexPath:indexPath];
    if(!cell) {
        cell = [[CollectionViewCell alloc] init];
    }
//    WKAssetModel *asset = self.dataSourceArr[indexPath.row];
//    if(indexPath.row == self.dataSourceArr.count-1) {
        [cell.imageViewForPhotoImg setImage:self.dataSourceArr[indexPath.row]];
//    }else{
//        CGFloat scale = [UIScreen mainScreen].scale;
//        CGSize cellSize = self.layout.itemSize;
//        CGSize imageSize = CGSizeMake(cellSize.width*scale, cellSize.height*scale);
//        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
//        
//        options.resizeMode = PHImageRequestOptionsResizeModeExact;
//        options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
//        options.synchronous = YES;
//        options.networkAccessAllowed = YES;
//        // 从asset中获得图片
//        [[PHImageManager defaultManager] requestImageForAsset:asset.asset targetSize:imageSize contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
//            [cell.imageViewForPhotoImg setImage:result];
//        }];
//    }
    if(weakSelf.dataSourceArr.count == 1) {
        [cell hiddenDeleteImgView:YES];
    }else{
        if(indexPath.row == weakSelf.dataSourceArr.count - 1) {
            [cell hiddenDeleteImgView:YES];
        }else{
            [cell hiddenDeleteImgView:NO];
        }
    }
    // 删除单个item
    [cell deleteImg:^{
        [weakSelf.dataSourceArr removeObjectAtIndex:indexPath.row];
        [weakSelf.dataSourceArrForData removeObjectAtIndex:indexPath.row];
        [weakSelf layoutViewForView];
        [weakSelf.collectionView reloadData];
        [weakSelf didSeletedEndDelegateForDataSource:weakSelf.dataSourceArrForData];
    }];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == self.dataSourceArr.count - 1) {
        if(_showCount) {
            if(self.dataSourceArr.count - 1 == _showCount) {
                [WKAlertManager alertMsg:[NSString stringWithFormat:@"最多展示：%ld张", _showCount] vc:WKRootVC];
            }else{
                [self selectedPhotoSourceType:_alertDataSourceType];
            }
        }else{
            
            [self selectedPhotoSourceType:_alertDataSourceType];
        }
        
        
    }
}

- (void)selectedPhotoSourceType:(WKAlertDataSourceType)sourceType {
    
    WKWeakSelf
    [WKAlertManager alertDataSourceType:sourceType albumHandler:^{
        WKNavigationController *navC = [[WKNavigationController alloc] initWithDelegate:self];
        [self.viewController presentViewController:navC animated:YES completion:nil];
    } cameraHandler:^{
        [weakSelf selectedCarmeraForPic];
    } videoHandler:^{
        [weakSelf selectedCarmeraForVideo];
    } cancelHandler:^{
        
    }];
}

+ (void)presentSourceType:(WKAlertDataSourceType)sourceType delegate:(id<WKPickerDelegate>)delegate vc:(UIViewController *)vc  {
    WKWeakSelf
    [WKAlertManager alertDataSourceType:sourceType albumHandler:^{
        WKNavigationController *navC = [[WKNavigationController alloc] initWithDelegate:delegate];
        [vc presentViewController:navC animated:YES completion:nil];
    } cameraHandler:^{
        [weakSelf selectedCarmeraForPic];
    } videoHandler:^{
        [weakSelf selectedCarmeraForVideo];
    } cancelHandler:^{
        
    }];
}



#pragma mark - 拍照
- (void)selectedCarmeraForPic {
    if([self isCamera]) {
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;//设置可编辑
        picker.delegate = self;
        [self.viewController presentViewController:picker animated:YES completion:nil];
    }else{
        [WKAlertManager alertMsg:@"相机未知错误" vc:WKRootVC];
    }
}
#pragma mark - 录制视频
- (void)selectedCarmeraForVideo {
    if([self isCamera]){
        if([self isKUTTypeMovie]) {
            AVCaptureDevice *device;
            AVCaptureConnection *connection;
            AVCaptureVideoStabilizationMode stabilizationMode = AVCaptureVideoStabilizationModeCinematic;
            if ([device.activeFormat isVideoStabilizationModeSupported:stabilizationMode]) {
                [connection setPreferredVideoStabilizationMode:stabilizationMode];
            }
            UIImagePickerController *picker = [UIImagePickerController new];
            NSArray * mediaTypes =[UIImagePickerController  availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.mediaTypes = mediaTypes;
            picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
            picker.videoQuality = UIImagePickerControllerQualityTypeMedium; //录像质量
            picker.videoMaximumDuration = 600.0f; //录像最长时间
            picker.delegate = self;
            [self.viewController presentViewController:picker animated:YES completion:nil];
        }else {
            [WKAlertManager alertMsg:@"不支持录制" vc:WKRootVC];
        }
    }else {
        [WKAlertManager alertMsg:@"相机未知错误" vc:WKRootVC];
    }
}
#pragma mark - 是否支持相机
- (BOOL)isCamera {
    if ([UIImagePickerController
         isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        return YES;
    }
    return NO;
}
#pragma mark - 是否支持视频录制
- (BOOL)isKUTTypeMovie {
    NSArray *availableMediaTypes = [UIImagePickerController
                                    availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    if ([availableMediaTypes containsObject:(NSString *)kUTTypeMovie]){
        return YES;
    }
    return NO;
}
#pragma mark - 重新布局
- (void)layoutViewForView {
    
    NSInteger count = self.dataSourceArr.count;
    if(count%4){
        [self setFrame:layoutFrameForLess4(0, self.frame.origin.y, (int)count)];
        
    }else{
        [self setFrame:layoutFrameForLess(0, self.frame.origin.y, (int)count)];
    }
    [self.collectionView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

- (void)setShowCount:(NSInteger)showCount {
    _showCount = showCount;
    WKClient.showCount = _showCount;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self= [super initWithFrame:frame];
    if(self) {
        [self.dataSourceArr addObject:[UIImage imageNamed:WKAddImgNamed]];
        [self.collectionView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:self.collectionView];
    }
    return self;
}

WKPhotoView *KphotoView() {
    return [WKPhotoView new];
}

- (WKPhotoView *(^)(id<WKPickerViewDelegate>))kDelegate {
    return ^(id<WKPickerViewDelegate> delegate) {
        [self setDelegate:delegate];
        return self;
    };
}

- (WKPhotoView *(^)(WKAlertDataSourceType))kAlertDatSourceType {
    return ^(WKAlertDataSourceType alertDataSourceType) {
        [self setAlertDataSourceType:alertDataSourceType];
        return self;
    };
}

- (WKPhotoView *(^)(NSInteger))kShowCount {
    return ^(NSInteger showCount) {
        [self setShowCount:showCount];
        return self;
    };
}

- (WKPhotoView *(^)(UIView *))kAddForView {
    return ^(UIView *addForView) {
        [addForView addSubview:self];
        return self;
    };
}

- (WKPhotoView *(^)(CGFloat x, CGFloat y, CGFloat w, CGFloat h))kFrame {
    return ^(CGFloat x, CGFloat y, CGFloat w, CGFloat h) {
        [self.collectionView setFrame:CGRectMake(0, 0, w, h)];
        [self setFrame:CGRectMake(x, y, w, h)];
        return self;
    };
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    __weak typeof(self)weakSelf = self;
    [self.viewController dismissViewControllerAnimated:YES completion:^{
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:@"public.image"]) {
            UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
            [weakSelf.dataSourceArrForData addObject:UIImageJPEGRepresentation((image), 0.005)];
            [weakSelf addObjectForImage:nil image:image];
            [weakSelf currentFile:UIImageJPEGRepresentation((image), 0.005) forImage:image];
            [weakSelf didSeletedEndDelegateForDataSource:weakSelf.dataSourceArrForData];
        }else{
            NSURL *videoUrl = [info objectForKey:UIImagePickerControllerMediaURL];
            UIImage *image = [WKTool thumbnailImageForVideo:videoUrl atTime:2.0];
            
            // 不做压缩处理
            NSData *videoData = [NSData dataWithContentsOfURL:videoUrl];
            // 总数据源
            [weakSelf.dataSourceArrForData addObject:videoData];
            // 单文件代理源
            [weakSelf currentFile:videoData forImage:image];
            // 总代理
            [weakSelf didSeletedEndDelegateForDataSource:weakSelf.dataSourceArrForData];
            // 视频压缩处理
            /*
             [WKTool compressedVideoOtherMethodWithURL:videoUrl compressionType:@"AVAssetExportPresetMediumQuality" compressionResultPath:^(NSString *path, CGFloat dataSize, NSData *data, CGFloat totaSize) {
             [weakSelf.dataSourceArrForData addObject:data];
             [weakSelf currentFile:data forImage:image];
             [weakSelf didSeletedEndDelegateForDataSource:weakSelf.dataSourceArrForData];
             }];
             */

            [self addObjectForImage:nil image:image];
        }
        
        
    }];
    
}

- (void)addObjectForImage:(NSArray *)images image:(UIImage *)image {
    [self.dataSourceArr removeObject:[self.dataSourceArr lastObject]];
    if(image) {
        [self.dataSourceArr addObject:image];
    }
    if(images.count) {
        [self.dataSourceArr addObjectsFromArray:images];
    }
    [self.dataSourceArr addObject:[UIImage imageNamed:@"add-img"]];
    [self layoutViewForView];
    [self.collectionView reloadData];
}

-(void)pickerSelectedImages:(NSArray *)images {
    NSLog(@"---图片集合%ld", images.count);
    
    
}
- (void)pickerSelectedAssets:(NSArray *)assets {
    NSLog(@"===Model集合%@", assets);
    WKWeakSelf
    [self.dataSourceArr removeAllObjects];
//    [self.dataSourceArr removeObject:[self.dataSourceArr lastObject]];
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize cellSize = self.layout.itemSize;
    CGSize imageSize = CGSizeMake(cellSize.width*scale, cellSize.height*scale);
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    
    options.resizeMode = PHImageRequestOptionsResizeModeExact;
    options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    options.synchronous = YES;
    options.networkAccessAllowed = YES;
    NSInteger dataCount = self.dataSourceArr.count;
    for (int i = 0; i < assets.count; i++) {
     
        WKAssetModel *asset = assets[i];
        
        // 从asset中获得图片
        [[PHImageManager defaultManager] requestImageForAsset:asset.asset targetSize:imageSize contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            
            [weakSelf.dataSourceArr addObject:result];
            if(weakSelf.dataSourceArr.count - dataCount == assets.count) {
                [weakSelf.dataSourceArr addObject:[UIImage imageNamed:WKAddImgNamed]];
                [weakSelf layoutViewForView];
                [weakSelf.collectionView reloadData];
            }
        }];
    }
    for (int i = 0; i < assets.count; i++) {
        WKAssetModel *asset = assets[i];
        
        [[PHImageManager defaultManager] requestImageDataForAsset:asset.asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            
            [weakSelf.dataSourceArrForData addObject:imageData];
            if(weakSelf.dataSourceArrForData.count == assets.count) {
                [self didSeletedEndDelegateForDataSource:weakSelf.dataSourceArrForData];
            }
        }];
    }
}

- (void)didSeletedEndDelegateForDataSource:(NSArray *)dataSource {
    if([self.delegate respondsToSelector:@selector(pickerView:selectedEndForDatas:count:)]) {
        [self.delegate pickerView:self selectedEndForDatas:dataSource count:dataSource.count];
    }
}
- (void)currentFile:(NSData *)data forImage:(UIImage *)image {
    if([self.delegate respondsToSelector:@selector(pickerVeiw:currentFile:forImage:)]) {
        [self.delegate pickerVeiw:self currentFile:data forImage:image];
    }
}

- (void)setAlertDataSourceType:(WKAlertDataSourceType)alertDataSourceType {
    _alertDataSourceType = alertDataSourceType;
}


@end
