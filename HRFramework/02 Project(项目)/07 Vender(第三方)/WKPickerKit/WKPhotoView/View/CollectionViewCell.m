//
//  CollectionViewCell.m
//  PhotoDemo
//
//  Created by 王凯 on 2016/12/10.
//  Copyright © 2016年 王凯. All rights reserved.
//


#import "CollectionViewCell.h"
#import "WKHeader.h"

@interface CollectionViewCell ()

@property (strong, nonatomic) UIImageView *selectedImageView;

@property (strong, nonatomic) UIButton *deleteBtn;

@end

@implementation CollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.imageViewForPhotoImg];

        [self.contentView addSubview:self.deleteBtn];

    }
    return self;
}

- (UIButton *)deleteBtn {
    if(!_deleteBtn) {
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn addTarget:self action:@selector(deleteCurrentImg:) forControlEvents:UIControlEventTouchUpInside];
        [_deleteBtn setImage:[UIImage imageNamed:@"关闭"] forState:UIControlStateNormal];
        [_deleteBtn setFrame:CGRectMake(photoViewitemSize(4).width-13, -7, 20, 20)];
    }
    return _deleteBtn;
}

- (UIImageView *)selectedImageView {
    if(!_selectedImageView) {
        CGFloat scal = (SCREEN_WIDTH-50)/4;
        _selectedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(scal-15, 0, 15, 15)];
    }
    return _selectedImageView;
}
- (UIImageView *)imageViewForPhotoImg {
    if(!_imageViewForPhotoImg) {
        _imageViewForPhotoImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, photoViewitemSize(4).width, photoViewitemSize(4).height)];
        [_imageViewForPhotoImg setUserInteractionEnabled:YES];
        [_imageViewForPhotoImg setContentMode:UIViewContentModeScaleAspectFill];
        [_imageViewForPhotoImg setClipsToBounds:YES];
    }
    return _imageViewForPhotoImg;
}

- (void)selectorImg:(UITapGestureRecognizer *)tap {
    
}
-(void)deleteImg:(DeleteImgBlock)deleteImg {
    self.deleteImg = deleteImg;
}
- (void)hiddenSelectedImgView {
    [self.selectedImageView setHidden:YES];
}
- (void)hiddenDeleteImgView:(BOOL)hidden {
    [self.deleteBtn setHidden:hidden];
}
- (void)deleteCurrentImg:(UIButton *)btn {
    if(self.deleteImg) {
        self.deleteImg();
    }
}

@end
