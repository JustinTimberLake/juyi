//
//  CollectionViewCell.h
//  PhotoDemo
//
//  Created by 王凯 on 2016/12/10.
//  Copyright © 2016年 王凯. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DeleteImgBlock) ();

@interface CollectionViewCell : UICollectionViewCell

/** Description:图片 */
@property (copy, nonatomic) UIImageView *imageViewForPhotoImg;

/** Description:是否选中 */
@property (assign, nonatomic) BOOL isSelectedImg;

/** Description:点选图片BLOCK */
@property (copy, nonatomic) DeleteImgBlock deleteImg;

- (void)deleteImg:(DeleteImgBlock)deleteImg;

//- (BOOL)selectedCurrentImg;

- (void)hiddenSelectedImgView;
- (void)hiddenDeleteImgView:(BOOL)hidden;
@end
