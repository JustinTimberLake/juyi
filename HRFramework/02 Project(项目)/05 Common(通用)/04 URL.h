

/*~!
 | @FUNC  项目网络请求url
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef HRURLConstant_h
#define HRURLConstant_h

#import <Foundation/Foundation.h>

#pragma mark - ---------- 协议(protocol) ----------
//协议（http/https）（含“//”后缀，不能为空）
#if DEBUG
static NSString *const URL_PROTOCOL = @"http://";
#else
static NSString *const URL_PROTOCOL = @"http://";
#endif


#pragma mark - ---------- 地址(host) ----------
//地址(host) （不能为空）
//123.57.137.151
#if DEBUG
static NSString *const URL_HOST = @"api.juyivip.com";
//static NSString *const URL_HOST = @"tapi.juyivip.com";

#else
static NSString *const URL_HOST = @"api.juyivip.com";
//static NSString *const URL_HOST = @"tapi.juyivip.com";

#endif

#pragma mark - ---------- 端口(port) ----------
//端口（port），（含“:”前缀，如果 URL_PORT 为空，则不含）
#if DEBUG
static NSString *const URL_PORT = @"";
//static NSString *const URL_PORT = @":8081";
#else
static NSString *const URL_PORT = @"";
//static NSString *const URL_PORT = @":8081";
#endif

#pragma mark - ---------- 路径(path) ----------
//路径通用前缀，（含后缀“/” ，如果 URL_PREFIX 为空， 则不含）
#if DEBUG
static NSString *const URL_PATH_PREFIX = @"";
static NSString *const URL_PATH_POSTFIX = @""; //接口连接后缀
//static NSString *const URL_PATH_POSTFIX = @".html"; //接口连接后缀
#else
static NSString *const URL_PATH_PREFIX = @"";
static NSString *const URL_PATH_POSTFIX = @""; //接口连接后缀
//static NSString *const URL_PATH_POSTFIX = @".html"; //接口连接后缀
#endif



//http://192.168.0.195/home/getHomeType.html
#define COMBIN(protocol,host,port,prefix,path,postfix) [NSString stringWithFormat:@"%@%@%@%@%@%@",protocol,host,port,prefix,path,postfix]

#define JY_HTTP_PREFIX  [NSString stringWithFormat:@"%@%@:8080",URL_PROTOCOL,URL_HOST]

#define JY_PATH(path) COMBIN(URL_PROTOCOL, URL_HOST, URL_PORT, URL_PATH_PREFIX, path,URL_PATH_POSTFIX)

//XXXX
static NSString *const JK_XXXX = @"JK_XXXX"; // ⚠️：变量名称全部大写，用下划线分
//----------登录注册-------------------------------------------------------//
//3.1.1. JY-001-001 登录
static NSString *const JY_LOGION = @"/login/login";

//3.1.2. JY-001-002 第三方登录
static NSString *const JY_LOGION_ThreeLogin = @"/login/threeLogin";

//3.1.3  JY-001-003 注册
static NSString *const JY_LOGION_Regist = @"/login/regist";

//JY-001-004 第三方注册
static NSString *const JY_LOGION_ThreeRegist = @"/login/threeRegist";

//JY-001-005 获取验证码
static NSString *const JY_LOGION_GetCode = @"/login/getCode";

//JY-001-006 忘记密码
static NSString *const JY_LOGION_ForgetPwd = @"/login/forgetPwd";

//JY-001-007 修改密码
static NSString *const JY_LOGION_ChangePwd = @"/login/changePwd";

//JY-001-008 获取用户信息
static NSString *const JY_LOGION_GetUser = @"/login/getUser";

//获取用户积分
static NSString *const JY_USER_GetMyPoints = @"/user/getMyPoints";

//JY-001-010 绑定手机号（校验手机号密码）
static NSString *const JY_LOGION_CheckAccount = @"/login/checkAccount";



//static NSString *const JK_HOME_TYPE = @"home/getHomeType";
//首页分类
//3.2.1. JY-002-001 获取城市编码
static NSString *const JY_HOME_GetCityCode = @"/home/getCityCode";

//3.2.2. JY-002-002 轮播图 banner
static NSString *const JY_HOME_Banner = @"/home/banner";

//3.2.3. JY-002-003 首页分类
static NSString *const JY_HOME_GetHomeType = @"/home/getHomeType";

//3.2.4.JY-002-004 广告位
static NSString *const JY_HOME_Position = @"/home/position";

//3.3	搜索
//3.3.1.	JY-003-001 搜索
static NSString *const JY_HOME_Search = @"/home/search";

//3.3.2.    JY-003-002 热门搜索
static NSString *const JY_HOME_HotSearch = @"/home/hotSearch";

//3.4	定位
//3.4.1	JY-004-001 获取热门城市
static NSString *const JY_LOCATION_GetHotCity = @"/location/getHotCity";

//3.3.1	JY-004-002 获取省
static NSString *const JY_LOCATION_GetProvince = @"/location/getProvince";

//3.3.1	JY-004-003 获取市
static NSString *const JY_LOCATION_GetCity = @"/location/getCity";

//3.3.1	JY-004-004 搜索市
static NSString *const JY_LOCATION_SearchCity = @"/location/searchCity";

//3.3.1	JY-004-005 获取所有省市区
static NSString *const JY_LOCATION_GetProCityCounty = @"/location/getProCityCounty";


//3.5	养车周边
//3.3.1	JY-005-001 获取养车店类型
static NSString *const JY_KEEPCAR_GetKeepType = @"/keepcar/getKeepType";

//3.3.1	JY-005-002 获取养车店列表
static NSString *const JY_KEEPCAR_GetKeepList = @"/keepcar/getKeepList";

//3.3.1	JY-005-003 获取养车店商品列表
static NSString *const JY_KEEPCAR_GetKeepGoodsList = @"/keepcar/getKeepGoodsList";

//3.6.1.	JY-006-001获取加油站类型
static NSString *const JY_GASSTATION_GetStationType = @"/gasstation/getStationType";

//3.6.2.	JY-006-002获取加油站列表
static NSString *const JY_GASSTATION_GetStationList = @"/gasstation/getStationList";

//3.6.2.	JY-006-003获取加油站详情
static NSString *const JY_GASSTATION_GetStationInfo = @"/gasstation/getStationInfo";

//3.6.2.	JY-006-004获取评论
static NSString *const JY_GASSTATION_GetComment = @"/gasstation/getComment";

//3.6.2.	JY-006-005获取油枪列表
static NSString *const JY_GASSTATION_GetOilGunList = @"/gasstation/getOilGunList";

//3.6.2.	JY-006-006创建订单
static NSString *const JY_GASSTATION_CreateOrder = @"/gasstation/createOrder";

//3.6.2.	JY-006-007提交订单
static NSString *const JY_GASSTATION_SubmitOrder = @"/gasstation/submitOrder";


//3.7	返利商城
//3.7.1.	JY-007-001 分类
static NSString *const JY_SHOP_GetShopClassify = @"/shop/getShopClassify";

//3.7.1.	JY-007-002 二级分类
static NSString *const JY_SHOP_GetShopSecondary= @"/shop/getShopSecondary";

//3.7.1.	JY-007-003 商品列表
static NSString *const JY_SHOP_GetGoodsList = @"/shop/getGoodsList";

//3.7.1.	JY-007-004 商品详情
static NSString *const JY_SHOP_GetGoodsInfo = @"/shop/getGoodsInfo";

//3.7.1.	JY-007-005 获取商品属性
static NSString *const JY_SHOP_GetAttribute = @"/shop/getAttribute";

//3.7.1.	JY-007-006 创建订单
static NSString *const JY_SHOP_CreateShopOrder = @"/shop/createShopOrder";

//3.7.1.	JY-007-007 提交订单
static NSString *const JY_SHOP_SubmitShopOrder = @"/shop/submitShopOrder";

//3.7.1.	JY-007-008 获取商城首页推广
static NSString *const JY_SHOP_GetHomeTop = @"/shop/getHomeTop";

//3.7.1.	JY-007-009 获取商品属性
static NSString *const JY_SHOP_GetAttributeTitle = @"/shop/getAttributeTitle";

//JY-007-010 获取商城首页/店铺首页商品列表
static NSString *const JY_SHOP_GetHomeGoodsList = @"/shop/getHomeGoodsList";

//JY-007-011 获取店铺头部数据
static NSString *const JY_SHOP_GetShopInformation = @"/shop/getShopInformation";

//Y-007-012 获取积分对换比率 
static NSString *const JY_SHOP_GetIntegralRatio = @"/shop/getIntegralRatio";

//JY-007-013  店铺搜索商品
static NSString *const JY_SHOP_ShopSearch = @"/shop/shopSearch";

//3.8	活动
//3.8.1.	JY-008-001 获取活动
static NSString *const JY_ACTIVITY_GetActivitys = @"/activity/getActivitys";

//
static NSString *const JY_ = @"";

#pragma mark - ======================== 社区模块 ========================

//3.9.1.	JY-009-001 获取社区帖子分类√
static NSString *const JY_BBS_GetBbsClassify = @"/bbs/getBbsClassify";

//3.9.1.	JY-009-002 获取社区帖子列表√
static NSString *const JY_BBS_GetBbsList = @"/bbs/getBbsList";

//3.9.1.	JY-009-003 获取社区帖子详情√
static NSString *const JY_BBS_GetBbsInfo = @"/bbs/getBbsInfo";

//3.9.1.	JY-009-004 写评论
static NSString *const JY_BBS_WriteComment = @"/bbs/writeComment";

//3.9.1.	JY-009-005 获取未读回复√
static NSString *const JY_BBS_GetUnreadList = @"/bbs/getUnreadList";

//3.9.1.	JY-009-006 发布帖子
static NSString *const JY_BBS_CreateBbs = @"/bbs/createBbs";

//3.9.1.	JY-009-007 获取未读回复数√
static NSString *const JY_BBS_GetUnreadNum = @"/bbs/getUnreadNum";

#pragma mark - ======================== 发现模块 ========================
//3.10.1.	JY-010-001 获取视频分类
static NSString *const JY_FIND_GetVideoClassify = @"/find/getVideoClassify";

//3.10.1.	JY-010-002 获取视频列表
static NSString *const JY_FIND_GetVideoList = @"/find/getVideoList";

//3.10.1.	JY-010-003 查违章
static NSString *const JY_FIND_GetIllegalList = @"/find/getIllegalList";

//3.10.1.	JY-010-004 获取合作加盟列表
static NSString *const JY_FIND_GetUnionsList = @"/find/getUnionsList";

//3.10.1.	JY-010-006 获取资讯列表
static NSString *const JY_FIND_GetNewsList = @"/find/getNewsList";

//3.10.1.	JY-010-008 爱车估值
static NSString *const JY_FIND_Valuation = @"/find/valuation";

//3.10.1.	JY-010-010 车辆类型
static NSString *const JY_FIND_GetCarType = @"/find/getCarType";

//3.10.1.    JY-010-011 品牌 (作废)
//static NSString *const JY_FIND_GetBrand = @"/find/getBrand";
//
////3.10.1.    JY-010-012 车辆用途(作废)
//static NSString *const JY_FIND_GetCarPurpose = @"/find/getCarPurpose";
//
////3.10.1.    JY-010-013 今日油价(作废)
//static NSString *const JY_FIND_GetOilPrice = @"/find/getOilPrice";

//3.10.1.	JY-010-014 获取油卡列表
static NSString *const JY_FIND_GetOilCardList = @"/find/getOilCardList";

//3.10.1.	JY-010-015 获取油卡详细
static NSString *const JY_FIND_GetOilCardInfo = @"/find/getOilCardInfo";

//3.10.1.	JY-010-016 绑定油卡
static NSString *const JY_FIND_BindOilCard = @"/find/bindOilCard";

//3.10.1.	JY-010-017 解除绑定
static NSString *const JY_FIND_RelieveBind = @"/find/relieveBind";

//3.10.1.	JY-010-018 应急电话
static NSString *const JY_FIND_Emergency = @"/find/emergency";

//聚合的请求
//JY-010-009 获取城市(限行查询)

static NSString *const JY_JUHE_GetRestrictCity = @"/juhe/getRestrictCity";

//Y-010-010 尾号限行查询
static NSString *const JY_JUHE_RestrictQuery = @"/juhe/restrictQuery";

//JY-010-028 违章高发地
static NSString *const JY_JUHE_IllegalHighHair = @"/juhe/illegalHighHair";

//JY-010-021 获取省份列表(二手车价值评估)
static NSString *const JY_JUHE_GetAssessmentProvince = @"/juhe/getAssessmentProvince";

//JY-010-022 获取城市列表(二手车价值评估)
static NSString *const JY_JUHE_GetAssessmentCity = @"/juhe/getAssessmentCity";

//JY-010-023 获取品牌列表(二手车价值评估)
static NSString *const JY_JUHE_GetAssessmentBrand = @"/juhe/getAssessmentBrand";

//JY-010-024 获取车系列表(二手车价值评估)
static NSString *const JY_JUHE_GetAssessmentCar = @"/juhe/getAssessmentCar";

//JY-010-025 获取车型列表(二手车价值评估)
static NSString *const JY_JUHE_GetAssessmentModels = @"/juhe/getAssessmentModels";

//JY-010-026 获取车型年份列表列表(二手车价值评估)
static NSString *const JY_JUHE_GetAssessmentModelYear = @"/juhe/getAssessmentModelYear";

//JY-010-027 二手车价值评估
static NSString *const JY_JUHE_ValueAssessment = @"/juhe/valueAssessment";

//JY-010-029 发现视频列表（4个）
static NSString *const JY_FIND_GetFindVideoList = @"/find/getFindVideoList";

//3.3	我的
//JY-011-001 修改头像
static NSString *const JY_USER_ChangeHead = @"/user/changeHead";

//JY-011-002 修改资料
static NSString *const JY_USER_ChangeUser = @"/user/changeUser";

//JY-011-003 提交车辆认证
static NSString *const JY_USER_SubmitLicense = @"/user/submitLicense";

//JY-011-004 获取车辆认证
static NSString *const JY_USER_GetLicense = @"/user/getLicense";

//JY-011-005 提交驾驶证认证
static NSString *const JY_USER_SubmitDrive = @"/user/submitDrive";

//JY-011-006 获取驾驶证认证
static NSString *const JY_USER_GetDrive = @"/user/getDrive";

//JY-011-007 删除驾驶证认证
static NSString *const JY_USER_DeleteDrive= @"/user/deleteDrive";

//JY-011-008 删除车辆认证
static NSString *const JY_USER_DeleteLicense = @"/user/deleteLicense";
//----------------------------我的模块----------------------------------------//

//3.3	我的账户
//3.12.1	JY-012-001 获取历史收支记录
static NSString *const JY_PERSONCENTER_MyAccountList = @"/account/getAccountList";
//JY-012-002 删除收支记录   
static NSString *const JY_PERSONCENTER_DelAccount = @"/account/delAccount";

//3.12.3	JY-012-003 充值
static NSString *const JY_PERSONCENTER_Rrcharge = @"/account/recharge";

//JY-013-004 清空记录
static NSString *const JY_PERSONCENTER_RemoveAccount = @"/account/removeAccount";

//JY-012-005 获取第三方支付信息(所有支付)
static NSString *const JY_PAY_GetPay = @"/pay/getPay";

//JY-012-006 获取支付宝待签名字符串接口
static NSString *const JY_PAY_GetAlipayPaySign = @"/pay/getAlipayPaySign";

//3.3.1    JY-012-007 获取微信支付请求预支付id接口
static NSString *const JY_PAY_GetWxPaySign = @"/pay/getWxPaySign";

//3.3.1    JY-012-008 退款【服务订单、实体订单和商品订单未发货】接口  √（目前只完成支付宝的）
static NSString *const JY_PAY_ApplyRefund = @"/user/applyRefund";


//Y-012-010 账户余额支付接口
static NSString *const JY_PAY_AccountBalancePay = @"/pay/accountBalancePay";

//Y-012-011 油库抵扣支付接口
static NSString *const JY_PAY_OilDepotPay = @"/pay/oilDepotPay";

//Y-012-012 支付成功后获取优惠券和积分接口
static NSString *const JY_PAY_GetDiscount = @"/pay/getDiscount";


//3.3	我的积分
//3.13.1.	JY-013-001 获取我的积分记录
static NSString *const JY_PERSONCENTER_SocreList = @"/score/getScoreList";

//JY-013-002 删除积分记录
static NSString *const JY_PERSONCENTER_DelScore = @"/score/delScore";

//JY-013-003 清空积分记录
static NSString *const JY_PERSONCENTER_RemoveScore = @"/score/removeScore";

//3.3	我的油库
//3.14.1.	JY-014-001 获取剩余油量
static NSString *const JY_PERSONCENTER_MyGasoline = @"/oildepot/getOilDepot";

//3.14.2.	JY-014-002 充值油库
static NSString *const JY_PERSONCENTER_MyRechangeGasoline = @"/oildepot/rechargeOil";

//JY-014-003 获取油库收支记录 
static NSString *const JY_OILDEPOT_GetOilDepotList = @"/oildepot/getOilDepotList";
//JY-014-004 删除油库收支记录
static NSString *const JY_OILDEPOT_DelOilDepot = @"/oildepot/delOilDepot";

//JY-014-005 清空油库收支记录
static NSString *const JY_OILDEPOT_RemoveOilDepot = @"/oildepot/removeOilDepot";

//JY-014-006 处理订单（扫描消费）
static NSString *const JY_OILDEPOT_DealOrder = @"/oilDepot/dealOrder";

//3.14.7.JY-014-007 获取充值油库的油型数据
static NSString *const JY_OILDEPOT_GetOilDepotDate = @"/oildepot/getOilDepotDate";

//3.14.8.JY-014-008 充值油卡
static NSString *const JY_OILDEPOT_RechargeOilCard = @"/oildepot/rechargeOilCard";

//3.14.9.JY-014-009 获取充值金额数据
static NSString *const JY_OILDEPOT_GetMoney = @"/oildepot/getMoney";




//3.15	服务订单
//3.15.1.	JY-015-001 获取服务订单列表
static NSString *const JY_PERSONCENTER_MyServiceOrder = @"/serviceorder/getServiceOrderList";

//3.15.2.	JY-015-002 获取服务订单详情
static NSString *const JY_PERSONCENTER_MyServiceOrderDetail = @"/serviceorder/getServiceOrderInfo";

//JY-015-003 删除、取消、退款 服务订单 
static NSString *const JY_SERVICEORDER_DelServiceOrder = @"/serviceorder/delServiceOrder";


//3.16	商品订单
//3.16.1.	JY-016-001 获取商品订单列表
static NSString *const JY_PERSONCENTER_MyProductOrder = @"/goodsorder/getGoodsOrderList";

//3.16.2.	JY-016-002 获取商品订单详细
static NSString *const JY_PERSONCENTER_MyProductOrderDeatil = @"/goodsorder/getGoodsOrderInfo";

//JY-016-003 删除、取消商品订单
static NSString *const JY_GOODSORDER_DelGoodsOrder = @"/goodsorder/delGoodsOrder";

//确认收货
static NSString *const JY_GOODSORDER_ConfirmReceipt =@"/goodsorder/confirmReceipt";

//3.16.1.	JY-016-004 获取物流信息
static NSString *const JY_GOODSORDER_GetLogistics = @"/goodsorder/getLogistics";

//JY-016-005 评价 
static NSString *const JY_GOODSORDER_Evaluate = @"/goodsorder/evaluate";

//JY-016-006 获取退货理由
static NSString *const JY_GOODSORDER_GetReasonList = @"/goodsorder/getReasonList";

//JY-016-007 退款 商品订单
static NSString *const JY_GOODSORDER_ApplyRefund = @"/goodsorder/applyRefund";

//JY-016-008 获取物流公司
static NSString *const JY_GOODSORDER_GetLogisticsList = @"/goodsorder/getLogisticsList";

//JY-016-009 填写物流信息
static NSString *const JY_GOODSORDER_WriteLogistics = @"/goodsorder/writeLogistics";

//JY-016-011  获取退款价格
static NSString *const JY_GOODSORDER_GetReturnPrice = @"/goodsorder/getReturnPrice";


//3.3    订单实体服务
//JY-017-002 获取实体服;务订单详情
static NSString *const JY_ENTITYORDER_GetEntityOrderInfo = @"/entityorder/getEntityOrderInfo";

//3.3	我的消息
//3.18.1. JY-018-001获取系统消息列表
static NSString *const JY_PERSONCENTR_MyMessage = @"/message/getMessageList";

//JY-018-002获取系统消息详情
static NSString *const JY_PERSONCENTR_getMessageInfo = @"/message/getMessageInfo";

//3.18.3. JY-018-003删除系统消息
static NSString *const JY_PERSONCENTER_DelMySysMessage = @"/message/delMessage";

//批量删除系统消息
static NSString *const JY_PERSONCENTER_BatchDelete = @"/message/batchDelete";

//3.18.6. JY-018-006系统消息 标记为已读
static NSString *const JY_MESSAGE_BatchRead = @"/message/batchRead";


//3.19.1. JY-019-001 我的收藏的油站
static NSString *const JY_PERSONCENTER_MyFavoriteStation = @"/collect/getCollectStation";

//3.19.2.	JY-019-002 收藏油站
static NSString *const JY_PERSONCENTER_CollectStation = @"/collect/collectStation";

//3.19.3. JY-019-003 我的收藏的商品
static NSString *const JY_PERSONCENTER_MyFavoriteProduct = @"/collect/getCollectGoods";

//JY-019-004 收藏商品
static NSString *const JY_PERSONCENTER_CollectGoods = @"/collect/collectGoods";

//3.19.5. JY-019-005 我的收藏的商家
static NSString *const JY_PERSONCENTER_MyFavoriteSeller = @"/collect/getCollectSeller";

//3.19.6.	JY-019-006 收藏商家
static NSString *const JY_PERSONCENTER_CollectSeller = @"/collect/collectSeller";


//3.19.7. JY-019-007 我的收藏的视频
static NSString *const JY_PERSONCENTER_MyFavoriteMv = @"/collect/getCollectVideo";

//3.19.8. JY-019-008 收藏视频
static NSString *const JY_PERSONCENTER_CollectVideo = @"/collect/collectVideo";

//3.20.1. JY-020-001 获取优惠券列表
static NSString *const JY_PERSONCENTER_MyRedPick = @"/coupon/getCouponList";

//3.20.2. JY-020-002 领取优惠券
static NSString *const JY_PERSONCENTER_DrawRedPick = @"/coupon/receiveCoupon";

//3.22.2.	JY-022-002 获取我的地址详细
static NSString *const JY_PERSONCENTER_MyAdressList = @"/address/getAddress";

//3.22.2.	JY-022-002 获取我的地址详细
static NSString *const JY_PERSONCENTER_AdressInfo = @"/address/getAddressInfo";

//3.22.3.	JY-022-003 添加修改地址
static NSString *const JY_PERSONCENTER_AddAdress = @"/address/addAddress";

//3.22.4.	JY-022-004 删除地址
static NSString *const JY_PERSONCENTER_DelAdress = @"/address/delAddress";

//3.22.5.	JY-022-005 修改默认地址
static NSString *const JY_PERSONCENTER_ChangeAdress = @"/address/changeDftAddress";

//3.20.1.	JY-027-001 获取h5内容页URL
static NSString *const JY_CONTENTURL_GetContentUrl = @"/contenturl/getContentUrl";

//JY-021-001我的发布
static NSString *const JY_MYPUBLISH_GetMyBbsList = @"/bbsrelease/getMyBbsList";

//我的发布删除
static NSString *const JY_MYPUBLISH_DeleteMyBbs  = @"/bbsrelease/deleteMyBbs";

//3.20.	我的发票信息
//JY-023-001 创建发票
static NSString *const JY_INVOICE_CreateInvoice = @"/invoice/createInvoice";

//JY-023-002 获取发票列表
static NSString *const JY_INVOICE_GetInvoice = @"/invoice/getInvoice";

//JY-023-003 删除发票 
static NSString *const JY_INVOICE_DelInvoice = @"/invoice/delInvoice";

//JY-023-004 获取发票记录
static NSString *const JY_INVOICE_GetInvoiceHistory = @"/invoice/getInvoiceHistory";

//JY-023-005 删除发票记录
static NSString *const JY_INVOICE_DelInvoiceHistory = @"/invoice/delInvoiceHistory";

//3.20.	意见反馈
//JY-024-001 意见反馈
static NSString *const JY_FEEDBACK_feedback = @"/feedback/feedback";

//JY-025-001联系我们
static NSString *const JY_CONTACT_contactUS = @"/contact/contactUS";

//二维码扫描登录
static NSString *const JY_CONTACT_Checkimg = @"http://www.juyivip.com/index.php/home/index/checkimg.html";

//图片上传
static NSString * const JY_USER_ImgUpFile = @"/user/imgUpFile";

//WebSocket
static NSString * const JY_USER_WebSocket = @"ws://123.56.223.56:9501";

//检测订单是否付款
static NSString * const JY_Staff_CheckOrderpay = @"/staff/checkOrderpay";

//JY-020-0021 判断是否弹出优惠券
static NSString *const JY_coupon_showCoupon = @"/coupon/showCoupon";

#pragma mark - ---------- 其他（others） ----------

#pragma mark 图片路径通用前缀
//包括协议、地址、端口号...。含“/”，如果 URL_IMG_PREFIX 为空，则不含。
static NSString *const URL_IMG_PREFIX = @"xxx/";


#endif /* HRURLConstant_h */
