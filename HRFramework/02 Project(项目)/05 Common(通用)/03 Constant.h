
/*~!
 | @FUNC  项目常量
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef Constant_h
#define Constant_h

/*
    //举例
    //是否是第一次进入程序
    static NSString *const USERDEFAULT_FIRST_ENTER = @"USERDEFAULT_FIRST_ENTER"; // ⚠️变量名称全部大写，多个单词用下划线分隔
 */

#pragma mark - ---------- 偏好设置（Userdefault）----------
//当前城市code
static NSString *const JY_CURRENT_CITYCODE = @"JYCurrentCityCode";
//当前定位城市code
static NSString *const JY_LOCATION_CITYCODE = @"JYLocationCityCode";
//当前城市名字
static NSString *const JY_CURRENT_CITYNAME = @"JYCurrentCityName";
//当前城市名字
static NSString *const JY_CURRENT_SELECTCITYNAME = @"JYCurrentSelectCityName";
//当前城市纬度
static NSString *const JY_CURRENT_CITYLAT = @"JYCurrentCityLat";
//当前城市经度
static NSString *const JY_CURRENT_CITYLNG = @"JYCurrentCityLng";





#pragma mark - ---------- 解归档（Archive）----------
// 本地地区缓存
static NSString *const LOCAL_REGION_IDENTIFIER = @"LOCAL_REGION_IDENTIFIER";
// 登录C
static NSString *const LOGIN_C = @"LOGIN_C";
// 客户电话
static NSString *const CUSTOMER_TEL = @"customerTel";
// 个人信息
static NSString *const LOGIN_PERSONAL = @"LOGIN_PERSONAL";
#pragma mark - ---------- 通知（Notification）----------
//登录返回通知
static NSString *const LOGIN_BACK = @"login_Back";
//修改城市
static NSString *const JY_SELECT_CITY = @"selectCity";

#pragma mark - ---------- 颜色 (Color) ----------

//俥驿用户端-统一色板
static NSString *const JYUCOLOR_GRAY_BG = @"F5F5F5"; //245:245:245  //统一灰色背景
static NSString *const JYUCOLOR_YELLOW_MAIN = @"FCC71E"; //252:199:30   //统一黄色（主色调）
static NSString *const JYUCOLOR_LINE_HEAD = @"666666";   //102:102:102  //统一线条1（头部）
static NSString *const JYUCOLOR_LINE = @"DBDBDB";    //219:219:219      //统一线条2
static NSString *const JYUCOLOR_TITLE = @"333333";      //51:51:51  标题颜色

#pragma mark - ---------- 字体(Font) ----------
//统一中文字体：冬青黑体简体中文


static NSString *const USER_HEADEIMAGE = @"USER_HEADEIMAGE";

static NSString *const USER_NICKNAME = @"USER_NICKNAME";

#pragma mark - ----------友盟 ----------

static NSString *const UM_INITIAL_APPKEY = @"5992676a7f2c7449e6001027";

//微信支付和友盟同一个key
static NSString *const UM_WEXIN_APPKEY = @"wxaa093aa0507ca67f";

static NSString *const UM_WEXIN_APPSECRET = @"9691971c3aba3a7380c179fab4054758";

//qq的APPkey 和 APPSECRET
static NSString *const JY_QQ_APPKEY = @"1106387068";

static NSString *const JY_QQ_APPSECRET = @"mSZGOb1yijI8g81R";

//微博的appkey和 appSecrte
//
static NSString *const JY_WB_APPKEY = @"231527261";

static NSString *const JY_WB_APPSECRET = @"347706ad51d927486e368c2efbc8309a";

#pragma mark - ---------- 支付宝 ----------
static NSString *const JY_ALIPAY_APPSCHEME = @"JYUSER";

//微信的apikey
static NSString *const JY_WXAPIKEY =@"77f77057e3afc94820695fd40da3d44b";



#pragma mark - ---------- 其他（Others） ----------

static NSString *const NET_NOT_WORK = @"网络断开连接，请检查网络";

static NSString *const ERROR_MESSAGE = @"网络请求失败";


#pragma mark - ---------- 通知 ----------

//我的油库中全部清除
static NSString *const JY_OILDEPOT_REMOVEALL = @"JY_OILDEPOT_REMOVEALL";

//我的积分全部清除
static NSString *const JY_MYSCORE_REMOVEALL = @"JY_MYSCORE_REMOVEALL";

//视频播放全屏
static NSString *const JY_VIDEO_FULLSCREEN = @"JY_VIDEO_FULLSCREEN";

//视频收藏
static NSString *const JY_VIDEO_COLLECT = @"JY_VIDEO_COLLECT";

//订单ID
static NSString *const JY_PAY_ORDERID = @"JY_PAY_ORDERID";

//积分清除
static NSString *const JY_SCORE_Remainder = @"JY_SCORE_REMOVEALL";

//支付宝支付后的结果通知
static NSString *const JY_NOTI_PAY_ALIPAY = @"JY_NOTI_PAY_ALIPAY";

//账户余额支付
static NSString *const JY_NOTI_PAY_ACCOUNT = @"JY_NOTI_PAY_ACCOUNT";

//支付成功
static NSString *const JY_NOTI_PAY_SUCCESS = @"JY_NOTI_PAY_SUCCESS";
//支付失败
static NSString *const JY_NOTI_PAY_FAILURE = @"JY_NOTI_PAY_FAILURE";

//充值账户余额成功
static NSString *const JY_NOTI_PAY_BALANCE_SUCCESS = @"JY_NOTI_PAY_BALANCE_SUCCESS";

//登录成功
static NSString *const JY_NOTI_LOGIN_SUCCESS = @"JY_NOTI_LOGIN_SUCCESS";

//登录异常
static NSString *const JY_NOTI_LOGIN_ERROE = @"JY_NOTI_LOGIN_ERROE";

//退出登录
static NSString *const JY_NOTI_LOGOUT = @"JY_NOTI_LOGOUT";

//当前定位城市
static NSString *const JY_NOTI_LOCATIONCITY = @"JY_NOTI_LOCATIONCITY";

//定位
static NSString *const JY_NOTI_START_LOCATIONCITY = @"JY_NOTI_START_LOCATIONCITY";

//搜索关键字
static NSString *const JY_NOTI_SEARCHKEYWORD = @"JY_NOTI_SEARCHKEYWORD";

//新人福利
//static NSString *const JY_NOTI_NEWWELFARE = @"JY_NOTI_NEWWELFARE";

#pragma mark --------------聚合第三方相关-------------------------

//违章查询
static NSString *const JY_JUHE_WEIZHANGCHAXUN_APPKEY = @"a6d98103d01d3799f9fec93755e7538a";
//违章查询城市URL
static NSString *const JY_JUHE_WEIZHANGCHAXUN_CITY_URL = @"http://v.juhe.cn/wz/citys";
//违章查询结果URL
static NSString *const JY_JUHE_WEIZHANGCHAXUN_QUERY_URL = @"http://v.juhe.cn/wz/query";

//违章查询本地存储数据位置
//static NSString *const JY_JUHE_ACHIVEDATA_WEIZHANG = @"JY_JUHE_ACHIVEDATA_WEIZHANG";


//爱车估值
static NSString *const JY_JUHE_AICHEGUZHI_APPKEY = @"fe2af34d92c1bc0cae546baffe7429d1";

//限行查询
static NSString *const JY_JUHE_XIANXINGCHAXUN_APPKEY = @"598ef5d863920877249faf19e10c28ab";

//今日油价
static NSString *const JY_JUHE_JINRIYOUJIA_APPKEY = @"558cd0f9c26738fb4b7d10668afc0b86";
//今日油价网页
static NSString *const JY_JUHE_JINRIYOUJIA_URL = @"http://apis.juhe.cn/cnoil/oil_city";


//违章高发地
static NSString *const JY_JUHE_WEIZHANGGAOFADI_APPKEY = @"047154db7f8f8af6f39974772ba63c2a";




#endif /* ProjectCommon_h */
