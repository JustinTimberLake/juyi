
/*~!
 | @FUNC  项目宏
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef Macro_h
#define Macro_h

#pragma mark ------------手机尺寸-----------
#define SCREEN_NAVTOPEDGE  64
#define SCREEN_StatusBarHeight  20

#pragma mark ------------第一次加载---------
#define JYVersionKey @"version"


#define LOADXIB(XIBNAME)  [[NSBundle mainBundle]loadNibNamed:XIBNAME owner:self options:nil].lastObject

// 弱引用
#define WEAKSELF __weak typeof(self) weakSelf = self;

#pragma mark - ======================== Placeholder ========================

#define JY_PLACEHOLDER_IMAGE [UIImage imageNamed:@"占位图"]
#define DEFAULT_HEADER_IMG [UIImage imageNamed:@"头像默认图"]//默认头像
#define DEFAULT_COURSE [UIImage imageNamed:@"占位图"] //默认商品图片
#define JY_PLACEHOLDER_IMAGE_VIDEO [UIImage imageNamed:@"视频图片-暗影"] //视频默认图

#define RESULT_SUCCESS [result[@"status"] intValue]
#define RESULT_DATA result[@"data"]
#define RESULT_MESSAGE result[@"errorMsg"]

 
#define JY_USERDEFAULTS [NSUserDefaults standardUserDefaults]

#define JY_NOTIFICATION [NSNotificationCenter defaultCenter]

#define JY_POST_NOTIFICATION(NOTINAME,OBJECTNAME)  [[NSNotificationCenter defaultCenter] postNotificationName:NOTINAME object:OBJECTNAME]

#define JY_ADD_NOTIFICATION(NOTINAME)   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotification:) name:NOTINAME  object:nil];


#define kDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#pragma mark  根据5或者6设计图adaptor

#define HB_IPONE5_ADAPTOR_HEIGHT(__HEIGHT__)  ([UIScreen mainScreen].bounds.size.height * __HEIGHT__ / 568)
#define HB_IPONE5_ADAPTOR_WIDTH(__WIDTH__)    ([UIScreen mainScreen].bounds.size.width * __WIDTH__ / 320)

#define HB_IPONE6_ADAPTOR_HEIGHT(__HEIGHT__)  ([UIScreen mainScreen].bounds.size.height * __HEIGHT__  / 667)
#define HB_IPONE6_ADAPTOR_WIDTH(__WIDTH__)    ([UIScreen mainScreen].bounds.size.width * __WIDTH__  / 375)


#endif /* Macro_h */
