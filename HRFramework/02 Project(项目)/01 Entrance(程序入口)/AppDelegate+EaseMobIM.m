//
//  AppDelegate+EaseMobIM.m
//  JY
//
//  Created by Stronger_WM on 2017/7/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "AppDelegate+EaseMobIM.h"

@interface AppDelegate ()<EMClientDelegate>


@end

@implementation AppDelegate (EaseMobIM)

- (void)easeMob_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    EMOptions *options = [EMOptions optionsWithAppkey:@"1124170714115661#moer-jyuser"];
    options.apnsCertName = @"moer-jyuser_dev";
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
    
}

- (void)easeMob_applicationDidEnterBackground:(UIApplication *)application
{
    [[EMClient sharedClient] applicationDidEnterBackground:application];
}

- (void)easeMob_applicationWillEnterForeground:(UIApplication *)application
{
    [[EMClient sharedClient] applicationWillEnterForeground:application];
    //[[EMClient sharedClient] applicationWillEnterForeground:application];
}

- (void)easeMob_login
{
    BOOL isAutoLogin = [EMClient sharedClient].options.isAutoLogin;
    if (!isAutoLogin) {
        EMError *error = [[EMClient sharedClient] loginWithUsername:[User_InfoShared shareUserInfo].personalModel.hxUser password:[User_InfoShared shareUserInfo].personalModel.hxPwd];
//        EMError *error = [[EMClient sharedClient] loginWithUsername:@"18330121607" password:@"123456q"];

        if (!error) {
            [[EMClient sharedClient].options setIsAutoLogin:YES];
            NSLog(@"环信登录成功");
        }else{
//            [self easeMob_login];
        }
    }
}

- (void)easeMob_logout
{
    EMError *error = [[EMClient sharedClient] logout:YES];
    if (!error) {
        NSLog(@"退出成功");
    }
}

#pragma mark - ======================== EMClientDelegate ========================

- (void)autoLoginDidCompleteWithError:(EMError *)error
{
    if (error) {
        NSLog(@"错误代码%u,错误描述%@",error.code,error.description);
    }
    else
    {
        NSLog(@"自动登录成功");
    }
}

- (void)connectionStateDidChange:(EMConnectionState)aConnectionState
{
    NSLog(@"网络变化，当前状态%u",aConnectionState);
}

- (void)userAccountDidLoginFromOtherDevice
{
    [self easeMob_logout];
}

- (void)userAccountDidRemoveFromServer
{
    [self easeMob_logout];
}

@end
