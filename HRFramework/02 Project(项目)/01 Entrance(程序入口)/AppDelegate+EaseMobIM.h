//
//  AppDelegate+EaseMobIM.h
//  JY
//
//  Created by Stronger_WM on 2017/7/26.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "AppDelegate.h"
#import <Hyphenate/Hyphenate.h>

@interface AppDelegate (EaseMobIM)

- (void)easeMob_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

// APP进入后台
- (void)easeMob_applicationDidEnterBackground:(UIApplication *)application;

// APP将要从后台返回
- (void)easeMob_applicationWillEnterForeground:(UIApplication *)application;

- (void)easeMob_login;


- (void)easeMob_logout;

#pragma mark - ======================== EMClientDelegate ========================

/*!
 *  自动登录返回结果
 *
 *  @param error 错误信息
 */
- (void)autoLoginDidCompleteWithError:(EMError *)error;

/*!
 *  SDK连接服务器的状态变化时会接收到该回调
 *
 *  有以下几种情况，会引起该方法的调用：
 *  1. 登录成功后，手机无法上网时，会调用该回调
 *  2. 登录成功后，网络状态变化时，会调用该回调
 *
 *  @param aConnectionState 当前状态
 */
- (void)connectionStateDidChange:(EMConnectionState)aConnectionState;

/*!
 *  当前登录账号在其它设备登录时会接收到该回调
 */
- (void)userAccountDidLoginFromOtherDevice;

/*!
 *  当前登录账号已经被从服务器端删除时会收到该回调
 */
- (void)userAccountDidRemoveFromServer;


@end
