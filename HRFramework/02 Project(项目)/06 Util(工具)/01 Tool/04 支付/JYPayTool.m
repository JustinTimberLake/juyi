//
//  JYPayTool.m
//  JY
//
//  Created by Duanhuifen on 2017/10/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYPayTool.h"
#import "JYAlipayClient.h"
#import "JYWeChatClient.h"

@implementation JYPayTool

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    static id singleton;
    dispatch_once(&onceToken, ^{
        singleton = [[self alloc] init];
    });
    
    return singleton;
}

- (void)payWithType:(JYPayType)payType{
    if (payType == JYPayType_Alipay) {
//        支付宝
        
        
    }else if (payType == JYPayType_WeChat){
//    微信
    }else{
//        账户
        
    }
}

//支付宝支付
- (void)payWithAlipayWithOrderString:(NSString *)orderString andAppScheme:(NSString *)appScheme{
    [[JYAlipayClient sharedInstance] pay:orderString scheme:appScheme result:^(NSString *code, NSString *msg) {
        
    }];
}


@end
