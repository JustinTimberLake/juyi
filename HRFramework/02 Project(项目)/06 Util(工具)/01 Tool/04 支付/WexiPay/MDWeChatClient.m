

#import "MDWeChatClient.h"
#import "WeXinRequest.h"
#import <CommonCrypto/CommonDigest.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

@implementation MDWeChatClient

- (void)pay:(WeXinRequest *)weXinRequest  {

    PayReq *request     = [[PayReq alloc] init];
    
    request.partnerId   = weXinRequest.partnerid;
    request.prepayId    = weXinRequest.prepayid;
    request.package     = weXinRequest.package;
    request.nonceStr    = weXinRequest.noncestr;
    request.timeStamp   = weXinRequest.timestamp.intValue;
//    
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    [dic setObject:weXinRequest.partnerid forKey:@"partnerid"];
//    [dic setObject:weXinRequest.prepayid forKey:@"prepayid"];
//    [dic setObject:weXinRequest.package forKey:@"package"];
//    [dic setObject:weXinRequest.noncestr forKey:@"nonceStr"];
//    [dic setObject:weXinRequest.timestamp forKey:@"timeStamp"];
//    [dic setObject:weXinRequest.appid forKey:@"appid"];
//    NSString *signStr = [[self class] createMd5Sign:dic key:@"19710119jiajianqiaojianhong72747"];
    request.sign        = weXinRequest.sign;
    
    
    [WXApi sendReq:request];

}

-(void)onResp:(BaseResp*)resp {
    if ([resp isKindOfClass:[PayResp class]]){
        PayResp*response=(PayResp*)resp;
        switch(response.errCode){
            case WXSuccess:
                //服务器端查询支付通知或查询API返回的结果再提示成功
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PAY_SUCCESS_WXPAY" object:nil userInfo:nil];
                NSLog(@"支付成功");
                break;
            default:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PAY_FAIL_WXPAY" object:nil userInfo:nil];
                NSLog(@"支付失败，retcode=%d",resp.errCode);
                break;
        }
    }
}

+ (NSString *)stringSignForWXWithAPPID:(NSString *)appID partnerid:(NSString *)partnerid prepayid:(NSString *)prepayid timestamp:(NSString *)timestamp noncestr:(NSString *)noncestr key:(NSString *)key {
    
    NSMutableDictionary *signParams = [NSMutableDictionary dictionary];
    [signParams setObject:appID forKey:@"appid"];//应用ID 微信开放平台审核通过的应用APPID
    [signParams setObject:partnerid forKey:@"partnerid"];//微信支付分配的商户号
    [signParams setObject:prepayid forKey:@"prepayid"];//预支付交易会话ID 微信返回的支付交易会话ID
    [signParams setObject:@"Sign=WXPay" forKey:@"package"];//扩展字段
    [signParams setObject:timestamp forKey:@"timestamp"];//时间戳
    [signParams setObject:noncestr forKey:@"noncestr"];//随机字符串
    
    return [self createMd5Sign:signParams key:key];
    
}

+ (NSString *) createMd5Sign:(NSMutableDictionary*)dict key:(NSString *)key
{
    NSMutableString *contentString  =[NSMutableString string];
    NSArray *keys = [dict allKeys];
    //按字母顺序排序
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    //拼接字符串
    for (NSString *categoryId in sortedArray)
    {
        if (   ![[dict objectForKey:categoryId] isEqualToString:@""]
            && ![categoryId isEqualToString:@"sign"]
            && ![categoryId isEqualToString:@"key"]
            )
        {
            [contentString appendFormat:@"%@=%@&", categoryId, [dict objectForKey:categoryId]];
        }
    }
    
    [contentString appendFormat:@"key=%@", key];
    
    NSString *md5Sign =[self md5str:contentString];
    
    return md5Sign;
}


+ (NSString *)md5str:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (unsigned int)strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02X", digest[i]];
    }
    
    return output;
}

+ (NSString *)createRandomStr:(NSInteger)byte {
     
    NSString *string = [[NSString alloc]init];
    for (int i = 0; i < byte; i++) {
        int number = arc4random() % 36;
        if (number < 10) {
            int figure = arc4random() % 10;
            NSString *tempString = [NSString stringWithFormat:@"%d", figure];
            string = [string stringByAppendingString:tempString];
        }else {
            int figure = (arc4random() % 26) + 97;
            char character = figure;
            NSString *tempString = [NSString stringWithFormat:@"%c", character];
            string = [string stringByAppendingString:tempString];
        }
    }
    return string;
}



+ (instancetype)sharedInstance{
    static dispatch_once_t once;
    static id __singleton__;
    dispatch_once( &once, ^{ __singleton__ = [[self alloc] init]; } );
    return __singleton__;
}


@end
