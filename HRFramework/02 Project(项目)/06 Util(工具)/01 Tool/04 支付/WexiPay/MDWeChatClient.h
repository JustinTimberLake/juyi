

#import "JYBaseViewModel.h"
@class WeXinRequest;

@interface MDWeChatClient : JYBaseViewModel<WXApiDelegate>

- (void)pay:(WeXinRequest *)weXinRequest;

+ (instancetype)sharedInstance;

@end
