

#import "JYBaseViewModel.h"
@class WeXinRequest;

@interface JYWeChatClient : JYBaseViewModel<WXApiDelegate>

+ (instancetype)sharedInstance;

//返回签名
+ (NSString *)stringSignForWXWithAPPID:(NSString *)appID partnerid:(NSString *)partnerid prepayid:(NSString *)prepayid timestamp:(NSString *)timestamp noncestr:(NSString *)noncestr key:(NSString *)key;

//发起请求
- (void)pay:(WeXinRequest *)weXinRequest;


@end
