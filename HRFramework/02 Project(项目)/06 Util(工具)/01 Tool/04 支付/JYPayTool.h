//
//  JYPayTool.h
//  JY
//
//  Created by Duanhuifen on 2017/10/10.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,JYPayType) {
    JYPayType_Account,//账户支付
    JYPayType_Alipay,//支付宝
    JYPayType_WeChat,//微信
};

@interface JYPayTool : NSObject

+ (instancetype)shareInstance;

//支付方式（暂时不用）
- (void)payWithType:(JYPayType)payType;

//支付宝支付
- (void)payWithAlipayWithOrderString:(NSString *)orderString andAppScheme:(NSString *)appScheme;




@end
