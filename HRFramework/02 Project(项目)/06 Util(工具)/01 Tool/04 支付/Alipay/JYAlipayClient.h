
#import "JYBaseViewModel.h"


typedef void (^PayResultBlock)(NSString *code, NSString *msg);

@interface JYAlipayClient : JYBaseViewModel

/** 支付结果回调 */
@property (copy, nonatomic) PayResultBlock payResultBlock;

/** pay */
@property (copy, nonatomic) JYAlipayClient *(^Pay)(NSString *orderString, NSString *appScheme);

- (void)setPay:(JYAlipayClient *(^)(NSString *, NSString *))Pay;

- (void)pay:(NSString *)orderString scheme:(NSString *)appScheme result:(PayResultBlock)result;

//- (void)payResult:(PayResultBlock)payResult;

+ (instancetype)sharedInstance;
@end
