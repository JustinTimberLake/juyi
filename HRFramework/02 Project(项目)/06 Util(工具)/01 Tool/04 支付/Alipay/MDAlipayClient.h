
#import "JYBaseViewModel.h"


typedef void (^PayResultBlock)(NSString *code, NSString *msg);

@interface MDAlipayClient : JYBaseViewModel

/** 支付结果回调 */
@property (copy, nonatomic) PayResultBlock payResultBlock;

/** pay */
@property (copy, nonatomic) MDAlipayClient *(^Pay)(NSString *orderString, NSString *appScheme);

- (void)setPay:(MDAlipayClient *(^)(NSString *, NSString *))Pay;

- (void)pay:(NSString *)orderString scheme:(NSString *)appScheme result:(PayResultBlock)result;

//- (void)payResult:(PayResultBlock)payResult;

+ (instancetype)sharedInstance;
@end
