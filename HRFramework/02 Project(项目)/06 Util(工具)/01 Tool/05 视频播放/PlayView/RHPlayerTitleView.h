//
//  RHPlayerTitleView.h
//  MCSchool
//
//  Created by 郭人豪 on 2017/4/14.
//  Copyright © 2017年 Abner_G. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RHPlayerTitleViewDelegate;
@interface RHPlayerTitleView : UIView

@property (nonatomic, weak) id<RHPlayerTitleViewDelegate> delegate;
@property (nonatomic, copy) NSString * title;
@property (nonatomic,copy) NSString *isCollect;
//视频收藏BLOCK
@property (nonatomic,copy) void (^VideoCollectionBlock)(NSString * isCollect);


- (void)showBackButton;
- (void)hideBackButton;
@end
@protocol RHPlayerTitleViewDelegate <NSObject>

@optional
- (void)titleViewDidExitFullScreen:(RHPlayerTitleView *)titleView;



@end
