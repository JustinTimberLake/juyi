//
//  JYAuthorTool.m
//  JY
//
//  Created by Duanhuifen on 2017/12/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "JYAuthorTool.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>


@implementation JYAuthorTool
/**
 *  请求相册访问权限
 *
 *  @param callback
 */
+ (void)requestImagePickerAuthorization:(void(^)(JYAuthorState status))callback{
    WEAKSELF
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ||
        [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        
        if ([UIDevice currentDevice].systemVersion.floatValue < 8.0) {
            ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
            if (authStatus == ALAuthorizationStatusNotDetermined) {
                [weakSelf executeCallback:callback status:JYAuthorStateAuthorized];
            }else if (authStatus == ALAuthorizationStatusAuthorized) {
                [weakSelf executeCallback:callback status:JYAuthorStateAuthorized];
            } else if (authStatus == ALAuthorizationStatusDenied) {
                [weakSelf executeCallback:callback status:JYAuthorStateDenied];
            } else if (authStatus == ALAuthorizationStatusRestricted) {
                [weakSelf executeCallback:callback status:JYAuthorStateRestricted];
            }
        }else{
            //获取相册访问权限
//            PHAuthorizationStatus photoStatus = [PHPhotoLibrary authorizationStatus];
            
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    switch (status) {
                        case PHAuthorizationStatusAuthorized: {
                            //已获取权限
                           [weakSelf executeCallback:callback status:JYAuthorStateAuthorized];
                    }
                            break;
                            
                        case PHAuthorizationStatusDenied:{
                            [weakSelf executeCallback:callback status:JYAuthorStateDenied];
                            //用户已经明确否认了这一照片数据的应用程序访问
                        }
                            
                            break;
                            
                        case PHAuthorizationStatusRestricted:{
                            [weakSelf executeCallback:callback status:JYAuthorStateRestricted];

                            //此应用程序没有被授权访问的照片数据。可能是家长控制权限
                            
                        }
                            break;
                            
                        default:{
                             [weakSelf executeCallback:callback status:JYAuthorStateNotSupport];
                            //其他。
                            
                        }
                            break;
                    }
                });
            }];
        }
//        ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
//        if (authStatus == ALAuthorizationStatusNotDetermined) { // 未授权
//            if ([UIDevice currentDevice].systemVersion.floatValue < 8.0) {
//                [self executeCallback:callback status:JYAuthorStateAuthorized];
//            } else {
//                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//                    if (status == PHAuthorizationStatusAuthorized) {
//                        [self executeCallback:callback status:JYAuthorStateAuthorized];
//                    } else if (status == PHAuthorizationStatusDenied) {
//                        [self executeCallback:callback status:JYAuthorStateDenied];
//                    } else if (status == PHAuthorizationStatusRestricted) {
//                        [self executeCallback:callback status:JYAuthorStateRestricted];
//                    }
//                }];
//            }
//
//        } else if (authStatus == ALAuthorizationStatusAuthorized) {
//            [self executeCallback:callback status:JYAuthorStateAuthorized];
//        } else if (authStatus == ALAuthorizationStatusDenied) {
//            [self executeCallback:callback status:JYAuthorStateDenied];
//        } else if (authStatus == ALAuthorizationStatusRestricted) {
//            [self executeCallback:callback status:JYAuthorStateRestricted];
//        }
    } else {
        [self executeCallback:callback status:JYAuthorStateNotSupport];
    }
}

/**
 *  请求相机权限
 *
 *  @param callback
 */
+ (void)requestCameraAuthorization:(void(^)(JYAuthorState status))callback{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (authStatus == AVAuthorizationStatusNotDetermined) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    [self executeCallback:callback status:JYAuthorStateAuthorized];
                } else {
                    [self executeCallback:callback status:JYAuthorStateDenied];
                }
            }];
        } else if (authStatus == AVAuthorizationStatusAuthorized) {
            [self executeCallback:callback status:JYAuthorStateAuthorized];
        } else if (authStatus == AVAuthorizationStatusDenied) {
            [self executeCallback:callback status:JYAuthorStateDenied];
        } else if (authStatus == AVAuthorizationStatusRestricted) {
            [self executeCallback:callback status:JYAuthorStateRestricted];
        }
    } else {
        [self executeCallback:callback status:JYAuthorStateNotSupport];
    }

}

#pragma mark - callback

+ (void)executeCallback:(void (^)(JYAuthorState))callback status:(JYAuthorState)status {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (callback) {
            callback(status);
        }
    });
}

@end
