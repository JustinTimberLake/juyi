//
//  JYAuthorTool.h
//  JY
//
//  Created by Duanhuifen on 2017/12/18.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,JYAuthorState) {
    JYAuthorStateAuthorized = 0,    // 已授权
    JYAuthorStateDenied,            // 拒绝
    JYAuthorStateRestricted,        // 应用没有相关权限，且当前用户无法改变这个权限，比如:家长控制
    JYAuthorStateNotSupport         // 硬件等不支持
};

@interface JYAuthorTool : NSObject

/**
 *  请求相册访问权限
 *
 *  @param callback
 */
+ (void)requestImagePickerAuthorization:(void(^)(JYAuthorState status))callback;

/**
 *  请求相机权限
 *
 *  @param callback 
 */
+ (void)requestCameraAuthorization:(void(^)(JYAuthorState status))callback;


@end
