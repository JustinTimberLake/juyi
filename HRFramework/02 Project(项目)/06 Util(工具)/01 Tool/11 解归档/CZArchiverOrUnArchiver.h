//
//  CZArchiverOrUnArchiver.h
//  CZW
//
//  Created by MAC on 16/7/29.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZArchiverOrUnArchiver : NSObject

//归档
+ (void)saveArchiverData:(id)object fileName:(NSString *)fileName;

//解档
+ (id)loadArchiverDataFileName:(NSString *)fileName;

//删除单个归档文件
+ (BOOL)removeArchiverFileName:(NSString *)fileName;

//删除所有归档文件
+ (BOOL)removeArchiverDirector;

//判断是否存在文件
+ (BOOL)isFileExisted:(NSString *)fileName;
//获取文件大小
+ (NSString *) getAllDataCachesSize;

@end
