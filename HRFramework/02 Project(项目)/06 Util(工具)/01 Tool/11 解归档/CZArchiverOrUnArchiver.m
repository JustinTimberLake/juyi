//
//  CZArchiverOrUnArchiver.m
//  CZW
//
//  Created by MAC on 16/7/29.
//  Copyright © 2016年 MAC. All rights reserved.
//

#import "CZArchiverOrUnArchiver.h"

@interface CZArchiverOrUnArchiver()

@end
@implementation CZArchiverOrUnArchiver
#define kDocuments_AppendingPathComponent(__Path__) \
[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:__Path__]

#define kFileManager_Default        [NSFileManager defaultManager]

#define ARCHIVER_DIRECTOR_PATH kDocuments_AppendingPathComponent(ARCHIVER_DIRECTOR_NAME)

#define ARCHIVER_DIRECTOR_NAME @"Archiver_director"
#define ARCHIVER_DIRECTOR_CONTENTFILE_PATH(__FILENAME__) \
[ARCHIVER_DIRECTOR_PATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.data",__FILENAME__]]\
//解档
+ (id)loadArchiverDataFileName:(NSString *)fileName
{

    NSString *filePath = ARCHIVER_DIRECTOR_CONTENTFILE_PATH(fileName);
    
    
    if (![kFileManager_Default fileExistsAtPath:filePath])
    {
        NSLog(@"归档文件并未创建,无法解档");
        return nil;
    }
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}

//归档
+ (void)saveArchiverData:(id)object fileName:(NSString *)fileName
{
    
    NSString *directoryPath = ARCHIVER_DIRECTOR_PATH;
    
    if (![kFileManager_Default fileExistsAtPath:directoryPath])
    {
        NSError *error ;
        [kFileManager_Default createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (error)
        {
            NSLog(@"归档文件夹不存在，而且创建失败");
            return;
        }
    }
    
    NSString* filePath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.data",fileName]];
    
    
    BOOL success = [NSKeyedArchiver archiveRootObject:object toFile:filePath];
    
    if (success)
    {
        NSLog(@"%@----->>>归档成功", filePath);
    }
}

//删除单个归档文件
+ (BOOL)removeArchiverFileName:(NSString *)fileName
{
    NSString* filePath = ARCHIVER_DIRECTOR_CONTENTFILE_PATH(fileName);
    if (![kFileManager_Default fileExistsAtPath:filePath])
    {
        NSLog(@"指定删除的归档文件并不存在，请仔细查看!!");
        return YES;
    }
    BOOL success = [kFileManager_Default removeItemAtPath:filePath error:nil];
    
    if (success)
    {
        NSLog(@"指定归档文件删除成功");
        return YES;
    }
    else
    {
        NSLog(@"指定归档文件删除失败");
        return NO;
    }
    
}

//删除所有归档文件
+ (BOOL)removeArchiverDirector {
    NSString *directorPath = ARCHIVER_DIRECTOR_PATH;
    if (![kFileManager_Default fileExistsAtPath:directorPath]) {
        NSLog(@"指定删除的归档文件夹并不存在，请仔细查看!!");
        return YES;
    }
    
    BOOL success = [kFileManager_Default removeItemAtPath:directorPath error:nil];
    
    if (success)
    {
        NSLog(@"指定归档文件夹删除成功");
        return YES;
    }
    else
    {
        NSLog(@"指定归档文件夹删除失败");
        return NO;
    }
    
    
}
//获取文件大小
+(NSString *)getAllDataCachesSize{
    //文件大小
    long long folderSize = 0.0;
    NSString *directoryPath = ARCHIVER_DIRECTOR_PATH;
    
    if (![kFileManager_Default fileExistsAtPath:directoryPath]){
        return 0;
    }
    NSArray *paths = [kFileManager_Default subpathsAtPath:directoryPath];
    for (NSString *fileName in paths) {
        NSString *allPath = [directoryPath stringByAppendingFormat:@"/%@",fileName];
        folderSize += [self fileSizeAtPath:allPath];
        NSLog(@"%lld",folderSize);
    }
    folderSize+=[[SDImageCache sharedImageCache] getSize]/1024.0/1024.0/1024.0;
    CGFloat size = folderSize;
    return [NSString stringWithFormat:@"%.2fKB",size];
}

//计算单个文件大小
+ (long long) fileSizeAtPath:(NSString *)filePath{
    NSFileManager * manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath :filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize]/1024.0/1024.0/1024.0;
    }
    return 0;
}


+ (BOOL)isFileExisted:(NSString *)fileName {
    
    NSString* filePath = ARCHIVER_DIRECTOR_CONTENTFILE_PATH(fileName);
    
    if ([kFileManager_Default fileExistsAtPath:filePath]) {
        return YES;
    }
    return NO;
}

@end
