//
//  UILabel+HighLight.h
//  JY
//
//  Created by Duanhuifen on 2018/2/27.
//  Copyright © 2018年 Risenb. All rights reserved.
//



@interface UILabel (HighLight)

- (void)setLabelTitleHighlight:(NSString *)title andSearchStr:(NSString *)searchStr;

// 新增特殊处理:标题的搜索颜色
- (NSMutableAttributedString *)attrStrFrom:(NSString *)titleStr searchStr:(NSString *)searchStr;
@end
