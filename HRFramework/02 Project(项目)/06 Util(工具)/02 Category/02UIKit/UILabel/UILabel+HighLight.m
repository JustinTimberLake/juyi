//
//  UILabel+HighLight.m
//  JY
//
//  Created by Duanhuifen on 2018/2/27.
//  Copyright © 2018年 Risenb. All rights reserved.
//

#import "UILabel+HighLight.h"

@implementation UILabel (HighLight)

- (void)setLabelTitleHighlight:(NSString *)title andSearchStr:(NSString *)searchStr
{
    if (searchStr.length > 0) {
        // 如果有搜索
        [self setAttributedText:[self attrStrFrom:title searchStr:searchStr]];
    }else{
        // do nothing;
//        [self setFont:[UIFont systemFontOfSize:18.f]];
    }
}

// 新增特殊处理:标题的搜索颜色
- (NSMutableAttributedString *)attrStrFrom:(NSString *)titleStr searchStr:(NSString *)searchStr
{
    if (!titleStr.length) {
        titleStr = @"";
    }
    NSMutableAttributedString *arrString = [[NSMutableAttributedString alloc]initWithString:titleStr];
    // 设置前面几个字串的格式:粗体、红色
    [arrString addAttributes:@{
                               NSForegroundColorAttributeName:[UIColor redColor]
                               }
                       range:[titleStr rangeOfString:searchStr]];
    return arrString;
}
@end
