//
//  UIImage+Color.h
//  JY
//
//  Created by Stronger_WM on 2017/6/28.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage*)imageWithColor:(UIColor*)aColor;

@end
