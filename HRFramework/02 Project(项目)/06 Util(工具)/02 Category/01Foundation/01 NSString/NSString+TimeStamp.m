//
//  NSString+TimeStamp.m
//  ipadTemp
//
//  Created by xiaoming on 16/12/15.
//  Copyright © 2016年 Risenb App Department With iOS. All rights reserved.
//

#import "NSString+TimeStamp.h"

@implementation NSString (TimeStamp)

- (NSString *)timeStamp13ToDateWithFormatter:(NSString *)formmatter {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[self integerValue] / 1000];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    f.dateFormat = formmatter;
    return [f stringFromDate:date];
}

- (NSString *)timeStamp10ToDateWithFormatter:(NSString *)formmatter {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.integerValue];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    f.dateFormat = formmatter;
    return [f stringFromDate:date];
}
+ (NSString *)YYYYMMDDWithTimevalue:(NSString *)timeString
{
    if (!timeString || !timeString.length || timeString.length < 10)
    {
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[timeString integerValue]];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

+ (NSString *)YYYYMMDDWithTimevalue:(NSString *)timeString styleWithDot:(BOOL)isDot
{
    if (!timeString || !timeString.length || timeString.length < 10)
    {
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    if (isDot) {
        [formatter setDateFormat:@"YYYY.MM.dd"];
        
    }else{
        
        [formatter setDateFormat:@"YYYY-MM-dd"];
    }
    
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[timeString integerValue]];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

+ (NSString *)YYYYMMDDHHMMWithTimevalue:(NSString *)timeString styleWithDot:(BOOL)isDot
{
    if (!timeString || !timeString.length || timeString.length < 10)
    {
        return @"";
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    if (isDot) {
        [formatter setDateFormat:@"YYYY.MM.dd HH:mm"];
        
    }else{
        
        [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    }
    
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[timeString integerValue]];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

//将时间 YYYY-MM-dd HH:mm   只保留年月日
+ (NSString *)YYYYMMDDWithDataStr:(NSString *)dataStr{
    if (dataStr.length) {
        NSArray * arr = [dataStr componentsSeparatedByString:@" "];
        NSString * str = arr[0];
        return str;
    }else{
        return nil;
    }
}
@end
