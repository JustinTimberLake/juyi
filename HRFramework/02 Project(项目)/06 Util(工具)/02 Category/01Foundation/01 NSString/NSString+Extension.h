//
//  NSString+SQExtension.h
//  SQ365
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 risenb-ios5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)
/**
 *  返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;

- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize;

- (CGSize)sizeWithAttributeText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize andLineSpacing:(int)lineSpacing;

- (BOOL)isEmptyStr;

- (BOOL)isBlankString:(NSString *)string;

- (NSString *)placeholder:(NSString *)placeholder;

- (NSString *)configSex;
// 处理手机号加*
- (NSString *)mobileHidden;

@end
