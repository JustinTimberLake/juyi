//
//  NSString+TimeStamp.h
//  ipadTemp
//
//  Created by xiaoming on 16/12/15.
//  Copyright © 2016年 Risenb App Department With iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TimeStamp)

///------------------------------------------------------
/// @(自定义时间样式)  mark
///------------------------------------------------------
- (NSString *)timeStamp13ToDateWithFormatter:(NSString *)formmatter;

- (NSString *)timeStamp10ToDateWithFormatter:(NSString *)formmatter;

//时间转换 YYYY-MM-dd
+ (NSString *)YYYYMMDDWithTimevalue:(NSString *)timeString;

//时间转换 isDot 是圆点
+ (NSString *)YYYYMMDDWithTimevalue:(NSString *)timeString styleWithDot:(BOOL)isDot;

//时间转换 YYYY-MM-dd HH:mm
+ (NSString *)YYYYMMDDHHMMWithTimevalue:(NSString *)timeString styleWithDot:(BOOL)isDot;

//将时间 YYYY-MM-dd HH:mm   只保留年月日
+ (NSString *)YYYYMMDDWithDataStr:(NSString *)dataStr;

@end
