//
//  NSString+SQExtension.m
//  SQ365
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 risenb-ios5. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)

- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (CGSize)sizeWithAttributeText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize andLineSpacing:(int)lineSpacing
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:lineSpacing];//调整行间距
    
    NSDictionary *attrs = @{NSFontAttributeName : font,NSParagraphStyleAttributeName:paragraphStyle.copy};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (BOOL)isEmptyStr{
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    return NO;
}
- (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

- (NSString *)placeholder:(NSString *)placeholder {
    if(self.length == 0 || self == nil) {
        return placeholder;
    }else{
        return self;
    }
}
- (NSString *)configSex {
    if(self.intValue == 1) {
        return @"男";
    }else{
        return @"女";
    }
}

- (NSString *)mobileHidden {
    if(self.length > 6) {
        return [self stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    return @"";
}

@end
