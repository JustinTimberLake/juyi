//
//  NSString+Verification.h
//  灵动生活
//
//  Created by MAC on 16/8/30.
//  Copyright © 2016年 LDSH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Verification)

- (BOOL)isEmailAddress;
- (BOOL)isUserName;
- (BOOL)isPassword;
- (BOOL)isEmail;
- (BOOL)isUrl;
- (BOOL)isTelephone;

/**
 *  Validate is number(限制输入纯数字时使用)
 *
 *  @param number The number is string
 *
 *  @return Bool
 */
+ (BOOL)validateNumber:(NSString*)number;

/**
 *  是否为空字符串
 *
 *  @return BOOL
 */
- (BOOL)isBlankString;

/**
 判断是否输入了 emoji（包含苹果自带） 表情

 @param string string

 @return BOOL
 */
+ (BOOL)stringContainsEmoji:(NSString *)string;

/**
 将所有表情替换为 @"" ,包含特殊符号//(在 didChange 方法调用)

 @param text 输入的文字

 @return new text
 */
- (NSString *)disable_emoji:(NSString *)text;

@end
