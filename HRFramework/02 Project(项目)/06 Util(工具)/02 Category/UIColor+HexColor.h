//
//  UIColor+HexColor.h
//  中润
//
//  Created by cxx on 16/4/1.
//  Copyright © 2016年 cxx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)
+ (UIColor*) colorWithHexString:(NSString *)color;
+ (UIColor*) colorWithHex:(long)hexColor alpha:(float)opacity;
+ (UIColor*) colorWithHex:(long)hexColor;
@end
